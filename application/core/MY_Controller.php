<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
** Project  : Scorilo
** Date     : 13-April-2021
** Defining My controller for admin Panel.(All admin page requests once passes from here Except Login)
*/
class MY_Controller extends MX_Controller {

    function __construct()
    { 
        parent::__construct();
        $this->output->enable_profiler(FALSE);
        if(!$this->session->userdata(ADMIN_SESSION_NAME)){
            redirect('authpanel/login');
        }

        if($this->session->userdata(ADMIN_LOCK_NAME) == 0)
            redirect('authpanel/login/lock_screen');
    }
}

class MY_Controller_Customer extends MX_Controller {
    function __construct()
    { 
        parent::__construct();
        $this->output->enable_profiler(FALSE);
        // echo "AN";
        // print_r($this->session->userdata('website_userdata'));
        // die;
        if(!$this->session->userdata('website_userdata'))
        {
            redirect('website');
        }

        // $response = $this->db->get_where("tbl_user",array("id"=>$this->session->userdata('user_id')))->row_array();
        $response = $this->common_model->common_singleSelect("tbl_user",array("id"=>$this->session->userdata('user_id')));
        if(!empty($response) && $response['status']=='Inactive')
        {
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('website_userdata');
            $this->session->sess_destroy();
            redirect('website');
        }
    }
}


?>