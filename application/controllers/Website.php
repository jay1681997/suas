<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
** Project Name : SUAS App
** Date         : 03-August-2022
*/
class Website extends MX_Controller {

    private $viewfolder = 'website/';
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('common_model');
        $this->load->library('customtwilio');
        
    }

    public function index(){
        if(!empty($this->session->userdata('user_id'))) {

               redirect('dashboard');
        }
        $currency = $this->common_model->common_singleSelect('tbl_setting', array('is_deleted'=>'0','status'=>'Active'));
        $this->session->set_userdata('currency',$currency['currency_symbol']);
        $data['classes'] = $this->common_model->common_multipleSelect('tbl_master_class',array('status'=>'Active','is_deleted'=>'0'));
        $data['home_videos'] = $this->common_model->common_multipleSelect('tbl_home_video',array('is_deleted'=>'0'));
        $data['contests'] = $this->common_model->get_contestlist();
        $this->load->view($this->viewfolder.'index', $data);
    }
    public function region_list($country_name){
        $country_data = $this->common_model->common_singleSelect("tbl_country",array("name"=>$country_name,"is_deleted"=>"0"));
        $region_list = $this->common_model->common_multipleSelect("tbl_tax_details",array("country_id"=>$country_data['id'],"is_deleted"=>"0","is_active"=>"1"));
           echo json_encode($region_list);
        die;
        // print_r($country_name);die;
    }
    public function signup(){

        if(!empty($this->session->userdata('user_id'))) {
     
               redirect('dashboard');
        }

     
         $data['countries']      = $this->common_model->get_country_code_new();
        if($this->input->post()){
     
            $this->form_validation->set_rules('firstname','First Name','required|trim|max_length[32]');
            $this->form_validation->set_rules('lastname','Last Name','required|trim|max_length[32]');
            $this->form_validation->set_rules('email','Email','trim|max_length[255]|valid_email|is_unique[tbl_user.is_deleted="0" AND email=]');
            $this->form_validation->set_rules('country_code','Country Code','trim|regex_match[/^\+[(0-9)]+$/]'); 
            $this->form_validation->set_rules('phone','Phone Number','trim|max_length[255]|is_unique[tbl_user.is_deleted="0" AND phone=]');
            $this->form_validation->set_rules('dob','DOB','required|trim');
            $this->form_validation->set_rules('country','Country','required|trim');
            $this->form_validation->set_rules('password','Password','required|trim|xss_clean');
            $this->form_validation->set_rules('latitude','Latitude','trim');
            $this->form_validation->set_rules('longitude','Longitude','trim');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if($this->form_validation->run()) {
                
                $profile_image = 'default.png'; 
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],USER_IMAGE);
                    
                    if(!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->viewfolder.'sign-up',$data);
                        return false;
                    }
                }

                $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                $age  = date_diff(date_create($date_format), date_create('today'))->y;
                $age_category = '';

                if($age >= 0 && $age <= 8){
                    $age_category = 'Primary';
                }else if($age >= 9 && $age <= 12){
                    $age_category = 'Juniors';
                }else if($age >= 13 && $age <= 17){
                    $age_category = 'Teens';
                }else if($age >= 18){
                    $age_category = 'Adults';
                }

                $otp = rand(1000,9999);
                $userarray = array(
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'username'      => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'email'         => $this->input->post('email'),
                    'country_code'  => !empty($this->input->post('country_code')) ? $this->input->post('country_code') : '',
                    'phone'         => $this->input->post('phone'),
                    'country'       => $this->input->post('country'),
                    'dob'           => $date_format,
                    'profile_image' => $profile_image,
                    'password'      => $this->common_model->encrypt_password($this->input->post('password')),
                    'latitude'      => !empty($this->input->post('latitude')) ? $this->input->post('latitude') : "",
                    'longitude'     => !empty($this->input->post('longitude')) ? $this->input->post('longitude') : "",
                    'promocode'     => !empty($this->input->post('promocode')) ? $this->input->post('promocode') : "",
                    'age'           => $age,
                    'age_category'  => $age_category,
                    'otp_code'      => $otp,
                    'login_status'  => 'Online',
                    'last_login'    => date('Y-m-d H:i:s'),
                    'region_id'     => !empty($this->input->post('region_id')) ? $this->input->post('region_id') : "0",
                );
               
                $user_id   =  $this->common_model->common_insert('tbl_user', $userarray);

                $user_data =  $this->common_model->common_singleSelect('tbl_user', array('id'=>$user_id));

                // $this->session->set_userdata('user_id', $user_id);
                $this->session->set_userdata('website_userdata', $user_data);
                $this->session->set_userdata('website_timezone', $this->input->post('timezone'));

                $deviceparam=array(
                    'user_id'       => $user_id,
                    'user_type'     => 'U',
                    'token'         => "",
                    'device_token'  => "",
                    'uuid'          => "",
                    'ip'            => "",
                    'os_version'    => "",
                    'model_name'    => "",
                    'device_type'   => "",
                );
                   $template = "Thanks for registering at Stand Up and Speak. Your OTP code is " . $otp;
                $phone = $this->input->post('country_code').$this->input->post('phone');
                // print_r($pho);
                $send = $this->common_model->sendSMS($phone,$template);
                $message = $this->load->view('template/registeruser', $userarray, TRUE);
                $this->common_model->send_mail(ADMIN_EMAIL,$this->input->post('email'), $message, 'Register New User');
                $this->common_model->common_insert('tbl_user_deviceinfo',$deviceparam);
                $this->session->set_flashdata('success_msg', $this->lang->line('website_otp_send_success'));
                $this->session->set_userdata("login_type","signup");
                redirect(base_url().'otp-verification');
              
            } else {
                $this->load->view($this->viewfolder.'signup',$data);    
            }
        }else{
            $this->load->view($this->viewfolder.'signup',$data);
        }
        
    }

    public function otp_verification($userid = ''){

        if(!empty($this->session->userdata('user_id'))) {
     
               redirect('dashboard');
        }

        $customerData = $this->session->userdata('website_userdata');

        if($this->input->post()){
        
            $this->form_validation->set_rules('otp_code','OTP Code','required|trim|integer|is_natural_no_zero|min_length[4]|max_length[4]');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if($this->form_validation->run()) {
                $userdata=$this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
                if(!empty($userdata)) {

                    if($userdata['otp_code']==$this->input->post('otp_code')) {

                        $param['otp_status']='Verify';
                        $param['login_status'] = 'Online';
                        $param['last_login'] = date('Y-m-d H:i:s');
                        // $userparam=array(
                        //     'login_status'=>'Online',
                        //     'last_login'=>date('Y-m-d H:i:s'),
                        // );
                        $this->common_model->common_singleUpdate('tbl_user', $param, array('id'=> $customerData['id']));
                      
                        $userdata=$this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
                        $this->session->set_userdata('website_userdata', $userdata);

                        if((!empty($this->session->userdata('forgot_password'))) && ($this->session->userdata('forgot_password') == 'phone')){
                            $data['redirect_url']="create-new-password";
                        }else{
                            if(empty($userdata['profile_video'])){
                                  $data['redirect_url']="upload-video";  
                            }else{
                                  $data['redirect_url']="dashboard";  
                            }
                          
                        }
                        $user_data=$this->common_model->common_singleSelect('tbl_user', array('id'=>$customerData['id']));
                        $this->session->set_userdata('user_id',$user_data['id']);
                        $this->session->set_userdata('website_userdata',$user_data);
                        $this->session->set_userdata('website_timezone', $this->input->post('timezone'));
                        $data['status']="success";
                        $data['msg']=$this->lang->line('website_otpcodeverify_sucess');

                    } else {

                        $data['status']="error";
                        $data['msg']=$this->lang->line('website_sentotp_error');

                    }
                } else {

                    $data['status']="error";
                    $data['msg']=$this->lang->line('website_customer_details_not_found');

                }
            } else {
                $data['status']="validation_error";
                $data['msg']=form_error('otp_code');
            }
         
            echo json_encode($data);die;
        }else{
            $this->load->view($this->viewfolder.'otp_verification');
        } 
    }

    public function resend_otp()
    {
        if(!empty($this->session->userdata('user_id'))) {
     
               redirect('dashboard');
        }

        $customerData = $this->session->userdata('website_userdata');
        if(!empty($customerData)) {
            if($customerData['otp_status'] == 'Verify') {
                $data['status'] = "Verify";
                $data['msg'] = $this->lang->line('website_otpalreadyverify_error');
            } else {
                //generat otp code
                $otp_code = rand(1000,9999);
                 $template = "Thanks for registering at Stand Up and Speak. Your OTP code is " . $otp_code;
                $phone = $customerData['country_code'].$customerData['phone'];
                if($this->common_model->sendSMS($phone,$template)) {

                    $userarray['otp_code']=$otp_code;
                    $userarray['otp_status']="Pending";
                   
                    $this->common_model->common_singleUpdate('tbl_user',$userarray, array('id'=>$customerData['id']));

                    $data['status']="success";
                    $data['msg']=$this->lang->line('website_sendotp_success');
                } else {
                    $data['status']="error";
                    $data['msg']=$this->lang->line('website_otpcodeverify_otp_failed');
                }
            }
        } else {
            $data['status']  = "error";
            $data['msg']     = $this->lang->line('website_customer_details_not_found');
        }
        echo json_encode($data);die;
    }

    public function create_password(){

        if(!empty($this->session->userdata('user_id'))) {
            // redirect(base_url());
               redirect('dashboard');
        }

        $customerData = $this->session->userdata('website_userdata');
        // echo $user_id ;
        if($this->input->post()){
   
            $this->form_validation->set_rules('password','Password','required|trim|xss_clean');  
            $this->form_validation->set_rules('confirmpassword','Confirm Password','required|trim|xss_clean'); 

            if($this->form_validation->run()) {

                $userdata = $this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
                // echo "<pre>"; print_r($userdata); die;
                if(!empty($userdata)) {
                    $password = trim($this->common_model->decrypt_password($userdata['password']));
                   
                        if($password != $this->input->post('password')) {
                            $user_array=array(
                                'password'=>$this->common_model->encrypt_password($this->input->post('password')),
                                'login_status'=>'Offline'
                            );
                            $this->common_model->common_singleUpdate('tbl_user', $user_array, array('id'=> $customerData['id']));
                         
                            $newuser_data = $this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
                            $this->session->set_userdata('website_userdata', $newuser_data);
                            $this->session->set_flashdata('success_msg',$this->lang->line('website_create_password_success'));
                            redirect(base_url().'create-new-password');
                        } else {
                 
                            $data['error_msg']=$this->lang->line('website_oldnew_password_same');
                            $this->load->view($this->viewfolder.'create_password',$data);
                            return ;
                        }
                    
                } else {
                   
                    $data['error_msg']=$this->lang->line('website_customer_details_not_found');
                    $this->load->view($this->viewfolder.'create_password',$data);
                    return ;
                }    
            } else {
                
                $this->load->view($this->viewfolder.'create_password');
            }
        } else {

            $this->load->view($this->viewfolder.'create_password');
        }
      
    }

    public function login(){

        $data['countries']      = $this->common_model->get_country_code_new();
       if(!empty($this->session->userdata('user_id'))){
            redirect('dashboard');
       }
        $this->session->unset_userdata('website_userdata');
        $this->session->unset_userdata('user_id');
        if($this->input->post()){
            $this->form_validation->set_rules('country_code','Country Code','required|trim');
            $this->form_validation->set_rules('phone','Phone','required|trim');
            $this->form_validation->set_rules('password','Password','required|trim|xss_clean');
           
            $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
            if($this->form_validation->run()) {
                  $login_user=$this->common_model->common_singleSelect('tbl_user', array('is_deleted'=>'0','country_code'=>$this->input->post('country_code'), 'phone'=>$this->input->post('phone')));
                if(!empty($login_user)){
                    if($this->common_model->decrypt_password($login_user['password'])!=$this->input->post('password')) {
                     
                        $this->session->set_flashdata('error_msg', $this->lang->line('website_loginpassword_error'));
                        $this->load->view($this->viewfolder.'login',$data);
                        return ;
                    } 
                      if($login_user['otp_status'] == 'Pending') {
                           $otp = rand(1000,9999);
                          $template = "Thanks for registering at Stand Up and Speak. Your OTP code is " . $otp;
                          $phone = $this->input->post('country_code').$this->input->post('phone');
                          
                          $send = $this->common_model->sendSMS($phone,$template);

                          $this->common_model->common_singleUpdate('tbl_user',array("otp_code"=>$otp), array('id'=>$login_user['id']));
                          $user_data =  $this->common_model->common_singleSelect('tbl_user', array('id'=>$login_user['id']));

                        $this->session->set_userdata('website_userdata', $user_data);
                        
                        $this->session->set_flashdata('success_msg', $this->lang->line('website_otp_send_success'));
                        $this->session->set_userdata("login_type","login");
                        redirect(base_url().'otp-verification');
                        
                    } 

                    if($login_user['is_deleted'] == 1){
                        echo "IF";
                        $this->session->set_flashdata('error_msg', $this->lang->line('website_login_country_code_phone_error'));
                        $this->load->view($this->viewfolder.'login',$data); 
                        redirect(base_url().'login');
                    }
                    if($login_user['status']=='Inactive') {
                        
                        $this->session->set_flashdata('error_msg', $this->lang->line('website_logindisactive_error'));
                        
                        $this->load->view($this->viewfolder.'login',$data);
                        return ;
                    } else {
                        $userparam=array(
                            'login_status'=>'Online',
                            'last_login'=>date('Y-m-d H:i:s'),
                        );
                      
                        $this->common_model->common_singleUpdate('tbl_user',$userparam, array('id'=>$login_user['id']));

                        $user_data=$this->common_model->common_singleSelect('tbl_user', array('id'=>$login_user['id']));
                        $currency_data = $this->common_model->get_currency_rate($user_data['country']);
                        // $this->session->set_userdata('user_id',$user_data['id']);
                        $this->session->set_userdata("currency_data",$currency_data[0]);
                        if($currency_data[0]['rate'] != '' && $currency_data[0]['rate'] != 0 && $currency_data[0]['rate'] != undefined){
                            $this->session->set_userdata("currency_rate",$currency_data[0]['rate']);   
                        }else{
                         $this->session->set_userdata("currency_rate",1);   
                        }
                        $this->session->set_userdata("currency",$currency_data[0]['currency_symbol']);
                        $this->session->set_userdata('website_userdata',$user_data);
                        $this->session->set_userdata('website_timezone', $this->input->post('timezone'));
                        if(empty($user_data['profile_video'])){
                            redirect(base_url().'upload-video');
                        }else{
                            $this->session->set_userdata('user_id',$user_data['id']);

                                redirect(base_url().'dashboard');
                        }
                            
                    }
                } else {
                    $this->session->set_flashdata('error_msg', $this->lang->line('website_login_country_code_phone_error'));
                    $this->load->view($this->viewfolder.'login',$data);
                }
            } else {
                $this->load->view($this->viewfolder.'login',$data);
            }
        }else{
            $this->load->view($this->viewfolder.'login',$data);
        }
    }

    public function forget_password(){
        if(!empty($this->session->userdata('user_id'))) {
            redirect(base_url());
        }

        $data['countries']      = $this->common_model->get_country_code_new();

        if($this->input->post()){

            $this->form_validation->set_rules('forgot_method','Forgot Method','required|trim');

            if($this->input->post('forgot_method')=='messages') {

                $this->form_validation->set_rules('country_code','Country Code','required|trim|regex_match[/^\+[(0-9)]+$/]');

                $this->form_validation->set_rules('phone','Phone','required|trim');

            } else {
                $this->form_validation->set_rules('email','Email','required|trim|valid_email');
            }
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if($this->form_validation->run()) {

                if($this->input->post('forgot_method')=='messages') {

                    $where['country_code']=$this->input->post('country_code');
                    $where['phone']=$this->input->post('phone');
                } else {

                    $where['email']=$this->input->post('email');
                }

                $where['is_deleted']='0';

                $login_user= $this->common_model->common_singleSelect('tbl_user', $where);

          
                if(!empty($login_user)){

                  
                        $sentcode=false;
                        $forgotpwd_code="";

                        if($this->input->post('forgot_method')=='messages') {

                            $forgotpwd_code=rand(1000,9999);
                            $opt_code_message = $forgotpwd_code.' is the OTP to login to your account.';

                            $sentcode=$this->common_model->sendSMS($login_user['country_code'].$login_user['phone'],$opt_code_message);

                        } else {

                            $forgotpwd_code=rand(100000,999999);
                            $login_user['forgotpwd_code']=$forgotpwd_code;
                           
                            $message = $this->load->view('template/forgot_password_email',$login_user,TRUE);
                            $subject = PROJECT_NAME.' - Forgot Password';

                            $sentcode=$this->common_model->send_mail($this->input->post('email'),ADMIN_EMAIL,$message,$subject);

                        }
                        if($sentcode){

                            $forgotdata=array(
                                'otp_code'                  =>  $forgotpwd_code,
                                // 'otp_status'                  =>  'Pending',
                                'forgotpassword_token'      =>  rand(100000,999999),
                                'forgotpassword_datetime'   =>  date('Y-m-d H:i:s')
                            );
                            if($this->input->post('forgot_method')=='messages') {
                                 $forgotdata['otp_status'] = 'Pending';
                            }

                            $this->common_model->common_singleUpdate('tbl_user',$forgotdata, array('id'=>$login_user['id']));

                            $user_data = $this->common_model->common_singleSelect('tbl_user',array('id'=>$login_user['id']));
                            $responsedata['status']="success";
                            if($this->input->post('forgot_method')=='messages') {
                             
                                $responsedata['msg']=$this->lang->line('website_forgotphone_success');
                                $this->session->set_userdata('website_userdata', $user_data);
                                $this->session->set_userdata('forgot_password', 'phone');
                            } else {
                                $this->session->set_userdata('forgot_password', "yes");
                                $responsedata['msg']=$this->lang->line('website_forgotemail_success');
                            }
                            
                            $responsedata['user_data']=$user_data;
                        } else {
                            $this->session->unset_userdata('forgot_password');
                            $responsedata['status']="error";
                            $responsedata['msg']=$this->lang->line('website_sentmail_error');
                        }
                  
                } else {
                    $this->session->unset_userdata('forgot_password');
                    $responsedata['status']="error";
                    if($this->input->post('forgot_method')=='messages') {
                        $responsedata['msg']=$this->lang->line('website_forgotphone_error');
                    } else {
                        $responsedata['msg']=$this->lang->line('website_forgotemail_error');
                    }
                }
            } else {
                $this->session->unset_userdata('forgot_password');
                $responsedata['status']="validation_error";
                if($this->input->post('forgot_method')=='messages') {
                    $responsedata['msg']=form_error('country_code');
                    $responsedata['msg']=form_error('phone');
                } else {
                    $responsedata['msg']=form_error('email');
                }
            }
            echo json_encode($responsedata);die;
        }else{
            $this->session->unset_userdata('forgot_password');
            $this->load->view($this->viewfolder.'forget_password', $data);
        }
    }

    public function privacy_policy(){
        $data['privacy_policy'] = $this->common_model->common_singleSelect('tbl_app_content', array());
        $this->load->view($this->viewfolder.'privacy_policy',$data);
    }
    public function terms_conditions(){
        $data['terms_conditions'] = $this->common_model->common_singleSelect('tbl_app_content', array());
        $this->load->view($this->viewfolder.'terms_conditions', $data);
    }

    public function about_us(){
        $data['about_us'] = $this->common_model->common_singleSelect('tbl_app_content', array());
        $this->load->view($this->viewfolder.'about_us', $data);
    }

    public function help_and_faq(){
        $data['help_and_faq'] = $this->common_model->common_multipleSelectWithoutOrder('tbl_faq', array('is_deleted'=>'0','status'=>'Active'));
        $this->load->view($this->viewfolder.'help_and_faq', $data);
    }

    public function subscription(){
        $data['subscription'] = $this->common_model->common_multipleSelect('tbl_master_subscription', array('is_deleted'=>'0','status'=>'Active'));
        $sub_sc = array();
        if(!empty($data['subscription'])){
            foreach ($data['subscription'] as $key => $value) {
                $order_data = $this->common_model->get_order_details("subscription",$value['id'],$this->session->userdata('user_id'));
                if(empty($order_data)){
                   array_push($sub_sc,$value); 
                }
            }
        }
         $data['subscription'] = $sub_sc;

        $this->load->view($this->viewfolder.'subscription', $data);
    }

    public function contact(){

        if($this->input->post()){

            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('email','Email','required|trim|valid_email');
            $this->form_validation->set_rules('description','Description','required|trim');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if($this->form_validation->run()) { 
             
                    $contact_us=array(
                        'name'          => $this->input->post('name'),
                        'email'         => $this->input->post('email'),
                        'description'   => $this->input->post('description'),
                        'user_id'       => !empty($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : 0,
                    );

                    $subject = 'Contact Us Enquiry';
                    $this->common_model->common_insert('tbl_contact_us', $contact_us);
            
                    $message  = "<html><body><table align='center' border='0' cellpadding='0' cellspacing='0' width='100px'>";
                    $message .= "<tr><td bgcolor='#ffffff' style='padding: 10px 20px 30px 5px;'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td><h4>Hi  ,</h4></td></tr>";
                    $message .= "<tr><td style='padding: 5px 0 10px 0;'><b>Customer Name: " .$this->input->post('name')." </b></td></tr>"; 
                    $message .= "<tr><td style='padding: 5px 0 10px 0;'><b>Email: ".$this->input->post('email')." </b></td></tr>";
                    $message .= "<tr><td style='padding: 5px 0 10px 0;'><b>Message: ".$this->input->post('description')." </b></td></tr>";
                    $message .= "<tr><td style='padding: 10px 0 20px 0;'><b>Thanks,</b> </td></tr><tr><td style='padding: 10px 0 20px 0;'><b>The Stand Up And Speak Team</b> </td> </tr></table></td></tr>";
                    $message .="</table></body></html>";
                    
                    if($this->common_model->send_mail(ADMIN_EMAIL, $this->input->post('email'),$message,$subject))
                    {
                        $message_user  = "<center><img src='http://52.7.79.220/assets/images/app_icon.png' style='width:100px;height:100px;' width='100px' height='100px'></center>";
                        $message_user .= "Hello ".$this->input->post('name').",";
                        $message_user .="<h3>Thanks for contacting Stand Up And Speak App!</h3><br>";
                        $message_user .= "<p>This is just a quick note to let you know we've received your message, and will respond as soon as we can.</p>";
                        $message_user .="<p>The Stand Up And Speak Team</p>";
                        $send_email_setting = $this->common_model->common_singleSelect('tbl_user_notification_setting', array("user_id"=>$this->session->userdata('user_id')));
                        if ($send_email_setting['email'] == 'yes') {
                        $this->common_model->send_mail($this->input->post('email'), ADMIN_EMAIL, $message_user,$subject);
                        $this->session->set_flashdata("success_msg",$this->lang->line('website_sentmail_success'));
                        redirect("contact");
                        }else if(empty($send_email_setting)){
                              $this->common_model->send_mail($this->input->post('email'), ADMIN_EMAIL, $message_user,$subject);
                              $this->session->set_flashdata("success_msg",$this->lang->line('website_sentmail_success'));
                             redirect("contact");

                        }else{
                          $this->session->set_flashdata("success_msg",$this->lang->line('website_sentmail_success'));
                          redirect("contact");
                        }
                    }else{
                        $this->session->set_flashdata("error_msg",$this->lang->line('website_sentmail_error'));
                         redirect("contact");
                    }  
            
            } else {
                $this->load->view($this->viewfolder.'contact',$data);    
            }    
        } else {
            $this->load->view($this->viewfolder.'contact',$data);
        }
    }

    public function logout()
    {
        
        if(empty($this->session->userdata('user_id'))) {
            redirect(base_url().'login');
        }

        $this->common_model->common_singleUpdate('tbl_user',array("login_status"=>'Offline'), array('id'=>$this->session->userdata('user_id')));
      
        $this->session->unset_userdata('forgot_password');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('website_userdata');        
        $this->session->sess_destroy();
        redirect(base_url());
    }

       public function delete_account()
    {
        
        if(empty($this->session->userdata('user_id'))) {
            redirect(base_url().'login');
        }

        $this->common_model->common_singleUpdate('tbl_user',array("login_status"=>'Offline',"is_deleted"=>"1"), array('id'=>$this->session->userdata('user_id')));
      
        $this->session->unset_userdata('forgot_password');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('website_userdata');        
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function upload_video(){

        if(!empty($this->session->userdata('user_id'))) {
            redirect(base_url());
        }

        $customerData = $this->session->userdata('website_userdata');

        if($_FILES){

            $videoname = "";
            if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) 
            {   
                $uploaded_filename = $this->common_model->uploadImageS3($_FILES['video'], USER_IMAGE);
                if(!$uploaded_filename) {
                    $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                    $this->load->view($this->viewfolder.'upload_video');
                    return false;
                }
                $videoname = $uploaded_filename;                   
            }

            $this->common_model->common_singleUpdate('tbl_user', array('profile_video'=> $videoname), array('id'=> $customerData['id']));
            $this->session->set_flashdata("success_msg",$this->lang->line('website_upload_video_success'));
            redirect("add-student");
            
        }else{
            $this->load->view($this->viewfolder.'upload_video');
        } 
    }

    public function add_student(){

        if(!empty($this->session->userdata('user_id'))) {
            redirect(base_url());
        }

        $customerData = $this->session->userdata('website_userdata');
        $data['user_data'] = $this->common_model->common_singleSelect('tbl_user',array("id"=>$customerData['id']));
        $data['result'] = $this->common_model->common_multipleSelect('tbl_student', array('is_deleted'=>'0', 'user_id'=>$customerData['id']));
        if($this->input->post()){
            // print_r($this->input->post());die;
            if(count($data['result']) < 5){
                $this->form_validation->set_rules('firstname','First Name','required|trim|max_length[32]');
                $this->form_validation->set_rules('lastname','Last Name','required|trim|max_length[32]');
                $this->form_validation->set_rules('dob','DOB','required|trim');
                $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

                if($this->form_validation->run()) {

                    $userdata = $this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
                 
                    if(!empty($userdata)) {

                        $profile_image = 'default.png'; 
                        if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                            $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],STUDENT_IMAGE);
                        
                            if(!$profile_image) {
                                $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                                $this->load->view($this->viewfolder.'add-student');
                                return false;
                            }
                        }

                        $profile_video = ''; 
                        if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
                            $profile_video = $this->common_model->uploadImageS3($_FILES['video'],STUDENT_IMAGE);
                        
                            if(!$profile_video) {
                                $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                                $this->load->view($this->viewfolder.'add-student');
                                return false;
                            }
                        }

                        $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                        $age  = date_diff(date_create($date_format), date_create('today'))->y;
                        $age_category = '';

                        if($age >= 0 && $age <= 8){
                            $age_category = 'Primary';
                        }else if($age >= 9 && $age <= 12){
                            $age_category = 'Juniors';
                        }else if($age >= 13 && $age <= 17){
                            $age_category = 'Teens';
                        }else if($age >= 18){
                            $age_category = 'Adults';
                        }

                        $otp = rand(1000,9999);
                        $userarray = array(
                            'firstname'     => $this->input->post('firstname'),
                            'lastname'      => $this->input->post('lastname'),
                            'username'      => $this->input->post('firstname').' '.$this->input->post('lastname'),
                            'dob'           => $date_format,
                            'profile_image' => $profile_image,
                            'profile_video' => $profile_video,
                            'age'           => $age,
                            'age_category'  => $age_category,
                            'user_id'       => $userdata['id'],
                        );
                       
                        $student_id   =  $this->common_model->common_insert('tbl_student', $userarray);
                        $this->session->set_flashdata("success_msg",$this->lang->line('website_student_added_success'));
                        redirect('add-student');
                    } else {
                        $this->session->set_flashdata("error_msg",$this->lang->line('website_customer_details_not_found'));
                        $this->load->view($this->viewfolder.'add_student',$data);
                    }    
                } else {
                    
                    $this->load->view($this->viewfolder.'add_student',$data);
                }
            }else{

                $this->session->set_flashdata("error_message",$this->lang->line('website_student_limit_reached'));
                $this->load->view($this->viewfolder.'add_student',$data);
            }
            
        } else {

            $this->load->view($this->viewfolder.'add_student',$data);
        }
    }

    public function edit_student($student_id){
        $student_id = base64_decode($student_id);
        if(!empty($this->session->userdata('user_id'))) {
            redirect(base_url());
        }

        $customerData = $this->session->userdata('website_userdata');
        
        $data['result'] = $this->common_model->common_singleSelect('tbl_student', array('id'=>$student_id));
        // echo count($data['result']); die;
        if($this->input->post()){

            // echo "<pre>"; print_r($_FILES); die;
            $this->form_validation->set_rules('firstname','First Name','required|trim|max_length[32]');
            $this->form_validation->set_rules('lastname','Last Name','required|trim|max_length[32]');
            $this->form_validation->set_rules('dob','DOB','required|trim');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if($this->form_validation->run()) {

                $userdata = $this->common_model->common_singleSelect('tbl_user', array('id'=> $customerData['id']));
             
                if(!empty($userdata)) {

                    $profile_image = $data['result']['profile_image']; 
                    if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                        $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],STUDENT_IMAGE);
                    
                        if(!$profile_image) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->viewfolder.'add-student');
                            return false;
                        }
                    }

                    $profile_video = $data['result']['profile_video']; 
                    if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
                        $profile_video = $this->common_model->uploadImageS3($_FILES['video'],STUDENT_IMAGE);
                    
                        if(!$profile_video) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->viewfolder.'add-student');
                            return false;
                        }
                    }

                    $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                    $age  = date_diff(date_create($date_format), date_create('today'))->y;
                    $age_category = '';

                    if($age >= 0 && $age <= 8){
                        $age_category = 'Primary';
                    }else if($age >= 9 && $age <= 12){
                        $age_category = 'Juniors';
                    }else if($age >= 13 && $age <= 17){
                        $age_category = 'Teens';
                    }else if($age >= 18){
                        $age_category = 'Adults';
                    }

                    $otp = rand(1000,9999);
                    $userarray = array(
                        'firstname'     => $this->input->post('firstname'),
                        'lastname'      => $this->input->post('lastname'),
                        'username'      => $this->input->post('firstname').' '.$this->input->post('lastname'),
                        'dob'           => $date_format,
                        'profile_image' => $profile_image,
                        'profile_video' => $profile_video,
                        'age'           => $age,
                        'age_category'  => $age_category,
                        'user_id'       => $userdata['id'],
                    );
                   
                    $student_id   = $this->common_model->common_singleUpdate('tbl_student', $userarray,array('id'=>$student_id));
                    $this->session->set_flashdata("success_msg",$this->lang->line('website_student_updated_success'));
                    redirect('add-student');
                } else {
                    $this->session->set_flashdata("error_msg",$this->lang->line('website_customer_details_not_found'));
                    $this->load->view($this->viewfolder.'add_student',$data);
                }    
            } else {
                
                $this->load->view($this->viewfolder.'edit_student',$data);
            }
            
        } else {

            $this->load->view($this->viewfolder.'edit_student',$data);
        }
    }

        public function notification_setting()
    {
        $customerData = $this->session->userdata('website_userdata');
        $user_id = $customerData['id'];
         $this->session->set_userdata("user_id",$customerData['id']);
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('user_id' => $user_id, 'is_deleted' => '0'));
        if ($this->input->post()) {

            $setting_data = array(
                "user_id" => $user_id,
                "push_notification" => ($this->input->post('push_notification') == 'on') ? 'yes' : 'no',
                "email" => ($this->input->post('email') == 'on') ? 'yes' : 'no',
                "sms" => ($this->input->post('sms') == 'on') ? 'yes' : 'no'
            );
            if (!empty($data['notification_setting'][0]) && $data['notification_setting'][0] != '') {
                $this->common_model->common_singleUpdate('tbl_user_notification_setting', $setting_data, array("id" => $data['notification_setting'][0]['id']));
                $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('user_id' => $user_id, 'is_deleted' => '0'));
                $this->session->set_flashdata('success_msg', $this->lang->line('website_notification_updated_success'));
                // $this->load->view($this->view_folder . 'notification_setting', $data);
                  redirect('dashboard');
            } else {
                $user_id = $this->common_model->common_insert('tbl_user_notification_setting', $setting_data);
                $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('id' => $user_id));
                $this->session->set_flashdata('success_msg', $this->lang->line('website_notification_updated_success'));
            redirect('dashboard');
            }
        } else {
              redirect('add-student');
        }
    }

    public function delete_student($student_id){
        
        $this->common_model->common_delete('tbl_student',array('id'=>$student_id));
         echo "Student deleted successfully";die;
    }
    
}