<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
** Project Name : SUAS App
** Date         : 12-April-2021
*/
class Home extends MX_Controller {

    private $viewfolder = 'page/';
    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('authpanel/content_model');
    }
    
    /*
    ** Funtion to load about us content for app
    ** 12-April-2021
    */
    public function about_content() {
        $data['app_content']=$this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'about_us',$data);
    }

    /*
    ** Funtion to load Terms & Condition content for app
    ** 12-April-2021
    */
    public function terms_and_condition() {
        $data['app_content']=$this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'terms_and_condition',$data);
    }

    /*
    ** Funtion to load Privacy Policy content for app
    ** 12-April-2021
    */
    public function policy_content() {
        $data['app_content'] = $this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'privacypolicy',$data);
    }

    /*
    ** Funtion to load Privacy Policy content for app
    ** 12-April-2021
    */
    public function date_of_birth() {
        $data['app_content'] = $this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'date_of_birth',$data);
    }

    /*
    ** Funtion to load FAQ content for app
    ** 12-April-2021
    */
    public function faqview() {
        $data['faqs_list'] = $this->common_model->common_multipleSelectWithoutOrder('tbl_faq', array('is_deleted'=>'0','status'=>'Active'));
        $this->load->view($this->viewfolder.'faqview',$data);
        // $this->load->view($this->viewfolder.'faq',$data);
    }

    function forgrtall() {
        $data['name']    = "test";
        $data['message'] = "test";
        $this->load->view('template/contactus_reply',$data);
    }

    /*
    ** Function to load forgot password page for User/Driver
    */
    function forgotpassword($usertype,$user_id) {

        if(!empty($this->uri->segment(3)) && !empty($this->uri->segment(4))) {
            $user_type      = $this->uri->segment(3);
            $user_id        = base64_decode($this->uri->segment(4));            
            $result         = $this->common_model->common_singleSelect('tbl_user',array("id"=>$user_id,'is_deleted'=>'0'));
            
            // if user details not found based on ID
            if(!empty($result)) {
                // echo "<pre>"; print_r($result);
                // echo "<pre>"; print_r($result['forgotpassword_datetime']);
                // echo "<pre>"; print_r(date("Y-m-d H:i:s",strtotime('-1 days'))); 
                if($result['forgotpassword_datetime'] >= date("Y-m-d H:i:s",strtotime('-1 days')) && $result['forgotpassword_token'] != "") {
                    
                    $data = array(
                        'user_id'   => $user_id,
                    );
                    // echo "<pre>"; print_r($data);die;
                    $this->load->view('template/forgot_password',$data);
                } else {
                    // echo "else statement"; die;
                    $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_passwordreset_linkexpired'));
                    redirect('failure');
                }
            } else {
                $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_detailsnotfound'));
                redirect('failure');
            }
        } else {
            $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_somethingwent_wrong'));
            redirect('failure');
        }
    }

    /*
    ** Function to check validations and update password
    */
    function changepassword(){

        $data = array();
        if (!empty($this->input->post())) {
            
            $this->form_validation->set_rules('user_id','User ID','required|trim');
            $this->form_validation->set_rules('password',"Password",'required|trim');
            $this->form_validation->set_rules('confirmpassword','Confirm Password','required|trim|matches[password]');
            if($this->form_validation->run()) {

                $result = $this->common_model->common_singleSelect('tbl_user',array("id"=>$this->input->post('user_id'),'is_deleted'=>'0'));
              
                // if user details not found based on ID
                if(!empty($result)) {
                    //echo "<pre>"; print_r($result); die;     
                    if($result['forgotpassword_datetime'] >= date("Y-m-d H:i:s",strtotime('-1 days')) && $result['forgotpassword_token'] != "") {
                        if ($this->common_model->decrypt_password($result['password']) != $this->input->post('password')) {

                            // Encrypt the new password for updattion
                            $password_upd = $this->common_model->encrypt_password($this->input->post('password'));

                          
                            $this->common_model->common_singleUpdate('tbl_user',array('password'=>$password_upd,'forgotpassword_token'=>''),array("id"=>$result['id']));    
                            
                          
                            $this->session->set_flashdata('success_msg', $this->lang->line('rest_keywords_passwordchange_success'));
                            redirect('success');
    
                        } else {
                            $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_cantreuse_oldpassword'));
                            redirect('failure');     
                        }
                    } else {
                        $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_passwordreset_linkexpired'));
                        redirect('failure');
                    }
                } else {
                    $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_detailsnotfound'));
                    redirect('failure');
                }

            } else {
                $data['user_id'] = $this->input->post('user_id');
                $this->load->view('template/forgot_password',$data);
            }
        } else {
            $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_passwordreset_linkexpired'));
            redirect('failure');
        }
    }

    /*
    ** Error page for whole project
    */
    function failure(){
        $this->load->view('template/successfailure');
    }

    /*
    ** Success page for whole project
    */
    function success(){
        $this->load->view('template/successfailure');
    }

    /*
    ** Success page for whole project
    */
    function share_url(){
        $this->load->view('page/appredirect');
    }
}
?>