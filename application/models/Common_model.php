<?php
 error_reporting(0);

use Aws\S3\S3Client;
use Twilio\Rest\Client;
require 'vendor/autoload.php';

class Common_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->s3  = S3Client::factory([
                'region'            => 'us-east-1',
                'version'           => '2006-03-01',
                'signature_version' => 'v4',
                'credentials' => [
                    'key'    => S3_BUCKET_KEY,
                    'secret' => S3_BUCKET_SECRET,
                ]
        ]);
          $this->twilio = new Client(TWILLIO_ACCOUNT_SID,TWILLIO_ACCOUNT_AUTH);
        $this->image_valid_formats = array('jpg', 'png', 'gif', 'bmp', 'jpeg','mov','mpeg','PNG', 'JPG', 'JPEG', 'GIF', 'BMP','mp4','MP4','MOV',"MPEG");
    }
    
    /*
    ** Latest Password encryption 
    */
    public function encrypt_password($password) {

        $secret = hash('sha256', KEY_256);  //must be 32 char length
        $encrypt_value = openssl_encrypt($password, "AES-256-CBC", $secret, 0, IV);
        return $encrypt_value;
    }

    /*
    ** Latest Password decryption 
    */
    public function decrypt_password($password) {
        $secret = hash('sha256', KEY_256); 
         //must be 32 char length
        $decrypt_value = openssl_decrypt($password, "AES-256-CBC", $secret, 0, IV);
        return $decrypt_value;
    }

    /*
    ** Send Email
    */
    public function send_mail($to,$from,$message,$subject) {
        // echo "<pre>";print_r($to);
        // echo "<pre>";print_r($from);
        // echo "<pre>";print_r($message);
        //  echo "<pre>";print_r($subject);die;
        // echo "SEND";
        $this->load->library('email');
        $config['protocol']     = 'smtp' ;
        $config['smtp_host']    = 'smtp.gmail.com';
        $config['smtp_port']    = '587';
        $config['smtp_user']    = SMTP_USER;
        $config['smtp_pass']    = SMTP_PASS;
        $config['charset']      = 'utf-8';
        $config['newline']      = "\r\n";
        $config['mailtype']     = 'html'; // or html
        $config['starttls']     = TRUE; // bool whether to validate email or not      
        $config['smtp_crypto']  = 'tls';
// $config = array(
//             'protocol'  => 'smtp',
//             'smtp_host' => 'ssl0.ovh.net',
//             'smtp_port' => 587,
//             'smtp_user' => EMAIL_ID,
//             'smtp_pass' => EMAIL_PASSWORD,
//             'mailtype'  => 'html',
//             'charset'   => 'utf-8',
//             "newline"   => "\r\n",
//             'starttls'  => true,
//             'smtp_crypto'=>'tls'
//         );
        $this->email->set_crlf("\r\n");
        $this->email->initialize($config);
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->set_header('X-Priority', '1');
        if ($this->email->send()) {
            return true;    
        } else {
            echo "<pre>";print_r("<b>Sorry for this, we are testing mail service now..</b><br>");
            echo $this->email->print_debugger();die;
            return false;
        }
    }

    /*
    ** Function to update device information of users
    */
    public function save_user_deviceinfo($user_id,$role,$param) {

        $extrainfo=$this->db->get_where('tbl_user_deviceinfo',array('user_id'=>$user_id,'user_type'=>$role))->row_array();
        if(!empty($extrainfo)) {
            $this->db->where(array('user_id'=>$user_id,'user_type'=>$role))->update('tbl_user_deviceinfo',$param);
        } else {
            $param['user_id']=$user_id;
            $param['user_type']=$role;
            $this->db->insert('tbl_user_deviceinfo',$param);
        }
    }

    /*
    ** Function to upload thumb image in s3 bucket
    */
    public function video_thumb_upload_S3($image_name, $path)
    {
        try 
        {
            $this->s3->putObject([
                'Bucket'    => S3_BUCKET_NAME,
                'Key'       => $path.$image_name,
                'SourceFile' => 'assets/media/'.$image_name,
                'ServerSideEncryption' => 'AES256',
                'ACL'       => 'public-read'
            ]);
            return $image_name;            
        } 
        catch (S3Exception $e) 
        {
            return false;
        }
    }

    /*
    ** Get Menu set
    */
    public function get_menu($menu)
    {
        
        if ($menu == $this->uri->segment(2)) {
          $menuname = "active";
        } else {
           $menuname = "has-submenu";
        }
        return  $menuname;
    } 

    /*
    ** Get Menu set
    */
    public function get_submenu($menu)
    {
        $menuname= "";
        $filename = $this->uri->segment(4);
        $tmp = stripos($filename, $menu);
        if ($tmp  === 0) {
          $menuname = "active";
        } else {
           $menuname = "has-submenu";
        }
        return  $menuname;
    } 
 
    /*
    ** Get Menu set
    */
    public function get_submenu_list($menu)
    {
        $menuname= "";
        $filename = $this->uri->segment(3);
        //$tmp = stripos($filename, $menu);
        if (in_array($filename,$menu)) {
          $menuname = "active";
        } else {
           $menuname = "has-submenu";
        }
        return  $menuname;
    }
  
    /*
    ** Function to send notification
    */
    function send_notification($push_param){
          // echo "<pre>"; print_r($push_param); die;
        $setting = $this->common_singleSelect('tbl_user_notification_setting', array('user_id'=>$push_param['receiver_id']));

        // if($setting['push_notification'] == 'yes' ){
            $secret = hash('sha256', KEY_256);
            $params_json_encoded = openssl_encrypt(json_encode($push_param), "AES-256-CBC", $secret, 0, IV);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                 CURLOPT_URL => 'http://52.7.79.220:8070/api/v1/user/sendPushNotification',
                // CURLOPT_URL => 'localhost:8070/api/v1/user/sendPushNotification',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $params_json_encoded,
                CURLOPT_HTTPHEADER => array(
                    'api-key: GnlP5gqbL9whTvTFYoPokA==',
                    'Content-Type: text/plain'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
        // }
     
    }

function create_token($push_param){
        $secret = hash('sha256', KEY_256);
            $params_json_encoded = openssl_encrypt(json_encode($push_param), "AES-256-CBC", $secret, 0, IV);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                 CURLOPT_URL => 'http://52.7.79.220:8070/token',
                // CURLOPT_URL => 'localhost:8070/api/v1/user/sendPushNotification',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $params_json_encoded,
                CURLOPT_HTTPHEADER => array(
                    'api-key: GnlP5gqbL9whTvTFYoPokA==',
                    'Content-Type: text/plain'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        // }
     
    }


    function Python_api($push_param){
        // print_r($push_param);
        // die;
          $secret = hash('sha256', KEY_256);
          $curl = curl_init();
            // $params_json_encoded = json_encode($push_param);
          curl_setopt_array($curl, array(
               CURLOPT_URL => 'https://9e98-122-170-108-112.in.ngrok.io/api/v1/get_ratting/',
              // CURLOPT_URL => 'localhost:8070/api/v1/user/sendPushNotification',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS => $push_param,
              CURLOPT_HTTPHEADER => array(
                  'api-key: SUASAPP08022022',
                  'Accept-Language:en'
              ),
          ));
          $response = curl_exec($curl);
          curl_close($curl);
          print_r($response);
          print_r($curl);
          return json_decode($response);
          // die;
      // }
   
  }


    /*
    ** Get Menu set
    */
    public function get_submenu_list_data($menu)
    {
        $menuname= "";
        $filename = $this->uri->segment(4);
        //$tmp = stripos($filename, $menu);
        if (in_array($filename,$menu)) {
          $menuname = "active";
        } else {
           $menuname = "has-submenu";
        }
        return  $menuname;
    }
      
    /*
    ** Common function to insert single record 
    */
    public function common_insert($tablename,$params)
    {
        $this->db->insert($tablename,$params);
        return $this->db->insert_id();
    }

    /*
    ** common function to insert batch records
    */
    public function common_insertbatch($tablename,$params)
    {
        $this->db->insert_batch($tablename, $params);
        return $this->db->insert_id();
    }

    /*
    ** Common function to select raw array
    */
    public function common_singleSelect($tablename,$where)
    {
        return $this->db->order_by('id','DESC')->get_where($tablename,$where)->row_array();
    }
    
    /*
    ** Common function to select result array
    */
    public function common_multipleSelect($tablename,$where)
    {
        return $this->db->order_by('id','DESC')->get_where($tablename,$where)->result_array();
    }
    /*
    ** Common function to select result array
    */
    public function common_multipleSelect_order($tablename,$where,$id,$asc)
    {
        return $this->db->order_by($id,$asc)->get_where($tablename,$where)->result_array();
    }

    /*
    ** Function for common delete
    ** 08-Feb-2022
    */
    public function common_delete($tablename,$where)
    {
        $this->db->where($where);
        $this->db->delete($tablename); 
    }
    
    /*
    ** Common function to update 
    */
    public function common_singleUpdate($tablename,$params,$where)
    {
        $this->db->update($tablename,$params,$where);
        return $this->db->affected_rows();
    }

    /*
    ** Date convert on specific timezone. 
    */ 
    public function date_convert($date,$dateformat,$timezone=NULL) {
        // echo $timezone;die;
        $timezone=!empty($timezone) ? $timezone : 'UTC'; 
        if($date == '0000-00-00 00:00:00') 
            return $date;
             
        $date = new DateTime($date); 
        $date->setTimezone(new DateTimeZone($timezone)); 
        return $date->format($dateformat); 
        
    }

    /*
    ** Function to get any time in UTC format
    */
    public function convertTimetoUTC($datetime,$dateformat,$defaulttimezone) {
        if ($datetime == '0000-00-00 00:00:00') {
            return $datetime;
        } else {
            date_default_timezone_set($defaulttimezone);
            $conversiondate = strtotime($datetime);
            date_default_timezone_set("UTC");
            $finaldate = date($dateformat,$conversiondate);
            return $finaldate;
        }
    }  

    /*
    ** Function to upload image in S3 bucket
    */
    public function uploadImageS3($files,$uploadPath) {
        
        $tmp_file = explode('.',$files['name']);
        $file_ext = end($tmp_file);
        // echo "p1";
        // print_r($this->image_valid_formats);
        // print_r($file_ext);
        if(in_array($file_ext,$this->image_valid_formats)) {
            // echo "p2";
            if (!empty($files['name'])) {
                $imageName = random_string('numeric', 5).strtotime(date("Ymd his")).".".$file_ext;
                // echo "p3";
                try {

                    $this->s3->putObject([
                        'Bucket'        => S3_BUCKET_NAME,
                        'Key'           => $uploadPath.$imageName,
                        'SourceFile'    => $files['tmp_name'],
                        'ServerSideEncryption' => 'AES256',
                        'ACL'           => 'public-read'
                    ]);
                    // echo "p4";
                    return $imageName;
                    
                } catch (S3Exception $e) {
                    return false;
                }

            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /*
    ** It will upload readed file to s3 bucket
    */
    public function directImageUploadS3($sourcepath,$uploadPath){

        $basename = basename($sourcepath);
        $fileextention = explode('.',$basename);
        if (!empty($fileextention[1])) {
            $imageName = uniqid().strtotime(date("Ymd his")).$fileextention[1];    
        } else {
            $imageName = uniqid().strtotime(date("Ymd his"))."png";    
        }
        try {
            
            $this->s3->putObject([
                'Bucket'        => S3_BUCKET_NAME,
                'Key'           => $uploadPath.$imageName,
                'SourceFile'    => $sourcepath,
                'ServerSideEncryption' => 'AES256',
                'ACL'           => 'public-read',
                ]);
            return $imageName;

        } catch (S3Exception $e) {
            return false;
        }
    }

    /*
    ** Function to upload multiple image in s3 bucket
    */
    public function s3BucketMultipleImage($field,$i,$uploadPath) {

        $tmp_file = explode('.', $_FILES[$field]['name'][$i]);

        $file_ext = end($tmp_file);
        if (in_array($file_ext,$this->image_valid_formats)) {
            $imageName = uniqid().strtotime(date("Ymd his")).".".$file_ext;
            try {
                
                $this->s3->putObject([
                    'Bucket'        => S3_BUCKET_NAME,
                    'Key'           => $uploadPath.$imageName,
                    'SourceFile'    => $_FILES[$field]['tmp_name'][$i],
                    'ServerSideEncryption' => 'AES256',
                    'ACL'           => 'public-read',
                    ]);
                return $imageName;

            } catch (S3Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
    ** function to delete image from s3 bucket
    */
    public function s3_delete_image($image_path) {
        $result = $this->s3->deleteObject(
            array(
                'Bucket'    => S3_BUCKET_NAME, 
                'Key'       => $image_path
            )
        );
        return $result;
    }

    /*
    ** function to get country code list
    */
    public function get_country_code(){
        $this->db->select("*");
        $this->db->from('tbl_master_countries');
        $this->db->where('is_active','1');
        $this->db->where('is_deleted','0');
        $this->db->order_by('dial_code','ASC');
        $query = $this->db->get();        
        return $query->result_array();
    }
    public function get_country_code_new(){
        $this->db->select("*");
        $this->db->from('tbl_country');
        $this->db->where('is_active','1');
        $this->db->where('is_deleted','0');
        $this->db->order_by('calling_code','ASC');
        $query = $this->db->get();        
        return $query->result_array();
    }
    /*
    ** Function to get all Users
    */
    function getUserList(){
        $this->db->select("tu.*, tui.device_token, tui.device_type, tu.username as username");
        $this->db->join('tbl_user_deviceinfo tui','tu.id = tui.user_id', 'left');
        $this->db->from('tbl_user tu');
        $this->db->where('tu.status','Active');
        $this->db->where('tu.is_deleted','0');
        $this->db->group_by('tu.id');
        $query = $this->db->get();        
        return $query->result_array();
    }
    function get_contest_list($type,$item_id){
        $this->db->select("u.*");
        $this->db->from("tbl_order o");
        $this->db->join("tbl_order_detail od","o.id = od.order_id");
        $this->db->join("tbl_user u","o.user_id = u.id");
        $this->db->where("od.item_type",$type);
        $this->db->where("od.item_id",$item_id);
        $this->db->where("od.is_deleted","0");
        $this->db->where("o.is_deleted","0");
        $this->db->where("u.is_deleted","0");
         $query = $this->db->get();        
        return $query->result_array();
    }

    /**
     * Function to get the currency rate
     */
    function get_currency_rate($name){
        $this->db->select("c.*,cr.rate");
        $this->db->from("tbl_country c");
        $this->db->join("tbl_currency_rate cr","c.currency_code = cr.to_currency_code");
        $this->db->where("c.name",$name);
          $query = $this->db->get();        
        return $query->result_array();
    }
    function getUserDetails($user_id){
        $this->db->select("tu.*, tui.device_token, tui.device_type, tu.username as username");
        $this->db->from('tbl_user tu');
        $this->db->join('tbl_user_deviceinfo tui','tu.id = tui.user_id', 'left');
        $this->db->where('tu.status','Active');
        $this->db->where('tui.user_type','U');
        $this->db->where('tu.is_deleted','0');
        $this->db->where('tu.id',$user_id);
        $query = $this->db->get();        
        return $query->row_array();
    }

    function get_selected_module($subadmin_id){
        $this->db->select('tc.*,(CASE WHEN tm.id IS NULL THEN "0" ELSE "1" END) as is_select')->from('tbl_subadmin_modules tc');
        $this->db->join('tbl_subadmin_access_modules tm', 'tc.id=tm.module_id AND tm.subadmin_id='.$subadmin_id,'left');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tc.status','Active');
        return $this->db->get()->result_array();
    }

    function get_agora_token($class_id, $subadmin_id, $type){
        $param = array('class_id'=>$class_id, 'user_id'=>$subadmin_id, 'type'=>$type);
        
        $secret = hash('sha256', KEY_256);
        $params_json_encoded = openssl_encrypt(json_encode($param), "AES-256-CBC", $secret, 0, IV);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://52.7.79.220:8070/api/v1/user/generate_token_agora',
            // CURLOPT_URL => 'http://localhost:8070/api/v1/user/generate_token_agora',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params_json_encoded,
            CURLOPT_HTTPHEADER => array(
                'api-key: GnlP5gqbL9whTvTFYoPokA==',
                'Content-Type: text/plain'
            ),
        ));
        $response = curl_exec($curl);
        $secret = hash('sha256', KEY_256); 
         //must be 32 char length
        $decrypt_value = openssl_decrypt($response, "AES-256-CBC", $secret, 0, IV);
         // echo "<pre>"; print_r($decrypt_value);
        $result = json_decode($decrypt_value, true);
          // echo "<pre>"; print_r($result);die;
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return $err;
        } else {
          return $result;
        }
    }

   public function sendSMS($phone,$message)
    {
        // echo "sms";
        // print_r($phone);
        // print_r($message);
        // die;
        if(!empty($phone) && !empty($message)){
            try{
                $message = $this->twilio->messages->create($phone,["body" => $message,"from" => TWILLIO_ACCOUNT_PHONE]);
                // echo "<pre>";
                // print_r($message);
                // die;
                if(!empty($message->sid)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } catch(Exception $e) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }


    function get_contestlist(){
        $this->db->select("tc.*, TIMEDIFF(tc.contest_enddate, now()) as remaining_time");
        $this->db->from('tbl_master_contest tc');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');
        $this->db->group_by('tc.id');
        $this->db->order_by('tc.id','DESC');
        $result =  $this->db->get()->result_array();
        if(!empty($result)){
            $sponsors = array();
            foreach ($result as $key => $value) {
                $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");
                // $this->db->from('tbl_sponsor ts');
                   $this->db->from('tbl_master_contest tc');
                $this->db->join('tbl_sponsor ts','ts.id = tc.sponsor_id','left');
                $this->db->where('ts.status','Active');
                $this->db->where('ts.is_deleted','0');
                $this->db->where('tc.id', $value['id']);
                $sponsor = $this->db->get()->row_array();
                $result[$key]['sponsors']=!empty($sponsor) ? $sponsor : array();
            } 
        }
        return $result;
    }

 public function common_multipleSelectWithoutOrder($tablename,$where)
    {
        return $this->db->get_where($tablename,$where)->result_array();
    }

   public function get_order_details($order_type, $item_id, $user_id)
    {
        $this->db->select("to.*,tod.item_type,tod.item_id,tod.price,tod.is_block_contest,tod.is_block_classess,tod.is_mute_contest,tod.is_video_contest,tod.is_mute_classess,tod.is_video_classess");
        $this->db->from("tbl_order to");
        $this->db->join("tbl_order_detail tod", "to.id = tod.order_id", "left");
        $this->db->where(array("to.user_id" => $user_id, "tod.item_type" => $order_type, "tod.item_id" => $item_id, "tod.is_deleted" => "0"));
        $result =  $this->db->get()->row_array();
        return $result;
    }

    public function get_tax_details($user_id){
        $this->db->select("c.*,r.rate");
        $this->db->from("tbl_country  c");
        $this->db->join("tbl_user u","c.name = u.country");
        $this->db->join("tbl_currency_rate r","c.currency_code = r.to_currency_code");
        $this->db->where("u.id",$user_id);
         $result =  $this->db->get()->row_array();
        return $result;
    }

    public function tax_details($user_id){
        $this->db->select("td.*");
        $this->db->from("tbl_user u");
        $this->db->join("tbl_tax_details td","u.region_id = td.id");
        $this->db->where("u.id",$user_id);
        $result =  $this->db->get()->row_array();
        return $result;
    }
}
?>