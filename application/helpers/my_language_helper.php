<?php

defined('BASEPATH') OR exit('No direct script access allowed');


if ( ! function_exists('lang'))
{
	function my_lang($line, $vars = array())
	{
	    $CI =& get_instance();
	    $line = $CI->lang->line($line);

	    if ($vars)
	    {
	        $line = vsprintf($line, (array) $vars);
	    }

	    return $line;
	}
}
