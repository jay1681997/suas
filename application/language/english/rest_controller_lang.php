<?php

$lang['rest_keywords_somethingwent_wrong']                         = "Something went wrong, please try again";
$lang['rest_keywords_detailsnotfound']                             = "Something went wrong, login details are missing";
$lang['rest_keywords_passwordreset_linkexpired']                   = "Sorry! Your password reset link is expired now, please request again";
$lang['rest_keywords_cantreuse_oldpassword']                       = "Your new password must be different from previous password.";
$lang['rest_keywords_passwordchange_success']                      = "Your password has successfully been reset";

//Admin panel messages
$lang['adminpanel_message_imageupload_failed']                     = "Something went wrong uploading your image, please try again";
$lang['adminpanel_message_invalid_email']                          = "Please enter valid email address";
$lang['adminpanel_message_invalid_password']                       = "Please enter valid password";
$lang['adminpanel_productimagedelete_success']                     = "Images has been deleted successfully";
$lang['adminpanel_profile_update_success']                         = "Admin profile has been updated successfully";

// FAQ module messages 
$lang['adminpanel_message_faqadd_success']                         = "FAQ details added successfully";
$lang['adminpanel_message_faq_update_success']                     = "FAQ details updated successfully";
$lang['adminpanel_message_faq_Active_success']                     = "FAQ activated successfully";
$lang['adminpanel_message_faq_Inactive_success']                   = "FAQ deactivated successfully";
$lang['adminpanel_message_faqdelete_success']                      = "FAQ removed successfully";

// Contact Us Modules
$lang['adminpanel_message_contactus_repliedsuccess']               = "Contact us replied successfully";
$lang['adminpanel_message_replycontacts_error']                    = "Something went wrong, Failed to sent email";
$lang['adminpanel_message_deletecontacts_succcess']                = "Contact us details removed successfully";
$lang['adminpanel_message_contacts_Active_success']                = "Contact us activated successfully";
$lang['adminpanel_message_contacts_Inactive_success']              = "Contact us deactivated successfully";

// User module messages 
$lang['adminpanel_message_customer_Active_success']                = "Customer account activated successfully";
$lang['adminpanel_message_customer_Inactive_success']              = "Customer account deactivated successfully";
$lang['adminpanel_message_customer_delete_success']                = "Customer removed successfully";
$lang['adminpanel_message_customer_add_success']                   = "Customer details added successfully";
$lang['adminpanel_message_customer_update_success']                = "Customer details updated successfully";
$lang['adminpanel_message_customer_restore_success']               = "Customer details restore successfully";

// Contest module messages 
$lang['adminpanel_message_contest_Active_success']                = "Contest activated successfully";
$lang['adminpanel_message_contest_Inactive_success']              = "Contest deactivated successfully";
$lang['adminpanel_message_contest_delete_success']                = "Contest removed successfully";
$lang['adminpanel_message_contest_add_success']                   = "Contest details added successfully";
$lang['adminpanel_message_contest_update_success']                = "Contest details updated successfully";

// Class module messages 
$lang['adminpanel_message_class_Active_success']                = "Class activated successfully";
$lang['adminpanel_message_class_Inactive_success']              = "Class deactivated successfully";
$lang['adminpanel_message_class_delete_success']                = "Class removed successfully";
$lang['adminpanel_message_class_add_success']                   = "Class details added successfully";
$lang['adminpanel_message_class_update_success']                = "Class details updated successfully";
$lang['adminpanel_message_already_book_success']                = "This Class is already book from your side";

// Prize module messages 
$lang['adminpanel_message_prize_Active_success']                  = "Prize activated successfully";
$lang['adminpanel_message_prize_Inactive_success']                = "Prize deactivated successfully";
$lang['adminpanel_message_prize_delete_success']                  = "Prize removed successfully";
$lang['adminpanel_message_prize_add_success']                     = "Prize details added successfully";
$lang['adminpanel_message_prize_update_success']                  = "Prize details updated successfully";

// Sponsor module messages 
$lang['adminpanel_message_sponsor_Active_success']                  = "Sponsor activated successfully";
$lang['adminpanel_message_sponsor_Inactive_success']                = "Sponsor deactivated successfully";
$lang['adminpanel_message_sponsor_delete_success']                  = "Sponsor removed successfully";
$lang['adminpanel_message_sponsor_add_success']                     = "Sponsor details added successfully";
$lang['adminpanel_message_sponsor_update_success']                  = "Sponsor details updated successfully";

// Promocode module messages 
$lang['adminpanel_message_promocode_Active_success']                  = "Promocode activated successfully";
$lang['adminpanel_message_promocode_Inactive_success']                = "Promocode deactivated successfully";
$lang['adminpanel_message_promocode_delete_success']                  = "Promocode removed successfully";
$lang['adminpanel_message_promocode_add_success']                     = "Promocode details added successfully";
$lang['adminpanel_message_promocode_update_success']                  = "Promocode details updated successfully";

// Video Library module messages 
$lang['adminpanel_message_videolibrary_Active_success']                  = "Video library activated successfully";
$lang['adminpanel_message_videolibrary_Inactive_success']                = "Video library deactivated successfully";
$lang['adminpanel_message_videolibrary_delete_success']                  = "Video library removed successfully";
$lang['adminpanel_message_videolibrary_add_success']                     = "Video library details added successfully";
$lang['adminpanel_message_videolibrary_update_success']                  = "Video library details updated successfully";

// Scorecard module messages 
$lang['adminpanel_message_scorecard_Active_success']                  = "Scorecard activated successfully";
$lang['adminpanel_message_scorecard_Inactive_success']                = "Scorecard deactivated successfully";
$lang['adminpanel_message_scorecard_delete_success']                  = "Scorecard removed successfully";
$lang['adminpanel_message_scorecard_add_success']                     = "Scorecard details added successfully";
$lang['adminpanel_message_scorecard_update_success']                  = "Scorecard details updated successfully";

// Evaluation module messages 
$lang['adminpanel_message_evaluation_Active_success']                  = "Evaluation factor activated successfully";
$lang['adminpanel_message_evaluation_Inactive_success']                = "Evaluation factor deactivated successfully";
$lang['adminpanel_message_evaluation_delete_success']                  = "Evaluation factor removed successfully";
$lang['adminpanel_message_evaluation_add_success']                     = "Evaluation factor details added successfully";
$lang['adminpanel_message_evaluation_update_success']                  = "Evaluation factor details updated successfully";

// Subscription module messages 
$lang['adminpanel_message_subscription_Active_success']                  = "Subscription activated successfully";
$lang['adminpanel_message_subscription_Inactive_success']                = "Subscription deactivated successfully";
$lang['adminpanel_message_subscription_delete_success']                  = "Subscription removed successfully";
$lang['adminpanel_message_subscription_add_success']                     = "Subscription details added successfully";
$lang['adminpanel_message_subscription_update_success']                  = "Subscription details updated successfully";

// Notification module messages 
$lang['adminpanel_message_send_push_success']                      = "Notification sent successfully";

// Setting module messages 
$lang['adminpanel_message_setting_Active_success']                  = "Setting activated successfully";
$lang['adminpanel_message_setting_Inactive_success']                = "Setting deactivated successfully";
$lang['adminpanel_message_setting_delete_success']                  = "Setting removed successfully";
$lang['adminpanel_message_setting_add_success']                     = "Setting details added successfully";
$lang['adminpanel_message_setting_update_success']                  = "Setting details updated successfully";

// Speechevaluation module messages 
$lang['adminpanel_message_speechevaluation_Active_success']                  = "Speech evaluation activated successfully";
$lang['adminpanel_message_speechevaluation_Inactive_success']                = "Speech evaluation deactivated successfully";
$lang['adminpanel_message_speechevaluation_delete_success']                  = "Speech evaluation removed successfully";
$lang['adminpanel_message_speechevaluation_add_success']                     = "Speech evaluation details added successfully";
$lang['adminpanel_message_speechevaluation_update_success']                  = "Speech evaluation details updated successfully";

// User module messages 
$lang['adminpanel_message_student_Active_success']                = "Student account activated successfully";
$lang['adminpanel_message_student_Inactive_success']              = "Student account deactivated successfully";
$lang['adminpanel_message_student_delete_success']                = "Student removed successfully";
$lang['adminpanel_message_student_add_success']                   = "Student details added successfully";
$lang['adminpanel_message_student_update_success']                = "Student details updated successfully";


// Suggested Courses module messages 
$lang['adminpanel_message_suggestedcourse_Active_success']                  = "Suggested Courses activated successfully";
$lang['adminpanel_message_suggestedcourse_Inactive_success']                = "Suggested Courses deactivated successfully";
$lang['adminpanel_message_suggestedcourse_delete_success']                  = "Suggested Courses removed successfully";
$lang['adminpanel_message_suggestedcourse_add_success']                     = "Suggested Courses details added successfully";
$lang['adminpanel_message_suggestedcourse_update_success']                  = "Suggested Courses details updated successfully";

// Leaderboard module messages 
$lang['adminpanel_message_leaderboard_Active_success']                  = "Leaderboard activated successfully";
$lang['adminpanel_message_leaderboard_Inactive_success']                = "Leaderboard deactivated successfully";
$lang['adminpanel_message_leaderboard_delete_success']                  = "Leaderboard removed successfully";
$lang['adminpanel_message_leaderboard_add_success']                     = "Leaderboard details added successfully";
$lang['adminpanel_message_leaderboard_update_success']                  = "Leaderboard details updated successfully";

// Sub Admin module messages 
$lang['adminpanel_message_subadmin_Active_success']                = "Sub Admin activated successfully";
$lang['adminpanel_message_subadmin_Inactive_success']              = "Sub Admin deactivated successfully";
$lang['adminpanel_message_subadmin_delete_success']                = "Sub Admin removed successfully";
$lang['adminpanel_message_subadmin_add_success']                   = "Sub Admin details added successfully";
$lang['adminpanel_message_subadmin_update_success']                = "Sub Admin details updated successfully";

// Banner module messages 
$lang['adminpanel_message_banner_Active_success']                = "Banner activated successfully";
$lang['adminpanel_message_banner_Inactive_success']              = "Banner deactivated successfully";
$lang['adminpanel_message_banner_delete_success']                = "Banner removed successfully";
$lang['adminpanel_message_banner_add_success']                   = "Banner details added successfully";
$lang['adminpanel_message_banner_update_success']                = "Banner details updated successfully";



//WEBSITE
$lang['website_customer_add_success'] 				= "Signup successfully";
$lang['website_customer_details_not_found'] 		= "User details not found";
$lang['website_otpalreadyverify_error'] 			= "This verification code already verified, please check again";
$lang['website_sendotp_success'] 					= "Verification code is sent to your registered mobile number";
$lang['website_sentotp_error'] 						= "Verification code is invalid";
$lang['website_otp_send_success'] 					= "Verification code is sent to your register mobile number";
$lang['website_otpcodeverify_sucess'] 				= "Verification done";
$lang['website_otpcodeverify_otp_failed']           = "Otp code is not send to your register mobile number please check your mobile number";
$lang['website_create_password_success'] 			= "Your password has been successfully changed";
$lang['website_oldnew_password_same'] 				= "Your new password must be different from previous password.";
$lang['website_login_country_code_phone_error'] 	= "Country code and phone number you have provided does not exist";
$lang['website_loginpassword_error'] 				= "Please enter valid password";
$lang['website_logindisactive_error'] 				= "Your account is deactivated by admin, Please contact admin to reactive it again";
$lang['website_forgotphone_success'] 				= "Verification code has been sent successfully to this phone number";
$lang['website_forgotemail_success'] 				= "A password reset link has been sent to the email associated with your account. Please open the link and create a new password";
$lang['website_sentmail_error'] 					= "Failed to sent forgot password mail, please try again.";
$lang['website_forgotphone_error'] 					= "Country code and phone number you have provided does not exist";
$lang['website_forgotemail_error'] 					= "Email address that you have entered is not in our records";
$lang['website_sentmail_success'] 					= "Your note was sent successfully and the team will get back to you soon";
$lang['website_sentmail_error'] 					= "Sorry! Something went wrong, Failed to sented email";
$lang['website_profile_updated_success'] 			= "Profile has been updated successfully";
$lang['website_changepassword_success'] 			= "Your password has been successfully changed";
$lang['website_changepassword_error'] 			    = "Old password is wrong, please try again.";
$lang['website_upload_video_success'] 			    = "Video has been uploaded successfully";
$lang['website_student_added_success'] 			    = "Student added successfully";
$lang['website_student_limit_reached']              = "You can add maximum 5 students to your profile";
$lang['website_student_updated_success'] 			= "Student has been updated successfully";
$lang['website_card_added_success'] 			    = "Your payment method has been saved";
$lang['website_speech_uploaded_success'] 			= "Your speech successfully uploaded for evaluation";
$lang['website_item_save_inmy_wishlist'] 			= "Item saved in my wish list";
$lang['website_item_delete_from_cart'] 			    = "Item removed from cart successfully";
$lang['website_card_token_not_created'] 			= "Something went wrong on creation of card token";
$lang['website_card_already_exists'] 			    = "Card details already exist";
$lang['website_card_customer_not_created'] 			= "Something went wrong on creation of card customer";
$lang['website_speech_uploaded_failed'] 			= "Your speech upload failed";
$lang['website_email_not_exist'] 			        = "This email address does not exist or no user has registered using this email address.";
$lang['website_age_category_is_different'] 			= "The age groups are not the same.";
$lang['website_gift_send_success'] 					= "Gift sent successfully";
$lang['website_student_upgrade_to_main_profile'] 	= "Student Upgrade to main profile successfully";

$lang['website_notification_updated_success'] 			= "Notification Setting has been updated successfully";
$lang['rest_keywords_edit_setting_details'] = "Edit Setting Details Successfully";
$lang['website_subscription_already_exists'] = "You have already one subscription";
$lang['rest_keywords_item_exist'] = "Item is already exist in the cart";
$lang['rest_keywords_promocode_not_find'] = "Promocode details not find successfully";
$lang['rest_keywords_promocode_find']      = "Promocode applied successfully";
$lang['rest_keywords_already_book'] = "You have already book";
$lang['rest_keywords_subscriptions_already_exist'] = "You have already one subscriptions";

$lang['rest_keywords_contest_video_upload_success'] ="Contest video uploaded successfully";
$lang['rest_keywords_contest_video_upload_failed'] = "Contest video not uploaded successfully.please try again";
$lang['rest_keywords_speechevalution_remove_success'] = "Speech evaluation removed successfully";

$lang['rest_keywords_card_number_not_vaild'] = "Your card number is incorred";
$lang['rest_keywords_eamil_already_exists'] = "email already exists";
$lang['rest_keywords_phone_already_exists'] = "Phone Number already exists";

$lang['adminpanel_message_refund_success'] = "Order amount refund successfully";
$lang["adminpanel_message_refund_error"] = "Something went wrong , Please try again";
$lang["rest_keywords_promocode_purchase"] = "You have to purchase the promocode";
$lang["rest_keywords_promocode_maxusagelimit_over"] = "This promocodes maximum limit exceeded";
$lang["rest_keywords_promocode_already_used"] = "You have already used this promocode";

$lang['text_rest_admincountry_Active_success'] = "Tax activated successfully";
$lang['text_rest_admincountry_Inactive_success'] = "Tax deactivated successfully";
$lang['admin_keywords_country_delete_success'] = "Tax removed successfully";
$lang['admin_keywords_countryupdate_success'] = "Tax Update successfully";
$lang['admin_keywords_add_country'] = "Tax Added successfully";
$lang['admin_keywords_admin_not_host_contest'] = "Admin has not hosted this contest , please wait for the some time";
$lang['admin_keywords_admin_not_host_class'] = "Admin has not hosted this class , please wait for the some time";