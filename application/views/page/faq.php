<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="<?php echo PROJECT_NAME." Admin Portal"; ?>">
    <title><?php echo PROJECT_NAME; ?> - FAQ's</title>
    <link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
    <style>
        .panel-group .panel {
            margin-bottom: -6px;
        }
        /*.input-group-btn .btn {
            margin: 0 0 0px 0;
        }*/
        .form-control {line-height: 2.5;}
        .zmdi-search{top: 3px !important;}
        .panel-group .panel-col-white .panel-title {
            background-color: #fff !important;
            color: #000;
        }

        #search_div {
            /*border-bottom: 1px solid black !important;*/
            position: relative;
            -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;

        }

        #myInput {
            width: 100%;            
            height: 100%;
        }

        .input-group {
            width: 100%;
            margin-bottom: 2px;
        }

        .form-control::placeholder {
            line-height: 2.429;
            background: white;
            font-size: 16px;
        }

        .form-control {
            height: 50px !important;
        }

        .input-group .input-group-btn {
            padding: 0 0px;
        }
        .zmdi-plus-square, .zmdi-minus-square{
            padding-right: 11px;
            padding-left: 3px;
        }
        .cursor_change:hover{
            cursor: pointer;
        }
        .zmdi-minus-square {
          color: #0088c5;
        }
        .zmdi-plus-square {
          color: #0088c5;
        }
    </style>
</head>

<body class="theme-orange">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <!-- <div class="card"> -->
            <div class="body" style = "margin-top: 5px; padding: 5px;">
                <div class="input-group bootstrap-touchspin" id="search_div" style="">
                    <div class="input-group-btn" style="background: white;">
                        <button class="btn btn-default bootstrap-touchspin-down" type="button"
                            style="box-shadow: 0 0px 0px rgba(0, 0, 0, 0), 0 0px 0px rgba(0, 0, 0, 0);">
                            <i class="zmdi zmdi-search" style="font-size: 1.8em;color: #565656;top: 8px;"></i>
                        </button>
                    </div>
                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                    <input id="myInput" value="" style=" padding: 5px;" placeholder="Search your question..." name="demo3" class="form-control" type="text" onkeyup="myFunction()">
                    <!-- <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;">
                    </span> -->
                </div>
                <div class="row clearfix">
                    <div class="col-md-12 col-lg-12">
                        <div class="panel-group" id="accordion_10" role="tablist" aria-multiselectable="false">
                            <?php if(isset($faqs_list) && !empty($faqs_list)) 
                            {foreach ($faqs_list as $key => $value) { ?>
                            <div class="panel panel-col-white" onclick="collpse(<?php echo $key;?>,this);">
                                <div class="panel-heading cursor_change" role="tab" id="headingOne_<?php echo $key;?>">
                                    <h4 class="panel-title"> <a role="button" data-toggle="collapse"
                                            data-parent="#accordion_<?php echo $key;?>"
                                            href="#collapseOne_<?php echo $key;?>" aria-expanded="true"
                                            aria-controls="collapseOne_<?php echo $key;?>">
                                            <i class="zmdi zmdi-plus-square"></i>
                                            <?php echo $value['question']; ?> 
                                        </a> 
                                    </h4>
                                </div>
                                <div id="collapseOne_<?php echo $key;?>" class="panel-collapse collapse in"
                                    role="tabpanel" aria-labelledby="headingOne_<?php echo $key;?>">
                                    <div class="panel-body"> <?php echo $value['answer']; ?>
                                    </div>
                                </div>
                            </div>
                            <?php } } ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- </div> -->
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script>
    <script>
        function collpse(id, ele) {
            $expand = $(ele).find("i");
            if ($expand.hasClass('zmdi zmdi-plus-square')) {
                $expand.removeClass('zmdi zmdi-plus-square').addClass('zmdi zmdi-minus-square');
            } else {
                $expand.removeClass('zmdi zmdi-minus-square').addClass('zmdi zmdi-plus-square');
            }
        }

        function myFunction() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("accordion_10");
            tr = document.getElementsByClassName("panel");
            for (i = 0; i < tr.length; i++) {
                td = tr[i];
                // .getElementsByTagName("td")[0]
                console.log(td)
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        $('input,textarea').focus(function () {
            $(this).data('placeholder', $(this).attr('placeholder'))
                .attr('placeholder', '');
        }).blur(function () {
            $(this).attr('placeholder', $(this).data('placeholder'));
        });
    </script>
</body>
</html>