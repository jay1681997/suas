<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<html> 
<head> 
	<meta http-equiv="content-type" content="text/html; charset=utf-8"> 
	<title><?php echo PROJECT_NAME; ?> - Terms and Condition</title> 
	<meta name="generator" content="LibreOffice 4.2.7.2 (Linux)"> 
	<meta name="created" content="20151002;175753429182121"> 
	<meta name="changed" content="20151002;175818374856723"> 
	<style type="text/css"> 
		@page { margin: 2cm } 
		p { margin-bottom: 0.25cm; line-height: 120% } 
	</style> 
</head> 
<body lang="en-IN" dir="ltr"> 
	<?php 
		if(isset($app_content['terms_and_condition']) && !empty($app_content['terms_and_condition'])) {
			echo $app_content['terms_and_condition'];
		} else {
			echo "No content";
		}
	?>  
</body> 
</html>