<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("inc/style.php"); ?>
</head>
<link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<style type="text/css">
.crn-numbers .select2-container {
    width: 40% !important;
}
.crn-numbers .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 35%;
}

.crn-numbers .select2-container--default .select2-selection--single {
    border-radius: 30px 0 0 30px;
    border-right: none !important;
    border: 2px solid #e6e8eb;
}

.select2-container--open .select2-dropdown {
    left: 0;
    width: 400px !important;
}
</style>
<body>
    <div class="wrapper">
        <?php include("inc/header.php"); ?>
        <section class="about-layout4 pb-0">
            <div class="container">
                <div class="sign-up-pr">
                    <div class="row justify-content-center align-items-center">
                        <div class="col-12 col-lg-6 col-md-6 ">
                            <div class="signin-process-left">
                                <img src="<?php echo base_url(); ?>website_assets/images/login.jpg" alt="login" title="login process">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 col-md-6">
                            <div class="rgt-sign-in py-md-0 py-5">
                                <div class="heading mb-0">
                                    <h2 class="title">Log In</h2>
                                    <p>If you have an account with us, please log in.</p>
                                </div><!-- End .heading -->
                                <?php if ($this->session->flashdata('success_msg')) { ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('success_msg') ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('error_msg')) { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('error_msg') ?>
                                    </div>
                                <?php } ?> <?php if (isset($error_msg) && $error_msg != '') { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $error_msg; ?>
                                    </div>
                                <?php } ?>
                                <?php echo form_open('login', array('method' => 'post')); ?>
                                <div class="form-group">
                                    <!-- <input type="email" class="form-control" placeholder="Email Address/Mobile Number" required> -->
                                    <i class="icon-phone form-group-icon text-success"></i>
                                    <div class="input-group mb-3 mb-md-4 mb-lg-3 d-flex crn-numbers">
                                        <select class="form-select form-control imageselect2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:140px;padding-left:30px">
                                            <!-- <option value="+1">+1</option> -->
                                            <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                            <?php if (!empty($countries)) {
                                                foreach ($countries as $key => $value) { ?>
                                                    <!--  <option value="<?php echo $value['calling_code']; ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['calling_code']; ?></option> -->
                                                    <option value="<?php echo $value['calling_code']; ?>" data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'] . '(' . $value['calling_code'] . ')'; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                        <?php echo form_error('country_code'); ?>
                                        <label id="countrieseerror"></label>

                                        <input type="text" class="form-control phone-format" id="phone" name="phone" aria-required="true" parsley-trigger="change" placeholder="Phone Number" data-parsley-errors-container="#phoneerror" data-parsley-required-message="Please enter phone number" required maxlength="12" data-parsley-minlength="8" data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" value="<?php echo set_value('phone'); ?>">
                                        <?php echo form_error('phone'); ?>
                                    </div>
                                    <div id="phoneerror"></div>
                                </div>
                                <div class="form-group">
                                    <i class="form-group-icon text-success">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z" />
                                        </svg>
                                    </i>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" aria-required="true" data-parsley-trigger="change" data-parsley-required-message="Please enter password" required data-parsley-errors-container="#passwordd">
                                    <span toggle="#password" class="fa fa-eye fa-eye-slash field-icon toggle-password alrt-div" style="top: 44% !important;right: 19px !important;"></span>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" name="timezone" id="timezone" value="">
                                <div class="form-footer d-md-flex mb-30">
                                    <button type="submit" class="btn-animated-bl">Login</button>
                                    <u><a href="<?php echo base_url(); ?>forget-password" class="forget-pass d-block mt-md-0 mt-3"> Forgot password?</a></u>
                                </div>
                                <!--  <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="newsletter-signup">
                                            <label class="custom-control-label" for="newsletter-signup">Remember Me</label>
                                        </div> --><!-- End .custom-checkbox -->
                                <?php echo form_close(); ?>

                                <div class="middle-or mb-5">
                                    <p>Or</p>
                                </div>

                                <div class="row omb_login mt-5">
                                    <div class="register-link">
                                        <p class="font-weight-bold">Don't have an account? <a href="<?php echo base_url(); ?>sign-up">Create Account</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include("inc/footer.php"); ?>
    </div>
    <?php include("inc/script.php"); ?>
    <script src="<?php echo base_url(); ?>assets/js/jstz.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
    <!-- For getting local timezone -->
    <script type="text/javascript">
        $(document).ready(function() {
            // 
            $('form').parsley();
            //Get browser timezone & set it
            var timezone = jstz.determine();
            $('#timezone').val(timezone.name());
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
        
            $('select').niceSelect('destroy');
            $('.select2').select2({
                "width": "10%"
            });
            $("#country_code").select2({
                "width": "100%",
            });
            $(".imageselect2").select2({
      "width":"100%",
      templateResult: UserTemplate,
      templateSelection: UserTemplate
    });
        });
  function UserTemplate (user) {
    if (!user.id) {
      return user.text;
    }
    var $user = $(
      '<span><img src="'+$(user.element).data('profileimage')+'" width="20px" height="20px" class="img-flag"/> '+user.text+'</span>'
    );
    return $user;
  };
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $(document).ready(function() {
            /***phone number format***/
            $(".phone-format").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("" + curval + "" + "-");
                } else if (curchr == 3 && curval.indexOf("(") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 3 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 7) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });
    </script>
</body>

</html>