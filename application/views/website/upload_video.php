<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <!--start-body-->
            <section class="otp-verif contact-panel px-2 px-lg-4 py-4">
                <div class="container">
                    <div class="heading pt-4">
                        <h4>Upload a video of yourself to verify your profile.</h4>
                        <span class="font-weight-bold mb-2 d-block">Instructions:</span>
                        <ul class="pl-20">
                            <li><p class="mb-0">State your full name and age.</p></li>
                            <li><p class="mb-0">State the country you live in.</p></li>
                        </ul>
                        <?php echo form_open('upload-video', array('method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                            <div class="file-upload">
                                <div class="image-upload-wrap">
                                    <input type="file" id="fileInput" class="file-upload-input" name="video" accept="video/*" parsley-trigger="change" data-parsley-required-message="Please upload video" required data-parsley-errors-container="#videoeerror" onchange="readURL(this);">

                                    <div class="drag-text">
                                        <span class="  d-block">
                                            <svg id="Iconly_Bold_Upload" data-name="Iconly/Bold/Upload" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 42 42">
                                                <g id="Upload" transform="translate(3.5 3.5)">
                                                    <path id="Upload-2" data-name="Upload" d="M7.787,36.365A7.856,7.856,0,0,1,.008,28.815L0,28.441V19.754a7.848,7.848,0,0,1,7.4-7.88l.365-.008h8.384V22.549a1.346,1.346,0,0,0,2.682.187l.013-.187V11.866h8.365a7.856,7.856,0,0,1,7.779,7.549l.008.373V28.46a7.852,7.852,0,0,1-7.4,7.9l-.365.008Zm8.366-24.5V4.619L13.37,7.42a1.353,1.353,0,0,1-1.907,0,1.336,1.336,0,0,1-.148-1.756l.131-.151L16.538.4A1.336,1.336,0,0,1,17.5,0a1.319,1.319,0,0,1,.788.26L18.445.4l5.094,5.109a1.349,1.349,0,0,1-1.758,2.038l-.15-.131-2.783-2.8v7.245Z" fill="#898e95"/>
                                                </g>
                                            </svg>
                                        </span>
                                        <p class="drop-zoon__paragraph">You Can Upload Video</p>
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <video class="file-upload-image" src="#" ></video>
                                    <span class="image-title d-block">Uploaded Image</span>
                                    <div class="image-title-wrap">
                                 
                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                            </svg> 
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_error('video'); ?>
                            <div id="videoeerror"></div>

                            <div class="button mt-150">
                                <button type="submit" class="btn btn__primary btn__rounded w-100 fz-16" >Submit For Verification</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include("inc/footer.php");?>
        </div>
        <?php  include("inc/script.php");?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function(e) {
                      $('.image-upload-wrap').hide();

                      $('.file-upload-image').attr('src', e.target.result);
                      $('.file-upload-content').show();

                      $('.image-title').html(input.files[0].name);
                    };

                    reader.readAsDataURL(input.files[0]);

                } else {
                    removeUpload();
                }
            }

            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });

        </script>
    </body>
</html>