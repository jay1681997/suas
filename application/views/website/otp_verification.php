<?php $customerData = $this->session->userdata('website_userdata'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <!--start-body-->
            <section class="otp-verif contact-panel text-center">
                <div class="container">
                    <?php if($this->session->flashdata('success_msg')){ ?>
                        <div class="alert alert-success" >
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg')?>
                        </div>                
                    <?php } ?>
                    <?php if($this->session->flashdata('error_msg')){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg')?>
                        </div>
                    <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>
                    <img src="<?php echo base_url(); ?>website_assets/images/verification.png" alt="verification" class="mb-4">
                    <div class="title">
                        <h4 class="line mb-0 pb-20 position-relative">Verification Code</h4>
                        <P class="mt-0 mb-4 pt-20 fz-16">Enter verification code sent to.<span class="font-weight-bold"><?php echo $customerData['country_code'].' '.$customerData['phone']; ?></span></P>
                    </div>

                    <div class="alert alert-success alert-dismissable zoomIn animated" id="verifyotpform_successmsg" style="display: none;"></div>
                    <div class="clearfix"></div>
                    <div class="alert alert-danger alert-dismissable zoomIn animated" id="verifyotpform_errormsg" style="display: none;"></div> 
                    <div class="form">
                        <?php echo form_open('otp-verification', array('method' => 'post','class'=> 'otp-box mt-5')); ?>
                            <input class="otp" type="text" name="otp_1" id="otp_1" maxlength=1 >
                            <input class="otp" type="text" name="otp_2" id="otp_2" maxlength=1 >
                            <input class="otp" type="text" name="otp_3" id="otp_3" maxlength=1 >
                            <input class="otp" type="text" name="otp_4" id="otp_4" maxlength=1 >


                        <?php echo form_close(); ?>
                    </div>
                     <input type="hidden" name="timezone" id="timezone" value="">
                    <div id="verifyotp_otperror" class="error" style="color:red; margin-top: 10px;"></div>   
                    <div class="mt-30 mb-30">
                        <p class="fz-16">Didn't receive verification code?</p>
                        <button type="button" id="resendotpbtn" class="font-weight-bold " onclick="resendotp();" style="color:#17966B"><u> Resend code</u></button>
                    </div>
                   <!--  <a href="#" class="btn btn__primary btn__outlined btn__rounded w-100 fz-16" data-toggle="modal" data-target="#form">Verify and Proceed</a> -->
                   
                    <button type="button" class="btn btn__primary btn__rounded w-100 fz-16 btn-animated-bl" id="verifyotpformbtn">Verify and Proceed</button>
                </div>
            </section>
            <!--end-body-->
            <!--start-modal-->
            <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content custom-radius">
                        <div class="modal-header border-bottom-0">
                             <!-- <h5 class="modal-title" id="exampleModalLabel"></h5> -->
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div> 
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/verification-done.png" alt="verification-dones" class="mb-20">
                            <div class="content">
                                <h4>Verification</h4>
                                <p class="fz-16">Your verification has been done successfully</p>
                                <!-- <a href="<?php echo base_url(); ?>create-new-password" class="btn btn__primary btn__outlined btn__rounded fz-16">Continue</a> -->
                                <a id="continue_button" class="btn btn__primary btn__rounded fz-16">Continue</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <!--end-modal-->
        <?php  include("inc/footer.php");?>
        </div><!-- /.wrapper -->

        <?php  include("inc/script.php");?>
          <script type="text/javascript">
        $(document).ready(function() {
            // 
            $('form').parsley();
            //Get browser timezone & set it
            var timezone = jstz.determine();
            $('#timezone').val(timezone.name());
        });
    </script>
        <script>
            $(document).ready(function(){
                $('#resendotpbtn').attr('disabled', true);
                setTimeout(function(){$('#resendotpbtn').attr('disabled', false);},10000);
            });
            // let digitValidate = function(ele){
            //     console.log(ele.value);
            //     ele.value = ele.value.replace(/[^0-9]/g,'');
            // }

            // let tabChange = function(val){
            //     let ele = document.querySelectorAll('.otp-box input');
            //     if(ele[val-1].value != ''){
            //         ele[val].focus()
            //     }else if(ele[val-1].value == ''){
            //         ele[val-2].focus()
            //     }   
            // }

            $("#otp_1").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {        
                    return false;
                }
            });
            $("#otp_2").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {        
                    return false;
                }
            });
            $("#otp_3").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {        
                    return false;
                }
            });
            $("#otp_4").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {        
                    return false;
                }
            });
            $("#otp_1").keyup(function(event){
                if (event.which == 8 && $(this).val().length == 0) {
                    $('#otp_1').focus();
                } else {
                    if($("#otp_1").val()!='') {
                        $("#otp_2").focus();
                    }   
                }
            });
            $("#otp_2").keyup(function(event){
                if (event.which == 8 && $(this).val().length == 0) {
                    $('#otp_1').focus();
                } else {
                    if($("#otp_2").val()!=''){
                        $("#otp_3").focus();          
                    }
                }
            });
            $("#otp_3").keyup(function(event){
                if (event.which == 8 && $(this).val().length == 0){
                    $('#otp_2').focus();
                } else {
                    if($("#otp_3").val()!='') {
                        $("#otp_4").focus();  
                    }
                }
            });
            $("#otp_4").keyup(function(event){      
                if (event.which == 8 && $(this).val().length == 0) {
                    $('#otp_3').focus();
                }
            });

            function resendotp()
            {
                $("#otp_1").attr('readOnly','true');
                $("#otp_2").attr('readOnly','true');
                $("#otp_3").attr('readOnly','true');
                $("#otp_4").attr('readOnly','true');
                $('#resendotpbtn').prop('disabled',true);
                $('#verifyotpformbtn').prop('disabled',true);
                $('#resendotpbtn').html('<?php echo $this->lang->line('website_keyword_sending'); ?> <i class="fa fa-spin fa-spinner"></i>');
                $.ajax({
                    url:"<?php echo base_url().'website/resend_otp'; ?>",
                    type:'GET',
                    dataType: 'json',
                    error:function(err){
                        $("#otp_1").removeAttr('readOnly');
                        $("#otp_2").removeAttr('readOnly');
                        $("#otp_3").removeAttr('readOnly');
                        $("#otp_4").removeAttr('readOnly');
                        $('#resendotpbtn').prop('disabled',false);
                        $('#verifyotpformbtn').prop('disabled',false);
                        $('#resendotpbtn').html('Resend code');
                    },
                    success:function(data){
                        if(data.status=='success'){
                            $('#verifyotpform_successmsg').html(data.msg);
                            $('#verifyotpform_successmsg').show();
                            setTimeout(function(){
                                $('#verifyotpform_successmsg').hide();
                            },3600);
                           
                        } else {
                            $('#verifyotpform_errormsg').html(data.msg);
                            $('#verifyotpform_errormsg').show();
                            setTimeout(function(){
                                $('#verifyotpform_errormsg').hide();
                            },3600);
                        }
                        $("#otp_1").removeAttr('readOnly');
                        $("#otp_2").removeAttr('readOnly');
                        $("#otp_3").removeAttr('readOnly');
                        $("#otp_4").removeAttr('readOnly');
                        // $('#resendotpbtn').prop('disabled',false);
                        $('#verifyotpformbtn').prop('disabled',false);
                        $('#resendotpbtn').attr('disabled', true);
                        setTimeout(function(){$('#resendotpbtn').attr('disabled', false);},10000);
                        $('#resendotpbtn').html('Resend code');
                    }
                });
            }

            $('#verifyotpformbtn').click(function(){
                if($("#otp_1").val()!='' && $("#otp_2").val()!='' && $("#otp_3").val()!='' && $("#otp_4").val()!='') {
                    $("#otp_1").attr('readOnly','true');
                    $("#otp_2").attr('readOnly','true');
                    $("#otp_3").attr('readOnly','true');
                    $("#otp_4").attr('readOnly','true');
                    $('#resendotpbtn').prop('disabled',true);
                    $('#verifyotpformbtn').prop('disabled',true);
                    $('#verifyotpformbtn').html('<?php echo $this->lang->line('website_keyword_submiting'); ?> <i class="fa fa-spin fa-spinner"></i>');
                    $('#verifyotp_otperror').hide();
                    var formdata= {"otp_code":$("#otp_1").val()+$("#otp_2").val()+$("#otp_3").val()+$("#otp_4").val()};
                    $.ajax({
                        url:"<?php echo base_url().'otp-verification'; ?>",
                        type:'POST',
                        data:formdata,
                        dataType: 'json',
                        error:function(err){
                            $("#otp_1").removeAttr('readOnly');
                            $("#otp_2").removeAttr('readOnly');
                            $("#otp_3").removeAttr('readOnly');
                            $("#otp_4").removeAttr('readOnly');
                            $('#resendotpbtn').prop('disabled',false);
                            $('#verifyotpformbtn').prop('disabled',false);
                            $('#verifyotpformbtn').html('Verify and Proceed');
                        },
                        success:function(data){
                            console.log(data)
                            if(data.status=='success'){
                                $('#verifyotpform_successmsg').html(data.msg);
                                $('#verifyotpform_successmsg').show();
                                $('#form').modal('show');
                                $('#continue_button').attr('href', '<?php echo base_url(); ?>'+data.redirect_url);
                            } else {
                                $("#otp_1").val('');
                                $("#otp_2").val('');
                                $("#otp_3").val('');
                                $("#otp_4").val('');
                                $('#verifyotpform_errormsg').html(data.msg);
                                $('#verifyotpform_errormsg').show();
                                setTimeout(function(){
                                    $('#verifyotpform_errormsg').hide();
                                },3600);
                            }
                            $("#otp_1").removeAttr('readOnly');
                            $("#otp_2").removeAttr('readOnly');
                            $("#otp_3").removeAttr('readOnly');
                            $("#otp_4").removeAttr('readOnly');
                            $('#resendotpbtn').prop('disabled',false);
                            $('#verifyotpformbtn').prop('disabled',false);
                            $('#verifyotpformbtn').html('Verify and Proceed');
                        }
                    });
                } else {
                    $('#verifyotp_otperror').html('Verification code is required');
                }
            });
        </script>
    </body>
</html>