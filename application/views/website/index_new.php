<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("inc/style.php"); ?>
</head>
<style>
    .sport-speach-heading h1 {
    text-align: center;
    margin-bottom: 30px;
    font-size: 70px;
}

.contest-block-date {
    margin-bottom: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
}

.date-bk {
    display: inline-flex;
    align-items: center;
    padding-right: 40px;
    margin-right: 40px;
}
.date-bk:after {
    content: '';
    position: relative;
    display: block;
    background: #767575;
    width: 2px;
    height: 40px;
    left: 35px;
}
.date-bk h5 {
    font-size: 55px;
    color: #08976d;
    font-weight: 700;
    margin-right: 15px;
    margin-bottom: 0;
}
.date-bk p {
    color: #767575;
    display: flex;
    flex-direction: column;
    font-size: 18px;
    margin-bottom: 0;
}
p.time-green {
    color: #08976d;
    font-weight: 600;
    font-size: 20px;
}
p.time-green span {
    color: #767575;
    font-size: 18px;
    font-weight: 500;
}
.date-bk:last-child {
    border-right: none;
    padding-right: 0;
    margin-right: 0;
}

.date-bk:last-child::after {
    content: '';
    width: 0;
    height: 0;
}

@media screen and (max-width: 767px) {
    .sport-speach-heading h1 {
        font-size: 30px;
    }
    .date-bk {
        padding-right: 10px;
        margin-right: 10px;
    }
    .date-bk h5 {
        font-size: 35px;
        margin-right: 8px;
    }
    .date-bk:after {
        left: 10px;
    }
    .date-bk p {
        line-height: 1.5;
        font-size: 16px;
    }
    p.time-green {
        line-height: 1.5;
        font-size: 16px;
    }
}
</style>

<body>
    <div class="wrapper" id="wrapper">
        <?php include("inc/header.php"); ?>
      
        <section class="slider">
       <div class="slick-carousel m-slides-0"
        data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "speed": 700,"fade": true,"cssEase": "linear"}'>
        <div class="slide-item align-v-h">
          <div class="bg-img"><img src="assets/images/sld-img-bn-sm.png" alt="slide img"></div>
          <div class="container">
            <div class="row align-items-center justify-content-center">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8"> 
                <div class="slide__content">
                  <div class="sport-speach-heading">
                    <h1 class="text-dark ">Sport Speech Contest <span class="d-block">2021</span></h1>
                  </div>
                 <!--  <div class="contest-block-date">
                    <div class="date-bk">
                      <h5>25</h5>
                      <p>May, <span>2021</span></p>
                    </div>
                    <div class="date-bk">
                      <p class="time-green">3:00 <span>PM</span></p>
                    </div>

                    <div class="date-bk">
                      <h5>50</h5>
                      <p>Total<span>Participants</span></p>
                    </div>
                    
                  </div> -->
                  
                  <div class="d-flex flex-wrap align-items-center">
                    <!-- <a href="#" class="btn btn__secondary btn__rounded mr-30">
                      <span>View More</span>
                      <i class="icon-arrow-right"></i>
                    </a> -->
                  </div>
                </div>
                <!-- /.slide-content -->
              </div>
              <!-- /.col-xl-7 -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.slide-item -->

        
      </div>
    <section class="primary-dm-block bg-light">
      <div class="container">
        <div class="slide__content">
          <div class="row">
            <div class="col-6 col-lg-3 col-md-3 mb-4">
              <div class="m-0 text-center">
                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><<img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group1.png" class="" alt="image1"></a>
              </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3 mb-4">
              <div class="m-0 text-center">
                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal1"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group2.png" class="" alt="image1"></a>
              </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3 mb-4">
              <div class="m-0 text-center">
                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal2"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group3.png" class="" alt="image1"></a>
              </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3 mb-4">
              <div class="m-0 text-center">
                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal3"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group4.png" class="" alt="image1"></a>
              </div>
            </div>
          </div>
          <div class="text-center">
            <h5 class="text-dark">Participate in an online speech contest and win cash prizes!</h5>
          <!-- <p class="slide__desc text-dark">Users are separated into 4 different categories; Primary Ages 5-8, Junior Ages 9-12, Teens Ages 13-17, and Adults Ages 18+. Only users in the specified age group will be able to view the content for that group.</p> -->
          </div>
          <div class="d-flex flex-wrap align-items-center">
            <!-- <a href="#" class="btn btn__secondary btn__rounded mr-30">
              <span>View More</span>
              <i class="icon-arrow-right"></i>
            </a> -->
          </div>
        </div>
      </div>
    </section>
          <!--   <div class="slick-carousel m-slides-0" data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "speed": 700,"fade": true,"cssEase": "linear"}'>
                <div class="slide-item align-v-h">
                    <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/backgrounds/banner-images.png" alt="slide img"></div>
                    <div class="container" id="container">
                        <div class="row align-items-center">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-7">
                                <div class="slide__content">
                                    <div class="row"> -->
                                        <!-- <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group1.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal1"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group2.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal2"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group3.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal3"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group4.png" class="" alt="image1"></a>
                                            </div>
                                        </div> -->
                                        <!-- <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/image11.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal1"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/image21.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal2"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/image31.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal3"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/image41.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h5 class="text-white">Participate in an online speech contest and win cash prizes!</h5> -->
                                    <!--   <p class="slide__desc text-white">Users are separated into 4 different categories; Primary Ages 5-8, Junior Ages 9-12, Teens Ages 13-17, and Adults Ages 18+. Only users in the specified age group will be able to view the content for that group.</p> -->
                                    <!-- <p class="slide__desc text-white">Users who are 5-8 years old will be in the primary category.Click video above for more information.</p>
                                    <div class="d-flex flex-wrap align-items-center"></div>
                                </div> -->
                                <!-- /.slide-content -->
                  <!--           </div>
                        </div>
                    </div>
                </div>
            </div> -->
        <!-- </section> -->

        <!-- ========================
                About Layout 4
                =========================== -->
        <section class="about-layout4 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="heading-layout2">
                            <!-- <h3 class="heading__title mb-40">About Stand up & Speak</h3> -->
                        </div><!-- /heading -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <!-- <div class="col-sm-12 col-md-12 col-lg-6">
                                <div class="about__Text">
                                    <p class="mb-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. 
                                    </p>
                                    <p class="mb-30">Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p class="mb-30">Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <div class="d-flex align-items-center mb-30">
                                        <a href="<?php echo base_url(); ?>" class="btn btn__primary btn__outlined btn__rounded mr-30">
                                         See Contests</a>
                                    </div>
                                </div>
                            </div> --><!-- /.col-lg-6 -->
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <!--  <div class="video-banner">
                                    <img src="<?php echo base_url(); ?>website_assets/images/about/4.jpg" alt="about">
                                    <a class="video__btn video__btn-white" href="#">
                                        <div class="video__player">
                                            <i class="fa fa-play"></i>
                                        </div>
                                    </a>
                                </div> --><!-- /.video-banner -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
        </section>

        <!-- ======================
                Features Layout 1
                ========================= -->

        <section class="features-layout1 pt-130 pb-50 mt--90">
            <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/backgrounds/1.jpg" alt="background"></div>
            <div class="container">
                <div class="row mb-40">
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div class="heading__layout2">
                            <!-- <h3 class="heading__title">change user to you</h3> -->
                        </div>
                    </div><!-- /col-lg-5 -->
                    <!-- <div class="col-sm-12 col-md-12 col-lg-5 offset-lg-1">
                        <p class="heading__desc font-weight-bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <div class="d-flex flex-wrap align-items-center mt-40 mb-30">
                            <a href="<?php echo base_url(); ?>sign-up" class="btn btn__primary btn__rounded mr-30">
                                <span>Register</span>
                                <i class="icon-arrow-right"></i>
                            </a>
                            <a href="<?php echo base_url(); ?>" class="btn btn__secondary btn__link">
                                <i class="icon-arrow-right icon-filled"></i>
                                <span>Our Subscriptions</span>
                            </a>
                        </div>
                    </div> --><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <!-- Feature item #1 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/video-upload.png">
                                </div>
                                <h4 class="feature__title">Upload Speech</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #2 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/speech-upload.png">
                                </div>
                                <h4 class="feature__title">My Speeches</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>/login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #3 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/data-analysis.png">
                                </div>
                                <h4 class="feature__title">My Evaluations</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #4 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/online-classes.png">
                                </div>
                                <h4 class="feature__title">Online Classes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #5 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/enrolled-class.png">
                                </div>
                                <h4 class="feature__title">Enrolled Classes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #6 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/contest.png">
                                </div>
                                <h4 class="feature__title">Contests</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #7 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/rewards.png">
                                </div>
                                <h4 class="feature__title">Earn Rewards</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #8 -->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/promocode.png">
                                </div>
                                <h4 class="feature__title">Promocodes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12 col-lg-6 offset-lg-3 text-center">
                        <!-- <p class="font-weight-bold mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis .<br/>
                                        <a href="#" class="color-secondary">
                                            <span>Contact Us For More Information</span> <i class="icon-arrow-right"></i>
                                        </a>
                                    </p> -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.Features Layout 1 -->

        <!-- ======================
                 Work Process 
                ========================= -->

        <section class="work-process work-process-carousel overflow-hidden pt-130 pb-0 bg-overlay bg-overlay-secondary">
            <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/banners/1.jpg" alt="background"></div>
            <div class="container">
                <div class="row heading-layout2">
                    <div class="col-12">
                        <!--  <h2 class="heading__subtitle color-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</h2> -->
                    </div><!-- /.col-12 -->
                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-5">
                        <!--  <h3 class="heading__title color-white">Ut enim ad minim veniam
                                    quis
                                </h3> -->
                        <h3 class="heading__title color-white">Online Classes</h3>
                        <p class="heading__desc font-weight-bold color-gray mb-40">Click on a class below to find out more information</p>
                    </div><!-- /.col-xl-5 -->
                    <!-- <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 offset-xl-1">
                                <p class="heading__desc font-weight-bold color-gray mb-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                  consequat.
                                </p>
                                <ul class="list-items list-items-layout2 list-items-light list-horizontal list-unstyled">
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                </ul>
                            </div> --><!-- /.col-xl-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container mt-90">
                            <div class="slick-carousel" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "infinite":false, "arrows": false, "dots": false, "responsive": [{"breakpoint": 1200, "settings": {"slidesToShow": 3}}, {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
                                <!-- process item #1 -->
                                <?php if (!empty($classes)) {
                                    foreach ($classes as $key => $value) { ?>

                                        <div class="process-item text-center">
                                            <div class="program-preview mb-30">
                                                <img src="<?php echo S3_BUCKET_ROOT . CLASS_IMAGE . $value['class_image']; ?>" title="program">
                                            </div>
                                            <a href="<?php echo base_url(); ?>login">
                                            <h4 class="process__title less-padding-df pr-0"><?php echo $value['program_title']; ?></h4>
                                            <div class="d-flex justify-content-around">
                                                <p class="spots-badge"><?php $booked_spots = $this->db->select('*')->from('tbl_order')->where('item_type', 'class')->where('item_id', $value['id'])->get()->num_rows();
                                                                        $remaining_spot = $value['total_spot'] - $booked_spots; ?>
                                                    <?php echo $remaining_spot . '/' . $value['total_spot']; ?> spots</p>
                                                <!-- <a href="<?php echo base_url(); ?>/login"> -->
                                                    <p class="spots-price"><?php echo $this->session->userdata('currency') . ' ' . $value['price']; ?></p>
                                                <!-- </a> -->
                                            </div>
                                        </a>
                                            <p class="process__desc"><?php $description_length = strlen(@$value['description']);
                                                                        if ($description_length > 30) {
                                                                            echo substr(@$value['description'], 0, 30 - $description_length) . "... <a href='javascript:void(0)'>Read More</a>";
                                                                        } else {
                                                                            echo @$value['description'];
                                                                        }  ?></p>
                                            <a href="#" class="btn btn__secondary btn__link small-custom-font">
                                                <span><i class="far fa-calendar-alt"></i> <?php echo $this->common_model->date_convert($value['start_datetime'], 'd M, Y', $this->session->userdata('website_timezone')) . ' - ', $this->common_model->date_convert($value['end_datetime'], 'd M, Y', $this->session->userdata('website_timezone')); ?></span>
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div><!-- /.process-item -->
                                <?php }
                                } ?>


                            </div><!-- /.carousel -->
                        </div>
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="cta bg-primary">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-2 col-lg-2">
                            <img src="<?php echo base_url(); ?>website_assets/images/icons/data.png" alt="alert">
                        </div><!-- /.col-lg-2 -->
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <!--   <h4 class="cta__title">Lorem ipsum dolor sit amet</h4>
                                    <p class="cta__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua.
                                    </p> -->
                            <h4 class="cta__title">My Evaluations</h4>
                            <p class="cta__desc">Build confidence one evaluation at a time</p>
                        </div><!-- /.col-lg-7 -->
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <a href="<?php echo base_url(); ?>login" class="btn btn__secondary btn__secondary-style2 btn__rounded mr-30">
                                <span>Evaluate my speech</span>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div><!-- /.col-lg-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.cta -->
        </section><!-- /.Work Process -->

        <!-- ======================
                Team
                ========================= -->

        <section class="team-layout2 pb-80 overflow-hidden">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                        <div class="heading text-center mb-40">
                            <h3 class="heading__title">Upcoming Contests</h3>
                            <!--  <p class="heading__desc">Our administration and support staff all have exceptional people skills and
                                        trained to assist you with all medical enquiries.
                                    </p> -->
                            <p class="heading__desc">Click below to see more details about upcoming contests
                            </p>
                        </div><!-- /.heading -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="slick-carousel" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true, "arrows": false, "dots": false, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 1}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
                            <?php if (!empty($contests)) {
                                foreach ($contests as $key => $value) { ?>
                                    <div class="member">
                                        <div class="member__img">
                                            <?php $image_name = explode(".", $value['contest_image']);
                                            if ($image_name[1] == 'mp4' || $image_name[1] == 'mp3') { ?>
                                                <!-- <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" alt="member img"> -->
                                                <!--  <video width="400px" height="300px" controls > <source src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" > Your browser does not support HTML5 video. </video> -->
                                                <img width="400px" height="300px" src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" alt="member img">
                                            <?php } else { ?>
                                                <img width="400px" height="300px" src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" alt="member img">
                                            <?php  }
                                            ?>

                                        </div>
                                        <div class="member__info">
                                            <h5 class="member__name"><a href="<?php echo base_url(); ?>/login"><?php echo $value['name']; ?></a></h5>
                                            <?php if ($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])) { ?>
                                                <p class="member__job"><img src="<?php echo $value['sponsors']['sponsor_logo']; ?>" class="round-profile-img"> Sponser by <?php echo $value['sponsors']['sponsor_name']; ?></p>
                                            <?php } ?>
                                            <div class="mt-20 d-flex flex-wrap justify-content-between align-items-center">
                                                <a href="<?php echo base_url(); ?>login" class="btn btn__secondary btn__link btn__rounded btn-live">
                                                    <span><i class="far fa-play-circle"></i> <?php echo ucfirst($value['contest_type']); ?></span>
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                                <ul class="social-icons list-unstyled mb-0">
                                                    <li><a href="<?php echo base_url(); ?>login" class="content-charge"> <?php echo $this->session->userdata('currency') . ' ' . $value['price']; ?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            <?php }
                            } ?>
                        </div><!-- /.carousel -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.Team -->

        <!-- ========================
                gallery
                =========================== -->

        <section class="gallery pt-0 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                        <div class="heading text-center mb-40">
                            <!-- <h3 class="heading__title">Download Our App</h3>
                                    <p class="heading__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt.
                                    </p> -->
                        </div><!-- /.heading -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <div class="title-wrap pb-0">
                            <!-- <div class="title-box">
                                        <h5> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor.</h5>
                                        <p class="my-4 pr-4"> Easy and quick to use</p>
                                        <div class="transpoer-listing-section-app">
                                            <ul>
                                                <li>Lorem ipsum dolor sit amet dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut</li>
                                                <li>Consectetur adipisicing elit tempor incididunt ut labore et dolore magna aliqua</li>
                                                <li>Sed do eiusmod reprehenderit in voluptate velit esse
                                                    cillum dolore eu fugiat</li>
                                                <li>tempor incididunt ut labore sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                                <li>Dolore magna aliqua Excepteur sint occaecat cupidatat non
                                                proident, sunt in culpa qui officia</li>
                                            </ul>
                                        </div>
                                        <div class="dwnld-link justify-content-center">
                                            <a href="#" class="mr-2"><img class="" src="<?php echo base_url(); ?>website_assets/images/app-store.png" alt=""></a>
                                        </div>
                                    </div> -->
                        </div>
                    </div>
                    <!-- <div class="col-12 col-md-6">
                                <picture>
                                    <source media="(min-width:650px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png">
                                    <source media="(min-width:465px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png"> <img src="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png" alt="Flowers" style="width:100%;"> 
                                </picture>
                            </div> -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.gallery 2 -->

        <!--start modal-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video1" class="w-100">
                            <source src="<?php echo base_url(); ?>website_assets/video/Primary.MP4" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal2-->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video2" class="w-100">
                            <source src="<?php echo base_url(); ?>website_assets/video/Junior.MP4" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal3-->
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video3" class="w-100">
                            <source src="<?php echo base_url(); ?>website_assets/video/Teens.MP4" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal4-->
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video4" class="w-100">
                            <source src="<?php echo base_url(); ?>website_assets/video/Adult.MP4" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->
        <?php include("inc/footer.php"); ?><!-- /.Footer -->
    </div><!-- /.wrapper -->
    <?php include("inc/script.php"); ?>
    <script>
        $('#myModal').on('shown.bs.modal', function() {
            $('#video1')[0].play();
        })
        $('#myModal').on('hidden.bs.modal', function() {
            $('#video1')[0].pause();
        })

        $('#myModal1').on('shown.bs.modal', function() {
            $('#video2')[0].play();
        })
        $('#myModal1').on('hidden.bs.modal', function() {
            $('#video2')[0].pause();
        })

        $('#myModal2').on('shown.bs.modal', function() {
            $('#video3')[0].play();
        })
        $('#myModal2').on('hidden.bs.modal', function() {
            $('#video3')[0].pause();
        })

        $('#myModal3').on('shown.bs.modal', function() {
            $('#video4')[0].play();
        })
        $('#myModal3').on('hidden.bs.modal', function() {
            $('#video4')[0].pause();
        })
    </script>
</body>

</html>