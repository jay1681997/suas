<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("inc/style.php"); ?>
</head>
<style type="text/css">
    @media screen and (min-width: 1440px) {
        .navbar .nav__item {
            margin-right: 2px;
        }

        .navbar .nav__item .nav__item-link {
            font-size: 15px;
        }
    }

    .nice-select {
        width: 190px;
    }

    .customization_text img {
        background: #383838;
        padding: 30px;
        border-radius: 50%;
        display: block;
        width: 50px;
        height: 50px;
    }

    .round-st-block {
        position: relative;
        display: inline-block;
    }

    .rs-student-section {
        width: 180px;
        height: 180px;
        border-radius: 50%;
        overflow: hidden;
        background-color: gray;
    }

    .rs-student-section img {
        width: 100%;
        height: 100%;
        object-fit: contain;
    }

    .vd-play-model {
        position: absolute;
        background: #ffffff;
        border-radius: 50%;
        width: 45px;
        height: 45px;
        padding: 12px;
        line-height: 23px;
        font-size: 15px;
        right: 0;
        top: 60%;
        transform: translateY(25px);
        box-shadow: 2px 2px 10px #0000001f;
    }

    .std-info-float h6 {
        color: #ffffff;
        font-weight: 700;
        margin-bottom: 0px;
    }

    .std-info-float {
        position: absolute;
        bottom: 20px;
        left: 0;
        right: 0;
        color: #ffffff;
    }

    .rs-student-section:before {
        content: '';
        position: absolute;
        background: #0000003d;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        border-radius: 50%;
    }

    .green-play i {
        color: #17966b;
    }

    .orange-play {
        color: #f78f08;
    }

    .pink-play {
        color: #fe326f;
    }

    .blue-play {
        color: #3b3c92;
    }

    @media (max-width: 992px) {
        .pb-50 {
            padding-bottom: 30px !important;
        }
    }

    @media (max-width: 767px) {
        .res-img-fix {
            text-align: center;
        }

        .res-img-fix img {
            width: 100px;
        }
    }

    .podium-block-img {
        text-align: center;
        position: relative;
        top: 0px;
    }

    .podium-block-img img {
        width: 90%;
        margin: 0 auto;
    }

    @media (min-width: 1920px) {
        .podium-block-img {
            text-align: center;
            position: relative;
            top: 80px;
        }

        .podium-block-img img {
            width: 80%;
            margin: 0 auto;
        }
    }

    @media (max-width: 1440px) {
        .podium-block-img {
            text-align: center;
            position: relative;
            top: 33px;
        }

        .podium-block-img img {
            width: 80%;
            margin: 0 auto;
        }
    }

    @media (max-width: 767px) {
        .podium-block-img {
            text-align: center;
            position: relative;
            top: 33px;
        }

        .podium-block-img img {
            width: 80%;
            margin: 0 auto;
        }
    }

    .res-ad-video {
        height: calc(100vh - 150px);
    }
</style>


<body>
    <div class="wrapper" id="wrapper">
        <?php include("inc/header.php"); ?>
        <section class="slider">
            <div class="slick-carousel m-slides-0" data-slick='{"slidesToShow": 1, "arrows": true, "dots": false, "speed": 700,"fade": true,"cssEase": "linear"}'>
                <div class="slide-item align-v-h">
                    <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/backgrounds/banner-images.png" alt="slide img"></div>
                    <div class="container" id="container">
                        <div class="row align-items-center">

                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-7">

                                <div class="slide__content">
                                    <div class="row">
                                        <h2 class="text-white">Participate in an online speech contest and win cash prizes!</h2>
                                        <!-- <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group1.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal1"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group2.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal2"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group3.png" class="" alt="image1"></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal3"><img src="<?php echo base_url(); ?>website_assets/images/banner-age/Group4.png" class="" alt="image1"></a>
                                            </div>
                                        </div> -->
                                        <div class="col-6 col-lg-6 col-md-3 mb-4 text-center ">
                                            <div class="m-0 round-st-block">
                                                <a href="#" data-toggle="modal" data-target="#myModal">
                                                    <div class="rs-student-section" style="background-color: #3b3c92;">
                                                        <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[3]['thumb_image']; ?>" class="" alt="image1">
                                                        <div class="std-info-float">
                                                            <h6><?php echo $home_videos[3]['title']; ?></h6>
                                                            <span>Age: <?php echo $home_videos[3]['age']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="vd-play-model blue-play">
                                                        <i class="fas fa-play"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-6 col-lg-6 col-md-3 mb-4 text-center ">
                                            <div class="m-0 round-st-block">
                                                <!-- <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><img src="./assets/images/banner-age/Group1.png" class="" alt="image1"></a> -->
                                                <a href="#" data-toggle="modal" data-target="#myModal1">
                                                    <div class="rs-student-section" style="background-color: #f78f08;">
                                                        <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[2]['thumb_image']; ?>" class="" alt="image1">
                                                        <div class="std-info-float">
                                                            <h6><?php echo $home_videos[2]['title']; ?></h6>
                                                            <span>Age: <?php echo $home_videos[2]['age']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="vd-play-model orange-play">
                                                        <i class="fas fa-play"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-6 col-lg-6 col-md-3 mb-4 text-center ">
                                            <div class="m-0 round-st-block">
                                                <a href="#" data-toggle="modal" data-target="#myModal2">
                                                    <div class="rs-student-section" style="background-color: #17966b;">
                                                        <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[1]['thumb_image']; ?>" class="" alt="image1">
                                                        <div class="std-info-float">
                                                            <h6><?php echo $home_videos[1]['title']; ?></h6>
                                                            <span>Age: <?php echo $home_videos[1]['age']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="vd-play-model green-play">
                                                        <i class="fas fa-play"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-6 col-lg-6 col-md-3 mb-4 text-center ">
                                            <div class="m-0 round-st-block">
                                                <a href="#" data-toggle="modal" data-target="#myModal3">
                                                    <div class="rs-student-section" style="background-color: #fe326f;">
                                                        <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[0]['thumb_image']; ?>" class="" alt="image1">
                                                        <div class="std-info-float">
                                                            <h6><?php echo $home_videos[0]['title']; ?></h6>
                                                            <span>Age: <?php echo $home_videos[0]['age']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="vd-play-model pink-play">
                                                        <i class="fas fa-play"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                        <!-- <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block"  data-toggle="modal" data-target="#myModal"><img style=" object-fit: contain;
    background-color: #3b3c92;border-radius: 50%;width: 150px;height: 150px;" src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[3]['thumb_image']; ?>" class="" alt="image1"> <p><?php echo $home_videos[3]['title']; ?></p><p><?php echo $home_videos[3]['age']; ?></p></a>
       
        
                                            </div>
                                        </div> -->
                                        <!--  <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal1"><img style="border-radius: 50%;width: 150px;height: 150px; object-fit: contain;
    background-color: #f78f08;" src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[2]['thumb_image']; ?>" class="" alt="image1"> <p><?php echo $home_videos[2]['title']; ?></p><p><?php echo $home_videos[2]['age']; ?></p></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal2"><img style="border-radius: 50%;width: 150px;height: 150px; object-fit: contain;
    background-color: #17966b;" src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[1]['thumb_image']; ?>" class="" alt="image1"> <p><?php echo $home_videos[1]['title']; ?></p><p><?php echo $home_videos[1]['age']; ?></p></a>
                                            </div>
                                        </div>
                                        <div class="col-6 mb-4">
                                            <div>
                                                <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal3"><img style="border-radius: 50%;width: 150px;height: 150px; object-fit: contain;
    background-color: #fe326f;" src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[0]['thumb_image']; ?>" class="" alt="image1"> <p><?php echo $home_videos[0]['title']; ?></p><p><?php echo $home_videos[0]['age']; ?></p></a>
                                            </div>
                                        </div>
                                    </div> -->
                                        <!-- <h5 class="text-white">Participate in an online speech contest and win cash prizes!</h5> -->
                                        <!--   <p class="slide__desc text-white">Users are separated into 4 different categories; Primary Ages 5-8, Junior Ages 9-12, Teens Ages 13-17, and Adults Ages 18+. Only users in the specified age group will be able to view the content for that group.</p> -->
                                        <!--   <p class="slide__desc text-white">Users who are 5-8 years old will be in the primary category.Click video above for more information.</p> -->
                                        <div class="d-flex flex-wrap align-items-center"></div>

                                    </div>

                                    <!-- /.slide-content -->
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                                <div class="podium-block-img">
                                    <img src="<?php echo base_url(); ?>website_assets/images/backgrounds/podium-img.png" alt="iamage">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

        </section>

        <!-- ========================
                About Layout 4
                =========================== -->


        <!-- ======================
                Features Layout 1
                ========================= -->

        <section class="features-layout1 pt-130 pb-50 mt--90">
            <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/backgrounds/1.jpg" alt="background"></div>
            <div class="container">
                <div class="row mb-40">
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div class="heading__layout2">
                            <!-- <h3 class="heading__title">change user to you</h3> -->
                        </div>
                    </div><!-- /col-lg-5 -->
                    <!-- <div class="col-sm-12 col-md-12 col-lg-5 offset-lg-1">
                        <p class="heading__desc font-weight-bold">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                        <div class="d-flex flex-wrap align-items-center mt-40 mb-30">
                            <a href="<?php echo base_url(); ?>sign-up" class="btn btn__primary btn__rounded mr-30">
                                <span>Register</span>
                                <i class="icon-arrow-right"></i>
                            </a>
                            <a href="<?php echo base_url(); ?>" class="btn btn__secondary btn__link">
                                <i class="icon-arrow-right icon-filled"></i>
                                <span>Our Subscriptions</span>
                            </a>
                        </div>
                    </div> --><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <!-- Feature item #1 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/video-upload.png">
                                </div>
                                <h4 class="feature__title">Upload Speech</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #2 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/speech-upload.png">
                                </div>
                                <h4 class="feature__title">My Speeches</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>/login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #3 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/data-analysis.png">
                                </div>
                                <h4 class="feature__title">My Evaluations</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #4 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/online-classes.png">
                                </div>
                                <h4 class="feature__title">Online Classes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #5 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/enrolled-class.png">
                                </div>
                                <h4 class="feature__title">Enrolled Classes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #6 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/contest.png">
                                </div>
                                <h4 class="feature__title">Contests</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #7 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/rewards.png">
                                </div>
                                <h4 class="feature__title">Earn Rewards</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                    <!-- Feature item #8 -->
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <div class="feature__content">
                                <div class="feature__icon custom-img-ic">
                                    <img src="<?php echo base_url(); ?>website_assets/images/icons/promocode.png">
                                </div>
                                <h4 class="feature__title">Promocodes</h4>
                            </div><!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>login" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div><!-- /.feature-item -->
                    </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-12 col-lg-6 offset-lg-3 text-center">
                        <!-- <p class="font-weight-bold mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                        quis .<br/>
                                        <a href="#" class="color-secondary">
                                            <span>Contact Us For More Information</span> <i class="icon-arrow-right"></i>
                                        </a>
                                    </p> -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.Features Layout 1 -->

        <!-- ======================
                 Work Process 
                ========================= -->

        <section class="work-process work-process-carousel overflow-hidden pt-130 pb-0 bg-overlay bg-overlay-secondary">
            <div class="bg-img"><img src="<?php echo base_url(); ?>website_assets/images/banners/1.jpg" alt="background"></div>
            <div class="container">
                <div class="row heading-layout2">
                    <div class="col-12">
                        <!--  <h2 class="heading__subtitle color-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</h2> -->
                    </div><!-- /.col-12 -->
                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-5">
                        <!--  <h3 class="heading__title color-white">Ut enim ad minim veniam
                                    quis
                                </h3> -->
                        <h3 class="heading__title color-white">Online Classes</h3>
                        <p class="heading__desc font-weight-bold color-gray mb-40">Click on a class below to find out more information</p>
                    </div><!-- /.col-xl-5 -->
                    <!-- <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 offset-xl-1">
                                <p class="heading__desc font-weight-bold color-gray mb-40">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                  consequat.
                                </p>
                                <ul class="list-items list-items-layout2 list-items-light list-horizontal list-unstyled">
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                    <li>Lorem ipsum dolor sit amet</li>
                                </ul>
                            </div> --><!-- /.col-xl-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="carousel-container mt-90">
                            <div class="slick-carousel" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "infinite":false, "arrows": false, "dots": false, "responsive": [{"breakpoint": 1200, "settings": {"slidesToShow": 3}}, {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 2}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
                                <!-- process item #1 -->
                                <?php if (!empty($classes)) {
                                    foreach ($classes as $key => $value) { ?>

                                        <div class="process-item text-center">
                                            <div class="program-preview mb-30">
                                                <img src="<?php echo S3_BUCKET_ROOT . CLASS_IMAGE . $value['class_image']; ?>" title="program">
                                            </div>
                                            <a href="<?php echo base_url(); ?>login">
                                                <h4 class="process__title less-padding-df pr-0"><?php echo $value['program_title']; ?></h4>
                                                <div class="d-flex justify-content-around">
                                                    <p class="spots-badge"><?php $booked_spots = $this->db->select('*')->from('tbl_order')->where('item_type', 'class')->where('item_id', $value['id'])->get()->num_rows();
                                                                            $remaining_spot = $value['total_spot'] - $booked_spots; ?>
                                                        <?php echo $remaining_spot . '/' . $value['total_spot']; ?> spots</p>
                                                    <!-- <a href="<?php echo base_url(); ?>/login"> -->
                                                    <p class="spots-price"><?php echo $this->session->userdata('currency') . ' ' . $value['price']; ?></p>
                                                    <!-- </a> -->
                                                </div>
                                            </a>
                                            <p class="process__desc"><?php $description_length = strlen(@$value['description']);
                                                                        if ($description_length > 30) {
                                                                            echo substr(@$value['description'], 0, 30 - $description_length) . "... <a href='javascript:void(0)'>Read More</a>";
                                                                        } else {
                                                                            echo @$value['description'];
                                                                        }  ?></p>
                                            <a href="#" class="btn btn__secondary btn__link small-custom-font">
                                                <span><i class="far fa-calendar-alt"></i> <?php echo $this->common_model->date_convert($value['start_datetime'], 'd M, Y', $this->session->userdata('website_timezone')) . ' - ', $this->common_model->date_convert($value['end_datetime'], 'd M, Y', $this->session->userdata('website_timezone')); ?></span>
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div><!-- /.process-item -->
                                <?php }
                                } ?>


                            </div><!-- /.carousel -->
                        </div>
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="cta bg-primary">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-2 col-lg-2 res-img-fix">
                            <img src="<?php echo base_url(); ?>website_assets/images/icons/data.png" alt="alert">
                        </div><!-- /.col-lg-2 -->
                        <div class="col-sm-12 col-md-7 col-lg-7 text-center">
                            <h4 class="cta__title">My Evaluations</h4>
                            <p class="cta__desc">Build confidence one evaluation at a time</p>
                        </div><!-- /.col-lg-7 -->
                        <div class="col-sm-12 col-md-3 col-lg-3 text-center">
                            <a href="<?php echo base_url(); ?>login" class="btn btn__secondary btn__secondary-style2 btn__rounded mr-30">
                                <span>Evaluate my speech</span>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div><!-- /.col-lg-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.cta -->
        </section><!-- /.Work Process -->

        <!-- ======================
                Team
                ========================= -->

        <section class="team-layout2 pb-80 overflow-hidden">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                        <div class="heading text-center mb-40">
                            <h3 class="heading__title">Upcoming Contests</h3>
                            <!--  <p class="heading__desc">Our administration and support staff all have exceptional people skills and
                                        trained to assist you with all medical enquiries.
                                    </p> -->
                            <p class="heading__desc">Click below to see more details about upcoming contests
                            </p>
                        </div><!-- /.heading -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="slick-carousel" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "autoplay": true, "arrows": false, "dots": false, "responsive": [ {"breakpoint": 992, "settings": {"slidesToShow": 2}}, {"breakpoint": 767, "settings": {"slidesToShow": 1}}, {"breakpoint": 480, "settings": {"slidesToShow": 1}}]}'>
                            <?php if (!empty($contests)) {
                                foreach ($contests as $key => $value) { ?>
                                    <div class="member">
                                        <div class="member__img">
                                            <?php $image_name = explode(".", $value['contest_image']);
                                            if ($image_name[1] == 'mp4' || $image_name[1] == 'mp3') { ?>
                                                <!-- <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" alt="member img"> -->
                                                <!--  <video width="400px" height="300px" controls > <source src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" > Your browser does not support HTML5 video. </video> -->
                                                <img width="400px" height="300px" src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" alt="member img">
                                            <?php } else { ?>
                                                <img width="400px" height="300px" src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" alt="member img">
                                            <?php  }
                                            ?>

                                        </div>
                                        <div class="member__info">
                                            <h5 class="member__name"><a href="<?php echo base_url(); ?>/login"><?php echo $value['name']; ?></a></h5>
                                            <?php if ($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])) { ?>
                                                <p class="member__job"><img src="<?php echo $value['sponsors']['sponsor_logo']; ?>" class="round-profile-img"> Sponser by <?php echo $value['sponsors']['sponsor_name']; ?></p>
                                            <?php } else { ?>
                                                <p class="member__job"> Sponser by </p>
                                            <?php   } ?>
                                            <div class="mt-20 d-flex flex-wrap justify-content-between align-items-center">
                                                <a href="<?php echo base_url(); ?>login" class="btn btn__secondary btn__link btn__rounded btn-live">
                                                    <span><i class="far fa-play-circle"></i> <?php echo ucfirst($value['contest_type']); ?></span>
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                                <ul class="social-icons list-unstyled mb-0">
                                                    <li><a href="<?php echo base_url(); ?>login" class="content-charge"> <?php echo $this->session->userdata('currency') . ' ' . $value['price']; ?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            <?php }
                            } ?>
                        </div><!-- /.carousel -->
                    </div><!-- /.col-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.Team -->

        <!-- ========================
                gallery
                =========================== -->


        <!--start modal-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video1" class="w-100 res-ad-video">
                            <source src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[3]['video']; ?>" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal2-->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video2" class="w-100 res-ad-video">
                            <source src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[2]['video']; ?>" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal3-->
        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video3" class="w-100 res-ad-video">
                            <source src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[1]['video']; ?>" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal4-->
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video4" class="w-100 res-ad-video">
                            <source src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[0]['video']; ?>" type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->
        <?php include("inc/footer.php"); ?><!-- /.Footer -->
    </div><!-- /.wrapper -->
    <?php include("inc/script.php"); ?>
    <script>
        $('#myModal').on('shown.bs.modal', function() {
            $('#video1')[0].play();
        })
        $('#myModal').on('hidden.bs.modal', function() {
            $('#video1')[0].pause();
        })

        $('#myModal1').on('shown.bs.modal', function() {
            $('#video2')[0].play();
        })
        $('#myModal1').on('hidden.bs.modal', function() {
            $('#video2')[0].pause();
        })

        $('#myModal2').on('shown.bs.modal', function() {
            $('#video3')[0].play();
        })
        $('#myModal2').on('hidden.bs.modal', function() {
            $('#video3')[0].pause();
        })

        $('#myModal3').on('shown.bs.modal', function() {
            $('#video4')[0].play();
        })
        $('#myModal3').on('hidden.bs.modal', function() {
            $('#video4')[0].pause();
        })
    </script>
</body>

</html>