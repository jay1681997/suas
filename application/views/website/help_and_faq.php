<!DOCTYPE html>
<html lang="en">
    <head>
      <?php  include("inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <!--start-body-->
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Help and FAQ</li>
                            </ol>
                        </nav>
                        <div class="content">
                            <div class="panels-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <?php if(!empty($help_and_faq)) { foreach ($help_and_faq as $key => $value) { ?>
                                    <div class="panels panels-default">
                                        <div class="panels-heading" id="headingOne<?php echo $key; ?>" role="tab">
                                            <h5 class="panels-title py-2 px-3 bg-green theme-rounded ">
                                                <a role="button" data-toggle="collapse" data-paren text-whitet="#accordion" href="#collapseOne<?php echo $key; ?>" aria-expanded="true" aria-controls="collapseOne<?php echo $key; ?>" class="d-flex justify-content-between text-white align-items-center"><?php echo $value['question']; ?><i class="fa fa-plus"></i>
                                                </a>
                                            </h5>
                                        </div>
                                        <div class="panel-collapse collapse px-4 in" id="collapseOne<?php echo $key; ?>" role="tabpanel" aria-labelledby="headingOne<?php echo $key; ?>" aria-expanded="true" style="">
                                            <div class="panel-body">
                                                <p class="fz-16"><?php echo $value['answer']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php }} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php  include("inc/footer.php");?>
        </div><!-- /.wrapper -->
    <?php  include("inc/script.php");?>
    </body>
</html>