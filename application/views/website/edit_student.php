<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
           <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <style type="text/css">
                    ::-webkit-file-upload-button {
    cursor: pointer;
}

img#imagePreview {
    width: 85px;
    height: 85px;
    overflow: hidden;
    border-radius: 50%;
    object-fit: cover;
}
    </style>
    <body>
        <div class="wrapper">
        <?php  include("inc/header.php");?>
        <section class="otp-verif contact-panel px-2 px-lg-4 py-4">
            <div class="container">
                <div class="heading pt-4">
                    <div class="title">
                        <center><h4 class="line mb-0 pb-20 position-relative">Edit Student's Profile</h4>
                        <p class="mt-0 mb-4 pt-20 fz-16">Fill out the information below</p></center>
                    </div>
                  
                    <!--start-form-->
                    <?php if($this->session->flashdata('success_msg')){ ?>
                        <div class="alert alert-success" >
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg')?>
                        </div>                
                    <?php } ?>
                    <?php if($this->session->flashdata('error_msg')){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg')?>
                        </div>
                    <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>
                    <?php echo form_open('edit-student/'.base64_encode($result['id']), array('method' => 'post', 'enctype'=>'multipart/form-data', 'class'=>'my-5')); ?>
                    
                        <div id="dropZoon" class="upload-area__drop-zoon drop-zoon border-0 profile-edit h-auto">
                           

                            <span class="drop-zoon__icon edit">
                                 <input type="file" id="profile_image" class="file-upload-input" name="profile_image" accept="image/*" parsley-trigger="change" onchange="showPreview(event);">
                                <img alt="user-profile" id="imagePreview" src="<?php echo S3_BUCKET_ROOT.STUDENT_IMAGE.$result['profile_image']; ?>">
                                <div class="edit-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                        <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"></path>
                                    </svg>
                                </div>
                            </span>
                            <span id="loadingText" class="drop-zoon__loading-text">Please Wait</span>
                            <img src="" alt="Preview Image" id="previewImage" class="drop-zoon__preview-image" draggable="false">
                        </div>
                        <?php echo form_error('profile_image'); ?>
                        <div id="profile_imageeerror"></div>
                        <!--end-profile-->
                        <div class="mt-4">
                            <div class="form-group">
                                <i class="form-group-icon icon-user text-success"></i>
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    id="firstname"
                                    name="firstname"
                                    placeholder="First name" 
                                    aria-required="true"
                                    parsley-trigger="change" 
                                    data-parsley-required-message="Please enter first name" 
                                    required 
                                    data-parsley-pattern="^[a-zA-Z ]+$" 
                                    data-parsley-pattern-message="Please enter valid first name" data-parsley-maxlength="32" 
                                    value="<?php echo !empty(set_value('firstname')) ? set_value('firstname') : $result['firstname']; ?>"
                                >
                                <?php echo form_error('firstname'); ?>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group">
                                <i class="form-group-icon icon-user text-success"></i>
                                <input 
                                    type="text" 
                                    class="form-control"
                                    id="lastname" 
                                    name="lastname" 
                                    placeholder="Last name" 
                                    aria-required="true"
                                    parsley-trigger="change"
                                    data-parsley-required-message="Please enter last name" 
                                    required 
                                    data-parsley-pattern="^[a-zA-Z ]+$" 
                                    data-parsley-pattern-message="Please enter valid last name" data-parsley-maxlength="32" 
                                    value="<?php echo !empty(set_value('lastname')) ? set_value('lastname') : $result['lastname']; ?>"
                                >
                                <?php echo form_error('lastname'); ?>
                            </div>
                        </div>
                          
                        <div class="">
                           <div class="form-group">
                                <i class="icon-calendar form-group-icon text-success"></i>
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    id="dob" 
                                    name="dob"
                                    placeholder="DOB(YYYY/MM/DD)" 
                                    aria-required="true"  
                                    data-parsley-required-message="Please select date of birth"  
                                    required 
                                    data-parsley-minlength="4" 
                                    data-parsley-maxlength="64"
                                     value="<?php echo !empty(set_value('dob')) ? set_value('dob') : date('m/d/Y', strtotime($result['dob'])); ?>" 
                                >
                          
                                <?php echo form_error('dob'); ?>
                            </div>
                        </div>
                       
                        <div class="">
                            <div class="form-group">
                                <i class="form-group-icon icon-user text-success"></i>
                                <?php if($result['age_category'] == 'Primary'){
                                    $age_range = '(0-8)';
                                }else if($result['age_category'] == 'Juniors'){
                                    $age_range = '(9-12)';
                                }else if($result['age_category'] == 'Teens'){
                                    $age_range = '(13-17)';
                                }else if($result['age_category'] == 'Adults'){
                                    $age_range = '(18+)';
                                }  ?>
                                <input type="text" class="form-control" placeholder="Teens (13-17 )" id="age_category" name="age_category" aria-required="true" disabled value="<?php echo !empty(set_value('age_category')) ? set_value('age_category') : $result['age_category'].' '.$age_range; ?>">
                                <span class="icon-alert teens-div" data-toggle="modal" data-target="#teens-info"></span>
                            </div>
                        </div>
                    
                        <!--start-video-->
                        <div class=" pt-4 text-left">
                            <h4>Upload a video of Student</h4>
                            <span class="font-weight-bold mb-2 d-block">Instructions:</span>
                            <ul class="pl-20">
                                <li><p class="mb-0">State your full name and age.</p></li>
                                <li><p class="mb-0">State the country you live in.</p></li>
                            </ul>
                            <div class="file-upload">
                                <div class="image-upload-wrap">
                                    <input type="file" id="fileInput" class="file-upload-input" name="video" accept="video/*" parsley-trigger="change" onchange="readURL(this);">

                                    <div class="drag-text">
                                        <span class="  d-block">
                                            <video controls class="file-upload-image"> 
                                                <source src="<?php echo S3_BUCKET_ROOT.STUDENT_IMAGE.$result['profile_video']; ?>"> Your browser does not support HTML5 video. 
                                            </video>
                                        </span>
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <video width="300" controls class="file-upload-image" > 
                                        <source src="#"> Your browser does not support HTML5 video. 
                                    </video>
                                    <div class="image-title-wrap">
                                 
                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                            </svg> 
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_error('video'); ?>
                            <div id="videoeerror"></div>
                        </div>
                        <!--end-video-->
                        <div class="button mt-4">
                            <button type="submit" class="btn btn__primary btn__rounded w-100 fz-16 mt-3 btn-animated-bl">Save</button>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </section>

        <!--start-modal-->
        <div class="modal fade" id="teens-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                        <div class="content">
                            <h4>Age Group</h4>
                            <p class="fz-16 text-dark">Student will be placed in the <b id="category_student">Junior</b> Category.</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <button type="button" class="btn btn__primary btn__rounded" data-dismiss="modal" aria-label="Close">Continue</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->  

        <!--start-modal-->
        <div class="modal fade" id="limit-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                        <div class="content">
                            <h4>Student Limit Reached</h4>
                            <p class="fz-16 text-dark">You can add maximum 5 students to your profile</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <button type="button" class="btn btn__rounded" data-dismiss="modal" aria-label="Close" style="color: #ffffff;background-color: #EA241A;">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->      

        <!--start-modal-->
        <div class="modal fade" id="remove-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                        <div class="content">
                            <h4>Delete Student's Profile</h4>
                            <p class="fz-16 text-dark">All videos will be permanently deleted. Do you still want to delete student's profile?</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <button type="button" class="btn btn__rounded" data-dismiss="modal" aria-label="Close" style="">No</button>
                        <a href="javascript:void(0)" onclick="remove_student();" class="btn btn__rounded" style="color: #ffffff;background-color: #EA241A;"><p id="append_id" style="display: none;"></p>Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->    

        <?php  include("inc/footer.php");?>
        </div>
        <?php  include("inc/script.php");?>
           <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script>
            $(document).ready(function(){
                <?php if(!empty($this->session->userdata('error_message'))) { ?>
                    $('#limit-info').modal('show');
                <?php } ?>
                $("#dob").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "Y/m/d",
                    disableMobile: "true",
                    maxDate: new Date()
                });

            

                $("#dob").change(function(){
                    var age_category = '';
                    var today = new Date();
                    var birthDate = new Date(this.value);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                
                    console.log(age)
                    if(age >= 0 && age <= 8){
                       age_category = 'Primary (0-8)';
                    }else if(age >= 9 && age <= 12){
                        age_category = 'Juniors (9-12)';
                    }else if(age >= 13 && age <= 17){
                        age_category = 'Teens (13-17)';
                    }else if(age >= 18){
                        age_category = 'Adults (18+)';
                    }
                    age_category.age = age;

                    $('#age_category').val(age_category);
                    $('#category_student').html(age_category);
                    console.log(age_category)
                })
            });

            function readURL(input) {
                if (input.files && input.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function(e) {
                      $('.image-upload-wrap').hide();

                      $('.file-upload-image').attr('src', e.target.result);
                      $('.file-upload-content').show();

                      $('.image-title').html(input.files[0].name);
                    };

                    reader.readAsDataURL(input.files[0]);

                } else {
                    removeUpload();
                }
            }

            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });

            function showPreview(event){
                if(event.target.files.length > 0){
                    var src = URL.createObjectURL(event.target.files[0]);
                    var preview = document.getElementById("imagePreview");
                    preview.src = src;
                    preview.style.display = "block";
                    preview.style.borderradius = "50%";
                }
            }

            $('.romove_modal').click(function(){
                $('#remove-info').modal('show');
                var student_id = $(this).data('id');
                $('#append_id').html(student_id);
            })

            function remove_student(){
                var student_id = $('#append_id').html();
                console.log(student_id)
                $.ajax({
                    url:  '<?php echo base_url(); ?>website/delete_student/'+student_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        swal("Success",message,"success");
                        window.location.href = '<?php echo base_url(); ?>add-student/';
                    }
                }); // END OF AJAX CALL
            }
        </script>
    </body>
</html>