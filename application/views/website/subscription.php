<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Subscriptions</li>
                            </ol>
                        </nav>
                        <div class="title mt-4 mb-4">
                            <h4 class="mb-0 position-relative">Subscriptions</h4>
                            <p class="fz-16 mt-0">Lorem ipsum dolor sit amet, consectetur adipiscig elit. Phasellus blandit nisl eget mollis lacinia. In con maximus.</p>
                        </div>
                        <div class="mb-4">
                            <a href="<?php echo base_url(); ?>subscription-active" class="btn btn__primary btn__outlined btn__rounded">My Active Subscriptions</a>
                        </div>
                        <?php if(!empty($subscription)) { 
                            ?>
                        <?php echo form_open('customer/user/add_item_intocart/subscription', array('method' => 'post')); ?>
                        <div class="select-plan">
                            <!-- <h5>AI Evaluation</h5> -->
                            <div class="row">
                                <?php foreach ($subscription as $key => $value) {
                                    if($value['evaluation_type'] == 'AI'){ ?>
                                        <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                            <div class="add-adres" onclick="not_disabled();">
                                                <div class="add-deta shadow theme-rounded"> 
                                                    <label class="custom-mine d-flex">
                                                        <div class="checkbox_add">
                                                            <h4><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                            <p class="fz-16"><?php echo $value['total_video']; ?> Videos Evaluations</p>
                                                            <span class="d-block mb-3">Validity - <?php echo $value['validity'].' '.ucfirst($value['duration']); ?></span>
                                                            <p><?php echo $value['description']; ?></p>
                                                            
                                                        </div>
                                                        <input type="radio" name="subscription_id" class="ml-auto option-input radio" checked value="<?php echo $value['id']; ?>">
                                                    </label> 
                                                </div>
                                            </div>
                                        </div>
                                <?php }} ?>
                            </div>
                        </div>
                        <div class="select-plan mt-30">
                            <h5>Manual Evaluation</h5>
                            <div class="row">
                                <?php foreach ($subscription as $key => $value) {
                                    if($value['evaluation_type'] == 'Manual'){ ?>
                                        <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                            <div class="add-adres" onclick="not_disabled();">
                                                <div class="add-deta shadow theme-rounded"> 
                                                    <label class="custom-mine d-flex">
                                                        <div class="checkbox_add">
                                                            <h4><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                            <p class="fz-16"><?php echo $value['total_video']; ?> Videos Evaluations</p>
                                                            <span class="d-block mb-3">Validity - <?php echo $value['validity'].' '.ucfirst($value['duration']); ?></span>
                                                            <p><?php echo $value['description']; ?></p>
                                                        </div>
                                                        <input type="radio" name="subscription_id" class="ml-auto option-input radio" value="<?php echo $value['id']; ?>">
                                                    </label> 
                                                </div>
                                            </div>
                                        </div>
                                <?php }} ?>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mb-4 mt-4">
                            <button type="submit" id="continue_button" name="continue_button" class="btn btn__primary btn__rounded" >Continue</button>
                        </div>
                        <?php echo form_close(); ?>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <?php  include("inc/footer.php");?>
        </div>
        <?php  include("inc/script.php");?>
        <script type="text/javascript">
            $(function () {

                $(".checkbox_add").click(function () {
                    $("input:radio").attr("checked", false);
                });

            });
            window.onload = function() {
                // $("#continue_button").
                document.getElementById("continue_button").disabled = true;
            };
            function not_disabled(){
                 document.getElementById("continue_button").disabled = false;
            }
        </script>
    </body>
</html>