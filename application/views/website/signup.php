<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
           <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .parsley-required{
                font-weight: 300;
            }
.crn-numbers .select2-container {
    width: 40% !important;
}
        
.crn-numbers .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 35%;
}

.crn-numbers .select2-container--default .select2-selection--single {
    border-radius: 30px 0 0 30px;
    border-right: none !important;
    border: 2px solid #e6e8eb;
}


           .crn-numbers1 .select2-container {
    width: 100% !important;
}
        
.crn-numbers1 .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers1 .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 10%;
}

.crn-numbers1 .select2-container--default .select2-selection--single {
    border-radius: 30px 30px 30px 30px;
    /*border-right: none !important;*/
    border: 2px solid #e6e8eb;
}


.select2-container--open .select2-dropdown {
    left: 0;
    width: 400px !important;
}


        </style>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <section class="about-layout4 pb-0">
                <div class="container">
                    <div class="sign-up-pr">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-12 col-lg-6 col-md-6 ">
                                <div class="signin-process-left singup-form">
                                    <img src="<?php echo base_url(); ?>website_assets/images/register.jpg" alt="login" title="login process">
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-md-6">
                                <div class="rgt-sign-in ">
                                    <div class="heading">
                                        <h2 class="title">Sign Up</h2>
                                        <P>Fill in your information below.</P>
                                    </div><!-- End .heading -->

                                    <?php echo form_open('sign-up', array('method' => 'post')); ?>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="form-group-icon icon-user text-success"></i>
                                                <input 
                                                    type="text" 
                                                    class="form-control" 
                                                    id="firstname"
                                                    name="firstname"
                                                    placeholder="First name" 
                                                    aria-required="true"
                                                    parsley-trigger="change" 
                                                    data-parsley-required-message="Please enter first name" 
                                                    required 
                                                    data-parsley-pattern="^[a-zA-Z ]+$" 
                                                    maxlength="22" 
                                                    data-parsley-pattern-message="Please enter valid first name" data-parsley-maxlength="22" 
                                                    value="<?php echo set_value('firstname') ?>"
                                                onkeyup="validate_firstname();">
                                                <?php echo form_error('firstname'); ?>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="form-group-icon icon-user text-success"></i>
                                                <input 
                                                    type="text" 
                                                    class="form-control"
                                                    id="lastname" 
                                                    name="lastname" 
                                                    placeholder="Last name" 
                                                    aria-required="true"
                                                    parsley-trigger="change"
                                                    data-parsley-required-message="Please enter last name" 
                                                    required 
                                                    maxlength="22"
                                                    data-parsley-pattern="^[a-zA-Z ]+$" 
                                                    data-parsley-pattern-message="Please enter valid last name" data-parsley-maxlength="22" 
                                                    value="<?php echo set_value('lastname') ?>"
                                                    onkeyup="validate_lastname();"
                                                >
                                                <?php echo form_error('lastname'); ?>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="icon-email form-group-icon text-success"></i>
                                                <input 
                                                    type="email" 
                                                    class="form-control" 
                                                    id="email" 
                                                    name="email" 
                                                    placeholder="Email" 
                                                    aria-required="true" 
                                                    parsley-trigger="change" 
                                                    data-parsley-required-message="Please enter email" 
                                                    required 
                                                    data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  
                                                    data-parsley-pattern-message="Please enter valid email address" 
                                                    data-parsley-maxlength="64"

                                                    value="<?php echo isset($email) ? $email : set_value('email'); ?>"
                                                >
                                                <?php echo form_error('email'); ?>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="icon-phone form-group-icon text-success"></i>
                                                <div class="input-group mb-3 mb-md-4 mb-lg-3 d-flex crn-numbers">
                                                    <select class="form-select form-control imageselect2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:140px;padding-left:40px">
                                            <!-- <option value="+1">+1</option> -->
                                            <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                            <?php if (!empty($countries)) {
                                                foreach ($countries as $key => $value) { ?>
                                                    <!--  <option value="<?php echo $value['calling_code']; ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['calling_code']; ?></option> -->
                                                    <option value="<?php echo $value['calling_code']; ?>" data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'] . '(' . $value['calling_code'] . ')'; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                                    <?php echo form_error('country_code'); ?>
                                                    <label id="countrieseerror"></label>
                                                  <input 
                                                        type="text" 
                                                        class="form-control phone-format" 
                                                        id="phone" 
                                                        name="phone" 
                                                        aria-required="true" 
                                                        parsley-trigger="change" 
                                                        data-parsley-errors-container="#phoneerror"
                                                        data-parsley-required-message="Please enter phone number"  
                                                        required 
                                                        
                                                        value="<?php echo set_value('phone'); ?>"
                                                    >
                                                    <!-- <input 
                                                        type="text" 
                                                        class="form-control" 
                                                        id="phone" 
                                                        name="phone" 
                                                        placeholder="Phone Number" 
                                                        aria-required="true" 
                                                        parsley-trigger="change" 
                                                        data-parsley-type="number" 
                                                        data-parsley-errors-container="#phoneerror"
                                                        data-parsley-required-message="Please enter phone number"  
                                                        required 
                                                        onkeypress="return isNumberKey(event);"  
                                                        maxlength="12" 
                                                        data-parsley-minlength="8" 
                                                        data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" 
                                                        value="<?php echo set_value('phone'); ?>"
                                                    > -->
                                                    <?php echo form_error('phone'); ?>
                                                </div>
                                            <div id="phoneerror"></div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="icon-calendar form-group-icon text-success"></i>
                                                <input 
                                                    type="text" 
                                                    class="form-control " 
                                                    id="dob" 
                                                    name="dob"
                                                    placeholder="DOB(MM/DD/YYYY)" 
                                                    aria-required="true"  
                                                    data-parsley-required-message="Please select date of birth"  
                                                    required 
                                                    data-parsley-minlength="4" 
                                                    data-parsley-maxlength="64"
                                                    value="<?php echo set_value('dob') ?>" 
                                                >
                                                <span class="icon-alert alrt-div" data-toggle="modal" data-target="#alert-info" style="right:19px !important"></span>
                                                <?php echo form_error('dob'); ?>
                                            </div>
                                        </div>
                                        <div class="crn-numbers1">
                                            <div class="form-group">
                                                <i class="icon-location text-success form-group-icon"></i>
                                                <select  
                                                    class="form-control imageselect3" 
                                                    id="country" 
                                                    name="country" 
                                                    data-parsley-trigger="change" 
                                                    data-parsley-errors-container="#code_errror"  
                                                    data-parsley-required-message="Please select country" 
                                                    required
                                                    onchange="region_list(this.value);"
                                                >

                                                    <option value="">Select Country </option>
                                                    <?php if(!empty($countries)){   
                                                        foreach ($countries as $key => $value){ ?>
                                                            <option data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" value="<?php echo $value['name']; ?>" <?php echo set_select('country',$value['name']) ?>> <?php echo $value['name']; ?> </option>
                                                    <?php } } ?>

                                                </select>
                                                <?php echo form_error('country'); ?>
                                                <div id="code_errror" style="margin-top: -5px !important"></div>
                                            </div>
                                        </div>
                                          <div class="crn-numbers1">
                                            <div class="form-group">
                                                  <i class="icon-location text-success form-group-icon"></i>
                                                    <select name="region_id" id="region_id" data-parsley-errors-container="#errormsg_region_list" class="form-control select2" data-parsley-trigger="change" required data-parsley-required-message="Please select region name">
                                                        <option value="">Region List</option>
                                                    </select>
                                                    <?php echo form_error('errormsg_region_list'); ?>
                                                    <label id="errormsg_region_list"></label>
                                                </div>
                                            </div>
                                        <div class="">
                                            <div class="form-group w-100 d-inline-block">
                                                <i class="form-group-icon icon-widget text-success"></i>
                                                <input 
                                                    type="text" 
                                                    class="form-control" 
                                                    id="Promo-Code" 
                                                    name="promocode"
                                                    placeholder="Link a Promo Code" 
                                                    aria-required="true"
                                                >
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="form-group">
                                                <i class="form-group-icon text-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                                        <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
                                                    </svg>
                                                </i>
                                                <input 
                                                    type="password" 
                                                    class="form-control"
                                                    id="password" 
                                                    name="password" 
                                                    placeholder="Password" 
                                                    aria-required="true"
                                                    data-parsley-trigger="change"   
                                                    data-parsley-required-message="Please enter password" 
                                                    required
                                                    data-parsley-errors-container="#passwordd" 
                                                    data-parsley-pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&£€^(){}:'|<>]).*"   
                                                    data-parsley-pattern-message="Please enter atleast 8 characters length with 1 upper case, 1 lower case, 1 number and 1 special character" 
                                                    required 
                                                    data-parsley-maxlength="64" 
                                                    data-parsley-minlength="8"
                                                    data-toggle="tooltip" 
                                                    data-placement="top" 
                                                >
                                                <span toggle="#password" class="fa fa-eye fa-eye-slash field-icon toggle-password alrt-div" style="top: 44% !important;right: 19px !important;"></span>
                                            </div>
                                        </div>
                                        <div class="">
                                            <p>Note: At least 8 characters, one letter, one capital letter and one number</p>
                                            <div class="form-check mb-3">
                                                <input 
                                                style="cursor: pointer"
                                                    type="checkbox"
                                                    class="form-check-input" 
                                                    id="flexCheckChecked"
                                                    data-parsley-required-message="Please select agree to terms & privacy policy"required 
                                                    data-parsley-errors-container="#agreeeerror" 
                                                    checked
                                                >
                                                <label   class="form-check-label" for="flexCheckChecked">
                                                    Agree to <a  href="<?php echo base_url(); ?>terms-conditions">Terms</a> & <a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a>
                                                </label>
                                                <div id="agreeeerror" style="margin-top: -5px !important;  margin-bottom: 30px !important;"></div>
                                            </div>
                                            <div class="form-check">
                                                <input 
                                                style="cursor: pointer"
                                                    type="checkbox"
                                                    class="form-check-input" 
                                                    id="flexCheckCheckeds" 
                                                    data-parsley-required-message="Please select permission to use my videos for promotional purposes" 
                                                    required 
                                                    data-parsley-errors-container="#permissioneerror" 
                                                    checked
                                                >
                                                <label  class="form-check-label" for="flexCheckCheckeds">
                                                    I give permission to use my videos for promotional purposes.
                                                </label>
                                                <div id="permissioneerror" style="margin-top: -5px !important; margin-bottom: 30px !important;"></div>
                                            </div>

                                            <input type="hidden" name="latitude" id="latitude" value="0.0">
                                            <input type="hidden" name="longitude" id="longitude" value="0.0">
                                            <input type="hidden" name="timezone" id="timezone" value="">

                                            <button type="submit" class="btn-animated-bl" style="margin-left:35%">Sign up</button>
                                        </div>
                                    <?php echo form_close(); ?>

                                    <div class="middle-or mb-5">
                                        <p>Or</p>
                                    </div>
                                    
                                    <div class="row omb_login mt-5">
                                        <div class="register-link">
                                            <p class="font-weight-bold">Have an account? <a href="<?php echo base_url(); ?>login">Sign In!</a></p> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--start-modal-->
            <div class="modal fade" id="alert-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                            <div class="content">
                                <h4>Why DOB Is Required</h4>
                                <p class="fz-16 text-dark">Classes and contests are categorized into 4 age groups. Primary, Junior, Teens and Adults.</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                         
                            <button type="button" class="btn btn__primary btn__rounded btn-animated-bl"  data-dismiss="modal" aria-label="Close">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->      
        <?php  include("inc/footer.php");?>

        </div>

        <?php  include("inc/script.php");?>
           <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jstz.min.js"></script>
          <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
           function UserTemplate (user) {
    if (!user.id) {
      return user.text;
    }
    var $user = $(
      '<span><img src="'+$(user.element).data('profileimage')+'" width="20px" height="20px" class="img-flag"/> '+user.text+'</span>'
    );
    return $user;
  };
            $(document).ready(function(){
                $('select').niceSelect('destroy');
                $('.select2').select2({"width":"100%"}); 
                $("#dob").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "m/d/Y",
                    disableMobile: "true",
                    maxDate: new Date()
                });
                  $("#country_code").select2({
                "width": "100%",
            });
               $("#country").select2({
                "width": "100%",
            });   
            $(".imageselect2").select2({
                "width": "100%",
                templateResult: UserTemplate,
                templateSelection: UserTemplate
            });
              $(".imageselect3").select2({
                "width": "100%",
                templateResult: UserTemplate,
                templateSelection: UserTemplate
            });
                var timezone = jstz.determine();
                    $('#timezone').val(timezone.name());
                });

            $(".toggle-password").click(function() {
                $(this).toggleClass("fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
             $(document).ready(function() {
            /***phone number format***/
            $(".phone-format").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("" + curval + "" + "-");
                } else if (curchr == 3 && curval.indexOf("(") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 3 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 7) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });

             function validate_firstname() {
  var element = document.getElementById('firstname');
  element.value = element.value.replace(/[^a-zA-Z@]+/, '');
};
          function validate_lastname() {
  var element = document.getElementById('lastname');
  element.value = element.value.replace(/[^a-zA-Z@]+/, '');
};

 function region_list(country_id) {

            $.ajax({
                url: "<?php echo base_url() . 'website/region_list/'; ?>" + country_id,
                type: 'GET',
                dataType: 'json',
                error: function(err) {
                    $('#region_id').html('<option value="">Select Region</option>');
                },
                success: function(response) {
                    if (response == "") {
                        $('#region_id').html('<option value="">Select Region List</option>');
                    } else {
                        var statehtml = "";
                        var statehtml = "<option value=''>Select Region List</option>";
                        $.each(response, function(index, value) {
                            var slectedstr = '';
                            if (value.id == "<?php echo (!empty(set_value('region_id')) ? set_value('region_id') : ''); ?>") {
                                slectedstr = "selected='selected'";
                            }
                            statehtml += "<option value='" + value.id + "' " + slectedstr + ">" + value.region + "</option>";
                        });
                        $('#region_id').html(statehtml);
                    }
                }
            });
        }

        </script>
    </body>
</html>