<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <body>
      <div class="wrapper">
        <?php  include("inc/header.php");?>
        <section class="">
            <div class="container">
                <div class="contact-panel px-4 py-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
            
               <div class="contents">
                    <p><?php echo $privacy_policy['privacy_policy']; ?></p>
                    </div>
           
                </div>
            </div>
        </section>
        <?php  include("inc/footer.php");?>
      </div>
      <?php  include("inc/script.php");?>
    </body>
</html>