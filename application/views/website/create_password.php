<!DOCTYPE html>
<?php echo $this->session->userdata('change_password'); ?>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <style type="text/css">
        .slick-slider .slick-list, .slick-slider .slick-track {
        padding-top: 0;
    }
    ul.parsley-errors-list li.parsley-required{
            margin-top: 10px !important;
            margin-bottom: 10px !important;
            text-align: left;
            margin-left: 30px !important;
           }
           .form-group{
            margin-bottom: 10px;
            margin-top: 10px !important;
           }
           .text-center{
            margin-left: 0 30px !important;
            text-align: center; !important;
           }
          /* ul.parsley-errors-list li.parsley-required{
            margin-left: 0 30px !important;
           }*/
    </style>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
            <!--start-body-->
            <section class="otp-verif contact-panel text-center">
                <div class="container">
                     <?php if($this->session->flashdata('success_msg')){ ?>
                        <div class="alert alert-success" >
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg')?>
                        </div>                
                    <?php } ?>
                    <?php if($this->session->flashdata('error_msg')){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg')?>
                        </div>
                    <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>
                    <img src="<?php echo base_url(); ?>website_assets/images/create-new-password.png" alt="create-new-password" class="mb-4">
                    <div class="title">
                        <h4 class="line mb-0 pb-20 position-relative">Create New Password</h4>
                        <P class="mt-0 mb-4 pt-20 fz-16">Your new password must be different from previous password.</P>
                        <p class="text-dark fz-16 mb-25">Note: Your password must contain at least 8 characters, one letter, one capital letter and one number.</p>
                    </div>
                    <?php echo form_open(base_url().'create-new-password',array('id'=>'changepasswordform')); ?>
                        <div class="form-group">
                            <i class="form-group-icon text-success">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                    <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
                                </svg>
                            </i>
                             <input 
                                type="password" 
                                class="form-control" 
                                id="password" 
                                name="password" 
                                placeholder="Password" 
                                aria-required="true"
                                data-parsley-trigger="change" 
                                data-parsley-minlength="8" 
                                data-parsley-errors-container="#changepassword_passworderror"
                                data-parsley-maxlength="64" 
                                data-parsley-pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&£€^(){}:'|<>]).*"   
                                data-parsley-pattern-message="Please enter atleast 8 characters length with 1 upper case, 1 lower case, 1 number and 1 special character" 
                                data-toggle="tooltip" 
                                data-placement="top"
                                data-parsley-required-message="Please enter password"
                                required 
                            >
                           <!--  <input 
                                type="password" 
                                class="form-control" 
                                id="password" 
                                name="password" 
                                placeholder="Password" 
                                aria-required="true"
                                data-parsley-trigger="change" 
                                data-parsley-minlength="4" 
                                data-parsley-errors-container="#changepassword_passworderror"
                                data-parsley-maxlength="64" 
                                data-parsley-pattern="^(?=.*[A-Za-z])(?=.*[$@$!%*#?&+\d])[^ ]{8,}$" 
                                data-parsley-pattern-message="Please enter valid password" 
                                data-toggle="tooltip" 
                                data-placement="top"
                                data-parsley-required-message="Please enter password"
                                required 
                            > -->
                            <i class="textbox-icon text-default fa fa-eye-slash password_icon" onclick="showpassword(this,'password');" style="top: 24px !important;right: 19px !important;position: absolute;"></i>
                            
                        </div>
                        <?php echo form_error('password'); ?>
                          <!--   <div id="changepassword_passworderror"></div> -->  <label id="changepassword_passworderror"></label>
                        <div class="form-group">
                            <i class="form-group-icon text-success">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                    <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
                                </svg>
                            </i>
                            
                            <input 
                                type="password" 
                                class="form-control" 
                                id="confirmpassword"
                                name="confirmpassword" 
                                placeholder="Confirm Password" 
                                aria-required="true"
                                data-parsley-trigger="change"
                                data-parsley-errors-container="#changepassword_confirmpassworderror"
                                data-parsley-minlength="8"
                                data-parsley-maxlength="64"
                                data-parsley-equalto="#password"
                                data-parsley-equalto-message="Password does not match"
                              data-parsley-pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&£€^(){}:'|<>]).*"   
                                data-parsley-pattern-message="Please enter atleast 8 characters length with 1 upper case, 1 lower case, 1 number and 1 special character" 
                                data-toggle="tooltip" 
                                data-placement="top" 
                                title="Confirm Password"
                                data-parsley-required-message="Please enter confirm password"
                                required>
                                <i class="textbox-icon text-default fa fa-eye-slash password_icon" onclick="showpassword(this,'confirmpassword');" style="top: 24px !important;right: 19px !important;position: absolute;"></i>
                            </div>
                            <!-- <div id="changepassword_confirmpassworderror"></div> -->
                        </div>
                      <label id="changepassword_confirmpassworderror"></label>
                        <button type="submit" class="btn btn__primary btn__rounded w-100 fz-16 mt-3" id="changepassword"> Update password</button>
                    <?php echo form_close(); ?>
                </div>
            </section>
            <!--end-body-->
      
            <!--start-modal-->
            <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content custom-radius">
                        <div class="modal-header border-bottom-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div> 
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/verification-done.png" alt="verification-dones" class="mb-20">
                            <div class="content">
                                <h4>Password Reset</h4>
                                <p class="fz-16">Your password has successfully been reset</p>
                                <a href="<?php echo base_url(); ?>login" class="btn btn__primary btn__rounded fz-16">Continue</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <!--end-modal-->
            <?php  include("inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include("inc/script.php");?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });

            function showpassword(elem,textboxid)
            {
                if($('#'+textboxid).attr('type')=='password') {
                    $(elem).removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    $('#'+textboxid).attr('type','text');
                } else {
                    $(elem).removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    $('#'+textboxid).attr('type','password');
                }
            }
        </script>
        <?php if($this->session->flashdata('success_msg')){ ?>
        <script type="text/javascript">
            
            $(document).ready(function() {
                $('#form').modal('show');
            });
        </script>
        <?php } ?>
    </body>
</html>