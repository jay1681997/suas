<!-- <div class="preloader">
  <div class="loading"><span></span><span></span><span></span><span></span></div>
</div> --><!-- /.preloader -->
<?php 
// $customer_data =  $this->session->userdata('website_userdata');
$customer_data =  $this->db->select("*")->from('tbl_user')->where("id",$this->session->userdata('user_id'))->get()->row_array();
$student_data =  $this->db->select("*")->from('tbl_student')->where("user_id",$this->session->userdata('user_id'))->get()->result_array();
// print_r($student_data);
if(!empty($this->session->userdata('user_id'))) {

    $notifications = $this->db->select("tn.*, tu.username, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image, TIMEDIFF(now(), tn.insert_datetime) as insert_timediff")->from('tbl_notification tn')->join('tbl_user tu','tn.sender_id = tu.id','left')->where('tn.is_deleted','0')->where('tn.is_read','unread')->where('tn.receiver_id',$this->session->userdata('user_id'))->order_by('tn.id','DESC')->get()->result_array(); ?>
    
  <style type="text/css">
        .badge {
            position: absolute;

            padding: 0px 0px 50px;
            /*border-radius: 50%;*/
            /*background: white;*/
            /*color: white;*/

        }

        .nice-select {
            width: 200px;
            height: 50px;
            top: 20%;
            line-height: 50px;
            /* padding-left: 10px;
            padding-right: 10px; */
        }
        @media screen and (min-width: 1440px) {
     .navbar .nav__item {
        margin-right: 2px;
    }
    .navbar .nav__item .nav__item-link {
        font-size: 15px;
    }
}
.nice-select {
    width: 190px;
}
.dropdown-item {letter-spacing: .4px;color: #0d204d;}
 .dropdown-item {
    font-size: 15px;
  font-weight: bold;
  text-transform: capitalize;
  display: block;
  position: relative;
  color: #213360;
  line-height: 20px;
  letter-spacing: .4px;
  padding-right: 15px;
}
.dropdown-menu.notification-ui_dd.show{
    margin-right: 2%;
}

    </style>
     <style>
        /* new submenu css */
        .rgt-submenu-icon {
            display: inline-block;
            float: right;
            clear: both;
        }

        ul.list-unstyled li.submenu {
            position: relative;
        }

        ul.list-unstyled li.submenu>ul {
            background-color: #fff;
            visibility: hidden;
            display: none;
            position: relative;
            left: unset;
            top: 0;
            transition: .3s;
            opacity: 0;
            padding: 10px 0;
            /* width: 150px; */

        }

        ul.list-unstyled li.submenu>ul li {
            list-style-type: none;
        }

        ul.list-unstyled li.submenu:hover>ul {
            visibility: visible;
            display: block;
            opacity: 1;
        }

        ul.list-unstyled:hover>ul {
            visibility: visible;
            opacity: 1;
        }

        /* ul.list-unstyled .submenu.atLeft > ul{
        left:-54%;
    } */
        ul.list-unstyled li.submenu>ul li {
            padding: .25rem 1.5rem;
        }

        ul.list-unstyled li.submenu>ul li:hover {
            color: #16181b;
            text-decoration: none;
            background-color: #f8f9fa;
        }

        ul.list-unstyled li.submenu>ul li a {
            color: #000;
        }

        .dropdown-menu.menus-dropdown {
            left: -210px;
            border-radius: 10px;
        }

        /* ul.list-unstyled li a{
        display:block;
        text-decoration:none;
        padding:8px;
        color:#000;
        transition:.1s;
        white-space:nowrap;
    }
    
    .dropdown a:hover,.dropdown .submenu:hover > a{
        background-color:mediumSeaGreen;
        color:#fff;
    } */
        @media (max-width: 767px) {
            ul.list-unstyled li.submenu>ul {
                position: relative;
                display: none;
            }

            ul.list-unstyled .submenu.atLeft>ul {
                left: unset;
            }

            ul.list-unstyled li.submenu:hover>ul {
                display: block;
            }
        }
    </style>
    <header class="header header-layout2">

        <nav class="navbar navbar-expand-lg sticky-navbar">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard">
                    <img src="<?php echo base_url(); ?>website_assets/images/logo/logo-green.png" class="green-logo" alt="logo">
                </a>
                <button class="navbar-toggler" type="button">
                    <span class="menu-lines"><span></span></span>
                </button>
                <div class="collapse navbar-collapse" id="mainNavigation">
                    <ul class="navbar-nav ml-auto">
                         <li class="nav__item">
                            <a href="<?php echo base_url(); ?>dashboard" class=" nav__item-link <?php echo ($this->uri->segment(1) == 'dashboard')?active:''; ?>">Home</a>
                        </li>
                        <li class="nav__item">
                            <a href="<?php echo base_url(); ?>upload-speeches" class=" nav__item-link <?php echo ($this->uri->segment(1) == 'upload-speeches')?active:''; ?>">Upload Speech </a>
                        </li>
                        <li class="nav__item">
                            <a href="<?php echo base_url(); ?>my-speeches" class=" nav__item-link <?php echo ($this->uri->segment(1) == 'my-speeches')?active:''; ?>">My Speeches </a>
                        </li>
                        <li class="nav__item">
                            <a href="<?php echo base_url(); ?>my-evaluations" class=" nav__item-link <?php echo ($this->uri->segment(1) == 'my-evaluations')?active:''; ?>">My Evaluations </a>
                        </li>
                        <li class="nav__item">
                            <a href="<?php echo base_url(); ?>online-class" class=" nav__item-link <?php echo ($this->uri->segment(1) == 'online-class')?active:''; ?>">Online Classes </a>
                        </li>
                          
                        <li class="nav__item">
                            <a href="<?php echo base_url(); ?>contest-list" class=" nav__item-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contests</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>contest-list">Live/Video</a>
                                <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>my-leaderboard">Leaderboard </a>
                                <!-- <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>sponsors">Sponsorships</a> -->
                            </div>
                        </li>
                       
                    </ul>
                    <button class="close-mobile-menu d-block d-lg-none"><i class="fas fa-times"></i></button>
                </div>
                <!-- <button class="action__btn-search ml-30"><i class="fa fa-search"></i></button> -->
                <div class="cart-btn header-icon  show-header-modal notification-bell" data-microtip-position="bottom" role="tooltip" aria-label="notification">
                    <i class="far fa-bell fz-16 text-dark ml-30" title="Notifications"></i>
                    <span class="cart-counter green-bg"> <span class="badge"><?php echo count($notifications); ?></span></span> 
                </div>
                <div class="dropdown-menu notification-ui_dd  " aria-labelledby="navbarDropdown">
                    <div class="notification-ui_dd-header">
                        <h5 class="text-center mb-0">Notifications</h5>
                    </div>
                    <?php if(!empty($notifications)){ ?>
                    <div class="notification-ui_dd-content">
                        <?php foreach ($notifications as $key => $value) { ?>
                        <div class="notification-list notification-list--unread">
                            <div class="notification-list_img">
                                <img src="<?php echo $value['profile_image']; ?>" alt="user">
                            </div>
                            <div class="notification-list_detail">
                                <?php if ($value['sender_type'] == 'admin') { ?>
                                            <p><b><?php echo "Admin"; ?></b> <?php echo $value['title']; ?></p>
                                            <!-- <p><small><?php echo $value['insert_timediff']; ?> hours ago</small></p> -->
                                            <p><small><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M Y h:m',$this->session->userdata('website_timezone')); ?> </small></p>
                                        <?php } else { ?>
                                            <p><b><?php echo $value['username']; ?></b> <?php echo $value['title']; ?></p>
                                            <!-- <p><small><?php echo $value['insert_timediff']; ?> hours ago</small></p> -->
                                        <p><small><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M Y h:m',$this->session->userdata('website_timezone')); ?></small></p>
                                        <?php } ?>
                           <!--      <p><b><?php echo $value['username']; ?></b> <?php echo $value['title']; ?></p>
                                <p><small><?php echo $value['insert_timediff']; ?> hours ago</small></p> -->
                            </div>
                            <div class="notification-list_feature-img">
                                <!-- <img src="https://i.imgur.com/AbZqFnR.jpg" alt="Feature image"> -->
                            </div>
                        </div>
                        <?php } ?>
                       
                    </div>
                    <div class="notification-ui_dd-footer text-center">
                        <a href="<?php echo base_url(); ?>notification"  class="btn-theme d-block">View All </a>
                    </div>
                    <?php }else{ ?>
                        <h6 class="mt-10"><center>Notification not found <a href="<?php echo base_url(); ?>notification"  class="btn-theme d-block">View All </a></center></h6>

                    <?php } ?>
                </div>
                <div class="list-unstyled ml-30">
                    <li class="dropdown">
                        <a class="rounded-circle" href="#" role="button" id="dropdownUser"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="avatar avatar-md avatar-indicators avatar-online">
                                <img alt="avatar" src="<?php echo S3_BUCKET_ROOT.USER_IMAGE.$customer_data['profile_image']; ?>" onerror="if (this.src != 'error.jpg') this.src = '<?php echo S3_BUCKET_ROOT.USER_IMAGE.'default.png'; ?>';" class="rounded-circle">
                            </div>
                        </a>
                        <div class="dropdown-menu menus-dropdown pb-2 pt-0 " aria-labelledby="dropdownUser">
                            <div class="dropdown-item">
                                <div class="d-flex py-2">
                                    <div class="avatar avatar-md avatar-indicators avatar-online">
                                        <img alt="avatar" src="<?php echo S3_BUCKET_ROOT.USER_IMAGE.$customer_data['profile_image']; ?>" onerror="if (this.src != 'error.jpg') this.src = '<?php echo S3_BUCKET_ROOT.USER_IMAGE.'default.png'; ?>';"class="rounded-circle">
                                    </div>
                                    <div class="ml-3 lh-1">
                                        <h6 class="mb-0"><?php echo $customer_data['username']; ?></h6>
                                        <p class="mb-0" style="text-transform:none;"><?php echo $customer_data['email']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider mt-0"></div>
                            <div class="">
                                <ul class="list-unstyled">
                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-profile">Profile</a></li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">My Speeches and Evaluations <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item" data-toggle="dropdown" id="navbarDropdown" href="<?php echo base_url(); ?>my-speeches" class="d-block py-2 px-2">My Speeches</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-evaluations" class="d-block py-2 px-2">My Evaluations </a></li>
                                        </ul>

                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo base_url(); ?>enrolled-classes" class="d-block py-2 px-2">My Enrolled Classes</a></li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">My Contests <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-contest-online" class="d-block py-2 px-2">My Contests</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-winnings" class="d-block py-2 px-2">Contest Results</a></li>
                                        </ul>

                                    </li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">Learderboard and Points <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>my-leaderboard">Leaderboard </a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-reward-points" class="d-block py-2 px-2">My Reward Points</a></li>
                                        </ul>

                                    </li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">Videos <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>video-library" class="d-block py-2 px-2">Video Library</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-owned-video" class="d-block py-2 px-2">Owned Videos</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">Purchases <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>purchase-promo-codes" class="d-block py-2 px-2">Purchase Promocode</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>subscription">Subscription</a></li>
                                        </ul>
                                    </li>
                                    <li class="submenu"><a class="dropdown-item" href="#!">Settings <div class="rgt-submenu-icon"><i class="fas fa-chevron-down"></i></div></a>
                                        <ul>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>notification-setting" class="d-block py-2 px-2">Notification Setting</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>my-payments" class="d-block py-2 px-2">Manage Payment</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>contact" class="d-block py-2 px-2">Contact Us</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>help-and-faq" class="d-block py-2 px-2">Help & FAQ</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>terms-conditions" class="d-block py-2 px-2">Terms & Conditions</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url(); ?>privacy-policy" class="d-block py-2 px-2">Privacy Policy</a></li>

                                            <li><a class="dropdown-item" onclick="return delete_user();" class="d-block py-2 px-2" style="color: #213360;">Delete User</a></li>
                                            <li>
                                                <a class="dropdown-item" href="javascript: void(0);" onclick="return user_logout();">
                                                    Sign Out
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                    </li>
              
                </div>
            </div>
        <!-- /.container -->
        </nav>
      <!-- /.navabr -->
    </header>

<?php } else { ?>
  <header class="header header-layout2">
    <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>website_assets/images/logo/logo-green.png" class="green-logo" alt="logo">
            </a>
            <button class="navbar-toggler" type="button">
                <span class="menu-lines"><span></span></span>
            </button>
            <div class="collapse navbar-collapse" id="mainNavigation">
                <ul class="navbar-nav ml-auto align-items-lg-center">
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>" class=" nav__item-link active">Home</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>login" class=" nav__item-link">Upload Speech </a>
                    </li>
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>login" class=" nav__item-link">My Speeches</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>login" class=" nav__item-link">My Evaluations</a>
                    </li>
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>login" class=" nav__item-link">Online Classes </a>
                    </li>
                    <li class="nav__item">
                        <a href="<?php echo base_url(); ?>login" class=" nav__item-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Contests</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>login">Live/Video</a>
                            <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>login">Leaderboard </a>
                           <!--  <a class="dropdown-item font-weight-bold" href="<?php echo base_url(); ?>login">Sponsorships</a> -->
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>login" class="btn btn__primary btn__rounded menu-inner-btn">
                        <i class="icon-calendar"></i>
                        <span>Sign in</span>
                        </a>
                    </li>
                </ul>
                <button class="close-mobile-menu d-block d-lg-none"><i class="fas fa-times"></i></button>
            </div>
            <!-- <button class="action__btn-search ml-30"><i class="fa fa-search"></i></button> -->
        </div>
    </nav>
</header>
<?php } ?>
