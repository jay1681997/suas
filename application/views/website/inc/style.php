<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<meta name="description" content="">
<meta name="description" content="">
<link href="<?php echo base_url(); ?>website_assets/images/favicon/favicon.png" rel="icon">
<title><?php echo PROJECT_NAME; ?></title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600;700&amp;family=Roboto:wght@400;700&amp;display=swap">
<link rel="stylesheet" href="<?php echo base_url(); ?>website_assets/fontawesome/css/all.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>website_assets/css/libraries.css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>website_assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>website_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet"> -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker.min.css">
<!-- <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" /> -->


