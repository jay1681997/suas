<?php 
$customer_data =  $this->session->userdata('website_userdata');
if(!empty($this->session->userdata('user_id'))) {   ?>
  
<footer class="footer">
    <div class="footer-primary">
        <div class="container">
            <div class="row">
                <!-- <div class="col-sm-12 col-md-12 col-lg-3">
                    <div class="footer-widget-about">
                              <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard">
                        <img src="<?php echo base_url(); ?>website_assets/images/logo.svg" alt="logo" class="mb-30">
                        <p class="color-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </a>
                        <a href="#" class="btn btn__primary btn__primary-style2 btn__link">
                            <span>See More..</span> <i class="icon-arrow-right"></i>
                        </a>
                    </div>
                </div> -->
                <div class="col-sm-12 col-md-12 col-lg-3">
        <div class="footer-widget-about">
             <img src="<?php echo base_url(); ?>website_assets/images/logo.svg" alt="logo" class="mb-30">
            <!-- <p class="color-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua.
            </p> -->

     

            <a href="#" class="btn btn__primary btn__primary-style2 btn__link">
                <span>See More..</span> <i class="icon-arrow-right"></i>
            </a>
        </div>
    </div>

                <div class="col-6 col-sm-6 col-md-6 col-lg-2 offset-lg-1">
                    <div class="footer-widget-nav">
                        <h6 class="footer-widget__title">Services</h6>
                        <nav>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url(); ?>my-speeches">My Speeches</a></li>
                                <li><a href="<?php echo base_url(); ?>my-evaluations">My Evaluations</a></li>
                                <li><a href="<?php echo base_url(); ?>online-class">Online Classes</a></li>
                                <li><a href="<?php echo base_url(); ?>enrolled-classes">Enrolled Classes</a></li>
                                <li><a href="<?php echo base_url(); ?>contest-list">My Contests</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-2">
                    <div class="footer-widget-nav">
                        <h6 class="footer-widget__title">Company</h6>
                        <nav>
                            <ul class="list-unstyled">
                                <!-- <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li> -->
                                <li><a href="<?php echo base_url(); ?>help-and-faq">Help & FAQ</a></li>
                                <li><a href="<?php echo base_url(); ?>subscription">Subscriptions</a></li>
                                <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="footer-widget-contact">
                        <h6 class="footer-widget__title color-heading">Quick Contacts</h6>
                        <ul class="contact-list list-unstyled">
                            <li>Fill out the information below to get in touch with us</li>
                            <!-- <li>If you have any questions or need help, feel free to contact with our team.</li>
                            <li class="color-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod...</li> -->
                        </ul>
                        <div class="d-flex align-items-center">
                            <a href="<?php echo base_url(); ?>contact" class="btn btn__primary btn__link mr-30">
                                <!-- <i class="icon-arrow-right"></i> <span>Get Directions</span> -->
                                <i class="icon-arrow-right"></i> <span>Contact Us</span>
                            </a>
                            <!-- <ul class="social-icons list-unstyled mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-secondary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <span class="fz-14">&copy; 2022 Stand up & Speak, All Rights Reserved.</span>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <nav>
                        <ul class="list-unstyled footer__copyright-links d-flex flex-wrap justify-content-end mb-0">
                            <li><a href="<?php echo base_url(); ?>terms-conditions">Terms & Conditions</a></li>
                            <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php } else { ?>
<footer class="footer">
    <div class="footer-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-3">
                    <div class="footer-widget-about">
                        <img src="<?php echo base_url(); ?>website_assets/images/logo.svg" alt="logo" class="mb-30">
                        <!-- <p class="color-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                         tempor incididunt ut labore et dolore magna aliqua.
                        </p> -->
                        <a href="#" class="btn btn__primary btn__primary-style2 btn__link">
                            <span>See More..</span> <i class="icon-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-2 offset-lg-1">
                    <div class="footer-widget-nav">
                        <h6 class="footer-widget__title">Services</h6>
                        <nav>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url(); ?>login">My Speeches</a></li>
                                <li><a href="<?php echo base_url(); ?>login">My Evaluations</a></li>
                                <li><a href="<?php echo base_url(); ?>login">Online Classes</a></li>
                                <li><a href="<?php echo base_url(); ?>login">Enrolled Classes</a></li>
                                <li><a href="<?php echo base_url(); ?>login">My Contests</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-2">
                    <div class="footer-widget-nav">
                        <h6 class="footer-widget__title">Company</h6>
                        <nav>
                            <ul class="list-unstyled">
                                <!-- <li><a href="<?php echo base_url(); ?>about-us">About Us</a></li> -->
                                <li><a href="<?php echo base_url(); ?>help-and-faq">Help & FAQ</a></li>
                                <li><a href="<?php echo base_url(); ?>">Subscriptions</a></li>
                                <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="footer-widget-contact">
                        <h6 class="footer-widget__title color-heading">Quick Contacts</h6>
                        <ul class="contact-list list-unstyled">
                                <li>Fill out the information below to get in touch with us</li>
                           <!--  <li>If you have any questions or need help, feel free to contact with our team.</li>
                            <li class="color-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod...</li> -->
                        </ul>
                        <div class="d-flex align-items-center">
                            <a href="<?php echo base_url(); ?>contact" class="btn btn__primary btn__link mr-30">
                                <i class="icon-arrow-right"></i> <span>Contact Us</span>
                            </a>
                            <!-- <ul class="social-icons list-unstyled mb-0">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-secondary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <span class="fz-14">&copy; 2022 Stand up & Speak, All Rights Reserved.</span>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <nav>
                        <ul class="list-unstyled footer__copyright-links d-flex flex-wrap justify-content-end mb-0">
                            <li><a href="<?php echo base_url(); ?>terms-conditions">Terms & Conditions</a></li>
                            <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php } ?>
<div class="search-popup">
    <button type="button" class="search-popup__close"><i class="fas fa-times"></i></button>
    <form class="search-popup__form">
        <input type="text" class="search-popup__form__input" placeholder="Type Words Then Enter" id="myInput" name="myInput" onkeyup="common_searchFunction()">
        <button class="search-popup__btn"><i class="icon-search"></i></button>
    </form>
</div><!-- /. search-popup -->
<button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>