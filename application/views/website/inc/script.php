<script src="<?php echo base_url(); ?>website_assets/js/jquery-3.5.1.min.js"></script>
<script src="<?php echo base_url(); ?>website_assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>website_assets/js/main.js"></script>

<script src="<?php echo base_url(); ?>website_assets/js/circle-progress.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
<!-- <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script> -->
<script src="<?php echo base_url();?>assets/plugins/momentjs/moment.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script> -->

<script type="text/javascript">$('form').parsley();</script>
<script type="text/javascript">
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode != 46)
        {
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;    
        }   
        return true;
    } 

    function user_logout(type)
    {
        swal({   
            title: "Sign Out?",   
            text: "Are you sure you want to sign out?",      
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#25de8d",   
            confirmButtonText: "Yes, Logout", 
            cancelButtonText:"Cancel",  
            closeOnConfirm: true 
        }, function(){   
            var url = '<?php echo site_url();?>website/logout';
            window.location.href = url;
        });
        return false;
    }

    function delete_user(type)
    {
        swal({   
            title: "Delete User?",   
            text: "Are you sure you want to delete your account?",      
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#25de8d",   
            confirmButtonText: "Yes, delete account", 
            cancelButtonText:"Cancel",  
            closeOnConfirm: true 
        }, function(){   
            var url = '<?php echo site_url();?>website/delete_account';
            window.location.href = url;
        });
        return false;
    }

    function common_searchFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("wrapper");
        tr = document.getElementsByClassName("container");
        for (i = 0; i < tr.length; i++) {
            td = tr[i];
            // .getElementsByTagName("td")[0]
            console.log(td)
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }


// upcoming classes
        $(".category-slider").owlCarousel({
            autoplay: true,
            loop: false,
            margin: 25,
            touchDrag: true,
            mouseDrag: true,
            items: 1,
            nav: false,
            dots: true,
            autoplayTimeout: 6000,
            autoplaySpeed: 1200,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });

        // online classes
        $(".cr-classes-slider").owlCarousel({
            autoplay: true,
            loop: false,
            margin: 25,
            touchDrag: true,
            mouseDrag: true,
            items: 1,
            nav: false,
            dots: true,
            autoplayTimeout: 6000,
            autoplaySpeed: 1200,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });

</script>

