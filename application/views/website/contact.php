<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
        <style type="text/css">
            /*.parsley-required{
                margin-top: -18px;
                margin-bottom: 38px;
            }*/
           .parsley-errors-list{
            margin: 0px 0 0px;
            line-height: 2em;
           }
           ul.parsley-errors-list li.parsley-required{
            margin-top: 5px !important;
           }
           .form-group{
            margin-bottom: 10px;
            margin-top: 10px !important;
           }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
                <section class="">
                    <div class="container">
                        <div class="contact-panel px-4 py-4">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                                </ol>
                            </nav>
                            <div class="contact-us">
                                <div class="align-items-center row">
                                    <div class="col-lg-6">
                                        <div class="contact_form_inner">
                                            <div class="contact_field">
                                                <h3 class="mb-1">Contact Us</h3>
                                                <!-- <p>Feel Free to contact us any time. We will get back to you as soon as we can!.</p> -->
                                                <p>Fill out the information below to get in touch with us</p>

                                                <?php if($this->session->flashdata('success_msg')){ ?>
                                                    <div class="alert alert-success" >
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                        <?php echo $this->session->flashdata('success_msg')?>
                                                    </div>                
                                                <?php } ?>
                                                <?php if($this->session->flashdata('error_msg')){ ?>
                                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                        <?php echo $this->session->flashdata('error_msg')?>
                                                    </div>
                                                <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                        <?php echo $error_msg; ?>
                                                    </div>
                                                <?php } ?>
                                                <?php echo form_open('contact', array('method' => 'post', 'class'=>"mt-40")); ?>
                                                    
                                                <?php  $customerData = $this->session->userdata('website_userdata');
                                                       $email = (!empty($customerData['email']))?$customerData['email']:'';
                                                       $username = (!empty($customerData['username']))?$customerData['username']:'';
                                                 ?>
                                                    <input 
                                                        type="email" 
                                                        class="form-control form-group"
                                                        id="email" 
                                                        name="email"
                                                        placeholder="Email" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" 
                                                        data-parsley-maxlength="64" 
                                                        value="<?php echo (set_value('email') != '')?set_value('email'):$email; ?>"
                                                        data-parsley-errors-container="#emailerror" 
                                                        data-parsley-pattern-message="Please enter valid email" 
                                                        data-parsley-required-message="Please enter email" 
                                                        required
                                                    >
                                                    <div id="emailerror"></div>
                                                   
                                                    <input 
                                                        type="text" 
                                                        class="form-control form-group"
                                                        id="name" 
                                                        name="name"
                                                        placeholder="Name" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-pattern="^[a-zA-Z ]+$" 
                                                        data-parsley-maxlength="64" 
                                                        value="<?php echo (set_value('name') != '')?set_value('name'):$username; ?>"
                                                        data-parsley-errors-container="#nameerror" 
                                                        data-parsley-required-message="Please enter name" 
                                                        data-parsley-pattern-message="Please enter valid name" 
                                                        required
                                                    >
                                                    <div id="nameerror"></div>
                                                    <textarea 
                                                        class="form-control form-group" 
                                                        name="description"
                                                        placeholder="Description"
                                                        data-parsley-required-message="Please write description" 
                                                        required
                                                        data-parsley-errors-container="#descriptionerror" 
                                                    ></textarea>
                                                    <div id="descriptionerror"></div>
                                                    <div class="d-flex justify-content-center mb-4 mt-4">
                                                      <button type="submit" class="btn btn__primary btn__rounded">Send</button>
                                                    </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                          
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="contact_info_sec">
                                            <h4 class="text-white">Contact Info</h4>
                                            <a href="mailto:app@standupandspeak.com" class="d-flex info_single align-items-center text-white position-relative">
                                                <i class="fas fa-envelope-open-text"></i>
                                                <span class="fz-16">app@standupandspeak.com</span>
                                            </a>
                                            <div class="d-flex info_single align-items-center position-relative">
                                                <i class="fas fa-map-marked-alt"></i>
                                                <!-- <span class="fz-16">2285 Dunwin Drive, Unit 7 Mississauga, ON L5L 3S3</span> -->
                                                <a href="https://goo.gl/maps/SyicH3W7MTtVvhsC9" target="_blank"><span style="color:white;"  class="fz-16">2285 Dunwin Drive, Unit 7 Mississauga, ON L5L 3S3</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php  include("inc/footer.php");?>
        </div>
        <?php  include("inc/script.php");?>
    </body>
</html>