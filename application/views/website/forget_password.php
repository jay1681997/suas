<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("inc/style.php"); ?>
</head>
<style type="text/css">
    .crn-numbers .select2-container {
        width: 40% !important;
    }

    .crn-numbers .select2-container .select2-selection--single {
        height: 60px;
    }

    .crn-numbers .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 60px;
    }

    .crn-numbers .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 4px;
    }

    .crn-numbers .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 20%;
    }

    .crn-numbers .select2-container--default .select2-selection--single {
        border-radius: 30px 0 0 30px;
        border-right: none !important;
        border: 2px solid #e6e8eb;
    }

    .select2-container--open .select2-dropdown {
        left: 0;
        width: 400px !important;
    }

    .line:after {
        border-radius: 15px 15px 0 0 !important;
        border-bottom: 5px solid #17966B !important;
    }

    .forgot-tab li.nav-item .nav-link.active:after {
        content: '';
        position: absolute;
        bottom: 0px;
        background: #17966b;
        left: 0;
        right: 0;
        width: 35px;
        margin: 0 auto;
        border-radius: 10px 10px 0 0;
        height: 5px;
    }

    .forgot-tab li.nav-item {
        position: relative;
    }

    .forgot-tab li.nav-item .nav-link.active {
        border-bottom: unset !important;
    }
</style>

<body>
    <div class="wrapper">
        <?php include("inc/header.php"); ?>
        <!--start-body-->
        <section class="otp-verif contact-panel text-center">
            <div class="container">
                <img src="<?php echo base_url(); ?>website_assets/images/forgot-password.png" alt="forgot-password" class="mb-4">
                <div class="title">
                    <h4 class="line mb-0 pb-20 position-relative">Forgot Password</h4>
                    <P class="mt-0 mb-4 pt-20 fz-16" id="type_text">Enter the email associated with your account.</P>
                </div>
                <?php echo form_open(base_url() . 'forget_password', array("method" => "post", "name" => "loginform", 'id' => "forgotpasswordform", "class" => "login")); ?>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs justify-content-center pb-2 mb-2 forgot-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link border-0 active usertypes" data-toggle="tab" href="#profile" role="tab" value="profile" id="profiless" data-id="tag_email">Email</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 usertypes" data-toggle="tab" href="#messages" role="tab" id="messagesss" value="messages" data-id="tag_phone">Mobile</a>
                    </li>
                </ul>
                <div class="alert alert-success alert-dismissable zoomIn animated" id="forgotpasswordform_successmsg" style="display: none;"></div>
                <div class="clearfix"></div>
                <div class="alert alert-danger alert-dismissable zoomIn animated" id="forgotpasswordform_errormsg" style="display: none;"></div>
                <!--start-Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="profile" role="tabpanel">
                        <div class="form-group">
                            <i class="icon-email form-group-icon text-success"></i>
                            <input type="email" class="form-control" id="forgotpassword_email" name="email" placeholder="Email" aria-required="true" data-parsley-trigger="change" data-parsley-pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" data-parsley-errors-container="#forgotpassword_emailerror" data-parsley-maxlength="64" data-parsley-required-message="Please enter email" required>
                            <?php echo form_error('email'); ?>
                            <div id="forgotpassword_emailerror"></div>
                        </div>

                        <!-- <a href="#" class="btn btn__primary btn__rounded w-100 fz-16" data-toggle="modal" data-target="#form">Send</a> -->

                    </div>
                    <div class="tab-pane" id="messages" role="tabpanel">
                        <div class="form-group">
                            <i class="icon-phone form-group-icon text-success"></i>
                            <div class="input-group mb-3 mb-md-4 mb-lg-3 crn-numbers">
                                <select class="form-select form-control imageselect2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:140px;padding-left:20px">
                                    <!-- <option value="+1">+1</option> -->
                                    <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                    <?php if (!empty($countries)) {
                                        foreach ($countries as $key => $value) { ?>
                                            <!--  <option value="<?php echo $value['calling_code']; ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['calling_code']; ?></option> -->
                                            <option value="<?php echo $value['calling_code']; ?>" data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'] . '(' . $value['calling_code'] . ')'; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                                <?php echo form_error('country_code'); ?>
                                <label id="countrieseerror"></label>
                                <input type="text" class="form-control phone-format" id="phone" name="phone" aria-required="true" parsley-trigger="change" placeholder="Phone Number" data-parsley-errors-container="#phoneerror" data-parsley-required-message="Please enter phone number" required maxlength="12" data-parsley-minlength="8" data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" value="<?php echo set_value('phone'); ?>">
                                <?php echo form_error('phone'); ?>
                            </div>
                            <div id="phoneerror"></div>
                        </div>

                        <!-- <a href="otp-verification.html" class="btn btn__primary btn__rounded w-100 fz-16">Send</a> -->

                    </div>
                    <button type="button" class="btn btn__primary btn__rounded w-100 fz-16 btn-animated-bl" id="forgotpasswordbtn">Send</button>
                </div>
                <?php echo form_close(); ?>
                <!--end-Tab panes -->
                <!-- <a href="#" class="btn btn__primary btn__rounded w-100 fz-16" data-toggle="modal" data-target="#form">Send</a> -->
            </div>
        </section>
        <!--end-body-->

        <!--start-modal-->
        <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content custom-radius">
                    <div class="modal-header border-bottom-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/verification-done.png" alt="verification-dones" class="mb-20">
                        <div class="content">
                            <h4>Forgot Password</h4>
                            <p class="fz-16">A password reset link has been sent to the email associated with your account. Please open the link and create a new password.</p>
                            <a href="<?php echo base_url(); ?>login" class="btn btn__primary btn__rounded fz-16">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("inc/footer.php"); ?>
    </div><!-- /.wrapper -->

    <?php include("inc/script.php"); ?>
    <script type="text/javascript">
        function UserTemplate(user) {
            if (!user.id) {
                return user.text;
            }
            var $user = $(
                '<span><img src="' + $(user.element).data('profileimage') + '" width="20px" height="20px" class="img-flag"/> ' + user.text + '</span>'
            );
            return $user;
        };
        $(document).ready(function() {
            $('select').niceSelect('destroy');
            $('.select2').select2({
                "width": "100%"
            });
            $("#country_code").select2({
                "width": "100%",
            });
            $(".imageselect2").select2({
                "width": "100%",
                templateResult: UserTemplate,
                templateSelection: UserTemplate
            });
            <?php if (!empty($this->session->userdata('forgot_password')) && $this->session->userdata('forgot_password') == 'yes') { ?>
                $('#form').modal('show');
            <?php } ?>

            $('#messagesss').click(function() {
                //$('#type_text').html('Enter the phone number associated with your account.'); 
                $('#type_text').html('Enter the email associated with your account.');
            })
            $('#profiless').click(function() {
                $('#type_text').html('Enter the email associated with your account.');
            })
        });

        $('#forgotpasswordbtn').click(function() {
            var tab_name = $('.tab-content .active').attr('id');

            if ((tab_name == 'profile' && $('#forgotpassword_email').parsley().validate()) || (tab_name == 'messages' && $('#country_code').parsley().validate() && $('#phone').parsley().validate())) {

                console.log("echo")
                $('#forgotpasswordform_errormsg').hide();
                $('#forgotpasswordform_successmsg').hide();
                $('#forgotpasswordbtn').prop('disabled', true);
                $('#forgotpasswordbtn').html("Send <i class='fa fa-spin fa-spinner'></i>");
                var formdata = {};
                $.each($('#forgotpasswordform').serializeArray(), function() {
                    console.log(this)
                    if (this.name == 'phone') {
                        formdata[this.name] = this.value;
                    } else {
                        formdata[this.name] = this.value;
                    }
                });
                formdata['forgot_method'] = tab_name;

                var email = formdata['email'];
                var country_code = formdata['country_code'];
                var phonenumber = formdata['phone'];
                console.log(formdata);
                console.log("SENDEND");
                $.ajax({
                    url: "<?php echo base_url() . 'forget-password'; ?>",
                    type: 'POST',
                    dataType: 'json',
                    data: formdata,
                    error: function(err) {
                        console.log(err)
                        $('#forgotpasswordform_errormsg').html(err);
                        $('#forgotpasswordform_errormsg').parent().show();
                        $('#forgotpassword_emailerror').html('');
                        $('#forgotpasswordbtn').prop('disabled', false);
                        $('#forgotpasswordbtn').html("Send");
                    },
                    success: function(data) {
                        console.log(data)

                        if (data.status == 'success') {
                            $('#forgotpasswordform_errormsg').hide();
                            $('#forgotpasswordform_successmsg').html(data.msg);
                            $('#forgotpasswordform_successmsg').show();
                            if (tab_name == 'messages') {

                                window.location.href = "<?php echo base_url() . 'otp-verification'; ?>";
                            } else {
                                $('#form').modal('show');
                            }
                        } else if (data.status == 'validation_error') {
                            $('#forgotpassword_emailerror').html(data.msg);
                        } else {
                            $('#forgotpasswordform_successmsg').hide();
                            $('#forgotpasswordform_errormsg').html(data.msg);
                            $('#forgotpasswordform_errormsg').show();
                            // setTimeout(function(){
                            //  $('#forgotpasswordform_errormsg').hide();
                            // },3600);
                        }
                        $('#forgotpasswordbtn').prop('disabled', false);
                        $('#forgotpasswordbtn').html("Send");
                    }
                });
            } else {
                console.log("test")
                $('#forgotpasswordform_successmsg').hide();
                $('#forgotpasswordform_errormsg').hide();
                $('#forgotpasswordbtn').prop('disabled', false);
                $('#forgotpasswordbtn').html("Send");
            }
        });
        $(".phone-format").keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
            var curchr = this.value.length;
            var curval = $(this).val();
            if (curchr == 3 && curval.indexOf("(") <= -1) {
                $(this).val("" + curval + "" + "-");
            } else if (curchr == 3 && curval.indexOf("(") > -1) {
                $(this).val(curval + "-");
            } else if (curchr == 3 && curval.indexOf(")") > -1) {
                $(this).val(curval + "-");
            } else if (curchr == 7) {
                $(this).val(curval + "-");
                $(this).attr('maxlength', '14');
            }
        });
    </script>
</body>

</html>