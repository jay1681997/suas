<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
        <?php  include("inc/header.php");?>
            <!--start-section-->
            <div class="founder-sec">
                <div class="container">
                    <h4 class="mb-50 text-center">A Message From Our Founder</h4>
                    <div class="row">
                        <div class="col-lg-3 text-center">
                            <div class="">
                                <div class="user-img mb-3">
                                    <img src="<?php echo base_url(); ?>website_assets/images/user/fouder.jpg" alt="fouder">
                                </div>
                                <span class="mb-2 d-block">Lori-Ann jakel</span>
                                <p>Founder,Public Speaking Maven</p>
                                <ul class="fo-social d-flex list-unstyled justify-content-center">
                                    <li>
                                        <a href="http://facebook.com/standupandspeakinc">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="facebook-square" viewBox="0 0 1536 1896.0833">
                                                <path d="M1248 128q119 0 203.5 84.5T1536 416v960q0 119-84.5 203.5T1248 1664h-188v-595h199l30-232h-229V689q0-56 23.5-84t91.5-28l122-1V369q-63-9-178-9-136 0-217.5 80T820 666v171H620v232h200v595H288q-119 0-203.5-84.5T0 1376V416q0-119 84.5-203.5T288 128h960z"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://linkedin.com/company/stand-up-and-speak-inc">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="linkedin-square" viewBox="0 0 1536 1896.0833">
                                                <path d="M237 1414h231V720H237v694zm246-908q-1-52-36-86t-93-34-94.5 34-36.5 86q0 51 35.5 85.5T351 626h1q59 0 95-34.5t36-85.5zm585 908h231v-398q0-154-73-233t-193-79q-136 0-209 117h2V720H595q3 66 0 694h231v-388q0-38 7-56 15-35 45-59.5t74-24.5q116 0 116 157v371zm468-998v960q0 119-84.5 203.5T1248 1664H288q-119 0-203.5-84.5T0 1376V416q0-119 84.5-203.5T288 128h960q119 0 203.5 84.5T1536 416z"></path>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <span class="quotion mb-30 d-flex justify-content-center">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="quote-right" viewBox="0 0 512 545.5"><path d="M64 160h160v160c0 52.826-43.174 96-96 96v-32c35.526 0 64-28.474 64-64H64V160zm224 0h160v160c0 52.826-43.174 96-96 96v-32c35.526 0 64-28.474 64-64H288V160zM96 192v96h96v-96H96zm224 0v96h96v-96h-96z"></path></svg>
                            </span>
                            <div class="content">
                                <p><?php echo $about_us['about_us']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-->

            <!--start-gallery-->
            <section class="gallery pt-90 pb-90">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                            <div class="heading text-center mb-40">
                                <h3 class="heading__title">Download Our App</h3>
                                <p class="heading__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.
                                </p>
                            </div><!-- /.heading -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                    <div class="row align-items-center">
                        <div class="col-12 col-md-6">
                            <div class="title-wrap pb-0">
                                <div class="title-box">
                                    <h5> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor.</h5>
                                    <p class="my-4 pr-4 fz-16"> Easy and quick to use</p>
                                    <div class="transpoer-listing-section-app">
                                        <ul>
                                            <li class="fz-16">Lorem ipsum dolor sit amet dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                    tempor incididunt ut</li>
                                            <li class="fz-16">Consectetur adipisicing elit tempor incididunt ut labore et dolore magna aliqua</li>
                                            <li class="fz-16">Sed do eiusmod reprehenderit in voluptate velit esse
                                                cillum dolore eu fugiat</li>
                                            <li class="fz-16">tempor incididunt ut labore sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li class="fz-16">Dolore magna aliqua Excepteur sint occaecat cupidatat non
                                                proident, sunt in culpa qui officia</li>
                                        </ul>
                                    </div>
                                    <div class="dwnld-link justify-content-center">
                                        <a href="#" class="mr-2"><img class="" src="<?php echo base_url(); ?>website_assets/images/app-store.png" alt=""></a>
                                         <!-- <a href="#"><img class="" src="<?php echo base_url(); ?>website_assets/images/play-store.png" alt=""></a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                        <picture>
                            <source media="(min-width:650px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png">
                            <source media="(min-width:465px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png"> 
                                <img src="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png" alt="Flowers" style="width:100%;"> 
                            </picture>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section>
        <?php  include("inc/footer.php");?>
        </div>
    <?php  include("inc/script.php");?>
    </body>
</html>