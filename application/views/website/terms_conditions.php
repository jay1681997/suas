<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include("inc/style.php");?>
    </head>

    <body>
        <div class="wrapper">
            <?php  include("inc/header.php");?>
                <section class="">
                    <div class="container">
                        <div class="contact-panel px-4 py-4">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Terms & Conditions</li>
                                </ol>
                            </nav>
                            <div class="content">
                                <p><?php echo $terms_conditions['terms_and_condition']; ?></p>
                            </div>
                        </div>
                    </div>
                </section>
            <?php  include("inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include("inc/script.php");?>
    </body>
</html>