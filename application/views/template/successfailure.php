<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title><?php echo PROJECT_NAME; ?></title>
        <link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate_page.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/authentication.css">
    </head>
    <body class="theme-orange">
        <div class="authentication">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header slideDown">
                                <div class="logo">
                                    <img src="<?php echo BASE_IMAGE_URL;?>" style="height: 100px; width:100px;" alt="<?php echo PROJECT_NAME; ?>">
                                </div>
                                <h1 class="" style="color: <?php echo THEME_COLOR;?>;">
                                    <?php echo PROJECT_NAME; ?> 
                                </h1>
                            </div>                      
                        </div>     
                        <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('success_msg')){ ?>
                            <div class="alert alert-success alert-dismissible zoomIn animated" >
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success_msg')?>
                            </div>                
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('error_msg')){ ?>
                            <div class="alert alert-danger alert-dismissible zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error_msg')?>
                            </div>
                            <?php } ?>  
                            <div class="clearfix"></div>                       
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script>           
        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();
            });
        </script>
    </body>
</html>