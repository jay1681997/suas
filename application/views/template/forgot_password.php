<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title><?php echo PROJECT_NAME.' - Change Password'; ?></title>
        <link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate_page.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/authentication.css">
    </head>
    <body class="theme-orange">
        <div class="authentication">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header slideDown">
                                <div class="logo">
                                    <img src="<?php echo BASE_IMAGE_URL;?>" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;padding-bottom:0;display:inline;vertical-align:bottom;max-width:199px;" alt="<?php echo PROJECT_NAME; ?>">
                                </div>
                                <h1 class="" style="color: <?php echo THEME_COLOR;?>;">
                                    <?php echo PROJECT_NAME." Change Password"; ?> 
                                </h1>
                            </div> 
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('success_msg')){ ?>
                            <div class="alert alert-success alert-dismissible zoomIn animated" >
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success_msg')?>
                            </div>                
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('error_msg')){ ?>
                            <div class="alert alert-danger alert-dismissible zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error_msg')?>
                            </div>
                            <?php } ?>                       
                        </div>     
                        <div class="clearfix"></div>
                        <?php echo form_open('home/changepassword',array('class'=>'col-lg-12','name'=>'login_form','id'=>'login_form','method'=>'post')); ?> 
                            <h5 class="title">Change password</h5>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Password</label>
                                    <input class="form-control" type="password"  id="password" name="password" parsley-type="password" value="<?php echo set_value('password'); ?>" data-parsley-trigger="change" required autocomplete="false" data-parsley-required-message="Please enter password" data-parsley-pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&£€^(){}:'|<>]).*"   data-parsley-pattern-message="Please enter atleast 4 characters length with 1 upper case, 1 lower case, 1 number and 1 special character" data-parsley-maxlength="64" data-parsley-minlength="4" data-parsley-errors-container="#password_error">
                                    
                                    <?php echo form_error('password'); ?>
                                </div>
                                <div id="password_error"></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group form-float">
                                <div class="form-line" >
                                    <label class="form-label">Confirm Password</label>
                                    <input class="form-control" type="password" id="confirmpassword" name="confirmpassword" data-parsley-trigger="change" data-parsley-equalto="#password" required autocomplete="false" data-parsley-required-message="Please enter confirm password" data-parsley-equalto-message="Password mismatch">
                                    
                                    <?php echo form_error('confirmpassword'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-raised bg-theme waves-effect">Change Password</button>
                            </div>                       
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();
            });
        </script>
    </body>
</html>