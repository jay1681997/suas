<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 10-Feb-2022
** Modified On  : -  
*/
require 'vendor/autoload.php';

class User extends MY_Controller_Customer
{

    private $view_folder = 'customer/user/';
    //Default constructor
    function __construct()
    {
        parent::__construct();
        $this->load->model('customer/user_model');
        $this->load->model('customer/service_model');
    }

    //Display subscription list and also display active subscription inside this listing
    public function subscription()
    {
        $this->load->view($this->view_folder . 'subscription');
    }

    public function region_list($country_name)
    {
        $country_data = $this->common_model->common_singleSelect("tbl_country", array("name" => $country_name, "is_deleted" => "0"));
        $region_list = $this->common_model->common_multipleSelect("tbl_tax_details", array("country_id" => $country_data['id'], "is_deleted" => "0", "is_active" => "1"));
        echo json_encode($region_list);
        die;
        // print_r($country_name);die;
    }

    //Function is used to get sub-profile or student list
    public function sub_profile_list()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['students'] = $this->common_model->common_multipleSelect('tbl_student', array('is_deleted' => '0', 'user_id' => $user_id));
        $this->load->view($this->view_folder . 'sub_profile_list', $data);
    }

    //Function is used to add sub profile or student
    public function add_sub_profile()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['result_students'] = $this->common_model->common_multipleSelect('tbl_student', array('is_deleted' => '0', 'user_id' => $user_id));
        if ($this->input->post()) {

            if (count($data['result_students']) < 5) {
                $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|max_length[32]');
                $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|max_length[32]');
                $this->form_validation->set_rules('dob', 'DOB', 'required|trim');
                $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

                if ($this->form_validation->run()) {

                    $profile_image = 'default.png';
                    if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                        $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'], STUDENT_IMAGE);

                        if (!$profile_image) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->viewfolder . 'add-student');
                            return false;
                        }
                    }


                    $profile_video = '';
                    if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
                        $profile_video = $this->common_model->uploadImageS3($_FILES['video'], STUDENT_IMAGE);

                        if (!$profile_video) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->viewfolder . 'add-student');
                            return false;
                        }
                    }

                    $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                    $age  = date_diff(date_create($date_format), date_create('today'))->y;
                    $age_category = '';


                    if ($age >= 0 && $age <= 8) {
                        $age_category = 'Primary';
                    } else if ($age >= 9 && $age <= 12) {
                        $age_category = 'Juniors';
                    } else if ($age >= 13 && $age <= 17) {
                        $age_category = 'Teens';
                    } else if ($age >= 18) {
                        $age_category = 'Adults';
                    }

                    $otp = rand(1000, 9999);
                    $userarray = array(
                        'firstname'     => $this->input->post('firstname'),
                        'lastname'      => $this->input->post('lastname'),
                        'username'      => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                        'dob'           => $date_format,
                        'profile_image' => $profile_image,
                        'profile_video' => $profile_video,
                        'age'           => $age,
                        'age_category'  => $age_category,
                        'user_id'       => $user_id,
                    );

                    $student_id   =  $this->common_model->common_insert('tbl_student', $userarray);
                    $this->session->set_flashdata("success_msg", $this->lang->line('website_student_added_success'));
                    redirect('sub-profile-list');
                } else {

                    $this->load->view($this->view_folder . 'add_sub_profile', $data);
                }
            } else {

                $this->session->set_flashdata("error_msg", $this->lang->line('website_student_limit_reached'));
                redirect('sub-profile-list');
            }
        } else {

            $this->load->view($this->view_folder . 'add_sub_profile', $data);
        }
    }

    //Function is used to edit sub profile details
    public function edit_sub_profile($student_id)
    {
        $student_id = base64_decode($student_id);
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['result_students'] = $this->common_model->common_singleSelect('tbl_student', array('id' => $student_id));
        if ($this->input->post()) {
            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim|max_length[32]');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim|max_length[32]');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if ($this->form_validation->run()) {

                $profile_image =  $data['result_students']['profile_image'];
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'], STUDENT_IMAGE);

                    if (!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->viewfolder . 'add-student');
                        return false;
                    }
                }


                $profile_video = $data['result_students']['profile_video'];
                if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
                    $profile_video = $this->common_model->uploadImageS3($_FILES['video'], STUDENT_IMAGE);

                    if (!$profile_video) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->viewfolder . 'add-student');
                        return false;
                    }
                }



                $otp = rand(1000, 9999);
                $userarray = array(
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'username'      => $this->input->post('firstname') . ' ' . $this->input->post('lastname'),
                    'profile_image' => $profile_image,
                    'profile_video' => $profile_video,
                );
                if ($data['result_students']['is_dob_edit'] == 1) {
                    $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                    $age  = date_diff(date_create($date_format), date_create('today'))->y;
                    $age_category = '';


                    if ($age >= 0 && $age <= 8) {
                        $age_category = 'Primary';
                    } else if ($age >= 9 && $age <= 12) {
                        $age_category = 'Juniors';
                    } else if ($age >= 13 && $age <= 17) {
                        $age_category = 'Teens';
                    } else if ($age >= 18) {
                        $age_category = 'Adults';
                    }
                    $userarray['dob'] = $date_format;
                    $userarray['age'] = $age;
                    $userarray['age_category'] = $age_category;
                    $userarray['is_dob_edit'] = '0';
                }
                $student_id   =  $this->common_model->common_singleUpdate('tbl_student', $userarray, array('id' => $student_id));
                $this->session->set_flashdata("success_msg", $this->lang->line('website_student_updated_success'));
                redirect('sub-profile-list');
            } else {

                $this->load->view($this->view_folder . 'edit_sub_profile', $data);
            }
        } else {

            $this->load->view($this->view_folder . 'edit_sub_profile', $data);
        }
    }

    //Function is used to upgrade student to main profile
    public function upgrade_profile($student_id)
    {
        $student_id = base64_decode($student_id);
        $data['student_id'] = $student_id;
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $students_data = $this->common_model->common_singleSelect('tbl_student', array('id' => $student_id));
        $data['countries']  = $this->common_model->get_country_code_new();

        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.is_deleted="0" AND email=]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'required|trim');
            $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[8]|max_length[12]|is_unique[tbl_user.is_deleted="0" AND phone=]|max_length[14]');
            $this->form_validation->set_rules('country', 'Country', 'required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');

            if ($this->form_validation->run()) {

                $customerdata = array(
                    'firstname'          => $students_data['firstname'],
                    'lastname'           => $students_data['lastname'],
                    'username'           => $students_data['username'],
                    'email'              => $this->input->post('email'),
                    'country_code'       => $this->input->post('country_code'),
                    'phone'              => $this->input->post('phone'),
                    'country'            => $this->input->post('country'),
                    'dob'                => $students_data['dob'],
                    'profile_image'      => $students_data['profile_image'],
                    'age'                => $students_data['age'],
                    'age_category'       => $students_data['age_category'],
                    'otp_code'           => rand(1000, 9999),
                    'password'           => $this->common_model->encrypt_password($this->input->post('password')),
                    'region_id'     => !empty($this->input->post('region_id')) ? $this->input->post('region_id') : "0",
                );
                // echo "<pre>"; print_r($customerdata); die;
                $this->common_model->common_insert('tbl_user', $customerdata);
                $this->common_model->common_delete('tbl_student', array('id' => $student_id));

                $this->session->set_flashdata('success_msg', $this->lang->line('website_student_upgrade_to_main_profile'));
                redirect('sub-profile-list');
            } else {
                $this->load->view($this->view_folder . 'upgrade_profile', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'upgrade_profile', $data);
        }
    }

    //Function is used to delete student
    public function delete_student($student_id)
    {
        $this->common_model->common_delete('tbl_student', array('id' => $student_id));
        echo "Student deleted successfully";
        die;
    }

    //Function is used to display user added payment methods
    public function my_payments()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['payment_methods'] = $this->common_model->common_multipleSelect('tbl_user_card', array('user_id' => $user_id, 'is_deleted' => '0'));
        $this->load->view($this->view_folder . 'my_payments', $data);
    }

    //Function is used to remove payment method
    public function remove_paymentMethod($card_id)
    {
        $this->common_model->common_singleUpdate('tbl_user_card', array('is_deleted' => '1'), array('id' => $card_id));
        echo "success";
        die;
    }

    //Function is used to get user speech list
    public function my_speeches()
    {
        $user_id = $this->session->userdata('user_id');

        //here load pagination library
        $this->load->library("pagination");

        //Create pagingnation 
        $config = array();
        $config["base_url"] = base_url() . "customer/user/my_speeches";
        $total_rows = $this->user_model->get_total_speech_count($user_id);
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 9;
        $config['use_page_numbers'] = TRUE;
        $config["uri_segment"] = 4;
        $config['full_tag_open']    = '<ul class="pagination d-flex justify-content-center mt-10">';
        $config['full_tag_close']   = '</ul>';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_link']        = '<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>';
        $config['next_tag_close']   = '</li>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']    = '</li>';
        $config['prev_tag_open']    = '<li class="page-item">';
        $config['prev_tag_close']   = '</li>';
        $config['prev_link']        = '<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>';
        $config['cur_tag_open']     = '<li class="page-item"><a href="#" class="current-page">';
        $config['cur_tag_close']    = '</a></li>';
        $config['first_link']       = '&#10094;&#10094;';
        $config['last_link']        = '&#10095;&#10095;';

        $this->pagination->initialize($config);
        $lastPage = ceil($total_rows / $config["per_page"]);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

        $data['speeches'] = $this->user_model->get_speech_list($user_id, (($page - 1) * $config['per_page']), $config["per_page"]);

        $data['index'] = (($page - 1) * $config['per_page']) + 1;

        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;', $str_links);
        $this->load->view($this->view_folder . 'my_speeches', $data);
    }

    //Function is used to get user evaluation list 
    public function my_evaluations()
    {

        $user_id = $this->session->userdata('user_id');

        //here load pagination library
        $this->load->library("pagination");

        //Create pagingnation 
        $config = array();
        $config["base_url"] = base_url() . "customer/user/my_evaluations";
        $total_rows = $this->user_model->get_total_evaluation_count($user_id);
        $config["total_rows"] = $total_rows;
        $config["per_page"] = 10;
        $config['use_page_numbers'] = TRUE;
        $config["uri_segment"] = 4;
        $config['full_tag_open']    = '<ul class="pagination d-flex justify-content-center mt-10">';
        $config['full_tag_close']   = '</ul>';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_link']        = '<span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span>';
        $config['next_tag_close']   = '</li>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']    = '</li>';
        $config['prev_tag_open']    = '<li class="page-item">';
        $config['prev_tag_close']   = '</li>';
        $config['prev_link']        = '<span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span>';
        $config['cur_tag_open']     = '<li class="page-item"><a href="#" class="current-page">';
        $config['cur_tag_close']    = '</a></li>';
        $config['first_link']       = '&#10094;&#10094;';
        $config['last_link']        = '&#10095;&#10095;';

        $this->pagination->initialize($config);
        $lastPage = ceil($total_rows / $config["per_page"]);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

        $data['evaluations'] = $this->user_model->get_speech_evaluationlist($user_id, (($page - 1) * $config['per_page']), $config["per_page"]);

        $data['index'] = (($page - 1) * $config['per_page']) + 1;

        $str_links = $this->pagination->create_links();
        $data["links"] = explode('&nbsp;', $str_links);
        $this->load->view($this->view_folder . 'my_evaluations', $data);
    }

    //Function is used to get user enrolled online class list
    public function enrolled_classes()
    {
        $user_id = $this->session->userdata('user_id');
        $data['enrolled_classes'] = $this->user_model->get_enrolled_classes($user_id);
        $this->load->view($this->view_folder . 'enrolled_classes', $data);
    }

    //Function is used to get user enrolled online class details
    public function enrolled_class_details($class_id)
    {
        $class_id = base64_decode($class_id);
        $data = array();
        $data['class_details'] = $this->service_model->get_class_details($class_id);
        $this->load->view($this->view_folder . 'enrolled_class_details', $data);
    }

    //Function is used to get user contest list
    public function my_contest_online()
    {
        $this->session->unset_userdata('contest_id');
        $this->session->unset_userdata('upload_video');
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['contest_list'] = $this->user_model->get_mycontest_list($user_id);
        $this->load->view($this->view_folder . 'my_contest_online', $data);
    }

    //Function is used to get user contest details
    public function my_contest_detail($contest_id)
    {
        $contest_id = base64_decode($contest_id);
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['contest_details'] = $this->user_model->get_contest_details($contest_id);
        $agora_token        = $this->common_model->get_agora_token($data['contest_details']['id'], $data['contest_details']['subadmin_id'], 'contest');
        $data['token']      = $agora_token['data']['token'];
        $data['channel']    = $agora_token['data']['channelname'];
        $data['userid']     = $data['contest_details']['id'];
        // echo "<pre>";print_r($data);die;
        $this->load->view($this->view_folder . 'my_contest_detail', $data);
    }

    //Function is used to get user contest winner list
    public function my_winnings()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['winner_list'] = $this->user_model->get_winnerlist($user_id);
        $this->load->view($this->view_folder . 'my_winnings', $data);
    }

    //Function is used to get user winner contest details
    public function won_contest_detail($contest_id)
    {
        $contest_id = base64_decode($contest_id);

        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['winner_details'] = $this->user_model->get_contest_details($contest_id);
        // print_r($this->db->last_query());
        // echo "<pre>";print_r($data);echo"<br>";print_r($user_id);echo"<br>";print_r($contest_id);die;
        $this->load->view($this->view_folder . 'won_contest_detail', $data);
    }
    // 
    //Function is used to get video library list
    public function video_library()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['videos'] = $this->common_model->common_multipleSelect('tbl_master_videolibrary', array('status' => 'Active', 'is_deleted' => '0'));
        $this->load->view($this->view_folder . 'video_library', $data);
    }

    //Function is used to get video library details
    public function video_library_details($video_id)
    {
        $video_id = base64_decode($video_id);

        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['video_details'] = $this->common_model->common_singleSelect('tbl_master_videolibrary', array('id' => $video_id));
        $data['video_order_details'] = $this->common_model->get_order_details("video", $data['video_details']['id'], $user_id);
        $this->load->view($this->view_folder . 'video_library_details', $data);
    }

    //Function is used to get user purchased video library details by its ID
    public function purchased_video_library_details($video_id)
    {
        $video_id = base64_decode($video_id);

        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['video_details'] = $this->user_model->get_user_puchasedVideo_details($video_id);
        $this->load->view($this->view_folder . 'purchased_video_library_details', $data);
    }

    //Function is used to get user owned or purchased video list
    public function my_owned_video()
    {
        $user_id = $this->session->userdata('user_id');

        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['user_videos'] = $this->user_model->get_user_puchasedVideos($user_id);

        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'my_owned_video', $data);
    }

    //Function is used to get user reward points details
    public function my_reward_points()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['rewards'] = $this->user_model->get_reward_points($user_id);
        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'my_reward_points', $data);
    }

    //Function is used to display gift enrolled class list
    public function gifted_enrolled_class()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['gift_class_list'] = $this->user_model->get_user_gifttoclass_list($user_id);
        $data['giftfrom_class_list'] = $this->user_model->get_user_giftfromclass_list($user_id);
        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'gifted_enrolled_class', $data);
    }

    //Function is used to get user wishlist
    public function wish_list()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['wishlist'] = $this->user_model->get_userwishlist($user_id);
        $this->load->view($this->view_folder . 'wish_list', $data);
    }

    //Function is used to fetch user purchase promocode list
    public function purchase_promo_codes()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        // echo $this->session->userdata('promocode_id'); die;
        $data['promocodes'] = $this->user_model->get_purchased_promocodes($user_id);
        // echo "<pre>"; print_r($data['promocodes']); die;
        $this->load->view($this->view_folder . 'purchase_promo_codes', $data);
    }

    //Function is used to fetch user purchase promocode details
    public function purchase_promo_details($promo_id)
    {

        $promo_id = base64_decode($promo_id);

        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['promocode_data'] = $this->user_model->get_promocode_details($promo_id, $user_id);

        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'purchase_promo_details', $data);
    }

    ////Function is used to get speech details by its ID
    public function my_speeches_inner($speech_id)
    {
        $speech_id = base64_decode($speech_id);

        $user_id = $this->session->userdata('user_id');

        // $data['video_count'] = $this->db->get_where('tbl_user_speech', array('user_id'=>$user_id, 'status'=>'Active','is_deleted'=>'0','is_evaluated'=>'no'))->num_rows();
        $data['video_count'] = $this->db->get_where('tbl_user_evaluation', array('user_id' => $user_id, 'is_deleted' => '0'))->num_rows();

        $data['speech_details'] = $this->common_model->common_singleSelect('tbl_user_speech', array('id' => $speech_id));
        $data['speech_evaluation'] = $this->common_model->common_singleSelect('tbl_user_evaluation', array("speech_id" => $speech_id, "is_deleted" => "0"));
        // echo $data['video_count']; die; 
        $this->load->view($this->view_folder . 'my_speeches_inner', $data);
    }

    //Function is used to remove speech
    public function remove_speech($speech_id)
    {
        $this->common_model->common_delete('tbl_user_speech', array('id' => $speech_id));
        echo "Speech deleted successfully";
        die;
    }

    //Function is used to save credit/debit card by user
    public function my_add_debit_card()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        if ($this->input->post()) {

            // echo "<pre>"; print_r($this->input->post()); die;
            $this->form_validation->set_rules('card_holder_name', 'Card Holder Name', 'required|trim');
            $this->form_validation->set_rules('card_number', 'Card Number', 'required|trim');
            $this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required|trim');
            $this->form_validation->set_rules('security_code', 'Security Code', 'required|trim');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if ($this->form_validation->run()) {

                $date_array = explode('-', $this->input->post('expiry_date'));
                // echo "<pre>";print_r($this->input->post()); print_r($date_array); die;
                // require_once('application/third_party/stripe-php/init.php');
                require_once APPPATH . '/libraries/Stripe/init.php';
                $stripe = new \Stripe\StripeClient(PAYMENT_SECRET_KEY);
                try {
                    $tokendata = $stripe->tokens->create([
                        'card' => [
                            'number'    => $this->input->post('card_number'),
                            'exp_month' => $date_array[1],
                            'exp_year'  => $date_array[0],
                            'cvc'       => $this->input->post('security_code'),
                        ],
                    ]);
                    // print_r($tokendata);
                    if (!empty($tokendata)) {

                        $is_carddata = $this->common_model->common_singleSelect('tbl_user_card', array('status' => 'Active', 'is_deleted' => '0', 'user_id' => $user_id, 'fingerprint' => $tokendata->card->fingerprint));

                        if (!empty($is_carddata)) {
                            $this->session->set_flashdata("error_msg", $this->lang->line('website_card_already_exists'));
                            $this->load->view($this->view_folder . 'my_add_debit_card');
                        } else {

                            $customerObject = array(
                                "source"        => $tokendata->id,
                                "email"         => $data['result']['email'],
                                "name"          => $data['result']['username'],
                                "phone"         => $data['result']['country_code'] . '' . $data['result']['phone'],
                                "description"   => PROJECT_NAME . " User #" . $user_id,
                                "address"       => array(
                                    "country"   => $data['result']['country'],
                                ),
                            );

                            $customer = $stripe->customers->create($customerObject);

                            if (!empty($customer)) {

                                // echo "<pre>"; print_r($customer); die;

                                $cardarray = array(
                                    "user_id"            => $user_id,
                                    "card_holder_name"  => $this->input->post('card_holder_name'),
                                    "card_number_last4" =>  $tokendata->card->last4,
                                    "fingerprint"       =>  $tokendata->card->fingerprint,
                                    "stripe_card_id"    =>  $tokendata->card->id,
                                    "card_token"        =>  $tokendata->id,
                                    "card_type"         =>  $tokendata->card->brand,
                                    "expiry_month"      =>  $date_array[1],
                                    "expiry_year"       =>  $date_array[0],
                                    "customer_id"       =>  $customer->id,
                                );


                                $this->common_model->common_insert('tbl_user_card', $cardarray);
                                $this->session->set_flashdata("success_msg", $this->lang->line('website_card_added_success'));
                                $this->load->view($this->view_folder . 'my_add_debit_card');
                            } else {

                                $this->session->set_flashdata("error_msg", $this->lang->line('website_card_customer_not_created'));
                                $this->load->view($this->view_folder . 'my_add_debit_card');
                            }
                        }
                    } else {
                        $this->session->set_flashdata("error_msg", $this->lang->line('website_card_token_not_created'));
                        $this->load->view($this->view_folder . 'my_add_debit_card');
                    }
                } catch (Exception $e) {

                    $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_card_number_not_vaild'));
                    $this->load->view($this->view_folder . 'my_add_debit_card');
                }
            } else {
                $this->load->view($this->view_folder . 'my_add_debit_card');
            }
        } else {
            $this->load->view($this->view_folder . 'my_add_debit_card');
        }
    }

    //Function is used to submit speech for evaluation 
    public function submit_speech_forevaluation($speech_id)
    {

        $user_id = $this->session->userdata('user_id');
        $speech_details = $this->common_model->common_singleSelect('tbl_user_speech', array('id' => $speech_id));
        $user_details = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $evaluation_array = array(
            'speech_id'     => $speech_id,
            'user_id'       => $user_id,
            'title'         => $speech_details['title'],
            'description'   => $speech_details['description'],
            'video'         => $speech_details['video'],
            "thumb_image" => $speech_details['thumb_image'],
            "student_id" => $speech_details['student_id']
        );
        if ($speech_details['uploading_type'] == 'AI') {
            $min = 1;
            $max = 4;
            $v1 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);
            $v2 = round(2 + mt_rand() / mt_getrandmax() * (4 - 2), 1);
            $v3 = round(0.5 + mt_rand() / mt_getrandmax() * (2 - 0.5), 1);
            $v4 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);
            $v5 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);


            $v1_l = explode(".", $v1);
            $v2_l = explode(".", $v2);
            $v3_l = explode(".", $v3);
            $v4_l = explode(".", $v4);
            $v5_l = explode(".", $v5);
            if ($v1_l[1] > 5) {
                $s_v1 = (intval($v1_l[0]) + 0.5);
            }
            if ($v1_l[1] < 5) {
                $s_v1 = (intval($v1_l[0]) + 0);
            }
            if ($v1_l[1] == 5) {
                $s_v1 = $v1;
            }

            if ($v2_l[1] > 5) {
                $s_v2 = (intval($v2_l[0]) + 0.5);
            }
            if ($v2_l[1] < 5) {
                $s_v2 = (intval($v2_l[0]) + 0);
            }
            if ($v2_l[1] == 5) {
                $s_v2 = $v2;
            }
            if ($v3_l[1] > 5) {
                $s_v3 = (intval($v3_l[0]) + 0.5);
            }
            if ($v3_l[1] < 5) {
                $s_v3 = (intval($v3_l[0]) + 0);
            }
            if ($v3_l[1] == 5) {
                $s_v3 = $v3;
            }
            if ($v4_l[1] > 5) {
                $s_v4 = (intval($v4_l[0]) + 0.5);
            }
            if ($v4_l[1] < 5) {
                $s_v4 = (intval($v4_l[0]) + 0);
            }
            if ($v4_l[1] == 5) {
                $s_v4 = $v4;
            }
            if ($v5_l[1] > 5) {
                $s_v5 = (intval($v5_l[0]) + 0.5);
            }
            if ($v5_l[1] < 5) {
                $s_v5 = (intval($v5_l[0]) + 0);
            }
            if ($v1_l[1] == 5) {
                $s_v5 = $v1;
            }
            // $evaluation_array['pace_result'] = $s_v1;
            // $evaluation_array['energy_result'] = $s_v2;
            // $evaluation_array['fillerword_result'] = $s_v3;
            // $evaluation_array['conciseness_result'] = $s_v4;
            // $evaluation_array['eyecontact_result'] = $s_v5;
              $evaluation_array['pace_result'] = ($s_v1 > 5)?5:$s_v1;
                $evaluation_array['energy_result'] = ($s_v2 > 5)?5:$s_v2;
                $evaluation_array['fillerword_result'] = ($s_v3 > 5)?5:$s_v3;
                $evaluation_array['conciseness_result'] = ($s_v4 > 5)?5:$s_v4;
                $evaluation_array['eyecontact_result'] = ($s_v5 > 5)?5:$s_v5;
            if ($s_v1[0] < 1) {
                $evaluation_array['pace_description'] = "Too fast for AI to understand. Slow down and add in pauses.";
            } else if ($s_v1[0] < 2) {
                $evaluation_array['pace_description'] = "Fast delivery, AI could understand some of the content. Add in more pauses.";
            } else if ($s_v1[0] < 3) {
                $evaluation_array['pace_description'] = " Moderate pace, try adding emphasis to key words.";
            } else if ($s_v1[0] < 4) {
                $evaluation_array['pace_description'] = "Good pace, well done!";
            } else if ($s_v1[0] <= 5) {
                $evaluation_array['pace_description'] = "Excellent pace, bravo! ";
            }

            if ($s_v2[0] < 1) {
                $evaluation_array['energy_description'] = "No energy in speech, add passion to your delivery";
            } else if ($s_v2[0] < 2) {
                $evaluation_array['energy_description'] = "Little energy, add rise and fall to your voice";
            } else if ($s_v2[0] < 3) {
                $evaluation_array['energy_description'] = "Moderate energy, be excited about your presentation";
            } else if ($s_v2[0] < 4) {
                $evaluation_array['energy_description'] = "Good energy and enthusiasm, great work!";
            } else if ($s_v2[0] <= 5) {
                $evaluation_array['energy_description'] = "Great energy and enthusiasm, wow!";
            }


            if ($s_v3[0] < 1) {
                $evaluation_array['fillerword_description'] = "Had a lot of filler words, take more pauses";
            } else if ($s_v3[0] < 2) {
                $evaluation_array['fillerword_description'] = "Had too many filler words, slow down your pace";
            } else if ($s_v3[0] < 3) {
                $evaluation_array['fillerword_description'] = "Had moderate filler words, add emphasis to key words";
            } else if ($s_v3[0] < 4) {
                $evaluation_array['fillerword_description'] = "Not bad at all, keep speaking!";
            } else if ($s_v3[0] <= 5) {
                $evaluation_array['fillerword_description'] = "Fantastic delivery!";
            }
            if ($s_v4[0] < 1) {
                $evaluation_array['conciseness_description'] = "Had a lot of filler phrases, focus on vocabulary in speech";
            } else if ($s_v4[0] < 2) {
                $evaluation_array['conciseness_description'] = "Had too many filler phrases or repetitive wording";
            } else if ($s_v4[0] < 3) {
                $evaluation_array['conciseness_description'] = "Had moderate or repetitive wording";
            } else if ($s_v4[0] < 4) {
                $evaluation_array['conciseness_description'] = "Concise and easy to follow, great job!";
            } else if ($s_v4[0] <= 5) {
                $evaluation_array['conciseness_description'] = "Conciseness award goes to you …Superb!";
            }

            if ($s_v5[0] < 1) {
                $evaluation_array['eyecontact_description'] = "Minimal eye contact; keep eyes on the camera area when speaking";
            } else if ($s_v5[0] < 2) {
                $evaluation_array['eyecontact_description'] = "Little eye contact detected; focus on the camera when speaking";
            } else if ($s_v5[0] < 3) {
                $evaluation_array['eyecontact_description'] = "Moderate eye contact, always keep your gaze straight ahead";
            } else if ($s_v5[0] < 4) {
                $evaluation_array['eyecontact_description'] = "Good eye contact, strong connection when speaking";
            } else if ($s_v5[0] <= 5) {
                $evaluation_array['eyecontact_description'] = "Great eye contact; you are very engaging!";
            }
            // print_r(round($min + mt_rand() / mt_getrandmax() * ($max - $min),2)); //Rand number in Flot like 2.74
            // die;

            $this->common_model->common_singleUpdate("tbl_user_speech", array("is_evaluated" => "yes"), array("id" => $speech_id));
        }
        $espeech_details = $this->common_model->common_singleSelect('tbl_user_evaluation', array('speech_id' => $speech_id, "is_deleted" => "0"));
        if (empty($espeech_details)) {
            $notification_data = array(
                'sender_id'        => '1',
                'sender_type'      => 'admin',
                'receiver_id'      => $speech_details['user_id'],
                'receiver_type'    => 'user',
                'primary_id'       => $speech_id,
                'notification_tag' => "speech_evaluations",
                'message'          => "Your " . $speech_details['title'] . " speech evaluation is completed",
                'title'            => 'Speech Evaluations'
            );
            $this->common_model->send_notification($notification_data);
            $send_email = array(
                'title'         => $speech_details['title'],
                'description'   => $speech_details['description'],
                "username"      => $user_details['username'],
                "email"        => $user_details['email']
            );

            // echo "<pre>";
            // print_r($evaluation_array);
            // die; 
            $message = $this->load->view('template/speechevaluation', $send_email, TRUE);
            $send_email = $this->common_model->send_mail(ADMIN_EMAIL, $user_details['email'], $message, 'Speech Evaluation');

            $this->common_model->common_insert('tbl_user_evaluation', $evaluation_array);
        }

        echo "success";
        die;
    }

    //Function is used to display user evaluation details by ID
    public function my_evaluations_details($evaluation_id)
    {
        $evaluation_id = base64_decode($evaluation_id);
        $data['evaluation_details'] = $this->user_model->get_evaluation_details($evaluation_id);
        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'my_evaluations_details', $data);
    }


    //Function is used to get user active subscription list
    public function subscription_active()
    {

        $user_id = $this->session->userdata('user_id');
        $data['active_subscriptions'] = $this->user_model->get_user_activeSubscriptionList($user_id);
        // echo "<pre>"; print_r($data); die;
        $subscriptions_list = array();
        $total_days = 0;
        if (!empty($data['active_subscriptions'])) {
            foreach ($data['active_subscriptions'] as $key => $value) {
                if ($value['duration'] == 'year') {
                    $total_days = (((int)365 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else if ($value['duration'] == 'month') {
                    $total_days = (((int)30 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else if ($value['duration'] == 'week') {
                    $total_days = (((int)7 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else {
                    $total_days = (((int)1 * (int)$value['validity_number']) - (int)$value['datediffer']);
                }

                if ($total_days > 1) {
                    if ($value['total_video'] >= $value['total_subscription_video']) {
                        array_push($subscriptions_list, $value);
                    }
                }
            }
        }
        $data['active_subscriptions'] = $subscriptions_list;
        $this->load->view($this->view_folder . 'subscription_active', $data);
    }

    public function cart_new(){
        echo "<pre>";print_r($this->input->post());die;
    }
    public function cart($item_type = 'abc')
    {

        $user_id = $this->session->userdata('user_id');
        $cart_datas = [];
        $cart_data_type = [];
        $data['result_card'] = $this->user_model->get_userCartDetails($user_id, $item_type);
        $data['tax'] = $this->common_model->get_tax_details($user_id);
        // echo "<pre>";print_r($data);die;

        foreach ($data['result_card'] as $key => $value) {
            if (!in_array($value['item_type'], $cart_data_type)) {
                // echo "<br>";
                // print_r($value['item_type']);
                array_push($cart_data_type, $value['item_type']);
                $ddata = $this->user_model->get_user_data($user_id, $value['item_type']);
                $cart_datas = array_merge($cart_datas, $ddata);
            }
        }
        if (!empty($cart_datas)) {
            $tax_details = $this->common_model->tax_details($user_id);
            if (!empty($tax_details)) {
                $cart_datas[0]['tax'] = $tax_details['tax'];
            } else {
                $cart_datas[0]['tax'] = 0;
            }
        }

        $data['result'] = $cart_datas;
        $data['total_points'] = $this->user_model->get_reward_points_new($user_id);
        $data['total_points']['total_point'] = (empty($data['total_points']['total_point']))?0:1;
        $data['cards'] = $this->common_model->common_multipleSelect("tbl_user_card", array("is_deleted" => "0", "user_id" => $user_id));
        if ($this->input->post()) {
            $all_post_data = $this->input->post();
               print_r($this->input->post());

            if ($data['total_points']['total_point'] < $this->input->post('redeem_points')) {
             
                $response['status'] = "validation_error";
                $response['description'] = "Your Redeem Points are " + $data['total_points']['total_point'];
                $this->load->view($this->view_folder . 'cart', $data);
                die;
            } else {
                if (empty($this->input->post('redeem_points'))) {
                }
                $cart_ids = json_decode(base64_decode($all_post_data['cart_id']));
                $this->form_validation->set_rules('cart_id', 'Cart ID', 'required|trim');
                $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');
                if (!empty($cart_ids)) {
                    if ($this->form_validation->run()) {
                        require_once APPPATH . '/libraries/Stripe/init.php';
                        $stripe = new \Stripe\StripeClient(PAYMENT_SECRET_KEY);
                        $card_data = $this->common_model->common_singleSelect('tbl_user_card', array('id' => $this->input->post('card_id')));
                        if (!empty($this->input->post('redeem_points')) && $this->input->post('redeem_points') != '0' && $this->input->post('redeem_points') != 0) {
                            $redeem_points = ($this->session->userdata("currency_rate") * ($this->input->post('redeem_points') / 10));
                            $total_amount = ($this->input->post('total_amount') - $redeem_points);
                        } else {
                            $redeem_points = 0;
                            $total_amount =  $this->input->post('total_amount');
                        }
                        if ($total_amount < 0) {
                            $total_amount = 0;
                        }
                        // $total_amount_new = ($this->session->userdata("currency_rate")*$total_amount);
                        $currency = strtolower($this->session->userdata("currency_data")['currency_code']);
                        $payment_obj = array(
                            "amount" => round($total_amount * 100),
                            "currency" => $currency,
                            "customer" => $card_data['customer_id'],
                            "description" => 'Place Order',
                        );
                        $cartarray = array(
                            'uniqueid'          => random_string('numeric', 6),
                            // 'item_id'           => $cart_data['item_id'],
                            'user_id'           => $user_id,
                            // 'item_type'         => $item_type,
                            'student_id'        => 0,
                            'transaction_id'    => 'abcd123456',
                            'card_id'           => $this->input->post('card_id'),
                            'payment_mode'      => 'Card',
                            'payment_status'    => 'succeeded',
                            'status'            => 'Confirm',
                            'subtotal'          => $this->input->post('subtotal'),
                            'tax'               => $this->input->post('tax'),
                            'discount'          => ($this->input->post('promocode_discount') + $redeem_points),
                            'totalamount'       => $total_amount,
                            'validity'          => !empty($itemsub_data) ? $validity_date : '',
                            'promocode'         => '0',
                            'student_id'        => 0,
                            'total_video'       => !empty($itemsub_data) ? $itemsub_data['total_video'] : '',
                            'remaining_video'   => !empty($itemsub_data) ? $itemsub_data['total_video'] : '',
                            'subscription_status' => 'Active',
                            // 'order_status'    => 'Paid'
                        );
                        if ($this->input->post('promocode_text') != '' && $this->input->post('promocode_text') != "undefined" && $this->input->post('promocode_discount') != 0) {
                            $cartarray['promocode'] = $this->input->post('promocode_text');
                            $data['promocode_data'] = $this->common_model->common_singleSelect("tbl_promocode", array("promocode" => $this->input->post('promocode_text')));
                            $this->common_model->common_insert("tbl_usedpromocode", array("user_id" => $user_id, "promocode_id" => $data['promocode_data']['id']));
                        }
                        if ($total_amount > 0) {
                            $payment_stripe = $stripe->charges->create($payment_obj);
                            $cartarray['transaction_id'] = $payment_stripe['id'];
                            $cartarray['receipt_link'] = $payment_stripe['receipt_url'];
                            $cartarray['order_token'] = $payment_stripe['balance_transaction'];
                            $cartarray['payment_datetime'] = date('Y-m-d H:i:s');
                            // $cartarray['order_status'] = 'Paid';
                        }

                        $cartarray['order_status'] = 'Paid';


                        $order_id = $this->common_model->common_insert('tbl_order', $cartarray);
                      
                        if (!empty($cart_ids)) {
                            foreach ($cart_ids as $key => $value) {

                                $order_item = array(
                                    "item_type" => $value->item_type,
                                    "item_id" => $value->item_id,
                                    "order_id" => $order_id,
                                    "price" => $value->price,
                                    "totalamount" => $total_amount
                                );
                                $this->common_model->common_insert('tbl_order_detail', $order_item);

                                if ($value->item_type == 'speech') {
                                    $this->common_model->common_singleUpdate("tbl_user_speech", array("is_deleted" => "0", "price" => $value->price), array("id" => $value->item_id));
                                }
                                if ($value->item_type == 'contest') {
                                    $contest_data = $this->common_model->common_singleSelect("tbl_master_contest", array("id" => $value->item_id));
                                    if ($contest_data['contest_type'] == 'live') {
                                        $contest_details = array(
                                            "user_id" => $user_id,
                                            "contest_id" => $value->item_id,
                                            "video" => "default.png",
                                            "thumb_image" => "default.png",
                                            "video_duration" => "0",

                                        );
                                        $this->common_model->common_insert("tbl_user_contest", $contest_details);
                                        echo "h2";
                                    }
                                }
                                echo "h1";
                                $this->common_model->common_delete('tbl_user_cart', array('id' => $value->cart_id));
                                if ($value->total_point != 0) {
                                    $wallet = array(
                                        "item_type" => $value->item_type,
                                        "item_id" => $value->item_id,
                                        "user_id" => $user_id,
                                        "order_id" => $order_id,
                                        "total_point" => $value->total_point,
                                        "status" => "credited",
                                        "redeem_points" => $value->total_point,
                                        "reward_points" => (int)$value->total_point
                                    );
                                    $this->common_model->common_insert('tbl_wallet', $wallet);
                                }
                            }
                            if ($this->input->post('redeem_points') != '0' && $this->input->post('redeem_points') != 0) {
                                $wallet = array(
                                    "item_type" => $value->item_type,
                                    "item_id" => $value->item_id,
                                    "user_id" => $user_id,
                                    "order_id" => $order_id,
                                    "total_point" => $value->total_point,
                                    "status" => "debited",
                                    "redeem_points" => $this->input->post('redeem_points'),
                                    "reward_points" => ((int)0 - (int)$this->input->post('redeem_points'))
                                );
                                $this->common_model->common_insert('tbl_wallet', $wallet);
                            }
                            $response['status'] = "success";
                            // $response['title'] = "Order Place";
                            $response['title'] = "Enrollment";
                            $response['description'] = "Your enrollment is successfull";
                            echo json_encode($response);
                            die;
                        }
                        $response['status'] = "success";
                        // $response['title'] = "Order Place";
                        $response['title'] = "Enrollment";
                        $response['description'] = "Your enrollment is successfull";
                        // $this->session->set_flashdata("success_msg",$this->lang->line('All Orders are Place'));
                        // $this->session->set_flashdata("success_msg",'All Orders are Place');
                        $this->session->set_flashdata("success_msg", 'Your enrollment is successfull');
                        echo json_encode($response);
                        // $response['description'] = "";
                        // redirect('dashboard');
                        die;
                    } else {
                        // echo "End";
                        $response['status'] = "validation_error";
                        $response['item_type'] = $item_type;
                        echo json_encode($response);
                        die;
                        // $response['item_id'] = $cart_data['item_id'];
                    }
                    // echo json_encode($response);
                    //  // redirect('dashboard');
                    // die;
                } else {
                    $response['status'] = "validation_error";
                    $response['description'] = "Cart can not have any order to add";
                    $this->load->view($this->view_folder . 'cart', $data);
                }
            }
        } else {
            $this->load->view($this->view_folder . 'cart', $data);
        }
    }

    public function deleted_all()
    {
        $user_id = $this->session->userdata('user_id');
        // $data['cards'] = $this->common_model->common_multipleSelect("tbl_user_card", array("is_deleted" => "0", "user_id" => $user_id));
        $data['result_card'] = $this->user_model->get_userCartDetails($user_id, 'abc');
        foreach ($data['result_card'] as $key => $value) {
            // echo "---------<br><pre>";
            // print_r($value);
            // if ($value['item_type'] == 'speech') {
            //     $this->common_model->common_singleUpdate('tbl_user_speech', array("is_deleted" => "1"), array('id' => $value['item_id']));
            //     $this->common_model->common_singleUpdate('tbl_user_evaluation', array("is_deleted" => "1"), array('speech_id' => $value['item_id']));
            // }
            $this->common_model->common_delete('tbl_user_cart', array('id' => $value['id']));
        }
        $this->session->set_flashdata("success_msg", $this->lang->line('website_item_delete_from_cart'));
        // echo $this->lang->line('website_item_delete_from_cart');
        $response['status'] = "success_msg";
        $response['message'] = $this->lang->line('website_item_delete_from_cart');
        echo json_encode($response);
        // echo"<pre>";print_r($data);
        die;
    }


    //Function is used to add items like class, subscription, contest, video, speech, promocode into cart
    public function add_item_intocart($item_type)
    {
        // echo $this->session->userdata('promocode_id'); die;

        $user_id = $this->session->userdata('user_id');
        // $this->common_model->common_delete('tbl_user_cart', array('user_id'=>$user_id));
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        if ($item_type == 'subscription') {

            $item_id = $this->input->post('subscription_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_subscription', array('id' => $item_id));
        } else if ($item_type == 'speech') {

            $item_id = $this->session->userdata('speech_id');
            $common_data = $this->common_model->common_singleSelect('tbl_user_speech', array('id' => $item_id));
        } else if ($item_type == 'video') {

            $item_id = $this->session->userdata('video_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_videolibrary', array('id' => $item_id));
        } else if ($item_type == 'promocode') {

            $item_id = $this->session->userdata('promocode_id');
            $common_data = $this->common_model->common_singleSelect('tbl_promocode', array('id' => $item_id));
        } else if ($item_type == 'class') {

            $item_id = $this->session->userdata('class_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $item_id));
        } else if ($item_type == 'contest') {

            $item_id = $this->session->userdata('contest_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $item_id));
        }

        $cartarray = array(
            'item_id'       => $item_id,
            'user_id'       => $user_id,
            'item_type'     => $item_type,
            'price'         => $common_data['price'],
            'student_id'    => 0,
        );
        if ($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != '') {
            $cartarray['student_id'] = $this->session->userdata('studentid');
        }
        if ($item_type == 'speech') {
            $cartarray['price'] = 10;
            $this->common_model->common_singleUpdate("tbl_user_speech", array("is_deleted" => "1"), array("id" => $item_id));
        }

        // print_r($cartarray);
        // die;
        $data['order_data'] = $this->common_model->get_order_details($item_type, $item_id, $user_id);
        // echo "P1";
        // print_r($data);
        if (empty($data['order_data'])) {
            $data['result_cart_data'] = $this->common_model->common_multipleSelect('tbl_user_cart', array('user_id' => $user_id, "is_deleted" => "0"));
            // echo "P2";
            foreach ($data['result_cart_data'] as $key => $value) {
                if ($cartarray['item_id'] == $value['item_id'] && $cartarray['item_type'] == $value['item_type']) {
                    $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_item_exist'));
                    redirect('cart');
                }
            }
            echo "P3";
            $data['active_subscriptions'] = $this->user_model->get_user_activeSubscriptionList($user_id);
            // echo "<pre>"; print_r($data); die;
            $subscriptions_list = array();
            $total_days = 0;
            if (!empty($data['active_subscriptions'])) {
                foreach ($data['active_subscriptions'] as $key => $value) {
                    if ($value['duration'] == 'year') {
                        $total_days = (((int)365 * (int)$value['validity_number']) - (int)$value['datediffer']);
                    } else if ($value['duration'] == 'month') {
                        $total_days = (((int)30 * (int)$value['validity_number']) - (int)$value['datediffer']);
                    } else if ($value['duration'] == 'week') {
                        $total_days = (((int)7 * (int)$value['validity_number']) - (int)$value['datediffer']);
                    } else {
                        $total_days = (((int)1 * (int)$value['validity_number']) - (int)$value['datediffer']);
                    }

                    if ($total_days > 1) {
                        if ($value['total_video'] >= $value['total_subscription_video']) {
                            array_push($subscriptions_list, $value);
                        }
                    }
                }
            }
            // echo "<pre>";
            // print_r($subscriptions_list);
            // die;
            if (empty($subscriptions_list)) {
                $this->common_model->common_insert('tbl_user_cart', $cartarray);
                $this->session->unset_userdata('speech_id');
                $this->session->unset_userdata('video_id');
                $this->session->unset_userdata('promocode_id');
                $this->session->unset_userdata('class_id');
                // $this->session->unset_userdata('contest_id');                
                redirect('cart/' . $item_type);
            } else {
                if ($item_type == 'subscription') {
                    $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_subscriptions_already_exist'));
                    $this->session->unset_userdata('speech_id');
                    $this->session->unset_userdata('video_id');
                    $this->session->unset_userdata('promocode_id');
                    $this->session->unset_userdata('class_id');
                    // $this->session->unset_userdata('contest_id');                
                    redirect('cart/' . $item_type);
                } else {
                    $this->common_model->common_insert('tbl_user_cart', $cartarray);
                    $this->session->unset_userdata('speech_id');
                    $this->session->unset_userdata('video_id');
                    $this->session->unset_userdata('promocode_id');
                    $this->session->unset_userdata('class_id');
                    // $this->session->unset_userdata('contest_id');                
                    redirect('cart/' . $item_type);
                }
            }
        } else {
            // die;
            $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_already_book'));
            $this->session->unset_userdata('speech_id');
            $this->session->unset_userdata('video_id');
            $this->session->unset_userdata('promocode_id');
            $this->session->unset_userdata('class_id');
            // $this->session->unset_userdata('contest_id');                
            redirect('cart/' . $item_type);
        }
    }

    public function add_item_intocart_old($item_type)
    {
        // echo $this->session->userdata('promocode_id'); die;

        $user_id = $this->session->userdata('user_id');
        // $this->common_model->common_delete('tbl_user_cart', array('user_id'=>$user_id));
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        if ($item_type == 'subscription') {

            $item_id = $this->input->post('subscription_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_subscription', array('id' => $item_id));
        } else if ($item_type == 'speech') {

            $item_id = $this->session->userdata('speech_id');
            $common_data = $this->common_model->common_singleSelect('tbl_user_speech', array('id' => $item_id));
        } else if ($item_type == 'video') {

            $item_id = $this->session->userdata('video_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_videolibrary', array('id' => $item_id));
        } else if ($item_type == 'promocode') {

            $item_id = $this->session->userdata('promocode_id');
            $common_data = $this->common_model->common_singleSelect('tbl_promocode', array('id' => $item_id));
        } else if ($item_type == 'class') {

            $item_id = $this->session->userdata('class_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $item_id));
        } else if ($item_type == 'contest') {

            $item_id = $this->session->userdata('contest_id');
            $common_data = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $item_id));
        }

        $cartarray = array(
            'item_id'       => $item_id,
            'user_id'       => $user_id,
            'item_type'     => $item_type,
            'price'         => $common_data['price'],
            'student_id'    => 0,
        );
        // print_r($cartarray);
        // die;
        $data['result_cart_data'] = $this->common_model->common_multipleSelect('tbl_user_cart', array('user_id' => $user_id, "is_deleted" => "0"));
        foreach ($data['result_cart_data'] as $key => $value) {
            if ($cartarray['item_id'] == $value['item_id'] && $cartarray['item_type'] == $value['item_type']) {
                $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_item_exist'));
                redirect('cart');
            }
        }

        $this->common_model->common_insert('tbl_user_cart', $cartarray);
        $this->session->unset_userdata('speech_id');
        $this->session->unset_userdata('video_id');
        $this->session->unset_userdata('promocode_id');
        $this->session->unset_userdata('class_id');
        // $this->session->unset_userdata('contest_id');                
        redirect('cart/' . $item_type);
    }

    //Function is used to user add credit/debit card 
    public function add_debit_card($item_type)
    {
        $user_id = $this->session->userdata('user_id');
        $data['item_type'] = $item_type;
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        if ($this->input->post()) {

            //     echo "<pre>"; print_r($this->input->post()); die;
            $this->form_validation->set_rules('card_holder_name', 'Card Holder Name', 'required|trim');
            $this->form_validation->set_rules('card_number', 'Card Number', 'required|trim');
            $this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required|trim');
            $this->form_validation->set_rules('security_code', 'Security Code', 'required|trim');
            $this->form_validation->set_error_delimiters('<span class="error" style="color:red">', '</span>');

            if ($this->form_validation->run()) {

                $date_array = explode('/', $this->input->post('expiry_date'));
                // echo "<pre>"; print_r($date_array); die;
                // require_once('application/third_party/stripe-php/init.php');
                require_once APPPATH . '/libraries/Stripe/init.php';
                $stripe = new \Stripe\StripeClient(PAYMENT_SECRET_KEY);
                try {
                    $tokendata = $stripe->tokens->create([
                        'card' => [
                            'number'    => $this->input->post('card_number'),
                            'exp_month' => $date_array[0],
                            'exp_year'  => $date_array[1],
                            'cvc'       => $this->input->post('security_code'),
                        ],
                    ]);

                    if (!empty($tokendata)) {

                        $is_carddata = $this->common_model->common_singleSelect('tbl_user_card', array('status' => 'Active', 'user_id' => $user_id, 'fingerprint' => $tokendata->card->fingerprint));

                        if (!empty($is_carddata)) {
                            $this->session->set_flashdata("error_msg", $this->lang->line('website_card_already_exists'));
                            redirect(base_url() . 'add-debit-card/' . $item_type);
                        } else {

                            $customerObject = array(
                                "source"        => $tokendata->id,
                                "email"         => $data['result']['email'],
                                "name"          => $data['result']['username'],
                                "phone"         => $data['result']['country_code'] . '' . $data['result']['phone'],
                                "description"   => PROJECT_NAME . " User #" . $user_id,
                                "address"       => array(
                                    "country"   => $data['result']['country'],
                                ),
                            );

                            $customer = $stripe->customers->create($customerObject);

                            if (!empty($customer)) {

                                // echo "<pre>"; print_r($customer); die;

                                $cardarray = array(
                                    "user_id"            => $user_id,
                                    "card_holder_name"  => $this->input->post('card_holder_name'),
                                    "card_number_last4" =>  $tokendata->card->last4,
                                    "fingerprint"       =>  $tokendata->card->fingerprint,
                                    "stripe_card_id"    =>  $tokendata->card->id,
                                    "card_token"        =>  $tokendata->id,
                                    "card_type"         =>  $tokendata->card->brand,
                                    "expiry_month"      =>  $date_array[0],
                                    "expiry_year"       =>  $date_array[1],
                                    "customer_id"       =>  $customer->id,
                                );


                                $this->common_model->common_insert('tbl_user_card', $cardarray);
                                // $data['item_type'] = $item_type;
                                $this->session->set_flashdata("success_msg", $this->lang->line('website_card_added_success'));
                                $this->load->view($this->view_folder . 'add_debit_card', $data);
                            } else {

                                $this->session->set_flashdata("error_msg", $this->lang->line('website_card_customer_not_created'));
                                $this->load->view($this->view_folder . 'add_debit_card', $data);
                            }
                        }
                    } else {
                        $this->session->set_flashdata("error_msg", $this->lang->line('website_card_token_not_created'));
                        $this->load->view($this->view_folder . 'add_debit_card', $data);
                    }
                } catch (Exception $e) {

                    $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_card_number_not_vaild'));
                    $this->load->view($this->view_folder . 'add_debit_card', $data);
                }
            } else {
                $this->load->view($this->view_folder . 'add_debit_card', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'add_debit_card', $data);
        }
    }

    //Function is used to add item like class, subscription, contest, video, speech, promocode into wishlist
    public function add_intowishlist($item_id, $item_type)
    {
        $user_id = $this->session->userdata('user_id');

        $cart_array = array(
            'user_id'       => $user_id,
            'item_id'       => $item_id,
            'item_type'     => $item_type,
        );
        $this->common_model->common_insert('tbl_user_wishlist', $cart_array);
        $this->session->set_flashdata("success_msg", $this->lang->line('website_item_save_inmy_wishlist'));
        redirect(base_url() . 'cart/' . $item_type);
    }

    //Function is used to remove or delete added item like class, subscription, contest, video, speech, promocode from cart
    public function remove_item_fromcart($cart_id)
    {
        // $data['cart_data'] = $this->common_model->common_singleSelect("tbl_user_cart",array("id"=>$cart_id));
        // // echo "<pre>";
        // // print_r($data['cart_data']['item_type']);
        // // die;
        // if($data['cart_data']['item_type'] == 'speech'){
        //      $this->common_model->common_singleUpdate('tbl_user_speech',array("is_deleted"=>"1"),array('id'=>$data['cart_data']['item_id']));
        //      $this->common_model->common_singleUpdate('tbl_user_evaluation',array("is_deleted"=>"1"),array('speech_id'=>$data['cart_data']['item_id']));  
        // }   
        $this->common_model->common_delete('tbl_user_cart', array('id' => $cart_id));

        $this->session->set_flashdata("success_msg", $this->lang->line('website_item_delete_from_cart'));
        echo $this->lang->line('website_item_delete_from_cart');
        die;
    }

    //Function is used to submit video for upcoming contest
    public function submission_video_for_contest($contest_id)
    {
        $contest_id = base64_decode($contest_id);
        $user_id = $this->session->userdata('user_id');
        $data['contest_details'] = $this->service_model->get_contest_details($contest_id);
        // echo "<pre>"; print_r($data); die;

        $video = '';
        if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
            $video = $this->common_model->uploadImageS3($_FILES['video'], CONTEST_IMAGE);
            if (!$video) {
                $this->session->set_flashdata("error_video_msg", $this->lang->line('website_speech_uploaded_failed'));
                $data['error_msg'] = $this->lang->line('website_speech_uploaded_failed');
                $this->load->view($this->view_folder . 'submission_video_for_contest', $data);
                return false;
            } else {
                $contest = array(
                    'contest_id'    => $contest_id,
                    'video'         => $video,
                    'user_id'       => $user_id
                );
                // echo "<pre>"; print_r($contest); die;
                $this->common_model->common_insert('tbl_user_contest', $contest);
                $this->session->set_userdata('upload_video', 'yes');
                // $this->session->unset_userdata('contest_id');
                $this->load->view($this->view_folder . 'submission_video_for_contest', $data);
            }
        }

        $this->load->view($this->view_folder . 'submission_video_for_contest', $data);
    }

    //Function is used to get user rewards point history
    public function points_history()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['history'] = $this->common_model->common_multipleSelect('tbl_wallet', array('user_id' => $user_id, 'is_deleted' => '0'));
        $this->load->view($this->view_folder . 'points_history', $data);
    }

    //Function is used to set session for age group in onlint classes listing
    public function apply_age_filter()
    {
        if (!empty($this->input->post('age_category'))) {
            $this->session->set_userdata('age_category', $this->input->post('age_category'));
        } else {
            $this->session->unset_userdata('age_category');
        }
        redirect(base_url() . 'online-class');
    }
    public function apply_age_filter_contest()
    {
        if (!empty($this->input->post('age_category_contest'))) {
            $this->session->set_userdata('age_category_contest', $this->input->post('age_category_contest'));
        } else {
            $this->session->unset_userdata('age_category_contest');
        }
        redirect(base_url() . 'contest-list');
    }
    //Function is used to reset user age group filter in online classes listing
    public function reset_age_filter()
    {
        $this->session->unset_userdata('age_category');
        redirect(base_url() . 'online-class');
    }

    public function reset_age_filter1()
    {
        $this->session->unset_userdata('age_category_contest');
        redirect(base_url() . 'contest-list');
    }


    //Function is used to get gift enrolled class details
    public function gift_class($class_id)
    {
        $data['class_id']       = base64_decode($class_id);
        $data['countries']      = $this->common_model->get_country_code();
        $user_id = $this->session->userdata('user_id');
        $userdata = $this->session->userdata('website_userdata');

        if ($this->input->post()) {

            // echo "<pre>"; print_r($_FILES); die;
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
            $this->form_validation->set_rules('country_code', 'Country Code', 'required|trim');
            $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[8]|max_length[12]');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');

            if ($this->form_validation->run()) {

                $email_exist = $this->common_model->common_singleSelect('tbl_user', array('email' => $this->input->post('email')));

                if (empty($email_exist)) {

                    $this->session->set_flashdata('error_msg', $this->lang->line('website_email_not_exist'));
                    redirect('gift-class/' . base64_encode($data['class_id']));
                } else if ($email_exist['age_category'] != $userdata['age_category']) {

                    $this->session->set_flashdata('error_msg', $this->lang->line('website_age_category_is_different'));
                    redirect('gift-class/' . base64_encode($data['class_id']));
                } else {
                    $giftdata = array(
                        'user_id'          => $user_id,
                        'class_id'         => $data['class_id'],
                        'receiver_id'      => $email_exist['id'],
                        'email'            => $this->input->post('email'),
                        'country_code'     => $this->input->post('country_code'),
                        'phone'            => $this->input->post('phone'),
                        'name'             => $this->input->post('name'),
                    );
                    // echo "<pre>"; print_r($giftdata); die;
                    $this->common_model->common_insert('tbl_gift_enrolledclass ', $giftdata);

                    $this->session->set_flashdata('success_msg', $this->lang->line('website_gift_send_success'));
                    redirect('gift-class/' . base64_encode($data['class_id']));
                }
            } else {
                $this->load->view($this->view_folder . 'gift_class', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'gift_class', $data);
        }
    }

    //Function is used to fetch user gift to enrolled class details
    public function gifted_enrolled_class_details($gift_id)
    {
        $gift_id = base64_decode($gift_id);
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['gift_details'] = $this->user_model->get_giftto_details_byID($gift_id);
        $this->load->view($this->view_folder . 'gifted_enrolled_class_details', $data);
    }

    //Function is used to fetch user gift received enrolled class details
    public function received_from_class_details($gift_id)
    {
        $gift_id = base64_decode($gift_id);
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['gift_details'] = $this->user_model->get_giftfrom_details_byID($gift_id);
        $this->load->view($this->view_folder . 'gifted_enrolled_class_details', $data);
    }

    //Function is used to remove giftclass and then redirect to evaluation list or add points into wallet
    public function remove_giftclass($type, $gift_id, $class_id)
    {
        $gift_id = base64_decode($gift_id);
        $class_id = base64_decode($class_id);
        $user_id = $this->session->userdata('user_id');
        $class_data = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $class_id));

        if ($type == 'wallet') {

            $wallet = array(
                'item_type'             => 'class',
                'item_id'               => $class_id,
                'user_id'               => $user_id,
                'order_id'              => 0,
                'total_point'           => $class_data['total_point'],
                'redeem_points'         => 0,
                'reward_points'         => $class_data['total_point'],
                'status'                => 'credited',
            );

            $this->common_model->common_insert('tbl_wallet', $wallet);
        }
        $this->common_model->common_delete('tbl_gift_enrolledclass', array('id' => $gift_id));
        redirect(base_url() . 'enrolled-classes');
    }

    //Function is used to display page of join classes and also display remote accessing user list
    public function joined_class($class_id)
    {
        $classes_id         = base64_decode($class_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $agora_token        = $this->common_model->get_agora_token($classes_id, $data['result']['subadmin_id'], 'class');
        $data['token']      = $agora_token['data']['token'];
        $data['channel']    = $agora_token['data']['channelname'];
        $data['userid']     = $data['result']['subadmin_id'];
        $data['host_id']     = $data['result']['host_id'];
        $data['class_id']   = $classes_id;
        $data['id'] = $this->session->userdata('user_id');
        $data['user_data'] = $this->common_model->common_singleSelect("tbl_user",array("id"=>$data['id']));
        // echo "<pre>"; print_r($data); die;
        $data['order_data'] = $this->common_model->get_order_details('class', $classes_id, $data['id']);
        if ($data['result']['is_host'] == '1') {
            
            $this->load->view($this->view_folder . 'joined_class', $data);
        } else {
            $this->session->set_flashdata('success_msg', $this->lang->line('admin_keywords_admin_not_host_class'));
            // $class_id = base64_encode($class_id);
            redirect("enrolled-class-details/".$class_id);
            // $this->enrolled_class_details($class_id);
        }
    }


    public function joined_contest($contest_id)
    {
        $contest_id         = base64_decode($contest_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $agora_token        = $this->common_model->get_agora_token($contest_id, $data['result']['subadmin_id'], 'contest');
        $data['token']      = $agora_token['data']['token'];
        $data['channel']    = $agora_token['data']['channelname'];
        $data['userid']     = $data['result']['subadmin_id'];
        $data['host_id']     = $data['result']['host_id'];
        $data['contest_id']   = $contest_id;
        $data['id'] = $this->session->userdata('user_id');
          $data['user_data'] = $this->common_model->common_singleSelect("tbl_user",array("id"=>$data['id']));
        $data['order_data'] = $this->common_model->get_order_details('contest', $contest_id, $data['id']);
        // echo "<pre>"; print_r($data); die;
        if ($data['result']['is_host'] == '1') {
            $this->load->view($this->view_folder . 'joined_contest', $data);
        } else {
            $this->session->set_flashdata('success_msg', $this->lang->line('admin_keywords_admin_not_host_contest'));
            $contest_id = base64_encode($contest_id);
            redirect("my-contest-detail/".$contest_id);

            // $this->my_contest_detail($contest_id);
        }
    }

    public function notification_setting()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('user_id' => $user_id, 'is_deleted' => '0'));
        if ($this->input->post()) {

            $setting_data = array(
                "user_id" => $user_id,
                "push_notification" => ($this->input->post('push_notification') == 'on') ? 'yes' : 'no',
                "email" => ($this->input->post('email') == 'on') ? 'yes' : 'no',
                "sms" => ($this->input->post('sms') == 'on') ? 'yes' : 'no'
            );
            if (!empty($data['notification_setting'][0]) && $data['notification_setting'][0] != '') {
                $this->common_model->common_singleUpdate('tbl_user_notification_setting', $setting_data, array("id" => $data['notification_setting'][0]['id']));
                $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('user_id' => $user_id, 'is_deleted' => '0'));
                $this->session->set_flashdata('success_msg', $this->lang->line('website_notification_updated_success'));
                $this->load->view($this->view_folder . 'notification_setting', $data);
            } else {
                $user_id = $this->common_model->common_insert('tbl_user_notification_setting', $setting_data);
                $data['notification_setting'] = $this->common_model->common_multipleSelect('tbl_user_notification_setting', array('id' => $user_id));
                $this->session->set_flashdata('success_msg', $this->lang->line('website_notification_updated_success'));
                $this->load->view($this->view_folder . 'notification_setting', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'notification_setting', $data);
        }
    }
    public function promocode_details()
    {
        $user_id = $this->session->userdata('user_id');
        $promocode_text = $this->input->post('promocode_text');
        $total_amount = $this->input->post('total_amount');
        $data['tax_data'] = $this->common_model->get_tax_details($user_id);
        $data['promocode_data'] = $this->common_model->common_singleSelect("tbl_promocode", array("promocode" => $promocode_text, "is_deleted" => "0", "(start_date >= now() OR end_date <= now())"));

        if (!empty($data['promocode_data'])) {
            $data["total_promocode_use"] = $this->common_model->common_multipleSelect("tbl_usedpromocode", array("promocode_id", $data['promocode_data']['id']));
            if (count($data["total_promocode_use"]) >= $data['promocode_data']['maxusage']) {
                $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_promocode_maxusagelimit_over'));
                $response['status'] = "error_msg";
                $response['message'] = $this->lang->line('rest_keywords_promocode_maxusagelimit_over');
                $response['total_amount'] = round($total_amount, 2);
                echo json_encode($response);
            } else {
                $data["total_use_promocode"] = $this->common_model->common_multipleSelect("tbl_order", array("user_id" => $user_id, "promocode" => $promocode_text, "is_deleted" => "0"));
                if (count($data["total_use_promocode"]) >= $data['promocode_data']["per_user_usage"]) {
                    $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_promocode_already_used'));
                    $response['status'] = "error_msg";
                    $response['message'] = $this->lang->line('rest_keywords_promocode_already_used');
                    $response['total_amount'] = round($total_amount, 2);
                    echo json_encode($response);
                } else {
                    $data["buy_promocode"] = $this->user_model->check_promocode($data['promocode_data']['id'], $user_id);
                    if (!empty($data['buy_promocode'])) {
                        if ($data['promocode_data']['discount_type'] == 'percentage') {

                            $total_p = (((float)$total_amount * (float)$data['promocode_data']['discount']) / 100);
                            $totala = ((float)$total_amount - (float)$total_p);
                            $response['status'] = "success_msg";
                            $response['message'] = $this->lang->line('rest_keywords_promocode_find');
                            $response['total_amount_promocode'] = round($total_p, 2);
                            $response['total_amount'] = round($totala, 2);
                            echo json_encode($response);
                            die;
                        } else {

                            $totala = ((float)$total_amount - ($data['tax_data']['rate'] * (float)$data['promocode_data']['discount']));
                            $response['status'] = "success_msg";
                            $response['message'] = $this->lang->line('rest_keywords_promocode_find');
                            $response['total_amount_promocode'] = round(($data['tax_data']['rate'] * (float)$data['promocode_data']['discount']), 2);
                            $response['total_amount'] = round($totala, 2);
                            echo json_encode($response);
                            die;
                        }
                    } else {
                        $data['user_promocode_list'] = $this->common_model->common_singleSelect("tbl_user_promocode", array("user_id" => $user_id, "promocode_id" => $data['promocode_data']['id']));
                        if (!empty($data['user_promocode_list'])) {
                            if ($data['promocode_data']['discount_type'] == 'percentage') {

                                $total_p = (((float)$total_amount * (float)$data['promocode_data']['discount']) / 100);
                                $totala = ((float)$total_amount - (float)$total_p);
                                $response['status'] = "success_msg";
                                $response['message'] = $this->lang->line('rest_keywords_promocode_find');
                                $response['total_amount_promocode'] = round($total_p, 2);
                                $response['total_amount'] = round($totala, 2);
                                echo json_encode($response);
                                die;
                            } else {

                                $totala = ((float)$total_amount - ($data['tax_data']['rate'] * (float)$data['promocode_data']['discount']));
                                $response['status'] = "success_msg";
                                $response['message'] = $this->lang->line('rest_keywords_promocode_find');
                                $response['total_amount_promocode'] = round(($data['tax_data']['rate'] * (float)$data['promocode_data']['discount']), 2);
                                $response['total_amount'] = round($totala, 2);
                                echo json_encode($response);
                                die;
                            }
                        } else {
                            $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_promocode_purchase'));
                            $response['status'] = "error_msg";
                            $response['message'] = $this->lang->line('rest_keywords_promocode_purchase');
                            $response['total_amount'] = round($total_amount, 2);
                            echo json_encode($response);
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_promocode_not_find'));
            // echo $this->lang->line('rest_keywords_promocode_not_find');
            // echo $total_amount;
            $response['status'] = "error_msg";
            $response['message'] = $this->lang->line('rest_keywords_promocode_not_find');
            $response['total_amount'] = round($total_amount, 2);
            echo json_encode($response);
            die;
        }
    }
    public function student_data_id($student_id)
    {
        // echo "ABC";
        $this->session->set_userdata('studentid', $student_id);
        $this->session->set_userdata("age_category", 0);
        $this->session->set_userdata("age_category_contest", 0);
        die;
    }

    public function my_suggested_details($suggested_id)
    {
        $suggested_id = base64_decode($suggested_id);
        $data['suggested'] = $this->common_model->common_singleSelect('tbl_suggested_courses', array('id' => $suggested_id));
        $this->load->view($this->view_folder . 'my_suggested_details', $data);
    }

    public function purchase_promo_codes_list()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        // echo $this->session->userdata('promocode_id'); die;
        $data['promocodes'] = $this->user_model->get_purchased_promocodes($user_id);
        // echo "<pre>"; print_r($data['promocodes']); die;
        $this->load->view($this->view_folder . 'purchase_promo_codes_list', $data);
    }
    public function apply_promocode()
    {
        echo "<pre>";
        print_r($this->session->userdata('promocode_id'));
        $data['promocode_details'] = $this->common_model->common_singleSelect('tbl_promocode', array('id' => $this->session->userdata('promocode_id')));

        $this->session->set_userdata('promocode_title', $data['promocode_details']['promocode']);

        redirect('cart');
    }
    public function purchase_promo_details_list($promo_id)
    {

        $promo_id = base64_decode($promo_id);

        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));

        $data['promocode_data'] = $this->user_model->get_promocode_details($promo_id, $user_id);

        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'purchase_promo_details_list', $data);
    }
    public function create_token($user_name)
    {
        // print_r($user_name);
        $token = $this->common_model->create_token(array("user_name" => $user_name));

        print_r($token);
        die;
    }
    public function user_data($user_id,$user_type){
        if($user_type == 'admin'){
           $data = $this->common_model->common_singleSelect("tbl_admin",array("id"=>"1")); 
        }else{
            $data = $this->common_model->common_singleSelect("tbl_user",array("id"=>$user_id));
        }
        // $data = $this->common_model->common_singleSelect("tbl_user",array("id"=>$user_id));
        echo json_encode($data);die;
    }
    public function check_user_place($id, $type){
        if($type == 'contest'){
            $data = $this->common_model->common_singleSelect("tbl_master_contest",array("id"=>$id));
        }else{
            $data = $this->common_model->common_singleSelect("tbl_master_class",array("id"=>$id));
        }
        echo json_encode($data);die;
    }
}
