<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 10-Feb-2022
** Modified On  : -  
*/
class Dashboard extends MY_Controller_Customer {

    private $view_folder = 'customer/dashboard/';
    //Default constructor
	function __construct()
    { 
        parent::__construct();
        $this->load->model('customer/service_model');
    }

    //load admin panel dashboard with all count.
    public function index()
    { 
        // echo "INDEX";
        // die;
        $this->session->unset_userdata('error_count_msg');
       
        // $data['contests'] = $this->common_model->get_contestlist();
         $data['user_data'] = $this->common_model->common_singleSelect('tbl_user',array("id"=>$this->session->userdata('user_id')));
         if($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
                $studentdata = $this->common_model->common_singleSelect("tbl_student",array("id"=>$this->session->userdata('studentid')));
                 $age_category = $studentdata['age_category'];   
        }else{
            $age_category = $data['user_data']['age_category'];
        }
         $data['classes'] = $this->common_model->common_multipleSelect('tbl_master_class',array('status'=>'Active','is_deleted'=>'0','age_category' => $age_category));
         // $student_data =  $this->db->select("*")->from('tbl_student')->where("user_id", $this->session->userdata('user_id'))->get()->result_array();
        $data['student_data'] = $this->common_model->common_singleSelect('tbl_student',array("user_id"=>$this->session->userdata('user_id')));
        
        $data['contests'] = $this->service_model->get_contestlist($age_category);
       $date_time = date("Y-m-d h:m:s");
       $data['contest_list'] = $this->db->select("*")->from('tbl_master_contest')->where(array("is_deleted"=>'0',"contest_startdate >"=>$date_time))->order_by("contest_startdate", "asc")->get()->result_array();
        $data['home_videos'] = $this->common_model->common_multipleSelect('tbl_home_video',array('is_deleted'=>'0'));
        $data['speech_contest'] = $this->service_model->speech_contest($this->session->userdata('user_id'));
          $date_time = $this->common_model->date_convert($data['speech_contest']['insert_datetime'], 'Y-m-d H:i:s',$this->session->userdata('website_timezone')); 
       $data['date_time'] = $date_time;    

        $this->load->view($this->view_folder.'dashboard',$data);
    }

    //Update user profile 
    public function my_profile()
    {   
        // echo "<pre>"; print_r($this->uri->segment(1)); die;
        $user_id = $this->session->userdata('user_id');
        $data['countries']      = $this->common_model->get_country_code_new();
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id'=>$user_id));

        if($this->input->post() && !empty($data['result'])){

            // echo "<pre>"; print_r($_FILES); die;
            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.id != '.$user_id.' AND is_deleted="0" AND email=]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'required|trim');     
    
            $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[8]|max_length[12]|is_unique[tbl_user.id != '.$user_id.' AND is_deleted="0" AND country_code ="'.$this->input->post('country_code').'" AND phone=]|max_length[14]');
           // $this->form_validation->set_rules('dob', 'Date of birth', 'required|trim'); 
            $this->form_validation->set_rules('country', 'Country', 'required|trim'); 
            $this->form_validation->set_rules('promocode', 'Promocode', 'trim'); 

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if ($this->form_validation->run()) {
                
                $profile_image = @$data['result']['profile_image']; 
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],USER_IMAGE);
                     //echo $profile_image; die;
                    if(!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

               
                $otp = rand(1000,9999);

                $customerdata = array(
                    'firstname'          => $this->input->post('firstname'),
                    'lastname'           => $this->input->post('lastname'),
                    'username'           => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'email'              => $this->input->post('email'),
                    'country_code'       => $this->input->post('country_code'),
                    'phone'              => $this->input->post('phone'),
                    'country'              => $this->input->post('country'),
                   // 'dob'                => $date_format,
                    'profile_image'      => $profile_image,
                   // 'age'                => $age,
                   // 'age_category'       => $age_category,
                   'is_dob_edit'        => '0',
                    'region_id'     => !empty($this->input->post('region_id')) ? $this->input->post('region_id') : "0",
                );
                if(@$data['result']['is_dob_edit'] == 1){
                     $date_format = date('Y-m-d', strtotime($this->input->post('dob')));
                $age  = date_diff(date_create($date_format), date_create('today'))->y;
                $age_category = '';

                if($age >= 0 && $age <= 8){
                    $age_category = 'Primary';
                }else if($age >= 9 && $age <= 12){
                    $age_category = 'Juniors';
                }else if($age >= 13 && $age <= 17){
                    $age_category = 'Teens';
                }else if($age >= 18){
                    $age_category = 'Adults';
                }
                $customerdata['dob'] = $date_format;
                $customerdata['age'] = $age;
                $customerdata['age_category'] = $age_category;
                }


                
                // echo "<pre>"; print_r($customerdata); die;
                $this->common_model->common_singleUpdate('tbl_user', $customerdata,  array('id'=>$user_id));
                 $user_data=$this->common_model->common_singleSelect('tbl_user', array('id'=>$user_id));
                   $currency_data = $this->common_model->get_currency_rate($user_data['country']);
                      $this->session->set_userdata("currency_data",$currency_data[0]);
                        if($currency_data[0]['rate'] != '' && $currency_data[0]['rate'] != 0 && $currency_data[0]['rate'] != undefined){
                            $this->session->set_userdata("currency_rate",$currency_data[0]['rate']);   
                        }else{
                         $this->session->set_userdata("currency_rate",1);   
                        }
                        $this->session->set_userdata("currency",$currency_data[0]['currency_symbol']);
                        $this->session->set_userdata('website_userdata',$user_data);

                if ($this->input->post('status') == 'Inactive') {
                    $userdeviceinfo = array('token'=>"",'device_token'=>"");
                    $this->common_model->save_user_deviceinfo($user_id,"U",$userdeviceinfo);
                }
                
                $this->session->set_flashdata('success_msg',$this->lang->line('website_profile_updated_success'));
               
                redirect('my-profile');   
            } else {
                $this->load->view($this->view_folder.'my_profile', $data);
            }
        }else{
            $this->load->view($this->view_folder.'my_profile', $data);
        }
        
    }

    //User create new password
    public function my_create_new_password(){
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id'=>$user_id));

        if($this->input->post()){

            $this->form_validation->set_rules('old_password','Old Password','required|trim|xss_clean');    
            $this->form_validation->set_rules('password','Password','required|trim|xss_clean');    
            $this->form_validation->set_error_delimiters('<div class = "error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

               
                    $password = trim($this->common_model->decrypt_password($data['result']['password']));

                    if($password == $this->input->post('old_password')) {

                        if($password != $this->input->post('password')) {

                            $user_array=array(
                                'password'      => $this->common_model->encrypt_password($this->input->post('password')),
                                'login_status'  => 'Offline'
                            );

                            $this->common_model->common_singleUpdate('tbl_user',$user_array,array('id'=>$user_id));
                            $this->common_model->common_singleUpdate('tbl_user_deviceinfo',array("token"=>"","device_token"=>""), array('user_id'=>$user_id));

                            $this->session->set_flashdata('success_msg',$this->lang->line('website_changepassword_success'));
                            redirect(base_url().'my-create-new-password');
                        } else {
                            $this->session->set_flashdata('error_msg',$this->lang->line('rest_keywords_cantreuse_oldpassword'));
                            $this->load->view($this->view_folder.'my_create_new_password', $data);
                        }
                    } else {
                        $this->session->set_flashdata('error_msg',$this->lang->line('website_changepassword_error'));
                        $this->load->view($this->view_folder.'my_create_new_password', $data);
                    }
                  
            } else {
                $this->load->view($this->view_folder.'my_create_new_password', $data);
            }
        } else {
           $this->load->view($this->view_folder.'my_create_new_password', $data);
        }
    }
    
    // Get notification list
    public function notification()
    { 
        $data['notifications_list'] = $this->common_model->common_multipleSelect('tbl_notification', array('receiver_type'=>'user',"receiver_id"=>$this->session->userdata('user_id')));
        $data['notifications_list_updata'] = $this->common_model->common_singleUpdate('tbl_notification',array('is_read'=>"read"),array('receiver_type'=>'user',"receiver_id"=>$this->session->userdata('user_id')));
        // echo "<pre>";
        // print_r($data);
        // die;
        $this->load->view($this->view_folder.'notification',$data);
    }

    //Clear single or all notification
    public function remove_notification($notify_id = ''){
        
        if(!empty($notify_id)){
            $this->common_model->common_delete('tbl_notification', array('id'=>$notify_id));
        }else{
            $this->common_model->common_delete('tbl_notification', array('is_deleted'=>'0'));
        }
        
        echo "success"; die;
    }
}
