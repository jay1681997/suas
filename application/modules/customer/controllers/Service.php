<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 10-Feb-2022
** Modified On  : -  
*/
require 'vendor/autoload.php';
class Service extends MY_Controller_Customer
{

    private $view_folder = 'customer/service/';
    //Default constructor
    function __construct()
    {
        parent::__construct();
        $this->load->model('customer/user_model');
        $this->load->model('customer/service_model');
    }

    //Add or upload speeches by user which is stored locally
    public function upload_speeches()
    {

        // $this->session->unset_userdata('speech_id');  

        $user_id = $this->session->userdata('user_id');
        $data['video_counts'] = $this->db->select("*")->from('tbl_user_speech')->where('user_id', $user_id)->where('status', 'Active')->get()->num_rows();

        $user_id = $this->session->userdata('user_id');
        $data['active_subscriptions'] = $this->user_model->get_user_activeSubscriptionList($user_id);
        $data['videos_count'] = $this->db->get_where('tbl_user_evaluation', array('user_id' => $user_id, 'is_deleted' => '0'))->num_rows();

        $subscriptions_list = array();
        $total_days = 0;

        if (!empty($data['active_subscriptions'])) {
            foreach ($data['active_subscriptions'] as $key => $value) {
                if ($value['duration'] == 'year') {
                    $total_days = (((int)365 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else if ($value['duration'] == 'month') {
                    $total_days = (((int)30 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else if ($value['duration'] == 'week') {
                    $total_days = (((int)7 * (int)$value['validity_number']) - (int)$value['datediffer']);
                } else {
                    $total_days = (((int)1 * (int)$value['validity_number']) - (int)$value['datediffer']);
                }

                if ($total_days > 1) {
                    if ($value['total_video'] >= $value['total_subscription_video']) {
                        array_push($subscriptions_list, $value);
                    }
                }
            }
        }
        $data['active_subscription'] = $subscriptions_list;
        $user_id = $this->session->userdata('user_id');
        $data['speech_list'] = $this->db->get_where('tbl_user_speech', array('user_id' => $user_id, 'is_deleted' => '0'))->result_array();
        if (empty($data['speech_list'])) {
            $data['total_video_pop'] = 1;
        } else {
            $data['total_video_pop'] = $data['speech_list'][0]['is_display_speech_pop'];
        }
        if ($this->input->post()) {

            $this->form_validation->set_rules('title', 'Title', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class = "error" style="color:#F44336;">', '</div>');

            if ($this->form_validation->run()) {
                $video = @$data['result']['video'];
                if (!empty($_FILES['video']) && $_FILES['video']['size'] > 0) {
                    $video = $this->common_model->uploadImageS3($_FILES['video'], SPEECH_IMAGE);

                    if (!$video) {
                        $this->session->set_flashdata("error_msg", $this->lang->line('website_speech_uploaded_failed'));
                        $data['error_msg'] = $this->lang->line('website_speech_uploaded_failed');
                        $this->load->view($this->view_folder . 'upload_speeches', $data);
                        return false;
                    }
                }
                $video_duration = '';
                // print_r($localtempfilename);
                $remotefilename = S3_BUCKET_ROOT . SPEECH_IMAGE . $video;
                if ($fp_remote = fopen($remotefilename, 'rb')) {
                    $localtempfilename = tempnam('/tmp', 'getID3');
                    if ($fp_local = fopen($localtempfilename, 'wb')) {
                        while ($buffer = fread($fp_remote, 8192)) {
                            fwrite($fp_local, $buffer);
                        }
                        fclose($fp_local);
                        // Initialize getID3 engine
                        $getID3 = new getID3;
                        $ThisFileInfo = $getID3->analyze($localtempfilename);
                        $video_duration = $ThisFileInfo['playtime_string'];
                        // Delete temporary file
                        unlink($localtempfilename);
                    }
                    fclose($fp_remote);
                }
                // print_r($video_duration);
                // print_r($ThisFileInfo);
                $imgname   = "";
                if (!empty($this->input->post('thumb_image'))) {
                    $thumb_image_data = $_POST['thumb_image'];
                    $thumb_image_data = str_replace('data:image/png;base64,', '', $thumb_image_data);
                    $thumb_image_data = str_replace(' ', '+', $thumb_image_data);
                    $thumb_image_data = base64_decode($thumb_image_data);
                    $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                    file_put_contents(THUMB_DIR . $thumb_media_name, $thumb_image_data);
                    $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, SPEECH_IMAGE);
                    $imgname = $uploaded_imagename;
                }

                if ($video_duration == '') {
                    $video_duration = $this->input->post('video_duration');
                }
                $speech = array(
                    'title'         => $this->input->post('title'),
                    'description'   => $this->input->post('description'),
                    'video'         => $video,
                    'video_duration' => $video_duration,
                    'user_id'       => $user_id,
                    'uploading_type' => $this->input->post('uploading_type'),
                    "thumb_image"   => $imgname,
                    "is_display_speech_pop"  => $data['total_video_pop'] + 1
                );
                //    echo "<pre>";print_r($speech);print_r($this->input->post());die;
                if ($this->input->post('uploading_type') == 'Manual' && !empty($data['active_subscription'][0])) {
                    $speech['subscription_id'] = $data['active_subscription'][0]['item_id'];
                }
                if ($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != '') {
                    $speech['student_id'] = $this->session->userdata('studentid');
                }

                $speech_id = $this->common_model->common_insert('tbl_user_speech', $speech);
                if ($data['total_video_pop'] == 3) {
                    $total_pop = 1;
                } else {
                    $total_pop = intval($data['total_video_pop']) + 1;
                }
                // echo "p2";
                $this->common_model->common_singleUpdate('tbl_user_speech', array("is_display_speech_pop" => $total_pop), array("user_id" => $user_id));
                $this->session->set_userdata('speech_id', $speech_id);
                // print_r($video_duration);
                // echo "p3";
                // die;
                if ($video_duration <= '2:00') {
                    // $this->session->unset_userdata('speech_id');
                    // echo "IF";
                    // die;
                    $this->session->set_flashdata("success_msg", $this->lang->line('website_speech_uploaded_success'));
                    redirect(base_url() . 'upload-speeches');
                } else {
                    // echo "ELSE";
                    // die;
                    if (empty($data['active_subscription'][0])) {
                        // echo "P1";
                        // die;
                        $this->session->set_flashdata("error_msg", $this->lang->line('website_speech_uploaded_failed'));
                        $data['error_msg'] = $this->lang->line('website_speech_uploaded_failed');
                        redirect(base_url() . 'upload-speeches');
                    } else {
                        // echo "P2";
                        // die;
                        $this->session->set_flashdata("success_msg", $this->lang->line('website_speech_uploaded_success'));
                        redirect(base_url() . 'upload-speeches');
                    }
                }
            } else {
                $this->load->view($this->view_folder . 'upload_speeches', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'upload_speeches', $data);
        }
    }

    public function submit_speech_evaluation()
    {
        $speech_id = $this->session->userdata('speech_id');
        $user_id = $this->session->userdata('user_id');
        $speech_details = $this->common_model->common_singleSelect('tbl_user_speech', array('id' => $speech_id));
        $user_details = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        if (!empty($speech_details)) {
            $evaluation_array = array(
                "speech_id" => $speech_id,
                "user_id" => $speech_details['user_id'],
                "student_id" => $speech_details['student_id'],
                "title" => $speech_details['title'],
                "description" => $speech_details['description'],
                "video" => $speech_details['video'],
                "thumb_image" => $speech_details['thumb_image']
            );
            if ($speech_details['uploading_type'] == 'AI') {
                $min = 1;
                $max = 4;
                $v1 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);
                $v2 = round(2 + mt_rand() / mt_getrandmax() * (4 - 2), 1);
                $v3 = round(0.5 + mt_rand() / mt_getrandmax() * (2 - 0.5), 1);
                $v4 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);
                $v5 = round(1 + mt_rand() / mt_getrandmax() * (3 - 1), 1);
                $v1_l = explode(".", $v1);
                $v2_l = explode(".", $v2);
                $v3_l = explode(".", $v3);
                $v4_l = explode(".", $v4);
                $v5_l = explode(".", $v5);
                if ($v1_l[1] > 5) {
                    $s_v1 = (intval($v1_l[0]) + 0.5);
                }
                if ($v1_l[1] < 5) {
                    $s_v1 = (intval($v1_l[0]) + 0);
                }
                if ($v1_l[1] == 5) {
                    $s_v1 = $v1;
                }

                if ($v2_l[1] > 5) {
                    $s_v2 = (intval($v2_l[0]) + 0.5);
                }
                if ($v2_l[1] < 5) {
                    $s_v2 = (intval($v2_l[0]) + 0);
                }
                if ($v2_l[1] == 5) {
                    $s_v2 = $v2;
                }
                if ($v3_l[1] > 5) {
                    $s_v3 = (intval($v3_l[0]) + 0.5);
                }
                if ($v3_l[1] < 5) {
                    $s_v3 = (intval($v3_l[0]) + 0);
                }
                if ($v3_l[1] == 5) {
                    $s_v3 = $v3;
                }
                if ($v4_l[1] > 5) {
                    $s_v4 = (intval($v4_l[0]) + 0.5);
                }
                if ($v4_l[1] < 5) {
                    $s_v4 = (intval($v4_l[0]) + 0);
                }
                if ($v4_l[1] == 5) {
                    $s_v4 = $v4;
                }
                if ($v5_l[1] > 5) {
                    $s_v5 = (intval($v5_l[0]) + 0.5);
                }
                if ($v5_l[1] < 5) {
                    $s_v5 = (intval($v5_l[0]) + 0);
                }
                if ($v1_l[1] == 5) {
                    $s_v5 = $v1;
                }
                $evaluation_array['pace_result'] = ($s_v1 > 5)?5:$s_v1;
                $evaluation_array['energy_result'] = ($s_v2 > 5)?5:$s_v2;
                $evaluation_array['fillerword_result'] = ($s_v3 > 5)?5:$s_v3;
                $evaluation_array['conciseness_result'] = ($s_v4 > 5)?5:$s_v4;
                $evaluation_array['eyecontact_result'] = ($s_v5 > 5)?5:$s_v5;
                if ($s_v1[0] < 1) {
                    $evaluation_array['pace_description'] = "Too fast for AI to understand. Slow down and add in pauses.";
                } else if ($s_v1[0] < 2) {
                    $evaluation_array['pace_description'] = "Fast delivery, AI could understand some of the content. Add in more pauses.";
                } else if ($s_v1[0] < 3) {
                    $evaluation_array['pace_description'] = " Moderate pace, try adding emphasis to key words.";
                } else if ($s_v1[0] < 4) {
                    $evaluation_array['pace_description'] = "Good pace, well done!";
                } else if ($s_v1[0] <= 5) {
                    $evaluation_array['pace_description'] = "Excellent pace, bravo! ";
                }

                if ($s_v2[0] < 1) {
                    $evaluation_array['energy_description'] = "No energy in speech, add passion to your delivery";
                } else if ($s_v2[0] < 2) {
                    $evaluation_array['energy_description'] = "Little energy, add rise and fall to your voice";
                } else if ($s_v2[0] < 3) {
                    $evaluation_array['energy_description'] = "Moderate energy, be excited about your presentation";
                } else if ($s_v2[0] < 4) {
                    $evaluation_array['energy_description'] = "Good energy and enthusiasm, great work!";
                } else if ($s_v2[0] <= 5) {
                    $evaluation_array['energy_description'] = "Great energy and enthusiasm, wow!";
                }


                if ($s_v3[0] < 1) {
                    $evaluation_array['fillerword_description'] = "Had a lot of filler words, take more pauses";
                } else if ($s_v3[0] < 2) {
                    $evaluation_array['fillerword_description'] = "Had too many filler words, slow down your pace";
                } else if ($s_v3[0] < 3) {
                    $evaluation_array['fillerword_description'] = "Had moderate filler words, add emphasis to key words";
                } else if ($s_v3[0] < 4) {
                    $evaluation_array['fillerword_description'] = "Not bad at all, keep speaking!";
                } else if ($s_v3[0] <= 5) {
                    $evaluation_array['fillerword_description'] = "Fantastic delivery!";
                }
                if ($s_v4[0] < 1) {
                    $evaluation_array['conciseness_description'] = "Had a lot of filler phrases, focus on vocabulary in speech";
                } else if ($s_v4[0] < 2) {
                    $evaluation_array['conciseness_description'] = "Had too many filler phrases or repetitive wording";
                } else if ($s_v4[0] < 3) {
                    $evaluation_array['conciseness_description'] = "Had moderate or repetitive wording";
                } else if ($s_v4[0] < 4) {
                    $evaluation_array['conciseness_description'] = "Concise and easy to follow, great job!";
                } else if ($s_v4[0] <= 5) {
                    $evaluation_array['conciseness_description'] = "Conciseness award goes to you …Superb!";
                }

                if ($s_v5[0] < 1) {
                    $evaluation_array['eyecontact_description'] = "Minimal eye contact; keep eyes on the camera area when speaking";
                } else if ($s_v5[0] < 2) {
                    $evaluation_array['eyecontact_description'] = "Little eye contact detected; focus on the camera when speaking";
                } else if ($s_v5[0] < 3) {
                    $evaluation_array['eyecontact_description'] = "Moderate eye contact, always keep your gaze straight ahead";
                } else if ($s_v5[0] < 4) {
                    $evaluation_array['eyecontact_description'] = "Good eye contact, strong connection when speaking";
                } else if ($s_v5[0] <= 5) {
                    $evaluation_array['eyecontact_description'] = "Great eye contact; you are very engaging!";
                }
            }
            $espeech_details = $this->common_model->common_singleSelect('tbl_user_evaluation', array('speech_id' => $speech_id, "is_deleted" => "0"));
            if (empty($espeech_details)) {
                $this->common_model->common_singleUpdate("tbl_user_speech", array("is_evaluated" => "yes"), array("id" => $speech_id));
                $notification_data = array(
                    'sender_id'        => '1',
                    'sender_type'      => 'admin',
                    'receiver_id'      => $speech_details['user_id'],
                    'receiver_type'    => 'user',
                    'primary_id'       => $speech_id,
                    'notification_tag' => "speech_evaluations",
                    'message'          => "Your " . $speech_details['title'] . " speech evaluations is completed",
                    'title'            => 'Speech Evaluations'
                );
                $this->common_model->send_notification($notification_data);
                $send_email = array(
                    'title'         => $speech_details['title'],
                    'description'   => $speech_details['description'],
                    "username"      => $user_details['username'],
                    "email"        => $user_details['email']
                );
                // echo "<pre>";
                // print_r($evaluation_array);
                // die; 
                $message = $this->load->view('template/speechevaluation', $send_email, TRUE);
                $send_email = $this->common_model->send_mail(ADMIN_EMAIL, $user_details['email'], $message, 'Speech Evaluation');

                $evalution_id = $this->common_model->common_insert('tbl_user_evaluation', $evaluation_array);
                $id = base64_encode($evalution_id);
            } else {
                $id = base64_encode($espeech_details['id']);
            }

            echo $id;
            // redirect(base_url() . 'my-evaluations-details/'+$id);
            // echo "success"; die;
        } else {
            $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_failed_speech_evaluation'));
            $data['error_msg'] = $this->lang->line('rest_keywords_failed_speech_evaluation');
            $this->load->view($this->view_folder . 'upload_speeches', $data);
            return false;
            // die; 
        }
        // echo "ANc";
        // die;
    }

    //Delete uploaded speech by it's ID
    public function delete_speech_uploaded()
    {
        $this->session->userdata('speech_id');
        $this->common_model->common_delete('tbl_user_speech', array('id' => $this->session->userdata('speech_id')));
        $this->session->unset_userdata('speech_id');
        redirect(base_url() . 'upload-speeches');
    }

    // Get online classes list
    public function online_class()
    {
        $userdata = $this->session->userdata('website_userdata');
        if (empty($this->session->userdata("age_category"))) {
            if ($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != '') {
                $studentdata = $this->common_model->common_singleSelect("tbl_student", array("id" => $this->session->userdata('studentid')));
                $this->session->set_userdata("age_category", $studentdata['age_category']);
            } else {
                $this->session->set_userdata("age_category", $userdata['age_category']);
            }
        }
        $age_category = $this->session->userdata("age_category");
        $data['classes'] = $this->common_model->common_multipleSelect('tbl_master_class', array('status' => 'Active', 'is_deleted' => '0', 'age_category' => $age_category));
        // $this->session->set_userdata("age_category",$userdata['age_category']);
        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'online_class', $data);
    }

    // Fetch online class details by ID
    public function online_class_detail($class_id)
    {
        $class_id = base64_decode($class_id);
        $data = array();
        $data['class_details'] = $this->service_model->get_class_details($class_id);
        // if($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
        //         $studentdata = $this->common_model->common_singleSelect("tbl_student",array("id"=>$this->session->userdata('studentid')));
        //            if($studentdata['age_category'] != $data['class_details']['age_category']){
        //                 redirect(base_url().'dashboard');
        //            }    
        // }

        $data['order_data'] = $this->common_model->get_order_details("class", $class_id, $this->session->userdata('user_id'));
        // echo "<pre>"; print_r($data); die;
        // echo "<pre>"; print_r($data);
        // print_r($class_id);
        //  die;

        $this->load->view($this->view_folder . 'online_class_detail', $data);
    }

    //Get contest list like live or video
    public function contest_list()
    {
        // print_r($this->session->userdata('studentid'));
        // die;
        if ($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != '') {
            $data['user_data'] = $this->common_model->common_singleSelect('tbl_student', array("id" => $this->session->userdata('studentid')));
        } else {
            $data['user_data'] = $this->common_model->common_singleSelect('tbl_user', array("id" => $this->session->userdata('user_id')));
        }
        if (!empty($this->session->userdata("age_category_contest"))) {

            $data['contests'] = $this->service_model->get_contestlist($this->session->userdata("age_category_contest"));
        } else {

            $data['contests'] = $this->service_model->get_contestlist($data['user_data']['age_category']);
            $this->session->set_userdata("age_category_contest", $data['user_data']['age_category']);
        }
        // $data['contests'] = $this->service_model->get_contestlist($data['user_data']['age_category']);
        // echo "<pre>"; print_r($data); die;
        $this->load->view($this->view_folder . 'contest_list', $data);
    }

    //Fetch contest details as LIVE or VIDEO by it's ID
    public function contests_live_details($contest_id)
    {
        if ($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != '') {
            $data['user_data'] = $this->common_model->common_singleSelect('tbl_student', array("id" => $this->session->userdata('studentid')));
        } else {
            $data['user_data'] = $this->common_model->common_singleSelect('tbl_user', array("id" => $this->session->userdata('user_id')));
        }

        $contest_id = base64_decode($contest_id);
        $data['contest_details'] = $this->service_model->get_contest_details($contest_id);
        $data['order_data'] = $this->common_model->get_order_details("contest", $contest_id, $this->session->userdata('user_id'));
        $data['upload_speeches'] = $this->common_model->common_multipleSelect("tbl_user_contest", array("user_id" => $this->session->userdata('user_id'), "contest_id" => $contest_id));
        // echo "<pre>"; print_r($data); die;
        if ($data['contest_details']['age_category'] == 'Primary') {
            $data['age_category_group'] = 'Ages 5 to 8';
        } else if ($data['contest_details']['age_category'] == 'Junior') {
            $data['age_category_group'] = 'Ages 9 to 12';
        } else if ($data['contest_details']['age_category'] == 'Teens') {
            $data['age_category_group'] = 'Ages 13 to 17';
        } else if ($data['contest_details']['age_category'] == 'Adults') {
            $data['age_category_group'] = 'Ages 18+ ';
        } else {
            $data['age_category_group'] = 'Ages 18+ ';
        }
        $this->load->view($this->view_folder . 'contests_live_details', $data);
    }

    // Get leaderboard list
    public function my_leaderboard()
    {
        $user_id = $this->session->userdata('user_id');
        $data['result'] = $this->common_model->common_singleSelect('tbl_user', array('id' => $user_id));
        $data['leaderboards'] = $this->common_model->common_multipleSelect('tbl_leaderboard', array('status' => 'Active', 'is_deleted' => '0'));
        $this->load->view($this->view_folder . 'my_leaderboard', $data);
    }

    //Display sponsors page 
    public function sponsors()
    {
        $this->load->view($this->view_folder . 'sponsors');
    }

    // Get scorecard of contest using contest ID
    public function score_card_for_contest($contest_id)
    {
        $contest_id = base64_decode($contest_id);
        $data['contest'] = $this->common_model->common_singleSelect('tbl_master_contest',array("id"=>$contest_id));
        $scorecards = array();
        $scorecard_list = explode(",",$data['contest']['scorecard_id']);
        // print_r($scorecard_list);
        foreach ($scorecard_list as $key => $value) {
            $data1 = $this->common_model->common_singleSelect('tbl_contest_scorecard',array("id"=>$value));
            array_push($scorecards,$data1);
        }
        // print_r($scorecards);
        // die;
        $data['scorecards'] = $scorecards;
        // $data['scorecards'] = $this->common_model->common_multipleSelect('tbl_contest_scorecard ', array('status' => 'Active', 'is_deleted' => '0', 'contest_id' => $contest_id));
        $this->load->view($this->view_folder . 'score_card_for_contest', $data);
    }

    // Get main prize list which is used to redeem reward points
    public function prize_list()
    {

        $data['prize_list'] = $this->service_model->get_prizelist();
        $this->load->view($this->view_folder . 'prize_list', $data);
    }

    // Fetch prize details by using ID
    public function prize_detail($prize_id)
    {
        $prize_id = base64_decode($prize_id);
        $data['prize_details'] = $this->common_model->common_singleSelect('tbl_master_prize', array('id' => $prize_id));
        $this->load->view($this->view_folder . 'prize_detail', $data);
    }

    //Set session for reward points in prize list and filter data
    public function apply_points_filter()
    {
        if (!empty($this->input->post('points'))) {
            $this->session->set_userdata('points', $this->input->post('points'));
        } else {
            $this->session->unset_userdata('points');
        }
        redirect(base_url() . 'prize-list');
    }

    //Reset prize list reward points filter
    public function reset_points_filter()
    {
        $this->session->unset_userdata('points');
        redirect(base_url() . 'prize-list');
    }

    // Add reward points into wallet when redeem points of prize
    public function add_reward_points($prize_id)
    {
        $prize_id = base64_decode($prize_id);

        $user_id = $this->session->userdata('user_id');
        $prizess = $this->common_model->common_singleSelect('tbl_master_prize', array('id' => $prize_id));
        $wallet = array(
            'item_type'             => 'prize',
            'item_id'               => $prize_id,
            'user_id'               => $user_id,
            'order_id'              => 0,
            'total_point'           => $prizess['reward_point'],
            'redeem_points'         => 0,
            'reward_points'         => $prizess['reward_point'],
            'status'                => 'debited',
        );

        $this->common_model->common_insert('tbl_wallet', $wallet);

        redirect(base_url() . 'my-reward-points');
    }
    public function upload_contest_video($contest_id)
    {
        $contest_id = base64_decode($contest_id);
        if (!empty($_FILES['media']) && $_FILES['media']['size'] > 0) {
            $uploaded_filename = $this->common_model->uploadImageS3($_FILES['media'], CONTEST_IMAGE);
            if (!$uploaded_filename) {
                $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                $this->load->view($this->view_folder . 'add', $data);
                return false;
            }

            if (!empty($uploaded_filename) && $uploaded_filename != "") {
                //FOR video and its IMAGE
                if (!empty($this->input->post('thumb_image'))) {
                    $data = $_POST['thumb_image'];
                    //list($type, $data) = explode(';', $data);
                    //list(, $data)      = explode(',', $data);
                    $data = str_replace('data:image/png;base64,', '', $data);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                    file_put_contents(THUMB_DIR . $thumb_media_name, $data);
                    $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, CONTEST_IMAGE);
                    $imgname = $uploaded_imagename;
                }
                $videoname = $uploaded_filename;
            } else {
                redirect(base_url() . 'contests-live-details' / base64_encode($contest_id));
                // $this->load->view($this->view_folder.'add', $data);
            }
        }
        $video_duration = '';


        $remotefilename = S3_BUCKET_ROOT . CONTEST_IMAGE . $videoname;
        if ($fp_remote = fopen($remotefilename, 'rb')) {
            $localtempfilename = tempnam('/tmp', 'getID3');
            if ($fp_local = fopen($localtempfilename, 'wb')) {
                while ($buffer = fread($fp_remote, 8192)) {
                    fwrite($fp_local, $buffer);
                }
                fclose($fp_local);
                // Initialize getID3 engine
                $getID3 = new getID3;
                $ThisFileInfo = $getID3->analyze($localtempfilename);
                $video_duration = $ThisFileInfo['playtime_string'];
                // Delete temporary file
                unlink($localtempfilename);
            }
            fclose($fp_remote);
        }

        if (!empty($imgname)) {
            $postdata = array(
                "user_id" => $this->session->userdata('user_id'),
                "contest_id" => $contest_id,
                "video" => $videoname,
                "thumb_image" => $imgname,
                "video_duration" => $video_duration
            );
            $this->common_model->common_insert("tbl_user_contest", $postdata);
            $this->session->set_flashdata("success_msg", $this->lang->line('rest_keywords_contest_video_upload_success'));
            redirect(base_url() . "contests-live-details/" . base64_encode($contest_id));
        } else {
            $this->session->set_flashdata("error_msg", $this->lang->line('rest_keywords_contest_video_upload_failed'));
            redirect(base_url() . "contests-live-details/" . base64_encode($contest_id));
        }
    }
}
