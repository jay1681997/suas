<?php
/*
** Project      : SUAS
** Date         : 10-August-2022
** Modified On  : -  
*/
class Service_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_class_details($class_id){
        $user_id = $this->session->userdata('user_id');
        
        $this->db->select("tc.*, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, COUNT(tor.id) as booked_class")->from('tbl_master_class tc');
        $this->db->join('tbl_order tor', 'tc.id = tor.item_id AND tor.item_type = "class"','left');
        $this->db->where('tc.id',$class_id);
        
        $qry= $this->db->get()->row_array();

        if(!empty($qry)){
            $this->db->select("*");
            $this->db->from('tbl_order');
            $this->db->where('item_id',$class_id);
            $this->db->where('item_type','class');
            $this->db->where('status','Confirm');
            $this->db->where('user_id',$user_id);
            $qry['is_purchased'] =  $this->db->get()->row_array();

        }
        return $qry;
    }

    function get_contest_details($contest_id){
        $this->db->select("tc.*, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image,CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.thumb_image) as contest_thumb_image, TIMEDIFF(tc.contest_enddate, now()) as remaining_time");
        $this->db->from('tbl_master_contest tc');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tc.id',$contest_id);
        $result =  $this->db->get()->row_array();

        if(!empty($result)){
            $this->db->select("tp.*, CONCAT('".S3_BUCKET_ROOT.PRIZE_IMAGE."','',tp.prize_image) as prize_image");
            $this->db->from('tbl_prize tp');
            $this->db->where('tp.status','Active');
            $this->db->where('tp.is_deleted','0');
            $this->db->where('tp.contest_id',$contest_id);
            $result['prizes'] =  $this->db->get()->result_array();

            
            $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");
             $this->db->from('tbl_master_contest tc');
            $this->db->join('tbl_sponsor ts','ts.id = tc.sponsor_id','left');
            $this->db->where('ts.status','Active');
            $this->db->where('ts.is_deleted','0');
            $this->db->where('tc.id', $contest_id);
            $sponsor = $this->db->get()->row_array();
            $result['sponsors']=!empty($sponsor) ? $sponsor : array();
            

            // echo "<pre>"; print_r($this->db->last_query()); die;
            return $result;
        }
        
    }


    function get_prizelist(){
        $this->db->select("tc.*");
        $this->db->from('tbl_master_prize tc');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');

        if(!empty($this->session->userdata('points'))){
            
            if($this->session->userdata('points') == '50'){
                $this->db->where('tc.reward_point >= 0 AND tc.reward_point <= 50');
            }else if($this->session->userdata('points') == '200'){
                $this->db->where('tc.reward_point >= 100 AND tc.reward_point <= 200');
            }else if($this->session->userdata('points') == '300'){
                $this->db->where('tc.reward_point >= 200 AND tc.reward_point <= 300');
            }else if($this->session->userdata('points') == '400'){
                $this->db->where('tc.reward_point >= 300 AND tc.reward_point <= 400');
            }else if($this->session->userdata('points') == '500'){
                $this->db->where('tc.reward_point > 500');
            }
        }
        $this->db->group_by('tc.id');
        $this->db->order_by('tc.id','DESC');
        return  $this->db->get()->result_array();
    }

       function speech_contest($user_id){
        $this->db->select("te.*");
        $this->db->from('tbl_user_evaluation te');
        $this->db->join('tbl_user_speech ts','te.speech_id = ts.id','left');
        $this->db->where('te.user_id',$user_id);
        $this->db->where('te.is_deleted','0');
        $this->db->order_by('te.id','DESC');
         return $this->db->get()->row_array();
    }



    function get_contestlist($age_category){
        $this->db->select("tc.*, TIMEDIFF(tc.contest_enddate, now()) as remaining_time");
        $this->db->from('tbl_master_contest tc');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');
        // $this->db->where('tc.age',$age);
        $this->db->where('tc.age_category',$age_category);
        $this->db->group_by('tc.id');
        $this->db->order_by('tc.id','DESC');
        $result =  $this->db->get()->result_array();
        if(!empty($result)){
            $sponsors = array();
            foreach ($result as $key => $value) {
                $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");
                // $this->db->from('tbl_sponsor ts');
                     $this->db->from('tbl_master_contest tc');
                $this->db->join('tbl_sponsor ts','ts.id = tc.sponsor_id','left');
                $this->db->where('ts.status','Active');
                $this->db->where('ts.is_deleted','0');
                $this->db->where('tc.id', $value['id']);
                $sponsor = $this->db->get()->row_array();
                $result[$key]['sponsors']=!empty($sponsor) ? $sponsor : array();
            } 
        }
        return $result;
    }
}