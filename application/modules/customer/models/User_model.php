<?php
/*
** Project      : SUAS
** Date         : 10-August-2022
** Modified On  : -  
*/
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_total_speech_count($user_id)
    {
        $this->db->select('*')->from('tbl_user_speech');
        $this->db->where('status','Active');
        $this->db->where('is_deleted','0');
        $this->db->where('is_evaluated','no');
          if($this->session->userdata('studentid') != '' && $this->session->userdata('studentid') != 0){
            $this->db->where('student_id',$this->session->userdata('studentid'));
        }
        $this->db->where('user_id',$user_id);
        return $this->db->get()->num_rows();
    }

    function get_speech_list($user_id, $limit, $start){
        $this->db->select('*')->from('tbl_user_speech');
        $this->db->where('is_deleted','0');
        $this->db->where('status','Active');
        // $this->db->where('is_evaluated','no');
        $this->db->where('user_id',$user_id);
        if($this->session->userdata('studentid') != '' && $this->session->userdata('studentid') != 0){
            $this->db->where('student_id',$this->session->userdata('studentid'));
        }
        $this->db->group_by('id');
        $this->db->order_by('id','DESC');
        $this->db->limit($start,$limit);
        return $this->db->get()->result_array();
    }

    function get_total_evaluation_count($user_id){
          $this->db->select('te.*, ts.video_duration')->from('tbl_user_evaluation te');
        $this->db->join('tbl_user_speech ts', 'te.speech_id = ts.id','left');
        $this->db->where('te.is_deleted','0');
        $this->db->where('te.status','Active');
          if($this->session->userdata('studentid') != '' && $this->session->userdata('studentid') != 0){
            $this->db->where('ts.student_id',$this->session->userdata('studentid'));
        }
        $this->db->where('te.user_id',$user_id);
        return $this->db->get()->num_rows();
    }

    function get_speech_evaluationlist($user_id, $limit, $start){
        $this->db->select('te.*, ts.video_duration')->from('tbl_user_evaluation te');
        $this->db->join('tbl_user_speech ts', 'te.speech_id = ts.id','left');
        $this->db->where('te.is_deleted','0');
        $this->db->where('te.status','Active');
        $this->db->where('te.user_id',$user_id);
        if($this->session->userdata('studentid') != '' && $this->session->userdata('studentid') != 0){
            $this->db->where('ts.student_id',$this->session->userdata('studentid'));
        }
        $this->db->limit($start,$limit);
        $this->db->group_by('te.id');
        $this->db->order_by('te.id','DESC');
        return $this->db->get()->result_array();
    }


    function get_userCartDetails($user_id, $item_type){
        if($item_type == 'speech'){

            $query = "tuc.*, tus.title as name, tus.price, tus.description, CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.video) as image , CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.thumb_image) as thumb_image  ";

        }else if($item_type == 'subscription'){

            $query = "tuc.*, ts.name, ts.price, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','','default.png') as image, ts.description";
        }else if($item_type == 'video'){

            $query = "tuc.*, tv.name, IFNULL(tv.total_point, '0') as total_point, tv.price, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.video) as image , CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.image) as thumb_image, tv.description";
        }else if($item_type == 'promocode'){

            $query = "tuc.*, (tp.promocode) as name, tp.price, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as image, tp.description";
        }else if($item_type == 'class'){

            $query = "tuc.*, (tmc.program_title) as name,IFNULL(tmc.total_point, '0') as total_point, tmc.price, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tmc.class_image) as image, tmc.description";

        }else if($item_type == 'contest'){

            $query = "tuc.*, tc.name, tc.price,IFNULL(tc.total_point, '0') as total_point, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as image, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.thumb_image) as thumb_image, tc.description";
        }

        $this->db->select("tuc.*, (SELECT tax from tbl_setting where is_deleted = '0' and status='Active') as tax, (SELECT credit_point_discount from tbl_setting where is_deleted = '0' and status='Active') as credit_point_discount, ".$query."")->from('tbl_user_cart tuc');

        if($item_type == 'speech'){

            $this->db->join('tbl_user_speech tus',' tuc.item_id = tus.id', 'left');

        }else if($item_type == 'subscription'){

            $this->db->join('tbl_master_subscription ts',' tuc.item_id = ts.id', 'left');

        }else if($item_type == 'video'){

            $this->db->join('tbl_master_videolibrary tv',' tuc.item_id = tv.id', 'left');

        }else if($item_type == 'promocode'){

            $this->db->join('tbl_promocode tp',' tuc.item_id = tp.id', 'left');

        }else if($item_type == 'class'){

            $this->db->join('tbl_master_class tmc',' tuc.item_id = tmc.id', 'left');

        }else if($item_type == 'contest'){

            $this->db->join('tbl_master_contest tc',' tuc.item_id = tc.id', 'left');

        }

        $this->db->where('tuc.user_id',$user_id);
        if($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
            $this->db->where('tuc.student_id',$this->session->userdata('studentid'));
        }
        // $this->db->where('tuc.item_type',$item_type);
        $query = $this->db->get()->result_array();
             // echo "<pre>";print_r($this->db->last_query());
            // echo "<pre>";print_r($query);die;
        return $query;
        // if(!empty($query)){
        //     $this->db->select("tuc.*")->from('tbl_user_card tuc');
        //     $this->db->where('tuc.is_deleted','0');
        //     $this->db->where('tuc.user_id',$user_id);
        //     $query['cards'] = $this->db->get()->result_array();
        //     return $query;
        // }
    }

    function get_reward_points_new($user_id){
        $this->db->select('SUM(reward_points) as total_point');
        $this->db->from('tbl_wallet');
        $this->db->where('user_id',$user_id);
        $this->db->where('is_deleted','0');
          $query = $this->db->get()->row_array();

          return $query;
    }

    function get_user_data($user_id,$item_type){
            if($item_type == 'speech'){

            $query = "tuc.*, tus.title as name, tus.price AS speech_price, tus.description, CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.video) as image , CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.thumb_image) as thumb_image  ";

        }else if($item_type == 'subscription'){

            $query = "tuc.*, ts.name, ts.price, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','','default.png') as image, ts.description";
        }else if($item_type == 'video'){

            $query = "tuc.*, tv.name, IFNULL(tv.total_point, '0') as total_point, tv.price, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.video) as image,  CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.image) as thumb_image, tv.description";
        }else if($item_type == 'promocode'){

            $query = "tuc.*, (tp.promocode) as name, tp.price, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as image, tp.description";
        }else if($item_type == 'class'){

            $query = "tuc.*, (tmc.program_title) as name,IFNULL(tmc.total_point, '0') as total_point, tmc.price, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tmc.class_image) as image, tmc.description";

        }else if($item_type == 'contest'){

            $query = "tuc.*, tc.name, tc.price,IFNULL(tc.total_point, '0') as total_point, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as image,  CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.thumb_image) as thumb_image, tc.description";
        }

        $this->db->select("tuc.*, (SELECT tax from tbl_setting where is_deleted = '0' and status='Active') as tax, (SELECT credit_point_discount from tbl_setting where is_deleted = '0' and status='Active') as credit_point_discount, ".$query."")->from('tbl_user_cart tuc');

        if($item_type == 'speech'){

            $this->db->join('tbl_user_speech tus',' tuc.item_id = tus.id', 'left');

        }else if($item_type == 'subscription'){

            $this->db->join('tbl_master_subscription ts',' tuc.item_id = ts.id', 'left');

        }else if($item_type == 'video'){

            $this->db->join('tbl_master_videolibrary tv',' tuc.item_id = tv.id', 'left');

        }else if($item_type == 'promocode'){

            $this->db->join('tbl_promocode tp',' tuc.item_id = tp.id', 'left');

        }else if($item_type == 'class'){

            $this->db->join('tbl_master_class tmc',' tuc.item_id = tmc.id', 'left');

        }else if($item_type == 'contest'){

            $this->db->join('tbl_master_contest tc',' tuc.item_id = tc.id', 'left');

        }

        $this->db->where('tuc.user_id',$user_id);
           if($this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
            $this->db->where('tuc.student_id',$this->session->userdata('studentid'));
        }
        $this->db->where('tuc.item_type',$item_type);
        $this->db->where("tuc.is_deleted","0");
        $query = $this->db->get()->result_array();
            //  echo "<pre>";print_r($this->db->last_query());
            // echo "<pre>";print_r($query);die;
        return $query;
        // if(!empty($query)){
        //     $this->db->select("tuc.*")->from('tbl_user_card tuc');
        //     $this->db->where('tuc.is_deleted','0');
        //     $this->db->where('tuc.user_id',$user_id);
        //     $query['cards'] = $this->db->get()->result_array();
        //     return $query;
        // }
    }

    function get_user_activeSubscriptionList($user_id){
    
        $this->db->select('tor.*,tod.id , tod.item_type , tod.item_id ,tod.insert_datetime , DATEDIFF(now(),tod.insert_datetime) AS datediffer , ts.name, ts.price, ts.validity as validity_number, ts.duration, ts.description, ts.evaluation_type, ts.total_video')->from('tbl_order tor');
        $this->db->join('tbl_order_detail tod','tor.id = tod.order_id');
        $this->db->join('tbl_master_subscription ts', 'tod.item_id = ts.id','left');
        $this->db->where('tor.is_deleted','0');
         $this->db->where('tod.is_deleted','0');
        // $this->db->where('tor.subscription_status','Active');
        // $this->db->where('tor.status','Confirm');
        $this->db->where('tor.user_id',$user_id);
        $this->db->where('tod.item_type','subscription');
        $this->db->group_by('tor.id');
        $this->db->order_by('tod.id','DESC');
        $result = $this->db->get()->result_array();
        if(!empty($result)){
            foreach ($result as $key => $value) {
                $this->db->select("COUNT(us.id) AS total_video");
                $this->db->from('tbl_user_speech us');
                $this->db->where(array('user_id'=>$user_id,'subscription_id'=>$value['item_id'],"is_deleted"=>"0"));
                $already = $this->db->get()->row_array();
                $result[$key]['total_subscription_video'] =!empty($already['total_video']) ? $already['total_video'] : 0;
            }
        }
        //  echo "<pre>";print_r($this->db->last_query());
        //  die;
        return $result;
    }

    function get_user_puchasedVideos($user_id){
        $this->db->select("tor.*,tod.item_type,tod.item_id,tv.name, tv.price, tv.video_duration, tv.description, tv.total_point, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.image) as video")->from('tbl_order tor');
        $this->db->join("tbl_order_detail tod",'tod.order_id = tor.id','left');
        $this->db->join('tbl_master_videolibrary tv', 'tod.item_id = tv.id','left');
        $this->db->where('tor.is_deleted','0');
        $this->db->where('tor.status','Confirm');
        $this->db->where('tor.user_id',$user_id);
        $this->db->where('tod.item_type','video');
        $this->db->group_by('tod.id');
        $this->db->order_by('tod.id','DESC');
        return $this->db->get()->result_array();
    }

    function get_user_puchasedVideo_details($video_id){
        $this->db->select("tor.*, tv.name, tv.price, tv.video_duration, tv.description, tv.total_point, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.video) as video,(SELECT credit_point_discount from tbl_setting where is_deleted = '0' and status='Active') as credit_point_discount")->from('tbl_order tor');
         $this->db->join("tbl_order_detail tod",'tod.order_id = tor.id','left');
        $this->db->join('tbl_master_videolibrary tv', 'tod.item_id = tv.id','left');
        $this->db->where('tod.item_id',$video_id);
        $this->db->where('tod.item_type','video');
        return $this->db->get()->row_array();
    }

    function get_purchased_promocodes($user_id){

        $this->db->select("tp.*, datediff(end_date, now()) as validity");
        $this->db->from('tbl_promocode tp');
        $this->db->where('tp.status','Active');
        $this->db->where('tp.is_deleted','0');
        $this->db->where("tp.type","0");
        $this->db->where('tp.end_date >= ',date('Y-m-d'));
        $result =  $this->db->get()->result_array();

        if(!empty($result)){
            foreach ($result as $key => $value) {
                $this->db->select("tor.*");
                $this->db->from('tbl_order tor');
                $this->db->join("tbl_order_detail tod",'tod.order_id = tor.id','left');
                $this->db->where('tor.user_id',$user_id);
                $this->db->where('tod.item_type','promocode');
                $this->db->where('tod.item_id', $value['id']);
                $this->db->where('tod.is_deleted','0');
                $already = $this->db->get()->row_array();
                $result[$key]['purchased_promo']=!empty($already) ? $already : array();
            } 
        }
        $this->db->select("tp.*, datediff(end_date, now()) as validity");
        $this->db->from('tbl_promocode tp');
        $this->db->join("tbl_user_promocode AS up","tp.id = up.promocode_id");
        $this->db->where('tp.status','Active');
        $this->db->where('tp.is_deleted','0');
        $this->db->where("tp.type","1");
        $this->db->where('tp.end_date >= ',date('Y-m-d'));
        $this->db->where("up.user_id",$user_id);
        $result1 =  $this->db->get()->result_array();
         if(!empty($result1)){
            foreach ($result1 as $key1 => $value1) {
                $result1[$key1]['purchased_promo'] = "done";
            }
        }
        // print_r($result1);
        $result2 = array_merge($result,$result1);
        return $result2; 
    }

    function get_promocode_details($promo_id,$user_id){
        $this->db->select("tp.*, datediff(end_date, now()) as validity");
        $this->db->from('tbl_promocode tp');
        $this->db->where('tp.id',$promo_id);
        $result =  $this->db->get()->row_array();

        if(!empty($result)){
            
            $this->db->select("*");
            $this->db->from('tbl_promocode_benefit');
            $this->db->where('promocode_id',$promo_id);
            $this->db->where('is_deleted','0');
            $result['benefits']  = $this->db->get()->result_array();

            $this->db->select("tor.*");
            $this->db->from('tbl_order tor');
            $this->db->join("tbl_order_detail tod",'tod.order_id = tor.id','left');
            $this->db->where('tor.user_id',$user_id);
            $this->db->where('tod.item_type','promocode');
            $this->db->where('tod.item_id', $promo_id);
            $result['purchased_promo'] = $this->db->get()->row_array();
            if(empty($result['purchased_promo'])){
                $this->db->select("*");
                $this->db->from("tbl_user_promocode");
                $this->db->where("promocode_id",$promo_id);
                $this->db->where("user_id",$user_id);
                $this->db->where("is_deleted","0");
                $result['purchased_promo'] = $this->db->get()->row_array();
            }
            return $result; 
        }
    }

    function get_userwishlist($user_id){
        $this->db->select("tw.*, IFNULL(tv.name,'') as name, tv.video_duration, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.image) as video_image, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tmc.class_image) as class_image, tmc.program_title, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, tc.name as contest_name");
        $this->db->from('tbl_user_wishlist tw');
        $this->db->join('tbl_master_videolibrary tv', 'tw.item_id = tv.id','left');
        $this->db->join('tbl_master_contest tc', 'tw.item_id = tc.id','left');
        $this->db->join('tbl_master_class tmc', 'tw.item_id = tmc.id','left');
        $this->db->where('tw.user_id',$user_id);
        $this->db->where('tw.is_deleted','0');
        $this->db->where_in('tw.item_type',array('video','class','contest'));
        return $this->db->get()->result_array();
    }

    function get_evaluation_details($evaluation_id){
        $this->db->select("tue.*, ts.video_duration, ts.is_evaluated");
        $this->db->from('tbl_user_evaluation tue');
        $this->db->join('tbl_user_speech ts', 'tue.speech_id = ts.id','left');
        $this->db->where('tue.id',$evaluation_id);
        $this->db->where('tue.status','Active');
        $this->db->where('tue.is_deleted', '0');
        $result = $this->db->get()->row_array();
        if(!empty($result)){
            $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',ts.video) as video , CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',ts.thumb_image) as thumb_image");
            $this->db->from('tbl_suggested_courses ts');
            $this->db->where('ts.status','Active');
            $this->db->where('ts.is_deleted','0');
            $this->db->order_by('ts.id','desc'); 
            $result['suggested_courses'] = $this->db->get()->result_array();
        }
        return $result;
    }

    function get_enrolled_classes($user_id){
        $current_datetime = date('Y-m-d H:i:s');
        $this->db->select("tor.*,tod.item_type , tod.item_id , tc.program_title, tc.price, tc.total_point, tc.description, tc.start_datetime, tc.end_datetime, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image")->from('tbl_order tor');
        $this->db->join('tbl_order_detail tod', 'tor.id = tod.order_id','left');
        $this->db->join('tbl_master_class tc', 'tod.item_id = tc.id','left');
        $this->db->where('tor.is_deleted','0');
        $this->db->where("tc.is_deleted","0");
        $this->db->where('tor.status','Confirm');
        $this->db->where('tor.user_id',$user_id);
        if(!empty($this->session->userdata('studentid')) && $this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
            $this->db->where('tor.student_id',$this->session->userdata('studentid'));
        }
        $this->db->where('tod.item_type','class');
        $this->db->where("tod.is_deleted","0");
        // $this->db->where('(tc.start_datetime >= "'.$current_datetime.'" AND tc.end_datetime <= "'.$current_datetime.'" )');
        $this->db->group_by('tod.id');
        $this->db->order_by('tod.id','DESC');
        return $this->db->get()->result_array();
    }

    function get_mycontest_list($user_id){
        $this->db->select("tor.*, tod.item_type  , tod.item_id ,tod.id AS order_details_id, tc.id AS contest_id ,tc.name, tc.price, tc.total_point, tc.description, tc.contest_type, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, TIMEDIFF(tc.contest_enddate, now()) as remaining_time,tc.video_duration ,  tc.contest_by")->from('tbl_order tor');
        $this->db->join('tbl_order_detail tod', 'tor.id = tod.order_id','left');
        $this->db->join('tbl_master_contest tc', 'tod.item_id = tc.id','left');
        // $this->db->join('tbl_user_contest tuc', 'tod.item_id = tuc.contest_id','');
        $this->db->where('tod.is_deleted','0');
        $this->db->where('tc.is_deleted','0');
        // $this->db->where('tuc.is_deleted','0');
        $this->db->where('tor.status','Confirm');
        // if(!empty($this->session->userdata('studentid')) && $this->session->userdata('studentid') != 0 && $this->session->userdata('studentid') != ''){
        //     $this->db->where('tor.student_id',$this->session->userdata('studentid'));
        // }
        $this->db->where('tor.user_id',$user_id);
        $this->db->where('tod.item_type','contest');
        $this->db->where("tod.is_deleted",'0');
        // $this->db->group_by('tor.id');
        $this->db->order_by('tor.id','DESC');
        $result =  $this->db->get()->result_array();
        // echo "<pre>"; print_r($this->db->last_query());print_r($result); die;

        if(!empty($result)){
            $sponsors = array();
            foreach ($result as $key => $value) {
                $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");
                $this->db->from('tbl_sponsor ts');
                $this->db->where('ts.status','Active');
                $this->db->where('ts.is_deleted','0');
                $this->db->where('ts.contest_id', $value['item_id']);
                $sponsor = $this->db->get()->row_array();
                $result[$key]['sponsors']=!empty($sponsor) ? $sponsor : array();
            } 
        }
        return $result;
    }


    function get_contest_details($contest_id){
        $this->db->select("tuc.id AS user_contest_id , tc.*, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, TIMEDIFF(tc.contest_enddate, now()) as remaining_time, tc.video_duration, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tuc.video) as video, tor.uniqueid, tor.subtotal, tor.tax, tor.discount, tor.totalamount, tor.insert_datetime as added_datetime, tuc.winner");
        $this->db->from('tbl_master_contest tc');
        $this->db->join('tbl_user_contest tuc', 'tc.id = tuc.contest_id','');
        $this->db->join('tbl_order_detail tod', 'tc.id = tod.item_id AND item_type = "contest"','left');
        $this->db->join('tbl_order tor', 'tod.order_id = tor.id','left');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tc.id',$contest_id);
        $result =  $this->db->get()->row_array();
        // echo "<pre>"; print_r($this->db->last_query());print_r($result); die;

        if(!empty($result)){
            $this->db->select("tp.*, CONCAT('".S3_BUCKET_ROOT.PRIZE_IMAGE."','',tp.prize_image) as prize_image");
            $this->db->from('tbl_prize tp');
            $this->db->where('tp.status','Active');
            $this->db->where('tp.is_deleted','0');
            $this->db->where('tp.contest_id',$contest_id);
            $result['prizes'] =  $this->db->get()->result_array();

            
            $this->db->select("ts.*, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");
            $this->db->from('tbl_sponsor ts');
            $this->db->where('ts.status','Active');
            $this->db->where('ts.is_deleted','0');
            $this->db->where('ts.contest_id', $contest_id);
            $sponsor = $this->db->get()->result_array();
            $result['sponsors']=!empty($sponsor) ? $sponsor : array();
            
            $this->db->select("tus.id AS user_contest_id, tus.contest_id, tu.*, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image, tus.winner");
            $this->db->from('tbl_user tu');
            $this->db->join('tbl_user_contest tus', 'tu.id = tus.user_id','left');
            $this->db->where('tu.status','Active');
            $this->db->where('tu.is_deleted','0');
            $this->db->where('tus.contest_id', $contest_id);
            $winners = $this->db->get()->result_array();
            $result['winners']=!empty($winners) ? $winners : array();
            // echo "<pre>"; print_r($this->db->last_query()); die;
            return $result;
        }
        
    }

    function get_winnerlist($user_id){
        $this->db->select("tuc.*,tc.id AS contest_id,tc.video_duration , CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tuc.video) as video, tc.name,tc.contest_type");
        $this->db->from('tbl_user_contest tuc');
        $this->db->join('tbl_master_contest tc', 'tuc.contest_id = tc.id','left');
        $this->db->where('tc.status','Active');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tuc.user_id',$user_id);
        $this->db->where('tuc.winner !=','0');
        $this->db->order_by('tuc.id','desc');
        return $this->db->get()->result_array();
    }

    function get_reward_points($user_id){
        // $this->db->select("tb.*, CONCAT('".S3_BUCKET_ROOT.BANNER_IMAGE."','',tb.banner_image) as banner_image, (SELECT IFNULL(SUM(reward_points),'0') as reward_points FROM tbl_wallet WHERE status = 'credited' AND user_id = ".$user_id.") as raward_total");
         $this->db->select("tb.*, CONCAT('".S3_BUCKET_ROOT.BANNER_IMAGE."','',tb.banner_image) as banner_image, (SELECT IFNULL(SUM(reward_points),'0') as reward_points FROM tbl_wallet WHERE  user_id = ".$user_id." AND is_deleted = '0') as raward_total");
        $this->db->from('tbl_banner tb');
        $this->db->where('tb.status','Active');
        $this->db->where('tb.is_deleted','0');
        return $this->db->get()->row_array();
    }

    function get_user_gifttoclass_list($user_id){
        $this->db->select("tg.*, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, tc.program_title, tu.username");
        $this->db->from('tbl_gift_enrolledclass tg');
        $this->db->join('tbl_master_class tc', 'tg.class_id = tc.id','left');
        $this->db->join('tbl_user tu', 'tg.user_id = tu.id','left');
        $this->db->where('tg.status','Active');
        $this->db->where('tg.is_deleted','0');
        $this->db->where('tg.user_id',$user_id);
        return $this->db->get()->result_array();
    }

    function get_user_giftfromclass_list($user_id){
        $this->db->select("tg.*, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, tc.program_title, tu.username");
        $this->db->from('tbl_gift_enrolledclass tg');
        $this->db->join('tbl_master_class tc', 'tg.class_id = tc.id','left');
        $this->db->join('tbl_user tu', 'tg.receiver_id = tu.id','left');
        $this->db->where('tg.status','Active');
        $this->db->where('tg.is_deleted','0');
        $this->db->where('tg.receiver_id',$user_id);
        return $this->db->get()->result_array();
    }

    function get_giftto_details_byID($gift_id){

        $this->db->select("tg.*, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, tc.program_title, tc.description, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image");
        $this->db->from('tbl_gift_enrolledclass tg');
        $this->db->join('tbl_master_class tc', 'tg.class_id = tc.id','left');
        $this->db->join('tbl_user tu', 'tg.user_id = tu.id','left');
        $this->db->where('tg.id',$gift_id);
        return $this->db->get()->row_array();
    }

    function get_giftfrom_details_byID($gift_id){

        $this->db->select("tg.*, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, tc.program_title, tc.description,  CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image");
        $this->db->from('tbl_gift_enrolledclass tg');
        $this->db->join('tbl_master_class tc', 'tg.class_id = tc.id','left');
        $this->db->join('tbl_user tu', 'tg.receiver_id = tu.id','left');
        $this->db->where('tg.id',$gift_id);
        return $this->db->get()->row_array();
    }
    function check_promocode($promo_id,$user_id) {
        $this->db->select("o.*");
        $this->db->from("tbl_order AS o");
        $this->db->join("tbl_order_detail AS od","o.id = od.order_id");
        $this->db->where("od.item_id",$promo_id);
        $this->db->where("od.item_type","promocode");
        $this->db->where("o.user_id",$user_id);
        $this->db->where("o.is_deleted","0");
        return $this->db->get()->row_array();
    }
     
}