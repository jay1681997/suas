<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
          <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />

    </head>
    <style type="text/css">
        .crn-numbers .select2-container {
    width: 25% !important;
}
.crn-numbers .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 15%;
}

.crn-numbers .select2-container--default .select2-selection--single {
    border-radius: 30px 0 0 30px;
    border-right: none !important;
    border: 2px solid #e6e8eb;
}

.select2-container--open .select2-dropdown {
    left: 0;
    width: 400px !important;
}

.crn-numbers1 .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers1 .select2-container .select2-selection--single .select2-selection__rendered {
            padding-left: 45px;
        }

.crn-numbers1 .select2-container--default .select2-selection--single {
    border-radius: 30px 30px 30px 30px;
    /*border-right: none !important;*/
    border: 2px solid #e6e8eb;
}
 .profile-nav,
        .profile-info {
            margin-bottom: 50px;
        }
    </style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header1.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">My profile</li>
                        </ol>
                    </nav>
                       <?php 
                        if($result['is_video_verify'] == 'Pending'){ ?>
                              <div class="alert alert-warning" role="alert">
                            Profile is under verification from admin <a onclick="My_message()">click for more info</a>

                        </div>

                       <?php }
                        ?>
                                          <!--end-nav--> 
                    <?php echo form_open('my-profile', array('method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                    <div class="container bootstrap snippets bootdey">
                        <div class="row">
                            <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                            <div class="profile-info col-lg-9">
                                <div class="shadow"> 
                                    <div class="px-4 py-4 bio-graph-info">
                                        <?php if($this->session->flashdata('success_msg')){ ?>
                                            <div class="alert alert-success" >
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('success_msg')?>
                                            </div>                
                                        <?php } ?>
                                        <?php if($this->session->flashdata('error_msg')){ ?>
                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('error_msg')?>
                                            </div>
                                        <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $error_msg; ?>
                                            </div>
                                        <?php } ?>
                                        <h1>Profile</h1>
                                        
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="form-group-icon icon-user text-success"></i>
                                                    <input 
                                                        type="text" 
                                                        class="form-control" 
                                                        id="firstname"
                                                        name="firstname"
                                                        placeholder="First name" 
                                                        aria-required="true"
                                                        parsley-trigger="change" 
                                                        data-parsley-required-message="Please enter first name" 
                                                        required 
                                                         maxlength="22" 
                                                        data-parsley-pattern="^[a-zA-Z ]+$" 
                                                        data-parsley-pattern-message="Please enter valid first name" data-parsley-maxlength="32" 
                                                        value="<?php echo !empty(set_value('firstname')) ? set_value('firstname') : $result['firstname']; ?>"
                                                        onkeyup="validate_firstname();"
                                                    >
                                                    <?php echo form_error('firstname'); ?>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="form-group-icon icon-user text-success"></i>
                                                  
                                                    <input 
                                                        type="text" 
                                                        class="form-control"
                                                        id="lastname" 
                                                        name="lastname" 
                                                        placeholder="Last name" 
                                                        aria-required="true"
                                                        parsley-trigger="change"
                                                        data-parsley-required-message="Please enter last name" 
                                                        required 
                                                        data-parsley-pattern="^[a-zA-Z ]+$" 
                                                        data-parsley-pattern-message="Please enter valid last name" data-parsley-maxlength="32" 
                                                        value="<?php echo !empty(set_value('lastname')) ? set_value('lastname') : $result['lastname']; ?>"
                                                         maxlength="22"
                                                        onkeyup="validate_lastname();"
                                                    >
                                                    <?php echo form_error('lastname'); ?>
                                                </div>
                                            </div> 
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="icon-email form-group-icon text-success"></i>
                                                 
                                                    <input 
                                                        type="email" 
                                                        class="form-control" 
                                                        id="email" 
                                                        name="email" 
                                                        placeholder="Email" 
                                                        aria-required="true" 
                                                        parsley-trigger="change" 
                                                        data-parsley-required-message="Please enter email" 
                                                        required 
                                                        data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  
                                                        data-parsley-pattern-message="Please enter valid email address" 
                                                        data-parsley-maxlength="64"
                                                        value="<?php echo !empty(set_value('email')) ? set_value('email') : $result['email']; ?>"
                                                    >
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="icon-phone form-group-icon text-success" style="left:10px !important"></i>
                                                    <div class="input-group crn-numbers">
                                                        <select class="form-select form-control imageselect2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:140px;">
                                                            <!-- <option value="+91">+91</option> -->
                                                            <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                                            <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                                <option value="<?php echo $value['calling_code']; ?>" data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" <?php echo ($country_codes==$value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'] . '(' . $value['calling_code'] . ')'; ?></option>
                                                            <?php } } ?>
                                                        </select>
                                                        <?php echo form_error('country_code'); ?>
                                                        <label id="countrieseerror"></label>
                                                    <input 
                                                        type="text" 
                                                        class="form-control phone-format" 
                                                        id="phone" 
                                                        name="phone" 
                                                        aria-required="true" 
                                                        parsley-trigger="change" 
                                                        data-parsley-errors-container="#phoneerror"
                                                        data-parsley-required-message="Please enter phone number"  
                                                        required 
                                                        maxlength="12" 
                                                        data-parsley-minlength="8" 
                                                        data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" 
                                                
                                                        value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>"
                                                    >
                                                        <!-- <input 
                                                            type="text" 
                                                            class="form-control" 
                                                            id="phone" 
                                                            name="phone" 
                                                            placeholder="Phone Number" 
                                                            aria-required="true" 
                                                            parsley-trigger="change" 
                                                            data-parsley-type="number" 
                                                            data-parsley-errors-container="#phoneerror"
                                                            data-parsley-required-message="Please enter phone number"  
                                                            required 
                                                            onkeypress="return isNumberKey(event);"  
                                                            maxlength="12" 
                                                            data-parsley-minlength="8" 
                                                            data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" 
                                                            value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>"
                                                        > -->
                                                        <?php echo form_error('phone'); ?>
                                                    </div>
                                                    <div id="phoneerror"></div>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="icon-calendar form-group-icon text-success"></i>
                                                    <input 
                                                        type="text" 
                                                        class="form-control flatpickr-input flatpickr-mobile cald-resp-control" 
                                                        id="dob" 
                                                        name="dob"
                                                        placeholder="DOB(MM/DD/YYYY)" 
                                                        aria-required="true"  
                                                        data-parsley-required-message="Please select date of birth"  
                                                        required 
                                                        value="<?php echo !empty(set_value('dob')) ? set_value('dob') : date('d-m-Y', strtotime($result['dob'])); ?>" 
                                                    <?php echo ($result['is_dob_edit'] == 0)?'disabled':'' ?>>
                                                    <span class="icon-alert alrt-div" data-toggle="modal" data-target="#alert-info" style="right:19px !important"></span>
                                                    <?php echo form_error('dob'); ?>
                                                </div>
                                            </div>
                                            <div class="crn-numbers1">
                                                <div class="form-group">
                                                    <i class="icon-location text-success form-group-icon"></i>
                                                   
                                                    <!-- <select 
                                                        class="form-control" 
                                                        id="country" 
                                                        name="country" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-errors-container="#code_errror"  
                                                        data-parsley-required-message="Please select country" 
                                                        required
                                                    >
                                                        <?php $country = !empty(set_value('country')) ? set_value('country') : $result['country']; ?>
                                                        <option value="">Select Country </option>
                                                        <?php if(!empty($countries)){   
                                                            foreach ($countries as $key => $value){ ?>
                                                                <option value="<?php echo $value['name']; ?>" <?php echo ($country==$value['name']) ? 'selected="selected"' : ''; ?>> <?php echo $value['name']; ?> </option>
                                                        <?php } } ?>

                                                    </select> -->
                                                     <select  
                                                    class="form-control imageselect3" 
                                                    id="country" 
                                                    name="country" 
                                                    data-parsley-trigger="change" 
                                                    data-parsley-errors-container="#code_errror"  
                                                    data-parsley-required-message="Please select country" 
                                                    required
                                                     onchange="region_list(this.value);"
                                                >
                                                  <?php $country = !empty(set_value('country')) ? set_value('country') : $result['country']; ?>
                                                    <option value="">Select Country </option>
                                                    <?php if(!empty($countries)){   
                                                        foreach ($countries as $key => $value){ ?>
                                                            <option data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>"  value="<?php echo $value['name']; ?>" <?php echo ($country==$value['name']) ? 'selected="selected"' : ''; ?>> <?php echo $value['name']; ?> </option>
                                                    <?php } } ?>

                                                </select>
                                                    <?php echo form_error('country'); ?>
                                                    <div id="code_errror" style="margin-top: -5px !important"></div>
                                                </div>
                                            </div>
                                              <div class="crn-numbers1">
                                            <div class="form-group">
                                                  <i class="icon-location text-success form-group-icon"></i>
                                                    <select name="region_id" id="region_id" data-parsley-errors-container="#errormsg_region_list" class="form-control select2" data-parsley-trigger="change" required data-parsley-required-message="Please select region name">
                                                        <option value="">Region List</option>
                                                    </select>
                                                    <?php echo form_error('errormsg_region_list'); ?>
                                                    <label id="errormsg_region_list"></label>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group w-100 d-inline-block">
                                                    <i class="form-group-icon icon-widget text-success"></i>
                                                    <input 
                                                        type="text" 
                                                        class="form-control" 
                                                        id="Promo-Code" 
                                                        name="promocode"
                                                        placeholder="Link a Promo Code" 
                                                        aria-required="true"
                                                        value="<?php echo !empty(set_value('promocode')) ? set_value('promocode') : $result['promocode']; ?>"
                                                    >
                                                </div>
                                            </div> 
                                            <!--btn-->
                                            <div class="d-flex justify-content-center">
                                                <button class="btn btn__primary btn__rounded">Save</button>
                                            </div>
                                            <!--start-password-->
                                            <a href="<?php echo base_url(); ?>my-create-new-password" class="shadow px-3 mt-4 py-3 scored-board d-flex justify-content-between theme-rounded align-items-center">
                                                <div class="d-flex">
                                                    <span class="mr-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                                                            <g id="Group_41143" data-name="Group 41143" transform="translate(-48 -925)">
                                                                <circle id="Ellipse_5035" data-name="Ellipse 5035" cx="23" cy="23" r="23" transform="translate(48 925)" fill="#17966b" opacity="0.05"/>
                                                                <g id="Iconly_Bold_Lock" data-name="Iconly/Bold/Lock" transform="translate(59 936)">
                                                                    <g id="Lock" transform="translate(3.5 2)">
                                                                        <path id="Lock-2" data-name="Lock" d="M12.732,20H4.269A4.227,4.227,0,0,1,0,15.826V10.888A4.165,4.165,0,0,1,2.977,6.929V5.4A5.472,5.472,0,0,1,8.485,0,5.576,5.576,0,0,1,12.4,1.58,5.3,5.3,0,0,1,14.023,5.4V6.929A4.165,4.165,0,0,1,17,10.888v4.937A4.227,4.227,0,0,1,12.732,20ZM8.5,11.384a.875.875,0,0,0-.884.865v2.206a.889.889,0,0,0,1.778,0V12.249A.881.881,0,0,0,8.5,11.384Zm.01-9.645A3.711,3.711,0,0,0,4.756,5.376V6.714h7.489V5.4A3.7,3.7,0,0,0,8.505,1.739Z" fill="#17966b"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    <div class="ml-2">
                                                        <h6 class="mb-1">Change Password</h6>
                                                        <span>Do you want to change your password?</span>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="5.877" height="9.753" viewBox="0 0 5.877 9.753">
                                                        <path id="Path_45623" data-name="Path 45623" d="M3.462,6.925,0,3.462,3.462,0" transform="translate(4.877 8.339) rotate(180)" fill="none" stroke="#1f1f1f" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                    </svg>
                                                </div>
                                            </a>
                                            <!--start-password-->
                                       
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                     <?php echo form_close(); ?>
                </div>
            </section>
            <!--end-body--> 

            <!--start-modal-->
            <div class="modal fade" id="alert-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                            <div class="content">
                                <h4>Why DOB Is Required</h4>
                                <p class="fz-16 text-dark">Classes and contests are categorized into 4 age groups. Primary, Junior, Teens and Adults.</p>
                                 <p class="fz-16 text-dark">If you need to change your birth date on your profile, please contact us through the settings section</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <a href="<?php echo base_url(); ?>my-profile" class="btn btn__primary btn__rounded">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->  

            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            
            function UserTemplate (user) {
    if (!user.id) {
      return user.text;
    }
    var $user = $(
      '<span><img src="'+$(user.element).data('profileimage')+'" width="20px" height="20px" class="img-flag"/> '+user.text+'</span>'
    );
    return $user;
  };
            $(document).ready(function(){
                $('select').niceSelect('destroy');
                $('.select2').select2({"width":"100%"}); 
                  $("#country_code").select2({
                "width": "100%",
            });
            $(".imageselect2").select2({
      "width":"100%",
      templateResult: UserTemplate,
      templateSelection: UserTemplate
    });
                             $("#country").select2({
                "width": "100%",
            }); 
                             $(".imageselect3").select2({
                "width": "100%",
                templateResult: UserTemplate,
                templateSelection: UserTemplate
            });
                // alert("<?php echo date('d/m/Y'); ?>");
                $("#dob").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    // dateFormat: "Y-m-d",
                    dateFormat: "d-m-Y",
                    disableMobile: "true",
                    maxDate: new Date()
                });
            });
            function My_message(){
                // alert("Hello");
                swal({ title: "Profile Under Verification",
 text: "All accounts are verified by our Admin Team and can take up to 24 hours to process.Thanks for your patience as we create a safe evnironment for you to stand up and speak!",
 type: "warning"})
            };
             $(document).ready(function() {
            /***phone number format***/
            $(".phone-format").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("" + curval + "" + "-");
                } else if (curchr == 3 && curval.indexOf("(") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 3 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 7) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });
        });
                         function validate_firstname() {
  var element = document.getElementById('firstname');
  element.value = element.value.replace(/[^a-zA-Z@]+/, '');
};
          function validate_lastname() {
  var element = document.getElementById('lastname');
  element.value = element.value.replace(/[^a-zA-Z@]+/, '');
};

window.onload = function() {
                var e = document.getElementById("country");
var value = e.value;
var text = e.options[e.selectedIndex].text;

              region_list(text);
            };
function region_list(country_id) {

            $.ajax({
                url: "<?php echo base_url() . 'website/region_list/'; ?>" + country_id,
                type: 'GET',
                dataType: 'json',
                error: function(err) {
                    $('#region_id').html('<option value="">Select Region</option>');
                },
                success: function(response) {
                    if (response == "") {
                        $('#region_id').html('<option value="">Select Region List</option>');
                    } else {
                        var statehtml = "";
                        var statehtml = "<option value=''>Select Region List</option>";
                        $.each(response, function(index, value) {
                            var slectedstr = '';
                            if (value.id == "<?php echo (!empty(set_value('region_id')) ? set_value('region_id') : $result['region_id']); ?>") {
                                slectedstr = "selected='selected'";
                            }
                            statehtml += "<option value='" + value.id + "' " + slectedstr + ">" + value.region + "</option>";
                        });
                        $('#region_id').html(statehtml);
                    }
                }
            });
        }

 
        </script>
    </body>
</html>
