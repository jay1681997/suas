<!DOCTYPE html>
<html lang="en">


<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<style>
    .speech_bg_content {
        background: url(website_assets/images/backgrounds/speech-bg.png) no-repeat;
        padding: 110px 0;
        background-position: center;
        background-size: cover;
        
    }
    .sport-speach-heading h1 {
        text-align: center;
        margin-bottom: 30px;
        font-size: 50px;
    }

    .contest-block-date {
        margin-bottom: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .date-bk {
        display: inline-flex;
        align-items: center;
        padding-right: 40px;
        margin-right: 40px;
    }

    .date-bk:after {
        content: '';
        position: relative;
        display: block;
        background: #767575;
        width: 2px;
        height: 40px;
        left: 35px;
    }

    .date-bk h5 {
        font-size: 55px;
        color: #08976d;
        font-weight: 700;
        margin-right: 15px;
        margin-bottom: 0;
    }

    .date-bk p {
        color: #767575;
        display: flex;
        flex-direction: column;
        font-size: 18px;
        margin-bottom: 0;
    }

    p.time-green {
        color: #08976d;
        font-weight: 600;
        font-size: 20px;
    }

    p.time-green span {
        color: #767575;
        font-size: 18px;
        font-weight: 500;
    }

    .date-bk:last-child {
        border-right: none;
        padding-right: 0;
        margin-right: 0;
    }

    .date-bk:last-child::after {
        content: '';
        width: 0;
        height: 0;
    }

    @media screen and (max-width: 767px) {
        .sport-speach-heading h1 {
            font-size: 30px;
        }

        .date-bk {
            padding-right: 10px;
            margin-right: 10px;
        }

        .date-bk h5 {
            font-size: 35px;
            margin-right: 8px;
        }

        .date-bk:after {
            left: 10px;
        }

        .date-bk p {
            line-height: 1.5;
            font-size: 16px;
        }

        p.time-green {
            line-height: 1.5;
            font-size: 16px;
        }
    }

    .round-st-block {
        position: relative;
        display: inline-block;
    }

    .rs-student-section {
        width: 180px;
        height: 180px;
        border-radius: 50%;
        overflow: hidden;
        background-color: gray;
    }

    .rs-student-section img {
        width: 100%;
        height: 100%;
        object-fit: contain;
    }

    .vd-play-model {
        position: absolute;
        background: #ffffff;
        border-radius: 50%;
        width: 45px;
        height: 45px;
        padding: 12px;
        line-height: 23px;
        font-size: 15px;
        right: 0;
        top: 60%;
        transform: translateY(25px);
        box-shadow: 2px 2px 10px #0000001f;
    }

    .std-info-float h6 {
        color: #ffffff;
        font-weight: 700;
        margin-bottom: 0px;
    }

    .std-info-float {
        position: absolute;
        bottom: 20px;
        left: 0;
        right: 0;
        color: #ffffff;
    }

    .rs-student-section:before {
        content: '';
        position: absolute;
        background: #0000003d;
        top: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        border-radius: 50%;
    }

    .green-play i {
        color: #17966b;
    }

    .orange-play {
        color: #f78f08;
    }

    .pink-play {
        color: #fe326f;
    }

    .blue-play {
        color: #3b3c92;
    }

    .res-ad-video {
        height: calc(100vh - 150px);
    }

    .slick-slider .slick-list,
    .slick-slider .slick-track {
        * padding-top: 0px;
    }

    /* @media (max-width: 767px) {
            .rs-student-section {
                width: 160px;
                height: 160px;
            }
        }
           @media screen and (max-width: 767px) {
            .feature-item .feature__content {
                padding: 16px !important;
            }
        }*/
</style>
<style>
    /* new css */

    .work-process .slick-slide {
        margin: 10px;
    }

    .process-item {
        padding: 20px;
    }

    .feature-item .feature__content {
        padding: 15px 30px 15px;
    }

    .work-process .cta {
        padding-top: 100px;
        padding-bottom: 40px;
        margin-top: -70px;
    }

    section {
        padding-top: 60px;
        padding-bottom: 60px;
    }

    .slider .slide-item {
        height: calc(100vh - 265px);
        min-height: 200px;
    }

    /* new css of 21 july */
    .custom_bg_dashboard {
        background-position: bottom;
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat;
        position: relative;
        padding: 0;
        padding: 50px 0;
        z-index: 0;
    }

    .container_white_block {
        background: #ffffff;
        padding: 60px 15px;
        border-radius: 45px 45px 0 0;
        box-shadow: 0 0 20px #0000004d;
    }

    .custom_bg_dashboard:before {
        content: '';
        position: absolute;
        background: #0000006e;
        z-index: -1;
        width: 100%;
        left: 0;
        top: 0;
        height: 100%;
    }

    .dashboard_heading_stuff {
        padding: 30px 0;
    }

    .dashboard_heading_stuff h1 {
        font-size: 35px;
        color: #ffffff;
        margin-bottom: 0;
    }

    .content-features {
        background: #f4f4f4;
    }

    .features-layout1.content-features:after {
        background: #fafafa;
    }

    .slick-initialized .slick-slide {
        border: 1px solid #dddddd;
    }

    .slick-initialized .slick-slide:hover {
        border: 1px solid transparent;
    }

    img.round-profile-img {
        border: 1px solid #cccccc;
    }

    .member .member__info .member__job {
        font-weight: 500;
    }

    .heading__title {
        font-size: 37px;
        margin-bottom: 20px;
        position: relative;
    }

    h3.heading__title:after {
        content: '';
        position: absolute;
        width: 100px;
        left: 0;
        right: 0;
        height: 2px;
        background: #000;
        z-index: 0;
        bottom: -10px;
        margin: 0 auto;
    }

    .hd_title_left {
        position: relative;
    }

    h3.heading__title.hd_title_left:after {
        content: '';
        display: none;
        position: absolute;
        width: 100px;
        left: 0;
        right: 0;
        height: 2px;
        background: rgb(0, 0, 0);
        z-index: 0;
        bottom: -10px;
        margin: 0;
    }

    .dashboard_heading {
        text-align: center;
    }

    .dashboard_heading h1 {
        color: #ffffff;
        margin-bottom: 0;
    }

    .dashboard_heading p {
        margin-bottom: 0;
        color: #ffffff;
        font-size: 15px;
    }

    .podium_img_container {
        width: 100%;
        height: 500px;
        overflow: hidden;
    }

    .podium_img_container img {
        width: 100%;
        height: 100%;
        object-fit: contain;
    }

    .nav-tabs {
        border-bottom: 1px solid transparent;
        justify-content: flex-end;
    }

    .nav-tabs .nav-item {
        margin-bottom: -1px;
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        color: #000000;
        background-color: transparent;
        border-color: #dee2e600 #dee2e600 black;
    }

    .nav-tabs.gradient .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        color: #17966b;
        background-color: transparent;
        border-color: #dee2e600 #dee2e600 #15976b;
        font-weight: 600;
    }

    .nav-tabs .nav-link {
        border: 2px solid transparent;
        border-top-left-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
        color: #000000;
        font-size: 16px;
    }

    .nav-tabs .nav-link:focus,
    .nav-tabs .nav-link:hover {
        border-color: #dee2e600 #dee2e600 black;
    }

    .tab-content {
        width: 100%;
        display: block;
    }

    img.round-profile-img {
        width: 20px !important;
        display: inline-block !important;
    }

    .member,
    .process-item {
        border: 1px solid #f3f3f3;
    }

    .ext_ev_block>.cta {
        padding: 80px 0;
        color: #fff;
    }

    .cr-classes-slider>.owl-stage-outer,
    .category-slider>.owl-stage-outer {
        padding: 50px 0;
    }

    .member:hover,
    .process-item:hover {
        box-shadow: 0px 6px 12px 0px rgb(9 29 62 / 15%) !important;
    }

    @media (max-width: 767px) {
        .podium_img_container {
            display: none;
        }
    }
</style>
<style>
    section {
        padding-top: 40px;
        padding-bottom: 40px;
    }

    .custom_bg_dashboard {
        padding: 20px 0;
    }

    .feature-item .feature__icon {
        margin-bottom: 10px;
    }

    .cr-classes-slider>.owl-stage-outer,
    .category-slider>.owl-stage-outer {
        padding: 20px 0;
    }

    .ext_ev_block>.cta {
        padding: 30px 0;
    }

    .slider .slide-item {
        height: calc(100vh - 255px);
    }
</style>

<body>
    <!--  <div class="wrapper"> -->
    <div class="wrapper" id="wrapper">
        <!-- <div class="preloader">
  <div class="loading"><span></span><span></span><span></span><span></span></div>
</div> -->
        <!-- /.preloader -->

        <style type="text/css">
            .badge {
                position: absolute;

                padding: 0px 0px 50px;
                /*border-radius: 50%;*/
                /*background: white;*/
                /*color: white;*/

            }

            .nice-select {
                width: 200px;
                height: 50px;
                top: 20%;
                line-height: 50px;
                /* padding-left: 10px;
            padding-right: 10px; */
            }

            .navbar .nav__item {
                margin-right: 2px;
            }

            .navbar .nav__item .nav__item-link {
                font-size: 15px;
            }

            .nice-select {
                width: 190px;
            }

            .dropdown-item {
                letter-spacing: .4px;
                color: #0d204d;
            }

            ul.navbar-nav li:last-child {
                margin-left: 1px;
            }

            .dropdown-item {
                font-size: 15px;
                font-weight: bold;
                text-transform: capitalize;
                display: block;
                position: relative;
                color: #213360;
                line-height: 20px;
                letter-spacing: .4px;
                padding-right: 15px;
            }

            .dropdown-menu.notification-ui_dd.show {
                margin-right: 2%;
            }
        </style>

        <style>
            /* new submenu css */
            .rgt-submenu-icon {
                display: inline-block;
                float: right;
                clear: both;
            }

            ul.list-unstyled li.submenu {
                position: relative;
            }

            ul.list-unstyled li.submenu>ul {
                background-color: #fff;
                visibility: hidden;
                display: none;
                position: relative;
                left: unset;
                top: 0;
                transition: .3s;
                opacity: 0;
                padding: 10px 0;
                /* width: 150px; */

            }

            ul.list-unstyled li.submenu>ul li {
                list-style-type: none;
            }

            ul.list-unstyled li.submenu:hover>ul {
                visibility: visible;
                display: block;
                opacity: 1;
            }

            ul.list-unstyled:hover>ul {
                visibility: visible;
                opacity: 1;
            }

            /* ul.list-unstyled .submenu.atLeft > ul{
        left:-54%;
    } */
            ul.list-unstyled li.submenu>ul li {
                padding: .25rem 1.5rem;
            }

            ul.list-unstyled li.submenu>ul li:hover {
                color: #16181b;
                text-decoration: none;
                background-color: #f8f9fa;
            }

            ul.list-unstyled li.submenu>ul li a {
                color: #000;
            }

            .dropdown-menu.menus-dropdown {
                left: -240px;
                border-radius: 10px;
            }

            /* ul.list-unstyled li a{
        display:block;
        text-decoration:none;
        padding:8px;
        color:#000;
        transition:.1s;
        white-space:nowrap;
    }
    
    .dropdown a:hover,.dropdown .submenu:hover > a{
        background-color:mediumSeaGreen;
        color:#fff;
    } */
            @media (max-width: 767px) {
                ul.list-unstyled li.submenu>ul {
                    position: relative;
                    display: none;
                }

                ul.list-unstyled .submenu.atLeft>ul {
                    left: unset;
                }

                ul.list-unstyled li.submenu:hover>ul {
                    display: block;
                }
            }
        </style>

        <style>
            .feature-item:hover {
                background-color: #17966b;
            }

            .feature-item:hover .feature__content h4.feature__title {
                color: #ffffff;
            }

            img.white_img {
                display: none;
            }

            .feature-item:hover img.white_img {
                display: block;
            }

            .feature-item:hover img.green_img {
                display: none;
            }
        </style>
        <!-- <pre>dashboard -->

        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <?php
        $customer_data =  $this->db->select("*")->from('tbl_user')->where("id", $this->session->userdata('user_id'))->get()->row_array();
        $student_data =  $this->db->select("*")->from('tbl_student')->where("user_id", $this->session->userdata('user_id'))->get()->result_array();
        ?>

        <section class="primary-dm-block bg-light custom_bg_dashboard"
            style="background-image: url(./assets/images/banners/image5.png);">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12">
                        <div class="dashboard_heading">
                            <h1>Dashboard</h1>
                            <p>Hello <span>
                                    <?php echo $customer_data['username']; ?>
                                </span></p>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="primary-dm-block bg-light" style="">
            <div class="container">
                <div class="row align-items-center">
                    <!-- <div class="col-12 col-lg-6 col-md-6 col-sm-6">
                        <div class="text-center podium_img_container">
                            <img src="assets/images/podium_v.svg" alt="image">
                        </div>
                    </div> -->
                    <div class="col-12 col-lg-12 col-md-6 col-sm-6">
                        <div class="slide__content">
                            <div class="row">
                                <div class="col-3 col-lg-3 col-md-3 mb-4 text-center ">
                                    <div class="m-0 round-st-block">
                                        <a href="#" data-toggle="modal" data-target="#myModal">
                                            <div class="rs-student-section" style="background-color: #3b3c92;">
                                                <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[3]['thumb_image']; ?>"
                                                    class="" alt="image1">
                                                <div class="std-info-float">
                                                    <h6>
                                                        <?php echo $home_videos[3]['title']; ?>
                                                    </h6>
                                                    <span>Age:
                                                        <?php echo $home_videos[3]['age']; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vd-play-model blue-play">
                                                <i class="fas fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-3 col-lg-3 col-md-3 mb-4 text-center ">
                                    <div class="m-0 round-st-block">
                                        <!-- <a href="#" class="video-btn d-inline-block" data-toggle="modal" data-target="#myModal"><img src="./assets/images/banner-age/Group1.png" class="" alt="image1"></a> -->
                                        <a href="#" data-toggle="modal" data-target="#myModal1">
                                            <div class="rs-student-section" style="background-color: #f78f08;">
                                                <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[2]['thumb_image']; ?>"
                                                    class="" alt="image1">
                                                <div class="std-info-float">
                                                    <h6>
                                                        <?php echo $home_videos[2]['title']; ?>
                                                    </h6>
                                                    <span>Age:
                                                        <?php echo $home_videos[2]['age']; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vd-play-model orange-play">
                                                <i class="fas fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-3 col-lg-3 col-md-3 mb-4 text-center ">
                                    <div class="m-0 round-st-block">
                                        <a href="#" data-toggle="modal" data-target="#myModal2">
                                            <div class="rs-student-section" style="background-color: #17966b;">
                                                <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[1]['thumb_image']; ?>"
                                                    class="" alt="image1">
                                                <div class="std-info-float">
                                                    <h6>
                                                        <?php echo $home_videos[1]['title']; ?>
                                                    </h6>
                                                    <span>Age:
                                                        <?php echo $home_videos[1]['age']; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vd-play-model green-play">
                                                <i class="fas fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-3 col-lg-3 col-md-3 mb-4 text-center ">
                                    <div class="m-0 round-st-block">
                                        <a href="#" data-toggle="modal" data-target="#myModal3">
                                            <div class="rs-student-section" style="background-color: #fe326f;">
                                                <img src="<?php echo S3_BUCKET_ROOT . HOME_VIDEO . $home_videos[0]['thumb_image']; ?>"
                                                    class="" alt="image1">
                                                <div class="std-info-float">
                                                    <h6>
                                                        <?php echo $home_videos[0]['title']; ?>
                                                    </h6>
                                                    <span>Age:
                                                        <?php echo $home_videos[0]['age']; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vd-play-model pink-play">
                                                <i class="fas fa-play"></i>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <h5 class="text-dark">Participate in an online speech contest and win cash prizes!</h5>
                            </div>
                            <div class="d-flex flex-wrap align-items-center justify-content-center">
                                <a href="<?php echo base_url(); ?>upload-speeches"
                                    class="btn btn__primary btn__outlined btn__rounded">
                                    Upload Speech</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="features-layout1 content-features pt-40 pb-40">

            <div class="bg-img">
                <img src="https://app.standupandspeak.com/website_assets/images/backgrounds/1.jpg" alt="background">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                        <div class="heading text-center mb-20">
                            <h3 class="heading__title">Our Services</h3>
                            <!-- <p class="heading__desc">Our administration and support staff all have exceptional people skills and
                                        trained to assist you with all medical enquiries.
                                    </p> -->
                            <p class="heading__desc">Click below to see more details about Services
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>upload-speeches">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <!--  <img
                                            src="<?php echo base_url(); ?>website_assets/images/icons/video-upload.png"> -->
                                        <img src="<?php echo base_url(); ?>assets/images/icons/video-upload.png"
                                            class="green_img">
                                        <img src="<?php echo base_url(); ?>assets/images/icons/video-upload-wh.png"
                                            class="white_img">
                                    </div>
                                    <h4 class="feature__title">Upload Speech</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>upload-speeches" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>my-speeches">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/speech-upload.png" class="green_img">
                                        <img src="assets/images/icons/speech-upload-wh.png" class="white_img">
                                        <!--   <img
                                            src="<?php echo base_url(); ?>website_assets/images/icons/speech-upload.png"> -->
                                    </div>
                                    <h4 class="feature__title">My Speeches</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>my-speeches" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>my-evaluations">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/data-analysis.png" class="green_img">
                                        <img src="assets/images/icons/data-analysis-wh.png" class="white_img">
                                        <!-- <img
                                            src="<?php echo base_url(); ?>website_assets/images/icons/data-analysis.png"> -->
                                    </div>
                                    <h4 class="feature__title">My Evaluations</h4>
                                </div>
                            </a>
                            <!-- /.feature__content -->
                            <a href="<?php echo base_url(); ?>my-evaluations" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>online-class">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/online-classes.png" class="green_img">
                                        <img src="assets/images/icons/online-classes-wh.png" class="white_img">
                                        <!-- <img
                                            src="<?php echo base_url(); ?>website_assets/images/icons/online-classes.png"> -->
                                    </div>
                                    <h4 class="feature__title">Online Classes</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>online-class" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>enrolled-classes">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/enrolled-class.png" class="green_img">
                                        <img src="assets/images/icons/enrolled-class-wh.png" class="white_img">
                                        <!-- <img
                                            src="<?php echo base_url(); ?>website_assets/images/icons/enrolled-class.png"> -->
                                    </div>
                                    <h4 class="feature__title">Enrolled Classes</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>enrolled-classes" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>contest-list">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/contest.png" class="green_img">
                                        <img src="assets/images/icons/contest-wh.png" class="white_img">
                                        <!--  <img src="<?php echo base_url(); ?>website_assets/images/icons/contest.png"> -->
                                    </div>
                                    <h4 class="feature__title">Contests</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>contest-list" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>my-reward-points">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <img src="assets/images/icons/rewards.png" class="green_img">
                                        <img src="assets/images/icons/rewards-wh.png" class="white_img">
                                        <!--   <img src="<?php echo base_url(); ?>website_assets/images/icons/rewards.png"> -->
                                    </div>
                                    <h4 class="feature__title">Earn Rewards</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>my-reward-points" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-6 col-sm-6 col-md-6 col-lg-3">
                        <div class="feature-item">
                            <a href="<?php echo base_url(); ?>purchase-promo-codes">
                                <div class="feature__content">
                                    <div class="feature__icon custom-img-ic">
                                        <!-- <img src="<?php echo base_url(); ?>website_assets/images/icons/promocode.png"> -->
                                        <img src="assets/images/icons/promocode.png" class="green_img">
                                        <img src="assets/images/icons/promocode-wh.png" class="white_img">
                                    </div>
                                    <h4 class="feature__title">Promocodes</h4>
                                </div>
                            </a>
                            <a href="<?php echo base_url(); ?>purchase-promo-codes" class="btn__link">
                                <i class="icon-arrow-right icon-outlined"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-6 offset-lg-3 text-center">
                        <!-- <p class="font-weight-bold mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis .<br/>
                                <a href="#" class="color-secondary">
                                    <span>Contact Us For More Information</span> <i class="icon-arrow-right"></i>
                                </a>
                            </p> -->
                    </div>
                </div>
            </div>
        </section>

        <section class="team-layout2 pb-40 overflow-hidden">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <div class="heading text-left mb-40">
                            <h3 class="heading__title hd_title_left mb-0">Upcoming Events</h3>
                            <!-- <p class="heading__desc">Our administration and support staff all have exceptional people skills and
                                        trained to assist you with all medical enquiries.
                                    </p> -->
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-6 wow fadeInUp">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Online
                                    Classes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Upcoming Contests</a>
                            </li>
                        </ul><!-- Tab panes -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 ">

                        <div class="tab-content">

                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="mt-5 mb-5">
                                    <div class="cr-classes-slider owl-carousel">
                                        <?php if (!empty($classes)) {
                                    foreach ($classes as $key => $value) { ?>
                                        <div class="process-item text-center">
                                            <div class="program-preview mb-10 ">
                                                <img src="<?php echo S3_BUCKET_ROOT . CLASS_IMAGE . $value['class_image']; ?>"
                                                    title="program">
                                            </div>
                                            <a
                                                href="<?php echo base_url(); ?>online-class-detail/<?php echo base64_encode($value['id']); ?>">
                                                <h4 class="process__title less-padding-df pr-0">
                                                    <?php echo $value['program_title']; ?>
                                                </h4>
                                                <div class="d-flex justify-content-around">
                                                    <p class="spots-badge">
                                                        <?php $booked_spots = $this->db->select('*')->from('tbl_order')->where('item_type', 'class')->where('item_id', $value['id'])->get()->num_rows();
                                                                            $remaining_spot = $value['total_spot'] - $booked_spots; ?>
                                                        <?php echo $remaining_spot . '/' . $value['total_spot']; ?>
                                                        spots
                                                    </p>
                                                    <!-- <a href="<?php echo base_url(); ?>online-class-detail/<?php echo base64_encode($value['id']); ?>"> -->
                                                    <p class="spots-price">
                                                        <?php echo $this->session->userdata('currency') . ' ' . round(($this->session->userdata('currency_rate') * $value['price']), 2); ?>
                                                    </p>
                                                    <!-- </a> -->
                                                </div>
                                            </a>
                                            <!-- <p class="process__desc">
                                                <?php $description_length = strlen(@$value['description']);
                                                if ($description_length > 30) {
                                                    echo substr(@$value['description'], 0, 30 - $description_length) . "... <a href='javascript:void(0)'>Read More</a>";
                                                } else {
                                                    echo @$value['description'];
                                                }  ?></p> -->
                                            <a href="<?php echo base_url(); ?>online-class-detail/<?php echo base64_encode($value['id']); ?>"
                                                class="btn btn__secondary btn__link small-custom-font">
                                                <span><i class="far fa-calendar-alt"></i>
                                                    <?php echo $this->common_model->date_convert($value['start_datetime'], 'd M, Y', $this->session->userdata('website_timezone')) . ' - ', $this->common_model->date_convert($value['end_datetime'], 'd M, Y', $this->session->userdata('website_timezone')); ?>
                                                </span>
                                                <i class="icon-arrow-right"></i>
                                            </a>
                                        </div><!-- /.process-item -->
                                        <?php } } ?>
                                    </div>

                                </div>
                            </div>


                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="mt-5 mb-5">
                                    <div class="category-slider owl-carousel">
                                        <?php if (!empty($contests)) {
                                foreach ($contests as $key => $value) { ?>
                                        <div class="member">
                                            <div class="member__img">

                                                <!-- <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" alt="member img"> -->
                                                <?php $image_name = explode(".", $value['contest_image']);
                                                if ($image_name[1] == 'mp4' || $image_name[1] == 'mp3') { ?>
                                                <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>"
                                                    width="300px" height="200px" alt="member img">
                                                <!-- <video width="400px" height="300px" controls > <source src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" > Your browser does not support HTML5 video. </video> -->
                                                <?php } else { ?>
                                                <img width="300px" height="200px"
                                                    src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>"
                                                    alt="member img">
                                                <?php  }
                                                ?>
                                            </div>
                                            <div class="member__info">
                                                <h5 class="member__name"><a href="#">
                                                        <?php echo $value['name']; ?>
                                                    </a></h5>
                                                <?php if ($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])) { ?>
                                                <p class="member__job"><img
                                                        src="<?php echo $value['sponsors']['sponsor_logo']; ?>"
                                                        class="round-profile-img"> Sponsor by
                                                    <?php echo $value['sponsors']['sponsor_name']; ?>
                                                </p>
                                                <?php } else { ?>
                                                <p class="member__job"> Sponsor by </p>
                                                <?php } ?>
                                                <div
                                                    class="mt-20 d-flex flex-wrap justify-content-between align-items-center">
                                                    <a href="<?php echo base_url(); ?>contests-live-details/<?php echo base64_encode($value['id']); ?>"
                                                        class="btn btn__secondary btn__link btn__rounded btn-live">
                                                        <span><i class="far fa-play-circle"></i>
                                                            <?php echo ucfirst($value['contest_type']); ?>
                                                        </span>
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                    <ul class="social-icons list-unstyled mb-0">
                                                        <li><a href="<?php echo base_url(); ?>contests-live-details/<?php echo base64_encode($value['id']); ?>"
                                                                class="content-charge">
                                                                <?php echo $this->session->userdata('currency') . ' ' . round(($this->session->userdata('currency_rate') * $value['price']), 2); ?>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } } ?>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="ext_ev_block p-0">
            <div class="cta bg-primary">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-sm-12 col-md-2 col-lg-2">
                            <img src="assets/images/icons/data.png" alt="alert">
                        </div>
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <h4 class="cta__title text-white">Lorem ipsum dolor sit amet</h4>
                            <p class="cta__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod
                                tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-3 col-lg-3">
                            <a href="<?php echo base_url(); ?>my-evaluations"
                                class="btn btn__secondary btn__secondary-style2 btn__rounded mr-30">
                                <span>Evaluate my speech</span>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

        <!--start modal-->
        <div class="modal fade modal-video" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video1" class="w-100 res-ad-video">
                            <source src="https://susalive.s3.amazonaws.com/home_video/987141675760116.MP4"
                                type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal2-->
        <div class="modal fade modal-video" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video2" class="w-100 res-ad-video">
                            <source src="https://susalive.s3.amazonaws.com/home_video/491231675490558.MP4"
                                type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal3-->
        <div class="modal fade modal-video" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video3" class="w-100 res-ad-video">
                            <source src="https://susalive.s3.amazonaws.com/home_video/703861675490516.MP4"
                                type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->

        <!--start modal4-->
        <div class="modal fade modal-video" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border-bottom-0 modal-header pb-1 pt-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body p-0">
                        <video controls id="video4" class="w-100 res-ad-video">
                            <source src="https://susalive.s3.amazonaws.com/home_video/359261675490433.MP4"
                                type="video/mp4">
                        </video>
                    </div>
                    <div class="border-0 modal-footer pb-1 pt-0">
                        <button data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end modal-->
        <!--start-modal-->
        <div class="modal fade" id="subscription" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="https://app.standupandspeak.com/website_assets/images/myprofile/clendar.png"
                            alt="modal-pop" class="mb-25">
                        <div class="content">
                            <h4>Subscription</h4>
                            <p class="fz-16">We have different subscription packages available for video evaluations, do
                                you want to get a subscription package now?</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0">
                        <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray"
                            data-dismiss="modal">No</button>
                        <a href="https://app.standupandspeak.com/subscription"
                            class="btn btn__primary btn__rounded">Yes</a>
                    </div>
                </div>
            </div>
        </div>
       <!--  <section class="slider">
            <div class="slick-carousel m-slides-0">
                <div class="slide-item align-v-h">
                    <div class="bg-img"><img
                            src="<?php echo base_url(); ?>website_assets/images/backgrounds/speech-bg.png"
                            alt="slide img"></div> -->
                    <section class="speech_bg_content">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-8">
                                    <div class="slide__content">
                                        <div class="sport-speach-heading">

                                            <h1 class="text-dark ">
                                                <?php echo (empty($contest_list)) ? 'No Speech Contests Yet' : $contest_list[0]['name']; ?>
                                            </h1>
                                        </div>
                                        <div class="contest-block-date">
                                            <div class="date-bk">
                                                <!-- <h5><?php echo $this->common_model->date_convert($contest_list[0]['insert_datetime'], 'd M, Y', $this->session->userdata('website_timezone')); ?></h5> -->
                                                <h5>
                                                    <?php echo (empty($contest_list)) ? '--' : $this->common_model->date_convert($contest_list[0]['contest_startdate'], 'd', $this->session->userdata('website_timezone')); ?>
                                                </h5>
                                                <p>
                                                    <?php echo $this->common_model->date_convert($contest_list[0]['contest_startdate'], 'M,', $this->session->userdata('website_timezone')); ?>
                                                    <span>
                                                        <?php echo $this->common_model->date_convert($contest_list[0]['contest_startdate'], ' Y', $this->session->userdata('website_timezone')); ?>
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="date-bk">
                                                <p class="time-green">
                                                    <?php echo (empty($contest_list)) ? '--:--' : $this->common_model->date_convert($contest_list[0]['contest_startdate'], 'h:m', $this->session->userdata('website_timezone')); ?>
                                                    <span>
                                                        <?php echo $this->common_model->date_convert($contest_list[0]['contest_startdate'], 'a', $this->session->userdata('website_timezone')); ?>
                                                    </span>
                                                </p>
                                            </div>

                                            <div class="date-bk">
                                                <h5>
                                                    <?php echo (empty($contest_list)) ? '--' : $contest_list[0]['total_participant']; ?>
                                                </h5>
                                                <p>Total<span>Participants</span></p>
                                            </div>

                                        </div>

                                        <div class="d-flex flex-wrap align-items-center">
                            
                                        </div>
                                    </div>
                                    <!-- /.slide-content -->
                                </div>
                                <!-- /.col-xl-7 -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </section>
                    <!-- /.container -->
                <!-- </div> -->
                <!-- /.slide-item -->
            <!-- </div>
        </section> -->
        <!--end-modal-->
        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
        <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>
    </div><!-- /.wrapper -->

    <?php include(APPPATH . "views/website/inc/script.php"); ?>
    <script type="text/javascript">
        var countDownDate = new Date("<?php echo $date_time; ?>").getTime();
        var myfunc = setInterval(function () {
            var now = new Date().getTime();
            var timeleft = now - countDownDate;
            var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
            var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
        }, 1000)
    </script>
    <script>
        $(document).ready(function () {
            // $('#subscription').modal('show');
        })


        $('#myModal').on('shown.bs.modal', function () {
            $('#video1')[0].play();
        })
        $('#myModal').on('hidden.bs.modal', function () {
            $('#video1')[0].pause();
        })

        $('#myModal1').on('shown.bs.modal', function () {
            $('#video2')[0].play();
        })
        $('#myModal1').on('hidden.bs.modal', function () {
            $('#video2')[0].pause();
        })

        $('#myModal2').on('shown.bs.modal', function () {
            $('#video3')[0].play();
        })
        $('#myModal2').on('hidden.bs.modal', function () {
            $('#video3')[0].pause();
        })

        $('#myModal3').on('shown.bs.modal', function () {
            $('#video4')[0].play();
        })
        $('#myModal3').on('hidden.bs.modal', function () {
            $('#video4')[0].pause();
        })
    </script>
    <script>

    </script>
</body>

</html>