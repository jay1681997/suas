<div class="col-lg-3 order-1 order-lg-0 profile-nav">
    <div class="shadow">
        <div class="user-heading round">
            <a href="#">
                <div class="avatar-upload">
                    
                    <?php if($this->uri->segment(1) == 'my-profile') { ?>
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="profile_image" accept=".png, .jpg, .jpeg" />
                        <label for="imageUpload">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-camera" viewBox="0 0 16 16">
                                <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z"/>
                                <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z"/>
                            </svg>
                        </label>
                    </div>
                    <?php } ?>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(<?php echo S3_BUCKET_ROOT.USER_IMAGE.$result['profile_image']; ?>);">
                        </div>
                    </div>
                </div>
            </a>
            <h1 class="text-white"><?php echo $result['username']; ?></h1>
            <p><?php echo $result['email']; ?></p>
        </div>
        <ul class="nav nav-pills nav-stacked d-block">

            <li class="<?php echo $this->uri->segment(1) == 'my-profile' ? "active" : "" ; ?>"> <a href="<?php echo base_url(); ?>my-profile" class="d-block py-2 px-2">  Profile</a> </li>
            <li class="<?php echo $this->uri->segment(1) == 'subscription' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>subscription" class="d-block py-2 px-2">Subscription</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'sub-profile-list' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>sub-profile-list" class="d-block py-2 px-2">Sub Profiles</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-leaderboard' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-leaderboard" class="d-block py-2 px-2">Leaderboard </a> </li> 

            <li class="<?php echo $this->uri->segment(1) == 'cart' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>cart" class="d-block py-2 px-2">Cart</a> </li> 

            <li class="<?php echo $this->uri->segment(1) == 'my-payments' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-payments" class="d-block py-2 px-2">Manage Payment</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-speeches' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-speeches" class="d-block py-2 px-2">My Speeches</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-evaluations' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-evaluations" class="d-block py-2 px-2">My Evaluations </a></li>
            <li class="<?php echo $this->uri->segment(1) == 'enrolled-classes' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>enrolled-classes" class="d-block py-2 px-2">My Enrolled Classes</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-contest-online' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-contest-online" class="d-block py-2 px-2">My Contests</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-winnings' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-winnings" class="d-block py-2 px-2">Contest Results</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'video-library' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>video-library" class="d-block py-2 px-2">Video Library</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-owned-video' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-owned-video" class="d-block py-2 px-2">Owned Videos</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'my-reward-points' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>my-reward-points" class="d-block py-2 px-2">My Reward Points</a></li>
         <!--    <li class="<?php echo $this->uri->segment(1) == 'gifted-enrolled-class' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>gifted-enrolled-class" class="d-block py-2 px-2">Gifted Enrolled Classes</a></li> -->
            <!-- <li class="<?php echo $this->uri->segment(1) == 'wish-list' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>wish-list" class="d-block py-2 px-2">Wishlist</a></li> -->
            <li class="<?php echo $this->uri->segment(1) == 'purchase-promo-codes' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>purchase-promo-codes" class="d-block py-2 px-2">Purchase Promocode</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'notification-setting' ? "active" : "" ; ?>"><a href="<?php echo base_url(); ?>notification-setting" class="d-block py-2 px-2">Notification Setting</a></li>
            <li class="<?php echo $this->uri->segment(1) == 'delete-user' ? "active" : "" ; ?>"><a onclick="return delete_user();" class="d-block py-2 px-2" style="color:#89817f;">Delete User</a></li>

        </ul>
    </div>
</div>