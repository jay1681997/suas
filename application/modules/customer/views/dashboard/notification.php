<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section class="">
                    <div class="container">
                        <div class="contact-panel px-2 px-lg-4 py-4  ">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                                </ol>
                            </nav>
                            <!--end-nav-->
                      
                            <!--start-title-->
                            <div class="title d-flex justify-content-between align-items-center mb-4">
                                <h4 class="mb-0 position-relative">Notifications <span class="nofi-count">(<?php echo count($notifications_list); ?>)</span></h4> 
                                <button data-toggle="modal" data-target="#delete-notifi">Clear All</button>
                            </div>
                            <!--end-title-->
                            <!-- <?php echo "ABC";print_r($notifications_list); ?> -->
                            <?php if(!empty($notifications_list)){ ?>
                            <div class="message-main">

                                <?php foreach ($notifications_list as $key => $value) { ?>
                                <div class="msg-main px-4 py-4 shadow theme-rounded mb-4 position-relative">
                                    <div class="Message-body position-relative">
                                        <p class="fz-16 mb-2"><?php echo $value['message']; ?></p> 
                                        <div class="row justify-content-between align-content-center">
                                            <div class="col-sm-6">
                                                <span class="fz-16 text-dark"><?php echo $value['title']; ?></span>
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="d-flex justify-content-sm-end"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'h:i A, d M Y',$this->session->userdata('website_timezone')); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="Message-close js-messageClose romove_modal" data-toggle="modal" data-target="#delete-notifi" data-id="<?php echo $value['id']; ?>"><i class="fa fa-times"></i></button>
                                </div>
                                 <?php } ?>
                            </div> 
                            <?php }else{ ?>
                                <h5><center>Notification list not found</center></h5>
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <!--end-body--> 

                <!--start-modal-->
                <div class="modal fade" id="delete-notifi"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/notification/delete.png" alt="delete" class="mb-25">
                                <div class="content">
                                    <h4>Clear All Notifications</h4>
                                    <p class="fz-16 text-dark">Are you sure you want to delete all
                                    notifications?</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0">
                                <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal">No</button>
                                <button type="button" class="btn btn__primary btn__rounded bg-danger" id="if-leave" onclick="remove_notification();"><p id="append_id" style="display: none;"></p>Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->
          
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">

            $('.romove_modal').click(function(){
                var notify_id = $(this).data('id');
                $('#append_id').html(notify_id);
            })


            function remove_notification(){
                var notify_id = $('#append_id').html();
                
                $.ajax({
                    url:  '<?php echo base_url(); ?>customer/dashboard/remove_notification/'+notify_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        swal("Success",message,"success");
                        window.location.href = '<?php echo base_url(); ?>notification';
                    }
                }); // END OF AJAX CALL
            }
        </script>
    </body>
</html>