<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
        <style type="text/css">
            .password_icon{
                position: absolute;
                top: 23px;
                right: 32px;
                z-index: 2;
                
            }
        </style>
          <style type="text/css">
        .slick-slider .slick-list, .slick-slider .slick-track {
        padding-top: 0;
    }
    </style>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My profile</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 
                        <?php echo form_open('my-create-new-password', array('method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-4 bio-graph-info text-center">
                                            <div class=" ">
                                                <img src="<?php echo base_url(); ?>website_assets/images/create-new-password.png" alt="create-new-password" class="mb-4">
                                                <div class="title">
                                                    <h4 class="line mb-0 pb-20 position-relative">Change Password</h4>
                                                    <p class="mt-0 mb-4 pt-20 fz-16">Your new password must be different from previous password.</p>
                                                    <p class="text-dark fz-16 mb-25">Note: At least  8 characters, one letter, one capital letter and one number </p>
                                                </div>
                                      
                                                <div class="form-group">
                                                    <i class="form-group-icon text-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                                            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
                                                        </svg>
                                                    </i>
                                                    
                                                    <input 
                                                        type="password" 
                                                        name="old_password" 
                                                        id="old_password" 
                                                        class="form-control" 
                                                        placeholder="Old Password" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-minlength="4" 
                                                        data-parsley-errors-container="#changepassword_oldpassworderror"
                                                        data-parsley-maxlength="64" 
                                                        data-parsley-pattern="^(?=.*[A-Za-z])(?=.*[$@$!%*#?&+\d])[^ ]{8,}$" 
                                                        data-parsley-pattern-message="Please enter valid old password"
                                                        data-parsley-required-message="Please enter old password"  
                                                        required
                                                    >
                                                    <i class="textbox-icon text-default fa fa-eye-slash password_icon" onclick="showpassword(this,'old_password');" style="top: 24px !important;right: 19px !important;position: absolute;"></i>
                                                    <div id="changepassword_oldpassworderror"></div>
                                                </div>
                                                <?php echo form_error('old_password'); ?>
                                             
                                           
                                                <div class="form-group">
                                                    <i class="form-group-icon text-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                                            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
                                                        </svg>
                                                    </i>
                                                    <input 
                                                        type="password" 
                                                        name="password" 
                                                        id="password" 
                                                        class="form-control tooltip.bottom" 
                                                        placeholder="New Password" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-minlength="4" 
                                                        data-parsley-errors-container="#changepassword_passworderror"
                                                        data-parsley-maxlength="64" 
                                                        data-parsley-pattern="^(?=.*[A-Za-z])(?=.*[$@$!%*#?&+\d])[^ ]{8,}$" 
                                                        data-parsley-pattern-message="Please enter valid password" 
                                                        data-toggle="tooltip" 
                                                        data-placement="top" 
                                                        title="Password"
                                                        data-parsley-required-message="Please enter password"
                                                        required
                                                    >
                                                    <i class="textbox-icon text-default fa fa-eye-slash password_icon" onclick="showpassword(this,'password');" style="top: 24px !important;right: 19px !important;position: absolute;"></i>
                                                    <div id="changepassword_passworderror"></div>
                                                </div>
                                                <?php echo form_error('password'); ?>
                                                
                                            
                                                <div class="form-group">
                                                    <i class="form-group-icon text-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                                            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
                                                        </svg>
                                                    </i>
                                                    <input 
                                                        type="password" 
                                                        name="confirmpassword" 
                                                        id="confirmpassword" 
                                                        class="form-control tooltip.bottom" 
                                                        placeholder="Confirm Password" 
                                                        data-parsley-trigger="change"
                                                        data-parsley-errors-container="#changepassword_confirmpassworderror"
                                                        data-parsley-minlength="4"
                                                        data-parsley-maxlength="64"
                                                        data-parsley-equalto="#password"
                                                        data-parsley-pattern="^(?=.*[A-Za-z])(?=.*[$@$!%*#?&+\d])[^ ]{8,}$"
                                                        data-parsley-pattern-message="Please enter valid confirm password"
                                                        data-toggle="tooltip" 
                                                        data-placement="top" 
                                                        title="Password"
                                                        data-parsley-required-message="Please enter confirm password"
                                                        required
                                                        data-parsley-equalto-message="Password mismatch"
                                                    >
                                                    <i class="textbox-icon text-default fa fa-eye-slash password_icon" onclick="showpassword(this,'confirmpassword');" style="top: 24px !important;right: 19px !important;"></i>
                                                     <div id="changepassword_confirmpassworderror"></div>
                                                </div>
                                               
                                         
                                                <button type="submit" class="btn btn__primary btn__outlined btn__rounded  fz-16 mt-3">Change Password</button>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <!--end-body--> 

            <!--start-modal-->
            <div class="modal fade" id="change-password"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/myprofile/modal-pop.png" alt="modal-pop" class="mb-25">
                                <div class="content">
                                    <h4>Change Password</h4>
                                    <p class="fz-16 text-dark">Your password has been
                                    successfully changed</p>
                                </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <a href="login" class="btn btn__primary btn__rounded">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip();
                $('form').parsley();
            });
            function showpassword(elem,textboxid)
            {
                if($('#'+textboxid).attr('type')=='text') {
                    $(elem).removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    $('#'+textboxid).attr('type','password');
                } else {
                    $(elem).removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    $('#'+textboxid).attr('type','text');
                }
            }
        </script>
        <?php if($this->session->flashdata('success_msg')){ ?>
        <script type="text/javascript">
            
            $(document).ready(function() {
                $('#change-password').modal('show');
            });
        </script>
        <?php } ?>
         <?php if($this->session->flashdata('error_msg')){ ?>
             <script type="text/javascript">
                alert("<?php echo $this->session->flashdata('error_msg') ?>");
 
            </script>
        <?php } ?>
        
    </body>
</html>