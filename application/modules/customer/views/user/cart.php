<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section class="">
            <div class="container">
                <div class="contact-panel px-4 py-4">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Cart</li>
                        </ol>
                    </nav>
                    <!--end-nav-->

                    <!--start-title-->
                    <div class="title">
                        <h4 class="mb-0 position-relative">Cart <span>(<?php echo count($result); ?>)</span></h4>
                        <p class="fz-16 mb-4 mt-0">You currently have <?php echo count($result); ?> item in your cart.</p>
                    </div>
                    <label onclick="deleted_all()">Deleted All</label>
                    <?php if ($this->session->flashdata('success_msg')) { ?>
                        <div class="alert alert-success">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg') ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('error_msg')) { ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg') ?>
                        </div>
                    <?php } ?> <?php if (isset($error_msg) && $error_msg != '') { ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>
                    <!--end-title-->
                    <?php
                    $total_sum_amount  = 0;
                    $total_point = 0;
                    $promocode_discount = 0;
                    $item_ids = [];
                    $total_point = $total_points['total_point'];
                    // print_r($total_points);
                    // print_r($result);
                    if (!empty($result)) {
                        foreach ($result as $key => $value) {
                            $total_sum_amount += round($this->session->userdata('currency_rate') * $value['price']);
                            $cart_data = array(
                                "cart_id" => $value['id'],
                                "item_type" => $value['item_type'],
                                "item_id" => $value['item_id'],
                                "price" => round($this->session->userdata('currency_rate') * $value['price']),
                                "total_point" => (!empty($value['total_point'])) ? $value['total_point'] : 0
                            );
                            array_push($item_ids, $cart_data);
                            // print_r($item_ids);
                            // echo "<pre>"; print_r($value);
                    ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <!--start-row-->
                                    <div class="row shadow">
                                        <div class="col-lg-3 py-3">
                                            <?php if ($value['item_type'] == 'speech' || $value['item_type'] == 'video') { ?>

                                                <video src="<?php echo $value['image']; ?>" class="w-100">

                                                <?php } else if ($value['item_type'] == 'subscription' || $value['item_type'] == 'promocode' || $value['item_type'] == 'class' || $value['item_type'] == 'contest') { ?>
                                                    <?php $file_ext = explode('.', $value['image']);
                                                    // print_r($file_ext);
                                                    if ($file_ext[4] == 'mp4' || $file_ext[4] == 'MP4') {  ?>
                                                        <img src="<?php echo $value['thumb_image']; ?>" alt="profile-image123" class="w-100">
                                                        <!--  <video class="w-100" controls>
                                                             <source src="<?php echo $value['image']; ?>" type="video/mp4">
                                                         </video> -->
                                                    <?php } else { ?>
                                                        <img src="<?php echo $value['image']; ?>" alt="profile-image123" class="w-100">

                                                <?php  }
                                                } ?>
                                        </div>
                                        <div class="col-lg-9 pl-lg-0 pl-3 py-3">
                                            <div class="profile-info">
                                                <h5 class="mb-1"><?php echo $value['name']; ?></h5>
                                                <p><?php echo $value['description']; ?></p>
                                            </div>
                                            <!--start-row-->
                                            <div class="row">
                                                <div class="col-sm-8 d-flex align-items-center justify-content-between">
                                                    <span class="fz-16 theme-color font-weight-bold"><?php echo $this->session->userdata('currency') . '' . round($this->session->userdata('currency_rate') * $value['price'], 2); ?></span>

                                                    <a href="<?php echo base_url(); ?>customer/user/add_intowishlist/<?php echo $value['item_id']; ?>/<?php echo $value['item_type']; ?>" class="mb-0 align-items-center d-flex">
                                                        <!--  <span class="mr-2">
                                                        <svg id="Iconly_Bold_Bookmark" data-name="Iconly/Bold/Bookmark" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 16 16">
                                                            <g id="Bookmark" transform="translate(2.514 1.257)">
                                                                <path id="Bookmark-2" data-name="Bookmark" d="M9.366,13a.622.622,0,0,1-.322-.091L4.994,10.8.938,12.909a.648.648,0,0,1-.307.084A.648.648,0,0,1,0,12.33V2.463A2.185,2.185,0,0,1,.9.569,3.733,3.733,0,0,1,3.062,0H6.919A3.749,3.749,0,0,1,9.087.569,2.214,2.214,0,0,1,10,2.463V12.33a.7.7,0,0,1-.075.318.65.65,0,0,1-.387.325A.562.562,0,0,1,9.366,13ZM2.638,3.926a.514.514,0,0,0,0,1.027H7.344a.514.514,0,0,0,0-1.027Z" transform="translate(0.486 -0.257)" fill="#17966b"/>
                                                            </g>
                                                        </svg>
                                                    </span> -->
                                                        <!-- <span class="text-gray fz-16">Add Wishlsit</span> -->
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <a href="javascript:void(0)" onclick="remove_cart('<?php echo $value['id']; ?>')">
                                                        <svg id="Iconly_Bold_Delete" data-name="Iconly/Bold/Delete" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 16 16">
                                                            <g id="Delete" transform="translate(2 1.333)">
                                                                <path id="Delete-2" data-name="Delete" d="M3.421,12.975a1.923,1.923,0,0,1-1.954-1.838C1.258,9.287.91,4.912.9,4.868a.506.506,0,0,1,.127-.363.477.477,0,0,1,.349-.152h9.247a.489.489,0,0,1,.349.152.476.476,0,0,1,.121.363c0,.044-.355,4.426-.558,6.27a1.925,1.925,0,0,1-2,1.838C7.676,12.994,6.833,13,6,13,5.121,13,4.258,12.994,3.421,12.975ZM.476,3.309A.48.48,0,0,1,0,2.832V2.585a.476.476,0,0,1,.476-.477H2.42a.85.85,0,0,0,.827-.659l.1-.443A1.319,1.319,0,0,1,4.624,0H7.376A1.319,1.319,0,0,1,8.645.973l.109.474a.848.848,0,0,0,.827.66h1.944A.476.476,0,0,1,12,2.585v.247a.48.48,0,0,1-.475.477Z" transform="translate(0 -0.333)" fill="#200e32" />
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <!--end-row-->
                                        </div>
                                    </div>
                                    <!--end-row-->
                                </div>
                                <div class="col-lg-6"></div>
                            </div>
                    <?php  }
                    } ?>
                    <br>
                    <!--start-purchase--->
                    <!--   <div class="py-3 px-2 mt-4 danger purchase-sec mb-4 d-flex align-items-center">
                            <span class="mr-3">
                                <svg id="Iconly_Bold_Danger" data-name="Iconly/Bold/Danger" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                    <g id="Danger" transform="translate(2 3)">
                                        <path id="Danger-2" data-name="Danger" d="M17.316,18H2.68a3.128,3.128,0,0,1-.91-.2A2.809,2.809,0,0,1,.219,16.275,2.747,2.747,0,0,1,.21,14.146L7.529,1.433a2.746,2.746,0,0,1,1.1-1.08A2.819,2.819,0,0,1,9.993,0a2.853,2.853,0,0,1,2.484,1.442l7.268,12.615a2.936,2.936,0,0,1,.25,1,2.753,2.753,0,0,1-.73,2.021A2.841,2.841,0,0,1,17.316,18ZM10,12.272a.873.873,0,1,0,0,1.745.877.877,0,0,0,.869-.883A.867.867,0,0,0,10,12.272ZM10,6.09a.872.872,0,0,0-.88.862v2.8a.888.888,0,0,0,.88.873.872.872,0,0,0,.869-.873v-2.8A.867.867,0,0,0,10,6.09Z" fill="#e0a370"/>
                                    </g>
                                </svg> 
                            </span>
                            <p class="mb-0">For evaluation of videos longer than 30 minutes, please reach us on <a href="mailto:app@standupandspeak.com">app@standupandspeak.com</a></p>
                        </div> -->
                    <!--end-purchase-->
                    <?php echo form_open('cart/' . $result['item_type'], array('method' => 'post', 'id' => 'cartdataform')); ?>
                    <div class="credit-card-sec">
                        <?php if (!empty($cards)) { ?>
                            <div class="card-main row">
                                <?php foreach ($cards as $key => $value) { ?>
                                    <div class="col-md-6 mb-3">
                                        <a href="javascript:void(0)" class="card d-flex justify-content-between position-relative pt-4 px-3">
                                            <div class="d-flex justify-content-between checkbox_add">
                                                <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="27.536" viewBox="0 0 33.588 27.536">
                                                        <path id="Path_45624" data-name="Path 45624" d="M58.01,106.055H39.923a4.729,4.729,0,0,0-4.724,4.724v24.139a4.73,4.73,0,0,0,4.724,4.724H58.01a4.73,4.73,0,0,0,4.724-4.724V110.779A4.729,4.729,0,0,0,58.01,106.055Zm-21.433,4.724a3.349,3.349,0,0,1,3.346-3.345h4.2v2.525a.689.689,0,1,0,1.379,0v-2.525h6.923v8.377a1.154,1.154,0,0,1-.049.332c-.006.017-.012.033-.017.051a1.155,1.155,0,0,1-1.088.77H46.658a1.155,1.155,0,0,1-1.088-.77c-.005-.017-.01-.034-.017-.051a1.142,1.142,0,0,1-.049-.332v-2.633a.689.689,0,1,0-1.379,0v2.516H43.81a6.565,6.565,0,0,0-5.988,3.886.291.291,0,0,1-.266.171h-.979Zm7.549,27.485h-4.2a3.349,3.349,0,0,1-3.346-3.346v-8.97h.979a.291.291,0,0,1,.266.171A6.566,6.566,0,0,0,43.81,130h.316Zm17.229-3.346a3.349,3.349,0,0,1-3.346,3.346h-4.2v-2.772a.689.689,0,0,0-1.379,0v2.772H45.5v-8.377a1.154,1.154,0,0,1,.049-.332c.006-.017.012-.034.017-.051a1.155,1.155,0,0,1,1.087-.77h4.617a1.155,1.155,0,0,1,1.088.77c.005.017.01.034.017.051a1.154,1.154,0,0,1,.048.332v2.386a.689.689,0,0,0,1.379,0V130h.316a6.565,6.565,0,0,0,5.988-3.886.291.291,0,0,1,.266-.171h.979Zm0-10.349h-.979a1.671,1.671,0,0,0-1.525.987,5.186,5.186,0,0,1-4.729,3.07H53.47a2.534,2.534,0,0,0-2.2-1.271H46.658a2.533,2.533,0,0,0-2.195,1.271H43.81a5.185,5.185,0,0,1-4.729-3.07,1.671,1.671,0,0,0-1.525-.987h-.979v-3.441h.979a1.671,1.671,0,0,0,1.525-.987,5.186,5.186,0,0,1,4.729-3.07h.653a2.533,2.533,0,0,0,2.2,1.271h4.617a2.533,2.533,0,0,0,2.195-1.271h.653a5.185,5.185,0,0,1,4.729,3.07,1.671,1.671,0,0,0,1.525.987h.979Zm0-4.82h-.979a.291.291,0,0,1-.266-.171,6.565,6.565,0,0,0-5.988-3.886h-.316v-8.259h4.2a3.349,3.349,0,0,1,3.346,3.345Zm0,0" transform="translate(-106.055 62.734) rotate(-90)" fill="#fff"></path>
                                                    </svg>
                                                </span>
                                                <span id="add_checkbox">
                                                    <input type="radio" name="card_id" class="ml-auto option-input radio" <?php if ($key == 0) {
                                                                                                                                echo "checked";
                                                                                                                            } else {
                                                                                                                                echo " ";
                                                                                                                            } ?> value="<?php echo $value['id']; ?>" style="background: white;color: green;">
                                                </span>
                                            </div>
                                            <div class="align-items-center d-flex justify-content-between">
                                                <div class="mb-4">
                                                    <h6 class="font-weight-bold mb-2 text-white"><?php echo $value['card_holder_name']; ?></h6>
                                                    <span class="fz-16 text-white">•••• </span><span class="fz-16 ml-2 text-white"><?php echo $value['card_number_last4']; ?></span>
                                                </div>
                                                <div class="">
                                                    <span>
                                                        <h5 style="color:white"><?php echo strtoupper($value['card_type']); ?></h5>
                                                    </span>
                                                </div>
                                            </div>
                                            <img src="<?php echo base_url(); ?>website_assets/images/card/card.png" alt="card">
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <p style="color:red; text-align: center;">Please add payment method</p>
                        <?php } ?>
                        <!--add-card-->
                        <?php if (!empty($result)) { ?>
                            <a href="<?php echo base_url(); ?>add-debit-card/<?php echo $result[0]['item_type']; ?>" class="my-4 d-flex align-items-center justify-content-center fz-16">
                                <span class="mr-2">
                                    <svg id="Iconly_Bold_Plus" data-name="Iconly/Bold/Plus" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g id="Plus" transform="translate(2 2)">
                                            <path id="Plus-2" data-name="Plus" d="M14.67,20H5.33a5.349,5.349,0,0,1-3.944-1.394A5.356,5.356,0,0,1,0,14.67V5.33A5.358,5.358,0,0,1,1.386,1.386,5.358,5.358,0,0,1,5.33,0h9.33a5.372,5.372,0,0,1,3.945,1.386A5.345,5.345,0,0,1,20,5.33v9.34C20,18.057,18.057,20,14.67,20ZM6.33,9.16a.819.819,0,0,0-.83.83.839.839,0,0,0,.83.84H9.16V13.66a.83.83,0,1,0,1.66,0V10.83h2.84a.835.835,0,0,0,0-1.669H10.82V6.34a.83.83,0,1,0-1.66,0V9.16Z" transform="translate(0 0)" fill="#a4a4a4" />
                                        </g>
                                    </svg>
                                </span>
                                <span class="text-gray">Add Credit/Debit Card</span>
                            </a>
                            <!--end-add-card-->
                    </div>

                    <!--differen-services-->
                 <!-- <div class="card-services row">
                            <div class="col-lg-6">
                                <a href="#" class="shadow px-4 py-4 pay-div bg-white mb-4 d-block">
                                    <div class="d-flex align-items-center">
                                        <span class="mr-3">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23.221" height="28.484" viewBox="0 0 23.221 28.484">
                                                <path id="apple_3_" data-name="apple (3)" d="M12.812,2.479A6.1,6.1,0,0,1,17.286,0a6.082,6.082,0,0,1-1.472,4.553,4.81,4.81,0,0,1-4.242,2A5.29,5.29,0,0,1,12.812,2.479Zm-1,5.694c.963,0,2.75-1.324,5.076-1.324A6.358,6.358,0,0,1,22.464,9.7a6.185,6.185,0,0,0-3.081,5.4,6.343,6.343,0,0,0,3.838,5.8s-2.683,7.55-6.306,7.55c-1.664,0-2.958-1.121-4.712-1.121-1.787,0-3.56,1.163-4.715,1.163C4.18,28.484,0,21.322,0,15.565,0,9.9,3.538,6.929,6.857,6.929c2.157,0,3.832,1.244,4.953,1.244Z" transform="translate(0 0)" fill="#999"/>
                                            </svg>
                                        </span>
                                        <span class="text-dark fz-16">Apple Pay</span>
                                    </div>
                                </a>
                            </div>
                        </div> -->
                    <!--differen-services-->

                    <!--start-->
                    <div class="form-group"> <label>Have Promocode?</label>
                        <?php $promocode_text = (!empty($this->session->userdata('promocode_title'))) ? $this->session->userdata('promocode_title') : $_COOKIE['promocode_text'];
                            print_r($this->session->userdata('promocode_title'));
                            if (!empty($this->session->userdata('promocode_title'))) {
                                $_COOKIE['promocode_text'] = $promocode_text;
                                $this->session->set_userdata('promocode_title', '');
                            }
                        ?>


                        <div class="input-group"> <input type="text" class="form-control coupon" value="<?php echo $promocode_text; ?>" name="promocode_text" id="promocode_text" placeholder="Promocode"> <span class="input-group-append"> <label class="btn btn-primary btn-apply coupon" style="height:100%" id="promocode" name="promocode" onclick="applypromocode()">Apply</label> </span> </div>
                    </div>
                    <a href="<?php echo base_url(); ?>purchase-promo-codes-list" class="my-4 d-flex align-items-center justify-content-center fz-16">
                        <span class="mr-2">
                            <svg id="Iconly_Bold_Plus" data-name="Iconly/Bold/Plus" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <g id="Plus" transform="translate(2 2)">
                                    <path id="Plus-2" data-name="Plus" d="M14.67,20H5.33a5.349,5.349,0,0,1-3.944-1.394A5.356,5.356,0,0,1,0,14.67V5.33A5.358,5.358,0,0,1,1.386,1.386,5.358,5.358,0,0,1,5.33,0h9.33a5.372,5.372,0,0,1,3.945,1.386A5.345,5.345,0,0,1,20,5.33v9.34C20,18.057,18.057,20,14.67,20ZM6.33,9.16a.819.819,0,0,0-.83.83.839.839,0,0,0,.83.84H9.16V13.66a.83.83,0,1,0,1.66,0V10.83h2.84a.835.835,0,0,0,0-1.669H10.82V6.34a.83.83,0,1,0-1.66,0V9.16Z" transform="translate(0 0)" fill="#a4a4a4" />
                                </g>
                            </svg>
                        </span>
                        <span class="text-gray">See All Promocode</span>
                    </a>
                    <!--end-add-card-->
                    <!-- </div> -->
                <?php } ?>
                <div id="price_div" class="mt-10 mb-4">
                    <h5>Order summary</h5>

                    <div class="table-main pb-3">
                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                            <span>Sub total</span>
                            <span><?php echo $this->session->userdata('currency') . '' . $total_sum_amount; ?></span>
                        </div>
                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                            <span>Tax.(<?php echo $result[0]['tax'] . '%';
                                        $result[0]['tax'] = round((($result[0]['tax'] / 100) * $total_sum_amount), 2);
                                        ?>)</span>
                            <span><?php echo $this->session->userdata('currency') . '' . $result[0]['tax']; ?></span>
                        </div>
                        <!--    <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                    <span>Credit points discount</span>
                                    <span>- <?php echo $this->session->userdata('currency') . '' . $result[0]['credit_point_discount']; ?></span>
                                </div> -->
                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                            <span>Promocode Discount</span>
                            <?php $promocode_discount = $_COOKIE['myJavascriptVar']; ?>
                            <span>- <?php echo $this->session->userdata('currency') . '' . $promocode_discount; ?></span>
                        </div>
                    </div>
                    <div class="d-flex fz-16 justify-content-between tabl-wrap pt-3">
                        <span class="font-weight-bold text-dark">Total amount</span>
                        <?php $total_sum = ((($total_sum_amount + $result[0]['tax']) - $promocode_discount) > 0)?(($total_sum_amount + $result[0]['tax']) - $promocode_discount):0.0; ?>
                        <span class="font-weight-bold text-dark"><?php echo $this->session->userdata('currency') . '' . $total_sum; ?></span>
                    </div>
                </div>
                <!--end-->
                <!--start-checkbox-->

                <fieldset class="question pt-4 d-flex justify-content-between align-items-center mb-4 fz-16">
                    <label for="coupon_question" class="mb-0">Use Credit Points <a href="#">(Available <?php echo $total_point; ?>)</a></label>

                    <input class="coupon_question" type="checkbox" name="coupon_question" onchange="valueChanged()" />
                </fieldset>

                <fieldset class="answer">
                    <p style="padding:1px 1px; color: blue;">10 points = 1$ </p>
                    <input type="number" min=0 style="padding:18px 18px;border-radius:50px;box-shadow:0px 0px 10px 1px rgb(0 0 0 / 7%);border:none!important;width:380px;max-width:100%;" max='<?php echo $total_point; ?>' data-parsley-trigger="change" id="redeem_points" value=0 name="redeem_points" placeholder="Enter points to redeem">
                </fieldset>
                <!--end-checkbox-->
                <input type="hidden" name="tax" value="<?php echo $result[0]['tax']; ?>">
                <input type="hidden" name="credit_point_discount" value="<?php echo $result[0]['credit_point_discount']; ?>">
                <input type="hidden" name="subtotal" value="<?php echo $total_sum_amount; ?>">
                <input type="hidden" id="total_amount" name="total_amount" value="<?php echo $total_sum; ?>">
                <input type="hidden" id="promocode_discount" name="promocode_discount" value="<?php echo $promocode_discount; ?>">


                <input type="hidden" name="cart_id" value="<?php echo base64_encode(json_encode($item_ids)); ?>">
                <?php if (!empty($result)) { ?>
                    <div class="d-flex justify-content-center">
                        <button type="button" class="btn btn__primary btn__rounded" id="proceed_btn"><span>Proceed To Checkout</span></button>
                    </div>
                <?php  } ?>
                <?php echo form_close(); ?>
                </div>
            </div>
        </section>
        <!--end-body-->
        <!--start-modal-->
        <div class="modal fade" id="purchased_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button> -->
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -94.png" alt="Group -94" class="mb-25">
                        <div class="content">
                            <h4 id="title"></h4>
                            <p class="fz-16" id="description"> </p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <a class="btn btn__primary btn__rounded" onclick="Send_to_homepage()" id="continue_button" style="color:#FFFF;">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->

        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div>
    <?php include(APPPATH . "views/website/inc/script.php"); ?>
    <script>
        function Send_to_homepage() {
            window.location.href = "<?php echo base_url(); ?>dashboard";
        }

        function deleted_all() {
            // alert("Deleted All");
            swal({
                title: "",
                text: "Are you sure you want to delete all item",
                // type: "success",
                showCancelButton: true,
                confirmButtonColor: "#29cffd",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>customer/user/deleted_all',
                        cache: false,
                        type: "GET",
                        dataType: 'json',
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Error", errorThrown, "error");
                        },
                        success: function(response) {
                            console.log(response);
                            location.reload();
                            if (response.status == 'error_msg') {
                                swal("Error", response.message, "error");

                            } else {
                                swal("Success", response.message, "success");
                            }
                        }
                    })
                }
            });

        }

        function applypromocode() {
            var promocode_text = document.getElementById("promocode_text").value;
            <?php $this->session->set_userdata('promocode_title', ''); ?>
            var total_amount = document.getElementById("total_amount").value;
            var promocode_discount = document.getElementById("promocode_discount").value;
            if (promocode_discount != '' && promocode_discount != 0) {
                total_amount = Number(promocode_discount) + Number(total_amount);
            }
            // console.log(json_encode(promocode_text));
            if (promocode_text != '') {
                // alert(total_amount);
                $.ajax({
                    url: '<?php echo base_url(); ?>customer/user/promocode_details',
                    data: {
                        "promocode_text": promocode_text,
                        "total_amount": total_amount
                    },
                    cache: false,
                    type: "POST",
                    dataType: 'json',
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal("Error", errorThrown, "error");

                        document.cookie = "promocode_text = " + promocode_text;
                        document.getElementById('promocode_discount').value = 0;
                        document.cookie = "myJavascriptVar = " + 0;
                        document.getElementById('total_amount').value = total_amount;
                        // document.getElementById('promocode_text').value = '';
                        $("#price_div").load(window.location.href + " #price_div");
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.status == 'error_msg') {
                            swal("Error", response.message, "error");
                            document.getElementById('promocode_discount').value = 0;
                            document.cookie = "myJavascriptVar = " + 0;
                            document.cookie = "promocode_text = " + promocode_text;
                            document.getElementById('total_amount').value = total_amount;
                            // document.getElementById('promocode_text').value = '';
                            $("#price_div").load(window.location.href + " #price_div");
                        } else {
                            document.cookie = "promocode_text = " + promocode_text;
                            document.cookie = "myJavascriptVar = " + response.total_amount_promocode;
                            console.log(response.total_amount_promocode);
                            console.log("AAAAAA RESPONSE BBB");
                            swal("Success", response.message, "success");
                            document.getElementById('promocode_discount').value = response.total_amount_promocode;
                            document.getElementById('total_amount').value = response.total_amount;

                            $("#price_div").load(window.location.href + " #price_div");
                        }
                    }
                });
            } else {
                document.cookie = "promocode_text = " + promocode_text;
                document.getElementById('promocode_discount').value = 0;
                document.cookie = "myJavascriptVar = " + 0;
                $("#price_div").load(window.location.href + " #price_div");
            }
        }
        $(document).ready(function() {
            $(".checkbox_add").click(function() {

                $("input:radio").attr("checked", false);
            });
            //  <?php if ($this->session->userdata('is_settype') && $this->session->userdata('is_settype') == 'video') { ?>
            //     $('#purchased_video').modal('show');
            // <?php } else if ($this->session->userdata('is_settype') && $this->session->userdata('is_settype') == 'subscription') {  ?>
            //     $('#purchased_subscription').modal('show');
            // <?php } else if ($this->session->userdata('is_settype') && $this->session->userdata('is_settype') == 'speech') { ?>
            //     $('#purchased_speech').modal('show');
            // <?php } ?> 
        });

        function valueChanged() {
            if ($('.coupon_question').is(":checked"))
                $(".answer").show();
            else
                $(".answer").hide();
        };

        function remove_cart(cart_id) {
            swal({
                title: "",
                text: "Are you sure you want to delete this item",
                // type: "success",
                showCancelButton: true,
                confirmButtonColor: "#29cffd",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>customer/user/remove_item_fromcart/' + cart_id,
                        type: "GET",
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Error", errorThrown, "error");
                        },
                        success: function(message) {
                            document.cookie = "myJavascriptVar = " + '';
                            document.cookie = "promocode_text = " + '';
                            location.reload();
                            // window.location.href = '<?php echo base_url(); ?>dashboard';
                        }
                    }); // END OF AJAX CALL
                }
            });
            //     let text = "Are you sure you want to delete this item";
            //     if (confirm(text) == true) {

            // }
        }

        $('#proceed_btn').click(function() {
            var formdata = {};
            $.each($('#cartdataform').serializeArray(), function() {
                console.log(this.name);
                formdata[this.name] = this.value;
            });
            $.ajax({
                url: "<?php echo base_url() . 'cart'; ?>",
                type: 'POST',
                data: formdata,
                dataType: 'json',
                error: function(error) {
                    location.reload();
                    console.log("Error");
                },
                success: function(response) {
                    console.log(response);
                    console.log("SUCCESSS");
                    document.cookie = "promocode_text = " + '';
                    document.getElementById('promocode_discount').value = 0;
                   document.cookie = "myJavascriptVar = " + '';
                    // location.reload();
                    swal("Success", "Order place successfully", "success");
                    console.log("SUCCESSS AFTER");
                    if (response.status == 'success') {
                        $('#purchased_item').modal('show');
                        $('#title').html(response.title);
                        $('#description').html(response.description);

                        if (response.item_type == 'subscription') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>subscription-active');
                            $('#continue_button').html("Continue");
                        } else if (response.item_type == 'video') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>my-owned-video');
                            $('#continue_button').html("Continue");
                        } else if (response.item_type == 'speech') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>my-speeches');
                            $('#continue_button').html("Continue");
                        } else if (response.item_type == 'promocode') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>purchase-promo-codes');
                            $('#continue_button').html("Continue");
                        } else if (response.item_type == 'class') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>enrolled-classes');
                            $('#continue_button').html("Continue");
                        } else if (response.item_type == 'contest') {
                            $('#continue_button').attr('href', '<?php echo base_url(); ?>submission-video-for-contest/<?php echo base64_encode($this->session->userdata('contest_id')); ?>');
                            $('#continue_button').html("Upload Video Now");
                        }


                    } else if (response.status == 'validation_error') {
                        swal("Validation Error", "Something's went wrong, please try again.", "error");
                    } else {
                        swal("Error", "You can't redeem points, please check available credit points", "error");

                        // window.location.href='<?php echo base_url(); ?>cart/'+response.item_type;

                    }
                }
            })
        })
    </script>
</body>

</html>