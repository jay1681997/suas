<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Reward Points</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">My Reward Points</h4> 
                                                <p class="fz-16 mb-4">Turn your points into classes, 
                                                    contests or prizes!</p>
                                            </div>
                                            <!--end-title-->
                                            <div class="">
                                                <img src="<?php echo $rewards['banner_image']; ?>" alt="reward-card">
                                            </div> 

                                            <div class="total-point pt-100" style="background:url(<?php echo base_url(); ?>website_assets/images/reward/banner-prop.png) no-repeat center;"> 
                                                <h4 class="font-weight-bold"><?php echo $rewards['raward_total']; ?></h4>
                                                <span class="text-dark fz-16 font-weight-bold">Total Points</span>
                                                <p class="fz-16"><span class="font-weight-bold">Congratulations</span> and keep on speaking!</p>
                                            </div>
                                            <ul class="text-left w-50 m-auto">
                                                <li>Sign up for classes, contests, videos and receive points!</li>
                                            </ul>
                                            <!--start-->
                                            <div  class="shadow px-3 mt-4 py-3 text-sm-left text-center scored-board row w-100 m-auto mt-40 justify-content-between theme-rounded align-items-center">
                                                <div class="d-flex col-sm-2 mb-sm-0 mb-2 justify-content-lg-start justify-content-center">
                                                    <span class="mr-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46">
                                                            <path id="Path_212432" data-name="Path 212432" d="M23,0A23,23,0,1,1,0,23,23,23,0,0,1,23,0Z" fill="#17966b" opacity="0.05"/>
                                                            <g id="_2933094" data-name="2933094" transform="translate(11 11)">
                                                                <path id="Path_209319" data-name="Path 209319" d="M271,275.4c.808-.2,1.406-.719,1.406-1.317s-.6-1.118-1.406-1.317Z" transform="translate(-258.297 -259.976)" fill="#17966b"/>
                                                                <path id="Path_209320" data-name="Path 209320" d="M98.734,91a7.734,7.734,0,1,0,7.734,7.734A7.743,7.743,0,0,0,98.734,91Zm3.516,9.837a3.059,3.059,0,0,1-2.812,2.756v.767a.7.7,0,1,1-1.406,0v-.767a3.059,3.059,0,0,1-2.812-2.756.7.7,0,1,1,1.406,0c0,.6.6,1.118,1.406,1.317v-2.78a2.756,2.756,0,1,1,0-5.511V93.1a.7.7,0,1,1,1.406,0v.76a3.059,3.059,0,0,1,2.812,2.756.7.7,0,0,1-1.406,0c0-.6-.6-1.118-1.406-1.317v2.78A3.059,3.059,0,0,1,102.25,100.837Z" transform="translate(-86.734 -86.734)" fill="#17966b"/>
                                                                <path id="Path_209321" data-name="Path 209321" d="M212.406,182.761c-.808.2-1.406.719-1.406,1.317s.6,1.118,1.406,1.317Z" transform="translate(-201.109 -174.194)" fill="#17966b"/>
                                                                <path id="Path_209322" data-name="Path 209322" d="M12,0A12,12,0,1,0,24,12,12.047,12.047,0,0,0,12,0Zm0,21.141A9.141,9.141,0,1,1,21.141,12,9.151,9.151,0,0,1,12,21.141Z" fill="#17966b"/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="col-sm-10 d-sm-flex justify-content-between">
                                                    <div class="text-sm-left text-center mb-sm-0 mb-3">
                                                        <h6 class="mb-1">Redeem Points</h6>
                                                        <span>Redeem points into prizes</span>
                                                    </div>
                                                    <div class="">
                                                         <a href="<?php echo base_url(); ?>prize-list" class="btn btn__primary btn__rounded">Redeem</a>
                                                    </div>
                                                </div> 
                                            </div>
                                            <!--end-->
                                            <!--button-->
                                            <div class="d-flex justify-content-center mt-50">
                                                <a href="<?php echo base_url(); ?>points-history" class="btn btn__primary btn__rounded">View History</a>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>