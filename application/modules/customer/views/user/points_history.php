<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
         <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Reward Points</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 pt-3 pb-5 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">Points History</h4> 
                                                <p class="fz-16 mb-4">See your points history below</p>
                                            </div>
                                            <!--end-title--> 
                                            <?php if(!empty($history)) { foreach ($history as $key => $value) { ?>
                                            <div  class="shadow px-0 mt-4 py-3 text-sm-left text-center scored-board row w-100 m-auto mt-20 justify-content-between theme-rounded align-items-center">
                                                <div class="col-sm-8 mb-sm-0 mb-3 justify-content-lg-start justify-content-center"> 
                                                    <span class="fz-16 d-block font-weight-bold"><?php echo ucfirst($value['item_type']); ?></span>
                                                    <p class="d-block mb-0"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M, Y - H:i A',$this->session->userdata('website_timezone'));?></p>
                                                </div>
                                                <div class="col-sm-4 text-sm-right "> 
                                                    <span class="d-block fz-16 text-dark font-weight-bold"><?php echo $value['reward_points'].'.00'; ?></span>
                                                    <?php if($value['status'] == 'debited'){ ?>
                                                    <span class="text-danger d-block"><?php echo ucfirst($value['status']); ?></span>
                                                    <?php } else { ?> 
                                                    <span class="text-success d-block"><?php echo ucfirst($value['status']); ?></span>
                                                    <?php } ?>
                                                </div> 
                                            </div>
                                            <?php }} else { ?> 
                                                <h5><center>Points history not found</center></h5>
                                            <?php } ?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
    <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>