<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Video Library</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info">
                                            <!--start-video-->
                                            <div class="speech-box inner-side">
                                                <div class="speech-img position-relative">
                                                    <div class="video-intro">
                                                        <div class="image">
                                                            <div class="play-button"></div>
                                                            <a href="javascript:" id="btn_play" class="">
                                                            <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play video"></a>
                                                        </div>
                                                          <?php  if(!empty($video_order_details)){ ?>
                                                        <video id="myVideo" controls="" class="w-100">
                                                            <source src="<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$video_details['video']; ?>" class="w-100">
                                                        </video>
                                                    <?php }else{ ?>
                                                    <img src="<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$video_details['image']; ?>" alt="play video"></a>
                                                   <?php } ?>
                                                    </div>
                                                    <div class="overlay position-absolute">
                                                        <div class="btt-plays">
                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="Mask Group -3" class="mr-2">
                                                                <span class="text-white"><?php echo $video_details['video_duration']; ?> min</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end-video-->
                                                <!--start-content-->
                                                <div class="content py-3">
                                                    <h4 class="mb-2"><?php echo $video_details['name']; ?></h4>
                                                    <div class="row align-items-center">
                                                        <div class="col-lg-8">
                                                            <div class="">
                                                                <span class="mr-2">
                                                                    <svg id="Iconly_Bold_Calendar" data-name="Iconly/Bold/Calendar" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                    <g id="Calendar" transform="translate(3.049 2.032)">
                                                                    <path id="Calendar-2" data-name="Calendar" d="M5.235,20.324H5.224a5.236,5.236,0,0,1-3.8-1.4,5.223,5.223,0,0,1-1.414-3.8L0,6.659A5.172,5.172,0,0,1,1.238,3.1,5.019,5.019,0,0,1,4.579,1.555V.793A.785.785,0,0,1,4.8.231.779.779,0,0,1,6.123.791V1.5l5.96-.008V.783a.782.782,0,0,1,.221-.561.779.779,0,0,1,1.323.559v.762A5.046,5.046,0,0,1,17,3.054,5.074,5.074,0,0,1,18.281,6.6l.011,8.563c0,3.176-1.993,5.152-5.211,5.157Zm8.019-5.9a.846.846,0,0,0-.822.865.843.843,0,1,0,.832-.865ZM5,14.422H5a.865.865,0,0,0-.821.874A.836.836,0,0,0,5,16.11h.039a.822.822,0,0,0,.574-.269.835.835,0,0,0,.218-.6A.826.826,0,0,0,5,14.423Zm4.134-.005h0a.869.869,0,0,0-.821.875.837.837,0,0,0,.827.812h.037a.837.837,0,0,0,.792-.875.826.826,0,0,0-.832-.811ZM4.992,10.766h0a.864.864,0,0,0-.821.874A.835.835,0,0,0,5,12.453h.039a.819.819,0,0,0,.572-.269.836.836,0,0,0,.218-.6.825.825,0,0,0-.831-.811Zm4.135-.036h0a.865.865,0,0,0-.822.874.837.837,0,0,0,.828.813h.037a.839.839,0,0,0,.791-.875.827.827,0,0,0-.831-.811Zm4.136.006h0a.84.84,0,0,0-.821.852V11.6a.827.827,0,0,0,.834.811h.019a.838.838,0,0,0-.029-1.676ZM4.581,3.108c-2.018.2-3.039,1.4-3.036,3.55l0,.316,15.191-.02V6.6c-.043-2.147-1.088-3.325-3.106-3.5v.782a.776.776,0,0,1-1.325.556.786.786,0,0,1-.22-.554V3.058l-5.96.008,0,.821A.777.777,0,0,1,4.8,4.444a.786.786,0,0,1-.22-.554V3.108Z" transform="translate(0)" fill="#17966b"></path>
                                                                    </g>
                                                                    </svg>
                                                                </span>
                                                                <span class="text-gray"><?php echo $this->common_model->date_convert($video_details['insert_datetime'], 'd F, Y',$this->session->userdata('website_timezone')); ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 text-right">
                                                            <strong class="fz-16 theme-color"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$video_details['price'],2); ?></strong>
                                                            <span class="d-block text-gray">+ Applicable Taxes</span>
                                                        </div>
                                                    </div>

                                                    <p class="my-3"><?php echo $video_details['description']; ?></p>

                                                    <!--start-label-->
                                                    <div class="py-3 px-2 purchase-sec mb-4 d-flex align-items-center rounded">
                                                        <span class="mr-2">
                                                            <svg id="_2933094" data-name="2933094" xmlns="http://www.w3.org/2000/svg" width="19.999" height="19.999" viewBox="0 0 19.999 19.999">
                                                                <path id="Path_209319" data-name="Path 209319" d="M271,274.957c.674-.165,1.172-.6,1.172-1.1s-.5-.932-1.172-1.1Z" transform="translate(-260.415 -262.108)" fill="#17966b"></path>
                                                                <path id="Path_209320" data-name="Path 209320" d="M97.445,91a6.445,6.445,0,1,0,6.445,6.445A6.452,6.452,0,0,0,97.445,91Zm2.93,8.2a2.549,2.549,0,0,1-2.344,2.3v.639a.586.586,0,1,1-1.172,0v-.639a2.549,2.549,0,0,1-2.344-2.3.586.586,0,1,1,1.172,0c0,.5.5.932,1.172,1.1V97.978a2.3,2.3,0,1,1,0-4.592v-.633a.586.586,0,0,1,1.172,0v.633a2.549,2.549,0,0,1,2.344,2.3.586.586,0,0,1-1.172,0c0-.5-.5-.932-1.172-1.1V96.9A2.549,2.549,0,0,1,100.375,99.2Z" transform="translate(-87.446 -87.446)" fill="#17966b"></path>
                                                                <path id="Path_209321" data-name="Path 209321" d="M212.172,182.761c-.674.165-1.172.6-1.172,1.1s.5.932,1.172,1.1Z" transform="translate(-202.758 -175.622)" fill="#17966b"></path>
                                                                <path id="Path_209322" data-name="Path 209322" d="M10,0A10,10,0,1,0,20,10,10.039,10.039,0,0,0,10,0Zm0,17.616A7.617,7.617,0,1,1,17.616,10,7.626,7.626,0,0,1,10,17.616Z" fill="#17966b"></path>
                                                            </svg>
                                                        </span>
                                                        <span>Participate in contest and get <?php echo $video_details['total_point']; ?> points.</span>
                                                    </div>
                                                    <!--end-label-->
                                                    <!--button-->
                                                    <?php if(empty($video_order_details)){ ?>
                                                         <div class="d-flex justify-content-center">
                                                        <?php $this->session->set_userdata('video_id', $video_details['id']); ?>
                                                        <a href="<?php echo base_url(); ?>customer/user/add_item_intocart/video" class="btn btn__primary btn__rounded">Buy Now</a>
                                                    </div>
                                                  <?php  } ?>
                                                   
                                                </div>
                                                <!--end-content-->
                                            </div>  
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php include(APPPATH."views/website/inc/script.php");?>
    <script type="text/javascript">
                        var vid = document.getElementById("myVideo");
vid.onplay = function() {
  // alert("The video has started to play");
  $("#btn_play").hide();
};    
vid.onpause = function(){
       $("#btn_play").show();
      // alert("The video has started to play1");
}
    </script>
    </body>
</html>