<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
    <div class="wrapper">
        <?php  include(APPPATH."views/website/inc/header.php");?>
        <!--start-body-->
        <section>
            <div class="container">
                <div class="contact-panel px-2 px-lg-4 py-4">
                    <!--start-nav-->
                    <!-- <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Purchase Promocode</li>
                        </ol>
                    </nav> -->
                    <!--end-nav--> 

                    <div class="container bootstrap snippets bootdey">
                        <div class="row">
                            <div class="profile-info col-lg-12">
                                <div class="shadow"> 
                                    <div class="px-lg-4 px-3 pt-3 pb-5 bio-graph-info text-center">
                                        <!--start-title-->
                                        <div class="title">
                                            <h4 class="line mb-3 pb-20 position-relative">Promo Code Details</h4> 
                                            <p class="fz-16 mb-4">Issued by Stand Up and Speak.</p>
                                        </div>
                                        <!--end-title-->  
                                        <!--start-->
                                        <div class="promo-card mb-30 green">
                                            <div class="main row  w-100 m-auto theme-rounded">
                                                <div class="col-sm-4 co-img d-flex justify-content-center align-items-center">
                                                    <div class="bg-shadow">
                                                        <span class="fz-25 font-weight-bold text-white"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$promocode_data['price'],2); ?></span>
                                                    </div>
                                                </div>
                                          
                                                <div class="col-sm-8 content px-5 py-4">
                                                    <div class="">
                                                        <p class="text-white text-sm-left fz-16"><?php echo $promocode_data['description']; ?></p>
                                                        <span class="d-block text-sm-right text-white">Valid: <?php echo $promocode_data['validity']; ?> Days</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end-->  

                                        <!--start-->
                                        <div class="shadow px-4 py-4 theme-rounded text-left">
                                            <div class="title mb-3">
                                                <h4 class="mb-1">Benefits</h4>
                                            </div>
                                            <ul>
                                                <?php foreach ($promocode_data['benefits'] as $key => $value) { ?>
                                                    <li class="mb-2"><?php echo $value['benefits']; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <!--end-->

                                        <!--start-->
                                        <div class="d-flex justify-content-center mt-50">
                                            <?php $this->session->set_userdata('promocode_id', $promocode_data['id']); ?>
                                            <input type="hidden" name="promocode_title" id="promocode_title" value="<?php echo $promocode_data['promocode']; ?>">
                                            <a href="<?php echo base_url(); ?>customer/user/apply_promocode" class="btn btn-info btn__rounded">Apply</a>
                                             <!-- <a onclick="set_session_data()" class="btn btn-info btn__rounded">Apply</a> -->
                                        </div>
                                   
                                        <!--end-->
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end-body-->  
        <?php include(APPPATH."views/website/inc/footer.php");?>
    </div>
        
    <?php include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>