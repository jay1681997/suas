<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>

    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-body--> 
            <section class="otp-verif contact-panel px-4 py-4">
                <div class="container">
                    <div class=" ">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li  class="breadcrumb-item active" aria-current="page">Gift Enrolled Class</li>
                            </ol>
                        </nav>
                        <!--end-nav-->
                    </div> 
                    
                    <div class="title mt-4">
                        <h4 class="mb-1 position-relative">Gift Enrolled Class</h4>
                        <p class="mt-0  fz-16">Enter recipient details below</p>
                    </div>
                    <?php if($this->session->flashdata('success_msg')){ ?>
                        <div class="alert alert-success" >
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg')?>
                        </div>                
                    <?php } ?>
                    <?php if($this->session->flashdata('error_msg')){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg')?>
                        </div>
                    <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                    <?php } ?>
                    <!--start-form-->
                    <?php echo form_open('gift-class/'.base64_encode($class_id), array('method' => 'post', 'enctype'=>'multipart/form-data','class'=>'mt-30 mb-10','id'=>'upload_speechform')); ?>
                        <div class="">
                            <div class="form-group">
                                <i class="form-group-icon icon-user text-success"></i>
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    id="name"
                                    name="name"
                                    placeholder="Name" 
                                    aria-required="true"
                                    parsley-trigger="change" 
                                    data-parsley-errors-container="#name_error"
                                    data-parsley-required-message="Please enter name" 
                                    required 
                                    value="<?php echo set_value('name'); ?>"
                                >
                                <?php echo form_error('name'); ?>
                                <div id="name_error" class="error" style="color: red;"></div>
                            </div>
                        </div>
                      
                        <div class="">
                            <div class="form-group">
                                <i class="icon-email form-group-icon text-success"></i>
                                <input 
                                    type="email" 
                                    class="form-control" 
                                    id="email" 
                                    name="email" 
                                    placeholder="Email" 
                                    aria-required="true" 
                                    parsley-trigger="change" 
                                    data-parsley-required-message="Please enter email" 
                                    required 
                                    data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  
                                    data-parsley-pattern-message="Please enter valid email address" 
                                    data-parsley-maxlength="64"
                                    value="<?php echo isset($email) ? $email : set_value('email'); ?>"
                                >
                                <?php echo form_error('email'); ?>
                            </div>
                        </div>
                        <div class="">
                            <div class="form-group">
                                <div class="input-group mb-3 mb-md-4 mb-lg-3 d-flex">
                                    <select class="form-select form-control" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:90px;">
                                        <option value="+91">+91</option>
                                        <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                            <option value="<?php echo $value['dial_code']; ?>" <?php echo ($country_codes==$value['dial_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['dial_code']; ?></option>
                                        <?php } } ?>
                                    </select>
                                    <?php echo form_error('country_code'); ?>
                                    <label id="countrieseerror"></label>
                                  
                                    <input 
                                        type="text" 
                                        class="form-control" 
                                        id="phone" 
                                        name="phone" 
                                        placeholder="Phone Number" 
                                        aria-required="true" 
                                        parsley-trigger="change" 
                                        data-parsley-type="number" 
                                        data-parsley-errors-container="#phoneerror"
                                        data-parsley-required-message="Please enter phone number"  
                                        required 
                                        onkeypress="return isNumberKey(event);"  
                                        maxlength="12" 
                                        data-parsley-minlength="8" 
                                        data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" 
                                        value="<?php echo set_value('phone'); ?>"
                                    >
                                    <?php echo form_error('phone'); ?>
                                </div>
                            <div id="phoneerror"></div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <a href="<?php echo base_url(); ?>online-class" class="btn btn__primary btn__rounded bg-transparent text-gray">Cancel</a>
                            </div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn__primary btn__rounded">Send</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
            <!--end-body--> 
            <!--start-modal-->
            
            <div class="modal fade" id="success_gift" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                            <div class="content">
                                <h4 class="mb-2">Gift Enrolled Class</h4>
                                <p class="fz-16">Your gift has been sent successfully.</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <a href="<?php echo base_url(); ?>gifted-enrolled-class" class="btn btn__primary btn__rounded">Continue</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->
               
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->

        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script>
            $(document).ready(function() {
                $('select').niceSelect('destroy');
                $('.select2').select2({"width":"100%"});    

                <?php  if($this->session->flashdata('success_msg')){ ?>
                    $('#success_gift').modal('show');
                <?php } ?>
            });
        </script>
    </body>
</html>