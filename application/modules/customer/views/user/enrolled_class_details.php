<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section class=" ">
                <div class="container">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>enrolled-classes">Enrolled Classes</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Enrolled Class Detail</li>
                        </ol>
                    </nav>
                    <!--end-nav-->
                    <!--start-row-->
                    <div class="main-list">
                        <div class="">
                            <div class="align-items-center inner-side row speech-box">
                                <div class="col-lg-6">
                                    <div class="speech-img position-relative">
                                        <img src="<?php echo $class_details['class_image']; ?>" alt="class5" class="w-100">
                                    </div>
                                </div>
                                <div class="content px-4 py-4 rounded shadow col-lg-6">
                                    <h4  class="text-gray mb-2"><?php echo $class_details['program_title']; ?></h4>
                                    <div class="row align-items-center mb-3">
                                        <div class="col-lg-8">
                                            <ul class="dots list-unstyled d-flex mb-0">
                                                <?php if($class_details['age_category'] == 'Primary'){
                                                    $age_range = '0/8';
                                                }else if($class_details['age_category'] == 'Juniors'){
                                                    $age_range = '9/12';
                                                }else if($class_details['age_category'] == 'Teens'){
                                                    $age_range = '13/17';
                                                }else if($class_details['age_category'] == 'Adults'){
                                                    $age_range = '18+';
                                                } ?>
                                                    <li class="pr-3"><?php echo $class_details['age_category'].' '.$age_range; ?></li>
                                                    <li class="pl-3">Grade <?php echo $class_details['grade']; ?></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <!-- <strong class="fz-16 theme-color">Starting At: <?php echo $this->common_model->date_convert($class_details['start_datetime'], 'h:i A',$this->session->userdata('website_timezone')); ?></strong> -->
                                            <strong class="fz-16 theme-color">Started </strong>
                                        </div>
                                    </div>
                                    <!--start-remaining-->
                                    <!-- <p class="text-gray fz-16 my-4"><span class="theme-color font-weight-bold">6</span>/<span class="font-weight-bold mr-1">10</span> Spots Remaining</p> -->
                                    <!--end-remaining-->
                                    <div class="row">
                                        <div class="col-lg-12 mb-3 d-flex align-items-center">
                                            <span class="mr-2">
                                                <svg id="Iconly_Bold_Calendar" data-name="Iconly/Bold/Calendar" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <g id="Calendar" transform="translate(3.049 2.032)">
                                                        <path id="Calendar-2" data-name="Calendar" d="M5.235,20.324H5.224a5.236,5.236,0,0,1-3.8-1.4,5.223,5.223,0,0,1-1.414-3.8L0,6.659A5.172,5.172,0,0,1,1.238,3.1,5.019,5.019,0,0,1,4.579,1.555V.793A.785.785,0,0,1,4.8.231.779.779,0,0,1,6.123.791V1.5l5.96-.008V.783a.782.782,0,0,1,.221-.561.779.779,0,0,1,1.323.559v.762A5.046,5.046,0,0,1,17,3.054,5.074,5.074,0,0,1,18.281,6.6l.011,8.563c0,3.176-1.993,5.152-5.211,5.157Zm8.019-5.9a.846.846,0,0,0-.822.865.843.843,0,1,0,.832-.865ZM5,14.422H5a.865.865,0,0,0-.821.874A.836.836,0,0,0,5,16.11h.039a.822.822,0,0,0,.574-.269.835.835,0,0,0,.218-.6A.826.826,0,0,0,5,14.423Zm4.134-.005h0a.869.869,0,0,0-.821.875.837.837,0,0,0,.827.812h.037a.837.837,0,0,0,.792-.875.826.826,0,0,0-.832-.811ZM4.992,10.766h0a.864.864,0,0,0-.821.874A.835.835,0,0,0,5,12.453h.039a.819.819,0,0,0,.572-.269.836.836,0,0,0,.218-.6.825.825,0,0,0-.831-.811Zm4.135-.036h0a.865.865,0,0,0-.822.874.837.837,0,0,0,.828.813h.037a.839.839,0,0,0,.791-.875.827.827,0,0,0-.831-.811Zm4.136.006h0a.84.84,0,0,0-.821.852V11.6a.827.827,0,0,0,.834.811h.019a.838.838,0,0,0-.029-1.676ZM4.581,3.108c-2.018.2-3.039,1.4-3.036,3.55l0,.316,15.191-.02V6.6c-.043-2.147-1.088-3.325-3.106-3.5v.782a.776.776,0,0,1-1.325.556.786.786,0,0,1-.22-.554V3.058l-5.96.008,0,.821A.777.777,0,0,1,4.8,4.444a.786.786,0,0,1-.22-.554V3.108Z" transform="translate(0)" fill="#17966b"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="text-gray"><?php echo $this->common_model->date_convert($class_details['start_datetime'], 'd M, Y',$this->session->userdata('website_timezone')).' - ',$this->common_model->date_convert($class_details['end_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                        </div>
                                        <div class="col-lg-12 mb-3 d-flex align-items-center">
                                            <span class="mr-2">
                                                <svg id="Iconly_Bold_Time_Square" data-name="Iconly/Bold/Time Square" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                    <g id="Time_Square" data-name="Time Square" transform="translate(2 2)">
                                                        <path id="Time_Square-2" data-name="Time Square" d="M14.34,20H5.67C2.279,20,0,17.624,0,14.089V5.919C0,2.379,2.279,0,5.67,0h8.67C17.725,0,20,2.379,20,5.919v8.169C20,17.624,17.725,20,14.34,20ZM9.65,4.919a.76.76,0,0,0-.75.75V10.72a.733.733,0,0,0,.37.64l3.92,2.34a.7.7,0,0,0,.39.11.745.745,0,0,0,.64-.37.72.72,0,0,0,.09-.552.762.762,0,0,0-.35-.477L10.4,10.29V5.669A.751.751,0,0,0,9.65,4.919Z" fill="#17966b"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="text-gray"><?php echo $this->common_model->date_convert($class_details['start_datetime'], 'h:i A',$this->session->userdata('website_timezone')).' - ',$this->common_model->date_convert($class_details['end_datetime'], 'h:i A',$this->session->userdata('website_timezone')); ?></span>
                                        </div>
                                    </div>
                            
                            
                                    <p class="text-gray mb-0 fz-16"><?php echo $class_details['description']; ?></p>
                                   <!--  <fieldset class="question pt-2 d-flex align-items-center mb-4">
                                        <input class="coupon_question mr-2" type="checkbox" name="coupon_question" value="1" onchange="valueChanged()" required>
                                        <label for="coupon_question" class="mb-0 ">I agree no refunds or make up classes are offered.</label>
                                    </fieldset> -->
                                    <!--start-->
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <a href="<?php echo base_url(); ?>joined-class/<?php echo base64_encode($class_details['id']); ?>" class="btn btn__primary btn__rounded d-flex"><span>Join Class</span></a>
                                        </div>
                                    </div>
                                    <!--end-row-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end-row-->
                </div>
            </section>
            <!--end-body-->
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?> 
    </body>
</html>