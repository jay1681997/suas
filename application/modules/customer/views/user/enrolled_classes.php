<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-body-->
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Online Class</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 
                 
                        <!--start-title-->
                        <div class="title">
                            <h4 class="mb-0 position-relative">Enrolled Classes <span class="class-count">(<?php echo count($enrolled_classes); ?>)</span></h4>
                            <p class="fz-16 mb-4 mt-0">Click to see the details of your 
                              upcoming class.</p>
                        </div>
                        <!--end-title-->
                
                        <!--start-row-->
                        <?php if(!empty($enrolled_classes)) {  ?>
                        <div class="row main-list ">
                            <?php foreach ($enrolled_classes as $key => $value) { ?>
                            <div class="col-md-6 col-lg-4 mb-4">
                                <a href="<?php echo base_url(); ?>enrolled-class-details/<?php echo base64_encode($value['item_id']); ?>" class="speech-box">
                                    <div class="speech-img position-relative">
                                        <img src="<?php echo $value['class_image']; ?>" alt="image1" class="w-100">
                                        <div class="overlay position-absolute w-100">  
                                            <div class="row align-items-end">
                                                <div class="col-lg-6 mb-lg-0 mb-2">
                                                    <div class="btt-plays">
                                                        <h6 class="text-white mb-1 mb-2"><?php echo $value['program_title']; ?></h6>
                                                        <p class="mb-0">
                                                            <span class="mr-2">
                                                                <svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 10.496 11.662">
                                                                    <path id="Calendar-2" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"/>
                                                                </svg>
                                                            </span>
                                                            <span class="text-white"><?php echo $this->common_model->date_convert($value['start_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 text-lg-right">
                                                    <div class="price d-block">
                                                        <span class="text-white font-weight-bold">Starting At: <?php echo $this->common_model->date_convert($value['start_datetime'], 'h:i A',$this->session->userdata('website_timezone')); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                            <h5><center>No enrolled classes found</center></h5>
                            <p><center>
                                If you want to purchase this enrolled classes then please click on the following link
                            :</center></p>
                            <p><center>
                                <a href="<?php echo base_url() ?>online-class">Online Classes</a>
                            </center></p>
                        <?php } ?>
                        <!--end-row-->

                       <!--  <nav aria-label="Page navigation example">
                            <ul class="pagination d-flex justify-content-center mt-10">
                                <li class="page-item">
                                    <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">1</a></li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">2</a></li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav> -->
                    </div>
                </div>
            </section>
            <!--end-body--> 
            
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
       <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>