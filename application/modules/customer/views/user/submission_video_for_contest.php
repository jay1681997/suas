<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section class="">
                    <div class="container">
                        <div class="contact-panel px-4 py-4 otp-verif">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>contest-list">Contest </a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Video For Contest</li>
                                </ol>
                            </nav>
                            <!--end-nav-->
          
                            <!--start-title-->
                            <div class="title">
                                <h4 class="mb-0 position-relative">Video Contest Submission</h4>
                                <p class="fz-16 mb-4 mt-0">Good luck in this contest!</p>
                            </div>
                            <!--end-title-->
                             <?php echo form_open('submission-video-for-contest/'.base64_encode($this->session->userdata('contest_id')), array('method' => 'post', 'enctype'=>'multipart/form-data', 'class'=>'my-5')); ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--start-row-->
                                    <div class="row shadow mb-50">
                                        <div class="col-lg-3 py-3">
                                            <img src="<?php echo $contest_details['contest_image']; ?>" alt="profile-image123" class="w-100">
                                        </div>
                                        <div class="col-lg-9 pl-lg-0 pl-3 py-3">
                                            <div class="profile-info">
                                                <h5 class="mb-1"><?php echo $contest_details['name']; ?></h5>
                                                <p><?php echo $contest_details['description']; ?></p>
                                                <span class="fz-16 theme-color font-weight-bolder"><?php echo $this->session->userdata('currency').''.$contest_details['price']; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end-row-->

                                    <div class="your-video mb-80">
                                        <h5>Your video</h5>
                                        <div class="video">
                                            <!-- <video controls="" class="w-100 rounded">
                                                <source src="<?php echo base_url(); ?>website_assets/images/video/video_preview_h264.mp4" class="w-100">
                                            </video> -->
                                            <div class="file-upload">
                                                <div class="image-upload-wrap">
                                                    <input type="file" id="fileInput" class="file-upload-input" name="video" accept="video/*" parsley-trigger="change" data-parsley-required-message="Please upload video" required data-parsley-errors-container="#videoeerror" onchange="readURL(this);">

                                                    <div class="drag-text">
                                                        <span class="  d-block">
                                                            <svg id="Iconly_Bold_Upload" data-name="Iconly/Bold/Upload" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 42 42">
                                                                <g id="Upload" transform="translate(3.5 3.5)">
                                                                    <path id="Upload-2" data-name="Upload" d="M7.787,36.365A7.856,7.856,0,0,1,.008,28.815L0,28.441V19.754a7.848,7.848,0,0,1,7.4-7.88l.365-.008h8.384V22.549a1.346,1.346,0,0,0,2.682.187l.013-.187V11.866h8.365a7.856,7.856,0,0,1,7.779,7.549l.008.373V28.46a7.852,7.852,0,0,1-7.4,7.9l-.365.008Zm8.366-24.5V4.619L13.37,7.42a1.353,1.353,0,0,1-1.907,0,1.336,1.336,0,0,1-.148-1.756l.131-.151L16.538.4A1.336,1.336,0,0,1,17.5,0a1.319,1.319,0,0,1,.788.26L18.445.4l5.094,5.109a1.349,1.349,0,0,1-1.758,2.038l-.15-.131-2.783-2.8v7.245Z" fill="#898e95"/>
                                                                </g>
                                                            </svg>
                                                        </span>
                                                        <p class="drop-zoon__paragraph">You Can Upload Video</p>
                                                    </div>
                                                </div>
                                                <div class="file-upload-content">
                                                    <!-- <video class="file-upload-image" src="#" ></video> -->
                                                    <video controls="" class="file-upload-image w-100 rounded">
                                                        <source src="#" class="w-100">
                                                    </video> 
                                                    <span class="image-title d-block">Uploaded Image</span>
                                                    <div class="image-title-wrap">
                                                 
                                                        <button type="button" onclick="removeUpload()" class="remove-image">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                                            </svg> 
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php echo form_error('video'); ?>
                                            <div id="videoeerror"></div>
                                        </div>
                                        <div class="note">
                                            <p><span class="text-dark">Note: </span> Preview your video before submitting to contest. One video submission per user.</p>
                                        </div>
                                    </div>
                                </div> 
                            </div> 

                            <div class="d-flex justify-content-center">
                                <!-- <a href="#" class="btn btn__primary btn__rounded " data-toggle="modal" data-target="#submit-video"><span>Submit Video</span></a> -->
                                <button type="submit" class="btn btn__primary btn__rounded" ><span>Submit Video</span></button>
                            </div>
                            <?php  //echo date("Y-m-d H:i:s",strtotime('-1 days')); ?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </section>
                <!--end-body--> 

                <!--start-modal-->
                <div class="modal fade" id="submit-video"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94" class="mb-25">
                                <div class="content">
                                    <h4>Video</h4>
                                    <p class="fz-16 text-dark">Your video has been submitted
                                    successfully</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0">
                                <a href="<?php echo base_url(); ?>my-contest-online" class="btn btn__primary btn__rounded">Continue</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">
            $(document).ready(function(){
                <?php if(!empty($this->session->userdata('upload_video'))){ ?>
                    $('#submit-video').modal('show');
                <?php } ?>
            });
            function readURL(input) {
                if (input.files && input.files[0]) {

                    var reader = new FileReader();

                    reader.onload = function(e) {
                      $('.image-upload-wrap').hide();

                      $('.file-upload-image').attr('src', e.target.result);
                      $('.file-upload-content').show();

                      $('.image-title').html(input.files[0].name);
                    };

                    reader.readAsDataURL(input.files[0]);

                } else {
                    removeUpload();
                }
            }

            function removeUpload() {
                $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                $('.file-upload-content').hide();
                $('.image-upload-wrap').show();
            }
            $('.image-upload-wrap').bind('dragover', function () {
                $('.image-upload-wrap').addClass('image-dropping');
            });
            $('.image-upload-wrap').bind('dragleave', function () {
                $('.image-upload-wrap').removeClass('image-dropping');
            });
        </script>
    </body>
</html>