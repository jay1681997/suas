<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style type="text/css">
        .speech-box.inner-side .speech-img>img{
            height: 200px;
        }
    </style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header1.php");?>
            <!--start-body-->
            <section class="">
                <div class="container">
                    <div class=" px-4 py-4 contact-panel">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>my-evaluations">My Evaluations</a></li>
                                <li  class="breadcrumb-item active" aria-current="page">My Evaluations Detail</li>
                            </ol>
                        </nav>
                        <!--end-nav-->

                        <!--start-row-->
                        <div class="main-list">
                            <div class="">
                                <div class="speech-box inner-side">
                                    <div class="speech-img position-relative">
                                        <div class="video-intro">
                                            <div class="image">
                                                <div class="play-button"></div>
                                                <a href="javascript:" id="btn_play" class="">
                                                    <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play-video" />
                                                </a>
                                            </div> 

                                            <video id="myVideo"controls class="w-100">
                                                <source  src="<?php echo S3_BUCKET_ROOT.SPEECH_IMAGE.$evaluation_details['video']; ?>" class="w-100" />
                                            </video>
                                           
                                        </div>
                                        <div class="overlay position-absolute" align="right">
                                            <div class="btt-plays">
                                                <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                    <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="Mask Group -3" class="mr-2">
                                                    <span class="text-white"><?php echo $evaluation_details['video_duration']; ?> Min</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content py-4">
                                        <h4  class="text-gray mb-2" style="color:black;"><?php echo $evaluation_details['title']; ?></h4>
                                        <p class="mb-3 d-flex align-items-center">
                                            <span class="mr-2">
                                                <svg id="Iconly_Bold_Calendar" data-name="Iconly/Bold/Calendar" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                                    <g id="Calendar" transform="translate(3.049 2.032)">
                                                        <path id="Calendar-2" data-name="Calendar" d="M5.235,20.324H5.224a5.236,5.236,0,0,1-3.8-1.4,5.223,5.223,0,0,1-1.414-3.8L0,6.659A5.172,5.172,0,0,1,1.238,3.1,5.019,5.019,0,0,1,4.579,1.555V.793A.785.785,0,0,1,4.8.231.779.779,0,0,1,6.123.791V1.5l5.96-.008V.783a.782.782,0,0,1,.221-.561.779.779,0,0,1,1.323.559v.762A5.046,5.046,0,0,1,17,3.054,5.074,5.074,0,0,1,18.281,6.6l.011,8.563c0,3.176-1.993,5.152-5.211,5.157Zm8.019-5.9a.846.846,0,0,0-.822.865.843.843,0,1,0,.832-.865ZM5,14.422H5a.865.865,0,0,0-.821.874A.836.836,0,0,0,5,16.11h.039a.822.822,0,0,0,.574-.269.835.835,0,0,0,.218-.6A.826.826,0,0,0,5,14.423Zm4.134-.005h0a.869.869,0,0,0-.821.875.837.837,0,0,0,.827.812h.037a.837.837,0,0,0,.792-.875.826.826,0,0,0-.832-.811ZM4.992,10.766h0a.864.864,0,0,0-.821.874A.835.835,0,0,0,5,12.453h.039a.819.819,0,0,0,.572-.269.836.836,0,0,0,.218-.6.825.825,0,0,0-.831-.811Zm4.135-.036h0a.865.865,0,0,0-.822.874.837.837,0,0,0,.828.813h.037a.839.839,0,0,0,.791-.875.827.827,0,0,0-.831-.811Zm4.136.006h0a.84.84,0,0,0-.821.852V11.6a.827.827,0,0,0,.834.811h.019a.838.838,0,0,0-.029-1.676ZM4.581,3.108c-2.018.2-3.039,1.4-3.036,3.55l0,.316,15.191-.02V6.6c-.043-2.147-1.088-3.325-3.106-3.5v.782a.776.776,0,0,1-1.325.556.786.786,0,0,1-.22-.554V3.058l-5.96.008,0,.821A.777.777,0,0,1,4.8,4.444a.786.786,0,0,1-.22-.554V3.108Z" transform="translate(0)" fill="#17966b"/>
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="text-gray fz-16"><?php echo $this->common_model->date_convert($evaluation_details['insert_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                        </p>
                                        <?php 
                                        $v1_l = explode(".", $evaluation_details['pace_result']);
                                          $pace_result = ($v1_l[1] == 0)?$v1_l[0]:$evaluation_details['pace_result'];
                                        $v2_l = explode(".", $evaluation_details['energy_result']);
                                          $energy_result = ($v2_l[1] == 0)?$v2_l[0]:$evaluation_details['energy_result'];
                                        $v3_l = explode(".", $evaluation_details['fillerword_result']);
                                          $fillerword_result = ($v3_l[1] == 0)?$v3_l[0]:$evaluation_details['fillerword_result'];
                                        $v4_l = explode(".", $evaluation_details['conciseness_result']);
                                          $conciseness_result = ($v4_l[1] == 0)?$v4_l[0]:$evaluation_details['conciseness_result'];
                                        $v5_l = explode(".", $evaluation_details['eyecontact_result']);
                                          $eyecontact_result = ($v5_l[1] == 0)?$v5_l[0]:$evaluation_details['eyecontact_result'];
                                         ?>
                                        <?php if($evaluation_details['is_evaluated'] == 'yes'){ ?>
                                        <!--start-row-->
                                        <div class="row mt-4 text-center">
                                            <?php if($evaluation_details['pace_result'] == '0'){ ?>
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                                <span class="d-block fz-16 text-dark mt-2">Pace</span>
                                            </div>
                                            <?php }else{ ?> 
                                                <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                    <div class="circle-a position-relative" id="circle-a">
                                                    </div>
                                                    <span class="outof"><?php echo $pace_result; ?>/5</span>
                                                    <span class="d-block fz-16 text-dark mt-2">Pace</span>
                                                </div>
                                            <?php } if($evaluation_details['energy_result'] == '0'){ ?>
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                                <span class="d-block fz-16 text-dark mt-2">Energy</span>
                                            </div>
                                            <?php }else{ ?> 
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circle-b position-relative" id="circle-b"></div>
                                                <span class="outof"><?php echo $energy_result; ?>/5</span>
                                                <span class="d-block fz-16 text-dark mt-2">Energy</span>
                                            </div>
                                            <?php } if($evaluation_details['fillerword_result'] == '0'){ ?>
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                                <span class="d-block fz-16 text-dark mt-2">Filler Words</span>
                                            </div>
                                            <?php }else{ ?> 
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circle-c position-relative" id="circle-c"></div>
                                                <span class="outof"><?php echo $fillerword_result; ?>/5</span>
                                                <span class="d-block fz-16 text-dark mt-2">Filler Words</span>
                                            </div>
                                            <?php } if($evaluation_details['conciseness_result'] == '0'){ ?>
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                                <span class="d-block fz-16 text-dark mt-2">Conciseness</span>
                                            </div>
                                            <?php }else{ ?> 
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circle-d position-relative" id="circle-d"> </div>
                                                <span class="outof"><?php echo $conciseness_result; ?>/5</span>
                                                <span class="d-block fz-16 text-dark mt-2">Conciseness</span>
                                            </div>
                                            <?php } if($evaluation_details['eyecontact_result'] == '0'){ ?>
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                                <span class="d-block fz-16 text-dark mt-2">Eye Contact</span>
                                            </div>
                                            <?php }else{ ?> 
                                            <div class="col-lg col-md-4 col-sm-6  mb-4">
                                                <div class="circle-f position-relative" id="circle-f"></div>
                                                <span class="outof"><?php echo $eyecontact_result; ?>/5</span>
                                                <span class="d-block fz-16 text-dark mt-2">Eye Contact</span>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="content">
                                            <p class="fz-16"><?php echo $evaluation_details['description']; ?></p>
                                        </div>
                                        <!--end-row-->

                                        <!--start-accordion-->
                                        <div id="accordion" class="custom-mb-acc">
                                            <div class="card border-0 acc-cutm-arr">
                                                <div class="card-header bg-transparent border-0" id="headingOne">
                                                    <h5 class="mb-0 text-center text-sm-left">
                                                        <button class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="d-sm-flex align-items-center">
                                                                <?php if($evaluation_details['pace_result'] == '0'){ ?>
                                                                <div class="circles error" id="">
                                                                    <strong>!</strong>
                                                                </div>
                                                                <?php }else{ ?> 
                                                                <div class="circle-a position-relative" id="circle-a"></div>
                                                                <span class="outof sin-block"><?php echo $pace_result; ?>/5</span>
                                                                <?php } ?>
                                                                
                                                                <span class="d-block fz-16 text-dark ml-20 ">Pace</span>
                                                            </div>

                                                            <div class="arrow">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/Path -13.png" alt="Path-13">
                                                            </div>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                    <div class="card-body pt-0">
                                                        <ul class="">
                                                            <li class="mb-2 fz-16"><?php echo $evaluation_details['pace_description']; ?></li>
                                                        </ul>
                                                        <?php if($evaluation_details['pace_result'] == '0'){ ?>
                                                        <a href="#" class="btn btn__primary btn__rounded d-flex w-25" data-toggle="modal" data-target="#exampleModalCenter"><span> Manually Evaluate</span></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-0 acc-cutm-arr">
                                                <div class="card-header bg-transparent border-0" id="headingTwo">
                                                    <h5 class="mb-0 text-center text-sm-left">
                                                        <button class="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            <div class="d-sm-flex align-items-center">
                                                                <?php if($evaluation_details['energy_result'] == '0'){ ?>
                                                                <div class="circles error" id="">
                                                                    <strong>!</strong>
                                                                </div>
                                                                <?php }else{ ?>
                                                                <div class="circle-b position-relative" id="circle-b"></div>
                                                                <span class="outof sin-block"><?php echo $energy_result; ?>/5</span>
                                                                <?php } ?>
                                                                <span class="d-block fz-16 text-dark ml-20 ">Energy</span>
                                                            </div>
                                                            <div class="arrow">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/Path -13.png" alt="Path-13">
                                                            </div>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                                                    <div class="card-body pt-0"> 
                                                        <ul class="">
                                                            <li class="mb-2 fz-16"><?php echo $evaluation_details['energy_description']; ?></li>
                                                        </ul>
                                                        <?php if($evaluation_details['energy_result'] == '0'){ ?>
                                                        <a href="#" class="btn btn__primary btn__rounded d-flex w-25" data-toggle="modal" data-target="#exampleModalCenter"><span> Manually Evaluate</span></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card border-0 acc-cutm-arr">
                                                <div class="card-header bg-transparent border-0" id="headingThree">
                                                    <h5 class="mb-0 text-center text-sm-left">
                                                        <button class="" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            <div class="d-sm-flex align-items-center">
                                                                <?php if($evaluation_details['fillerword_result'] == '0'){ ?>
                                                                <div class="circles error" id="">
                                                                    <strong>!</strong>
                                                                </div>
                                                                <?php }else{ ?>
                                                                <div class="circle-c position-relative" id="circle-c"></div>
                                                                <span class="outof sin-block"><?php echo $fillerword_result; ?>/5</span>
                                                                <?php } ?>
                                                                <span class="d-block fz-16 text-dark ml-20 ">Filler Words</span>
                                                            </div>
                                                            <div class="arrow">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/Path -13.png" alt="Path-13">
                                                            </div>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
                                                    <div class="card-body pt-0"> 
                                                        <ul class="">
                                                            <li class="mb-2 fz-16"><?php echo $evaluation_details['fillerword_description']; ?></li>
                                                        </ul> 
                                                        <?php if($evaluation_details['fillerword_result'] == '0'){ ?>
                                                        <a href="#" class="btn btn__primary btn__rounded d-flex w-25" data-toggle="modal" data-target="#exampleModalCenter"><span> Manually Evaluate</span></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card border-0 acc-cutm-arr">
                                                <div class="card-header bg-transparent border-0" id="headingFour">
                                                    <h5 class="mb-0 text-center text-sm-left">
                                                        <button class="" data-toggle="collapse show" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                            <div class="d-sm-flex align-items-center">
                                                                <?php if($evaluation_details['conciseness_result'] == '0'){ ?>
                                                                <div class="circles error" id="">
                                                                    <strong>!</strong>
                                                                </div>
                                                                <?php }else{ ?>
                                                                <div class="circle-d position-relative" id="circle-d"></div>
                                                                <span class="outof sin-block"><?php echo $conciseness_result; ?>/5</span>
                                                                <?php } ?>
                                                                <span class="d-block fz-16 text-dark ml-20 ">Conciseness</span>
                                                            </div>
                                                            <div class="arrow">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/Path -13.png" alt="Path-13">
                                                            </div>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordion">
                                                    <div class="card-body pt-0"> 
                                                        <ul class="">
                                                            <li class="mb-2 fz-16"><?php echo $evaluation_details['conciseness_description']; ?></li>
                                                        </ul> 
                                                        <?php if($evaluation_details['conciseness_result'] == '0'){ ?>
                                                        <a href="#" class="btn btn__primary btn__rounded d-flex w-25" data-toggle="modal" data-target="#exampleModalCenter"><span> Manually Evaluate</span></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card border-0 acc-cutm-arr">
                                                <div class="card-header bg-transparent border-0" id="headingFive">
                                                    <h5 class="mb-0 text-center text-sm-left">
                                                        <button class="" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                                            <div class="d-sm-flex align-items-center">
                                                                <?php if($evaluation_details['eyecontact_result'] == '0'){ ?>
                                                                <div class="circles error" id="">
                                                                    <strong>!</strong>
                                                                </div>
                                                                <?php }else{ ?>
                                                                <div class="circle-f position-relative" id="circle-f"></div>
                                                                <span class="outof sin-block"><?php echo $eyecontact_result; ?>/5</span>
                                                                <?php } ?>
                                                                <span class="d-block fz-16 text-dark ml-20 ">Eye Contact</span>
                                                            </div>
                                                            <div class="arrow">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/Path -13.png" alt="Path-13">
                                                            </div>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#accordion">
                                                    <div class="card-body pt-0"> 
                                                        <ul class="">
                                                           <li class="mb-2 fz-16"><?php echo $evaluation_details['eyecontact_description']; ?></li>
                                                        </ul>
                                                        <?php if($evaluation_details['eyecontact_result'] == '0'){ ?>
                                                        <a href="#" class="btn btn__primary btn__rounded d-flex w-25" data-toggle="modal" data-target="#exampleModalCenter"><span> Manually Evaluate</span></a>
                                                        <?php } ?> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end-accordion-->

                                        <!--start-slider-->
                                        <?php if(!empty($evaluation_details['suggested_courses'])){ ?>
                                        <div class="suggested-cours">
                                            <h4>Suggested Course Or Video</h4>
                                                <!--start-row-->
                                            <div class="row">
                                                <?php foreach ($evaluation_details['suggested_courses'] as $key => $value) {?>
                                                <div class="col-md-6 col-lg-4 mb-4">
                                                    <a href="<?php echo base_url(); ?>my-suggested-details/<?php echo base64_encode($value['id']); ?>" class="speech-box">
                                                        <div class="speech-img position-relative">
                                                           
                                                            <!-- <video src=" <?php echo $value['video']; ?> " class="w-100"> </video> -->
                                                            <img src="<?php echo $value['thumb_image']; ?>" >
                                                            <div class="overlay position-absolute">
                                                                <div class="btt-plays">
                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                        <img src="<?php echo base_url(); ?>website_assets/images/speech/play-btn.png" alt="play-btn" class="mr-2">
                                                                        <span class="text-white"><?php echo $value['video_duration']; ?> min</span>
                                                                    </div>
                                                                    <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6>
                                                                    <p class="mb-0">
                                                                        <span class="mr-2"><svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="10.496" height="11.662" viewBox="0 0 10.496 11.662">
                                                                        <path id="Calendar-2" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"></path>
                                                                        </svg></span>
                                                                        <span class="text-white"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <!--end-->
                                        </div>
                                        <?php } }else{ ?>
                                            <div class="text-center mb-4"  style="background-color:#E7FDF5">
                                                <img src="<?php echo base_url(); ?>website_assets/images/evaluation.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                                <h6 class="mb-10">Your speech is being evaluated, <br>we will notify you when it is completed.</h6>
                                            </div>
                                            <div>
                                                <p><?php echo $evaluation_details['description']; ?></p>
                                                
                                            </div>
                                        <?php } ?>
                                        <!--end-slider-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end-row-->
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <!--start-modal-->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                            <div class="content">
                                <h4 class="mb-2">Paid Evaluation</h4>
                                <p class="fz-16">There is a charge for speeches manually evaluated. Do you still want to continue?</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-around">
                            <button class="btn btn__primary btn__outlined btn__rounded fz-16 border-0 text-gray"  data-dismiss="modal" aria-label="Close">No</button>
                            <?php $this->session->set_userdata('speech_id', $evaluation_details['speech_id']); ?>
                            <a href="<?php echo base_url(); ?>customer/user/add_item_intocart/speech" class="btn btn__primary btn__rounded bg-danger" >Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->

        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script>  
        var vid = document.getElementById("myVideo");
vid.onplay = function() {
  // alert("The video has started to play");
  $("#btn_play").hide();
};    
vid.onpause = function(){
       $("#btn_play").show();
}
             <?php if($evaluation_details['pace_result'] == '1') { ?>
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['pace_result'] == '2') { ?> 
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['pace_result'] == '3') { ?>    
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['pace_result'] == '4') { ?> 
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['pace_result'] == '5') { ?> 
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datas_pace = (($evaluation_details['pace_result'] * 20)/100);
                ?> 
                 $('.circle-a').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value: <?php echo $value_datas_pace; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>   

            <?php if($evaluation_details['energy_result'] == '1') { ?>
                $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['energy_result'] == '2') { ?> 
                $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['energy_result'] == '3') { ?>    
                 $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['energy_result'] == '4') { ?> 
                $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['energy_result'] == '5') { ?> 
                $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datas_eyecontact = (($evaluation_details['eyecontact_result'] * 20)/100);
             
            ?> 
               $('.circle-b').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value: <?php echo $value_datas_eyecontact; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>
            
            <?php if($evaluation_details['fillerword_result'] == '1') { ?>
                $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['fillerword_result'] == '2') { ?> 
                $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['fillerword_result'] == '3') { ?>    
                 $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['fillerword_result'] == '4') { ?> 
                $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['fillerword_result'] == '5') { ?> 
                $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datas_fillerword = (($evaluation_details['fillerword_result']*20)/100);
                ?> 
               $('.circle-c').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value: <?php echo $value_datas_fillerword; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>

            <?php if($evaluation_details['conciseness_result'] == '1') { ?>
                $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['conciseness_result'] == '2') { ?> 
                $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['conciseness_result'] == '3') { ?>    
                 $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['conciseness_result'] == '4') { ?> 
                $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['conciseness_result'] == '5') { ?> 
                $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datas_conciseness = (($evaluation_details['conciseness_result']*20)/100);
            ?> 
               $('.circle-d').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value: <?php echo $value_datas_conciseness; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>

            <?php if($evaluation_details['eyecontact_result'] == '1') { ?>
               
                $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['eyecontact_result'] == '2') { ?> 
                $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['eyecontact_result'] == '3') { ?>    
                 $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['eyecontact_result'] == '4') { ?> 
                $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($evaluation_details['eyecontact_result'] == '5') { ?> 
                $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datae_eyecontact = (($evaluation_details['eyecontact_result']*20)/100);
                ?> 
               $('.circle-f').circleProgress({
                    startAngle: -1.55,
                    size: 100,
                    value:<?php echo $value_datae_eyecontact; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>
        </script>
    </body>
</html>