<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style>
        .speech-box.img-height .speech-img>img {
            height: 225px;
            object-fit: cover;
        }
    </style>
    
<style>
    /* new css */

    .currancy_block {
        display: inline-block;
        background: #15976b;
        border-radius: 50%;
        width: 24px;
        height: 24px;
        line-height: 25px;
        padding: 2px 2px;
        text-align:center;
    }
    .currency_part {
        color: #ffffff;
        font-size: 12px;
        border: 1px solid #fff;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        line-height: 20px;
    }
</style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section>
                    <div class="container">
                        <div class="contact-panel px-2 px-lg-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Video Library</li>
                                </ol>
                            </nav>
                            <!--end-nav--> 

                            <div class="container bootstrap snippets bootdey">
                                <div class="row">
                                    <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                    <div class="profile-info col-lg-9">
                                        <div class="shadow"> 
                                            <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                                <!--start-title-->
                                                <div class="title">
                                                    <h4 class="line mb-3 pb-20 position-relative">Purchased Videos</h4> 
                                                    <p class="fz-16 mb-4">Watch your purchased educational videos at any time.</p>
                                                </div>
                                                <!--end-title-->
                                                <?php if(!empty($user_videos)) { ?>
                                                <div class="row"> 
                                                    <?php foreach ($user_videos as $key => $value) { ?>
                                                        <div class="col-md-6 mb-4">
                                                            <a href="<?php echo base_url(); ?>purchased-video-library-details/<?php echo base64_encode($value['item_id']); ?>" class="speech-box img-height">
                                                                <div class="speech-img position-relative bg-overlay-blck">
                                                                    <img src="<?php echo $value['video']; ?>" alt="image1" class="w-100">
                                                                    <div class="overlay position-absolute w-100">
                                                                        <div class="row align-items-end">
                                                                            <div class="col-lg-7 mb-lg-0 mb-2 text-lg-left">
                                                                                <div class="btt-plays">
                                                                                    <span class="text-white">
                                                                                        <div class="btt-plays">
                                                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                                <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play" class="mr-2">
                                                                                                <span class="text-white"><?php echo $value['video_duration']; ?> min</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                                    <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6> 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--start-price-tage-->
                                                                    <div class="pric-tag-tp">
                                                                        <div class="btt-plays">
                                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                <div class="currancy_block">
                                                                              <div class="currency_part"><?php echo $this->session->userdata('currency'); ?></div>
                                                                            </div>
                                                                                <span class="text-white"><?php echo round($this->session->userdata('currency_rate')*$value['price'],2); ?></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end-price-tage-->
                                                                </div> 
                                                            </a>
                                                        </div>
                                                    <?php } ?>
                                                </div> 
                                                <?php } else { ?>
                                                    <h5><center>You have no videos purchased yet.</center></h5>
                                                      <p><center>
                                                            If you want to purchase this videos then please click on the following link
                                                        :</center></p>
                                                        <p><center>
                                                            <a href="<?php echo base_url() ?>video-library">Video Library</a>
                                                    </center></p>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>