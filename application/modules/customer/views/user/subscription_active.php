<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
    
                <!--start-body-->
                <section class="">
                    <div class="container">
                        <div class="contact-panel px-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url(); ?>subscription">Subscriptions</a></li>
                                </ol>
                            </nav>
                            <!--end-nav--> 
                            <!--start-title-->
                            <div class="title mt-4 mb-4">
                                <h4 class="mb-0 position-relative">Active Subscriptions</h4>
                                <p class="fz-16 mt-0">Lorem ipsum dolor sit amet, consectetur adipiscig elit. Phasellus blandit nisl eget mollis lacinia. In con maximus.</p>
                            </div>
                            <!--end-title-->
                            <?php if(!empty($active_subscriptions)) { ?>
                            <div class="select-plan my-5">
                                <!-- <h5>AI Evaluation</h5> -->
                                <div class="row align-items-center">
                                    <?php foreach ($active_subscriptions as $key => $value) { 
                                        if($value['evaluation_type'] == 'AI') { ?>
                                    <div class="col-lg-4 mb-lg-0 mb-4">
                                        <div class="add-adres  bg-primary">
                                            <div class="add-deta shadow theme-rounded"> 
                                                <label class="custom-mine d-flex">
                                                    <div class="">
                                                        <h4 class="text-white"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                        <p class="fz-16 text-white"><?php echo $value['total_video']; ?> Videos Evaluations</p>
                                                        <span class="d-block mb-3 text-white">Validity - <?php echo $value['validity_number'].' '.ucfirst($value['duration']); ?></span>
                                                        <p class="text-white"><?php echo $value['description']; ?></p>
                                                    </div>
                                                </label> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="content">
                                            <ul class="list-unstyled fz-16">
                                                <li class="mb-3"><span>Status</span>-<span><?php echo $value['subscription_status']; ?></span></li>
                                                <li class="mb-3"><span>Validity </span>-<span><?php echo $this->common_model->date_convert($value['validity'], 'd F Y',$this->session->userdata('website_timezone')); ?></span></li>
                                                <li class="mb-3"><span>Total Videos</span>-<span><?php echo $value['total_video']; ?> Evaluations</span></li>
                                                <li class="mb-3"><span>Remaining Videos</span>-<span><?php echo $value['total_video']; ?> Evaluations</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php } } ?>
                                </div>
                            </div>

                            <div class="select-plan mb-4">
                                <h5>Manual Evaluation</h5>
                                <div class="row align-items-center"> 
                                    <?php foreach ($active_subscriptions as $key => $value) { 
                                        if($value['evaluation_type'] == 'Manual') { ?>
                                    <div class="col-lg-4 mb-lg-0 mb-4">
                                        <div class="add-adres  bg-primary">
                                            <div class="add-deta shadow theme-rounded"> 
                                                <label class="custom-mine d-flex">
                                                    <div class="">
                                                        <h4 class="text-white"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                        <p class="fz-16 text-white"><?php echo $value['total_video']; ?> Videos Evaluation</p>
                                                        <span class="d-block mb-3 text-white">Validity - <?php echo $value['validity_number'].' '.$value['duration']; ?></span>
                                                        <p class="text-white"><?php echo $value['description']; ?> </p>
                                                    </div>
                                                </label> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="content">
                                            <ul class="list-unstyled fz-16">
                                                <li class="mb-3"><span>Status</span>-<span><?php echo $value['subscription_status']; ?></span></li>
                                                <li class="mb-3"><span>Validity </span>-<span><?php echo $this->common_model->date_convert($value['validity'], 'd F Y',$this->session->userdata('website_timezone')); ?></span></li>
                                                <li class="mb-3"><span>Total Videos</span>-<span><?php echo $value['total_video']; ?> Evaluations</span></li>
                                                <li class="mb-3"><span>Remaining Videos</span>-<span><?php echo $value['remaining_video']; ?> Evaluations</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php } } ?>
                                </div>
                            </div> 
                            <?php }else { ?>
                                <h5><center>No active subscription package found</center></h5>
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <!--end-body-->  
            <?php include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>