<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Subscriptions</li>
                            </ol>
                        </nav>
                        <div class="title mt-4 mb-4">
                            <h4 class="mb-0 position-relative">Subscriptions</h4>
                            <p class="fz-16 mt-0">Lorem ipsum dolor sit amet, consectetur adipiscig elit. Phasellus blandit nisl eget mollis lacinia. In con maximus.</p>
                        </div>
                        <div class="mb-4">
                            <a href="subscription-active.html#" class="btn btn__primary btn__rounded">My Active Subscriptions</a>
                        </div>
                        <div class="select-plan">
                            <h5>AI Evaluation</h5>
                            <div class="row">
                                <?php if(!empty($subscription)) { foreach ($subscription as $key => $value) {
                                    if($value['evaluation_type'] == 'AI'){ ?>
                                        <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                            <div class="add-adres">
                                                <div class="add-deta shadow theme-rounded"> 
                                                    <label class="custom-mine d-flex">
                                                        <div class="">
                                                            <h4><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                            <p class="fz-16"><?php echo $value['total_video']; ?> Videos Evaluations</p>
                                                            <span class="d-block mb-3">Validity - <?php echo $value['validity'].' '.$value['duration']; ?></span>
                                                            <p><?php echo $value['description']; ?></p>
                                                        </div>
                                                        <input type="radio" name="que-1" class="ml-auto option-input radio " checked="">
                                                    </label> 
                                                </div>
                                            </div>
                                        </div>
                                <?php }}} ?>
                            </div>
                        </div>
                        <div class="select-plan mt-30">
                            <h5>Manual Evaluation</h5>
                            <div class="row">
                                <?php if(!empty($subscription)) { foreach ($subscription as $key => $value) {
                                    if($value['evaluation_type'] == 'Manual'){ ?>
                                        <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                            <div class="add-adres">
                                                <div class="add-deta shadow theme-rounded"> 
                                                    <label class="custom-mine d-flex">
                                                        <div class="">
                                                            <h4><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></h4>
                                                            <p class="fz-16"><?php echo $value['total_video']; ?> Videos Evaluations</p>
                                                            <span class="d-block mb-3">Validity - <?php echo $value['validity'].' '.$value['duration']; ?></span>
                                                            <p><?php echo $value['description']; ?></p>
                                                        </div>
                                                        <input type="radio" name="que-2" class="ml-auto option-input radio " checked="">
                                                    </label> 
                                                </div>
                                            </div>
                                        </div>
                                <?php }}} ?>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mb-4 mt-4">
                            <a href="#" class="btn btn__primary btn__rounded">Continue</a>
                        </div>
                    </div>
                </div>
            </section>
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>