<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style type="text/css">
        .speech-img {
    height: 300px;
    overflow: hidden;
}
.speech-img video {
    object-fit: cover !important;
    width: 100%;
    height: 100%;
    object-position: center;
}

@media screen and (max-width: 767px) {
    .speech-img {
        height: 180px;
        overflow: hidden;
    }
}
        .res-my-ev-block {
            align-items: end;
        }
        .circles.error{
            width: 60px !important;
            height: 60px !important;
        }
       /* .span.outof{
            top: 20px !important;
        }*/

    </style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section class="">
                    <div class="container">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li  class="breadcrumb-item active" aria-current="page">My Evaluations</li>
                            </ol>
                        </nav>
                        <!--end-nav-->
                     
                        <!--start-title-->
                        <div class="title mt-4">
                            <h4 class="mb-0 position-relative">My Evaluations</h4>
                            <p class="fz-16 mb-4 mt-0">Build confidence one evaluation at a time.</p>
                        </div>
                        <!--end-title-->
                    
                        <!--start-row-->
                        <?php if(!empty($evaluations)) { ?>
                        <div class="row main-list my-evaluations">
                            <?php foreach ($evaluations as $key => $value) {?>
                            <div class="col-lg-6 mb-lg-4">
                                <a href="<?php echo base_url(); ?>my-evaluations-details/<?php echo base64_encode($value['id']); ?>" class="speech-box shadow">
                                    <div class="speech-img position-relative">
                                        <video  src=" <?php echo S3_BUCKET_ROOT.SPEECH_IMAGE.$value['video']; ?> " class="w-100"  style="object-fit: cover;"> 
                                        </video>
                                        <!-- <div class="overlay position-absolute w-100 px-5 py-4"> -->
                                             <div class="overlay position-absolute w-100 px-5 py-4 resp-padding-ev">
                                          <!--   <div class="btt-plays row justify-content-between align-items-end"> -->
                                             <div class="row mt-4 res-my-ev-block">
                                                <div class="col-6 col-lg-6">
                                                    <h6 class="text-white mb-1 mb-2" style="font-size:42px"><?php echo $value['title']; ?></h6>  
                                                    <p class="mb-0">
                                                        <span class="mr-2">
                                                            <svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="20.496" height="21.662" viewBox="0 0 10.496 11.662">
                                                                <path id="Calendar-2" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"/>
                                                            </svg>
                                                        </span>
                                                        <span class="text-white" style="font-size:20px"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                                    </p>
                                                </div> 
                                                <div class="col-lg-6 text-right">
                                                    <div class="play-time py-1 px-2 mb-0 rounded-pill d-inline-block">
                                                        <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play-btn" class="mr-1">
                                                        <span class="text-white"><?php echo $value['video_duration']; ?> Min</span>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-4 text-center">
                                        <div class="col-4 col-lg col-md-4 col-sm-6 mb-4">
                                            <?php  if($value['pace_result'] == 0){ ?>
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                            <?php }else {
                                                 $v1_l = explode(".", $value['pace_result']);
                                                 $pace_result = ($v1_l[1] == 0)?$v1_l[0]:$value['pace_result'];
                                                ?>
                                            <div class="circle-a<?php echo $key; ?> position-relative" id="circle-a">
                                                 <!-- <strong></strong> -->
                                            </div>
                                            <span class="outof" style="top: 20px"><?php echo $pace_result; ?>/5</span>
                                             <?php } ?>
                                            <span class="d-block fz-16 text-dark mt-2">Pace</span>

                                        </div>
                                        <div class=" col-4 col-lg col-md-4 col-sm-6 mb-4">
                                            <?php if($value['energy_result'] == 0){ ?>
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                            <?php }else{ 
                                                $v2_l = explode(".", $value['energy_result']);
                                                 $energy_result = ($v2_l[1] == 0)?$v2_l[0]:$value['energy_result'];
                                                ?>
                                                <div class="circle-b<?php echo $key; ?> position-relative" id="circle-b">
                                            </div>
                                            <span class="outof" style="top: 20px"><?php echo $energy_result; ?>/5</span>
                                            
                                           <?php } ?>
                                            <span class="d-block fz-16 text-dark mt-2">Energy</span>
                                        </div>
                                        <div class="col-4 col-lg col-md-4 col-sm-6 mb-4">
                                            <?php if($value['fillerword_result'] == 0){ ?>
                                                 <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                          <?php }else{ 
                                                $v3_l = explode(".", $value['fillerword_result']);
                                                $fillerword_result = ($v3_l[1] == 0)?$v3_l[0]:$value['fillerword_result'];
                                            ?>
                                            <div class="circle-c<?php echo $key; ?> position-relative" id="circle-c">
                                            </div>
                                            <span class="outof" style="top: 20px"><?php echo $fillerword_result; ?>/5</span>
                                            <?php } ?>
                                            <span class="d-block fz-16 text-dark mt-2">Filler Words</span>
                                        </div>
                                        <div class="col-4 col-lg col-md-4 col-sm-6 mb-4">
                                            <?php if($value['conciseness_result'] == 0){ ?>
                                                 <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                           <?php }else{ 
                                            $v4_l = explode(".", $value['conciseness_result']);
                                                $conciseness_result = ($v4_l[1] == 0)?$v4_l[0]:$value['conciseness_result'];
                                            ?>
                                                <div class="circle-d<?php echo $key; ?> position-relative" id="circle-d">
                                            </div>
                                            <span class="outof" style="top: 20px"><?php echo $conciseness_result; ?>/5</span>
                                          <?php } ?>
                                            <span class="d-block fz-16 text-dark mt-2">Conciseness</span>
                                        </div>
                                        <div class="col-4 col-lg col-md-4 col-sm-6 mb-4">
                                            <?php if($value['eyecontact_result'] == 0){ ?>
                                                <div class="circles error">
                                                    <strong>!</strong>
                                                </div>
                                            <?php }else{ 
                                                  $v5_l = explode(".", $value['eyecontact_result']);
                                                $eyecontact_result = ($v5_l[1] == 0)?$v5_l[0]:$value['eyecontact_result'];
                                                ?>
                                                <div class="circle-f<?php echo $key; ?> position-relative" id="circle-f">
                                            </div>
                                            <span class="outof" style="top: 20px"><?php echo $eyecontact_result; ?>/5</span>
                                            <?php } ?>
                                            <span class="d-block fz-16 text-dark mt-2">Eye Contact</span>
                                        </div> 
                                    </div>
                                </a>
                            </div> 
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                            <div class="text-center mb-4" >
                                <img src="<?php echo base_url(); ?>website_assets/images/evaluation.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                <h5 class="mb-10">No Evaluations Yet</h5>
                                <!-- <p class="mb-10">Your speech upload failed.</p> -->
                                 <p><center>
                               <!--  If you want to evaluations this speeches then please click on the following link -->
                               Click on my speeches and upload new speech and evaluate
                            :</center></p>
                            <p><center>
                                <a href="<?php echo base_url() ?>my-speeches">My Speeches</a>
                            </center></p>

                            </div>
                        <?php } ?>
                        <!--end-row-->

                        <nav aria-label="Page navigation example">
                            <?php echo $this->pagination->create_links(); ?>
                        </nav>
                    </div>
                </section>
                <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>

        <script> 

            <?php foreach ($evaluations as $key => $value) {?>
           
            <?php if($value['pace_result'] == '1') { ?>
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['pace_result'] == '2') { ?> 
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['pace_result'] == '3') { ?>    
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['pace_result'] == '4') { ?> 
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['pace_result'] == '5') { ?> 
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                $value_datas = (($value['pace_result']*20)/100);
                ?> 
                 $('.circle-a<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value: <?php echo $value_datas; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>   

            <?php if($value['energy_result'] == '1') { ?>
                $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['energy_result'] == '2') { ?> 
                $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['energy_result'] == '3') { ?>    
                 $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['energy_result'] == '4') { ?> 
                $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['energy_result'] == '5') { ?> 
                $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                   size: 60,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                 $value_datas_ene = (($value['energy_result']*20)/100);
                ?> 
               $('.circle-b<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value: <?php echo $value_datas_ene; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>
            
            <?php if($value['fillerword_result'] == '1') { ?>
                $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['fillerword_result'] == '2') { ?> 
                $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['fillerword_result'] == '3') { ?>    
                 $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['fillerword_result'] == '4') { ?> 
                $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['fillerword_result'] == '5') { ?> 
                $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                   $value_datas_fill = (($value['fillerword_result']*20)/100);
                ?> 
               $('.circle-c<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value: <?php echo $value_datas_fill; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>

            <?php if($value['conciseness_result'] == '1') { ?>
                $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['conciseness_result'] == '2') { ?> 
                $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['conciseness_result'] == '3') { ?>    
                 $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['conciseness_result'] == '4') { ?> 
                $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['conciseness_result'] == '5') { ?> 
                $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{ 
                  $value_datas_con = (($value['conciseness_result']*20)/100);
                ?> 
               $('.circle-d<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value: <?php echo $value_datas_con; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } ?>

            <?php if($value['eyecontact_result'] == '1') { ?>
               
                $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.20,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['eyecontact_result'] == '2') { ?> 
                $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.40,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['eyecontact_result'] == '3') { ?>    
                 $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                    size: 60,
                    value:0.60,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['eyecontact_result'] == '4') { ?> 
                $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:0.80,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } else if($value['eyecontact_result'] == '5') { ?> 
                $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value:1,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php }else{
                  $value_datas_eye = (($value['eyecontact_result']*20)/100);
             ?> 
               $('.circle-f<?php echo $key; ?>').circleProgress({
                    startAngle: -1.55,
                     size: 60,
                    value: <?php echo $value_datas_eye; ?>,
                    fill: {
                        color: '#17966B'
                    }
                });
            <?php } }?>
        </script>
    </body>
</html>