<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/agora.css">
        <style type="text/css">
               .player {
              width: 100%;
              height: 400px;
              margin-left: 0px;
              margin-bottom: 10px;
            }
        </style>
        <!--start-body-->
        <section>
            <div class="container">
                <div class="contact-panel px-2 px-lg-4 py-4">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">My Contests</li>
                        </ol>
                    </nav>
                    <!--end-nav-->

                    <div class="container bootstrap snippets bootdey">
                        <div class="row">
                            <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                            <div class="profile-info col-lg-9">
                                <div class="shadow">
                                    <div class="bio-graph-info px-lg-4 px-3 py-3">
                                        <div class="">
                                            <div class="row">
                                                <input id="appid" type="hidden" placeholder="enter appid" value="<?php echo AGORA_APP_KEY; ?>">
                                                <input id="token" type="hidden" placeholder="enter token" value="<?php echo $token; ?>">
                                                <input id="channel" type="hidden" placeholder="enter channel name" value="<?php echo $channel; ?>">
                                                <div class="col-lg-12" id="images">
                                                    <div class="speech-img position-relative speech-box bg-overlay-blck">
                                                        <?php $file_ext = explode('.', $contest_details['contest_image']);
                                                        if ($contest_details['contest_type'] != 'live') {  ?>
                                                            <video src=" <?php echo $contest_details['contest_image']; ?> " class="w-100">
                                                            </video>
                                                        <?php } else { ?>
                                                            <img src="<?php echo $contest_details['contest_image']; ?>" alt="image1" class="w-100">
                                                        <?php } ?>
                                                        <div class="overlay position-absolute text-right">
                                                            <div class="btt-plays">
                                                                <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                    <span class="text-white"><?php echo ($contest_details['video_duration'] == '') ? 'Live' : $contest_details['video_duration']; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col-lg-12" id="Live_video">
                                                    <div class="speech-img position-relative speech-box bg-overlay-blck">
                                                  <div class="col">
                                                        <div id="remote-playerlist" class="w-100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="col-lg-12 col-md-0 content px-4 py-4 rounded">
                                                    <h4 class="mb-2"><?php echo $contest_details['name']; ?></h4>
                                                    <div class="align-items-center mb-4 row ">
                                                        <div class="col-lg-8">
                                                            <ul class="dots list-unstyled d-flex mb-1">
                                                                <li class="pr-3 fz-16 theme-color font-weight-bold"><?php echo $this->session->userdata('currency') . '' . round($this->session->userdata('currency_rate')*$contest_details['price'],2); ?></li>
                                                                <li class="pl-3 fz-16 theme-color"><?php echo ucfirst($contest_details['contest_type']); ?></li>
                                                            </ul>
                                                            <span class="text-gray">+ Applicable Taxes</span>
                                                        </div>
                                                        <div class="col-lg-4 text-right">
                                                            <span>Category : </span><span><?php echo $contest_details['age_category']; ?></span>
                                                        </div>
                                                    </div>
                                                    <p class="text-gray mb-4 fz-16"><?php echo $contest_details['description']; ?></p>
                                                </div>
                                            </div>
                                            <?php if (!empty($contest_details['prizes'])) { ?>
                                                <div class="pb-30">
                                                    <h5 class="mb-2">Prizes:</h5>
                                                    <p class="fz-16">Winners will be announced on <span class="text-dark"><?php echo $this->common_model->date_convert($contest_details['winner_date'], 'd M Y', $this->session->userdata('website_timezone')); ?></span></p>
                                                    <!--start-row-->
                                                    <div class="row pt-10">
                                                        <?php foreach ($contest_details['prizes'] as $key => $value) { ?>
                                                            <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                                                <div class="pric-main shadow text-center">
                                                                    <img src="<?php echo $value['prize_image']; ?>" alt="class5" width="100" height="100" class="mb-3">
                                                                    <h5 class="mb-3"><?php echo $value['prize_name']; ?></h5>
                                                                    <span class="d-block text-warning"><?php echo $this->session->userdata('currency') . '' . round($this->session->userdata('currency_rate')*$value['price'],2); ?></span>
                                                                    <span class="d-block text-warning">+ <?php echo $value['credit_points']; ?> points</span>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <!--end-row-->
                                                </div>
                                            <?php } ?>
                                            <!--start-video-->
                                            <?php if($contest_details['contest_type'] != 'live'){ ?>

                                         
                                            <div class="your-video">
                                                <h5>Uploaded Video</h5>
                                                <div class="video">
                                                    <video controls="" class="w-100 theme-rounded">
                                                        <source src="<?php echo $contest_details['video']; ?>" class="w-100">
                                                    </video>
                                                </div>
                                            </div>
                                            <?php   } ?>
                                            <!--end-video-->
                                            <div class="pt-30 pb-20">
                                                <?php if (!empty($contest_details['sponsors'])) { ?>
                                                    <h5 class="mb-4">Sponsor Details</h5>
                                                    <?php foreach ($contest_details['sponsors'] as $key => $value) { ?>
                                                        <div class="row align-items-center mb-3">
                                                            <div class="col-sm-2">
                                                                <img src="<?php echo $value['sponsor_logo']; ?>" alt="user" class="w-100 rounded">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <h5 class="mb-1"><?php echo $value['sponsor_name']; ?></h5>
                                                                <p class="mb-0 fz-16">Sponsor contest</p>
                                                            </div>
                                                        </div>
                                                        <p class="fz-16"><?php echo $value['bio']; ?></p>
                                                <?php }
                                                } ?>
                                                <!--table-->
                                                <div class="mt-10">
                                                    <h5>Details</h5>
                                                    <div class="pb-3">
                                                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Participant Id</span>
                                                            <span><?php echo $contest_details['id']; ?> </span>
                                                        </div>
                                                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Date</span>
                                                            <span><?php echo $this->common_model->date_convert($contest_details['contest_startdate'], 'd M Y', $this->session->userdata('website_timezone')); ?></span>
                                                        </div>
                                                        <!-- <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Credit points discount</span>
                                                            <span>- <?php echo $this->session->userdata('currency') . '' . $contest_details['discount']; ?></span>
                                                        </div> -->
                                                    </div>
                                                </div>

                                              <!--   <h6>People Joined Here</h6><br> -->
                                                <!-- <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="other-user-pic" id="remote-playerlist"></div>
                                                    </div>
                                                </div> -->
                                                <div class="row video-group">
                                                    <div class="col">
                                                        <div id="remote-playerlist"></div>
                                                    </div>
                                                </div>
                                                <!--table-->


                                                <!--table-->
                                                <div class="mt-10 mb-40">
                                                    <h5>Payment Summary</h5>
                                                    <div class="table-main pb-3">
                                                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Sub total</span>
                                                            <span><?php echo $this->session->userdata('currency') . '' . $contest_details['subtotal']; ?></span>
                                                        </div>
                                                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Tax.</span>
                                                            <span><?php echo $this->session->userdata('currency') . '' . $contest_details['tax']; ?></span>
                                                        </div>
                                                        <div class="d-flex fz-16 justify-content-between tabl-wrap mb-2">
                                                            <span>Credit points discount</span>
                                                            <span>- <?php echo $this->session->userdata('currency') . '' . $contest_details['discount']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex fz-16 justify-content-between tabl-wrap pt-3">
                                                        <span class="font-weight-bold text-dark">Total amount</span>
                                                        <span class="font-weight-bold text-dark"><?php echo $this->session->userdata('currency') . '' . $contest_details['totalamount']; ?></span>
                                                    </div>
                                                </div>
                                                <!--tabel-->

                                                <a href="<?php echo base_url(); ?>score-card-for-contest/<?php echo base64_encode($contest_details['id']); ?>" class="shadow px-3 py-3 scored-board d-flex justify-content-between rounded align-items-center">
                                                    <div>
                                                        <span class="mr-2">
                                                            <svg id="Group_41155" data-name="Group 41155" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                                                <path id="Path_208171" data-name="Path 208171" d="M0,0H24V24H0Z" fill="none"></path>
                                                                <path id="Path_208172" data-name="Path 208172" d="M21,2.992V21.008a1,1,0,0,1-.993.992H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,3.993,2H20.007A.993.993,0,0,1,21,2.992ZM7,4v9l3.5-2L14,13V4Z" fill="#17966b"></path>
                                                                <path id="Path_208173" data-name="Path 208173" d="M1.631-10.934a1.034,1.034,0,0,1,.728.269.881.881,0,0,1,.292.677.9.9,0,0,1-.292.681,1.024,1.024,0,0,1-.728.273A1.04,1.04,0,0,1,.9-9.307a.889.889,0,0,1-.3-.681.881.881,0,0,1,.292-.677A1.046,1.046,0,0,1,1.631-10.934ZM2.5-3.693H.75V-8.5H2.5Z" transform="translate(15 23.693)" fill="#fff"></path>
                                                            </svg>
                                                        </span>
                                                        <span>Scorecard for contest</span>
                                                    </div>
                                                    <div class="">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="5.877" height="9.753" viewBox="0 0 5.877 9.753">
                                                            <path id="Path_45623" data-name="Path 45623" d="M3.462,6.925,0,3.462,3.462,0" transform="translate(4.877 8.339) rotate(180)" fill="none" stroke="#1f1f1f" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </div>
                                            <!--start-label-->
                                            <div class="py-3 px-2 purchase-sec mb-4 d-flex align-items-center rounded">
                                                <span class="mr-2">
                                                    <svg id="_2933094" data-name="2933094" xmlns="http://www.w3.org/2000/svg" width="19.999" height="19.999" viewBox="0 0 19.999 19.999">
                                                        <path id="Path_209319" data-name="Path 209319" d="M271,274.957c.674-.165,1.172-.6,1.172-1.1s-.5-.932-1.172-1.1Z" transform="translate(-260.415 -262.108)" fill="#17966b"></path>
                                                        <path id="Path_209320" data-name="Path 209320" d="M97.445,91a6.445,6.445,0,1,0,6.445,6.445A6.452,6.452,0,0,0,97.445,91Zm2.93,8.2a2.549,2.549,0,0,1-2.344,2.3v.639a.586.586,0,1,1-1.172,0v-.639a2.549,2.549,0,0,1-2.344-2.3.586.586,0,1,1,1.172,0c0,.5.5.932,1.172,1.1V97.978a2.3,2.3,0,1,1,0-4.592v-.633a.586.586,0,0,1,1.172,0v.633a2.549,2.549,0,0,1,2.344,2.3.586.586,0,0,1-1.172,0c0-.5-.5-.932-1.172-1.1V96.9A2.549,2.549,0,0,1,100.375,99.2Z" transform="translate(-87.446 -87.446)" fill="#17966b"></path>
                                                        <path id="Path_209321" data-name="Path 209321" d="M212.172,182.761c-.674.165-1.172.6-1.172,1.1s.5.932,1.172,1.1Z" transform="translate(-202.758 -175.622)" fill="#17966b"></path>
                                                        <path id="Path_209322" data-name="Path 209322" d="M10,0A10,10,0,1,0,20,10,10.039,10.039,0,0,0,10,0Zm0,17.616A7.617,7.617,0,1,1,17.616,10,7.626,7.626,0,0,1,10,17.616Z" fill="#17966b"></path>
                                                    </svg>
                                                </span>
                                                <span>Participate in contest and get <?php echo $contest_details['total_point']; ?> points.</span>
                                            </div>
                                             <!-- <button onclick="join_now()" style="width:100%" class="btn btn__primary btn__rounded"> -->
                                                <a href="<?php echo base_url(); ?>joined-contest/<?php echo base64_encode($contest_details['id']); ?>" class="btn btn__primary btn__rounded d-flex"><span>Join Now</span></a>
                                        <!-- Join Now -->
                                    </button>
                                            <!--end-label-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end-body-->
        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div>
    <?php include(APPPATH . "views/website/inc/script.php"); ?>
</body>
<script src="https://download.agora.io/sdk/release/AgoraRTC_N.js"></script>
<!-- <script src="<?php echo site_url(); ?>assets/js/agora.js"></script> -->
<script type="text/javascript">

    var client = AgoraRTC.createClient({
        mode: "live",
        codec: "vp8"
    });
    var localTracks = {
        videoTrack: null,
        audioTrack: null
    }
    var localTrackState = {
            videoTrackMuted: false,
            audioTrackMuted: false
        };
    var remoteUsers = {};
    // Agora client options
    var options = {
        appid: '<?php echo AGORA_APP_KEY; ?>',
        channel: '<?php echo $channel; ?>',
        uid: 0,
        token: '<?php echo $token; ?>',
        role: "audience", // host or audience
        audienceLatency: 2
    };
    window.onload = function() { // create Agora client
    $("#images").show();
        $("#Live_video").hide();

        // alert("P1");
        // the demo can auto join channel with params in url
        $(() => {
            // alert("P2");
            var urlParams = new URL(location.href).searchParams;
            options.appid = "<?php echo AGORA_APP_KEY; ?>";
            options.channel = "<?php echo $channel; ?>";
            options.token = "<?php echo $token; ?>";
            if (options.appid && options.channel) {
                // alert("p5");
                $("#uid").val(options.uid);
                $("#appid").val(options.appid);
                $("#token").val(options.token);
                $("#channel").val(options.channel);

               
                // $("#join-form").submit();
            }
        })
    };
       function join_now(){
                 join();     
                }
    $("#host-join").click(function(e) {
        options.role = "host"
    })

    $("#lowLatency").click(function(e) {
        options.role = "audience"
        options.audienceLatency = 1
        $("#join-form").submit()
    })

    $("#ultraLowLatency").click(function(e) {
        options.role = "audience"
        options.audienceLatency = 2
        $("#join-form").submit()
    })

    $("#join-form").submit(async function(e) {
        // alert("P6");
        e.preventDefault();
        $("#audience-join").attr("disabled", true);
        try {
            // alert("P4");
            options.appid = $("#appid").val();
            options.token = $("#token").val();
            options.channel = $("#channel").val();
            options.uid = Number($("#uid").val());
            await join();
            if (options.role === "host") {
                $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                if (options.token) {
                    $("#success-alert-with-token").css("display", "block");
                } else {
                    $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                    $("#success-alert").css("display", "block");
                }
            }
        } catch (error) {
            console.error(error);
        } finally {
            $("#leave").attr("disabled", false);
        }
    })

    $("#leave").click(function(e) {
        leave();
    })

    async function join() {
        // create Agora client
        // alert("P3");
        // alert(options.role);
        console.log(options);
        if (options.role === "audience") {
            client.setClientRole(options.role, {
                level: options.audienceLatency
            });
            // alert("ABC");
            // alert(options.audienceLatency);
            // add event listener to play remote tracks when remote user publishs.
            client.on("user-published", handleUserPublished);
            client.on("user-unpublished", handleUserUnpublished);
        } else {
            client.setClientRole(options.role);
            client.on("user-published", handleUserPublished);
            client.on("user-unpublished", handleUserUnpublished);
        }

        // console.log(options);
        // console.log("HBNABC_____________________");
        // join the channel
        options.uid = await client.join(options.appid, options.channel, options.token || null, options.uid || null);

        // options.uid = options.uid;
        // console.log(options);
        // console.log("HBNABC_____________________");
        subscribe(options, 'video');
        if (options.role === "host") {
            // create local audio and video tracks
            localTracks.audioTrack = await AgoraRTC.createMicrophoneAudioTrack();
            localTracks.videoTrack = await AgoraRTC.createCameraVideoTrack();
            // play local video track
            localTracks.videoTrack.play("local-player");
            $("#local-player-name").text(`localTrack(${options.uid})`);
            // publish local tracks to channel
            await client.publish(Object.values(localTracks));
            console.log("publish success");
        }
    }

    async function leave() {
        for (trackName in localTracks) {
            var track = localTracks[trackName];
            if (track) {
                track.stop();
                track.close();
                localTracks[trackName] = undefined;
            }
        }

        // remove remote users and player views
        remoteUsers = {};
        $("#remote-playerlist").html("");

        // leave the channel
        await client.leave();

        $("#local-player-name").text("");
        // $("#host-join").attr("disabled", false);
        $("#audience-join").attr("disabled", false);
        // $("#leave").attr("disabled", true);
        console.log("client leaves channel success");
    }

    async function subscribe(user, mediaType) {
        console.log(user);
        console.log(mediaType);
        console.log("SUBSCRIB");
        const uid = user.uid;
        // subscribe to a remote user
        await client.subscribe(user, mediaType);
        console.log("subscribe success");
        if (mediaType === 'video') {
            const player = $(`
      <div id="player-wrapper-${uid}">
        <div id="player-${uid}" class="player" ></div>
      </div>
    `);
            $("#remote-playerlist").append(player);
            user.videoTrack.play(`player-${uid}`);
        }
        if (mediaType === 'audio') {
            user.audioTrack.play();
        }
    }

    function handleUserPublished(user, mediaType) {
        // alert("P6")
           $("#images").hide();
        $("#Live_video").show();
        //print in the console log for debugging 
        console.log('"user-published" event for remote users is triggered.');
        console.log(user);
        console.log(mediaType);
        console.log("HANDLE USER handleUserPublished");
        const id = user.uid;
        remoteUsers[id] = user;
        subscribe(user, mediaType);
    }

    function handleUserUnpublished(user, mediaType) {
        // alert("P7")
        //print in the console log for debugging 
        $("#images").show();
        $("#Live_video").hide();
        console.log('"user-unpublished" event for remote users is triggered.');
        console.log(user);
        console.log(mediaType);
        console.log("HANDLE USER handleUserUnpublished");
        if (mediaType === 'video') {
            const id = user.uid;
            delete remoteUsers[id];
            $(`#player-wrapper-${id}`).remove();
        }
    }
</script>

</html>