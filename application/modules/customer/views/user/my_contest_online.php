<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style>
    /* new css */

    .currancy_block {
        display: inline-block;
        background: #15976b;
        border-radius: 50%;
        width: 24px;
        height: 24px;
        line-height: 25px;
        padding: 2px 2px;
        text-align:center;
    }
    .currency_part {
        color: #ffffff;
        font-size: 12px;
        border: 1px solid #fff;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        line-height: 20px;
    }
</style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Contests</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">My Contests <span>(<?php echo count($contest_list); ?>)</span></h4> 
                                            </div>
                                            <!--end-title-->
                                            <?php if(!empty($contest_list)){ ?>
                                            <div class="row">
                                                <?php foreach ($contest_list as $key => $value) {
                                                    $file_ext = explode('.', $value['contest_image']); ?>
                                                    <div class="col-md-12 mb-4">
                                                        <a href="<?php echo base_url(); ?>my-contest-detail/<?php echo base64_encode($value['contest_id']); ?>" class="speech-box img-height">
                                                            <div class="speech-img position-relative bg-overlay-blck">
                                                                <?php if($value['contest_type'] != 'live'){  ?>
                                                                    <video src=" <?php echo $value['contest_image']; ?> " class="w-100"> 
                                                                    </video>
                                                                <?php }else { ?> 
                                                                    <img src="<?php echo $value['contest_image']; ?>" alt="image1" class="w-100">
                                                                <?php } ?>
                                                               
                                                                <div class="overlay position-absolute w-100">
                                                                    <div class="row align-items-end">
                                                                        <div class="col-lg-7 mb-lg-0 mb-2 text-left">
                                                                            <div class="btt-plays">
                                                                                <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6>
                                                                                <?php if($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])){ ?>
                                                                                <p class="mb-0">
                                                                                    <span class="mr-2 user-img">
                                                                                        <img src="<?php echo $value['sponsors']['sponsor_logo']; ?>" alt="Rectangle -6">
                                                                                    </span>
                                                                                    <span class="text-white">Sponsor By <?php echo $value['sponsors']['sponsor_name']; ?></span>
                                                                                </p>
                                                                                <?php } ?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-5 text-lg-right">
                                                                            <div class="price d-block"><span class="d-block text-white mb-2 fz-16"><?php echo ucfirst($value['contest_type']); ?></span>
                                                                                <span class="text-white font-weight-bold">
                                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                        <span class="text-white"><?php echo ($value['video_duration'] == '')?'Live':$value['video_duration']; ?></span>
                                                                                     </div>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--start-price-tage-->
                                                            <div class="pric-tag-tp">
                                                                <div class="btt-plays">
                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                        <div class="currancy_block">
                                                                              <div class="currency_part"><?php echo $this->session->userdata('currency'); ?></div>
                                                                            </div>
                                                                        <span class="text-white"><?php echo round(($this->session->userdata('currency_rate')*$value['price']),2); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end-price-tage-->
                                                        </a>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php } else{ ?>
                                                <h5><center>No contest found</center></h5>
                                            <?php } ?>
                                            <!--btn-->
                                       <!--      <a href="#" class="btn btn__primary btn__outlined btn__rounded  fz-16 mt-3" data-toggle="modal" data-target="#form">+ Add More</a> -->
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>