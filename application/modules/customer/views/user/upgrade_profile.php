<!DOCTYPE html>
<html lang="en">
    <head>
       <?php  include(APPPATH."views/website/inc/style.php");?>
       <style type="text/css">
        .crn-numbers .select2-container {
    width: 25% !important;
}
.crn-numbers .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 15%;
}

.crn-numbers .select2-container--default .select2-selection--single {
    border-radius: 30px 0 0 30px;
    border-right: none !important;
    border: 2px solid #e6e8eb;
}

.select2-container--open .select2-dropdown {
    left: 0;
    width: 400px !important;
}

.crn-numbers1 .select2-container .select2-selection--single {
    height: 60px;
}

.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 60px;
}
.crn-numbers1 .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px;
}

.crn-numbers1 .select2-container .select2-selection--single .select2-selection__rendered {
    padding-left: 10%;
}

.crn-numbers1 .select2-container--default .select2-selection--single {
    border-radius: 30px 30px 30px 30px;
    /*border-right: none !important;*/
    border: 2px solid #e6e8eb;
}
       </style>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header1.php");?>
    
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Upgrade To Main Profile</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-0 pb-20 position-relative">Upgrade To Main Profile</h4>
                                                <p class="mt-0 mb-4 pt-20 fz-16">If you are upgrade to main profile then student will remove from your studentd list. Add Followning details to upgrade to main profile </p>
                                            </div>
                                            <!--end-title-->
                                            <?php echo form_open('upgrade-profile/'.base64_encode($student_id), array('method' => 'post', 'enctype'=>'multipart/form-data', 'class'=>'my-5')); ?>
                                            
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="icon-email form-group-icon text-success"></i>
                                                    <input 
                                                        type="email" 
                                                        class="form-control" 
                                                        id="email" 
                                                        name="email" 
                                                        placeholder="Email" 
                                                        aria-required="true" 
                                                        parsley-trigger="change" 
                                                        data-parsley-required-message="Please enter email" 
                                                        required 
                                                        data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  
                                                        data-parsley-pattern-message="Please enter valid email address" 
                                                        data-parsley-maxlength="64"
                                                        value="<?php echo !empty(set_value('email')) ? set_value('email') : ''; ?>"
                                                    >
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                    <!-- <input type="email" class="form-control" placeholder="Email Address/Mobile Number" required> -->
                                    <i class="icon-phone form-group-icon text-success"></i>
                                    <div class="input-group mb-3 mb-md-4 mb-lg-3 d-flex crn-numbers">
                                        <select class="form-select form-control imageselect2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countrieseerror" required style="max-width:60px;min-width:140px;padding-left:40px">
                                            <!-- <option value="+1">+1</option> -->
                                            <?php $country_codes = !empty(set_value('country_code')) ? set_value('country_code') : ''; ?>
                                            <?php if (!empty($countries)) {
                                                foreach ($countries as $key => $value) { ?>
                                                    <!--  <option value="<?php echo $value['calling_code']; ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['calling_code']; ?></option> -->
                                                    <option value="<?php echo $value['calling_code']; ?>" data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>" <?php echo ($country_codes == $value['calling_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'] . '(' . $value['calling_code'] . ')'; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                        <?php echo form_error('country_code'); ?>
                                        <label id="countrieseerror"></label>

                                        <input type="text" class="form-control phone-format" id="phone" name="phone" aria-required="true" parsley-trigger="change" placeholder="Phone Number" data-parsley-errors-container="#phoneerror" data-parsley-required-message="Please enter phone number" required maxlength="12" data-parsley-minlength="8" data-parsley-minlength-message="Please enter valid phone number min 8 digits or max 12" value="<?php echo set_value('phone'); ?>">
                                        <?php echo form_error('phone'); ?>
                                    </div>
                                    <div id="phoneerror"></div>
                                </div>
                                            <!-- <div class="">
                                                <div class="form-group">
                                                    <i class="icon-location text-success form-group-icon"></i>
                                                    <select 
                                                        class="form-control" 
                                                        id="country" 
                                                        name="country" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-errors-container="#code_errror"  
                                                        data-parsley-required-message="Please select country" 
                                                        required
                                                    >

                                                        <option value="">Select Country </option>
                                                        <?php if(!empty($countries)){   
                                                            foreach ($countries as $key => $value){ ?>
                                                                <option value="<?php echo $value['name']; ?>" <?php echo set_select('country',$value['name']) ?>> <?php echo $value['name']; ?> </option>
                                                        <?php } } ?>

                                                    </select>
                                                    <?php echo form_error('country'); ?>
                                                    <div id="code_errror" style="margin-top: -5px !important"></div>
                                                </div>
                                            </div> -->
                                             <div class="crn-numbers1">
                                                <div class="form-group">
                                                    <i class="icon-location text-success form-group-icon"></i>
                                                   
                                                    <!-- <select 
                                                        class="form-control" 
                                                        id="country" 
                                                        name="country" 
                                                        data-parsley-trigger="change" 
                                                        data-parsley-errors-container="#code_errror"  
                                                        data-parsley-required-message="Please select country" 
                                                        required
                                                    >
                                                        <?php $country = !empty(set_value('country')) ? set_value('country') : $result['country']; ?>
                                                        <option value="">Select Country </option>
                                                        <?php if(!empty($countries)){   
                                                            foreach ($countries as $key => $value){ ?>
                                                                <option value="<?php echo $value['name']; ?>" <?php echo ($country==$value['name']) ? 'selected="selected"' : ''; ?>> <?php echo $value['name']; ?> </option>
                                                        <?php } } ?>

                                                    </select> -->
                                                     <select  
                                                    class="form-control imageselect3" 
                                                    id="country" 
                                                    name="country" 
                                                    data-parsley-trigger="change" 
                                                    data-parsley-errors-container="#code_errror"  
                                                    data-parsley-required-message="Please select country" 
                                                    required
                                                    onchange="region_list(this.value);"
                                                >
                                                  <?php $country = !empty(set_value('country')) ? set_value('country') : ''; ?>
                                                    <option value="">Select Country </option>
                                                    <?php if(!empty($countries)){   
                                                        foreach ($countries as $key => $value){ ?>
                                                            <option data-profileimage="<?php echo base_url() . 'website_assets/country_flag/' . $value['flag'] ?>"  value="<?php echo $value['name']; ?>" <?php echo ($country==$value['name']) ? 'selected="selected"' : ''; ?>> <?php echo $value['name']; ?> </option>
                                                    <?php } } ?>

                                                </select>
                                                    <?php echo form_error('country'); ?>
                                                    <div id="code_errror" style="margin-top: -5px !important"></div>
                                                </div>
                                            </div>
                                                     <div class="crn-numbers1">
                                            <div class="form-group">
                                                  <i class="icon-location text-success form-group-icon"></i>
                                                    <select name="region_id" id="region_id" data-parsley-errors-container="#errormsg_region_list" class="form-control select2" data-parsley-trigger="change" required data-parsley-required-message="Please select region name">
                                                        <option value="">Region List</option>
                                                    </select>
                                                    <?php echo form_error('errormsg_region_list'); ?>
                                                    <label id="errormsg_region_list"></label>
                                                </div>
                                            </div>
                                            <div class="">
                                                <div class="form-group w-100 d-inline-block">
                                                    <i class="form-group-icon icon-widget text-success"></i>
                                                    <input 
                                                        type="text" 
                                                        class="form-control" 
                                                        id="Promo-Code" 
                                                        name="promocode"
                                                        placeholder="Link a Promo Code" 
                                                        aria-required="true"
                                                        value="<?php echo !empty(set_value('promocode')) ? set_value('promocode') : ''; ?>"
                                                    >
                                                </div>
                                            </div> 
                                            <div class="">
                                                <div class="form-group">
                                                    <i class="form-group-icon text-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi mb-10 bi-lock-fill" viewBox="0 0 16 16">
                                                            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"/>
                                                        </svg>
                                                    </i>
                                                    <input 
                                                        type="password" 
                                                        class="form-control"
                                                        id="password" 
                                                        name="password" 
                                                        placeholder="Password" 
                                                        aria-required="true"
                                                        data-parsley-trigger="change"   
                                                        data-parsley-required-message="Please enter password" 
                                                        required
                                                        data-parsley-errors-container="#passwordd" 
                                                        data-parsley-pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&£€^(){}:'|<>]).*"   data-parsley-pattern-message="Please enter atleast 4 characters length with 1 upper case, 1 lower case, 1 number and 1 special character" 
                                                        required 
                                                        data-parsley-maxlength="64" 
                                                        data-parsley-minlength="4"
                                                        data-toggle="tooltip" 
                                                        data-placement="top" 
                                                    >
                                                    <span toggle="#password" class="fa fa-eye fa-eye-slash field-icon toggle-password alrt-div" style="top: 44% !important;right: 19px !important;"></span>
                                                </div>
                                            </div>
                                          
                                            <!--btn-->
                                            <button type="submit" class="btn btn__primary btn__outlined btn__rounded  fz-16 mt-3 mr-4" style="color: white;background-color: #17966B;">Upgrade</button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
    
            <!--start-modal-->
            <div class="modal fade" id="alert-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                            <div class="content">
                                <h4>Why DOB Is Required</h4>
                                <p class="fz-16 text-dark">Classes and contests are categorized into 4 age groups. Primary, Junior, Teens and Adults.</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <button type="button" class="btn btn__primary btn__rounded" data-dismiss="modal" aria-label="Close">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->

            <!--start-modal-->
            <div class="modal fade" id="teens-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                            <div class="content">
                                <h4>Age Group</h4>
                                <p class="fz-16 text-dark">Student will be placed in the <b id="category_student">Junior</b> Category.</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <button type="button" class="btn btn__primary btn__rounded" data-dismiss="modal" aria-label="Close">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->

            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
           <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('select').niceSelect('destroy');
                $('.select2').select2({"width":"100%"}); 
                  $("#country_code").select2({
                "width": "100%",
            });
            $(".imageselect2").select2({
      "width":"100%",
      templateResult: UserTemplate,
      templateSelection: UserTemplate
    });
                $("#country").select2({
                "width": "100%",
            }); 
                             $(".imageselect3").select2({
                "width": "100%",
                templateResult: UserTemplate,
                templateSelection: UserTemplate
            }); 
            });
 function UserTemplate (user) {
    if (!user.id) {
      return user.text;
    }
    var $user = $(
      '<span><img src="'+$(user.element).data('profileimage')+'" width="20px" height="20px" class="img-flag"/> '+user.text+'</span>'
    );
    return $user;
  };
            $(".toggle-password").click(function() {
                $(this).toggleClass("fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
             /***phone number format***/
            $(".phone-format").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                var curchr = this.value.length;
                var curval = $(this).val();
                if (curchr == 3 && curval.indexOf("(") <= -1) {
                    $(this).val("" + curval + "" + "-");
                } else if (curchr == 3 && curval.indexOf("(") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 3 && curval.indexOf(")") > -1) {
                    $(this).val(curval + "-");
                } else if (curchr == 7) {
                    $(this).val(curval + "-");
                    $(this).attr('maxlength', '14');
                }
            });


            function region_list(country_id) {

            $.ajax({
                url: "<?php echo base_url() . 'website/region_list/'; ?>" + country_id,
                type: 'GET',
                dataType: 'json',
                error: function(err) {
                    $('#region_id').html('<option value="">Select Region</option>');
                },
                success: function(response) {
                    if (response == "") {
                        $('#region_id').html('<option value="">Select Region List</option>');
                    } else {
                        var statehtml = "";
                        var statehtml = "<option value=''>Select Region List</option>";
                        $.each(response, function(index, value) {
                            var slectedstr = '';
                            if (value.id == "<?php echo (!empty(set_value('region_id')) ? set_value('region_id') : ''); ?>") {
                                slectedstr = "selected='selected'";
                            }
                            statehtml += "<option value='" + value.id + "' " + slectedstr + ">" + value.region + "</option>";
                        });
                        $('#region_id').html(statehtml);
                    }
                }
            });
        }


        </script>
    </body>
</html>