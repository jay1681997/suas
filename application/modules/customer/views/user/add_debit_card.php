<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include(APPPATH."views/website/inc/style.php");?>
        <style type="text/css">
            .parsley-required{
                margin-bottom: 35px;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section class="">
                    <div class="container">
                        <div class="contact-panel otp-verif px-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Card</li>
                                </ol>
                            </nav>
                            <!--end-nav--> 
                            <div class="text-center my-5">
                                <img src="<?php echo base_url(); ?>website_assets/images/card/undraw_credit_card.png" alt="undraw_credit_card">
                            </div>
                            <!--title-->
                            <div class="title text-center">
                                <h4 class="line mb-0 pb-20 position-relative">Add Credit/Debit Card</h4>
                                <p class="mt-0 mb-4 pt-20 fz-16">Fill out the details below.</p>
                            </div>
                            <!--end-title-->
                            <?php if($this->session->flashdata('success_msg')){ ?>
                            <div class="alert alert-success" >
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success_msg')?>
                            </div>                
                        <?php } ?>
                        <?php if($this->session->flashdata('error_msg')){ ?>
                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error_msg')?>
                            </div>
                        <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $error_msg; ?>
                            </div>
                        <?php } ?>
                            <!--start-form-->
                            <div class="add-card">
                                <?php echo form_open('add-debit-card/'.$item_type, array('method' => 'post','id'=>'cardform')); ?>
                                <div class="">
                                    <div class="form-group">
                                        <i class="form-group-icon">
                                            <svg id="Iconly_Bold_Message" data-name="Iconly/Bold/Message" xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                                <g id="Message" transform="translate(0 0)">
                                                    <path id="Message-2" data-name="Message" d="M18.519,1.481A5.057,5.057,0,0,0,14.939,0H5.06A5.05,5.05,0,0,0,0,5.05v7.9A5.057,5.057,0,0,0,5.06,18h9.879A5.058,5.058,0,0,0,20,12.95V5.05a5.037,5.037,0,0,0-1.481-3.569" fill="#17966b"/>
                                                </g>
                                                <path id="Path_208128" data-name="Path 208128" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(3 3)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-width="1" opacity="0.7"/>
                                                <path id="Path_208127" data-name="Path 208127" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(6 3)" fill="#fff"/>
                                                <path id="Path_208126" data-name="Path 208126" d="M0,0H10" transform="translate(3.5 11.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                                                <path id="Path_208125" data-name="Path 208125" d="M0,0H5" transform="translate(3.5 14)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                                            </svg>
                                        </i>
                                        <input 
                                            type="text" 
                                            class="form-control" 
                                            id="card_holder_name"
                                            name="card_holder_name"
                                            placeholder="Card Holder Name" 
                                            aria-required="true"
                                            parsley-trigger="change" 
                                            data-parsley-required-message="Please enter card holder name" 
                                            required 
                                            data-parsley-pattern="^[a-zA-Z ]+$" 
                                            data-parsley-pattern-message="Please enter valid card holder name" data-parsley-maxlength="32" 
                                            value="<?php echo set_value('card_holder_name') ?>"
                                        >
                                        <?php echo form_error('card_holder_name'); ?>
                                    </div>    
                                </div>
                                <div class="">
                                    <div class="form-group">
                                        <i class="form-group-icon">
                                            <svg id="Iconly_Bold_Message" data-name="Iconly/Bold/Message" xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                                <g id="Message" transform="translate(0 0)">
                                                    <path id="Message-2" data-name="Message" d="M18.519,1.481A5.057,5.057,0,0,0,14.939,0H5.06A5.05,5.05,0,0,0,0,5.05v7.9A5.057,5.057,0,0,0,5.06,18h9.879A5.058,5.058,0,0,0,20,12.95V5.05a5.037,5.037,0,0,0-1.481-3.569" fill="#17966b"/>
                                                </g>
                                                <path id="Path_208128" data-name="Path 208128" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(3 3)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-width="1" opacity="0.7"/>
                                                <path id="Path_208127" data-name="Path 208127" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(6 3)" fill="#fff"/>
                                                <path id="Path_208126" data-name="Path 208126" d="M0,0H10" transform="translate(3.5 11.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                                                <path id="Path_208125" data-name="Path 208125" d="M0,0H5" transform="translate(3.5 14)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                                            </svg>
                                        </i>
                                        <input 
                                            type="text" 
                                            class="form-control" 
                                            id="card_number"
                                            name="card_number"
                                            placeholder="Card Number" 
                                            aria-required="true"
                                            parsley-trigger="change" 
                                            data-parsley-required-message="Please enter card number" 
                                            required 
                                            data-parsley-minlength="12" 
                                            data-parsley-minlength-message="Please enter valid card number min 12 digits or max 16"
                                            maxlength="16"
                                            onkeypress="return isNumberKey(event);"
                                            value="<?php echo set_value('card_number') ?>"
                                        >
                                        <?php echo form_error('card_number'); ?>
                                    </div>
                                </div>
                                <div class="">
                                    
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="">
                                                                <div class="form-group form-group-date">
                                                                    <i class="form-group-icon">
                                                                        <svg id="Iconly_Bold_Message" data-name="Iconly/Bold/Message" xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                                                            <g id="Message" transform="translate(0 0)">
                                                                                <path id="Message-2" data-name="Message" d="M18.519,1.481A5.057,5.057,0,0,0,14.939,0H5.06A5.05,5.05,0,0,0,0,5.05v7.9A5.057,5.057,0,0,0,5.06,18h9.879A5.058,5.058,0,0,0,20,12.95V5.05a5.037,5.037,0,0,0-1.481-3.569" fill="#17966b"></path>
                                                                            </g>
                                                                            <path id="Path_208128" data-name="Path 208128" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(3 3)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-width="1" opacity="0.7"></path>
                                                                            <path id="Path_208127" data-name="Path 208127" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(6 3)" fill="#fff"></path>
                                                                            <path id="Path_208129" data-name="Path 208129" d="M.192-1.4A1.6,1.6,0,0,1,.528-2.444a1.065,1.065,0,0,1,.864-.412,1.059,1.059,0,0,1,.86.406A1.6,1.6,0,0,1,2.584-1.4a1.6,1.6,0,0,1-.336,1.05,1.067,1.067,0,0,1-.864.41,1.06,1.06,0,0,1-.862-.4A1.6,1.6,0,0,1,.192-1.4ZM.808-1.4a1.2,1.2,0,0,0,.16.658.485.485,0,0,0,.424.25A.478.478,0,0,0,1.81-.744,1.2,1.2,0,0,0,1.968-1.4a1.207,1.207,0,0,0-.16-.656A.484.484,0,0,0,1.384-2.3a.478.478,0,0,0-.418.248A1.2,1.2,0,0,0,.808-1.4Zm2.136-.1-.008,0v-.644l1.224-.7.068.02V0h-.62V-1.892ZM5.016.52H4.56l1.4-3.48H6.42ZM7.6-.536l1.14-.008L8.54,0H6.716l-.04-.276.98-1.1a2.054,2.054,0,0,0,.242-.33.514.514,0,0,0,.062-.242.312.312,0,0,0-.1-.236.344.344,0,0,0-.248-.092.73.73,0,0,0-.39.128,2.065,2.065,0,0,0-.434.4l-.008,0L6.724-2.4a1.39,1.39,0,0,1,.45-.332A1.276,1.276,0,0,1,7.7-2.844a.9.9,0,0,1,.638.222.764.764,0,0,1,.238.586.964.964,0,0,1-.1.426,2.349,2.349,0,0,1-.372.5ZM9-1.5,9-1.508v-.644l1.224-.7.068.02V0h-.62V-1.892Z" transform="translate(2.808 14)" fill="#fff"></path>
                                                                        </svg>
                                                                    </i>
                                                                    <input 
                                                                        type="month"
                                                                        class="form-control" 
                                                                        id="expiry_date" 
                                                                        name="expiry_date"
                                                                        placeholder="MM/YYYY" 
                                                                        aria-required="true"  
                                                                        data-parsley-required-message="Please select expiry date"required 
                                                                        data-parsley-minlength="4" 
                                                                        data-parsley-maxlength="64"
                                                                        value="<?php echo set_value('expiry_date') ?>"
                                                                    >
                                                                </div>
                                                            </div>
                                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <i class="form-group-icon   ">
                                                    <svg id="Iconly_Bold_Message" data-name="Iconly/Bold/Message" xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                                                        <g id="Message" transform="translate(0 0)">
                                                            <path id="Message-2" data-name="Message" d="M18.519,1.481A5.057,5.057,0,0,0,14.939,0H5.06A5.05,5.05,0,0,0,0,5.05v7.9A5.057,5.057,0,0,0,5.06,18h9.879A5.058,5.058,0,0,0,20,12.95V5.05a5.037,5.037,0,0,0-1.481-3.569" fill="#17966b"/>
                                                        </g>
                                                        <path id="Path_208128" data-name="Path 208128" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(3 3)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-width="1" opacity="0.7"/>
                                                        <path id="Path_208127" data-name="Path 208127" d="M2.5,0A2.5,2.5,0,1,1,0,2.5,2.5,2.5,0,0,1,2.5,0Z" transform="translate(6 3)" fill="#fff"/>
                                                        <path id="Path_208130" data-name="Path 208130" d="M.625,0A.625.625,0,1,1,0,.625.625.625,0,0,1,.625,0Z" transform="translate(3 11)" fill="#fff"/>
                                                        <path id="Path_208131" data-name="Path 208131" d="M.625,0A.625.625,0,1,1,0,.625.625.625,0,0,1,.625,0Z" transform="translate(5 11)" fill="#fff"/>
                                                        <path id="Path_208132" data-name="Path 208132" d="M.625,0A.625.625,0,1,1,0,.625.625.625,0,0,1,.625,0Z" transform="translate(7 11)" fill="#fff"/>
                                                        <path id="Path_208125" data-name="Path 208125" d="M0,0H5" transform="translate(3.5 14)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="1"/>
                                                    </svg>
                                                </i>
                                                <input 
                                                    type="text" 
                                                    class="form-control" 
                                                    id="security_code"
                                                    name="security_code"
                                                    placeholder="Security Code" 
                                                    aria-required="true"
                                                    parsley-trigger="change" 
                                                    data-parsley-required-message="Please enter security code" 
                                                    required 
                                                    data-parsley-minlength="3" 
                                                    data-parsley-minlength-message="Please enter valid security code min 3 digits or max 4"
                                                    maxlength="4"
                                                    onkeypress="return isNumberKey(event);"
                                                    value="<?php echo set_value('security_code') ?>"
                                                >
                                                <?php echo form_error('security_code'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="d-flex justify-content-center">
                                        <button type="submit" class="btn btn__primary btn__rounded" id="add_cardmethod"><span>Save</span></button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                            <!--end-form-->
                        </div>
                    </div>
                </section>
                <!--end-body--> 

                <!--start-modal-->
                <div class="modal fade" id="card-is-saved"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/Group -94.png" alt="Group -94" class="mb-25">
                                <div class="content">
                                    <h4>Credit/Debit Card</h4>
                                    <p class="fz-16">Your payment method has
                                    been saved</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0 justify-content-center">
                                <a href="<?php echo base_url(); ?>cart/<?php echo $item_type; ?>" class="btn btn__primary btn__rounded">Continue</a>
                            </div>
                        </div>
                    </div> 
                </div>
                <!--end-modal-->
            <?php include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">
$(document).ready(function(){
                var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();
if (mm < 10) {
   mm = '0' + mm;
} 
today = mm + ',' + yyyy;
            });

            <?php if($this->session->flashdata('success_msg')){ ?>
               
                $('#card-is-saved').modal('show');
            <?php } ?>
             
            // $('#add_cardmethod').click(function(){
              
            //     var formdata = { };
            //     $.each($('#cardform').serializeArray(), function() {
            //         console.log(this)
            //         console.log(this.value)
            //         formdata[this.name] = this.value;
            //     });
                
            //     var card_holder_name = formdata['card_holder_name'];
            //     var card_number = formdata['card_number'];
            //     var expiry_date = formdata['expiry_date'];
            //     var security_code = formdata['security_code']; 
            //     console.log(formdata)
            //     $.ajax({
            //         url:"<?php echo base_url().'customer/user/add_debit_card'; ?>",
            //         type:'POST',
            //         dataType: 'json',
            //         data:formdata,
            //         error:function(err){
            //             console.log(err)
            //         },
            //         success:function(data){
            //             console.log(data)
            //             $('#card-is-saved').modal('show');
            //             window.location.href= "<?php echo base_url().'add-debit-card'; ?>";
            //         }
            //     });
                
            // });
        </script>
    </body>
</html>