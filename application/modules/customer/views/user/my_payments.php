<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My profile</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-4 py-4 bio-graph-info">
                                            <div class="text-center my-5">
                                                <img src="<?php echo base_url(); ?>website_assets/images/card/undraw_credit_card.png" alt="undraw_credit_card">
                                            </div>

                                            <div class="title text-center">
                                                <h4 class="line mb-0 pb-20 position-relative">My Payments</h4>
                                                <p class="mt-0 mb-4 pt-20 fz-16">Update your payment preferences below</p>
                                            </div>

                                            <div class="credit-card-sec">
                                                <?php if(!empty($payment_methods)) { ?>
                                                <div class="card-main row">
                                                    <?php foreach ($payment_methods as $key => $value) { 
                                                      ?>
                                                    <div class="col-md-6 mb-lg-3 mb-3">
                                                        <a href="javascript:void(0)" class="card d-flex justify-content-between position-relative pt-4 px-3 romove_modal" data-toggle="modal" data-target="#delete_payment" data-id="<?php echo $value['id']; ?>">
                                                            <div class="d-flex justify-content-between">
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="27.536" viewBox="0 0 33.588 27.536">
                                                                        <path id="Path_45624" data-name="Path 45624" d="M58.01,106.055H39.923a4.729,4.729,0,0,0-4.724,4.724v24.139a4.73,4.73,0,0,0,4.724,4.724H58.01a4.73,4.73,0,0,0,4.724-4.724V110.779A4.729,4.729,0,0,0,58.01,106.055Zm-21.433,4.724a3.349,3.349,0,0,1,3.346-3.345h4.2v2.525a.689.689,0,1,0,1.379,0v-2.525h6.923v8.377a1.154,1.154,0,0,1-.049.332c-.006.017-.012.033-.017.051a1.155,1.155,0,0,1-1.088.77H46.658a1.155,1.155,0,0,1-1.088-.77c-.005-.017-.01-.034-.017-.051a1.142,1.142,0,0,1-.049-.332v-2.633a.689.689,0,1,0-1.379,0v2.516H43.81a6.565,6.565,0,0,0-5.988,3.886.291.291,0,0,1-.266.171h-.979Zm7.549,27.485h-4.2a3.349,3.349,0,0,1-3.346-3.346v-8.97h.979a.291.291,0,0,1,.266.171A6.566,6.566,0,0,0,43.81,130h.316Zm17.229-3.346a3.349,3.349,0,0,1-3.346,3.346h-4.2v-2.772a.689.689,0,0,0-1.379,0v2.772H45.5v-8.377a1.154,1.154,0,0,1,.049-.332c.006-.017.012-.034.017-.051a1.155,1.155,0,0,1,1.087-.77h4.617a1.155,1.155,0,0,1,1.088.77c.005.017.01.034.017.051a1.154,1.154,0,0,1,.048.332v2.386a.689.689,0,0,0,1.379,0V130h.316a6.565,6.565,0,0,0,5.988-3.886.291.291,0,0,1,.266-.171h.979Zm0-10.349h-.979a1.671,1.671,0,0,0-1.525.987,5.186,5.186,0,0,1-4.729,3.07H53.47a2.534,2.534,0,0,0-2.2-1.271H46.658a2.533,2.533,0,0,0-2.195,1.271H43.81a5.185,5.185,0,0,1-4.729-3.07,1.671,1.671,0,0,0-1.525-.987h-.979v-3.441h.979a1.671,1.671,0,0,0,1.525-.987,5.186,5.186,0,0,1,4.729-3.07h.653a2.533,2.533,0,0,0,2.2,1.271h4.617a2.533,2.533,0,0,0,2.195-1.271h.653a5.185,5.185,0,0,1,4.729,3.07,1.671,1.671,0,0,0,1.525.987h.979Zm0-4.82h-.979a.291.291,0,0,1-.266-.171,6.565,6.565,0,0,0-5.988-3.886h-.316v-8.259h4.2a3.349,3.349,0,0,1,3.346,3.345Zm0,0" transform="translate(-106.055 62.734) rotate(-90)" fill="#fff"></path>
                                                                    </svg>
                                                                </span>
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.751" height="24.751" viewBox="0 0 24.751 24.751">
                                                                        <g id="Group_41124" data-name="Group 41124" transform="translate(-313.5 -312)">
                                                                            <circle id="circle_1_" data-name="circle (1)" cx="12.375" cy="12.375" r="12.375" transform="translate(313.5 312)" fill="#fff"/>
                                                                            <g id="Iconly_Bold_Delete" data-name="Iconly/Bold/Delete" transform="translate(317.46 315.96)">
                                                                                <g id="Delete" transform="translate(2.104 1.403)">
                                                                                    <path id="Delete-2" data-name="Delete" d="M3.6,14a2.045,2.045,0,0,1-2.056-1.982c-.22-2-.586-6.716-.593-6.764a.555.555,0,0,1,.134-.391A.5.5,0,0,1,1.45,4.7h9.727a.508.508,0,0,1,.367.164.522.522,0,0,1,.127.391c0,.048-.374,4.775-.587,6.764A2.046,2.046,0,0,1,8.983,14c-.908.02-1.8.027-2.669.027C5.387,14.025,4.479,14.018,3.6,14ZM.5,3.57A.512.512,0,0,1,0,3.055V2.789a.507.507,0,0,1,.5-.515H2.545a.9.9,0,0,0,.87-.711l.107-.478A1.4,1.4,0,0,1,4.864,0h2.9A1.394,1.394,0,0,1,9.094,1.05l.114.512a.9.9,0,0,0,.87.713h2.045a.507.507,0,0,1,.5.515v.266a.512.512,0,0,1-.5.515Z" transform="translate(0)" fill="red"/>
                                                                                </g>
                                                                            </g>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <div class="align-items-center d-flex justify-content-between">
                                                                <div class="mb-4">
                                                                    <h6 class="font-weight-bold mb-2 text-white"><?php echo $value['card_holder_name']; ?></h6>
                                                                    <span class="fz-16 text-white">•••• </span><span class="fz-16 ml-2 text-white"><?php echo $value['card_number_last4']; ?></span>
                                                                </div>
                                                                <div class="">
                                                                    <span>
                                                                        <h6 style="color:white"><?php echo strtoupper($value['card_type']); ?></h6>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <img src="<?php echo base_url(); ?>website_assets/images/card/card.png" alt="card">
                                                        </a>
                                                    </div>
                                                    <?php  }?>
                                                  
                                                </div>
                                                <?php } else { ?> 
                                                    <h5><center>No payment method found</center></h5>
                                                <?php } ?>
                                                <!--add-card-->
                                                <a href="<?php echo base_url(); ?>my-add-debit-card" class="my-4 d-flex align-items-center justify-content-center fz-16">
                                                    <span class="mr-2">
                                                        <svg id="Iconly_Bold_Plus" data-name="Iconly/Bold/Plus" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                            <g id="Plus" transform="translate(2 2)">
                                                                <path id="Plus-2" data-name="Plus" d="M14.67,20H5.33a5.349,5.349,0,0,1-3.944-1.394A5.356,5.356,0,0,1,0,14.67V5.33A5.358,5.358,0,0,1,1.386,1.386,5.358,5.358,0,0,1,5.33,0h9.33a5.372,5.372,0,0,1,3.945,1.386A5.345,5.345,0,0,1,20,5.33v9.34C20,18.057,18.057,20,14.67,20ZM6.33,9.16a.819.819,0,0,0-.83.83.839.839,0,0,0,.83.84H9.16V13.66a.83.83,0,1,0,1.66,0V10.83h2.84a.835.835,0,0,0,0-1.669H10.82V6.34a.83.83,0,1,0-1.66,0V9.16Z" transform="translate(0 0)" fill="#a4a4a4"></path>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                    <span class="text-gray">Add Credit/Debit Card</span>
                                                </a>
                                                <!--end-add-card-->
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <!--start-delete-Modal -->
                <div class="modal fade" id="delete_payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                                <div class="content">
                                    <h4>Credit/Debit Card</h4>
                                    <p>Do you want to delete the payment method?</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0">
                                <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal">No</button>
                                <a href="javascript:void(0)" onclick="remove_paymentMethod();" class="btn btn__primary btn__rounded bg-danger"><p id="append_id" style="display: none;"></p>Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">

            $('.romove_modal').click(function(){
                var card_id = $(this).data('id');
                $('#append_id').html(card_id);
            })

            function remove_paymentMethod(){
                var card_id = $('#append_id').html();
                console.log(card_id)
                $.ajax({
                    url:  '<?php echo base_url(); ?>customer/user/remove_paymentMethod/'+card_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        swal("Success",message,"success");
                        window.location.href = '<?php echo base_url(); ?>my-payments';
                    }
                }); // END OF AJAX CALL
            }
        </script>
    </body>
</html>