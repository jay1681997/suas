<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/agora.css">

</head>

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section class=" ">
            <div class="container">
                <!--start-nav-->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Join class</li>
                    </ol>
                </nav>
                <!--end-nav-->

                <!--start-row-->
                <div class=" ">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- <p id="local-player-name" class="player-name"></p> -->
                            <div id="local-player" class="player" style="background-image: url('<?php echo S3_BUCKET_ROOT . USER_IMAGE . $user_data['profile_image']; ?>');background-position: center;
    background-repeat: no-repeat;"></div>
                        </div>
                        <div class="col-lg-6">
                            <form id="join-form" name="join-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input id="appid" type="hidden" placeholder="enter appid"
                                            value="<?php echo AGORA_APP_KEY; ?>">
                                        <input id="token" type="hidden" placeholder="enter token"
                                            value="<?php echo $token; ?>">
                                        <input id="channel" type="hidden" placeholder="enter channel name"
                                            value="<?php echo $channel; ?>">
                                        <!-- <input id="uid" type="hidden" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onafterpaste="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="Enter the user ID" value="0"> -->
                                        <input id="uid" type="hidden" placeholder="Enter the user ID"
                                            value="<?php echo $id; ?>">

                                        <button id="mute-audio" type="button"
                                            class="btn btn-primary btn-sm remoteMicrophone micOn"
                                            style="min-width: 50px !important;margin-left: 100px;border-radius: 50%;background-color: #a3a6a3;"><i
                                                class='fas fa-microphone'></i></button>
                                        <button id="mute-video" type="button"
                                            class="btn btn-primary btn-sm remoteCamera camOn"
                                            style="min-width: 50px !important;margin-left: 18px;border-radius: 50%;background-color: #a3a6a3;"><i
                                                class='fas fa-video'></i></button>
                                    </div>

                                    <div class="col-md-3">
                                        <button id="host-join" type="submit" class="btn btn-primary btn__rounded "
                                            style="margin-left:0%">Join Class</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn__primary btn__rounded bg-danger"
                                            data-toggle="modal" data-target="#Leave"><span>Leave</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><br><br>
                    <h6>People Joined Here</h6><br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="other-user-pic" id="remote-playerlist"></div>
                        </div>
                    </div>
                </div>
                <!--end-row-->
            </div>
        </section>
        <!--end-body-->


        <!--start-modal-->
        <div class="modal fade" id="Leave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94"
                            class="mb-25">
                        <div class="content">
                            <h4>Class In Progress</h4>
                            <p class="fz-16">There are no make-up classes or
                                refunds if you leave the
                                class in progress.</p>
                            <!-- <p class="fz-16">Are you sure you want to leave 
                            the class in progress?</p> -->
                        </div>
                    </div>
                    <div class="modal-footer border-0">
                        <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray"
                            data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn__primary btn__rounded bg-danger" id="if-leave">Leave
                            Now</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->

        <!--start-sucessful-Modal-->
        <div class="modal fade" id="sucessful-leave" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94"
                            class="mb-25">
                        <div class="content">
                            <h4>Class In Progress</h4>
                            <!-- <p class="fz-16">There are no make-up classes or
                                refunds if you leave the 
                                class in progress.</p> -->
                            <p class="fz-16">Are you sure you want to leave
                                the class in progress?</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <div class="modal-footer border-0">
                            <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray"
                                data-dismiss="modal"> No</button>
                            <a href="<?php echo base_url(); ?>enrolled-class-details/<?php echo base64_encode($class_id); ?>"
                                class="btn btn__primary btn__rounded bg-danger" id="leave">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->

        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div><!-- /.wrapper -->
    <?php include(APPPATH . "views/website/inc/script.php"); ?>
    <script>
        //set button id on click to hide first modal
        $("#if-leave").on("click", function () {
            $('#Leave').modal('hide');
        });
        //trigger next modal
        $("#if-leave").on("click", function () {
            $('#sucessful-leave').modal('show');
        });
    </script>

    <script src="https://download.agora.io/sdk/release/AgoraRTC_N.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/agora-rtm-sdk@1.3.1/index.js"></script>
    <script src="https://cdn.agora.io/rtmsdk/release/AgoraRTMTokenBuilder-1.4.0.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            var is_block_contest = "<?php echo $order_data['is_block_contest']; ?>";
            var is_mute_classess = "<?php echo $order_data['is_mute_classess']; ?>";
            var is_video_classess = "<?php echo $order_data['is_video_classess']; ?>";
            console.log(is_mute_classess);
            console.log("ID", "<?php echo $order_data['id']; ?>");
            if (is_block_contest == 1) {
                document.querySelector('#host-join').disabled = true;
            }
        };
        // create Agora client
        var client = AgoraRTC.createClient({
            mode: "live",
            codec: "vp8"
        });
        var localTracks = {
            videoTrack: null,
            audioTrack: null
        };

        var localTrackState = {
            videoTrackMuted: false,
            audioTrackMuted: false
        }
        var user_ids = [];
        var remoteUsers = {};
        var admin_id = "<?php echo $host_id; ?>";
        let initRtm = async (name) => {
            rtmClient = AgoraRTM.createInstance('<?php echo AGORA_APP_KEY; ?>');
            await rtmClient.login({
                'uid': rtmUid,
                'token': token
            })
            Channel = rtmClient.createChannel('<?php echo $channel; ?>');
            await channel.join();
        }
        // Agora client options
        var options = {
            appid: '<?php echo AGORA_APP_KEY; ?>',
            channel: '<?php echo $channel; ?>',
            uid: '<? php echo $id; ?>',
            token: '<?php echo $token; ?>',
            role: "audience", // host or audience
            audienceLatency: 2
        };

        // the demo can auto join channel with params in url
        $(() => {
            var urlParams = new URL(location.href).searchParams;
            options.appid = urlParams.get("appid");
            options.channel = urlParams.get("channel");
            options.token = urlParams.get("token");
            options.uid = urlParams.get("uid");
            if (options.appid && options.channel) {
                $("#uid").val(options.uid);
                $("#appid").val(options.appid);
                $("#token").val(options.token);
                $("#channel").val(options.channel);
                $("#join-form").submit();
            }
        })

        $("#host-join").click(function (e) {
            options.role = "host"
        })

        $("#lowLatency").click(function (e) {
            options.role = "audience"
            options.audienceLatency = 1
            $("#join-form").submit()
        })

        $("#ultraLowLatency").click(function (e) {
            options.role = "audience"
            options.audienceLatency = 2
            $("#join-form").submit()
        })

        $("#join-form").submit(async function (e) {
            e.preventDefault();
            $("#host-join").attr("disabled", true);
            $("#audience-join").attr("disabled", true);
            try {
                options.appid = $("#appid").val();
                options.token = $("#token").val();
                options.channel = $("#channel").val();
                options.uid = Number($("#uid").val());
                await join();
                if (options.role === "host") {
                    $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                    if (options.token) {
                        $("#success-alert-with-token").css("display", "block");
                    } else {
                        $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                        $("#success-alert").css("display", "block");
                    }
                }
            } catch (error) {
                console.error(error);
            } finally {
                $("#leave").attr("disabled", false);
            }
        })

        $("#leave").click(function (e) {
            leave();
        })

        $("#mute-audio").click(function (e) {
            if (!localTrackState.audioTrackMuted) {
                muteAudio();
            } else {
                unmuteAudio();
            }
        });

        $("#mute-video").click(function (e) {
            if (!localTrackState.videoTrackMuted) {
                muteVideo();
            } else {
                unmuteVideo();
            }
        })

        async function join() {
            // create Agora client
            console.log(options);
            console.log("AAAESADASSad");
            if (options.role === "audience") {
                client.setClientRole(options.role, {
                    level: options.audienceLatency
                });
                // add event listener to play remote tracks when remote user publishs.
                client.on("user-published", handleUserPublished);
                client.on("user-unpublished", handleUserUnpublished);
            } else {
                client.setClientRole(options.role);
                client.on("user-published", handleUserPublished);
                client.on("user-unpublished", handleUserUnpublished);
            }

            // join the channel
            options.uid = await client.join(options.appid, options.channel, options.token || null, options.uid || null);

            if (options.role === "host") {
                // create local audio and video tracks
                localTracks.audioTrack = await AgoraRTC.createMicrophoneAudioTrack();
                localTracks.videoTrack = await AgoraRTC.createCameraVideoTrack();
                // play local video track
                localTracks.videoTrack.play("local-player");

                $("#local-player-name").text(`localTrack(${options.uid})`);
                // publish local tracks to channel
                await client.publish(Object.values(localTracks));
                document.getElementById("uid").value = options.uid;
                RTMJoin();
                var is_mute_classess = "<?php echo $order_data['is_mute_classess']; ?>";
                var is_video_classess = "<?php echo $order_data['is_video_classess']; ?>";

                console.log("publish success");
                if (is_mute_classess == "1") {
                    muteAudio();
                    document.getElementById("mute-audio").disabled = true;
                }
                if (is_video_classess == "1") {
                    muteVideo();
                    document.getElementById("mute-video").disabled = true;
                }
            }
        }



        async function leave() {
            for (trackName in localTracks) {
                var track = localTracks[trackName];
                if (track) {
                    track.stop();
                    track.close();
                    localTracks[trackName] = undefined;
                }
            }

            // remove remote users and player views
            remoteUsers = {};
            $("#remote-playerlist").html("");

            // leave the channel
            await client.leave();

            $("#local-player-name").text("");
            $("#host-join").attr("disabled", false);
            $("#audience-join").attr("disabled", false);
            $("#leave").attr("disabled", true);
            console.log("client leaves channel success");
        }

        async function subscribe(user, mediaType) {
            const uid = user.uid;
            // subscribe to a remote user
            await client.subscribe(user, mediaType);
            if (admin_id == uid) {
                var user_type = "admin";
                var path = "<?php echo S3_BUCKET_ROOT . ADMIN_IMAGE; ?>";
            } else {
                var user_type = "user";
                var path = "<?php echo S3_BUCKET_ROOT . USER_IMAGE; ?>";
            }
            $.ajax({
                // url: SITE_URL + "customer/user/user_data/" + uid,
                url: "<?php echo base_url(); ?>customer/user/user_data/" + uid + "/" + user_type,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message1) {
                    var dataa = JSON.parse(message1);
                    if (user_type != 'admin') {
                        var username = dataa.username;
                    } else {
                        var username = 'Admin';
                    }
                    if (mediaType === 'video') {
                        const player = $(`
                            
                  <div id="player-wrapper-${uid}">
                      <label style="text-align:right;font-size:x-large;margin-left:30%">${username}</label>
                    <div id="player-${uid}" class="player" style="background-image: url('${path}${dataa.profile_image}');background-position: center;
    background-repeat: no-repeat;"></div>
                     
                  </div>
                `);
                        console.log("player**********************************");
                        console.log(player);
                        if (user_ids.includes(user.uid)) {
                            $(`#player-wrapper-${uid}`).remove();
                        }
                        $("#remote-playerlist").append(player);
                        user_ids.push(uid);
                        user.videoTrack.play(`player-${uid}`, {
                            fit: "contain"
                        });
                        // $("#remote-playerlist").append(player);
                        user.videoTrack.play(`player-${uid}`, {
                            fit: "contain"
                        });
                        // $("#remote-playerlist").append(player);
                        // user.videoTrack.play(`player-${uid}`, {fit:"contain"});
                    }
                    if (mediaType === 'audio') {
                        user.audioTrack.play();
                    }
                    check_user_place(uid);
                },
            });
            
        }
        client.on('user-joined', function (user) {
            var uid = user.uid;
            console.log('User joined the channel: ' + uid);
            if (admin_id == uid) {
                var user_type = "admin";
                var path = "<?php echo S3_BUCKET_ROOT . ADMIN_IMAGE; ?>";
            } else {
                var user_type = "user";
                var path = "<?php echo S3_BUCKET_ROOT . USER_IMAGE; ?>";
            }

            $.ajax({
                // url: SITE_URL + "customer/user/user_data/" + uid,
                url: "<?php echo base_url(); ?>customer/user/user_data/" + uid + "/" + user_type,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message1) {
                    var dataa = JSON.parse(message1);
                    if (user_type != 'admin') {
                        var username = dataa.username;
                    } else {
                        var username = 'Admin';
                    }

                    const player = $(`
                  <div id="player-wrapper-${uid}">
                    <label style="text-align:right;font-size:x-large;margin-left:30%">${username}</label>
                    <div id="player-${uid}" class="player" style="background-image: url('${path}${dataa.profile_image}');background-position: center;
    background-repeat: no-repeat;"></div>
                  </div>
                `);
                    console.log("player***************user-joined*******************");
                    console.log(player)
                    if (user_ids.includes(user.uid)) {
                        $(`#player-wrapper-${uid}`).remove();
                    }
                    $("#remote-playerlist").append(player);
                    user_ids.push(uid);
                    // check_user_place(uid);
                    user.videoTrack.play(`player-${uid}`, {
                        fit: "contain"
                    });
                    // // $("#remote-playerlist").append(player);
                    user.videoTrack.play(`player-${uid}`, {
                        fit: "contain"
                    });
                    if (mediaType === 'audio') {
                        user.audioTrack.play();
                    }
                    check_user_place(uid);
                },
            });

        })
        function handleUserPublished(user, mediaType) {

            //print in the console log for debugging 
            console.log('"user-published" event for remote users is triggered.');

            const id = user.uid;
            remoteUsers[id] = user;
            subscribe(user, mediaType);
        }
        client.on('user-left', function (evt) {
            var uid = evt.uid;
            console.log('User left the channel: ' + uid);
            if (admin_id == uid) {
                leave();
                window.location = "<?php echo base_url(); ?>enrolled-class-details/<?php echo base64_encode($class_id); ?>";
            }
            $(`#player-wrapper-${uid}`).remove();
        });

        function handleUserUnpublished(user, mediaType) {

            //print in the console log for debugging 
            console.log('"user-unpublished" event for remote users is triggered.');

            // if (mediaType === 'video') {
            //     const id = user.uid;
            //     delete remoteUsers[id];
            //     $(`#player-wrapper-${id}`).remove();
            // }
        }

        function hideMuteButton() {
            $("#mute-video").css("display", "none");
            $("#mute-audio").css("display", "none");
        }

        function showMuteButton() {
            $("#mute-video").css("display", "inline-block");
            $("#mute-audio").css("display", "inline-block");
        }

        async function muteAudio() {
            if (!localTracks.audioTrack) return;
            /**
             * After calling setMuted to mute an audio or video track, the SDK stops sending the audio or video stream. Users whose tracks are muted are not counted as users sending streams.
             * Calling setEnabled to disable a track, the SDK stops audio or video capture
             */
            await localTracks.audioTrack.setMuted(true);
            localTrackState.audioTrackMuted = true;
            $("#mute-audio").html("<i class='fas fa-microphone-slash'></i>");
        }

        async function muteVideo() {
            if (!localTracks.videoTrack) return;
            await localTracks.videoTrack.setMuted(true);
            localTrackState.videoTrackMuted = true;
            $("#mute-video").removeClass('camOn');
            $("#mute-video").html("<i class='fas fa-video-slash'></i>");
        }

        async function unmuteAudio() {
            if (!localTracks.audioTrack) return;
            await localTracks.audioTrack.setMuted(false);
            localTrackState.audioTrackMuted = false;
            $("#mute-audio").addClass('micOn');
            $("#mute-audio").html("<i class='fas fa-microphone'></i>");
        }

        async function unmuteVideo() {
            if (!localTracks.videoTrack) return;
            await localTracks.videoTrack.setMuted(false);
            localTrackState.videoTrackMuted = false;
            $("#mute-video").addClass('camOn');
            $("#mute-video").html("<i class='fas fa-video'></i>");
        }
        async function RTMJoin() {
            // Create Agora RTM client
            const clientRTM = AgoraRTM.createInstance($("#appid").val(), {
                enableLogUpload: false
            });
            const userId = document.getElementById("uid").value;

            $.ajax({
                url: "<?php echo base_url(); ?>customer/user/create_token/" + userId,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    // showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
                    console.log('errr');
                },
                success: function (message) {
                    clientRTM.login({
                        uid: userId,
                        "token": JSON.parse(message).token
                    }).then(() => {
                        // console.log('AgoraRTM client login success. Username: ' + accountName);
                        isLoggedIn = true;
                        // RTM Channel Join
                        var channelName = $('#channel').val();
                        channel = clientRTM.createChannel(channelName);
                        channel.join().then(() => {
                            console.log('AgoraRTM client channel join success.');
                            // Get all members in RTM Channel
                            channel.getMembers().then((memberNames) => {
                                console.log("All members in the channel are as follows: ");
                                console.log(memberNames);

                            });
                            // Send peer-to-peer message for audio muting and unmuting
                            $(document).on('click', '.remoteMicrophone', function () {
                                fullDivId = $(this).attr('id');
                                peerId = "<?php echo $host_id; ?>";
                                console.log("Remote microphone button pressed.");
                                let peerMessage = "audio";
                                if ($("#mute-audio").hasClass('micOn')) {
                                    peerMessage = "Mute";
                                } else {
                                    peerMessage = "Unmute";
                                }
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {
                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            });
                            // Send peer-to-peer message for video muting and unmuting
                            $(document).on('click', '.remoteCamera', function () {
                                fullDivId = $(this).attr('id');
                                // peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                peerId = "<?php echo $host_id; ?>";
                                console.log("Remote video button pressed.");
                                let peerMessage = "video";
                                if ($("#mute-video").hasClass('camOn')) {
                                    peerMessage = "VideoOff";
                                } else {
                                    peerMessage = "VideoOn";
                                }
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {
                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            });

                            function sendMessage(peerMessage, peerId) {
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {
                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            }
                            // Display messages from peer
                            clientRTM.on('MessageFromPeer', function ({
                                text
                            }, peerId) {
                                console.log(peerId + " muted/unmuted your" + text);
                                const myArray = text.split("_");
                                console.log("ChannelMessage", myArray);
                                if (myArray[0] == 'ChangeSpeakView') {
                                    var user_id = $("#uid").val();
                                    var parentContainer = document.getElementById("remote-playerlist");
                                    console.log(myArray[1]);
                                    console.log(user_id);
                                    if (myArray[1] != user_id) {
                                        var divToMove = document.getElementById("player-wrapper-" + myArray[1]);
                                    } else {
                                        var admin_id = "<?php echo $host_id; ?>";
                                        var divToMove = document.getElementById("player-wrapper-" + admin_id);
                                    }
                                    parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                                }
                                if (text == 'Mute') {
                                    muteAudio();
                                    sendMessage(text, peerId);
                                    document.getElementById("mute-audio").disabled = true;
                                } else if (text == "Unmute") {
                                    unmuteAudio();
                                    sendMessage(text, peerId);
                                    document.getElementById("mute-audio").disabled = false;
                                } else if (text == "VideoOn") {
                                    unmuteVideo();
                                    sendMessage(text, peerId);
                                    document.getElementById("mute-video").disabled = false;
                                } else if (text == "VideoOff") {
                                    muteVideo();
                                    sendMessage(text, peerId);
                                    document.getElementById("mute-video").disabled = true;
                                } else if (text == "Remove") {
                                    leave();
                                    sendMessage(text, peerId);
                                    setTimeout(function () {
                                        // window.location.reload(1);
                                        window.location = "<?php echo base_url(); ?>enrolled-class-details/<?php echo base64_encode($class_id); ?>";
                                    }, 1000);
                                }
                                if (text == "audio") {
                                    console.log("Remote video toggle reached with " + peerId);
                                    if (!localTrackState.audioTrackMuted) {
                                        muteAudio();
                                    } else {
                                        unmuteAudio();
                                    }
                                } else if (text == "video") {
                                    console.log("Remote video toggle reached with " + peerId);
                                    if (!localTrackState.videoTrackMuted) {
                                        muteVideo();
                                    } else {
                                        unmuteVideo();
                                    }
                                }
                            })
                            // Display channel member joined updated users
                            channel.on('MemberJoined', function () {
                                // Get all members in RTM Channel
                                channel.getMembers().then((memberNames) => {
                                    console.log("New member joined so updated list is: ");
                                    console.log(memberNames);
                                });
                            })
                            // Display channel member left updated users
                            channel.on('MemberLeft', function () {
                                // Get all members in RTM Channel
                                channel.getMembers().then((memberNames) => {
                                    console.log("A member left so updated list is: ");
                                    console.log(memberNames);
                                });
                            });
                            // channel.on("ChannelMessage",function(message){
                            //     const myArray = message.text.split("_");
                            //     console.log("ChannelMessage",myArray);
                            //    //===================================
                            //    console.log("player-wrapper-"+myArray[1]);
                            //    var user_id = $("#uid").val();
                            //      var parentContainer = document.getElementById("remote-playerlist");
                            //    if(myArray[1] != user_id){
                            //    var divToMove = document.getElementById("player-wrapper-"+myArray[1]);
                            //    }else{
                            //      var admin_id = "<?php echo $host_id; ?>";
                            //       var divToMove = document.getElementById("player-wrapper-"+admin_id);
                            //    }
                            //    parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                            //   //===================================
                            // });
                        }).catch(error => {
                            console.log('AgoraRTM client channel join failed: ', error);
                        }).catch(err => {
                            console.log('AgoraRTM client login failure: ', err);
                        });
                    });
                }
            })
            // Logout
            document.getElementById("leave").onclick = async function () {
                console.log("Client logged out of RTM.");
                await clientRTM.logout();
            }
        }

        function check_user_place(user_id) {
            var classes_id = "<?php echo $class_id; ?>";
            $.ajax({
                url: "<?php echo base_url(); ?>customer/user/check_user_place/" + classes_id + "/" + "classess",
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    console.log("check_user_place :- ", message);
                    var message1 = JSON.parse(message);
                    console.log("check_user_place :- ", message1.speaker_id);

                    if (message1.speaker_id == 0) {
                        var parentContainer = document.getElementById("remote-playerlist");
                        var admin_id = "<?php echo $host_id; ?>";
                        console.log(admin_id);
                        var divToMove = document.getElementById("player-wrapper-" + admin_id);
                        parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                    } else if (message1.speaker_id == user_id) {
                        var parentContainer = document.getElementById("remote-playerlist");
                        var divToMove = document.getElementById("player-wrapper-" + user_id);
                        parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                    }
                }
            });
        }
    </script>
</body>

</html>