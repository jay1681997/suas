<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Video Library</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info">
                                            <!--start-video-->
                                            <div class="speech-box inner-side">
                                                <div class="speech-img position-relative">
                                                    <div class="video-intro">
                                                        <div class="image">
                                                            <div class="play-button"></div>
                                                            <a href="javascript:" id="btn_play" class="">
                                                            <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play video"></a>
                                                        </div>
                                                        <video id="myVideo" controls="" class="w-100">
                                                            <source src="<?php echo $video_details['video']; ?>" class="w-100">
                                                        </video>
                                                    </div>
                                                    <div class="overlay position-absolute">
                                                        <div class="btt-plays">
                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="Mask Group -3" class="mr-2">
                                                                <span class="text-white"><?php echo $video_details['video_duration']; ?> min</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end-video-->
                                                <!--start-content-->
                                                <div class="content py-3">
                                                    <h4 class="mb-2"><?php echo $video_details['name']; ?></h4>
                                                    <p class="my-3"><?php echo $video_details['description']; ?></p>
                                                    <h6>Earn Points:</h6>
                                                    <p class="">Purchased video and earn <b style="color:#17966B"><?php echo $video_details['total_point']; ?> Points</b></p>
                                                  
                                                    <h6>Payment Summary</h6>
                                                    <div class="table-main pb-3">
                                                        <div class="d-flex fz-10 justify-content-between tabl-wrap mb-2">
                                                            <span>Purchase Date</span>
                                                            <span><?php echo $this->session->userdata('currency').''.$video_details['price']; ?></span>
                                                        </div>
                                                        <div class="d-flex fz-10 justify-content-between tabl-wrap mb-2">
                                                            <span>Sub total</span>
                                                            <span><?php echo $this->session->userdata('currency').''.$video_details['price']; ?></span>
                                                        </div>
                                                        <div class="d-flex fz-10 justify-content-between tabl-wrap mb-2">
                                                            <span>Tax</span>
                                                            <span><?php echo $this->session->userdata('currency').''.$video_details['tax']; ?></span>
                                                        </div>
                                                        <div class="d-flex fz-10 justify-content-between tabl-wrap mb-2">
                                                            <span>Credit points discount</span>
                                                            <span>- <?php echo $this->session->userdata('currency').''.$video_details['credit_point_discount']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex fz-16 justify-content-between tabl-wrap pt-3">
                                                        <span class="font-weight-bold text-dark">Total amount</span>
                                                        <?php $total_sum = (($video_details['price'] + $video_details['tax']) - $video_details['credit_point_discount']); ?>
                                                        <span class="font-weight-bold text-dark"><?php echo $this->session->userdata('currency').''.$total_sum; ?></span>
                                                    </div>
                                                 
                                                    
                                                </div>
                                                <!--end-content-->
                                            </div>  
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php include(APPPATH."views/website/inc/script.php");?>
         <script type="text/javascript">
                        var vid = document.getElementById("myVideo");
vid.onplay = function() {
  // alert("The video has started to play");
  $("#btn_play").hide();
};    
vid.onpause = function(){
       $("#btn_play").show();
      // alert("The video has started to play1");
}
    </script>
    </body>
</html>