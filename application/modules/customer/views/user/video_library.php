<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style>
        .speech-box.img-height .speech-img>img {
            height: 225px;
            object-fit: cover;
        }
    </style>
    <style>
    /* new css */

    .currancy_block {
        display: inline-block;
        background: #15976b;
        border-radius: 50%;
        width: 24px;
        height: 24px;
        line-height: 25px;
        padding: 2px 2px;
    }
    .currency_part {
        color: #ffffff;
        font-size: 12px;
        border: 1px solid #fff;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        line-height: 20px;
    }
</style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Video Library</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">Video Library</h4> 
                                                <p class="fz-16 mb-4">Click below to see details on selected 
                                                    educational videos.</p>
                                            </div>
                                            <!--end-title-->
                                            <?php if(!empty($videos)) { ?>
                                            <div class="row"> 
                                                <?php foreach ($videos as $key => $value) { ?>
                                                <div class="col-md-6 mb-4">
                                                    <a href="<?php echo base_url(); ?>video-library-details/<?php echo base64_encode($value['id']); ?>" class="speech-box img-height">
                                                        <div class="speech-img position-relative bg-overlay-blck">
                                                            <img src="<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$value['image']; ?>" alt="image1" class="w-100">
                                                           <!-- <video src=" <?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$value['video']; ?> " class="w-100" style="object-fit: cover;"> 
                                                            </video> -->
                                                            <div class="overlay position-absolute w-100" >
                                                                <div class="row align-items-end" >
                                                                    <div class="col-lg-7 mb-lg-0 mb-2 text-lg-left" >
                                                                        <div class="btt-plays">
                                                                            <span class="text-white">
                                                                                <div class="btt-plays">
                                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                        <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play" class="mr-2">
                                                                                        <span class="text-white"><?php echo $value['video_duration']; ?> min</span>
                                                                                    </div>
                                                                                </div>
                                                                            </span>
                                                                            <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6> 
                                                                            <p class="mb-0">
                                                                                <span class="mr-2"><svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 10.496 11.662">
                                                                                    <path id="Calendar-2" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"></path>
                                                                                </svg></span>
                                                                                <span class="text-white"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd F, Y',$this->session->userdata('website_timezone')); ?></span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--start-price-tage-->
                                                            <div class="pric-tag-tp">
                                                                <div class="btt-plays">
                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                        <div class="currancy_block">
                                                                              <div class="currency_part"><?php echo $this->session->userdata('currency'); ?></div>
                                                                            </div>
                                                                        <span class="text-white"><?php echo round($this->session->userdata('currency_rate')*$value['price'],2); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end-price-tage-->
                                                        </div> 
                                                    </a>
                                                </div>
                                                <?php } ?>
                                            </div> 
                                            <?php }else{ ?>
                                                <h5><center>Video library not found</center></h5>
                                            <?php } ?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>