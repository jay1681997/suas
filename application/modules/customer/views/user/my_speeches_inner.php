<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <style type="text/css">
        .abc{
            background-color: #999;
        }
        .btn:hover{
            color: #FFFF;
        }
    </style>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
                <section class="">
                    <div class="container">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>my-speeches">Speech</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Speech Detail</li>
                            </ol>
                        </nav>
                        <!--end-nav-->

                        <!--start-row-->
                        <div class="main-list">
                            <div class="">
                                <div href="#" class="align-items-center inner-side row speech-box">
                                    <div class="col-lg-6"> 
                                        <div class="video-intro">
                                            <div class="image">
                                                <div class="play-button"></div>
                                                <a href="javascript:" id="btn_play" class="">
                                                    <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play-video" />
                                                </a>
                                            </div> 

                                            <video id="myVideo"controls class="w-100">
                                                <source  src="<?php echo S3_BUCKET_ROOT.SPEECH_IMAGE.$speech_details['video']; ?>" class="w-100" />
                                            </video>
                                           <div class="overlay position-absolute px-5" align="right">
                                                <div class="btt-plays">
                                                    <div  class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                        <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="Mask Group -3" class="mr-2">
                                                        <span class="text-white"><?php echo  $speech_details['video_duration']; ?> min</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="speech-img position-relative">
                                            <!-- <video controls="" class="w-100 rounded">
                                                <source src="<?php echo S3_BUCKET_ROOT.SPEECH_IMAGE.$speech_details['video']; ?>" class="w-100">
                                            </video> -->
                                            <!-- <div class="overlay position-absolute px-5" align="right">
                                                <div class="btt-plays">
                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                        <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="Mask Group -3" class="mr-2">
                                                        <span class="text-white"><?php echo  $speech_details['video_duration']; ?> min</span>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-6 content px-4 py-4 rounded shadow">
                                        <h4 class="text-gray mb-2" style="color:black;"><?php echo $speech_details['title']; ?></h4>
                                        <p class="mb-3">
                                            <span class="mr-2">
                                                <svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 10.496 11.662">
                                                    <path id="Calendar-2" style="fill: green;" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"></path>
                                                </svg>
                                            </span>
                                            <span class="text-gray"><?php echo $this->common_model->date_convert($speech_details['insert_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                        </p>
                                        <p class="text-gray mb-4"><?php echo $speech_details['description']; ?></p>
                                        <!--start-->
                                        <div class="row align-items-center">
                                            <div class="col-lg-2">
                                                <a href="javascript:void(0)" class="rounded-circle bg-danger text-white px-3 py-3" data-toggle="modal" data-target="#Evaluationdelete">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                                        <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                            <div class="col-lg-10">
                                                <?php if(empty($speech_evaluation)){ ?>
                                                      <a href="login.html" class="btn-animated-bl" data-toggle="modal" data-target="#exampleModalCenter"><span>Submit For Evaluation</span></a>
                                               <?php }else{ ?>
                                                <style type="text/css">
                                                    .btn-animated-bl::before{
                                                        background-color: #999;
                                                    }
                                                    .btn-animated-bl:hover{
                                                        border: 1px solid #999;
                                                        background:  #999;
                                                    }
                                                </style>
                                                <a  class="btn-animated-bl" data-toggle="modal" disabled><span style="color:#FFFFFF;">Submitted For Evaluation</span></a>

                                             <?php  } ?>
                                              
                                            </div>
                                        </div>
                                        <!--end-row-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end-row-->
                    </div>
                </section>
                <!--end-body--> 

                <!--start-Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                                <?php $remaining_video = 10 - $video_count; ?>
                                <div class="content">
                                    <h4>You have submitted <?php echo $video_count; ?> videos for evaluation  <span style="color:#17966B"></span></h4>
                                    <p>Are you sure you want to 
                                        submit video?</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0">
                                <button type="button" class="btn  btn__rounded" data-dismiss="modal">No</button>
                                <button type="button" class="btn btn__primary btn__rounded" id="if-say-yes" onclick="submit_speechEvaluation('<?php echo $speech_details['id']; ?>')">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->


                <!--start-sucessful-Modal-->
                <div class="modal fade" id="sucessful-evaluation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/Group -94.png" alt="Group -94" class="mb-25">
                                <div class="content">
                                    <h4>Submit For Evaluation</h4>
                                    <p class="fz-16">Your speech has successfully been
                                      submitted for evaluation.</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0 justify-content-center">
                                <a href="<?php echo base_url(); ?>my-evaluations" class="btn btn__primary btn__rounded">Continue</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->



                <!--start-delete-Modal -->
                <div class="modal fade" id="Evaluationdelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                                <div class="content">
                                    <h4>Delete Speech</h4>
                                    <p>Do you want to delete this 
                                    speech?</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0">
                                <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal">No</button>
                                <a href="javascript:void(0)" onclick="remove_speech('<?php echo $speech_details['id']; ?>')" class="btn btn__primary btn__rounded bg-danger">Yes</a>
                            </div>
                        </div>
                    </div>
                  </div>
                <!--end-modal-->

            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->

        <?php include(APPPATH."views/website/inc/script.php");?>
        <script>

  var vid = document.getElementById("myVideo");
vid.onplay = function() {
  $("#btn_play").hide();
};    
vid.onpause = function(){
       $("#btn_play").show();
      
}
            function remove_speech(speech_id){
               
                $.ajax({
                    url:  '<?php echo base_url(); ?>customer/user/remove_speech/'+speech_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        swal("Success",message,"success");
                        window.location.href = '<?php echo base_url(); ?>my-speeches';
                    }
                }); // END OF AJAX CALL
            }

            function submit_speechEvaluation(speech_id){
                $.ajax({
                    url:  '<?php echo base_url(); ?>customer/user/submit_speech_forevaluation/'+speech_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        $('#sucessful-evaluation').modal('show'); 
                    }
                }); // END OF AJAX CALL
            }
            function speech_upload_done(){
                    swal("Success",'Your speech already uploaded for the evaluation',"success");
            }
        </script>
    </body>
</html>