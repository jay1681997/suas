<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Contests</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="bio-graph-info px-lg-4 px-3 py-3">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="speech-img position-relative speech-box bg-overlay-blck">
                                                                <?php $file_ext = explode('.', $winner_details['contest_image']); 
                                                                 if($file_ext[4] == 'mp4' || $file_ext[4] == 'MP4'){  ?>
                                                                    <video src=" <?php echo $winner_details['video']; ?> " class="w-100"> 
                                                                    </video>
                                                                <?php }else { ?> 
                                                                    <img src="<?php echo $winner_details['contest_image']; ?>" alt="image1" class="w-100">
                                                                <?php } ?>
                                                            
                                                            <div class="overlay position-absolute text-right">
                                                                <div class="btt-plays">
                                                                    <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                        <span class="text-white"><?php echo ($winner_details['video_duration'] == '')?'00:00:00':$winner_details['video_duration']; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-0 content px-4 py-4 rounded">
                                                        <div class="align-items-center mb-0 row ">
                                                            <div class="col-lg-8">
                                                                <ul class="dots list-unstyled d-flex mb-0 align-items-center">
                                                                    <li class="pr-3 fz-16 theme-color font-weight-bold"><h4 class="mb-0"><?php echo $winner_details['name']; ?></h4></li>
                                                                    <li class="pl-3 fz-16 theme-color"><?php echo ucfirst($winner_details['contest_type']); ?></li>
                                                                </ul>
                                                            </div> 
                                                        </div>
                                                        <?php if($winner_details['winner'] == 1){ ?>
                                                            <span class="fz-16 theme-color font-weight-bold mb-4 d-block">Congratulations you won 1st Prize!</span>
                                                        <?php } else if($winner_details['winner'] == 2){ ?>
                                                            <span class="fz-16 theme-color font-weight-bold mb-4 d-block">Congratulations you won 2nd Prize!</span>
                                                        <?php } else if($winner_details['winner'] == 3){ ?>
                                                            <span class="fz-16 theme-color font-weight-bold mb-4 d-block">Congratulations you won 3rd Prize!</span>
                                                        <?php } ?>
                                                        <p class="text-gray mb-4 fz-16"><?php echo $winner_details['description']; ?></p>
                                                    </div>
                                                </div>
                                                
                                                <!--start-row-->
                                                <?php if(!empty($winner_details['prizes'])) { ?>
                                                <div class="pb-30">
                                                    <h5 class="mb-2">Prizes:</h5>
                                                    <p class="fz-16">Winners will be announced on <span class="text-dark"><?php echo $this->common_model->date_convert($winner_details['winner_date'], 'd M Y',$this->session->userdata('website_timezone'));?></span></p>
                                                    <!--start-row-->
                                                    <div class="row pt-10">
                                                        <?php foreach ($winner_details['prizes'] as $key => $value) {?>
                                                        <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                                            <div class="pric-main shadow text-center">
                                                                <img src="<?php echo $value['prize_image']; ?>" alt="class5" width="100" height="100" class="mb-3">
                                                                <h5 class="mb-3"><?php echo $value['prize_name']; ?></h5>
                                                                <span class="d-block text-warning"><?php echo $this->session->userdata('currency').''.$value['price']; ?></span>
                                                                <span class="d-block text-warning">+ <?php echo $value['credit_points']; ?> points</span>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <!--end-row-->
                                                </div>
                                                <?php } ?>
                                                  
                                                <?php if(!empty($winner_details['winners'])) { ?>
                                                <div class="pt-30 pb-20">
                                                    <h5 class="mb-4">Winners:</h5>
                                                    <?php foreach ($winner_details['winners'] as $key => $value) {?>
                                                    <div class="align-items-center border-dashed pb-20 row">
                                                        <div class="col-sm-2">
                                                            <img src="<?php echo $value['profile_image']; ?>" alt="user" class="w-100 rounded">
                                                        </div>
                                                        <div class="col-sm-10 d-flex justify-content-between">
                                                            <div>
                                                                <h5 class="mb-1"><?php echo $value['username']; ?></h5>
                                                                <p class="mb-0 fz-16"><?php echo $value['country']; ?></p>
                                                            </div>
                                                            <div>

                                                                <?php if($value['winner'] == 1){ echo "P1";?> 
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 29 29">
                                                                            <g id="Group_41254" data-name="Group 41254" transform="translate(-47.933 -924.933)">
                                                                                <circle id="Ellipse_5035" data-name="Ellipse 5035" cx="14.5" cy="14.5" r="14.5" transform="translate(47.933 924.933)" fill="#ffb900" opacity="0.05"/>
                                                                                <g id="Group_41142" data-name="Group 41142" transform="translate(54.903 931.903)">
                                                                                    <path id="Path_208169" data-name="Path 208169" d="M0,0H15.06V15.06H0Z" fill="none"/>
                                                                                    <path id="Path_208170" data-name="Path 208170" d="M9.02,5.137A5.02,5.02,0,1,1,4,10.157,5.02,5.02,0,0,1,9.02,5.137Zm0,2.2L8.19,9.015l-1.856.27,1.343,1.308L7.36,12.442l1.66-.873,1.66.872-.317-1.848,1.343-1.309L9.85,9.015ZM9.648,2h3.138V3.882L11.93,4.6a6.234,6.234,0,0,0-2.282-.683V2ZM8.393,2V3.913A6.234,6.234,0,0,0,6.111,4.6l-.856-.713V2H8.393Z" transform="translate(-1.49 -0.745)" fill="#ffb900"/>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                                                <?php } else if($value['winner'] == 2) { echo "P2";?>
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 29 29">
                                                                            <g id="Group_41255" data-name="Group 41255" transform="translate(-48 -925)">
                                                                            <circle id="Ellipse_5035" data-name="Ellipse 5035" cx="14.5" cy="14.5" r="14.5" transform="translate(48 925)" fill="#ccc" opacity="0.1"/>
                                                                                <g id="Group_41142" data-name="Group 41142" transform="translate(54.935 931.935)">
                                                                                    <path id="Path_208169" data-name="Path 208169" d="M0,0H15.13V15.13H0Z" fill="none"/>
                                                                                    <path id="Path_208170" data-name="Path 208170" d="M9.043,5.152A5.043,5.043,0,1,1,4,10.2,5.043,5.043,0,0,1,9.043,5.152Zm0,2.207-.834,1.69-1.864.271,1.349,1.314-.318,1.857,1.668-.877,1.668.876-.318-1.857,1.349-1.315L9.878,9.047ZM9.674,2h3.152V3.891l-.859.717a6.263,6.263,0,0,0-2.293-.687V2ZM8.413,2V3.922a6.263,6.263,0,0,0-2.292.686l-.86-.717V2H8.413Z" transform="translate(-1.478 -0.739)" fill="#1974b1"/>
                                                                                </g>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                                                <?php } else if($value['winner'] == 3) { echo "P3";?>
                                                                    <span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="29" viewBox="0 0 29 29">
                                                                        <g id="Group_41256" data-name="Group 41256" transform="translate(-48 -925)">
                                                                            <circle id="Ellipse_5035" data-name="Ellipse 5035" cx="14.5" cy="14.5" r="14.5" transform="translate(48 925)" fill="#f7894a" opacity="0.05"/>
                                                                            <g id="Group_41142" data-name="Group 41142" transform="translate(54.935 931.935)">
                                                                                <path id="Path_208169" data-name="Path 208169" d="M0,0H15.13V15.13H0Z" fill="none"/>
                                                                                <path id="Path_208170" data-name="Path 208170" d="M9.043,5.152A5.043,5.043,0,1,1,4,10.2,5.043,5.043,0,0,1,9.043,5.152Zm0,2.207-.834,1.69-1.864.271,1.349,1.314-.318,1.857,1.668-.877,1.668.876-.318-1.857,1.349-1.315L9.878,9.047ZM9.674,2h3.152V3.891l-.859.717a6.263,6.263,0,0,0-2.293-.687V2ZM8.413,2V3.922a6.263,6.263,0,0,0-2.292.686l-.86-.717V2H8.413Z" transform="translate(-1.478 -0.739)" fill="#f7894a"/>
                                                                            </g>
                                                                        </g>
                                                                        </svg>
                                                                    </span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>  
                                                </div> 
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>