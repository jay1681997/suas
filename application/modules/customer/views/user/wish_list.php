<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Video Library</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">Wish List</h4> 
                                                <p class="fz-16 mb-4">Future classes, contests and videos.</p>
                                            </div>
                                            <!--end-title-->
                                            <?php if(!empty($wishlist)) { ?>
                                                <div class="row">
                                                    <?php foreach ($wishlist as $key => $value) { ?> 
                                                        <div class="col-md-6 mb-4">
                                                            <a href="javascript:void(0)" class="speech-box img-height">
                                                                <div class="speech-img position-relative bg-overlay-blck">
                                                                    <?php if($value['item_type'] == 'video') { ?>
                                                                    <img src="<?php echo $value['video_image']; ?>" alt="image1" class="w-100">
                                                                    <?php }else if($value['item_type'] == 'class') { ?>
                                                                        <img src="<?php echo $value['class_image']; ?>" alt="image1" class="w-100">
                                                                    <?php }else if($value['item_type'] == 'contest') { ?>
                                                                        <img src="<?php echo $value['contest_image']; ?>" alt="image1" class="w-100">
                                                                    <?php } ?>
                                                                    <div class="overlay position-absolute w-100">
                                                                        <div class="row align-items-end">
                                                                            <div class="col-lg-7 mb-lg-0 mb-2 text-lg-left">
                                                                                <?php if($value['item_type'] == 'video') { ?>
                                                                                <div class="btt-plays">
                                                                                    <span class="text-white">
                                                                                        <div class="btt-plays">
                                                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                                <img src="<?php echo base_url(); ?>website_assets/images/video/play.png" alt="play" class="mr-2">
                                                                                                <span class="text-white"><?php echo $value['video_duration']; ?> min</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                                    <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6> 
                                                                                </div>
                                                                                <?php }else{ ?>
                                                                                    <div class="">
                                                                                        <h6 class="text-white mb-1 mb-2">
                                                                                            <?php if($value['item_type'] == 'class') { echo $value['program_title']; } else { echo $value['contest_name'] ;} ?></h6> 
                                                                                        <div class="d-flex align-items-center">
                                                                                          <span class="mr-2">
                                                                                            <svg id="Iconly_Bold_Calendar" data-name="Iconly/Bold/Calendar" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                                                              <g id="Calendar" transform="translate(2 1.333)">
                                                                                                <path id="Calendar-2" data-name="Calendar" d="M3.434,13.333H3.427a3.435,3.435,0,0,1-2.493-.921A3.426,3.426,0,0,1,.007,9.92L0,4.369A3.393,3.393,0,0,1,.812,2.033,3.293,3.293,0,0,1,3,1.02V.52A.515.515,0,0,1,3.15.151a.511.511,0,0,1,.867.368V.986L7.928.98V.514A.513.513,0,0,1,8.072.146a.511.511,0,0,1,.868.367v.5A3.31,3.31,0,0,1,11.155,2a3.329,3.329,0,0,1,.838,2.323L12,9.944a3.156,3.156,0,0,1-3.419,3.383ZM8.7,9.461a.555.555,0,0,0-.539.568A.553.553,0,1,0,8.7,9.461Zm-5.417,0h0a.567.567,0,0,0-.539.574.548.548,0,0,0,.541.533h.026a.539.539,0,0,0,.376-.177.548.548,0,0,0,.143-.4.542.542,0,0,0-.546-.532Zm2.712,0h0a.57.57,0,0,0-.539.574.549.549,0,0,0,.542.533h.024a.549.549,0,0,0,.52-.574.542.542,0,0,0-.546-.532Zm-2.715-2.4h0a.567.567,0,0,0-.539.574.548.548,0,0,0,.541.533H3.3a.538.538,0,0,0,.376-.177.548.548,0,0,0,.143-.4.541.541,0,0,0-.545-.532Zm2.713-.023h0a.567.567,0,0,0-.539.574.549.549,0,0,0,.543.533h.024a.55.55,0,0,0,.519-.574.542.542,0,0,0-.545-.532Zm2.714,0h0A.551.551,0,0,0,8.16,7.6V7.61a.542.542,0,0,0,.547.532H8.72a.55.55,0,0,0-.019-1.1Zm-5.7-5A2.046,2.046,0,0,0,1.013,4.367v.207l9.965-.013V4.328a2.061,2.061,0,0,0-2.038-2.3v.513a.509.509,0,0,1-.869.365.516.516,0,0,1-.144-.363V2.006l-3.91.005v.539a.51.51,0,0,1-.87.365.516.516,0,0,1-.144-.364V2.039Z" transform="translate(0)" fill="#fff"/>
                                                                                              </g>
                                                                                            </svg>
                                                                                          </span>
                                                                                        <span class="text-white"><?php echo $this->common_model->date_convert($value['insert_datetime'], 'd M, Y',$this->session->userdata('website_timezone'));?></span>
                                                                                        </div>
                                                                                      </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--start-price-tage-->
                                                                    <div class="pric-tag-tp">
                                                                        <div class="btt-plays">
                                                                            <div class="d-inline-block">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 10 13">
                                                                                    <g id="Bookmark" transform="translate(-0.5 0.25)">
                                                                                        <path id="Bookmark-2" data-name="Bookmark" d="M9.366,13a.622.622,0,0,1-.322-.091L4.994,10.8.938,12.909a.648.648,0,0,1-.307.084A.648.648,0,0,1,0,12.33V2.463A2.185,2.185,0,0,1,.9.569,3.733,3.733,0,0,1,3.062,0H6.919A3.749,3.749,0,0,1,9.087.569,2.214,2.214,0,0,1,10,2.463V12.33a.7.7,0,0,1-.075.318.65.65,0,0,1-.387.325A.562.562,0,0,1,9.366,13ZM2.638,3.926a.514.514,0,0,0,0,1.027H7.344a.514.514,0,0,0,0-1.027Z" transform="translate(0.5 -0.25)" fill="#17966b"/>
                                                                                    </g>
                                                                                </svg> 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--end-price-tage-->
                                                                </div> 
                                                            </a>
                                                        </div>
                                                    <?php } ?> 
                                                </div>
                                            <?php } else { ?>
                                                <h5><center>No classes, contests and videos added in wishlist</center></h5>
                                            <?php } ?> 
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>