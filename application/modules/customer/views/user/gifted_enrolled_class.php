<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section>
                    <div class="container">
                        <div class="contact-panel px-2 px-lg-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Gifted Enrolled</li>
                                </ol>
                            </nav>
                            <!--end-nav--> 

                            <div class="container bootstrap snippets bootdey">
                                <div class="row">
                                    <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                    <div class="profile-info col-lg-9">
                                        <div class="shadow"> 
                                            <div class="px-lg-4 px-3 py-3 bio-graph-info">
                                                <!--start-nav-->
                                                <ul class="nav nav-tabs mb-1 forgot-tab"role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link border-0 active"data-toggle="tab"href="#profile"role="tab">Gifted To</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link border-0"data-toggle="tab"href="#messages"role="tab">Received</a>
                                                    </li>
                                                </ul>
                                                <!--end-nav-->

                                                <!--tab-content-->
                                                <div class="tab-content">
                                                    <!------------------------------------tab 1-------------------------------------------->
                                                    <div class="tab-pane active" id="profile" role="tabpanel">
                                                        <p class="fz-16">See your gift history below</p>
                                                        <?php if(!empty('gift_class_list')){ ?>
                                                        <div class="row">
                                                            <?php foreach ($gift_class_list as $key => $value) { ?>
                                                            <div class="col-md-12 mb-4">
                                                                <a href="<?php echo base_url(); ?>gifted-enrolled-class-details/<?php echo base64_encode($value['id']); ?>" class="speech-box img-height">
                                                                    <div class="speech-img position-relative bg-overlay-blck">
                                                                        <img src="<?php echo $value['class_image']; ?>" alt="image1" class="w-100">
                                                                        <div class="overlay position-absolute w-100">
                                                                            <div class="row align-items-end">
                                                                                <div class="col-lg-7 mb-lg-0 mb-2 text-lg-left">
                                                                                    <div class="btt-plays">
                                                                                        <h5 class="text-white mb-1 mb-2"><?php echo $value['program_title']; ?></h5>
                                                                                        <p class="mb-0">
                                                                                        <span class="text-white">Gifted To - <?php echo ucfirst($value['username']); ?></span>
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-lg-5 text-lg-right">
                                                                                    <div class="price d-block">
                                                                                        <span>
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 25 23.755">
                                                                                            <g id="b9e2ee35-d5ff-4898-81b3-d162a56746b9" transform="translate(0 -12.707)">
                                                                                            <path id="Path_210841" data-name="Path 210841" d="M24.062,17.712H18.057A3.432,3.432,0,0,0,12.5,13.8a3.432,3.432,0,0,0-5.558,3.916h-6A.937.937,0,0,0,0,18.649V22.4a.937.937,0,0,0,.938.937h10V17.712H10a1.563,1.563,0,1,1,1.563-1.563v1.563h1.875V16.149A1.563,1.563,0,1,1,15,17.712h-.938v5.625h10A.937.937,0,0,0,25,22.4v-3.75a.937.937,0,0,0-.937-.937Z" transform="translate(0)" fill="#fff"/>
                                                                                            <path id="Path_210842" data-name="Path 210842" d="M38.4,279.112a.938.938,0,0,0,.938.938h8.125V268.8H38.4Z" transform="translate(-36.525 -243.588)" fill="#fff"/>
                                                                                            <path id="Path_210843" data-name="Path 210843" d="M288,280.05h8.125a.937.937,0,0,0,.937-.937V268.8H288Z" transform="translate(-273.938 -243.588)" fill="#fff"/>
                                                                                            </g>
                                                                                            </svg>
                                                                                        </span> 
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                </a>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                    </div>
                                                    <!------------------------------------tab 2-------------------------------------------->
                                                    <div class="tab-pane" id="messages" role="tabpanel">
                                                        <p class="fz-16">See your received gift history below</p>
                                                        <?php if(!empty('giftfrom_class_list')){ ?>
                                                        <div class="row">
                                                            <?php foreach ($giftfrom_class_list as $key => $value) { ?>
                                                                <div class="col-md-12 mb-4">
                                                                    <a href="<?php echo base_url(); ?>received-from-class-details/<?php echo base64_encode($value['id']); ?>" class="speech-box img-height">
                                                                        <div class="speech-img position-relative bg-overlay-blck">
                                                                            <img src="<?php echo $value['class_image']; ?>" alt="image1" class="w-100">
                                                                            <div class="overlay position-absolute w-100">
                                                                                <div class="row align-items-end">
                                                                                    <div class="col-lg-7 mb-lg-0 mb-2 text-lg-left">
                                                                                        <div class="btt-plays">
                                                                                            <h5 class="text-white mb-1 mb-2"><?php echo $value['program_title']; ?></h5>
                                                                                            <p class="mb-0">
                                                                                                <span class="text-white">Gifted From - <?php echo ucfirst($value['username']); ?></span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-5 text-lg-right">
                                                                                        <div class="price d-block">
                                                                                            <span>
                                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 25 23.755">
                                                                                                <g id="b9e2ee35-d5ff-4898-81b3-d162a56746b9" transform="translate(0 -12.707)">
                                                                                                <path id="Path_210841" data-name="Path 210841" d="M24.062,17.712H18.057A3.432,3.432,0,0,0,12.5,13.8a3.432,3.432,0,0,0-5.558,3.916h-6A.937.937,0,0,0,0,18.649V22.4a.937.937,0,0,0,.938.937h10V17.712H10a1.563,1.563,0,1,1,1.563-1.563v1.563h1.875V16.149A1.563,1.563,0,1,1,15,17.712h-.938v5.625h10A.937.937,0,0,0,25,22.4v-3.75a.937.937,0,0,0-.937-.937Z" transform="translate(0)" fill="#fff"/>
                                                                                                <path id="Path_210842" data-name="Path 210842" d="M38.4,279.112a.938.938,0,0,0,.938.938h8.125V268.8H38.4Z" transform="translate(-36.525 -243.588)" fill="#fff"/>
                                                                                                <path id="Path_210843" data-name="Path 210843" d="M288,280.05h8.125a.937.937,0,0,0,.937-.937V268.8H288Z" transform="translate(-273.938 -243.588)" fill="#fff"/>
                                                                                                </g>
                                                                                                </svg>
                                                                                            </span> 
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div> 
                                                                    </a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!--end-tab-content-->
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>