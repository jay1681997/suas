<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section>
                    <div class="container">
                        <div class="contact-panel px-2 px-lg-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Gifted Enrolled</li>
                                </ol>
                            </nav>
                            <!--end-nav--> 

                            <div class="container bootstrap snippets bootdey">
                                <div class="row">
                                    <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                    <div class="profile-info col-lg-9">
                                        <div class="shadow"> 
                                            <div class="px-lg-4 px-3 py-3 bio-graph-info">
                                                <!--start-video-->
                                                <div class="speech-box inner-side">
                                                    <div class="speech-img position-relative">
                                                        <img src="<?php echo $gift_details['class_image']; ?>" alt="user" class="w-100 rounded">
                                                        <!-- <div class="video-intro">
                                                            <div class="image">
                                                                <div class="play-button"></div>
                                                                <a href="javascript:" id="btn_play" class="">
                                                                <img src="<?php echo base_url();?>website_assets/images/video/play.png" alt="play-video"></a>
                                                            </div>
                                                            <video controls="" class="w-100">
                                                                <source src="<?php echo base_url();?>website_assets/images/video/video_preview_h264.mp4" class="w-100">
                                                            </video>
                                                        </div>
                                                        <div class="overlay position-absolute">
                                                            <div class="btt-plays">
                                                                <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                    <img src="<?php echo base_url();?>website_assets/images/video/play.png" alt="play" class="mr-2">
                                                                    <span class="text-white">5 min</span>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <!--end-video-->
                                                    <!--start-content-->
                                                    <div class="content py-3">
                                                        <div class="row align-items-start mb-3">
                                                            <div class="col-lg-8">
                                                                <h4 class="mb-2"><?php echo $gift_details['program_title']; ?></h4>
                                                                <div class="d-flex align-items-center">
                                                                    <span class="fz-16 font-weight-bold theme-color"><?php echo $this->common_model->date_convert($gift_details['insert_datetime'], 'd M Y',$this->session->userdata('website_timezone'));?></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 text-right">
                                                                <span>
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="31.573" height="30" viewBox="0 0 31.573 30">
                                                                        <g id="b9e2ee35-d5ff-4898-81b3-d162a56746b9" transform="translate(0 -12.707)">
                                                                            <path id="Path_210841" data-name="Path 210841" d="M30.389,19.027H22.8a4.334,4.334,0,0,0-7.018-4.946,4.334,4.334,0,0,0-7.019,4.946H1.184A1.184,1.184,0,0,0,0,20.211v4.736a1.184,1.184,0,0,0,1.184,1.184H13.813v-7.1H12.629A1.973,1.973,0,1,1,14.6,17.054v1.973H16.97V17.054a1.973,1.973,0,1,1,1.973,1.973H17.76v7.1H30.389a1.184,1.184,0,0,0,1.184-1.184V20.211a1.184,1.184,0,0,0-1.184-1.184Z" transform="translate(0 0)" fill="#fed333"/>
                                                                            <path id="Path_210842" data-name="Path 210842" d="M38.4,281.824a1.184,1.184,0,0,0,1.184,1.184H49.845V268.8H38.4Z" transform="translate(-36.032 -240.301)" fill="#fed333"/>
                                                                            <path id="Path_210843" data-name="Path 210843" d="M288,283.008h10.261a1.184,1.184,0,0,0,1.184-1.184V268.8H288Z" transform="translate(-270.24 -240.301)" fill="#fed333"/>
                                                                        </g>
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <p class="fz-16"><?php echo $gift_details['description']; ?></p>
                                                        <div>
                                                        <?php if($this->uri->segment(1) == 'gifted-enrolled-class-details'){ ?>
                                                        <h5 class="mb-4">Gifted To</h5>
                                                        <?php } else { ?> 
                                                            <h5 class="mb-4">Gifted From</h5>
                                                        <?php } ?>
                                                        <div class="row align-items-center mb-3">
                                                            <div class="col-sm-2">
                                                                <img src="<?php echo $gift_details['profile_image']; ?>" alt="user" class="w-100 rounded">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <h5 class="mb-1"><?php echo $gift_details['name']; ?></h5>
                                                                <p class="mb-0 fz-16">User</p>
                                                            </div>
                                                           </div>
                                                           <a class="d-block text-gray fz-16" href="mailto:Michel_smith@email.com"><?php echo $gift_details['email']; ?></a>
                                                           <a class="d-block text-gray fz-16" href="tel:+1 123-456-7890"><?php echo $gift_details['country_code'].' '.$gift_details['phone']; ?></a>
                                                        </div>

                                                        <!--start-label-->
                                                      <!--   <div class="py-3 px-2 purchase-sec error mt-30 d-flex align-items-center rounded">
                                                            <span class="mr-2">
                                                                <svg id="Iconly_Bold_Danger" data-name="Iconly/Bold/Danger" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                    <g id="Danger" transform="translate(2 3)">
                                                                        <path id="Danger-2" data-name="Danger" d="M17.316,18H2.68a3.128,3.128,0,0,1-.91-.2A2.809,2.809,0,0,1,.219,16.275,2.747,2.747,0,0,1,.21,14.146L7.529,1.433a2.746,2.746,0,0,1,1.1-1.08A2.819,2.819,0,0,1,9.993,0a2.853,2.853,0,0,1,2.484,1.442l7.268,12.615a2.936,2.936,0,0,1,.25,1,2.753,2.753,0,0,1-.73,2.021A2.841,2.841,0,0,1,17.316,18ZM10,12.272a.873.873,0,1,0,0,1.745.877.877,0,0,0,.869-.883A.867.867,0,0,0,10,12.272ZM10,6.09a.872.872,0,0,0-.88.862v2.8a.888.888,0,0,0,.88.873.872.872,0,0,0,.869-.873v-2.8A.867.867,0,0,0,10,6.09Z" fill="#e0a370"/>
                                                                    </g>
                                                                </svg> 
                                                            </span>
                                                            <span>This user has still not enrolled for this class.</span>
                                                        </div> -->
                                                        <!--end-content-->

                                                        <?php if($this->uri->segment(1) == 'gifted-enrolled-class-details'){ ?>
                                                        <?php } else { ?> 
                                                            <div class="d-flex mt-30">
                                                                <a href="<?php echo base_url(); ?>customer/user/remove_giftclass/enroll/<?php echo base64_encode($gift_details['id']); ?>/<?php echo base64_encode($gift_details['class_id']); ?>" class="btn btn__primary btn__rounded">Enroll In This Class</a>
                                                            </div>
                                                            <P class="mt-30  mb-20">Don't want to enroll? Convert this to credit points.</P>
                                                            <div class="d-flex">
                                                                <a href="<?php echo base_url(); ?>customer/user/remove_giftclass/wallet/<?php echo base64_encode($gift_details['id']); ?>/<?php echo base64_encode($gift_details['class_id']); ?>" class="btn btn__primary btn__rounded">Get Credit Points</a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>  
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>