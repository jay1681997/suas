<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
    
                <!--start-body-->
                <section>
                    <div class="container">
                        <div class="contact-panel px-2 px-lg-4 py-4">
                            <!--start-nav-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Sub Profile</li>
                                </ol>
                            </nav>
                            <!--end-nav--> 

                            <div class="container bootstrap snippets bootdey">
                                <div class="row">
                                    <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                    <div class="profile-info col-lg-9">
                                        <div class="shadow"> 
                                            <div class="px-lg-4 px-3 py-3 bio-graph-info text-center">
                                                <!--start-title-->
                                                <div class="title">
                                                    <h4 class="line mb-0 pb-20 position-relative">Student Profiles</h4>
                                                    <p class="mt-0 mb-4 pt-20 fz-16">Edit your student profiles below</p>
                                                </div>
                                                
                                                <!--end-title-->
                                                <?php if(!empty($students)){ ?>
                                                <div class="row justify-content-center">
                                                    <div class="col-lg-10">
                                                        <?php if($this->session->flashdata('success_msg')){ ?>
                                                            <div class="alert alert-success" >
                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                                <?php echo $this->session->flashdata('success_msg')?>
                                                            </div>                
                                                        <?php } ?>
                                                        <?php if($this->session->flashdata('error_msg')){ ?>
                                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                                <?php echo $this->session->flashdata('error_msg')?>
                                                            </div>
                                                        <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                                <?php echo $error_msg; ?>
                                                            </div>
                                                        <?php } ?>
                                                        <ul class="list-unstyled student-list mt-30 mx-3">
                                                            <?php foreach ($students as $key => $value) { ?>
                                                            <li class=" px-2 row py-2 align-items-center shadow rounded-theme mb-4">
                                                                <div class="left col-lg-10 pl-lg-0">
                                                                    <div class="d-lg-flex align-items-center">
                                                                        <div class="stud-img mr-20">
                                                                            <img src="<?php echo S3_BUCKET_ROOT.STUDENT_IMAGE.$value['profile_image']; ?>" alt="user1">
                                                                        </div>
                                                                        <div class="stud-ctn text-left">
                                                                            <h5 class="mb-2"><?php echo $value['username']; ?></h5>
                                                                            <span class="mr-lg-5 mr-2">DOB: <?php echo date('d M Y', strtotime($value['dob'])); ?> </span>
                                                                            <?php if($value['age_category'] == 'Primary'){
                                                                                $age_range = '0-8';
                                                                            }else if($value['age_category'] == 'Juniors'){
                                                                                $age_range = '9-12';
                                                                            }else if($value['age_category'] == 'Teens'){
                                                                                $age_range = '13-17';
                                                                            }else if($value['age_category'] == 'Adults'){
                                                                                $age_range = '18+';
                                                                            }  ?>
                                                                            <span><?php echo $value['age_category'].' '.$age_range; ?></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="right col-lg-2 pr-lg-0">
                                                                    <ul class="list-unstyled d-flex justify-content-between res-center-icon" style="justify-content: center !important;">
                                                                        <li>
                                                                            <a href="<?php echo base_url(); ?>edit-sub-profile/<?php echo base64_encode($value['id']); ?>" class="text-dark">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                                                                    <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"></path>
                                                                                </svg>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:void(0);" class="text-dark romove_modal" data-id="<?php echo $value['id']; ?>">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                                                                    <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                                                                </svg>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div> 
                                                <?php }else{ ?>
                                                    <center><h6>No sub profile list found</h6></center>
                                                <?php } ?>
                                                <!--btn-->
                                                <a href="<?php echo base_url(); ?>add-sub-profile" class="btn btn__primary btn__outlined btn__rounded  fz-16 mt-3"  >+ Add More</a>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end-body--> 
                <!--start-modal-->
                <div class="modal fade" id="remove-info"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content rounded-modal">
                            <div class="modal-header border-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center">
                                <img src="<?php echo base_url(); ?>website_assets/images/myprofile/clendar.png" alt="modal-pop" class="mb-25">
                                <div class="content">
                                    <h4>Delete Student's Profile</h4>
                                    <p class="fz-16 text-dark">All videos will be permanently deleted. Do you still want to delete student's profile?</p>
                                </div>
                            </div>
                            <div class="modal-footer border-0 justify-content-center">
                                <button type="button" class="btn btn__rounded" data-dismiss="modal" aria-label="Close" style="">No</button>
                                <a href="javascript:void(0)" onclick="remove_student();" class="btn btn__rounded" style="color: #ffffff;background-color: #EA241A;"><p id="append_id" style="display: none;"></p>Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end-modal-->   
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script type="text/javascript">
            $('.romove_modal').click(function(){
                $('#remove-info').modal('show');
                var student_id = $(this).data('id');
                $('#append_id').html(student_id);
            })

            function remove_student(){
                var student_id = $('#append_id').html();
                console.log(student_id)
                $.ajax({
                    url:  '<?php echo base_url(); ?>customer/user/delete_student/'+student_id,
                    type: "GET",
                    error: function(jqXHR, textStatus, errorThrown){   
                        swal("Error",errorThrown,"error");
                    },
                    success: function(message){
                        console.log(message)
                        swal("Success",message,"success");
                        window.location.href = '<?php echo base_url(); ?>sub-profile-list';
                    }
                }); // END OF AJAX CALL
            }
        </script>
    </body>
</html>