<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Purchase Promocode</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 

                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-lg-4 px-3 pt-3 pb-5 bio-graph-info text-center">
                                            <!--start-title-->
                                            <div class="title">
                                                <h4 class="line mb-3 pb-20 position-relative">Purchase Promo Codes</h4> 
                                                  <p class="fz-16 mb-4">Promo codes are issued to various
                                                  educational institutions, companies
                                                  and organizations.</p>
                                            </div>
                                            <!--end-title-->  
                                            <?php 
                                                if(!empty($promocodes)){ 
                                                foreach ($promocodes as $key => $promocode) {
                                             ?>
                                            <div class="promo-card mb-30 green">
                                                <div class="main row  w-100 m-auto theme-rounded">
                                                    <div class="col-sm-4 co-img d-flex justify-content-center align-items-center position-relative">
                                                        <div class="bg-shadow">
                                                            <span class="fz-25 font-weight-bold text-white"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$promocode['price'],2); ?></span>
                                                        </div>
                                                        <a href="<?php echo base_url(); ?>purchase-promo-details/<?php echo base64_encode($promocode['id']); ?>" class="link-box"></a>
                                                    </div>
                                                  
                                                    <div class="col-sm-8 content px-5 py-4">
                                                        <div class="">
                                                            <div class="text-sm-right mb-3">
                                                               
                                                                <?php if(!empty($promocode['purchased_promo'])) { ?>
                                                                    <a href="<?php echo base_url(); ?>purchase-promo-details/<?php echo base64_encode($promocode['id']); ?>" class="bg-white px-2 py-1 rounded-pill">Already Purchased</a>
                                                                <?php } else { ?> 

                                                                    <a href="<?php echo base_url(); ?>purchase-promo-details/<?php echo base64_encode($promocode['id']); ?>" class="bg-white px-2 py-1 rounded-pill">Add to Cart</a>
                                                                <?php } ?>
                                                            </div>
                                                            <p class="text-white text-sm-left fz-16"><?php echo $promocode['description']; ?></p>
                                                            <span class="d-block text-sm-right text-white">Valid for <?php echo $promocode['validity']; ?> Days</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } else { ?> 
                                                <h5><center>No promocode found</center></h5>
                                            <?php } ?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body-->  
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>