<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<body>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        
    </style>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section>
            <div class="container">
                <div class="contact-panel px-2 px-lg-4 py-4">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">My profile</li>
                        </ol>
                    </nav>
                    <!--end-nav-->
                    <?php echo form_open('notification-setting', array('method' => 'post', 'enctype' => 'multipart/form-data')); ?>
                    <div class="container bootstrap snippets bootdey">
                        <div class="row">
                            <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                            <div class="profile-info col-lg-9">
                                <div class="shadow">
                                    <div class="px-4 py-4 bio-graph-info">
                                        <?php if ($this->session->flashdata('success_msg')) { ?>
                                            <div class="alert alert-success">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('success_msg') ?>
                                            </div>
                                        <?php } ?>
                                        <?php if ($this->session->flashdata('error_msg')) { ?>
                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('error_msg') ?>
                                            </div>
                                        <?php } ?> <?php if (isset($error_msg) && $error_msg != '') { ?>
                                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $error_msg; ?>
                                            </div>
                                        <?php } ?>
                                        <h1>Notification Setting</h1>

                                        <div class="">
                                            <h1>Push Notification</h1>
                                            <label class="switch">
                                                <input type="checkbox" name="push_notification" <?php if (!empty($notification_setting) && $notification_setting[0]['push_notification'] == 'yes') echo "checked='checked'"; ?>>
                                                <span class="slider round"></span>
                                            </label>
                                            <h1>Email</h1>
                                            <label class="switch">
                                                <input type="checkbox" name="email" <?php if (!empty($notification_setting) && $notification_setting[0]['email'] == 'yes') echo "checked='checked'"; ?>>
                                                <span class="slider round"></span>
                                            </label>
                                            <h1>SMS</h1>
                                            <label class="switch">
                                                <input type="checkbox" name="sms" <?php if (!empty($notification_setting) && $notification_setting[0]['sms'] == 'yes') echo "checked='checked'"; ?>>
                                                <span class="slider round"></span>
                                            </label>

                                        </div>
                                        <!-- <div class="clearfix"></div> -->
                                        <input type="hidden" name="timezone" id="timezone" value="1">
                                        <!--btn-->
                                        <div class="d-flex justify-content-center">
                                            <button class="btn btn__primary btn__rounded">Save</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
        </section>
        <!--end-body-->

        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div>
    <?php include(APPPATH . "views/website/inc/script.php"); ?>

</body>

</html>