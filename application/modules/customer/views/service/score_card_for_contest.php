<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        color: #000000;
        text-align: left;
        padding: 8px;
        /*background-color: #0000FF;*/
    }
    th {
       background-color: #0000FF; 
       color: #FFFFFF;
    }

    tr:nth-child(even) {
        background-color: #89CFF0;
    }
</style>

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section class="">
            <div class="container">
                <div class="contact-panel px-4 py-4  ">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>contest-list">Contest </a></li>
                            <li class="breadcrumb-item active" aria-current="page">Score Card</li>
                        </ol>
                    </nav>
                    <!--end-nav-->

                    <!--start-title-->
                    <div class="title">
                        <h4 class="mb-0 position-relative">Score Card For Contest</h4>
                    </div>
                    <!--end-title-->
                    <br>
                    <table>
                        <tr>
                            <th>Scorecard for Impromptu Speech Contest</th>
                            <th>Points</th>
                        </tr>
                        <?php
                        $total_points = 0;
                        if (!empty($scorecards)) {
                            foreach ($scorecards as $key => $value) {
                                $total_points += $value['points'];
                        ?>
                                <tr>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><?php echo $value['points']; ?></td>
                                </tr>

                            <?php } ?>
                            <tr>
                                <td>Total Points:</td>
                                <td><?php echo $total_points; ?></td>
                            </tr>
                        <?php } ?>

                    </table>

                    <!--  <div class="content pt-30 pb-30">
                            <?php foreach ($scorecards as $key => $value) { ?>
                               
                            <h6 class="mb-0"><?php echo $value['title']; ?></h6>
                            <p class="fz-16 "><?php echo $value['description']; ?></p>
                            <?php } ?>
                        </div> -->
                </div>
            </div>
        </section>
        <!--end-body-->
        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div><!-- /.wrapper -->
    <?php include(APPPATH . "views/website/inc/script.php"); ?>
</body>

</html>