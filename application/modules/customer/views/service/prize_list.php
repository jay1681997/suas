<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-body-->
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Prize List</li>
                            </ol>
                        </nav>
                        <!--end-nav-->

                        <div class="row mb-4">
                            <div class="col-6"></div>
                            <div class="col-6 text-right">
                                <div class="add-speech">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#filter_classess">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                            <g id="Iconly_Bold_Filter" data-name="Iconly/Bold/Filter" transform="translate(24) rotate(90)">
                                                <g id="Filter" transform="translate(2 3)">
                                                    <path id="Filter-2" data-name="Filter" d="M13.122,14.4a3.439,3.439,0,1,1,3.439,3.379A3.41,3.41,0,0,1,13.122,14.4ZM1.508,15.921a1.482,1.482,0,1,1,0-2.963H8.083a1.482,1.482,0,1,1,0,2.963ZM0,3.379A3.409,3.409,0,0,1,3.439,0,3.409,3.409,0,0,1,6.878,3.379,3.409,3.409,0,0,1,3.439,6.758,3.41,3.41,0,0,1,0,3.379ZM11.918,4.86a1.481,1.481,0,1,1,0-2.962h6.575a1.481,1.481,0,1,1,0,2.962Z" transform="translate(0 0)" fill="#17966b"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
             
                        <!--start-title-->
                        <div class="title">
                            <h4 class="mb-0 position-relative">Prize List</h4>
                            <p class="fz-16 mb-4 mt-0">Choose you preferred product</p>
                        </div>
                        <!--end-title-->
            
                        <!--start-row-->
                        <?php if(!empty($prize_list)) {  ?>
                        <div class="row main-list ">
                            <?php foreach ($prize_list as $key => $value) { ?>
                            <div class="col-md-6 col-lg-4 mb-4">
                                <a href="<?php echo base_url(); ?>prize-detail/<?php echo base64_encode($value['id']); ?>" class="speech-box">
                                    <div class="speech-img position-relative">
                                        <img src="<?php echo S3_BUCKET_ROOT.PRIZE_IMAGE.$value['prize_image']; ?>" alt="image1" class="w-100">
                                        <div class="overlay position-absolute w-100">  
                                            <div class="row">
                                                <div class="col-lg-8 mb-lg-0 mb-2">
                                                    <div class="btt-plays">
                                                        <h6 class=" mb-1 mb-2"><?php echo $value['name']; ?></h6>
                                                        <p class="mb-0" style="color: black">
                                                            <?php 
                                                            $description_length=strlen(@$value['description']);
                                                            if($description_length>80){
                                                                echo substr(@$value['description'],0,80-$description_length)."... <b>Read More</b>";
                                                            }else{
                                                                echo @$value['description'];    
                                                            } 
                                                             ?>
                                                        </p>
                                                        <h6 class=" mb-1 mb-2" style="color: #17966B"><?php echo $value['reward_point']; ?> Points</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                            <div class="text-center mb-4" >
                                <img src="<?php echo base_url(); ?>website_assets/images/online_class.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                <h5 class="mb-10">No Prize List Found</h5>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <!--end-body--> 

            <!--start-sucessful-Modal-->
            <div class="modal fade" id="filter_classess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php echo form_open('customer/service/apply_points_filter', array('method' => 'post','id'=>'cardform')); ?>
                        <div class="modal-body p-4">
                            <div class="title">
                                <h4 class="mb-0">Filter</h4>
                                <p class="fz-16">Select points range</p>
                            </div>
                            <div class="check-modal"> 
                            
                                <div class="mb-3">
                                    <input id="radio-1" class="radio-custom" name="points" value="50" type="radio" checked <?php if(!empty($this->session->userdata('points')) && $this->session->userdata('points') == '50')  { echo "checked" ;}  ?>>
                                    <label for="radio-1" class="radio-custom-label mb-0">0-50 Points</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-2" class="radio-custom"name="points" type="radio" value="200" <?php if(!empty($this->session->userdata('points')) && $this->session->userdata('points') == '200')  { echo "checked" ;}  ?>>
                                    <label for="radio-2" class="radio-custom-label mb-0">100-200 Points</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-3" class="radio-custom" name="points" type="radio" value="300" <?php if(!empty($this->session->userdata('points')) && $this->session->userdata('points') == '300')  { echo "checked" ;}  ?>>
                                    <label for="radio-3" class="radio-custom-label mb-0">200-300 Points</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-4" class="radio-custom" name="points" type="radio" value="400" <?php if(!empty($this->session->userdata('points')) && $this->session->userdata('points') == '400')  { echo "checked" ;}  ?>>
                                    <label for="radio-4" class="radio-custom-label mb-0">300-400 Points</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-5" class="radio-custom" name="points" type="radio" value="500" <?php if(!empty($this->session->userdata('points')) && $this->session->userdata('points') == '500')  { echo "checked" ;}  ?>>
                                    <label for="radio-5" class="radio-custom-label mb-0">> 500 Points</label>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 mb-4 text-center">
                            <a href="<?php echo base_url(); ?>customer/service/reset_points_filter" class="btn btn__primary btn__rounded bg-transparent text-dark">Cancel</a>
                            <button type="submit" class="btn btn__primary btn__rounded">Apply</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <!--end-modal-->
        <!--end-modal-->
        <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>

</html>