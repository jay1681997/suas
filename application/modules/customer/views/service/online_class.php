<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-body-->
            <section class="">
                <div class="container">
                    <div class="contact-panel px-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Online Class</li>
                            </ol>
                        </nav>
                        <!--end-nav-->

                        <div class="row mb-4">
                            <div class="col-6"></div>
                            <div class="col-6 text-right">
                                <div class="add-speech">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#filter_classess">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                            <g id="Iconly_Bold_Filter" data-name="Iconly/Bold/Filter" transform="translate(24) rotate(90)">
                                                <g id="Filter" transform="translate(2 3)">
                                                    <path id="Filter-2" data-name="Filter" d="M13.122,14.4a3.439,3.439,0,1,1,3.439,3.379A3.41,3.41,0,0,1,13.122,14.4ZM1.508,15.921a1.482,1.482,0,1,1,0-2.963H8.083a1.482,1.482,0,1,1,0,2.963ZM0,3.379A3.409,3.409,0,0,1,3.439,0,3.409,3.409,0,0,1,6.878,3.379,3.409,3.409,0,0,1,3.439,6.758,3.41,3.41,0,0,1,0,3.379ZM11.918,4.86a1.481,1.481,0,1,1,0-2.962h6.575a1.481,1.481,0,1,1,0,2.962Z" transform="translate(0 0)" fill="#17966b"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
             
                        <!--start-title-->
                        <div class="title">
                            <h4 class="mb-0 position-relative">Online Classes</h4>
                            <!-- <?php if(!empty($classes)) {  ?>
                                <p class="fz-16 mb-4 mt-0">Click on a class below to find out 
                            more information.</p>
                            <?php }else { ?> 
                                <p class="fz-16 mb-4 mt-0">You have registered for the following online classes.</p>
                            <?php } ?> -->
                               <p class="fz-16 mb-4 mt-0">Click on a class below to find out more information</p>
                        </div>
                        <!--end-title-->
            
                        <!--start-row-->
                        <?php if(!empty($classes)) {  ?>
                        <div class="row main-list ">
                            <?php foreach ($classes as $key => $value) { ?>
                            <div class="col-md-6 col-lg-4 mb-4">
                                <a href="<?php echo base_url(); ?>online-class-detail/<?php echo base64_encode($value['id']); ?>" class="speech-box">
                                    <div class="speech-img position-relative bg-overlay-blck">
                                        <img src="<?php echo S3_BUCKET_ROOT.CLASS_IMAGE.$value['class_image']; ?>" width="100px" height="150px" alt="image1" class="w-100">
                                        <div class="overlay position-absolute w-100">  
                                            <div class="row">
                                                <div class="col-lg-8 mb-lg-0 mb-2">
                                                    <div class="btt-plays">
                                                        <h6 class="text-white mb-1 mb-2"><?php echo $value['program_title']; ?></h6>
                                                        <p class="mb-0">
                                                            <span class="mr-2">
                                                                <svg id="Calendar" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 10.496 11.662">
                                                                    <path id="Calendar-2" data-name="Calendar" d="M3,11.662H3a3,3,0,0,1-2.18-.805,3,3,0,0,1-.811-2.18L0,3.821A2.968,2.968,0,0,1,.711,1.778,2.88,2.88,0,0,1,2.628.892V.455A.45.45,0,0,1,2.755.132a.447.447,0,0,1,.758.322V.862l3.42,0V.449A.449.449,0,0,1,7.061.127.447.447,0,0,1,7.82.448V.885a2.9,2.9,0,0,1,1.936.867,2.912,2.912,0,0,1,.733,2.032L10.5,8.7a2.76,2.76,0,0,1-2.99,2.959Zm4.6-3.387a.486.486,0,0,0-.472.5.484.484,0,1,0,.478-.5Zm-4.738,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.473.466h.023a.472.472,0,0,0,.329-.155.479.479,0,0,0,.125-.347.474.474,0,0,0-.478-.466Zm2.372,0h0a.5.5,0,0,0-.471.5.48.48,0,0,0,.474.466h.021a.48.48,0,0,0,.454-.5.474.474,0,0,0-.478-.466Zm-2.375-2.1h0a.5.5,0,0,0-.471.5.479.479,0,0,0,.473.466h.023a.47.47,0,0,0,.328-.155.48.48,0,0,0,.125-.347.474.474,0,0,0-.477-.466Zm2.373-.02h0a.5.5,0,0,0-.472.5.48.48,0,0,0,.475.467h.021a.481.481,0,0,0,.454-.5.474.474,0,0,0-.477-.466Zm2.374,0h0a.482.482,0,0,0-.471.489v.007a.474.474,0,0,0,.479.466h.011a.481.481,0,0,0-.016-.961ZM2.629,1.783A1.79,1.79,0,0,0,.886,3.82V4L9.6,3.99v-.2A1.8,1.8,0,0,0,7.822,1.777v.449a.445.445,0,0,1-.76.319.451.451,0,0,1-.126-.318V1.755l-3.42,0v.471a.446.446,0,0,1-.761.319.451.451,0,0,1-.126-.318V1.783Z" transform="translate(0)" fill="#fff"/>
                                                                </svg>
                                                            </span>
                                                            <span class="text-white"><?php echo $this->common_model->date_convert($value['start_datetime'], 'd M, Y',$this->session->userdata('website_timezone')).' - ',$this->common_model->date_convert($value['end_datetime'], 'd M, Y',$this->session->userdata('website_timezone')); ?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 text-lg-right">
                                                    <span class="d-block text-white mb-2 fz-16"><strong class="mr-2">
                                                        <?php $booked_spots = $this->db->select('*')->from('tbl_order_detail')->where('item_type','class')->where('item_id',$value['id'])->where('is_deleted',"0")->get()->num_rows();
                                                       // echo "<pre>"; print_r($booked_spots);
                                                       //  print_r($value['total_spot']);
                                                        $remaining_spot = $value['total_spot'] - $booked_spots; ?>
                                                    <?php echo $remaining_spot.'/'.$value['total_spot']; ?></strong>  Spots</span>
                                                    <div class="price d-block">
                                                        <span class="text-white font-weight-bold fz-16"><?php echo $this->session->userdata('currency').''.round($this->session->userdata('currency_rate')*$value['price'],2); ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                            <div class="text-center mb-4" >
                                <img src="<?php echo base_url(); ?>website_assets/images/online_class.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                <h5 class="mb-10">No online class yet</h5>
                                <p class="mb-10">You will see your enrolled classes here.</p>
                            </div>
                        <?php } ?>
                        <!--end-row-->

                        <!-- <nav aria-label="Page navigation example">
                            <ul class="pagination d-flex justify-content-center mt-10">
                                <li class="page-item">
                                    <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">1</a></li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">2</a></li>
                                <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav> -->
                    </div>
                </div>
            </section>
            <!--end-body--> 

            <!--start-sucessful-Modal-->
            <div class="modal fade" id="filter_classess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php echo form_open('customer/user/apply_age_filter', array('method' => 'post','id'=>'cardform')); ?>
                        <div class="modal-body p-4">
                            <div class="title">
                                <h4 class="mb-0">Filter</h4>
                                <p class="fz-16">Enter your category below.</p>
                            </div>
                            <div class="check-modal"> 
                            
                                <div class="mb-3">
                                    <input id="radio-1" class="radio-custom" name="age_category" value="Primary" type="radio" checked <?php if(!empty($this->session->userdata('age_category')) && $this->session->userdata('age_category') == 'Primary')  { echo "checked" ;}  ?>>
                                    <label for="radio-1" class="radio-custom-label mb-0">Primary (Age 5-8)</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-2" class="radio-custom"name="age_category" type="radio" value="Juniors" <?php if(!empty($this->session->userdata('age_category')) && $this->session->userdata('age_category') == 'Juniors')  { echo "checked" ;}  ?>>
                                    <label for="radio-2" class="radio-custom-label mb-0">Junior (Age 9-12)</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-3" class="radio-custom" name="age_category" type="radio" value="Teens" <?php if(!empty($this->session->userdata('age_category')) && $this->session->userdata('age_category') == 'Teens')  { echo "checked" ;}  ?>>
                                    <label for="radio-3" class="radio-custom-label mb-0">Teens (Age 13-17)</label>
                                </div>
                                <div class="mb-3">
                                    <input id="radio-4" class="radio-custom" name="age_category" type="radio" value="Adults" <?php if(!empty($this->session->userdata('age_category')) && $this->session->userdata('age_category') == 'Adults')  { echo "checked" ;}  ?>>
                                    <label for="radio-4" class="radio-custom-label mb-0">Adults (Age 18+)</label>
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 mb-4 text-center">
                            <a href="<?php echo base_url(); ?>customer/user/reset_age_filter" class="btn btn__primary btn__rounded bg-transparent text-dark">Cancel</a>
                            <button type="submit" class="btn btn__primary btn__rounded">Apply</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <!--end-modal-->
        <!--end-modal-->
        <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div><!-- /.wrapper -->
        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>

</html>