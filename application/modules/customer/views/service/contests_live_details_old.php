<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
    <div class="wrapper">
        <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section class=" ">
                <div class="container">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>contest-list">Contests </a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contest  Detail</li>
                        </ol>
                    </nav>
                    <!--end-nav-->

                    <!--start-row-->
                    <div class="main-list">
                        <div class="">
                            <div class="inner-side row speech-box">
                                <div class="col-lg-6">
                                    <div class="speech-img position-relative">
                                        <?php $file_ext = explode('.', $contest_details['contest_image']); 
                                    
                                         if($file_ext[4] == 'mp4' || $file_ext[4] == 'MP4'){  ?>
                                           <!--  <video src=" <?php echo $contest_details['contest_image']; ?> " class="w-100"> 
                                            </video> -->
                                               <video width="100%" height="100%" controls>
                                                             <source src="<?php echo $contest_details['contest_image']; ?>" type="video/mp4">
                                                         </video>
                                        <?php }else { ?> 
                                            <img src="<?php echo $contest_details['contest_image']; ?>" alt="image1" class="w-100">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 content px-4 py-4 rounded shadow col-md-0 mt-20 mb-70">
                                    <h4  class="mb-2"><?php echo $contest_details['name']; ?></h4>
                                    <div class="row align-items-center mb-4">
                                        <div class="col-lg-8">
                                            <ul class="dots list-unstyled d-flex mb-1">
                                                <li class="pr-3 fz-16 theme-color font-weight-bold"><?php echo $this->session->userdata('currency').''.$contest_details['price']; ?></li>
                                                <li class="pl-3 fz-16 theme-color"><?php echo ucfirst($contest_details['contest_type']); ?></li>
                                            </ul>
                                            <span class="text-gray">+ Applicable Taxes</span>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <span>Category : </span><span><?php echo $contest_details['age_category']; ?></span>
                                        </div>
                                    </div>   
                                    <p class="text-gray mb-4 fz-16"><?php echo $contest_details['description']; ?></p> 
                                </div>
                            </div>
                            <?php if(!empty($contest_details['prizes'])) { ?>
                            <div class="pt-50 pb-20">
                                <h5 class="mb-2">Prizes:</h5>
                                <p class="fz-16">Winners will be announced on <span class="text-dark"><?php echo $this->common_model->date_convert($contest_details['winner_date'], 'd M Y',$this->session->userdata('website_timezone'));?></span></p>
                                <!--start-row-->
                                <div class="row pt-10">
                                    <?php foreach ($contest_details['prizes'] as $key => $value) {?>
                                    <div class="col-md-6 col-lg-4 mb-lg-0 mb-4">
                                        <div class="pric-main shadow text-center">
                                            <img src="<?php echo $value['prize_image']; ?>" alt="class5" width="100" height="100" class="mb-3">
                                            <h5 class="mb-3"><?php echo $value['prize_name']; ?></h5>
                                            <span class="d-block text-warning"><?php echo $this->session->userdata('currency').''.$value['price']; ?></span>
                                            <span class="d-block text-warning">+ <?php echo $value['credit_points']; ?> points</span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <!--end-row-->
                            </div>
                            <?php } ?>
                            <div class="pt-30 pb-20">
                                <?php if(!empty($contest_details['sponsors'])) { ?> 
                                <h5 class="mb-4">Sponsor Details</h5>
                                <div class="row align-items-center mb-3">
                                    <div class="col-lg-1">
                                        <img src="<?php echo $contest_details['sponsors']['sponsor_logo']; ?>" alt="user" class=" rounded">
                                    </div>
                                    <div class="col-lg-4">
                                        <h5 class="mb-1"><?php echo $contest_details['sponsors']['sponsor_name']; ?></h5>
                                        <p class="mb-0 fz-16">Sponsor contest</p>
                                    </div>
                                </div>
                                <p class="fz-16"><?php echo $contest_details['sponsors']['bio']; ?></p>
                                <?php } ?>
                                <a href="<?php echo base_url(); ?>score-card-for-contest/<?php echo base64_encode($contest_details['id']); ?>" class="shadow px-3 py-3 scored-board d-flex justify-content-between w-50 rounded align-items-center">
                                    <div>
                                        <span class="mr-2">
                                            <svg id="Group_41155" data-name="Group 41155" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                                <path id="Path_208171" data-name="Path 208171" d="M0,0H24V24H0Z" fill="none"/>
                                                <path id="Path_208172" data-name="Path 208172" d="M21,2.992V21.008a1,1,0,0,1-.993.992H3.993A.993.993,0,0,1,3,21.008V2.992A1,1,0,0,1,3.993,2H20.007A.993.993,0,0,1,21,2.992ZM7,4v9l3.5-2L14,13V4Z" fill="#17966b"/>
                                                <path id="Path_208173" data-name="Path 208173" d="M1.631-10.934a1.034,1.034,0,0,1,.728.269.881.881,0,0,1,.292.677.9.9,0,0,1-.292.681,1.024,1.024,0,0,1-.728.273A1.04,1.04,0,0,1,.9-9.307a.889.889,0,0,1-.3-.681.881.881,0,0,1,.292-.677A1.046,1.046,0,0,1,1.631-10.934ZM2.5-3.693H.75V-8.5H2.5Z" transform="translate(15 23.693)" fill="#fff"/>
                                            </svg>
                                        </span>
                                        <span>Scorecard for contest</span>
                                    </div>
                                    <div class=""> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="5.877" height="9.753" viewBox="0 0 5.877 9.753">
                                            <path id="Path_45623" data-name="Path 45623" d="M3.462,6.925,0,3.462,3.462,0" transform="translate(4.877 8.339) rotate(180)" fill="none" stroke="#1f1f1f" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                        </svg> 
                                    </div>
                                </a>
                            </div> 

                            <!--start-label-->
                            <div class="py-3 px-2 purchase-sec mb-4 d-flex align-items-center rounded">
                                <span class="mr-2">
                                    <svg id="_2933094" data-name="2933094" xmlns="http://www.w3.org/2000/svg" width="19.999" height="19.999" viewBox="0 0 19.999 19.999">
                                        <path id="Path_209319" data-name="Path 209319" d="M271,274.957c.674-.165,1.172-.6,1.172-1.1s-.5-.932-1.172-1.1Z" transform="translate(-260.415 -262.108)" fill="#17966b"/>
                                        <path id="Path_209320" data-name="Path 209320" d="M97.445,91a6.445,6.445,0,1,0,6.445,6.445A6.452,6.452,0,0,0,97.445,91Zm2.93,8.2a2.549,2.549,0,0,1-2.344,2.3v.639a.586.586,0,1,1-1.172,0v-.639a2.549,2.549,0,0,1-2.344-2.3.586.586,0,1,1,1.172,0c0,.5.5.932,1.172,1.1V97.978a2.3,2.3,0,1,1,0-4.592v-.633a.586.586,0,0,1,1.172,0v.633a2.549,2.549,0,0,1,2.344,2.3.586.586,0,0,1-1.172,0c0-.5-.5-.932-1.172-1.1V96.9A2.549,2.549,0,0,1,100.375,99.2Z" transform="translate(-87.446 -87.446)" fill="#17966b"/>
                                        <path id="Path_209321" data-name="Path 209321" d="M212.172,182.761c-.674.165-1.172.6-1.172,1.1s.5.932,1.172,1.1Z" transform="translate(-202.758 -175.622)" fill="#17966b"/>
                                        <path id="Path_209322" data-name="Path 209322" d="M10,0A10,10,0,1,0,20,10,10.039,10.039,0,0,0,10,0Zm0,17.616A7.617,7.617,0,1,1,17.616,10,7.626,7.626,0,0,1,10,17.616Z" fill="#17966b"/>
                                    </svg>
                                </span>
                                <span>Participate in contest and get <?php echo $contest_details['total_point']; ?> points.</span>
                            </div>
                            <!--end-label-->
                            <!--start-->
                            <div class="row justify-content-center">
                                <div class="col-lg-3">
                                     <?php $this->session->set_userdata('contest_id', $contest_details['id']); ?>
                                  
                                    <?php if(!empty($order_data)){ ?>
                                        <a href="#" class="btn btn__primary btn__rounded d-flex"><span>Upload Video</span></a>
                                   <?php  }else{ ?>
                                      <a href="<?php echo base_url(); ?>customer/user/add_item_intocart/contest" class="btn btn__primary btn__rounded d-flex"><span>Add To Cart</span></a>
                                    <?php } ?>
                                   
                                  
                                </div>
                            </div>
                            <!--end-row-->
                        </div>
                    </div>
                    <!--end-row-->
                </div>
            </section>
            <!--end-body-->

            <!--start-modal-->
            <div class="modal fade" id="add-to-cart"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94" class="mb-25">
                            <div class="content">
                                <h4>Age Group</h4>
                                <p class="fz-16 text-dark"><span class="text-gray">Please confirm that you are in the </span><br>
                                    Teens Category - Ages 13 to 17?</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0">
                            <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal">No</button>
                            <a href="submission-video-for-contest.html" class="btn btn__primary btn__rounded" id="if-leave">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->
        <?php  include(APPPATH."views/website/inc/footer.php");?>
    </div>
    <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>