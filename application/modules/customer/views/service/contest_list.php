<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<style>
    /* new css */

    .currancy_block {
        display: inline-block;
        background: #15976b;
        border-radius: 50%;
        width: 24px;
        height: 24px;
        line-height: 25px;
        padding: 2px 2px;
        text-align:center;
    }
    .currency_part {
        color: #ffffff;
        font-size: 12px;
        border: 1px solid #fff;
        width: 20px;
        height: 20px;
        border-radius: 50%;
        line-height: 20px;
    }
</style>
<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section class="">
            <div class="container">
                <div class="contact-panel px-4 py-4">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contests</li>
                        </ol>
                    </nav>
                    <!--end-nav-->
                         <div class="row mb-4">
                        <div class="col-6"></div>
                        <div class="col-6 text-right">
                            <div class="add-speech">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#filter_classess">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24">
                                        <g id="Iconly_Bold_Filter" data-name="Iconly/Bold/Filter" transform="translate(24) rotate(90)">
                                            <g id="Filter" transform="translate(2 3)">
                                                <path id="Filter-2" data-name="Filter" d="M13.122,14.4a3.439,3.439,0,1,1,3.439,3.379A3.41,3.41,0,0,1,13.122,14.4ZM1.508,15.921a1.482,1.482,0,1,1,0-2.963H8.083a1.482,1.482,0,1,1,0,2.963ZM0,3.379A3.409,3.409,0,0,1,3.439,0,3.409,3.409,0,0,1,6.878,3.379,3.409,3.409,0,0,1,3.439,6.758,3.41,3.41,0,0,1,0,3.379ZM11.918,4.86a1.481,1.481,0,1,1,0-2.962h6.575a1.481,1.481,0,1,1,0,2.962Z" transform="translate(0 0)" fill="#17966b" />
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--start-nav-->
                    <?php $live_number = 0;
                    $video_number = 0;
                    foreach ($contests as $key => $value) {
                        // print_r($value);
                        if ($value['contest_type'] == 'live') {
                            $live_number = $live_number + 1;
                        }
                        if ($value['contest_type'] == 'video') {
                            $video_number = $video_number + 1;
                        }
                    } ?>

                    <ul class="nav nav-tabs mb-1 forgot-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link border-0 active" data-toggle="tab" href="#profile" role="tab">Live</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link border-0" data-toggle="tab" href="#messages" role="tab">Video</a>
                        </li>
                    </ul>
                    <!--end-nav-->
                    <!--start-content-->
                    <div class="tab-content">
                        <!------------------------------tab1---------------------------------->
                        <div class="tab-pane active" id="profile" role="tabpanel">
                            <p class="fz-16">Click below to see more details about
                                upcoming contests</p>
                            <!--start-row-->

                            <!--  <?php if (!empty($contests)) { ?> -->

                            <div class="row main-list ">

                                <?php foreach ($contests as $key => $value) {
                                            // print_r($value);
                                            if ($value['contest_type'] == 'live') { ?>

                                        <div class="col-md-6 col-lg-4 mb-4">
                                            <a href="<?php echo base_url(); ?>contests-live-details/<?php echo base64_encode($value['id']); ?>" class="speech-box">
                                                <div class="speech-img position-relative bg-overlay-blck">

                                                    <?php $file_ext = explode('.', $value['contest_image']);
                                                    if ($file_ext[1] == 'mp4' || $file_ext[1] == 'MP4') {  ?>
                                                        <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" width="10" height="200" alt="image1" class="w-100">
                                                        <!--   <video width="450" height="100" controls>
                                                                     <source src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" width="50" height="100" type="video/mp4">
                                                                 </video> -->
                                                    <?php } else { ?>
                                                        <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" width="10" height="200" alt="image1" class="w-100">
                                                    <?php  } ?>

                                                    <div class="overlay position-absolute w-100">
                                                        <div class="row align-items-end">
                                                            <div class="col-lg-7 mb-lg-0 mb-2">
                                                                <div class="btt-plays">
                                                                    <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6>
                                                                    <?php if ($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])) { ?>
                                                                        <p class="mb-0">
                                                                            <span class="mr-2 user-img">
                                                                                <img src="<?php echo $value['sponsors']['sponsor_logo']; ?>" alt="Rectangle -6">
                                                                            </span>
                                                                            <span class="text-white">Sponsor By <?php echo $value['sponsors']['sponsor_name']; ?></span>
                                                                        </p>
                                                                    <?php } else { ?>
                                                                        <span class="mr-2 user-img">
                                                                            <img src="<?php echo base_url(); ?>website_assets/images/contests/p1.png" alt="Rectangle -6">
                                                                        </span>
                                                                        <span class="text-white">Sponsor By NA</span>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-5 text-lg-right">
                                                                <div class="price d-block">
                                                                    <span class="text-white font-weight-bold">
                                                                        <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                            <span class="text-white">Live</span>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--start-price-tage-->
                                                <div class="pric-tag-tp">
                                                    <div class="btt-plays">
                                                        <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                            <div class="currancy_block">
                                                                              <div class="currency_part"><?php echo $this->session->userdata('currency'); ?></div>
                                                                            </div>

                                                            <span class="text-white"><?php echo round(($this->session->userdata('currency_rate')*$value['price']),2); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end-price-tage-->
                                            </a>
                                        </div>
                                <?php }
                                        } ?>
                            </div>
                        <?php } else { ?>
                            <!-- <div class="text-center mb-4">
                                <img src="<?php echo base_url(); ?>website_assets/images/contest_live.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                <h5 class="mb-10">No Live Contests Yet</h5>
                                <p class="mb-10">When there is a new contest it will show up here.</p>
                            </div> -->
                        <?php } ?>

                        <!--end-row-->

                        <?php if ($live_number == 0) { ?>
                            <div class="text-center mb-4">
                                <img src="<?php echo base_url(); ?>website_assets/images/contest_live.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                <h5 class="mb-10">No Live Contests Yet</h5>
                                <p class="mb-10">When there is a new contest it will show up here.</p>
                            </div>
                        <?php } ?>
                        </div>

                        <!------------------------------tab2---------------------------------->
                        <div class="tab-pane" id="messages" role="tabpanel">
                            <p class="fz-16">Uploaded videos for contests.</p>
                            <!--start-row-->
                            <?php if (!empty($contests)) { ?>
                                <div class="row main-list ">

                                    <?php foreach ($contests as $key => $value) {

                                        if ($value['contest_type'] == 'video') { ?>
                                            <div class="col-md-6 col-lg-4 mb-4">
                                                <a href="<?php echo base_url(); ?>contests-live-details/<?php echo base64_encode($value['id']); ?>" class="speech-box">
                                                    <div class="speech-img position-relative bg-overlay-blck">
                                                        <!-- <video src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" width="50" height="100" alt="image1" class="w-100"></video> -->
                                                        <?php $file_ext = explode('.', $value['contest_image']);
                                                        if ($file_ext[1] == 'mp4' || $file_ext[1] == 'MP4') {  ?>
                                                            <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['thumb_image']; ?>" width="10" height="200" alt="image1" class="w-100">
                                                            <!-- <video width="350" height="200" controls>
                                                             <source src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" type="video/mp4">
                                                         </video> -->
                                                        <?php } else { ?>
                                                            <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . $value['contest_image']; ?>" width="50" height="100" alt="image1" class="w-100">
                                                        <?php  } ?>

                                                        <div class="overlay position-absolute w-100">
                                                            <div class="row align-items-end">
                                                                <div class="col-lg-7 mb-lg-0 mb-2">
                                                                    <div class="btt-plays">
                                                                        <h6 class="text-white mb-1 mb-2"><?php echo $value['name']; ?></h6>
                                                                        <p class="mb-0">
                                                                            <?php if ($value['contest_by'] == 'Sponsor' && !empty($value['sponsors'])) { ?>
                                                                        <p class="mb-0">
                                                                            <span class="mr-2 user-img">
                                                                                <img src="<?php echo $value['sponsors']['sponsor_logo']; ?>" alt="Rectangle -6">
                                                                            </span>
                                                                            <span class="text-white">Sponsor By <?php echo $value['sponsors']['sponsor_name']; ?></span>
                                                                        </p>
                                                                    <?php } else { ?>
                                                                        <span class="mr-2 user-img">
                                                                            <img src="<?php echo base_url(); ?>website_assets/images/contests/p1.png" alt="Rectangle -6">
                                                                        </span>
                                                                        <span class="text-white">Sponsor By NA</span>
                                                                    <?php } ?>
                                                                    <!--    <span class="mr-2 user-img">
                                                                            <img src="<?php echo base_url(); ?>website_assets/images/contests/1.jpg" alt="Rectangle -6">
                                                                        </span>
                                                                        <span class="text-white">Sponsor By John Doe</span> -->
                                                                    </p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 text-lg-right">
                                                                    <div class="price d-block">
                                                                        <span class="text-white font-weight-bold">
                                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                                <span class="text-white"><?php echo $value['video_duration']; ?></span>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--start-price-tage-->
                                                    <div class="pric-tag-tp">
                                                        <div class="btt-plays">
                                                            <div class="play-time py-1 px-2 mb-2 rounded-pill d-inline-block">
                                                                   <div class="currancy_block">
          <div class="currency_part">₹</div>
        </div>
                                                                <span class="text-white"><?php echo round(($this->session->userdata('currency_rate')*$value['price']),2); ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end-price-tage-->
                                                </a>
                                            </div>
                                    <?php }
                                    } ?>
                                </div>
                            <?php } else { ?>
                                <!--   <div class="text-center mb-4">
                                    <img src="<?php echo base_url(); ?>website_assets/images/contest_video.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                    <h5 class="mb-10">No Video Contests Yet</h5>
                                    <p class="mb-10">When there is a new contest it will show up here.</p>
                                </div> -->
                            <?php } ?>
                            <?php if ($video_number == 0) { ?>
                                <div class="text-center mb-4">
                                    <img src="<?php echo base_url(); ?>website_assets/images/contest_video.png" alt="Mask Group -3" class="mr-2 mb-4 mt-4">
                                    <h5 class="mb-10">No Video Contests Yet</h5>
                                    <p class="mb-10">When there is a new contest it will show up here.</p>
                                </div>
                            <?php } ?>

                            <!--end-row-->
                        </div>
                    </div>
                    <!--end-content-->

                    <!--   <nav aria-label="Page navigation example">
                                <ul class="pagination d-flex justify-content-center mt-10">
                                    <li class="page-item">
                                      <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                    </li>
                                    <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="align-items-center d-flex justify-content-center page-link" href="#">3</a></li>
                                    <li class="page-item">
                                      <a class="align-items-center d-flex justify-content-center page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </li>
                                </ul>
                            </nav> -->
                </div>
            </div>
        </section>
        <!--end-body-->

         <!--start-sucessful-Modal-->
        <div class="modal fade" id="filter_classess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php echo form_open('customer/user/apply_age_filter_contest', array('method' => 'post', 'id' => 'cardform')); ?>
                    <div class="modal-body p-4">
                        <div class="title">
                            <h4 class="mb-0">Filter</h4>
                            <p class="fz-16">Enter your category below.</p>
                        </div>
                        <div class="check-modal">

                            <div class="mb-3">
                                <input id="radio-1" class="radio-custom" name="age_category_contest" value="Primary" type="radio" checked <?php if (!empty($this->session->userdata('age_category_contest')) && $this->session->userdata('age_category_contest') == 'Primary') {
                                                                                                                                        echo "checked";
                                                                                                                                    }  ?>>
                                <label for="radio-1" class="radio-custom-label mb-0">Primary (Age 5-8)</label>
                            </div>
                            <div class="mb-3">
                                <input id="radio-2" class="radio-custom" name="age_category_contest" type="radio" value="Juniors" <?php if (!empty($this->session->userdata('age_category_contest')) && $this->session->userdata('age_category_contest') == 'Juniors') {
                                                                                                                                echo "checked";
                                                                                                                            }  ?>>
                                <label for="radio-2" class="radio-custom-label mb-0">Junior (Age 9-12)</label>
                            </div>
                            <div class="mb-3">
                                <input id="radio-3" class="radio-custom" name="age_category_contest" type="radio" value="Teens" <?php if (!empty($this->session->userdata('age_category_contest')) && $this->session->userdata('age_category_contest') == 'Teens') {
                                                                                                                            echo "checked";
                                                                                                                        }  ?>>
                                <label for="radio-3" class="radio-custom-label mb-0">Teens (Age 13-17)</label>
                            </div>
                            <div class="mb-3">
                                <input id="radio-4" class="radio-custom" name="age_category_contest" type="radio" value="Adults" <?php if (!empty($this->session->userdata('age_category_contest')) && $this->session->userdata('age_category_contest') == 'Adults') {
                                                                                                                                echo "checked";
                                                                                                                            }  ?>>
                                <label for="radio-4" class="radio-custom-label mb-0">Adults (Age 18+)</label>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 mb-4 text-center">
                        <a href="<?php echo base_url(); ?>customer/user/reset_age_filter1" class="btn btn__primary btn__rounded bg-transparent text-dark">Cancel</a>
                        <button type="submit" class="btn btn__primary btn__rounded">Apply</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <!--end-modal-->
        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div>
    <?php include(APPPATH . "views/website/inc/script.php"); ?>
</body>

</html>