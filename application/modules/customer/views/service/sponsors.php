<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>

            <!--start-section-->
            <div class="sponsor-sec">
                <div class="container">
                    <h4 class="mb-50 text-center">More companies we’ve worked with</h4>
                    <ul class="sponsor-logo list-unstyled row text-center">
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/CCA-Logo-768x558.png" alt="CCA-Logo-768x558"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/DN-Post-Logo.png" alt="DN-Post-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Gain-Your-Edge-Logo.png" alt="Gain-Your-Edge-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Halton-District-School-Board-Logo.png" alt="Halton-District-School-Board-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/ITEK-Logo.png" alt="ITEK-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Marina-Commodities_BLUE.png" alt="Marina-Commodities_BLUE"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Miller-Paving-Logo.jpeg" alt="Miller-Paving-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Mississauga_city_logo_2014.svg-2.png" alt="Mississauga_city_logo_2014.svg-2"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Top-Quality-Recruitment.jpg" alt="Top-Quality-Recruitment"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/Toronto-TMS-logo-footer.png" alt="Toronto-TMS-logo-footer"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/ZipTel-Logo.png" alt="ZipTel-Logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/377-3775787_previous-item-rbc-use-royal-bank-of-canada.jpeg" alt="377-3775787_previous-item-rbc-use-royal-bank-of-canada"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/conestoga-logo.png" alt="conestoga-logo"></li>
                        <li class="col-lg-3 col-md-4 col-sm-6 mb-3"><img src="<?php echo base_url(); ?>website_assets/images/sponsor/cropped-TNTLogo_WithNumber_NEW.png" alt="cropped-TNTLogo_WithNumber_NEW"></li>
                    </ul>
                </div>
            </div>
            <!--end-->

            <section class="bg-gray">
                <div class="container">
                    <div class="contact-panel px-4 py-4"> 
                        <!--start-->
                        <div class="contact-us">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="contact_form_inner">
                                        <div class="contact_field">
                                            <h3 class="mb-1">Become a Sponsor</h3>
                                            <p>Want to partner with Stand Up and Speak? Fill out the form to get started!</p>
                                            <form class="mt-40">
                                                <input type="text" class="form-control form-group" placeholder="Your Name" required>
                                                <input type="text" class="form-control form-group" placeholder="Company Name" required>
                                                <input type="text" class="form-control form-group" placeholder="Your Email">
                                                <div class="form-group">
                                                    <i class="icon-phone form-group-icon text-success"></i>
                                                    <input type="text" class="form-control" placeholder="Phone" id="contact-Phone" name="contact-phone" required="" aria-required="true">
                                                </div>
                                                <textarea class="form-control form-group" placeholder="Additional Notes"></textarea>
                                                <div class="d-flex justify-content-center mb-4 mt-4">
                                                    <a href="#" class="btn btn__primary btn__rounded">Send</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="contact_info_sec w-100 mb-30 h-auto">
                                        <h4 class="text-white">Contact Info</h4>
                                        <a href="mailto:app@standupandspeak.com" class="d-flex info_single align-items-center text-white position-relative">
                                            <i class="fas fa-envelope-open-text"></i>
                                            <span class="fz-16">app@standupandspeak.com</span>
                                        </a>
                                        <div class="d-flex info_single align-items-center position-relative">
                                            <i class="fas fa-map-marked-alt"></i>
                                            <span class="fz-16">2285 Dunwin Drive, Unit 7 Mississauga, ON L5L 3S3</span>
                                        </div>
                                    </div>
                                    <div class="">
                                        <iframe class="theme-rounded" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2892.4442870325483!2d-79.67814038427986!3d43.53477986809785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b43c08cfaaaab%3A0xf8ce534a9bce91c5!2s2285%20Dunwin%20Dr%20Unit%207%2C%20Mississauga%2C%20ON%20L5L%203S3%2C%20Canada!5e0!3m2!1sen!2sin!4v1651053886298!5m2!1sen!2sin" width="100%"  height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end--> 
                    </div>
                </div>
            </section>

            <!--start-gallery-->
            <section class="gallery pt-90 pb-90">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
                            <div class="heading text-center mb-40">
                                <h3 class="heading__title">Download Our App</h3>
                                <p class="heading__desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt.
                                </p>
                            </div><!-- /.heading -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                    <div class="row align-items-center">
                        <div class="col-12 col-md-6">
                            <div class="title-wrap pb-0">
                                <div class="title-box">
                                    <h5> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                        tempor.</h5>
                                    <p class="my-4 pr-4 fz-16"> Easy and quick to use</p>
                                    <div class="transpoer-listing-section-app">
                                        <ul>
                                            <li class="fz-16">Lorem ipsum dolor sit amet dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor incididunt ut</li>
                                            <li class="fz-16">Consectetur adipisicing elit tempor incididunt ut labore et dolore magna aliqua</li>
                                            <li class="fz-16">Sed do eiusmod reprehenderit in voluptate velit esse
                                            cillum dolore eu fugiat</li>
                                            <li class="fz-16">tempor incididunt ut labore sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                            <li class="fz-16">Dolore magna aliqua Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia</li>
                                        </ul>
                                    </div>
                                    <div class="dwnld-link justify-content-center">
                                        <a href="#" class="mr-2"><img class="" src="<?php echo base_url(); ?>website_assets/images/app-store.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <picture>
                                <source media="(min-width:650px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png">
                                <source media="(min-width:465px)" srcset="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png"> 
                                <img src="<?php echo base_url(); ?>website_assets/images/about/mobile-app1.png" alt="Flowers" style="width:100%;"> 
                            </picture>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section>
            <!--end-gallery-->
    
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>

        <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>