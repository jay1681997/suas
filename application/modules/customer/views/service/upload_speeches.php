<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
</head>

<!--  <style>
        @media (max-width: 767px) {
            .contact-panel {
                margin-top: 80px !important;
            }
        }
    </style> -->

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>

        <!--start-body-->
        <section class="otp-verif contact-panel px-4 py-4">
            <div class="container">
                <div class=" ">
                    <!--start-nav-->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>my-speeches">Speech</a></li>
                            <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>my-speeches-inner"> Speech Detail</a></li> -->
                            <li class="breadcrumb-item active" aria-current="page">Speech Upload</li>
                        </ol>
                    </nav>
                    <!--end-nav-->
                </div>

                <div class="title mt-4">
                    <h4 class="mb-1 position-relative" style="text-transform:none">Upload a Speech</h4>
                    <p class="mt-0  fz-16">Fill out the information below to help us
                        evaluate your speech.</p>
                </div>
                <div class="alert alert-success" id="start_recording" style="text-align: center;  width:90%; margin: auto;margin-top:1%">

                    Recording is started
                </div>
                <div class="alert alert-success" id="end_recording" style="text-align: center;width:90%; margin: auto;margin-top:1%">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Recording is End
                </div>
                <!--start-form-->
                <?php echo form_open('upload-speeches', array('method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'mt-30 mb-10', 'id' => 'upload_speechform')); ?>
                <div class="">
                    <div class="form-group">
                        <i class="form-group-icon icon-user text-success"></i>

                        <input type="text" class="form-control" id="title" name="title" placeholder="Title" aria-required="true" parsley-trigger="change" data-parsley-errors-container="#title_error" data-parsley-required-message="Please enter title" required value="<?php echo set_value('title'); ?>">
                        <?php echo form_error('title'); ?>
                        <div id="title_error" class="error" style="color: red;"></div>
                    </div>
                </div>

                <div class="">
                    <div class="form-group img-icon">
                        <i class="form-group-icon  text-success">
                            <img src="<?php echo base_url(); ?>website_assets/images/Iconly-Bold-Document.png" alt="Iconly-Bold-Document">
                        </i>
                        <textarea placeholder="Description" name="description" rows="3" class="form-control" data-parsley-required-message="Please enter description" required data-parsley-errors-container="#description_error"><?php echo set_value('description'); ?></textarea>
                        <div id="description_error" class="error" style="color: red;"></div>
                    </div>
                </div>

                <!--end-form-->
                <div id="camera">
                    <video id="video_details" width="100%" height="50%" autoplay></video>
                    <div class="time">
                        <span id="minute">00</span>:
                        <span id="second">00</span>
                        <span id="ms" hidden>00</span>
                    </div>
                    <label id="start-camera">Start Camera</label>
                    <label id="start-record" style="padding-left:5%">Start Recording</label>
                    <label id="stop-record" style="padding-left:1%">Stop Recording</label>
                    <input type="hidden" id="video_duration" name="video_duration" value="00:00">
                </div>
                <!--start-video-->
                <div class="text-left">
                    <div class="file-upload">
                        <div class="image-upload-wrap">
                            <input class="file-upload-input" name="video" id="video" type='file' onchange="readURL(this);"  data-parsley-required-message="Please upload video" required data-parsley-errors-container="#videoeerror" />

                            <div class="drag-text">
                                <span class="  d-block">
                                    <svg id="Iconly_Bold_Upload" data-name="Iconly/Bold/Upload" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 42 42">
                                        <g id="Upload" transform="translate(3.5 3.5)">
                                            <path id="Upload-2" data-name="Upload" d="M7.787,36.365A7.856,7.856,0,0,1,.008,28.815L0,28.441V19.754a7.848,7.848,0,0,1,7.4-7.88l.365-.008h8.384V22.549a1.346,1.346,0,0,0,2.682.187l.013-.187V11.866h8.365a7.856,7.856,0,0,1,7.779,7.549l.008.373V28.46a7.852,7.852,0,0,1-7.4,7.9l-.365.008Zm8.366-24.5V4.619L13.37,7.42a1.353,1.353,0,0,1-1.907,0,1.336,1.336,0,0,1-.148-1.756l.131-.151L16.538.4A1.336,1.336,0,0,1,17.5,0a1.319,1.319,0,0,1,.788.26L18.445.4l5.094,5.109a1.349,1.349,0,0,1-1.758,2.038l-.15-.131-2.783-2.8v7.245Z" fill="#898e95" />
                                        </g>
                                    </svg>
                                </span>
                                <p class="drop-zoon__paragraph">You Can Upload Video</p>
                            </div>
                        </div>
                        <div class="file-upload-content">
                            <video class="file-upload-image"  src="#" id="video_data" name="video_data"><source src="" id="videoPreview"></video>
                            <span class="image-title d-block">Uploaded Image</span>
                            <canvas class="img-responsive img-thumbnail d-none"></canvas>
                            <img class="img-responsive img-thumbnail" id="imagePreview" name="imagePreview" hidden/>
                            <input type="hidden" name="thumb_image" id="thumb_image" style="overflow:auto">
                            <div class="image-title-wrap">
                                <button type="button" onclick="removeUpload()" class="remove-image">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                        <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_error('video'); ?>
                    <div id="videoeerror" style="color: red;"></div>
                    <!-- <?php if (!empty($active_subscription)) { ?>
                        <span class="font-weight-bold mt-3 d-block text-dark mb-2 mt-4">Note:</span>
                        <ul class="pl-20">
                        <li>
                            <p class="mb-0">You have an active subscription of manual valuation,use this to get your video evaluated.</p>
                        <p class="mb-0" style="color:blue;">
                            <?php echo $active_subscription[0]['total_video'] . '/' . ($active_subscription[0]['total_video'] - $active_subscription[0]['total_subscription_video']); ?> Videos remaining    
                        </p>
                    </li>
                </ul>
                   <?php } ?> -->
                    <?php if (!empty($active_subscription)) { ?>
                        <p class="mt-2 mb-4" style="margin-bottom:0.0rem!important"><span class="font-weight-bold mr-2">Note:</span>You have an active subscription of manual valuation,use this to get your video evaluated.</p>
                        <h9 style="color:blue;">
                            <?php echo ($active_subscription[0]['total_video'] - $active_subscription[0]['total_subscription_video']) . '/' . $active_subscription[0]['total_video']; ?> Videos remaining
                        </h9>

                    <?php } ?>
                    <?php $total_count = 10;
                    // echo $active_subscription[0]['total_video'];
                    // echo "<br>";
                    // echo $active_subscription[0]['total_subscription_video'];
                    // echo "<pre>";print_r($active_subscription);
                    $remaining_count = $total_count - $video_counts; ?>
                    <!-- <p class="mt-4 mb-4"><span class="font-weight-bold mr-2">Note:</span>You are uploading you <?php echo $video_counts; ?>th video for evaluation. You have <?php echo $remaining_count; ?> free evaluations remaining.</p> -->
                    <span class="font-weight-bold mt-3 d-block text-dark mb-2 mt-4">Instructions:</span>
                    <ul class="pl-20">
                        <li>
                            <p class="mb-0">Please record video with plain background.</p>
                        </li>
                        <li>
                            <p class="mb-0">Make sure there is no background noise.</p>
                        </li>
                        <li>
                            <p class="mb-0">Look directiy at the camera.</p>
                        </li>
                        <li>
                            <p class="mb-0">Ensure your microphone is working.</p>
                        </li>
                        <li>
                            <p class="mb-0">Maintain a steady distance from the camera.</p>
                        </li>
                    </ul>
                    <p class="mt-2 mb-4"><span class="font-weight-bold mr-2">Note:</span>All AI evaluations are free. Maximum time limit is 2 minutes per video.</p>
                </div>
                <input type="hidden" id="uploading_type" name="uploading_type">
                <!--end-video-->
                <div id="manual_uploading_type">

                    <button type="submit" onclick="speeches_type('Manual');" class="btn btn__primary btn__outlined btn__rounded w-100 fz-16" id="upload_speech">Upload for Manual Evaluation</button>
                </div>
                <br>
                <div id="ai_uploading_type">
                    <!-- <input type="hidden" name="uploading_type" value="AI"> -->
                    <button type="submit" onclick="speeches_type('AI');" class="btn btn__primary btn__outlined btn__rounded w-100 fz-16" id="upload_speech">Upload for AI Evaluation</button>
                </div>
                <!-- <div class="button">
                            <?php if ($active_subscription[0]['evaluation_type'] == 'AI') {
                                $button_name = "Upload for AI evaluation";
                            } else if ($active_subscription[0]['evaluation_type'] == 'Manual') {
                                $button_name = "Upload for manual evaluation";
                            } else {
                                $button_name = "Upload";
                            } ?>
                            <button type="submit" class="btn btn__primary btn__outlined btn__rounded w-100 fz-16" id="upload_speech"><?php echo $button_name; ?></button>
                        </div>  -->
                <?php echo form_close(); ?>
            </div>
        </section>
        <!--end-body-->
        <!--start-modal-->

        <div class="modal fade" id="uploadspeech" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                        <div class="content">
                            <h4 class="mb-2">Speech Uploaded</h4>
                            <p class="fz-16">Your speech has been uploaded
                                for evaluation.</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <a onclick="speech_evaluation_show();" class="btn btn__primary btn__rounded"><span style="color:#FFFFFF">Continue</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="speech_for_evaluation_display" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                        <div class="content">
                            <h4 class="mb-2">Speech For Evaluation</h4>
                            <p class="fz-16">Your speech has successfully been submitted for evaluation.</p>
                        </div>
                    </div>
                    <div id="demo">
                        <!-- <div class="modal-footer border-0 justify-content-center">
                        <a href="<?php echo base_url(); ?>my-speeches" class="btn btn__primary btn__rounded"><span style="color:#FFFFFF">Continue</span></a>
                    </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->
        <div class="modal fade" id="speech_evaluation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/speech/modal.png" alt="modal" class="mb-25">
                        <div class="content">
                            <!-- <h4 class="">You have submitted <?php echo ($videos_count == '') ? 0 : $videos_count; ?> videos for evaluation</h4> -->
                            <h4 class="">Submit For Evaluation</h4>

                            <p class="fz-16">Are you sure you want to submit video</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-around">
                        <a href="<?php echo base_url(); ?>my-speeches" class="btn btn__primary btn__outlined btn__rounded fz-16 border-0 text-gray">No</a>
                        <a onclick="submit_evaluation_speech()" class="btn btn__primary btn__rounded" style="color:#FFFF">Yes</a>
                    </div>
                    <!-- <div class="modal-footer border-0 justify-content-center">
                        <a href="<?php echo base_url(); ?>my-speeches" class="btn btn__primary btn__rounded">Continue</a>
                    </div> -->
                </div>
            </div>
        </div>
        <!--start-modal-->
        <div class="modal fade" id="video_failed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -15.png" alt="modal" class="mb-25">
                        <div class="content">
                            <h4 class="mb-2">Speech Upload Failed</h4>
                            <p class="fz-16">Your speech upload failed.</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-around">
                        <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn__primary btn__outlined btn__rounded fz-16 border-0 text-gray">Cancel</button>
                        <a href="<?php echo base_url(); ?>upload-speeches" class="btn btn__primary btn__outlined btn__rounded fz-16text-white">Retry</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->
        <!--start-modal-->
        <div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -15.png" alt="modal" class="mb-25">
                        <div class="content">
                            <h4 class="mb-2">Video is too long</h4>
                            <p class="fz-16">Maximum 2 minutes allowed for free speech evaluation. Do you want to continue with a paid evaluation</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-around">
                        <a href="<?php echo base_url(); ?>customer/service/delete_speech_uploaded" class="btn btn__primary btn__outlined btn__rounded fz-16 border-0 text-gray">No</a>
                        <a href="<?php echo base_url(); ?>customer/user/add_item_intocart/speech" class="btn btn__primary btn__rounded bg-danger">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->
        <div class="modal fade" id="failed_count" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -15.png" alt="modal" class="mb-25">
                        <div class="content">
                            <h4 class="mb-2">Limit reached</h4>
                            <p class="fz-16">Maximum 10 videos allowed for free evaluation</p>
                        </div>
                    </div>
                    <div class="modal-footer border-0 justify-content-around">
                        <button type="button" data-dismiss="modal" class="btn btn__primary btn__outlined btn__rounded fz-16 border-0 text-gray">No</button>

                        <a href="<?php echo base_url(); ?>dashboard" class="btn btn__primary btn__rounded bg-danger">Yes</a>
                    </div>
                </div>
            </div>
        </div>
        <?php include(APPPATH . "views/website/inc/footer.php"); ?>
    </div><!-- /.wrapper -->

    <?php include(APPPATH . "views/website/inc/script.php"); ?>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.tmpl.min.js"></script>
    <script>
        $("#start_recording").hide();
        $("#end_recording").hide();
        $("#video_details").hide();
        $(".time").hide();
        let camera_button = document.querySelector('#start-camera');
        let video = document.querySelector('#video_details');
        var video_display = document.getElementById("video");
        let start_button = document.querySelector('#start-record');
        let stop_button = document.querySelector('#stop-record');
        let result = document.querySelector('result');
        let video_data = document.querySelector('#video_data');
        let start_time = "";
        let end_time = "";
        var delta = "";
        var days = "";
        var hours = "";
        var minutes = "";
        var seconds = "";
        var timer = 0;
        var timerInterval;
        var ms = document.getElementById('ms');
        var second = document.getElementById('second');
        var minute = document.getElementById('minute');
        var duration = 0;
        var img = $("#imagePreview");
        // let download_link = document.querySelector("#download-video");
        let camera_stream = null;
        let media_recorder = null;
        let blobs_recorded = [];
        let streams = [];
        let res;

        camera_button.addEventListener('click', async function() {
            $("#video_details").show();
            $("#start_recording").hide();
            $("#end_recording").hide();
            $(".time").show();
            try {
                camera_stream = await navigator.mediaDevices.getUserMedia({
                    video: true,
                    audio: true
                });
                video.srcObject = camera_stream;
            } catch (err) {
                // document.getElementById("demo").innerHTML = err.message;
                $("#video_details").hide();
                $(".time").hide();
                alert(err.message);
                console.log(err.message);

            }
        })
        start_button.addEventListener('click', function() {
            $("#start_recording").show();
            $("#end_recording").hide();
            restart();
            start();
            // set MIME type of recording as video/webm
            start_time = new Date();
            media_recorder = new MediaRecorder(camera_stream, {
                // mimeType: 'video\/mp4'
                mimeType: 'video/webm'
            });

            // event : new recorded video blob available 
            media_recorder.addEventListener('dataavailable', function(e) {
                blobs_recorded.push(e.data);
            });

            // event : recording stopped & all blobs sent
            media_recorder.addEventListener('stop', function() {
                $("#start_recording").hide();
                $("#end_recording").show();
                stop();
                end_time = new Date();

                // get total seconds between the times
                delta = Math.abs(end_time - start_time) / 1000;
                // calculate (and subtract) whole days
                days = Math.floor(delta / 86400);
                delta -= days * 86400;
                // calculate (and subtract) whole hours
                hours = Math.floor(delta / 3600) % 24;
                delta -= hours * 3600;
                // calculate (and subtract) whole minutes
                minutes = Math.floor(delta / 60) % 60;
                delta -= minutes * 60;
                // what's left is seconds
                seconds = delta % 60;
                console.log(hours, ':', minutes, ":", seconds);
                if (hours > 0) {
                    document.getElementById("video_duration").value = Math.round(hours) + ':' + Math.round(minutes) + ":" + Math.round(seconds);
                } else {
                    document.getElementById("video_duration").value = Math.round(minutes) + ":" + Math.round(seconds);
                }

                // create local object URL from the recorded video blobs

                const file = new File([blobs_recorded], Date.now() + ".mp4", {
                    type: blobs_recorded.type
                });

                let video_local = URL.createObjectURL(new Blob(blobs_recorded, {
                    // type: 'video/webm'
                    "type": "video\/mp4"
                }));
                let obj = {
                    name: Date.now() + "Reocrding.mp4",
                    url: video_local
                }
                fetch(video_local)
                    .then((res) => res.blob())
                    .then((myBlob) => {
                        console.log("!!!edjklnsmk");
                        myBlob.name = obj.name;
                        myBlob.lastModified = new Date();


                        const last_file = new File([myBlob], Date.now() + ".mp4", {
                            type: myBlob.type
                        });
                        let container = new DataTransfer();
                        container.items.add(last_file);

                        document.querySelector('#video').files = container.files;

                        var reader = new FileReader();
                        video_data.setAttribute('type', 'video/webm');

                        reader.onload = function(e) {

                            $('.image-upload-wrap').hide();

                            $('.file-upload-image').attr('src', obj.url);
                            $('.file-upload-content').show();

                            $('.image-title').html(obj.name);
                            $('.metadata').attr('src', obj.url);

                            var active_subscription = "<?php echo $active_subscription[0]['evaluation_type']; ?>";
                            if (minutes < 2) {
                                $('#ai_uploading_type').show();
                                if (active_subscription == 'Manual') {
                                    $('#manual_uploading_type').show();
                                } else {
                                    $('#manual_uploading_type').hide();
                                }

                            } else {
                                $('#manual_uploading_type').show();
                                $('#ai_uploading_type').hide();
                                // return;
                            }
                            var thumbnail = $("canvas");
                            var input = $("#video");
                            var ctx = thumbnail.get(0).getContext("2d");
                            var last_video = document.querySelector("video");

                            thumbnail[0].width = last_video.videoWidth;
                            thumbnail[0].height = last_video.videoHeight;

                            ctx.drawImage(last_video, 0, 0, last_video.videoWidth, last_video.videoHeight);
                            img.append(img.attr("src", thumbnail[0].toDataURL()));
                            $('#thumb_image').val(thumbnail[0].toDataURL());

                        };

                        reader.readAsDataURL(file);


                    });
            });

            // start recording with each recorded blob having 1 second video
            media_recorder.start(1000);
        });

        stop_button.addEventListener('click', function() {
            $("#start_recording").hide();
            $("#end_recording").show();
            var end_time = new Date();
            media_recorder.stop();
        });
    </script>
    <script>
        function get_subscription() {
            swal({
                title: "Subscription",
                text: "We have different subscription packages available for video evaluations, do you want to get a subscription package now?",
                // type: "success",
                imageUrl: "<?php echo base_url(); ?>website_assets/images/Group41050.png",
                showCancelButton: true,
                confirmButtonColor: "#29cffd",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "<?php echo base_url(); ?>subscription";
                }
            });
        }

        function speeches_type(sp_type) {
            // alert(sp_type);
            document.getElementById("uploading_type").value = sp_type;
        }
        window.onload = function() {
            var active_subscription = "<?php echo $active_subscription[0]['evaluation_type']; ?>"
            var is_display_speech_pop = "<?php echo $total_video_pop; ?>"
            // alert(is_display_speech_pop);
            if (active_subscription == 'Manual') {
                $('#manual_uploading_type').show();
                $('#ai_uploading_type').hide();

            } else {
                // alert(<?php echo $this->session->flashdata('error_msg'); ?> );
                //   alert(<?php echo $this->session->flashdata('success_msg'); ?> );
                <?php if (empty($this->session->flashdata('error_msg')) && empty($this->session->flashdata('error_video_msg')) && empty($this->session->flashdata('error_count_msg')) && empty($this->session->flashdata('success_msg'))) { ?>
                    if (is_display_speech_pop == 3) {
                        this.get_subscription();
                    }
                <?php } ?>

                $('#manual_uploading_type').hide();
                $('#ai_uploading_type').show();
            }
        };
        $(document).ready(function() {
            <?php if ($this->session->flashdata('success_msg')) { ?>
                $('#uploadspeech').modal('show');
            <?php }
            if ($this->session->flashdata('error_video_msg')) { ?>
                $('#video_failed').modal('show');
            <?php }
            if ($this->session->flashdata('error_msg')) { ?>
                $('#failed').modal('show');
            <?php } ?>
            <?php if (!empty($this->session->userdata('error_count_msg'))) { ?>
                // alert("sdf")
                $('#failed_count').modal('show');
            <?php } ?>
        });
    </script>
    <script>
        function speech_evaluation_show() {
            $('#uploadspeech').modal('hide');
            //  alert("speech_evaluation_show");
            $('#speech_evaluation').modal('show');
        }

        function submit_evaluation_speech() {
            // alert("speech_evaluation_show");
            $.ajax({
                url: "customer/service/submit_speech_evaluation",
                type: "GET",
                success: function(data) {
                    // location.reload();
                    console.log(data);
                    var data_id = {
                        "id": data
                    }
                    console.log("!!!sassas!!!");
                    $('#speech_evaluation').modal('hide');

                    $('#speech_for_evaluation_display').modal('show');

                    var speech_hmlt = `
                    <div class="modal-footer border-0 justify-content-center">
                        <a href="<?php echo base_url(); ?>my-evaluations-details/` + data + `" class="btn btn__primary btn__rounded"><span style="color:#FFFFFF">Continue</span></a>
                    </div>

                `;
                    // document.getElementById("speech_for_evaluation_display").innerHMTL = speech_hmlt;
                    // $("#speech_for_evaluation_display").show();
                    $("#demo").html(speech_hmlt);
                    // window.location.href = "<?php echo base_url(); ?>my-speeches";
                },
            });
        }

        function readURL(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                console.log("F1");
                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();
                    console.log("F2");
                    $('.file-upload-image').attr('src', e.target.result);
                    $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    // $("#thumb_image").css("background-image", "url('')");
                    console.log("F3");
                    // $('#thumb_image').val(e.target.result);
                    $('.file-upload-content').show();

                    $('.image-title').html(input.files[0].name);
                    // console.log(input.files[0].name);
                    validateFile(input.files[0]);
                 
                 
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function validateFile(file) {

            var video = document.createElement('video');
            video.preload = 'metadata';

            video.onloadedmetadata = function() {

                window.URL.revokeObjectURL(video.src);
                // console.log(video.duration);
                var active_subscription = "<?php echo $active_subscription[0]['evaluation_type']; ?>"
                if (video.duration < 120) {
                    $('#ai_uploading_type').show();
                    if (active_subscription == 'Manual') {
                        $('#manual_uploading_type').show();
                    } else {
                        $('#manual_uploading_type').hide();
                    }

                    // console.log("Invalid Video! video is less than 1 second");
                    return;
                } else {
                    $('#manual_uploading_type').show();
                    $('#ai_uploading_type').hide();
                    return;
                }
            }

            video.src = URL.createObjectURL(file);
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function() {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function() {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

        $(function() {
            var video = $("#video_data");
            var thumbnail = $("canvas");
            var input = $("#video");
            var ctx = thumbnail.get(0).getContext("2d");
            var duration = 0;
            var img = $("#imagePreview");

            input.on("change", function(e) {
                var file = e.target.files[0];

                console.log(file)
                console.log(file.length);
                console.log(file.type);
                console.log("p1");
                //alert("OUTER VIDEO ");
                // Validate video file type
                // if (["video/mp4","video/mov","video/mpeg","video/quicktime"].indexOf(file.type) === -1) {
                //     alert("Only 'MP4' video format allowed.");
                //     return;
                // }
                // Set video source

                video.find("source").attr("src", URL.createObjectURL(file));
                
                // Load the video
                video.get(0).load();
                console.log(video.get(0));
                // duration = video.get(0).duration;
                //     console.log(duration);
                //     console.log("p2");
                // Load metadata of the video to get video duration and dimensions
                video.on("loadedmetadata", function(e) {
                    duration = video.get(0).duration;
                    console.log(duration);
                    console.log("p2");
                    // if(duration > 1800){
                    //     alert("You have to Upload the video of total time 30 minutes");
                    //     video.find("source").attr("src", '');
                    //     video.get(0).load();
                    //     $("#imagePreview").css("background-image", "url('')");
                    //        return;
                    // }
                    // Set canvas dimensions same as video dimensions
                    thumbnail[0].width = video[0].videoWidth;
                    thumbnail[0].height = video[0].videoHeight;
                    console.log("p3");
                    // Set video current time to get some random image
                    //video[0].currentTime = Math.ceil(duration / 2);
                    video[0].currentTime = 5;
                    // Draw the base-64 encoded image data when the time updates
                    video.one("timeupdate", function() {
                        console.log("p5");
                        ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                        img.append(img.attr("src", thumbnail[0].toDataURL()));
                        $('#thumb_image').val(thumbnail[0].toDataURL());
                    });
                });
            });
        });
        function start() {
            stop();
            timerInterval = setInterval(function() {
                timer += 1 / 60;
                msVal = Math.floor((timer - Math.floor(timer)) * 100);
                secondVal = Math.floor(timer) - Math.floor(timer / 60) * 60;
                minuteVal = Math.floor(timer / 60);
                ms.innerHTML = msVal < 10 ? "0" + msVal.toString() : msVal;
                second.innerHTML = secondVal < 10 ? "0" + secondVal.toString() : secondVal;
                minute.innerHTML = minuteVal < 10 ? "0" + minuteVal.toString() : minuteVal;
            }, 1000 / 60);
        }

        function stop() {
            clearInterval(timerInterval);
        }

        function restart() {
            timer = 0;
        }
    </script>
    <script id="homepagepagination_templatehtml" type="text/x-jQuery-tmpl">
    </script>
</body>

</html>