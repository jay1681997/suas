<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
                <!--start-body-->
                <section class=" ">
                    <div class="container">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>prize-list">Prize List</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Prize Detail</li>
                            </ol>
                        </nav>
                        <!--end-nav-->

                        <!--start-row-->
                        <div class="main-list">
                            <div class="">
                                <div class="align-items-center inner-side row speech-box">
                                    <div class="col-lg-6">
                                        <div class="speech-img position-relative">
                                            <img src="<?php echo S3_BUCKET_ROOT.PRIZE_IMAGE.$prize_details['prize_image']; ?>" alt="class5" class="w-100">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 content px-4 py-4 rounded shadow">
                                        <h4  class="text-gray mb-2"><?php echo $prize_details['name']; ?></h4>
                                        <div class="row align-items-center">
                                            <div class="col-lg-8">
                                                <p><?php echo $prize_details['description']; ?></p>
                                            </div>
                                        </div>
                                        <!--start-remaining-->
                                        <p class="text-gray fz-16 my-4"><span class="theme-color font-weight-bold">
                                            <?php echo $prize_details['reward_point'];?> Points</span></p>
                                        <!--end-remaining-->
                                       
                                        <!--end-label-->
                                        <!--start-->
                                        <div class="row justify-content-center">
                                            
                                            <div class="col-lg-6">
                                                <a href="<?php echo base_url(); ?>customer/service/add_reward_points/<?php echo base64_encode($prize_details['id']); ?>" class="btn btn__primary btn__rounded d-flex"><span>Redeem Now</span></a>
                                            </div>
                                        </div>
                                        <!--end-row-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!--end-row-->
                    </div>
                </section>
                <!--end-body-->

            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
        <?php  include(APPPATH."views/website/inc/script.php");?>
        <script>
            //set button id on click to hide first modal
            $("#if-say-yes").on( "click", function() {
                $('#exampleModalCenter').modal('hide');  
            });
            //trigger next modal
            $("#if-say-yes").on( "click", function() {
                $('#sucessful-evaluation').modal('show');  
            });
        </script>
    </body>
</html>