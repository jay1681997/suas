<!DOCTYPE html>
<html lang="en">
    <head>
        <?php  include(APPPATH."views/website/inc/style.php");?>
    </head>
    <body>
        <div class="wrapper">
            <?php  include(APPPATH."views/website/inc/header.php");?>
            <!--start-body-->
            <section>
                <div class="container">
                    <div class="contact-panel px-2 px-lg-4 py-4">
                        <!--start-nav-->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">My profile</li>
                            </ol>
                        </nav>
                        <!--end-nav--> 
                        <div class="container bootstrap snippets bootdey">
                            <div class="row">
                                <?php $this->load->view('customer/dashboard/left_sidebar'); ?>
                                <div class="profile-info col-lg-9">
                                    <div class="shadow"> 
                                        <div class="px-4 py-4 bio-graph-info">
                                            <h1>Leaderboard</h1>
                                            <div class="table-responsive-sm">
                                                <table class="table table-hover">
                                                    <thead class="mdb-color bg-primary">
                                                        <tr class="text-white">
                                                            <th>#</th>
                                                            <th>First Name</th>
                                                            <th>Country</th>
                                                            <th>Points</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if(!empty($leaderboards)) { foreach ($leaderboards as $key => $value) {?>
                                                        <tr>
                                                            <th scope="row"><?php echo ($key + 1); ?></th>
                                                            <td><?php echo $value['name']; ?></td>
                                                            <td><?php echo $value['country']; ?></td>
                                                            <td><?php echo $value['value']; ?></td>
                                                        </tr>
                                                        <?php }} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end-body--> 
            <?php  include(APPPATH."views/website/inc/footer.php");?>
        </div>
    <?php  include(APPPATH."views/website/inc/script.php");?>
    </body>
</html>