<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Scorecard";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Scorecard Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/scorecard">
                                    Scorecard List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Scorecard Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/scorecard/edit/'.base64_encode($result['id']), array('id'=>'add_scorecard', 'name'=>'add_scorecard', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                               
                                        <div class="row clearfix">
                                            <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Contest<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="contest_id" id="contest_id" parsley-trigger="change" data-parsley-errors-container="#contest_iderror" data-parsley-required-message="Please select contest"  required>
                                                        <?php $contestid = !empty(set_value('contest_id')) ? set_value('contest_id') : $result['contest_id']; ?>
                                                        <option value="">Select Contest</option>
                                                        <?php foreach($contests as $key => $value){ ?> 
                                                            <option value="<?php echo $value['id']; ?>"  <?php echo ($contestid==$value['id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <?php echo form_error('contest_id'); ?>
                                                    <label id="contest_iderror"></label>
                                                </div>
                                            </div> -->
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Title<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('title')) || !empty($result['title']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo !empty(set_value('title')) ? set_value('title') : $result['title']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter scorecard title" data-parsley-errors-container="#error" required>
                                                        
                                                    </div>
                                                    <?php echo form_error('title'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Points<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('points')) || !empty($result['points']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="points" id="points" value="<?php echo !empty(set_value('points')) ? set_value('points') : $result['points']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter scorecard points" data-parsley-errors-container="#error" required>
                                                        
                                                    </div>
                                                    <?php echo form_error('points'); ?>
                                                </div>
                                            </div>
                                         <!--    <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description') || @$result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" style="height: 80px" data-parsley-required-message="Please write description" required><?php echo !empty(set_value('description')) ? set_value('description') : $result['description']; ?></textarea>
                                                        <label class="form-label">Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
 -->                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/scorecard" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
            });
        </script>
    </body>
</html>