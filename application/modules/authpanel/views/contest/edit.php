<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Contest";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Contest Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/contest">
                                    Contest List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Contest Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/contest/edit/'.base64_encode($result['id']),array('id'=>'add_contest', 'name'=>'add_contest', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div id="video_data" class="form-group">
                                            <div class="text-center preview_holder" >
                                                <div class="row">
                                                    <div class="col-6">
                                                        <video width="400" controls class="img-responsive img-thumbnail" id="videoss">
                                                            <source src="<?php echo (!empty($result['contest_image'])) ? S3_BUCKET_ROOT.CONTEST_IMAGE.$result['contest_image'] : "" ; ?>" id="videoPreview">
                                                        </video>
                                                        <canvas class="img-responsive img-thumbnail d-none"></canvas>        
                                                    </div>
                                                    <div class="col-6">
                                                        <img class="img-responsive img-thumbnail" id="imagePreview" style="height:100px;width: 100px;"/>
                                                        <input type="hidden" name="thumb_image" id="thumb_image">
                                                        <label id="thumb_image_error"></label>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="text-center">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Contest Video <input type="file" id="contest_image" name="contest_image" class="filestyle" data-parsley-trigger="change"  data-parsley-errors-container="#contest_error" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="contest_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="image_data" class="form-group">
                                        <div class="text-center preview_holder" >
                                                <img id="imagePreview_image" width="200" height="200" class="img-responsive img-thumbnail"/>
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file" >
                                                            Upload Contest Image <input type="file" id="contest_image_image" name="contest_image_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png,  image/jpg" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Contest Type<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="contest_type" onchange="hide_show(this.value)" id="contest_type" parsley-trigger="change" data-parsley-errors-container="#contest_typeerror" data-parsley-required-message="Please select contest type"  required>
                                                        <?php $contest_type = !empty(set_value('contest_type')) ? set_value('contest_type') : $result['contest_type'];?>
                                                        <option value="">Select Contest Type</option>
                                                        <option value="live" <?php echo ($contest_type=='live') ? 'selected="selected"' : ''; ?>>Live</option>
                                                        <option value="video" <?php echo ($contest_type=='video') ? 'selected="selected"' : ''; ?>>Video</option>
                                                    </select>
                                                    <?php echo form_error('contest_type'); ?>
                                                    <label id="contest_typeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Name<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('name')) || !empty($result['name']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo !empty(set_value('name')) ? set_value('name') : $result['name']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter name"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Total Participants <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('total_participant')) || !empty($result['total_participant']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_participant" id="total_participant" value="<?php echo !empty(set_value('total_participant')) ? set_value('total_participant') : $result['total_participant']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total participant number" data-parsley-errors-container="#totalparticipanterr" required  onkeypress="return isNumberKey1(event);">
                                                        
                                                    </div>
                                                    <?php echo form_error('total_participant'); ?>
                                                    <label id="totalparticipanterr"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Total Points (purchasing Time)<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('total_point')) || !empty($result['total_point']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_point" id="total_point" value="<?php echo !empty(set_value('total_point')) ? set_value('total_point') : $result['total_point']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total point number" data-parsley-errors-container="#totalpointerr" required  onkeypress="return isNumberKey1(event);">
                                                       
                                                    </div>
                                                    <?php echo form_error('total_point'); ?>
                                                    <label id="totalpointerr"></label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Contest Start Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('contest_startdate')) || !empty($result['contest_startdate']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="contest_startdate" name="contest_startdate" data-parsley-trigger="change" data-parsley-required-message="Please select contest start datetime" required value="<?php echo !empty(set_value('contest_startdate')) ? set_value('contest_startdate') : date('d-m-Y H:i', strtotime($result['contest_startdate'])); ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('contest_startdate'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Contest End Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('contest_enddate')) || !empty($result['contest_enddate']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="contest_enddate" name="contest_enddate" data-parsley-trigger="change" data-parsley-required-message="Please select contest end datetime" required value="<?php echo !empty(set_value('contest_enddate')) ? set_value('contest_enddate') : date('d-m-Y H:i', strtotime($result['contest_enddate'])); ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('contest_enddate'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Price <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo (!empty(set_value('price')) || !empty($result['price']) ? 'focused' : '');?>">
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo !empty(set_value('price')) ? set_value('price') : $result['price']; ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" required>
                                                            <!-- <label class="form-label">Price <span class="text-danger">*</span></label> -->
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Age Category<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="age_category" id="age_category" parsley-trigger="change" data-parsley-errors-container="#age_categoryerror" data-parsley-required-message="Please select age category"  required>
                                                        <?php $age_categorys = !empty(set_value('age_category')) ? set_value('age_category') : $result['age_category']; ?>
                                                        <option value="">Select Age Category</option>
                                                        <option value="Adults" <?php echo ($age_categorys=='Adults') ? 'selected="selected"' : ''; ?>>Adults</option>
                                                        <option value="Teens" <?php echo ($age_categorys=='Teens') ? 'selected="selected"' : ''; ?>>Teens</option>
                                                        <option value="Juniors" <?php echo ($age_categorys=='Juniors') ? 'selected="selected"' : ''; ?>>Juniors</option>
                                                        <option value="Primary" <?php echo ($age_categorys=='Primary') ? 'selected="selected"' : ''; ?>>Primary</option>
                                                    </select>
                                                    <?php echo form_error('age_category'); ?>
                                                    <label id="age_categoryerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Winner Date<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('winner_date')) || !empty($result['winner_date']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="winner_date" name="winner_date" data-parsley-trigger="change" data-parsley-required-message="Please select winner date" required value="<?php echo !empty(set_value('winner_date')) ? set_value('winner_date') : date('d-m-Y', strtotime($result['winner_date'])); ?>">
                                                    </div>
                                                    <?php echo form_error('winner_date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Contest<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="contest_by" id="contest_by" parsley-trigger="change" data-parsley-errors-container="#contest_byerror" data-parsley-required-message="Please select contest"  required>
                                                        <?php $contest_by = !empty(set_value('contest_by')) ? set_value('contest_by') : $result['contest_by']; ?>
                                                        <option value="">Select Contest</option>
                                                        <option value="Normal" <?php echo ($contest_by=='Normal') ? 'selected="selected"' : ''; ?>>Normal</option>
                                                        <option value="Sponsor" <?php echo ($contest_by=='Sponsor') ? 'selected="selected"' : ''; ?>>Sponsor</option>
                                                    </select>
                                                    <?php echo form_error('contest_by'); ?>
                                                    <label id="contest_byerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label text-left">
                                                        Select Sub Admin <b style="color: #000;">(As Host)</b>
                                                        <!-- <span class="text-danger">*</span> -->
                                                    </label>
                                                    <?php $subadmin_id = !empty(set_value('subadmin_id')) ? set_value('subadmin_id') : $result['subadmin_id']; ?>
                                                    <select class="form-control select2" id="subadmin_id" name="subadmin_id" data-parsley-trigger="change" data-parsley-required-message="Please select sub admin" data-parsley-errors-container="#subadmin_iderror" >
                                                        <option value="">Select Sub Admin</option>
                                                        <?php $subadmin_id = !empty(set_value('subadmin_id')) ? set_value('subadmin_id') : $result['subadmin_id']; ?>
                                                        <?php if(!empty($subadmins)){ foreach ($subadmins as $key => $value) { ?>
                                                            <option value="<?php echo $value['id']; ?>" <?php echo ($subadmin_id==$value['id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('subadmin_id'); ?>
                                                    <label id="subadmin_iderror"></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="form-label text-left">
                                                Select Sponsor
                                                <!-- <span class="text-danger">*</span> -->
                                            </label>
                                            <select class="form-control select2" id="sponsor_id" name="sponsor_id" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#sponsor_iderror">
                                                <option value="">Select Sponsor</option>
                                                <?php $sponsor_id = !empty(set_value('sponsor_id')) ? set_value('sponsor_id') : $result['sponsor_id']; ?>
                                                <?php if (!empty($sponsor)) {
                                                    foreach ($sponsor as $key => $value) { ?>
                                                        <option data-sort_code="<?php echo $value['id']; ?>" value="<?php echo $value['id']; ?>" <?php echo ($sponsor_id == $value['id']) ? 'selected="selected"' : ''; ?>><?php echo $value['sponsor_name']; ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                            <?php echo form_error('sponsor_id'); ?>
                                            <label id="sponsor_iderror"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="form-label text-left">Scorecard <span class="text-danger"></span></label>
                                            <?php $scorecard_id = !empty(set_value('scorecard_id')) ? set_value('scorecard_id') :  explode(',', $result['scorecard_id']); ?>
                                            <select class="form-control select2" name="scorecard_id[]" id="scorecard_id" data-parsley-errors-container="#scorecard_id_error" data-parsley-trigger="change" onchange="validate_field('scorecard_id')" data-parsley-required-message="Please select scorecard name" multiple>
                                                <?php if (!empty($scorecard_list)) {
                                                    foreach ($scorecard_list as $key => $value) { ?>
                                                        <option value="<?php echo $value['id']; ?>" <?php echo (in_array($value['id'], $scorecard_id)) ? 'selected' : '' ?>><?php echo $value['title']; ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                            <?php echo form_error('scorecard_id'); ?>
                                            <label id="stop_ids_error"></label>
                                        </div>
                                    </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description')) || !empty($result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" style="height: 100px" data-parsley-required-message="Please write description" required><?php echo !empty(set_value('description')) ? set_value('description') : $result['description']; ?></textarea>
                                                        <label class="form-label">Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>                        
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/contest"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/momentjs/moment.js"></script>
        <script type="text/javascript">
             window.onload = function() {
            var contest_type = $("#contest_type").val(); 
            // alert(contest_type);
            if (contest_type == 'video') {
                $("#image_data").hide();
                $("#video_data").show();
            }else{
                $("#video_data").hide();
                $("#image_data").show();
            }
            // document.getElementById("contest_image_image").required = true;
        };

        function hide_show($type_name) {
            // alert($type_name);
            var video = $("video");
            if ($type_name == 'video') {
                $("#image_data").hide();
                $("#video_data").show();
                $("#imagePreview").css("background-image", "url('')");
                $("#videoPreview").css("background-image", "url('')");
                document.getElementById('contest_image').value = '';
                document.getElementById('contest_image_image').value = '';
                document.getElementById('videoss').value = "";
                video.find("source").attr("src", '');
                video.get(0).load();
                // img.append(img.attr("src", ''));
                // document.getElementById("contest_image").required = true;
                // document.getElementById("contest_image_image").required = false;
            } else {
                $("#video_data").hide();
                $("#image_data").show();
                $("#imagePreview").css("background-image", "url('')");
                $("#videoPreview").css("background-image", "url('')");
                document.getElementById('contest_image').value = '';
                document.getElementById('contest_image_image').value = '';
                document.getElementById('videoss').value = "";
                document.getElementById('thumb_image').value = "";
                video.find("source").attr("src", '');
                video.get(0).load();
                // img.append(img.attr("src", ''));
                // document.getElementById("contest_image").required = false;
                // document.getElementById("contest_image_image").required = true;
            }
        }
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                });   
                var url =    '<?php echo S3_BUCKET_ROOT.CONTEST_IMAGE.$result['thumb_image']; ?>';        
                $("#imagePreview").css("background-image", "url("+url+")");
                  var url_image =    '<?php echo S3_BUCKET_ROOT.CONTEST_IMAGE.$result['contest_image']; ?>';        
                $("#imagePreview_image").css("background-image", "url("+url_image+")");
                console.log(url);                
                $("#imagePreview").css("background-size", "100% 100%");  
                $("#imagePreview_image").css("background-size", "100% 100%");  
                $("#thumb_image").css("background-image", "url("+url+")");
                var contest_startdate = $("#contest_startdate").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: "today",
                    onClose: function(selDate,dateStr){
                        contest_enddate.set('minDate',dateStr);
                    },
                });
                var contest_enddate = $("#contest_enddate").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: moment().format("YYYY-MM-DD HH:MM"),
                    onClose: function(selDate,dateStr){
                        contest_startdate.set('maxDate',dateStr);
                    }
                });
                var winner_date = $("#winner_date").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y",
                    minDate: moment().format("DD-MM-YYYY"),
                    onClose: function(selDate,dateStr){
                        end_date.set('minDate',dateStr);
                    },
                });
            });
         
            function readURL(input) {
                var fileTypes = ['jpeg','jpg','png','gif','tiff'];  //acceptable file types
                if (input.files && input.files[0]) {
                    var video = $("video");
                    var thumbnail = $("canvas");
                    var img = $("#imagePreview");
                    
                    var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                        isImages = fileTypes.indexOf(extension) > -1; 
                  
                    if(isImages)
                    {
                        video.find("source").attr("src", '');
                        video.get(0).load();
                        img.append(img.attr("src", ''));
                    }
                     

                    var reader = new FileReader();
 
                    //videoPreview
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#contest_image").change(function () {
                readURL(this);
            });

            function readURL_image(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview_image").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#contest_image_image").change(function () {
                readURL_image(this);
            });

            function validate_dropdown(){
                let country_code = $("#country_code").val();
                if(country_code != '' && country_code.length > 0){
                    $("#countrycodeerror").text('');
                } else {
                    $("#country_code").attr('required','required');
                }   
            }

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
         <script type="text/javascript">
            $(function() {
                var video = $("video");
                var thumbnail = $("canvas");
                var input = $("#contest_image");
                var ctx = thumbnail.get(0).getContext("2d");
                var duration = 0;
                var img = $("#imagePreview");

                input.on("change", function(e) {
                    var file = e.target.files[0];

                    console.log(file)
                    console.log(file.length)

                    //alert("OUTER VIDEO ");
                    // Validate video file type
                    // if (["video/mp4"].indexOf(file.type) === -1) {
                    //     //alert("Only 'MP4' video format allowed.");
                    //     return;
                    // }
                    // Set video source

                    video.find("source").attr("src", URL.createObjectURL(file));

                    // Load the video
                    video.get(0).load();
                    // Load metadata of the video to get video duration and dimensions
                    video.on("loadedmetadata", function(e) {
                        duration = video.get(0).duration;
                        // Set canvas dimensions same as video dimensions
                        thumbnail[0].width = video[0].videoWidth;
                        thumbnail[0].height = video[0].videoHeight;
                        // Set video current time to get some random image
                        //video[0].currentTime = Math.ceil(duration / 2);
                        video[0].currentTime = 5;
                        // Draw the base-64 encoded image data when the time updates
                        video.one("timeupdate", function() {
                            ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                            img.append(img.attr("src", thumbnail[0].toDataURL()));
                            $('#thumb_image').val(thumbnail[0].toDataURL());
                        });
                    });
                });
            });
        </script>
    </body>
</html>