<!doctype html>
<html class="no-js " lang="en">

<head>
    <?php $data['title'] = "Contest Details";
    $this->load->view('authpanel/common/stylesheet', $data);  ?>
</head>
<style>
    table {
        border-collapse: collapse;
    }

    th {
        background: #ccc;
    }

    th,
    td {
        border: 1px solid #ccc;
        padding: 8px;
    }

    tr:nth-child(even) {
        background: #efefef;
    }

    tr:hover {
        background: #d1d1d1;
    }

    .xyz {
        position: relative;
        top: 100px;
        right: 20%;
        height: 220px;
        overflow: hidden;
        overflow-y: auto;
        border: 1px solid #000;
    }

    .xyz table {
        width: 100%;
    }

    xyz {
        -ms-overflow-style: none;/ for Internet Explorer,
        Edge / scrollbar-width: none;/ for Firefox / overflow-y: scroll;
    }

    xyz::-webkit-scrollbar {
        display: none;/ for Chrome,
        Safari,
        and Opera /
    }
</style>
<style type="text/css">
    .player {
        width: 280px !important;
        height: 200px !important;
        margin-bottom: 20px !important;
        margin: 10px 20px !important;
        margin-left: unset !important;
        border-radius: 20px;
    }

    .center_btn_container {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        max-width: 100%;
    }

    .center_btn_container>button.btn-primary {
        margin-right: 10px;
    }

    .center_btn_container>button.btn-primary:last-child {
        margin-right: 0;
    }

    .player>div {
        border-radius: 20px;
    }

    @media (max-width: 767px) {
        .player {
            width: 100%;
        }
    }
</style>

<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/css/agora.css">
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Contest Detail
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/dashboard"><i class="zmdi zmdi-home"></i>
                                Dashboard </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/contest">Contest List</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a>Contest Detail</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <!-- View -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12">
                            <div class="card member-card">
                                <div class="header l-red">
                                    <div class="col-12 text-left">
                                        <a href="javascript:void(0)" onclick="history.go(-1)"
                                            class="btn btn-raised waves-effect waves-float waves-red text-left"><i
                                                class="zmdi zmdi-arrow-left"></i></a>
                                    </div>
                                    <div class="col-12 text-center">
                                        <img src="<?php echo S3_BUCKET_ROOT . CONTEST_IMAGE . @$result['contest_image']; ?>"
                                            class="rounded-circle" alt="profile-image" width="150" height="150px">
                                        <h4 class="m-t-5">
                                            <?php echo @$result['name']; ?>
                                        </h4>
                                        </li>
                                    </div>
                                </div>
                                <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                <div class="card">
                                    <div class="row text-left">
                                        <div class="col-md-10">
                                            <div class="member-img m-t-5">
                                                <form id="join-form" name="join-form">
                                                    <div class="row join-info-group">
                                                        <div class="col-sm">
                                                            <input id="appid" type="hidden" placeholder="enter appid"
                                                                value="<?php echo AGORA_APP_KEY; ?>">
                                                            <input id="token" type="hidden" placeholder="enter token"
                                                                value="<?php echo $token; ?>">
                                                            <input id="channel" type="hidden"
                                                                placeholder="enter channel name"
                                                                value="<?php echo $channel; ?>">
                                                            <input id="accountName" type="hidden"
                                                                placeholder="enter channel name"
                                                                value="<?php echo $accountName; ?>">
                                                            <input id="contest_id" type="hidden"
                                                                placeholder="enter channel name"
                                                                value="<?php echo $contest_id; ?>">
                                                            <!-- <input id="uid" type="hidden" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onafterpaste="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="Enter the user ID" value="0"> -->
                                                            <input id="uid" type="hidden"
                                                                placeholder="Enter the user ID"
                                                                value="<?php echo $id; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-12">
                                                        <div class="button-group text-center">
                                                            <button id="host-join" type="submit"
                                                                class="btn btn-primary btn-sm">START</button>
                                                            <button id="leave" name="leave" type="button"
                                                                class="btn btn-primary btn-sm leave_contest"
                                                                disabled>Leave</button>
                                                            <button id="mute-audio" type="button"
                                                                class="btn btn-primary btn-sm"
                                                                style="height:  50px;min-width: 50px !important;margin-left: 30px;border-radius: 50%;background-color: #a3a6a3;"><i
                                                                    class='zmdi zmdi-mic-outline'></i></button>
                                                            <button id="mute-video" type="button"
                                                                class="btn btn-primary btn-sm"
                                                                style="height:  50px;margin-left: 30px;min-width: 50px !important;margin-left: 18px;border-radius: 50%;background-color: #a3a6a3;"><i
                                                                    class='zmdi zmdi-videocam'></i></button>
                                                            <button id="change_view-0" type="button"
                                                                class="btn btn-primary btn-sm ChangeView"
                                                                style="height:  50px;margin-left: 15px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i
                                                                    class='zmdi zmdi-eye' id="iconview-0"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- Single button -->

                                            <div class="row video-group justify-content-center">
                                                <div class="col-6 col-lg-4 col-md-4">
                                                    <p id="local-player-name" class="player-name text-center"></p>
                                                    <div id="local-player" class="player"
                                                        style="  display: block;margin: 0 auto !important;text-align: center">
                                                    </div>
                                                    <!-- <button id="mute-audio" type="button" class="btn btn-primary btn-sm" style="min-width: 50px !important;margin-left: 100px;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-mic-outline'></i></button>
                                        <button id="mute-video" type="button" class="btn btn-primary btn-sm" style="min-width: 50px !important;margin-left: 18px;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-videocam'></i></button> -->
                                                </div>

                                                <!--  <div class="card">
                                        <div class="row text-left">
                                            <div class="col-md-10"> -->
                                                <div class="w-100"></div>
                                                <div class="col-6 col-lg-12 col-md-12">
                                                    <div id="remote-playerlist"
                                                        style="display: flex;justify-content: center;align-items: center;flex-wrap: wrap;margin-bottom: 30px;">
                                                    </div>
                                                    <!-- <div id="insert-all-users"></div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="xyz">
                                                <table id="myTable" style="width:100%">
                                                    <tr>
                                                        <td colspan="2" style="position: sticky;
    top: 0;
    background: #ffffff;
    z-index: 12;">Block User List</td>
                                                    </tr>
                                                    <tr style=" position: sticky;
    top: 38px;
    background: #ffffff;
    z-index: 12;">
                                                        <td>Name</td>
                                                        <td>Unblock</td>
                                                    </tr>
                                                    <tbody id="appendhomeviewmoredata">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- End View -->
        </div>
        </div>
        </div>
    </section>


    <!-- Jquery Core Js -->
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script src="https://download.agora.io/sdk/release/AgoraRTC_N.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/agora-rtm-sdk@1.3.1/index.js"></script>
    <script src="https://cdn.agora.io/rtmsdk/release/AgoraRTMTokenBuilder-1.4.0.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.tmpl.min.js"></script>

    <script id="block_list_templatehtml" type="text/x-jQuery-tmpl">
        <tr>
    <td>${username}</td>
    <td><button class="btn btn-primary btn-sm remoteremove removeon" onclick="Ublock_user(${id})"><i class='zmdi zmdi-close'></i></button></td>
  </tr>
</script>

    <script type="text/javascript">
        var token = undefined;
        // var remoteUsers = [];
        var user_ids = [];
        // create Agora client
        var client = AgoraRTC.createClient({
            mode: "live",
            codec: "vp8"
        });
        // let token = null;
        // const rtmUid = String(Math.floor(Math.random() * 2032))
        const rtmUid = '<?php echo $id; ?>';
        var localTracks = {
            videoTrack: null,
            audioTrack: null
        };
        var localTrackState = {
            videoTrackMuted: false,
            audioTrackMuted: false
        }
        var remoteUsers = {};
        var rtmClient;
        // rtmClient = AgoraRTM.createInstance('<?php echo AGORA_APP_KEY; ?>');
        // Agora client options
        let initRtm = async (name) => {
            rtmClient = AgoraRTM.createInstance('<?php echo AGORA_APP_KEY; ?>');
            await rtmClient.login({
                'uid': rtmUid,
                'token': token
            })
            Channel = rtmClient.createChannel('<?php echo $channel; ?>');
            await channel.join();
        }
        var options = {
            appid: '<?php echo AGORA_APP_KEY; ?>',
            channel: '<?php echo $channel; ?>',
            uid: '<?php echo $id; ?>',
            token: '<?php echo $token; ?>',
            role: "host", // host or audience
            audienceLatency: 2
        };

        // the demo can auto join channel with params in url
        $(() => {
            var urlParams = new URL(location.href).searchParams;
            options.appid = urlParams.get("appid");
            options.channel = urlParams.get("channel");
            options.token = urlParams.get("token");
            options.uid = urlParams.get("uid");
            if (options.appid && options.channel) {
                $("#uid").val(options.uid);
                $("#appid").val(options.appid);
                $("#token").val(options.token);
                $("#channel").val(options.channel);
                $("#join-form").submit();
            }
        })

        $("#host-join").click(function (e) {
            options.role = "host"
        })

        $("#lowLatency").click(function (e) {
            options.role = "audience"
            options.audienceLatency = 1
            $("#join-form").submit()
        })

        $("#ultraLowLatency").click(function (e) {
            options.role = "audience"
            options.audienceLatency = 2
            $("#join-form").submit()
        })

        $("#join-form").submit(async function (e) {
            // console.log("VERSION",AgoraRTC.VERSION);
            e.preventDefault();
            $("#host-join").attr("disabled", true);
            $("#audience-join").attr("disabled", true);
            // RTMJoin();
            try {
                options.appid = $("#appid").val();
                options.token = $("#token").val();
                options.channel = $("#channel").val();
                // options.uid = Number($("#uid").val());
                options.uid = Number($("#uid").val());
                await join();
                if (options.role === "host") {
                    $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                    if (options.token) {
                        $("#success-alert-with-token").css("display", "block");
                    } else {
                        $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                        $("#success-alert").css("display", "block");
                    }
                }
            } catch (error) {
                console.error(error);
            } finally {
                $("#leave").attr("disabled", false);
            }
        })

        $("#leave").click(function (e) {
            localStorage.removeItem("last_view_user");
            leave();
        })


        $("#mute-audio").click(function (e) {
            if (!localTrackState.audioTrackMuted) {
                muteAudio();
            } else {
                unmuteAudio();
            }
        });

        $("#mute-video").click(function (e) {
            if (!localTrackState.videoTrackMuted) {
                muteVideo();
            } else {
                unmuteVideo();
            }
        })


        async function join() {
            if (options.role === "audience") {
                client.setClientRole(options.role, {
                    level: options.audienceLatency
                });
                // add event listener to play remote tracks when remote user publishs.
                client.on("user-published", handleUserPublished);
                client.on("user-unpublished", handleUserUnpublished);
            } else {
                client.setClientRole(options.role);
                client.on("user-published", handleUserPublished);
                client.on("user-unpublished", handleUserUnpublished);
            }


            // join the channel
            options.uid = await client.join(options.appid, options.channel, options.token || null, options.uid || null);

            if (options.role === "host") {
                // create local audio and video tracks
                localTracks.audioTrack = await AgoraRTC.createMicrophoneAudioTrack();
                localTracks.videoTrack = await AgoraRTC.createCameraVideoTrack();
                // play local video track
                localTracks.videoTrack.play("local-player");
                // $("#local-player-name").text(`localTrack(${options.uid})`);
                $("#local-player-name").text(`Admin`);
                // publish local tracks to channel
                await client.publish(Object.values(localTracks));
                document.getElementById("uid").value = options.uid;
                RTMJoin();
                console.log("publish success");
                // RTMJoin();
            }

        }

        async function leave() {

            for (trackName in localTracks) {
                var track = localTracks[trackName];
                if (track) {
                    track.stop();
                    track.close();
                    localTracks[trackName] = undefined;
                }
            }

            // remove remote users and player views
            remoteUsers = {};
            $("#remote-playerlist").html("");

            // leave the channel
            await client.leave();
            $("#local-player-name").text("");
            $("#host-join").attr("disabled", false);
            $("#audience-join").attr("disabled", false);
            $("#leave").attr("disabled", true);
            // client.destroy();
            console.log("client leaves channel success");
        }

        async function subscribe(user, mediaType) {

            const uid = user.uid;
            console.log('AgoraRTM client login success. Username: ' + user);

            await client.subscribe(user, mediaType);

            $.ajax({
                url: SITE_URL + "authpanel/contest/user_data/" + uid,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message1) {
                    var dataa = JSON.parse(message1);
                    var path = "<?php echo S3_BUCKET_ROOT . USER_IMAGE; ?>";
                    dataa.profile_image = (dataa.profile_image == '') ? 'default.png' : dataa.profile_image;
                    if (mediaType === 'video') {
                        const player = $(`
                              <div id="player-wrapper-${uid}">
                                <p class="player-name text-center">
                                <img src="${path}${dataa.profile_image}" style="width: 35px;height: 35px;border-radius: 100%;border: 1px solid #fff;margin-right: 10px;">( ${dataa.username} )</p>
                                <div id="player-${uid}" class="player" style="background-image: url('${path}${dataa.profile_image}');    background-position: center;
    background-repeat: no-repeat;"></div>
                                <div class="center_btn_container">
                                 <button id="audio-${uid}" type="button" class="btn btn-primary btn-sm remoteMicrophone micOn" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-mic-outline' id="iconaudio-${uid}"></i></button>
                                                            <button id="video-${uid}" type="button" class="btn btn-primary btn-sm remoteCamera camOn" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-videocam' id="iconvideo-${uid}"></i></button>
                                <button id="remove-${uid}" type="button" class="btn btn-primary btn-sm remoteremove removeon" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-close'></i></button>
                                <button id="change_view-${uid}" type="button" class="btn btn-primary btn-sm ChangeView" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-eye-off' id="iconview-${uid}"></i></button>
                              </div>
                              </div>
                            `);
                        if (user_ids.includes(user.uid)) {
                            $(`#player-wrapper-${uid}`).remove();
                        }
                        $("#remote-playerlist").append(player);
                        user_ids.push(uid);
                        user.videoTrack.play(`player-${uid}`, {
                            fit: "contain"
                        });
                    }
                    if (mediaType === 'audio') {
                        user.audioTrack.play();
                    }
                    var contest_id = "<?php echo $contest_id; ?>";
                    check_user_place(uid);
                    $.ajax({
                        url: SITE_URL + "authpanel/contest/check_status/" + uid + "/" + contest_id,
                        type: "POST",
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('errr');
                        },
                        success: function (check_status) {
                            var check_status1 = JSON.parse(check_status);
                            if (check_status1.is_mute_contest == '1' && check_status1.is_video_contest == '1') {

                                $("#video-" + uid).removeClass('camOn');
                                $("#iconvideo-" + uid).removeClass('zmdi-videocam');
                                $("#iconvideo-" + uid).addClass('zmdi-videocam-off');
                                $("#audio-" + uid).removeClass('micOn');
                                $("#iconaudio-" + uid).removeClass('zmdi-mic-outline');
                                $("#iconaudio-" + uid).addClass('zmdi-mic-off');
                            } else if (check_status1.is_video_contest == '1') {

                                $("#video-" + uid).removeClass('camOn');
                                $("#iconvideo-" + uid).removeClass('zmdi-videocam');
                                $("#iconvideo-" + uid).addClass('zmdi-videocam-off');
                            } else if (check_status1.is_mute_contest == '1') {

                                $("#audio-" + uid).removeClass('micOn');
                                $("#iconaudio-" + uid).removeClass('zmdi-mic-outline');
                                $("#iconaudio-" + uid).addClass('zmdi-mic-off');
                            }
                        }
                    });

                },
            });
            // subscribe to a remote user
        }
        client.on("volume-indicator", (volumes) => {
            console.log("volumes :- ", volumes);
        })
        client.on('user-left', function (evt) {
            var uid = evt.uid;
            console.log('User left the channel: ' + uid);
            $(`#player-wrapper-${uid}`).remove();
        });
        client.on('user-joined', function (user) {
            var uid = user.uid;
            console.log('User joined the channel: ' + uid);
            var contest_id = "<?php echo $contest_id; ?>";
            // subscribe(evt,'audio');
            $.ajax({
                url: SITE_URL + "authpanel/contest/user_data/" + uid,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message1) {
                    var dataa = JSON.parse(message1);
                    var path = "<?php echo S3_BUCKET_ROOT . USER_IMAGE; ?>";
                    // if (mediaType === 'video') {
                    dataa.profile_image = (dataa.profile_image == '') ? 'default.png' : dataa.profile_image;
                    const player = $(`
                              <div id="player-wrapper-${uid}">
                                     <p class="player-name">
                                <img src="${path}${dataa.profile_image}" style="
    width: 35px;
    height: 35px;
    border-radius: 100%;
    border: 1px solid #fff;
    margin-right: 10px;
">( ${dataa.username} )</p>
                                <div id="player-${uid}" class="player" style="background-image: url('${path}${dataa.profile_image}');    background-position: center;
    background-repeat: no-repeat;"></div>
                                <div class="center_btn_container">
                                 <button id="audio-${uid}" type="button" class="btn btn-primary btn-sm remoteMicrophone micOn" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-mic-outline' id="iconaudio-${uid}"></i></button>
                                                            <button id="video-${uid}" type="button" class="btn btn-primary btn-sm remoteCamera camOn" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-videocam' id="iconvideo-${uid}"></i></button>
                                <button id="remove-${uid}" type="button" class="btn btn-primary btn-sm remoteremove removeon" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-close'></i></button>
                                 <button id="change_view-${uid}" type="button" class="btn btn-primary btn-sm ChangeView" style="height:  50px;min-width: 50px !important;border-radius: 50%;background-color: #a3a6a3;"><i class='zmdi zmdi-eye-off' id="iconview-${uid}"></i></button>
                              </div>
                              </div>
                            `);

                    console.log("player**********************************");
                    console.log(player)
                    if (user_ids.includes(uid)) {
                        $(`#player-wrapper-${uid}`).remove();
                    }
                    check_user_place(uid);
                    $.ajax({
                        url: SITE_URL + "authpanel/contest/check_status/" + uid + "/" + contest_id,
                        type: "POST",
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('errr');
                        },
                        success: function (check_status) {
                            var check_status1 = JSON.parse(check_status);
                            if (check_status1.is_mute_contest == '1' && check_status1.is_video_contest == '1') {
                                user_ids.push(uid);
                                $(`#player-wrapper-${uid}`).remove();
                                $("#remote-playerlist").append(player);
                                $("#video-" + uid).removeClass('camOn');
                                $("#iconvideo-" + uid).removeClass('zmdi-videocam');
                                $("#iconvideo-" + uid).addClass('zmdi-videocam-off');
                                $("#audio-" + uid).removeClass('micOn');
                                $("#iconaudio-" + uid).removeClass('zmdi-mic-outline');
                                $("#iconaudio-" + uid).addClass('zmdi-mic-off');
                            } else if (check_status1.is_video_contest == '1') {
                                user_ids.push(uid);
                                $(`#player-wrapper-${uid}`).remove();
                                $("#remote-playerlist").append(player);
                                $("#video-" + uid).removeClass('camOn');
                                $("#iconvideo-" + uid).removeClass('zmdi-videocam');
                                $("#iconvideo-" + uid).addClass('zmdi-videocam-off');
                            } else if (check_status1.is_mute_contest == '1') {
                                user_ids.push(uid);
                                $(`#player-wrapper-${uid}`).remove();
                                $("#remote-playerlist").append(player);
                                $("#audio-" + uid).removeClass('micOn');
                                $("#iconaudio-" + uid).removeClass('zmdi-mic-outline');
                                $("#iconaudio-" + uid).addClass('zmdi-mic-off');
                            }
                        }
                    });


                },
            });
            // Perform any necessary actions when a user joins, e.g., update UI
        });


        function handleUserPublished(user, mediaType) {
            //print in the console log for debugging 
            const id = user.uid;
            remoteUsers[id] = user;
            subscribe(user, mediaType);
        }

        function handleUserUnpublished(user, mediaType) {
            //print in the console log for debugging 
            console.log('"user-unpublished" event for remote users is triggered.');

        }

        function hideMuteButton() {
            $("#mute-video").css("display", "none");
            $("#mute-audio").css("display", "none");
        }

        function showMuteButton() {
            $("#mute-video").css("display", "inline-block");
            $("#mute-audio").css("display", "inline-block");
        }

        async function muteAudio() {

            if (!localTracks.audioTrack) return;
            /**
             * After calling setMuted to mute an audio or video track, the SDK stops sending the audio or video stream. Users whose tracks are muted are not counted as users sending streams.
             * Calling setEnabled to disable a track, the SDK stops audio or video capture
             */
            await localTracks.audioTrack.setMuted(true);
            localTrackState.audioTrackMuted = true;
            $("#mute-audio").html("<i class='zmdi zmdi-mic-off'></i>");
        }

        async function muteVideo() {
            if (!localTracks.videoTrack) return;
            await localTracks.videoTrack.setMuted(true);
            localTrackState.videoTrackMuted = true;
            $("#mute-video").html("<i class='zmdi zmdi-videocam-off'></i>");
        }

        async function unmuteAudio() {
            if (!localTracks.audioTrack) return;
            await localTracks.audioTrack.setMuted(false);
            localTrackState.audioTrackMuted = false;
            $
                ("#mute-audio").html("<i class='zmdi zmdi-mic-outline'></i>");
        }

        async function unmuteVideo() {
            if (!localTracks.videoTrack) return;
            await localTracks.videoTrack.setMuted(false);
            localTrackState.videoTrackMuted = false;
            $("#mute-video").html("<i class='zmdi zmdi-videocam'></i>");
        }

        // Function to mute the remote user by their user ID
        function muteRemoteUser(userId) {

            var remoteUser = client.remoteUsers.find((user) => user.uid === userId);

            var remoteAudioTrack = remoteUser && remoteUser.audioTrack;


            if (remoteAudioTrack) {
                remoteUser.audioTrack.setMuted(true); // Mute the user's audio track
                console.log("DONE");
            }

        }


        async function RTMJoin() {
            // Create Agora RTM client
            const clientRTM = AgoraRTM.createInstance($("#appid").val(), {
                enableLogUpload: false
            });
            var accountName = $('#uid').val();
            const userId = document.getElementById("uid").value;

            $.ajax({
                url: SITE_URL + "authpanel/contest/create_token/" + userId,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    // showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
                    console.log('errr');
                },
                success: function (message) {
                    clientRTM.login({
                        uid: userId,
                        "token": JSON.parse(message).token
                    }).then(() => {
                        console.log('AgoraRTM client login success. Username: ' + accountName);
                        isLoggedIn = true;
                        // RTM Channel Join
                        var channelName = $('#channel').val();
                        channel = clientRTM.createChannel(channelName);
                        channel.join().then(() => {
                            console.log('AgoraRTM client channel join success.');

                            // Send peer-to-peer message for audio muting and unmuting
                            $(document).on('click', '.remoteMicrophone', function () {
                                fullDivId = $(this).attr('id');
                                peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);

                                let peerMessage = "audio";
                                if ($("#audio-" + peerId).hasClass('micOn')) {
                                    // localTracks.audioTrack.setEnabled(false);
                                    peerMessage = "Mute";
                                    console.log("Remote Audio Muted for: " + peerId);
                                    change_flag("audio", '1', peerId);
                                } else {
                                    peerMessage = "Unmute";
                                    // localTracks.audioTrack.setEnabled(true);
                                    console.log("Remote Audio Unmuted for: " + peerId);
                                    change_flag("audio", '0', peerId);
                                }
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {

                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            });


                            // Send peer-to-peer message for video muting and unmuting
                            $(document).on('click', '.remoteCamera', function () {
                                fullDivId = $(this).attr('id');
                                peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                console.log("Remote video button pressed.");
                                let peerMessage = "video";
                                if ($("#video-" + peerId).hasClass('camOn')) {
                                    // localTracks.audioTrack.setEnabled(false);
                                    // peerMessage = "VideoOn";
                                    peerMessage = "VideoOff";
                                    console.log("Remote Audio Muted for: " + peerId);
                                    change_flag("video", '1', peerId);
                                    // $("#video-" + peerId).removeClass('camOn');
                                } else {
                                    // peerMessage = "VideoOff";
                                    peerMessage = "VideoOn";
                                    // localTracks.audioTrack.setEnabled(true);
                                    console.log("Remote Audio Unmuted for: " + peerId);
                                    change_flag("video", '0', peerId);
                                    // $("#video-" + peerId).addClass('camOn');
                                }
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {
                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            });
                            $(document).on('click', '.remoteremove', function () {
                                fullDivId = $(this).attr('id');
                                peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                console.log("Remote video button pressed.");
                                let peerMessage = "Remove";
                                // send_notification(peerId);
                                clientRTM.sendMessageToPeer({
                                    text: peerMessage
                                },
                                    peerId,
                                ).then(sendResult => {
                                    if (sendResult.hasPeerReceived) {
                                        console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                    } else {
                                        console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                    }
                                })
                            });

                            $(document).on('click', '.ChangeView', function () {
                                fullDivId = $(this).attr('id');
                                peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                if (peerId == 0) {
                                    peerId = $('#uid').val();
                                }
                                console.log("Remote video button pressed.");
                                let peerMessage = "ChangeSpeakView_" + peerId;
                                // send_notification(peerId);
                                var x = localStorage.getItem("last_view_user");
                                localStorage.setItem('last_view_user', peerId);
                                if (peerId != $('#uid').val()) {
                                    var parentContainer = document.getElementById("remote-playerlist");
                                    var divToMove = document.getElementById("player-wrapper-" + peerId);
                                    parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                                }
                                if (x == null) {
                                    $("#iconview-0").removeClass('zmdi-eye');
                                    $("#iconview-0").addClass('zmdi-eye-off');
                                } else {
                                    $("#iconview-" + x).removeClass('zmdi-eye');
                                    $("#iconview-" + x).addClass('zmdi-eye-off');
                                }
                                if (x == $('#uid').val()) {
                                    $("#iconview-0").addClass('zmdi-eye-off');
                                    $("#iconview-0").removeClass('zmdi-eye');
                                }
                                if (peerId == $('#uid').val()) {
                                    $("#iconview-0").removeClass('zmdi-eye-off');
                                    $("#iconview-0").addClass('zmdi-eye');
                                } else {
                                    $("#iconview-" + peerId).removeClass('zmdi-eye-off');
                                    $("#iconview-" + peerId).addClass('zmdi-eye');
                                }
                                // var set_speaker_value = ($('#uid').val() == peerId)?0:peerId;
                                 var set_speaker_value = peerId;
                                set_speaker(set_speaker_value);
                                // Get the list of channel members
                                channel.getMembers()
                                    .then(memberList => {
                                        console.log('Channel Members:', memberList);
                                        for (const userId of memberList) {
                                            clientRTM.sendMessageToPeer(
                                                { text: peerMessage },
                                                userId
                                            ).then(() => {
                                                console.log(`Message sent to ${userId} successfully`);
                                                console.log('Message sent to the group');

                                            }).catch(error => {
                                                console.error(`Error sending message to ${userId}:`, error);
                                            });
                                        }
                                        
                                    })
                                    .catch(error => {
                                        console.error('Error getting channel members:', error);
                                    });
                            });

                            // Display messages from peer
                            clientRTM.on('MessageFromPeer', function ({
                                text
                            }, peerId) {
                                console.log("text", text);
                                console.log("peerId", peerId);
                                if (text == "Mute") {
                                    // $("#audio-" + peerId).addClass('micOn');
                                    $("#audio-" + peerId).removeClass('micOn');
                                    $("#iconaudio-" + peerId).removeClass('zmdi-mic-outline');
                                    $("#iconaudio-" + peerId).addClass('zmdi-mic-off');
                                    // change_flag("audio", '1', peerId);

                                } else if (text == "Unmute") {
                                    $("#audio-" + peerId).addClass('micOn');
                                    $("#iconaudio-" + peerId).addClass('zmdi-mic-outline');
                                    $("#iconaudio-" + peerId).removeClass('zmdi-mic-off');
                                    // change_flag("audio", '0', peerId);
                                } else if (text == "VideoOn") {
                                    $("#video-" + peerId).addClass('camOn');
                                    //    $("#audio-" + peerId).addClass('micOn');
                                    $("#iconvideo-" + peerId).addClass('zmdi-videocam');
                                    $("#iconvideo-" + peerId).removeClass('zmdi-videocam-off');
                                    // change_flag("video", '0', peerId);
                                } else if (text == "VideoOff") {
                                    $("#video-" + peerId).removeClass('camOn');
                                    $("#iconvideo-" + peerId).removeClass('zmdi-videocam');
                                    $("#iconvideo-" + peerId).addClass('zmdi-videocam-off');
                                    // change_flag("video", '1', peerId);
                                } else if (text == "Remove") {
                                    $("#player-wrapper-" + peerId).remove();
                                    send_notification(peerId);
                                    block_unblock_user(peerId);
                                }

                            })

                        }).catch(error => {
                            console.log('AgoraRTM client channel join failed: ', error);
                        }).catch(err => {
                            console.log('AgoraRTM client login failure: ', err);
                        });
                    });
                }
            });

            function Leave_memder() {
                channel.getMembers().then((memberNames) => {
                    console.log("All members in the channel are as follows: ");
                    console.log(memberNames);
                    var newHTML = $.map(memberNames, function (singleMember) {
                        console.log(singleMember);
                    })
                })
            }

            function send_notification(user_id) {
                var contest_id = $("#contest_id").val();
                $.ajax({
                    url: SITE_URL + "authpanel/contest/send_notification/" + user_id + "/" + contest_id,
                    type: "POST",
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('errr');
                    },
                    success: function (message) {
                        console.log(message);
                    }
                });
            }

            function block_unblock_user(user_id) {
                var contest_id = $("#contest_id").val();
                $.ajax({
                    url: SITE_URL + "authpanel/contest/block_unblock_user/" + user_id + "/" + contest_id,
                    type: "POST",
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('errr');
                    },
                    success: function (message) {
                        console.log(message);
                        get_block_user_list();
                    }
                });
            }

            function get_block_user_list() {
                var contest_id = $("#contest_id").val();
                $.ajax({
                    url: SITE_URL + "authpanel/contest/get_block_user_list/" + contest_id,
                    type: "POST",
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('errr');
                    },
                    success: function (message) {
                        document.getElementById('appendhomeviewmoredata').innerHTML = '';
                        var result = JSON.parse(message);
                        result.forEach(element => {
                            $("#block_list_templatehtml").tmpl(element).appendTo("#appendhomeviewmoredata");
                        })
                    }
                });
            }
            // Logout
            document.getElementById("leave").onclick = async function () {
                await clientRTM.logout();
            }
        }

        $("#host-join").click(function (e) {

            var contest_id = $("#contest_id").val();
            var uid = $("#uid").val();
            var flag = 1;
            setInterval(host_join(contest_id, flag), 50000);

        })

        function host_join(contest_id, flag) {
            $.ajax({
                url: SITE_URL + "authpanel/contest/host_start/" + contest_id + "/" + flag + "/" + $("#uid").val(),
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    console.log(message);
                }
            })
        }
        $("#leave").click(function (e) {
            var contest_id = $("#contest_id").val();
            var flag = 0;
            var uid = $("#uid").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/host_start/" + contest_id + "/" + flag + "/" + uid,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) { }
            });
        })

        function Ublock_user(user_id) {
            var contest_id = $("#contest_id").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/unblock_user/" + user_id + "/" + contest_id,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    get_block_user_list1();
                }
            });
        }

        function get_block_user_list1() {
            var contest_id = $("#contest_id").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/get_block_user_list/" + contest_id,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    document.getElementById('appendhomeviewmoredata').innerHTML = '';
                    var result = JSON.parse(message);
                    if (result.length > 0) {

                        result.forEach(element => {
                            $("#block_list_templatehtml").tmpl(element).appendTo("#appendhomeviewmoredata");
                        })
                    }

                }
            });
        }

        function change_flag(type, flag, user_id) {
            var contest_id = $("#contest_id").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/change_flag/" + type + "/" + flag + "/" + user_id + "/" + contest_id,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    console.log(message);
                }
            });
        }
        function set_speaker(user_id) {
            var contest_id = $("#contest_id").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/set_speaker/" + user_id + "/" + contest_id,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    console.log(message);
                }
            });
        }
        function check_user_place(user_id){
            var contest_id = $("#contest_id").val();
            $.ajax({
                url: SITE_URL + "authpanel/contest/check_user_place/"+ contest_id,
                type: "POST",
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('errr');
                },
                success: function (message) {
                    console.log("check_user_place :- ",message);
                    var message1 = JSON.parse(message);
                    console.log("check_user_place :- ",message1.speaker_id);
                    if(message1.speaker_id != 0 && message1.speaker_id == user_id){
                        var parentContainer = document.getElementById("remote-playerlist");
                        var divToMove = document.getElementById("player-wrapper-" + message1.speaker_id);
                        parentContainer.insertBefore(divToMove, parentContainer.firstChild);
                        $("#iconview-" + user_id).removeClass('zmdi-eye-off');
                        $("#iconview-" + user_id).addClass('zmdi-eye');
                    }
                }
            });
        }
    </script>
</body>

</html>