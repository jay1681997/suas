<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Contest Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  /*text-align: left;*/
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
    <section class="content">
     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Contest Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/contest">Contest List</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a>Contest Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- View -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card member-card">
                            <div class="header l-red">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>   
                                </div>
                            </div>
                                <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                <div class="member-img m-t-5">
                                   

                                    <?php 
                                    $file_ext = explode('.', $result['contest_image']);
                                       // print_r($file_ext); die;                                   
                                    if($file_ext[1] == 'mp4') {  ?> 
                                        <video width="300" controls style="margin-top: -7%;" > 
                                            <source src=" <?php echo  S3_BUCKET_ROOT.CONTEST_IMAGE.@$result['contest_image']; ?> "> Your browser does not support HTML5 video. 
                                        </video>
                                    <?php } else {?>
                                        <img src="<?php echo  S3_BUCKET_ROOT.CONTEST_IMAGE.@$result['contest_image']; ?>" class="rounded-circle" alt="no-image" width="100" height="150px" style="margin-top: -7%">
                                    <?php } ?> 


                                </div>
                                <div class="body">
                                    <div class="col-12">
                                        <ul class="list-inline">
                                            <li><h4 class="m-t-5"><?php echo @$result['name'];?></h4></li>
                                            <a href="<?php echo site_url('authpanel/contest/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                        </ul>
                                    </div>
                                <hr>
                                <div class="card">
                                    <div class="row text-left" >
                                        <div class="col-md-12">
                                            <div class="card">
                                                <h4 class=""><b>Contest Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    <div class="about-info-p">
                                                        <strong>Contest Type</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['contest_type']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Description</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['description']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Total Participant</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['total_participant']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Total Point</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['total_point']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Price</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['currency'].''.@$result['price']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Contest Start Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo date('d M Y H:i A', strtotime($result['contest_startdate'])); ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Contest End Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo date('d M Y H:i A', strtotime($result['contest_enddate'])); ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Age Category</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['age_category']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Winner Date</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo date('d M Y', strtotime($result['winner_date'])); ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Contest</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['contest_by']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['status'] == 'Active')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted">'.@$result['status'].'</p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Added Date</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['insert_datetime'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="card">
                                                <h4 class="" align="center"><b>Participant List</b></h4>
                                                <hr>
                                                <table id="contest_table" data-toggle="table"
                                                    data-toolbar="#toolbar"
                                                    data-url="<?php echo site_url('authpanel/contest/participant_ajax_list/'.@$result['id'])?>"
                                                    data-pagination="true"
                                                    data-side-pagination="server"
                                                    data-search="false"
                                                    data-show-toggle="false"
                                                    data-show-columns="false"
                                                    data-sort-name="id"
                                                    data-page-list="[10,50,100,500]"
                                                    data-page-size="10"
                                                    data-sort-order="desc"
                                                    data-show-refresh="false"
                                                    data-show-export="true"
                                                    data-export-types="['excel','csv','pdf']"
                                                    class="table-bordered"
                                                    data-row-style="rowStyle"
                                                >
                                                    <thead>
                                                        <tr>
                                                            <th data-field="action" data-align="center">Winner</th>
                                                            <?php if(@$result['contest_type'] != 'live'){ ?>
                                                                  <th data-field="video" data-sortable="true" data-align="center">Uploaded Video</th>
                                                          <?php  } ?>
                                                         
                                                            <th data-field="username" data-sortable="true" data-align="center">User Name</th>
                                                            <th data-field="insert_datetime" data-sortable="true">Added Date</th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card">
                                                <h4 class="" align="center"><b>Scorecard List</b></h4>
                                                <hr>
                                                <table>
                                                  <tr>
                                                    <th>Scorecard for Impromptu Speech Contest</th>
                                                    <th>Points</th>
                                                  </tr>
                                                  <?php $total_point = 0;
                                                    if(!empty($scorecard)){
                                                    foreach ($scorecard as $key => $value) { 
                                                        $total_point += $value['points'];
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $value['title']; ?></td>
                                                            <td><?php echo $value['points']; ?></td>
                                                        </tr>
                                                        </tr>
                                                   <?php }
                                                  }else{ ?>
                                                    <tr>
                                                    <td align="center" colspan="2">Data Not Found</td>
                                                    <!-- <td></td> -->
                                                </tr>
                                                  <?php } ?>
                                                  <tr>
                                                    <td>Total Points:</td>
                                                    <td><?php echo $total_point; ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- End View -->
    </div>
</div>      
</div>   
</section>
<!-- Jquery Core Js --> 
<?php $this->load->view('authpanel/common/scripts'); ?>
<script type="text/javascript">
    var table = $('#contest_table');
    $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
    });

    /*
    ** Function for remove customer details
    */
    function remove(contest_id) 
    {
        swal({   
            title: "Remove Contest?",   
            text: "Are you sure you want to delete this Contest?",   
            type: "error",   
            showCancelButton: true, 
            confirmButtonColor: "#fdd329",   
            confirmButtonText: "Confirm",   
            closeOnConfirm: false 
        },  function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url:  SITE_URL+"authpanel/contest/removecontest/"+contest_id,
                    type: "GET",
                    success: function(data) {
                        var url = SITE_URL+'authpanel/contest/';
                        window.location.href = url;
                    },
                    error: function(jqXHR, textStatus, errorThrown){},
                    complete: function(){}
                }); // END OF AJAX CALL
            }
        });
    }

    function add_winner(winner_id, num) 
    {
        console.log(num);
        swal({   
          title: "Winner?",   
          text: "Are you sure you want to winner this User?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/contest/add_winner/"+winner_id+"/"+num,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#contest_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
    }

</script>
</body>
</html>
