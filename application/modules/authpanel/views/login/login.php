<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title><?php echo PROJECT_NAME.' Admin Login'; ?></title>
        <link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">

        
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate_page.css">
        <link href="<?php echo base_url();?>assets/plugins/select2/select2.jquery.json" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/authentication.css">
    </head>
    <body class="theme-orange">
        <div class="authentication">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header slideDown">
                                <div class="logo">
                                    <img src="<?php echo base_url().LOGO_NAME;?>" style="height: 100px; width:130px; margin-bottom: 0px" alt="<?php echo PROJECT_NAME; ?>">
                                </div>
                                <h1 class="" style="color: <?php echo THEME_COLOR;?>;">
                                    <?php echo PROJECT_NAME." Admin Portal"; ?> 
                                </h1>
                            </div> 
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('success_msg')){ ?>
                            <div class="alert alert-success alert-dismissible zoomIn animated" >
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success_msg')?>
                            </div>                
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('error_msg')){ ?>
                            <div class="alert alert-danger alert-dismissible zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error_msg')?>
                            </div>
                            <?php } ?>                       
                        </div>     
                        <div class="clearfix"></div>
                        <?php echo form_open('authpanel/login/checklogin',array('class'=>'col-lg-12','name'=>'login_form','id'=>'login_form','method'=>'post')); ?> 
                            <h5 class="title">Sign in to your Account</h5>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input class="form-control" type="email"  id="email" name="email" parsley-type="email" value="<?php echo set_value('email'); ?>" data-parsley-trigger="change" required data-parsley-required-message="Please enter email">
                                    <label class="form-label">Email</label>
                                    <?php echo form_error('email'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group form-float">
                                <div class="form-line" >
                                    <input class="form-control" type="password" id="password" name="password" data-parsley-trigger="change" required data-parsley-required-message="Please enter password">
                                    <label class="form-label">Password</label>
                                    <?php echo form_error('password'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <input type="hidden" name="timezone" id="timezone" value="">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-raised bg-red waves-effect">Sign In</button>
                            </div>                       
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery Core Js -->
        <script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
        <script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script><!-- Parsley Js -->
        <script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>            
        <!-- For getting local timezone -->
        <script src="<?php echo base_url();?>assets/js/jstz.min.js"></script>
        <!-- For getting local timezone -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();
                //Get browser timezone & set it
                var timezone = jstz.determine();
                $('#timezone').val(timezone.name());
            });
        </script>
    </body>
</html>
