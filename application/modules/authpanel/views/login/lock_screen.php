<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="dityer admin panel">
    	<meta name="author" content="Codeigniter">
    	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">
    	<title><?php echo PROJECT_NAME; ?> Admin lock Screen</title>
        <!-- Favicon-->
        <link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">
        <!-- Custom Css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
        <!-- Sweet Alerts  -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate_page.css">
        <link href="<?php echo base_url();?>assets/plugins/select2/select2.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/authentication.css">
    </head>
    <body class="theme-orange">
        <?php $admindata=$this->session->userdata(ADMIN_SESSION_NAME); ?>
        <div class="authentication">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header slideDown">
                                <div class="logo">
                                    <img src="<?php echo base_url().LOGO_NAME;?>" style="height: 100px; width:100px;" alt="<?php echo PROJECT_NAME; ?>">
                                </div>
                                <h1 class="" style="color: <?php echo THEME_COLOR;?>;">
                                    <?php echo PROJECT_NAME." Admin Portal"; ?> 
                                </h1>
                            </div>                        
                        </div>
                        <div class="col-md-12">
                            <div class="thumb">
                                <img class="rounded-circle" src="<?php echo S3_BUCKET_ROOT.ADMIN_IMAGE.@$admindata['profile_image']; ?>" alt="<?php echo @$admindata['name'];?>" width="100px" height="100px">
                                <h4 class="m-b-0 m-t-10"><?php echo @$admindata['name'];?></h4>
                            </div>                    
                            <?php if($this->session->flashdata('success_msg')){ ?>
                            <div class="alert alert-success alert-dismissible zoomIn animated" >
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success_msg')?>
                            </div>                
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php if($this->session->flashdata('error_msg')){ ?>
                            <div class="alert alert-danger alert-dismissible zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error_msg')?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php echo form_open('authpanel/login/unlock',array('class'=>'col-lg-12','name'=>'adminlock_form','id'=>'adminlock_form','method'=>'post')); ?> 
                            <h5 class="title">
                                Enter your password to access the admin portal.
                            </h5>
                            <div class="form-group form-float">
                                <div class="form-line" >
                                    <input class="form-control" type="password" id="password" name="password" data-parsley-trigger="change" required>
                                    <label class="form-label">Password</label>
                                    <?php echo form_error('email'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-raised bg-theme waves-effect">
                                    Sign In
                                </button>
                            </div>                    
                        <?php echo form_close(); ?>
                                          
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery Core Js -->
        <script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
        <script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script><!-- Parsley Js -->
        <script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js" type="text/javascript"></script>            
        <!-- For getting local timezone -->
        <script src="<?php echo base_url();?>assets/js/jstz.min.js"></script>
        <!-- For getting local timezone -->
        <script type="text/javascript">
            $(document).ready(function() {
                $('form').parsley();
                //Get browser timezone & set it
                var timezone = jstz.determine();
                $('#timezone').val(timezone.name());
            });
        </script>
    </body>
</html>