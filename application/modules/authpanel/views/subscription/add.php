<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Subscription";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Subscription Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/subscription">
                                    Subscription List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Subscription Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/subscription/add/',array('id'=>'add_subscription', 'name'=>'add_subscription', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                       <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Subscription Image <input type="file" id="subscription_image" name="subscription_image" class="filestyle" data-parsley-trigger="change"   data-parsley-required-message="Please select subscription image" data-parsley-errors-container="#image_error"  accept="image/jpeg, image/png, image/gif, image/jpg" required=""/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                     <label id="image_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Evaluation Type<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="evaluation_type" id="evaluation_type" parsley-trigger="change" data-parsley-errors-container="#evaluation_typeerror" data-parsley-required-message="Please select evaluation type"  required>
                                                        <option value="">Select Evaluation Type</option>
                                                        <!-- <option value="AI" <?php echo set_select('evaluation_type','AI',false); ?> selected>AI Evaluation</option> -->
                                                        <option value="Manual" <?php echo set_select('evaluation_type','Manual',false); ?>>Manual Evaluation</option>
                                                    </select>
                                                    <?php echo form_error('evaluation_type'); ?>
                                                    <label id="evaluation_typeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 m-t-30">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('name')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter subscription name" data-parsley-errors-container="#error" required>
                                                        <label class="form-label">Subscription Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo !empty(set_value('price')) ? 'focused' : ''; ?>">
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo set_value('price'); ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" required>
                                                            <label class="form-label">Price <span class="text-danger">*</span></label>
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('total_video')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_video" id="total_video" value="<?php echo set_value('total_video');  ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total video" data-parsley-errors-container="#total_video_error" required  onkeypress="return isNumberKey1(event);">
                                                        <label class="form-label">
                                                            Total Video Evaluation <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('total_video'); ?>
                                                    <label id="total_video_error"></label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Duration<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="duration" id="duration" parsley-trigger="change" data-parsley-errors-container="#durationerror" data-parsley-required-message="Please select duration"  required onchange="add_validity(this.value)">
                                                        <option value="">Select Duration</option>
                                                        <option value="day" <?php echo set_select('duration','day',false); ?>>Day</option>
                                                        <option value="week" <?php echo set_select('duration','week',false); ?>>Week</option>
                                                        <option value="month" <?php echo set_select('duration','month',false); ?>>Month</option>
                                                        <option value="Year" <?php echo set_select('duration','year',false); ?>>year</option>
                                                    </select>
                                                    <?php echo form_error('duration'); ?>
                                                    <label id="durationerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label"> Validity <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="validity" id="validity" parsley-trigger="change" data-parsley-errors-container="#validityerror" data-parsley-required-message="Please select validity"  required>
                                                        <option value="">Select Validity</option>
                                                    </select>
                                                    <?php echo form_error('validity'); ?>
                                                    <label id="validityerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description') || @$result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" style="height: 100px" data-parsley-required-message="Please write description" required><?php echo set_value('description'); ?></textarea>
                                                        <label class="form-label">Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/subscription" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.SUBSCRIPTION_IMAGE.'default.png'; ?>')");
            });
            

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#subscription_image").change(function () {
                readURL(this);
            });

          

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 

            function add_validity(duration){
                console.log(duration)
                select = document.getElementById('validity');
                if(duration == 'day'){
                    $('#validity').find('option').remove()
                    for (var i = 1; i <= 31; i++) {
                        var opt = document.createElement('option');
                        opt.value = i;
                        opt.innerHTML = i;
                        select.appendChild(opt);
                    }
                }else if(duration == 'week'){
                    $('#validity').find('option').remove()
                    for (var i = 1; i <= 52; i++) {
                        var opt = document.createElement('option');
                        opt.value = i;
                        opt.innerHTML = i;
                        select.appendChild(opt);
                    }

                }else if(duration == 'month'){
                    $('#validity').find('option').remove()
                    for (var i = 1; i <= 12; i++) {
                        var opt = document.createElement('option');
                        opt.value = i;
                        opt.innerHTML = i;
                        select.appendChild(opt);
                    }
                }else {
                    $('#validity').find('option').remove()
                    for (var i = 1; i <= 12; i++) {
                        var opt = document.createElement('option');
                        opt.value = i;
                        opt.innerHTML = i;
                        select.appendChild(opt);
                    }
                }
            }
        </script>
    </body>
</html>