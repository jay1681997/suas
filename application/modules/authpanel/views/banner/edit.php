<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Banner";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Banner Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/banner">
                                    Banner List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Banner Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/banner/edit/'.base64_encode($result['id']),array('id'=>'add_class', 'name'=>'add_class', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Profile Image <input type="file" id="banner_image" name="banner_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/gif, image/jpg"/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-12 col-lg-12 col-xl-12 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Title<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('title')) || !empty($result['title']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo !empty(set_value('title')) ? set_value('title') : $result['title']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter title"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>                 
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/banner"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>

        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                });   
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.BANNER_IMAGE.$result['banner_image']; ?>')");
             
            });
         
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#banner_image").change(function () {
                readURL(this);
            });
        
        </script>
    </body>
</html>