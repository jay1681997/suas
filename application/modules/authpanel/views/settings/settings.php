<!doctype html>
<html class="no-js " lang="en">

<head>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/twitter-select-bootstrap.css">
    <?php $data['title'] = "Setting";
    $this->load->view('authpanel/common/stylesheet', $data);  ?>
    <!-- Bootstrap Material Datetime Picker Css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" />
    <style type="text/css">
        .navbar-header {
            height: 60px;
        }

        .dropup .dropdown-toggle::after {
            display: none;
        }

        .dropdown-toggle::after {
            display: none;
        }

        .navbar-header .navbar-brand {
            font-size: 19.2px;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu li.selected a span.check-mark {
            display: none;
        }
    </style>
</head>

<body class="<?php echo THEME_COLOR; ?>">
    <!-- Top Bar -->
    <?php $this->load->view('authpanel/common/header'); ?>
    <!-- Left Sidebar -->
    <?php $data['pagename'] = "Users List";
    $data['subpagename'] = "Setting";
    $this->load->view('authpanel/common/left-menu', $data); ?>
    <!-- Right Sidebar -->
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.js"></script>
    <style type="text/css">
        .dropdown-menu.open {
            overflow: unset !important;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
            margin-right: 34px;
            margin-left: 34px !important;
        }

        .bootstrap-select.btn-group .dropdown-toggle .filter-option {
            display: inline-block;

        }

        .bootstrap-select>.dropdown-toggle.bs-placeholder,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:hover,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:focus,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:active {
            color: #fff;
            /*padding-left: 20px;*/
        }

        .bootstrap-select .dropdown-menu li.selected a {
            background-color: #51b1d7 !important;
            color: #555 !important;
        }

        .btn:not(.btn-link):not(.btn-circle) {
            border: 1px solid #101111;
        }

        .btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle):hover {
            cursor: pointer !important;
            background-color: #6aa2e8 !important;
        }

        .btn-group>.btn:last-child:not(:first-child):not(.dropdown-toggle):hover {
            cursor: pointer !important;
            background-color: #6aa2e8 !important;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu.inner {
            max-height: 150px !important;
        }
    </style>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>
                        Setting
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/dashboard">
                                <i class="zmdi zmdi-home"></i><span style="margin-left: 20px">Dashboard</span>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/setting">
                                Setting</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Setting
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid animated zoomIn">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="header ">
                            <div class="col-12 text-left">
                                <!-- <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-black text-left">
                                        <i class="zmdi zmdi-arrow-left"></i>
                                    </a> -->

                                <!-- <div class="col-12 text-right">
                                        <a href="<?php echo base_url(); ?>authpanel/setting/history"  class="btn btn-raised bg-theme btn-xs waves-effect">
                                            <i class="zmdi zmdi-time-restore"></i><span><b> History</b></span>
                                        </a>
                                    </div> -->

                            </div>
                        </div>
                        <div class="bg-custom bg-theme button-color-css"></div>
                        <div class="body">
                            <div class="col-md-8 col-lg-8 col-xl-8 offset-md-1">
                                <?php if ($this->session->flashdata('success_msg')) { ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('success_msg') ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('error_msg')) { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('error_msg') ?>
                                    </div>
                                <?php } ?> <?php if (isset($error_msg) && $error_msg != '') { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $error_msg; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                <?php echo form_open('authpanel/settings/edit/'.base64_encode($result['id']), array('user_id' => 'edit_user', 'name' => 'edit_user', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post')); ?>


                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Currency <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <div class="form-line <?php echo (!empty(set_value('currency')) || (!empty($result['currency'])) ? 'focused' : ''); ?>">
                                                    <input type="text" class="form-control" name="currency" id="currency" value="<?php echo !empty(set_value('currency')) ? set_value('currency') : $result['currency'] ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter currency" required>
                                                    <?php echo form_error('currency'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Currency Symbol <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <div class="form-line <?php echo (!empty(set_value('currency_symbol')) || (!empty($result['currency_symbol'])) ? 'focused' : ''); ?>">
                                                    <input type="text" class="form-control" name="currency_symbol" id="currency_symbol" value="<?php echo !empty(set_value('currency_symbol')) ? set_value('currency_symbol') : $result['currency_symbol'] ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter currency symbol" required>
                                                    <?php echo form_error('currency_symbol'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Tax <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <div class="form-line <?php echo (!empty(set_value('tax')) || (!empty($result['tax'])) ? 'focused' : ''); ?>">
                                                    <input type="text" class="form-control" name="tax" id="tax" value="<?php echo !empty(set_value('tax')) ? set_value('tax') : $result['tax'] ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter tax" required>
                                                    <?php echo form_error('tax'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Credit Point Discount<span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <div class="form-line <?php echo (!empty(set_value('credit_point_discount')) || (!empty($result['credit_point_discount'])) ? 'focused' : ''); ?>">
                                                    <input type="text" class="form-control" name="credit_point_discount" id="credit_point_discount" value="<?php echo !empty(set_value('credit_point_discount')) ? set_value('credit_point_discount') : $result['credit_point_discount'] ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter credit point discount" required>
                                                    <?php echo form_error('credit_point_discount'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 offset-md-5" style="margin-left: 250px">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-raised bg-theme waves-effect" style="margin:0 auto;" onsubmit="validate()">
                                            Send
                                        </button>
                                        <a href="<?php echo base_url(); ?>authpanel/setting" class="btn btn-raised btn-default waves-effect">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script src="<?php echo base_url(); ?>assets/plugins/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js?<?php echo time(); ?>"></script>
    <script>
        autosize(document.querySelectorAll('textarea'));
    </script>
    <script type="text/javascript">
        $('.selectpicker').selectpicker({
            noneResultsText: 'I found no results'
        });

        function submitSetting() {
            return false;
        }

        function validate() {
            var user = $('#users').val();
            var driver = $('#drivers').val();
            if ((user != '' && user.length > 0) && (driver != '' && driver.length > 0)) {
                $("#userserrors").text('');
                $("#driverserrors").text('');
            } else {
                $("#users").attr('required', 'required');

                $("#drivers").attr('required', 'required');
            }
        }
    </script>

</body>

</html>