<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Speech Evaluation";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Speech Evaluation Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/speechevaluation">
                                    Speech Evaluation List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Speech Evaluation Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/speechevaluation/edit/'.base64_encode($result['id']), array('id'=>'add_speechevaluation', 'name'=>'add_speechevaluation', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                               
                                        <div class="row clearfix">
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('title') || $result['title']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control no-resize auto-growth" name="title" id="title"  data-parsley-trigger="change" value="<?php echo $result['title'];?>" readonly>
                                                        <label class="form-label">Title<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('title'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description') || $result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" required style="overflow: hidden; overflow-wrap: break-word; height: 50px;" readonly><?php echo $result['description']; ?></textarea>
                                                        <label class="form-label">User Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Pace Result<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('pace_result') || $result['pace_result']) ? 'focused' : '');?>">
                                                        <input type="number" step="0.01" max="5" min="0.01" class="form-control" name="pace_result" id="pace_result" value="<?php echo !empty(set_value('pace_result')) ? set_value('pace_result') : $result_factor['pace_result']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter pace result" data-parsley-errors-container="#error" required  >
                                                    </div>
                                                    <?php echo form_error('pace_result'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Pace Description<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('pace_description') || $result['pace_description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control" name="pace_description" id="pace_description"  data-parsley-trigger="change"  data-parsley-required-message="Please write pace description" required><?php echo $result_factor['pace_description']; ?></textarea>
                                                    </div>
                                                    <?php echo form_error('pace_description'); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Energy/Enthusiasm Result<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('energy_result') || $result['energy_result']) ? 'focused' : '');?>">
                                                        <input type="number" step="0.01" max="5" min="0.01" class="form-control" name="energy_result" id="energy_result" value="<?php echo !empty(set_value('energy_result')) ? set_value('energy_result') : $result_factor['energy_result']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter energy result" data-parsley-errors-container="#error" required >
                                                    </div>
                                                    <?php echo form_error('energy_result'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Energy/Enthusiasm Description<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('energy_description') || $result['energy_description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control" name="energy_description" id="energy_description"  data-parsley-trigger="change"  data-parsley-required-message="Please write energy description" required><?php echo $result_factor['energy_description']; ?></textarea>
                                                    </div>
                                                    <?php echo form_error('energy_description'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Filler Words Result<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('fillerword_result') || $result['fillerword_result']) ? 'focused' : '');?>">
                                                        <input type="number" step="0.01" max="5" min="0.01" class="form-control" name="fillerword_result" id="fillerword_result" value="<?php echo !empty(set_value('fillerword_result')) ? set_value('fillerword_result') : $result_factor['fillerword_result']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter filler word result" data-parsley-errors-container="#error" required >
                                                    </div>
                                                    <?php echo form_error('fillerword_result'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Filler Words Description<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('fillerword_description') || $result['fillerword_description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control" name="fillerword_description" id="fillerword_description"  data-parsley-trigger="change"  data-parsley-required-message="Please write filler word description" required><?php echo $result_factor['fillerword_description']; ?></textarea>
                                                    </div>
                                                    <?php echo form_error('fillerword_description'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Conciseness Result<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('conciseness_result') || $result['conciseness_result']) ? 'focused' : '');?>">
                                                        <input type="number" step="0.01" max="5" min="0.01" class="form-control" name="conciseness_result" id="conciseness_result" value="<?php echo !empty(set_value('conciseness_result')) ? set_value('conciseness_result') : $result_factor['conciseness_result']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter conciseness result" data-parsley-errors-container="#error" required >
                                                    </div>
                                                    <?php echo form_error('conciseness_result'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Conciseness Description<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('conciseness_description') || $result['conciseness_description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control" name="conciseness_description" id="conciseness_description"  data-parsley-trigger="change"  data-parsley-required-message="Please write conciseness description" required><?php echo $result_factor['conciseness_description']; ?></textarea>
                                                    </div>
                                                    <?php echo form_error('conciseness_description'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Eye Contact Result<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('eyecontact_result') || $result['eyecontact_result']) ? 'focused' : '');?>">
                                                        <input type="number" step="0.01" max="5" min="0.01" class="form-control" name="eyecontact_result" id="eyecontact_result" value="<?php echo !empty(set_value('eyecontact_result')) ? set_value('eyecontact_result') : $result_factor['eyecontact_result']; ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter eye contact result" data-parsley-errors-container="#error" required >
                                                    </div>
                                                    <?php echo form_error('eyecontact_result'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Eye Contact Description<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('eyecontact_description') || $result['eyecontact_description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control" name="eyecontact_description" id="eyecontact_description"  data-parsley-trigger="change"  data-parsley-required-message="Please write eye contact description" required><?php echo $result_factor['eyecontact_description']; ?></textarea>
                                                    </div>
                                                    <?php echo form_error('eyecontact_description'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-6 offset-md-3">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/speechevaluation" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
            });
            function isNumberKey1(evt)
            {   console.log("IS NUMBER");
                var charCode = (evt.which) ? evt.which : event.keyCode;
                var value = Number(evt.target.value + evt.key) || 0;
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                     return isValidNumber(value);  
                }   
                return true;
            } 

            function digitKeyOnly(e) {
              var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
              console.log(keyCode);
              var value = Number(e.target.value + e.key) || 0;

              if ((keyCode >= 37 && keyCode <= 40) || (keyCode == 8 || keyCode == 9 || keyCode == 13) || (keyCode >= 48 && keyCode <= 57)) {
                return isValidNumber(value);
              }
              return false;
            }
            console.log("NUmber");
            console.log(number);

            function isValidNumber (number) {
              return (0 <= number && number <= 5 )
            }
        </script>

         
    </body>
</html>