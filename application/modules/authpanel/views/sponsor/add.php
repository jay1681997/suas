<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Sponsor";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Sponsor Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/sponsor">
                                    Sponsor List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Sponsor Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/sponsor/add/',array('id'=>'add_sponsor', 'name'=>'add_sponsor', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Sponsor Logo <input type="file" id="sponsor_logo" name="sponsor_logo" class="filestyle" data-parsley-trigger="change"  
                                                                data-parsley-required-message = "Please upload sponsor logo"
                                                                 data-parsley-errors-container="#sponsor_error"  accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="sponsor_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Contest<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="contest_id" id="contest_id" parsley-trigger="change" data-parsley-errors-container="#contest_iderror" data-parsley-required-message="Please select contest"  required>
                                                        <option value="">Select Contest</option>
                                                        <?php foreach($contests as $key => $value){ ?> 
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <?php echo form_error('contest_id'); ?>
                                                    <label id="contest_iderror"></label>
                                                </div>
                                            </div> -->
                                            <div class="col-md-6 col-lg-6 col-xl-6 m-t-30">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('sponsor_name')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="sponsor_name" id="sponsor_name" value="<?php echo set_value('sponsor_name'); ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter sponsor name" data-parsley-errors-container="#error" required>
                                                        <label class="form-label">Sponsor Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('sponsor_name'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('sponsor_link')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="sponsor_link" id="sponsor_link" value="<?php echo set_value('sponsor_link');  ?>" parsley-trigger="change" data-parsley-required-message="Please enter sponsor link" data-parsley-errors-container="#sponsorlink_error" required>
                                                        <label class="form-label">
                                                            Sponsor Link <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('sponsor_link'); ?>
                                                    <label id="sponsorlink_error"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('bio') || @$result['bio']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="bio" id="bio"  data-parsley-trigger="change" style="height: 70px" data-parsley-required-message="Please write bio" required><?php echo set_value('bio'); ?></textarea>
                                                        <label class="form-label">Bio<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('bio'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/sponsor" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.SPONSOR_LOGO.'default.png'; ?>')");
            });
            

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#sponsor_logo").change(function () {
                readURL(this);
            });

          

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>