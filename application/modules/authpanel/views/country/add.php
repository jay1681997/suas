<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Tax Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                           Tax Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/country">
                                Tax List </a>
                                </li>
                            <li class="breadcrumb-item active">
                              <a> Add Tax Details </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/country/add',array('id'=>'add_customer', 'name'=>'add_customer', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                       
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country Code
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="country_id" name="country_id" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#country_codeerror"  required >
                                                        <option value="">Select Country Code</option>
                                                        <?php $country_id = !empty(set_value('country_id')) ? set_value('country_id') : ''; ?>
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['id']; ?>" <?php echo ($country_id==$value['id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'].'('.$value['calling_code'].')'; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country_id'); ?>
                                                    <label id="country_codeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Region<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('region')) || !empty($result['region']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="region" id="region" value="<?php echo !empty(set_value('region')) ? set_value('region') : $result['region']; ?>" data-parsley-trigger="change" required>
                                                        
                                                    </div>
                                                    <?php echo form_error('region'); ?>
                                                </div>
                                            </div>
                                              <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                     <label class="form-label">Tax(%)<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('tax')) || !empty($result['tax']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="tax" id="tax" value="<?php echo !empty(set_value('tax')) ? set_value('tax') : $result['tax']; ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="0" max = "99.99" required>
                                                       
                                                    </div>
                                                    <?php echo form_error('tax'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        <div class="col-md-6 offset-md-5">
                                            <div class="form-group">
                                                <button type="submit" id="country" class="btn btn-raised bg-theme waves-effect">
                                                    Update
                                                </button>
                                                 <a href="<?php echo base_url();?>authpanel/country" class="btn btn-raised btn-default waves-effect">
                                                    Cancel
                                                </a>
                                            </div>
                                        </div>
                                           <!--  <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised g-bg-blue waves-effect m-t-20">
                                                       <?php echo $this->lang->line('rest_keywords_submit'); ?>
                                                    </button>
                                                    <a href="javascript:;" onclick="history.go(-1); return false;" class="btn btn-raised g-bg-blue waves-effect m-t-20">
                                                        <?php echo $this->lang->line('rest_keywords_cancel'); ?>
                                                    </a>
                                                </div>
                                            </div> -->
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                   $('.select2').select2({
                "width": "100%"
            });
                   $("#country_id").select2({
                    "width":"100%",   
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo COUNTRY_FLAG_IMAGE.$result['flag']; ?>')");
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#flag_image").change(function () {
                readURL(this);
            });
        </script>
    </body>
</html>