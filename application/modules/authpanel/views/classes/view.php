<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Class Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Class Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/classes">Class List</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a>Class Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- View -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card member-card">
                            <div class="header l-red">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>   
                                </div>
                            </div>
                                <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                <div class="member-img m-t-5">
                                    <img src="<?php echo S3_BUCKET_ROOT.CLASS_IMAGE.@$result['class_image']; ?>" class="rounded-circle" alt="profile-image" width="100" height="150px" style="margin-top: -7%;">
                                </div>
                                <div class="body">
                                    <div class="col-12">
                                        <ul class="list-inline">
                                            <li><h4 class="m-t-5"><?php echo @$result['program_title'];?></h4></li>
                                            <a href="<?php echo site_url('authpanel/classes/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                        </ul>
                                    </div>
                                <hr>
                                <div class="card">
                                    <div class="row text-left" >
                                        <div class="col-md-12">
                                            <div class="card">
                                                <h4 class=""><b>Class Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    <div class="about-info-p">
                                                        <strong>Age Category</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['age_category']; ?></p>
                                                    </div>
                                                    <!-- <div class="about-info-p">
                                                        <strong>Class Start Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo date('d M Y H:i A', strtotime($result['start_datetime'])); ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Class End Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo date('d M Y H:i A', strtotime($result['end_datetime'])); ?></p>
                                                    </div> -->
                                                    <div class="about-info-p">
                                                        <strong>Class Start Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $result['start_datetime']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Class End Datetime</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $result['end_datetime']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Grade</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['grade']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Total Spots</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['total_spot']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Total Point</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['total_point']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Price</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['currency'].''.@$result['price']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Description</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['description']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['status'] == 'Active')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted">'.@$result['status'].'</p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Added Date</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['insert_datetime'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- End View -->
    </div>
</div>      
</div>   
</section>
<!-- Jquery Core Js --> 
<?php $this->load->view('authpanel/common/scripts'); ?>
<script type="text/javascript">
    var table = $('#class_table');
    $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
    });

    /*
    ** Function for remove customer details
    */
    function remove(class_id) 
    {
        swal({   
            title: "Remove Class?",   
            text: "Are you sure you want to delete this Class?",   
            type: "error",   
            showCancelButton: true, 
            confirmButtonColor: "#fdd329",   
            confirmButtonText: "Confirm",   
            closeOnConfirm: false 
        },  function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url:  SITE_URL+"authpanel/classes/removeclasses/"+class_id,
                    type: "GET",
                    success: function(data) {
                        var url = SITE_URL+'authpanel/classes/';
                        window.location.href = url;
                    },
                    error: function(jqXHR, textStatus, errorThrown){},
                    complete: function(){}
                }); // END OF AJAX CALL
            }
        });
    }
</script>
</body>
</html>
