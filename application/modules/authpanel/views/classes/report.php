<?php
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
//header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
//header("Content-Type: application/vnd.ms-excel;charset=UTF-8");
header("Content-Type: application/download");
// header("Content-Disposition: attachment; filename=".date('Y-m-d')."_Customer_list.xls");
 header('Content-Disposition: attachment;filename="Gift_Class_list_('.date('Y-m-d').').xls"');
?>
<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered">
<thead>
    <tr>
      <td colspan="8" align="center"><font size="5">Gift Class List</font></td>
    </tr>
    
    <tr>
    <th>Id</th>
    <th>User Name</th>
    <th>User Email</th>
    <th>Class Name</th>
    <th>Age Category</th>
    <th>Status</th>
    <th>Last Login</th>
    <th>Signup Date</th>

</tr>
</thead>
<tbody>
<?php 
if(!empty($data_list))
{
  foreach ($data_list as $key => $row){  ?>
    <tr class="gradeX odd">
        <td><strong><?php echo $key+1;?></strong></td>
        <td><?php echo $row['username']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><?php echo $row['program_title']; ?></td>
        <td><?php echo $row['age_category'] ;?></td>
        <td><?php echo $row['status'] ;?></td>
        <td><?php if($row['last_login'] == '0000-00-00 00:00:00'){ echo '';}else{ echo date('m-d-Y H:i:s',strtotime($row['last_login']));} ?></td>
        <td><?php if($row['inserted_date'] == '0000-00-00 00:00:00'){ echo '';}else{ echo date('m-d-Y H:i:s',strtotime($row['inserted_date']));} ?></td>
    </tr>
<?php } }?> 
</tbody>
</table>