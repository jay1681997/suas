<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Class";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Class Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/class">
                                    Class List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Class Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?>
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/classes/edit/'.base64_encode($result['id']),array('id'=>'add_class', 'name'=>'add_class', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" style="height:100px;width: 100px;"/>
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Class Image <input type="file" id="class_image" name="class_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/gif, image/jpg"/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Program Title<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('program_title')) || !empty($result['program_title']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="program_title" id="program_title" value="<?php echo !empty(set_value('program_title')) ? set_value('program_title') : $result['program_title']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter program title"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('program_title'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Age Category<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="age_category" onchange="category_value(this)" id="age_category" parsley-trigger="change" data-parsley-errors-container="#age_categoryerror" data-parsley-required-message="Please select age category"  required>
                                                        <?php $age_categorys = !empty(set_value('age_category')) ? set_value('age_category') : $result['age_category']; ?>
                                                        <option value="">Select Age Category</option>
                                                        <option value="Adults" <?php echo ($age_categorys=='Adults') ? 'selected="selected"' : ''; ?>>Adults</option>
                                                        <option value="Teens" <?php echo ($age_categorys=='Teens') ? 'selected="selected"' : ''; ?>>Teens</option>
                                                        <option value="Juniors" <?php echo ($age_categorys=='Juniors') ? 'selected="selected"' : ''; ?>>Juniors</option>
                                                        <option value="Primary" <?php echo ($age_categorys=='Primary') ? 'selected="selected"' : ''; ?>>Primary</option>
                                                    </select>
                                                    <?php echo form_error('age_category'); ?>
                                                    <label id="age_categoryerror"></label>
                                                </div>
                                            </div>
                                               <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Class Start Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('start_datetime')) || !empty($result['start_datetime']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="start_datetime" name="start_datetime" data-parsley-trigger="change" data-parsley-required-message="Please select class start datetime" required value="<?php echo !empty(set_value('start_datetime')) ? set_value('start_datetime') : $result['start_datetime']; ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('start_datetime'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Class End Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('end_datetime')) || !empty($result['end_datetime']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="end_datetime" name="end_datetime" data-parsley-trigger="change" data-parsley-required-message="Please select class end datetime" required value="<?php echo !empty(set_value('end_datetime')) ? set_value('end_datetime') : $result['end_datetime']; ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('end_datetime'); ?>
                                                </div>
                                            </div>
                                         <!--    <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Class Start Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('start_datetime')) || !empty($result['start_datetime']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="start_datetime" name="start_datetime" data-parsley-trigger="change" data-parsley-required-message="Please select class start datetime" required value="<?php echo !empty(set_value('start_datetime')) ? set_value('start_datetime') : date('d-m-Y H:i', strtotime($result['start_datetime'])); ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('start_datetime'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Class End Datetime<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('end_datetime')) || !empty($result['end_datetime']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="end_datetime" name="end_datetime" data-parsley-trigger="change" data-parsley-required-message="Please select class end datetime" required value="<?php echo !empty(set_value('end_datetime')) ? set_value('end_datetime') : date('d-m-Y H:i', strtotime($result['end_datetime'])); ?>">
                                                       
                                                    </div>
                                                    <?php echo form_error('end_datetime'); ?>
                                                </div>
                                            </div> -->
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Price <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo (!empty(set_value('price')) || !empty($result['price']) ? 'focused' : '');?>">
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo !empty(set_value('price')) ? set_value('price') : $result['price']; ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" required>
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Grade <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line  <?php echo (!empty(set_value('grade')) || !empty($result['grade']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="grade" id="grade" value="<?php echo !empty(set_value('grade')) ? set_value('grade') : $result['grade']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter grade" data-parsley-errors-container="#gradeerr" required  onkeypress="return isNumberKey1(event);">
                                                    </div>
                                                    <?php echo form_error('grade'); ?>
                                                    <label id="gradeerr"></label>
                                                </div>
                                            </div> -->
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Total Spots <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('total_spot')) || !empty($result['total_spot']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_spot" id="total_spot" value="<?php echo !empty(set_value('total_spot')) ? set_value('total_spot') : $result['total_spot']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total spot" data-parsley-errors-container="#totalspoterr" required  onkeypress="return isNumberKey1(event);">
                                                    </div>
                                                    <?php echo form_error('total_spot'); ?>
                                                    <label id="totalspoterr"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Total Points <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('total_point')) || !empty($result['total_point']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_point" id="total_point" value="<?php echo !empty(set_value('total_point')) ? set_value('total_point') : $result['total_point']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total point number" data-parsley-errors-container="#totalpointerr" required  onkeypress="return isNumberKey1(event);">
                                                    </div>
                                                    <?php echo form_error('total_point'); ?>
                                                    <label id="totalpointerr"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label text-left">
                                                        Select Sub Admin <b style="color: #000;">(As Host)</b>
                                                        
                                                    </label>
                                                    <?php $subadmin_id = !empty(set_value('subadmin_id')) ? set_value('subadmin_id') : $result['subadmin_id']; ?>
                                                    <select class="form-control select2" id="subadmin_id" name="subadmin_id" data-parsley-trigger="change" data-parsley-required-message="Please select sub admin" data-parsley-errors-container="#subadmin_iderror"  onchange="validate_dropdown()">
                                                        <option value="0">Select Sub Admin</option>
                                                        <?php $subadmin_id = !empty(set_value('subadmin_id')) ? set_value('subadmin_id') : $result['subadmin_id']; ?>
                                                        <?php if(!empty($subadmins)){ foreach ($subadmins as $key => $value) { ?>
                                                            <option value="<?php echo $value['id']; ?>" <?php echo ($subadmin_id==$value['id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('subadmin_id'); ?>
                                                    <label id="subadmin_iderror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12 m-t-20">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description')) || !empty($result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" style="height: 100px" data-parsley-required-message="Please write description" required><?php echo !empty(set_value('description')) ? set_value('description') : $result['description']; ?></textarea>
                                                        <label class="form-label">Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>                        
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/classes"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/momentjs/moment.js"></script>
        <script type="text/javascript">
               function category_value(category_value) {
                 // var selectedText = category_value.options[category_value.selectedIndex].innerHTML;
                var selectedValue = category_value.value;
                const grade = document.getElementById('grade');
                // alert("Selected Text: " + selectedText + " Value: " + selectedValue);
                if(selectedValue != 'Adults'){
                    grade.setAttribute('required', '');
                }else{
                    grade.removeAttribute('required');
                }
            }
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                });   
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.CLASS_IMAGE.$result['class_image']; ?>')");
                     $("#imagePreview").css("background-size", "100% 100%");
                var start_datetime = $("#start_datetime").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: "today",
                    onClose: function(selDate,dateStr){
                        end_datetime.set('minDate',dateStr);
                    },
                });
                var end_datetime = $("#end_datetime").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: moment().format("YYYY-MM-DD HH:MM"),
                    onClose: function(selDate,dateStr){
                        start_datetime.set('maxDate',dateStr);
                    }
                });
            });
         
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#class_image").change(function () {
                readURL(this);
            });
            function validate_dropdown(){
                let country_code = $("#country_code").val();
                if(country_code != '' && country_code.length > 0){
                    $("#countrycodeerror").text('');
                } else {
                    $("#country_code").attr('required','required');
                }   
            }

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>