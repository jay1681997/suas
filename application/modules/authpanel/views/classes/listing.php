<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Class";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Class List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Class List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <?php if($this->session->flashdata('success_msg')){ ?>
              <div class="alert alert-success" >
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success_msg')?>
              </div>                
              <?php } ?>
              <?php if($this->session->flashdata('error_msg')){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('error_msg')?>
              </div>
              <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $error_msg; ?>
              </div>
            <?php } ?> 
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <div id="toolbar">
                          <a href="<?php echo base_url().'authpanel/classes/add/'; ?>" class="btn btn-raised bg-theme btn-xs waves-effect"> 
                            <b>
                              <i class="material-icons" style="top: -2px;">add</i>&nbsp;Add Class
                            </b>
                          </a>
                          <a href="<?php echo base_url().'authpanel/classes/enrolled_classlist/'; ?>" class="btn btn-raised bg-theme btn-xs waves-effect"> 
                            <b><i class="material-icons" style="top: -2px;">class</i>&nbsp;Enrolled Classes</b>
                          </a>
                          <a href="<?php echo base_url().'authpanel/classes/gift_classlist/'; ?>" class="btn btn-raised bg-theme btn-xs waves-effect"> 
                            <b><i class="material-icons" style="top: -2px;">redeem</i>&nbsp;Gifted Classes</b>
                          </a>
                        </div>
                        <table id="class_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/classes/ajax_list')?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="action" data-align="center">Action</th>
                              <th data-field="class_image"  data-align="center"> Class Image </th>
                              <th data-field="program_title" data-align="center" data-sortable="true">Program Title</th>
                              <th data-field="age_category" data-align="center" data-sortable="true">Age Category</th>
                              <th data-field="start_datetime" data-align="center" data-sortable="true">Start Datetime</th>
                              <th data-field="end_datetime" data-align="center" data-sortable="true">End Datetime</th>
                              <th data-field="grade" data-align="center" data-sortable="true">Grade</th>
                              <th data-field="total_spot" data-align="center" data-sortable="true">Total Spots</th>
                              <th data-field="total_point" data-align="center" data-sortable="true">Total Point</th>
                              <th data-field="price" data-align="center" data-sortable="true">Price</th>
                              <th data-field="subadmin_name" data-align="center">Sub Admin Name (As Host)</th>
                              <th data-field="hosting" data-align="center">Join(Host)</th>
                              <th data-field="status" data-align="center">Status</th>
                              <th data-field="insert_datetime" data-sortable="true">Added Date</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#class_table');
      $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
      });

      $(document).on('change','input[name="changestatus"]',function(){
        if($(this).prop("checked") == true){
          var status="Active";
        } else {
          var status="Inactive";
        }
        $.ajax({
          url:  SITE_URL+"authpanel/classes/changestatus/"+$(this).data('classid')+"/"+status,
          type: "GET",
          error: function(jqXHR, textStatus, errorThrown){
            showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
          },
          success: function(message){
            showNotification('alert-success',message ,'top','center','zoomIn animated','zoomOut animated');  
            $('#class_table').bootstrapTable('refresh');
          }
        }); 
      });

      function remove(class_id) 
      {
        swal({   
          title: "Remove Class?",   
          text: "Are you sure you want to delete this Class?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/classes/removeclasses/"+class_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#class_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
      }

     
    </script>
  </body>
 

</html>