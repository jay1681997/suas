<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Sub Admin";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Sub Admin Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/subadmin">
                                    Sub Admin List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Sub Admin Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/subadmin/add/',array('id'=>'add_class', 'name'=>'add_class', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Profile Image <input type="file" id="profile_image" name="profile_image" class="filestyle" data-parsley-trigger="change" data-parsley-required-message = "Please upload profile image" data-parsley-errors-container="#profile_error"  accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="profile_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('name')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter valid name" data-parsley-required-message="Please enter name" required>
                                                        <label class="form-label">Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('email')) ? 'focused' : '');?>">
                                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>" data-parsley-trigger="change" data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  data-parsley-pattern-message="Please enter valid email" data-parsley-required-message="Please enter email"  required>
                                                        <label class="form-label">Email<span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('password')) ? 'focused' : '');?>">
                                             
                                                        <input type="password" class="form-control" name="password" id="password"  data-parsley-required-message="Please enter password"  data-parsley-trigger="change" required>
                                                        <label class="form-label">
                                                            Password<span class="text-danger">*</span>
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('confirmpassword')) ? 'focused' : '');?>">
                                                 
                                                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" data-parsley-trigger="change"  data-parsley-required-message="Please enter confirm password" data-parsley-equalto="#password" data-parsley-equalto-message="Confirm password should be same as password" required>
                                                        <label class="form-label">
                                                            Confirm Password<span class="text-danger">*</span>
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country Code
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control select2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#country_codeerror"  required onchange="validate_dropdown()">
                                                        <option value="">Select Country Code</option>
                                                       
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['dial_code']; ?>"><?php echo $value['name'].'('.$value['dial_code'].')'; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country_code'); ?>
                                                    <label id="country_codeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 m-t-30">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('phone')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter phone number" data-parsley-minlength="10" data-parsley-maxlength="12" data-parsley-errors-container="#phoneerr" required onkeypress="return isNumberKey1(event);" maxlength="12">
                                                        <label class="form-label">
                                                            Phone Number <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('phone'); ?>
                                                     <label id="phoneerr"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="permission-list">
                                                    <div class="module-section">
                                                     <h4 class="m-t-20 text-left">Modules Permission</h4> 
                                                        <div class="row m-t-20"> 
                                                            <?php if(!empty($module)) { foreach ($module as $key => $sec_value) { ?>    
                                                                <div class="col-md-3 col-lg-3 col-xl-3">
                                                                    <input type="checkbox" id="section_<?php echo $sec_value['id'];?>" name="module[]" class="chk-col-red select allcheckbox section__<?php echo $sec_value['id'];?> sectioncheckbox" value="<?php echo $sec_value['id']; ?>">
                                                                    <label for="section_<?php echo $sec_value['id'];?>"><?php echo $sec_value['name'];?></label>
                                                                </div>              
                                                            <?php } } ?> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/subadmin" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.ADMIN_IMAGE.'default.png'; ?>')");
            });
            
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile_image").change(function () {
                readURL(this);
            });

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>