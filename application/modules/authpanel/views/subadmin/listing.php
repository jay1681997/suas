<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Sub Admin";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Sub Admin List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Sub Admin List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <?php if($this->session->flashdata('success_msg')){ ?>
              <div class="alert alert-success" >
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success_msg')?>
              </div>                
              <?php } ?>
              <?php if($this->session->flashdata('error_msg')){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('error_msg')?>
              </div>
              <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $error_msg; ?>
              </div>
            <?php } ?> 
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <div id="toolbar">
                          <a href="<?php echo base_url().'authpanel/subadmin/add/'; ?>" class="btn btn-raised bg-theme btn-xs waves-effect"> 
                            <b>
                              <i class="material-icons" style="top: -2px;">add</i>&nbsp;Add Sub Admin
                            </b>
                          </a>
                        </div>
                        <table id="class_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/subadmin/ajax_list')?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="action" data-align="center">Action</th>
                              <th data-field="profile_image"  data-align="center"> Sub Admin Image </th>
                              <th data-field="name" data-align="center" data-sortable="true">Name</th>
                              <th data-field="email" data-align="center" data-sortable="true">Email</th>
                              <th data-field="phone" data-align="center" data-sortable="true">Phone Number</th>
                              <th data-field="status" data-align="center">Status</th>
                              <th data-field="insert_datetime" data-sortable="true">Added Date</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#class_table');
      $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
      });

      $(document).on('change','input[name="changestatus"]',function(){
        if($(this).prop("checked") == true){
          var status="Active";
        } else {
          var status="Inactive";
        }
        $.ajax({
          url:  SITE_URL+"authpanel/subadmin/changestatus/"+$(this).data('subadminid')+"/"+status,
          type: "GET",
          error: function(jqXHR, textStatus, errorThrown){
            showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
          },
          success: function(message){
            showNotification('alert-success',message ,'top','center','zoomIn animated','zoomOut animated');  
            $('#class_table').bootstrapTable('refresh');
          }
        }); 
      });

      function remove(subadmin_id) 
      {
        swal({   
          title: "Remove Sub Admin?",   
          text: "Are you sure you want to delete this Sub Admin?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/subadmin/removesubadmin/"+subadmin_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#class_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
      }

     
    </script>
  </body>
 

</html>