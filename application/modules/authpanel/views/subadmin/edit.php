<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Sub Admin";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Sub Admin Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/subadmin">
                                    Sub Admin List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Sub Admin Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/subadmin/edit/'.base64_encode($result['id']),array('id'=>'add_class', 'name'=>'add_class', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Profile Image <input type="file" id="profile_image" name="profile_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/gif, image/jpg"/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Name<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('name')) || !empty($result['name']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo !empty(set_value('name')) ? set_value('name') : $result['name']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter name"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Email<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('email')) || !empty($result['email']) ? 'focused' : '');?>">
                                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo !empty(set_value('email')) ? set_value('email') : $result['email']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter email"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>
                                             <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="password" style="display: none;">
                                                        <input type="password" class="form-control" name="password" id="password"  data-parsley-required-message="Please enter password"  data-parsley-trigger="change" >
                                                        <label class="form-label">
                                                            Password
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="password" style="display: none;">
                                                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" data-parsley-trigger="change"  data-parsley-required-message="Please enter confirm password" data-parsley-equalto="#password" data-parsley-equalto-message="Confirm password should be same as password" >
                                                        <label class="form-label">
                                                            Confirm Password
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                           <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country Code
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control select2" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country Code" data-parsley-errors-container="#countrycodeerror"  required>
                                                        <option value="">Select Country Code</option>
                                                        <?php $country_code = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?> 
                                                        <?php //echo $country_code; die; ?>
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['dial_code']; ?>" <?php echo ($country_code==$value['dial_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'].'('.$value['dial_code'].')'; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country_code'); ?>
                                                    <label id="countrycodeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label text-left"></label>
                                                    <div class="form-line <?php echo (!empty(set_value('phone')) || !empty($result['phone']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>" parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter phone number" data-parsley-minlength="10" data-parsley-maxlength="12"data-parsley-errors-container="#phoneerr" required maxlength="12" onkeypress="return isNumberKey1(event);">
                                                        <label class="form-label" style="margin-top: -5px">
                                                            Phone Number <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('phone'); ?>
                                                    <label id="phoneerr"></label>
                                                </div>
                                            </div>  
                                             <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="permission-list">
                                                    <div class="module-section">
                                                        <h4 class="m-t-20 text-left">Modules Permission</h4>
                                                         <div class="row m-t-20">
                                                    <?php if(!empty($assign_module)) { foreach ($assign_module as $key => $sec_value) { ?>
                                                           
                                                            <div class="col-md-3 col-lg-3 col-xl-3">
                                                                <input type="checkbox" id="section_<?php echo $sec_value['id'];?>" name="module[]" class="chk-col-red select allcheckbox section__<?php echo $sec_value['id'];?> sectioncheckbox" value="<?php echo $sec_value['id']; ?>" <?php echo ($sec_value['is_select']=="1") ? "checked" : ""; ?> onchange="check_chataccess(this.value);">
                                                                <label for="section_<?php echo $sec_value['id'];?>"><?php echo $sec_value['name'];?></label>
                                                            </div>
                                                    <?php } } ?>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>                  
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/subadmin"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>

        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                });   
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.ADMIN_IMAGE.$result['profile_image']; ?>')");
             
            });
         
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile_image").change(function () {
                readURL(this);
            });
            function validate_dropdown(){
                let country_code = $("#country_code").val();
                if(country_code != '' && country_code.length > 0){
                    $("#countrycodeerror").text('');
                } else {
                    $("#country_code").attr('required','required');
                }   
            }

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>