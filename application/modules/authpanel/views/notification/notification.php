<!doctype html>
<html class="no-js " lang="en">

<head>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/twitter-select-bootstrap.css">
    <?php $data['title'] = "Notification";
    $this->load->view('authpanel/common/stylesheet', $data);  ?>
    <!-- Bootstrap Material Datetime Picker Css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" />
    <style type="text/css">
        .navbar-header {
            height: 60px;
        }

        .dropup .dropdown-toggle::after {
            display: none;
        }

        .dropdown-toggle::after {
            display: none;
        }

        .navbar-header .navbar-brand {
            font-size: 19.2px;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu li.selected a span.check-mark {
            display: none;
        }
    </style>
</head>

<body class="<?php echo THEME_COLOR; ?>">
    <!-- Top Bar -->
    <?php $this->load->view('authpanel/common/header'); ?>
    <!-- Left Sidebar -->
    <?php $data['pagename'] = "Users List";
    $data['subpagename'] = "Notification";
    $this->load->view('authpanel/common/left-menu', $data); ?>
    <!-- Right Sidebar -->
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.js"></script>
    <style type="text/css">
        .dropdown-menu.open {
            overflow: unset !important;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
            margin-right: 34px;
            margin-left: 34px !important;
        }

        .bootstrap-select.btn-group .dropdown-toggle .filter-option {
            display: inline-block;

        }

        .bootstrap-select>.dropdown-toggle.bs-placeholder,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:hover,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:focus,
        .bootstrap-select>.dropdown-toggle.bs-placeholder:active {
            color: #fff;
            /*padding-left: 20px;*/
        }

        .bootstrap-select .dropdown-menu li.selected a {
            background-color: #51b1d7 !important;
            color: #555 !important;
        }

        .btn:not(.btn-link):not(.btn-circle) {
            border: 1px solid #101111;
        }

        .btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle):hover {
            cursor: pointer !important;
            background-color: #6aa2e8 !important;
        }

        .btn-group>.btn:last-child:not(:first-child):not(.dropdown-toggle):hover {
            cursor: pointer !important;
            background-color: #6aa2e8 !important;
        }

        .bootstrap-select.btn-group.show-tick .dropdown-menu.inner {
            max-height: 150px !important;
        }
    </style>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>
                        Send Notification
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/dashboard">
                                <i class="zmdi zmdi-home"></i><span style="margin-left: 20px">Dashboard</span>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/notification">
                                Notification</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Send Notification
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid animated zoomIn">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="header ">
                            <div class="col-12 text-left">
                                <!-- <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-black text-left">
                                        <i class="zmdi zmdi-arrow-left"></i>
                                    </a> -->

                                <!-- <div class="col-12 text-right">
                                        <a href="<?php echo base_url(); ?>authpanel/notification/history"  class="btn btn-raised bg-theme btn-xs waves-effect">
                                            <i class="zmdi zmdi-time-restore"></i><span><b> History</b></span>
                                        </a>
                                    </div> -->

                            </div>
                        </div>
                        <div class="bg-custom bg-theme button-color-css"></div>
                        <div class="body">
                            <div class="col-md-8 col-lg-8 col-xl-8 offset-md-1">
                                <?php if ($this->session->flashdata('success_msg')) { ?>
                                    <div class="alert alert-success">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('success_msg') ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('error_msg')) { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('error_msg') ?>
                                    </div>
                                <?php } ?> <?php if (isset($error_msg) && $error_msg != '') { ?>
                                    <div class="alert alert-danger alert-dismissable zoomIn animated">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $error_msg; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                <?php echo form_open('authpanel/notification/send_push', array('user_id' => 'edit_user', 'name' => 'edit_user', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post')); ?>


                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-10 col-lg-10 col-xl-10 ">
                                        <div class="form-group">
                                            <label class="form-label text-left">
                                                Enter Message
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="form-line <?php echo !empty(set_value('message')) ? 'focused' : ''; ?>">
                                                <textarea row="50" class="form-control" name="message" id="message" parsley-trigger="change" data-parsley-required-message="Please enter message" required><?php echo set_value('message'); ?></textarea>
                                            </div>
                                            <?php echo form_error('message'); ?>
                                        </div>
                                    </div>
                                    <!-- <?php echo "<pre>";
                                            print_r($users); ?> -->
                                    <div class="clearfix"></div>
                                    <div class="col-md-10 col-lg-10 col-xl-10">
                                        <div class="form-group">

                                            <select class="form-control select2"  multiple="multiple" id="users" name="users[]" data-parsley-trigger="change" title="Select Users" data-actions-box='true' data-live-search="true" data-style="btn-primary" style="color: #fff" parsley-trigger="change" data-parsley-errors-container="#userserrors" data-parsley-required-message="Please select user" required onchange="validate()">
                                           
                                                <?php $users1 = !empty(set_value('users')) ? set_value('users') : ''; ?>
                                                <?php if (!empty($users)) {
                                                    foreach ($users as $key => $value) { ?>
                                                       <option value="<?php echo $value['id']; ?>" data-tokens="<?php echo $value['username']; ?>" <?php echo set_select('users[]', $value['id'], false); ?>><?php echo ucfirst($value['username']); ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                            <?php echo form_error('users'); ?>
                                            <label id="userserror"></label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-10 col-lg-10 col-xl-10 ">
                                                    <div class="form-group">
                                                        <select class="form-control ms selectpicker" id="users" name="users[]" parsley-trigger="change" data-parsley-errors-container="#userserrors" multiple="multiple" placeholder="Users" title="Select Users" data-actions-box='true' data-live-search="true" data-style="btn-primary" style="color: #fff" parsley-trigger="change" data-parsley-errors-container="#userserrors" data-parsley-required-message="Please select user" required onchange="validate()">
                                                            <?php if (!empty($users)) {
                                                                foreach ($users as $key => $value) { ?>
                                                                <option value="<?php echo $value['id']; ?>" data-tokens="<?php echo $value['username']; ?>" <?php echo set_select('users[]', $value['id'], false); ?>><?php echo ucfirst($value['username']); ?></option>
                                                            <?php }
                                                            } ?>
                                                        </select>
                                                        <?php echo form_error('users'); ?>
                                                        <label id="userserrors"></label>
                                                    </div>
                                                </div> -->
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6 offset-md-5" style="margin-left: 250px">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-raised bg-theme waves-effect" style="margin:0 auto;" onsubmit="validate()">
                                            Send
                                        </button>
                                        <a href="<?php echo base_url(); ?>authpanel/notification" class="btn btn-raised btn-default waves-effect">
                                            Cancel
                                        </a>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script src="<?php echo base_url(); ?>assets/plugins/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js?<?php echo time(); ?>"></script>
    <script>
        autosize(document.querySelectorAll('textarea'));
    </script>
    <script type="text/javascript">
        $('.selectpicker').selectpicker({
            noneResultsText: 'I found no results'
        });
        $(".select2").select2({
            "width": "100%"
        });

        function submitNotification() {
            return false;
        }

        function validate() {
            var user = $('#users').val();
            var driver = $('#drivers').val();
            if ((user != '' && user.length > 0) && (driver != '' && driver.length > 0)) {
                $("#userserrors").text('');
                $("#driverserrors").text('');
            } else {
                $("#users").attr('required', 'required');

                $("#drivers").attr('required', 'required');
            }
        }
    </script>

</body>

</html>