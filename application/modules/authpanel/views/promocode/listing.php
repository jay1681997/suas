<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Promocodes";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Promocodes List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Promocodes List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <div id="toolbar">
                          <a href="<?php echo base_url().'authpanel/promocode/add/'; ?>" class="btn btn-raised bg-theme btn-xs waves-effect"> 
                            <b>
                              <i class="zmdi zmdi-local-offer zmdi-hc-fw"></i>&nbsp;Add
                            </b>
                          </a>
                        </div>
                        <table id="promo_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/promocode/ajax_list')?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="action" data-align="center">Action</th>       
                              <th data-field="promo_image" data-align="center">Promocode Image</th>                       
                              <th data-field="promocode"data-sortable="true" data-align="center">Name</th>
                              <th data-field="discount_type" data-sortable="true" data-align="center">Promocode Type</th>
                              <th data-field="discount" data-sortable="true" data-align="center">Offer</th>
                              <th data-field="maxusage" data-sortable="true" data-align="center">Max Usability</th>
                              <th data-field="per_user_usage" data-sortable="true" data-align="center">Per User Limit</th>
                              <th data-field="start_date" data-sortable="true" data-align="center">Start Date</th>
                              <th data-field="end_date" data-sortable="true" data-align="center">End Date</th>
                              <th data-field="price" data-sortable="true" data-align="center">Price</th>
                              <th data-field="status" data-sortable="true" data-align="center">Status</th>
                              <th data-field="inserted_date" data-sortable="true">Created On</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#promo_table');
      $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
      });
      $('#promo_table').on('load-success.bs.table', function (data) {
        $('.image-popup').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          mainClass: 'mfp-fade',
          gallery: {
            enabled: false,
            navigateByImgClick: true,
            preload: [0,1]
          }
        });
      });

      $(document).on('change','input[name="changestatus"]',function(){
        if($(this).prop("checked") == true){
          var status="Active";
        } else {
          var status="Inactive";
        }
        $.ajax({
          url:  SITE_URL+"authpanel/promocode/changestatus/"+$(this).data('promocodeid')+"/"+status,
          type: "GET",
          error: function(jqXHR, textStatus, errorThrown){
            showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
          },
          success: function(message){
            showNotification('alert-success',message,'top','center','zoomIn animated','zoomOut animated');  
            $('#promo_table').bootstrapTable('refresh');
          }
        }); 
      });

      function remove(promocode_id) 
      {
        swal({   
          title: "Remove promocode?",   
          text: "Are you sure you want to delete this promocode?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/promocode/removepromocode/"+promocode_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#promo_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
      }
    </script>
  </body>
</html>