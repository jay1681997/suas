<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Promocode Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Promocode Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/promocode">Promocode List</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a>Promocode Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- View -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card member-card">
                            <div class="header l-red">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>   
                                </div>
                                <!-- <ul class="list-inline">
                                    <li><h4 class="m-t-5"><?php echo @$result['promocode'];?></h4></li>
                                    <a href="<?php echo site_url('authpanel/promocode/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                    <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                </ul> -->
                            </div>
                            <div class="bg-custom bg-profile  button-color-css">
                            </div>
                                <div class="member-img m-t-5">
                                    <img src="<?php echo S3_BUCKET_ROOT.PROMOCODE_IMAGE.@$result['promocode_image']; ?>" class="rounded-circle" alt="category-image" width="100" height="150px" style="margin-top: -7%;">
                                </div>
                                <div class="body">
                                    <div class="col-12">
                                        <ul class="list-inline">
                                            <li><h4 class="m-t-5"><?php echo @$result['promocode'];?></h4></li>
                                            <a href="<?php echo site_url('authpanel/promocode/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                        </ul>
                                    </div>
                                <hr>
                            <div class="body">
                                <div class="col-12">
                                <div class="card">
                                    <div class="row text-left" >
                                        <div class="col-md-6">
                                            <div class="card">
                                                <h4 class=""><b>Promocode Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    <div class="about-info-p">
                                                        <strong>Discount Type</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['discount_type']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Discount</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['discount']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Max Usability</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['maxusage']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                    <strong>Per User Limit</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo @$result['per_user_usage']; ?></p>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Description</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo @$result['description']; ?></p>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Benefits</strong>
                                                    <br>
                                                    <ul>
                                                    <?php foreach ($benefits as $key => $value) { ?>
                                                        <li class="text-muted"><?php echo @$value['benefits']; ?></li>
                                                    <?php } ?>
                                                    </ul>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h4 class=""><b>Other Information</b></h4>
                                            <hr>
                                            <div class="p-20">
                                                <div class="about-info-p">
                                                    <strong>Price</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo @$result['price']; ?></p>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Start Date</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo @$result['start_date']; ?></p>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>End Date</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo @$result['end_date']; ?></p>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Status</strong>
                                                    <br>
                                                    <?php 
                                                    if(@$result['status'] == 'Active')
                                                        echo '<b><p class="text-muted"><span class="text-success">'.@$result['status'].'</span></p></b>';
                                                    else 
                                                        echo '<b><p class="text-muted">'.@$result['status'].'</p></b>';
                                                    ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Created On</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['insert_datetime'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- End View -->
    </div>
</div>      
</div>   
</section>
<!-- Jquery Core Js --> 
<?php $this->load->view('authpanel/common/scripts'); ?>
<script type="text/javascript">
    /*
    ** Function for remove customer details
    */
    function remove(promocode_id) 
    {
        swal({   
            title: "Remove promocode?",   
            text: "Are you sure you want to delete this promocode?",   
            type: "error",   
            showCancelButton: true, 
            confirmButtonColor: "#fdd329",   
            confirmButtonText: "Confirm",   
            closeOnConfirm: false 
        },  function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url:  SITE_URL+"authpanel/promocode/removepromocode/"+promocode_id,
                    type: "GET",
                    success: function(data) {
                        var url = SITE_URL+'authpanel/promocode/';
                        window.location.href = url;
                    },
                    error: function(jqXHR, textStatus, errorThrown){},
                    complete: function(){}
                }); // END OF AJAX CALL
            }
        });
    }
</script>
</body>
</html>
