<!doctype html>
<html class="no-js " lang="en">

<head>
    <?php $data['title'] = "Edit promocode";
    $this->load->view('authpanel/common/stylesheet', $data);  ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css" />
    <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
</head>

<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>
                        Edit promocode Details
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/dashboard">
                                <i class="zmdi zmdi-home"></i>Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url(); ?>authpanel/promocode">
                                promocode List</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Edit promocode Details
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <?php if (isset($error_msg) && $error_msg != '') { ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                    <?php } ?>
                    <div class="card">
                        <div class="body">
                            <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                <?php echo form_open('authpanel/promocode/edit/' . base64_encode($result['id']), array('id' => 'edit_promocode', 'name' => 'edit_promocode', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype' => 'multipart/form-data')); ?>
                                <div class="form-group">
                                    <div class="text-center preview_holder">
                                        <img id="imagePreview" class="img-responsive img-thumbnail" />
                                    </div>
                                    <div class="text-center m-b-10">
                                        <div class="form-group text-center">
                                            <div class="">
                                                <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                    Upload Promocode Image <input type="file" id="promo_image" name="promo_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/gif, image/jpg" />
                                                </span>
                                                <?php echo form_error('image'); ?>
                                                <?php if (isset($img_error_msg) && $img_error_msg != '') { ?><ul class="parsley-errors-list filled">
                                                        <li class="parsley-required"><?php echo $img_error_msg; ?></li>
                                                    </ul><?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Promocode<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('promocode')) || !empty($result['promocode']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="promocode" id="promocode" value="<?php echo !empty(set_value('promocode')) ? set_value('promocode') : $result['promocode']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter promocode" required>

                                            </div>
                                            <?php echo form_error('promocode'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Price <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <div class="form-line <?php echo (!empty(set_value('price')) || !empty($result['price']) ? 'focused' : ''); ?>">
                                                    <input type="text" class="form-control" name="price" id="price" value="<?php echo !empty(set_value('price')) ? set_value('price') : $result['price']; ?>" data-parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter price" min="1" required>
                                                    <?php echo form_error('price'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="form-label">Discount Type<span class="text-danger">*</span></label>
                                            <select class="form-control select2" id="discount_type" name="discount_type" parsley-trigger="change" data-parsley-errors-container="#discount_typeerror" data-parsley-required-message="Please select discount" required>
                                                <?php $discounttype = !empty(set_value('discount_type')) ? set_value('discount_type') : $result['discount_type']; ?>
                                                <option value="">Select Discount Type</option>
                                                <option value="flat" <?php echo ($discounttype == 'flat') ? 'selected' : ''; ?>>Flat</option>
                                                <option value="percentage" <?php echo ($discounttype == 'percentage') ? 'selected' : ''; ?>>Percentage</option>

                                            </select>
                                            <?php echo form_error('discount_type'); ?>
                                            <label id="discount_typeerror"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label" id="discountlabel">Discount<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('discount')) || !empty($result['discount']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="discount" id="discount" value="<?php echo !empty(set_value('discount')) ? set_value('discount') : $result['discount']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter discount" required>

                                            </div>
                                            <?php echo form_error('discount'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Start Date<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('start_date')) || !empty($result['start_date']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="start_date" id="start_date" value="<?php echo !empty(set_value('start_date')) ? set_value('start_date') : $result['start_date']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please select start date" required>
                                            </div>
                                            <?php echo form_error('start_date'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">End Date<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('end_date')) || !empty($result['end_date']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="end_date" id="end_date" value="<?php echo !empty(set_value('end_date')) ? set_value('end_date') : $result['end_date']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please select end date" required>
                                            </div>
                                            <?php echo form_error('end_date'); ?>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Max Usage Limit<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('maxusage')) || !empty($result['maxusage']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="maxusage" id="maxusage" value="<?php echo !empty(set_value('maxusage')) ? set_value('maxusage') : $result['maxusage']; ?>" data-parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter max usage limit" required>

                                            </div>
                                            <?php echo form_error('maxusage'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group form-float">
                                            <label class="form-label">Per User Usage Limit<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('per_user_usage')) || !empty($result['per_user_usage']) ? 'focused' : ''); ?>">
                                                <input type="text" class="form-control" name="per_user_usage" id="per_user_usage" value="<?php echo !empty(set_value('per_user_usage')) ? set_value('per_user_usage') : $result['per_user_usage']; ?>" data-parsley-trigger="change" data-parsley-type="number" 
                                                max="<?php echo !empty(set_value('maxusage')) ? set_value('maxusage') : $result['maxusage']; ?>" data-parsley-required-message="Please enter per user usage limit" required>

                                            </div>
                                            <?php echo form_error('per_user_usage'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="form-label text-left">
                                                Type
                                                <span class="text-danger">*</span>
                                            </label>
                                            <?php $type_id = !empty(set_value('type')) ? set_value('type') : $result['type']; ?>
                                            <select class="form-control select2" name="type" id="type" parsley-trigger="change" data-parsley-errors-container="#typeerror" required>
                                                <option value="">Select Type</option>
                                                <option value="1" <?php echo ($type_id == '1') ? 'selected="selected"' : ''; ?>>Private</option>
                                                <option value="0" <?php echo ($type_id == '0') ? 'selected="selected"' : ''; ?>>Public</option>
                                            </select>
                                            <?php echo form_error('type'); ?>
                                            <label id="typeerror"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 col-lg-6 col-xl-6">
                                        <div class="form-group">
                                            <label class="form-label text-left">User List <span class="text-danger"></span></label>
                                            <?php $user_ids = !empty(set_value('user_ids')) ? set_value('user_ids') :  $promocode_user_list; ?>
                                            <select class="form-control select2" name="user_ids[]" id="user_ids" data-parsley-errors-container="#user_ids_error" data-parsley-trigger="change" onchange="validate_field('user_ids')" data-parsley-required-message="Please select event name" multiple>
                                                <?php if (!empty($user_list)) {
                                                    foreach ($user_list as $key => $value) { ?>
                                                        <option value="<?php echo $value['id']; ?>" <?php echo (in_array($value['id'], $user_ids)) ? 'selected' : '' ?>><?php echo $value['username']; ?></option>
                                                <?php }
                                                } ?>
                                            </select>
                                            <?php echo form_error('user_ids'); ?>
                                            <label id="event_id_error"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <div class="form-group form-float">
                                            <label class="form-label">Description<span class="text-danger">*</span></label>
                                            <div class="form-line <?php echo (!empty(set_value('description') || @$result['description']) ? 'focused' : ''); ?>">
                                                <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description" data-parsley-trigger="change" data-parsley-required-message="Please write description" required><?php echo !empty(set_value('description')) ? set_value('description') : @$result['description']; ?></textarea>

                                            </div>
                                            <?php echo form_error('description'); ?>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12 col-lg-12 col-xl-12 after-add-more">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Benefits</label>
                                                    <div class="form-line">
                                                        <input type="text" class="form-control change" id="benfits" name="benefits[]" data-parsley-trigger="change">
                                                        
                                                    </div><?php echo form_error('benefit'); ?>
                                                </div>
                                            </div> -->
                                    <?php if (!empty($benfits)) {
                                        foreach ($benfits as $key => $value) { ?>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float"><label class="form-label">Benefits</label>
                                                    <div class="form-line"><input type="text" class="form-control change" id="benfits" name="benefits[]" data-parsley-trigger="change" value="<?php echo $value['benefits']; ?>"></div><?php echo form_error('benefit'); ?>
                                                </div>
                                            </div>
                                    <?php }
                                    } ?>
                                    <div class="col-md-12 col-lg-12 col-xl-12">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-raised g-bg-blue waves-effect m-t-20" id="add-more">Add Benefits</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 offset-md-5">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                Update
                                            </button>
                                            <a href="<?php echo base_url(); ?>authpanel/promocode" class="btn btn-raised btn-default waves-effect m-t-20">
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script src="<?php echo base_url(); ?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/momentjs/moment.js"></script>
    <script>
        autosize(document.querySelectorAll('textarea'));
    </script>
    <script type="text/javascript">
               $("#maxusage").on('change',function(){
                  var x = document.getElementById("maxusage").value;
                 
          const inputElement = document.getElementById('per_user_usage');
  inputElement.max = x;
    });
        $(document).ready(function(e) {
            $('.select2').select2({
                "width": "100%"
            });
            $("#imagePreview").css("background-image", "url('')");

            var start_date = $("#start_date").flatpickr({
                // enableTime: !0,
                // noCalendar: !0,
                dateFormat: "Y-m-d",
                minDate: moment().format("YYYY-MM-DD"),
                onClose: function(selDate, dateStr) {
                    end_date.set('minDate', dateStr);
                },
            });
            var end_date = $("#end_date").flatpickr({
                // enableTime: !0,
                // noCalendar: !0,
                dateFormat: "Y-m-d",
                minDate: moment().format("YYYY-MM-DD"),
                onClose: function(selDate, dateStr) {
                    start_date.set('maxDate', dateStr);
                }
            });
            $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT . PROMOCODE_IMAGE . $result['promocode_image']; ?>')");

            // $("body").on("click","#add-more",function(){ 
            //     var html = $(".after-add-more").first().clone();
            //     $(html).find(".change").html("<a class='btn btn-danger remove'>- Remove</a>");
            //     $(".after-add-more").last().after(html);
            // });
            // $("body").on("click",".remove",function(){ 
            //     $(this).parents(".after-add-more").remove();
            // });
        });

        $('#discount_type').on('change', function() {
            if (this.value === 'percentage') {
                $("#discountlabel").text("Percentage");
            } else {
                $("#discountlabel").text("Amount");
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $("#imagePreview").css("background-image", "url(" + e.target.result + ")");
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#promo_image").change(function() {
            readURL(this);
        });


        $(document).on('click', '#add-more', function() {
            var html = '<div class="col-md-12 col-lg-12 col-xl-12"><div class="form-group form-float"><label class="form-label">Benefits</label><div class="form-line"><input type="text" class="form-control change" id="benfits" name="benefits[]" data-parsley-trigger="change"></div><?php echo form_error('benefit'); ?></div></div>';
            $(this).parent().append(html);
        });

        $(".remove").click(function() {
            $(this).parents("#add-more").remove();
        });
    </script>
</body>

</html>