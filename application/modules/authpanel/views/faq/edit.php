<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit FAQ";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit FAQ Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/faq">
                                    FAQ's List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit FAQ Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/faq/edit/'.base64_encode($result['id']),array('id'=>'add_faq', 'name'=>'add_faq', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        
                                        <div class="row clearfix">
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('question')) || !empty($result['question']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="question" id="question"  data-parsley-trigger="change" required style="overflow: hidden; overflow-wrap: break-word; height: 50px;" data-parsley-required-message="Please enter question"><?php echo !empty(set_value('question')) ? set_value('question') : $result['question']; ?></textarea>
                                                        <label class="form-label">Question<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('question'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('answer')) || !empty($result['answer']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="answer" id="answer"  data-parsley-trigger="change" required style="overflow: hidden; overflow-wrap: break-word; height: 50px;" data-parsley-required-message="Please enter answer"><?php echo !empty(set_value('answer')) ? set_value('answer') : $result['answer']; ?></textarea>
                                                        <label class="form-label">Answer<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('answer'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-3">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url();?>authpanel/faq"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.min.js"></script>
        <script type="text/javascript">
            autosize(document.querySelectorAll('textarea'));
            $(document).ready(function(e) {
                $("#country_code").select2({
                    "width":"100%",
                    templateResult: CountryTemplate,
                    templateSelection: CountryTemplate
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.USER_IMAGE.$result['profile_image']; ?>')");
            });
            function CountryTemplate (country) {
                if (!country.id) {
                    return country.text;
                }
                var $country = $(
                    '<span><img src="'+$(country.element).data('flagimage')+'" width="20px" height="20px" class="img-flag"/> '+country.text+'</span>'
                );
                $('#sort_code').val($(country.element).data('sort_code'));
                return $country;
            };
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile_image").change(function () {
                readURL(this);
            });
        </script>
    </body>
</html>