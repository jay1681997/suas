<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Prize";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Prize Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/prize">
                                    Prize List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Prize Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/prize/add/',array('id'=>'add_prize', 'name'=>'add_prize', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" style="height:100px;width: 100px;"/>
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Prize Image <input type="file" id="prize_image" name="prize_image" class="filestyle" data-parsley-trigger="change"  
                                                                data-parsley-required-message = "Please upload prize image"
                                                                 data-parsley-errors-container="#prize_error"  accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="prize_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label">Contest<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="contest_id" id="contest_id" parsley-trigger="change" data-parsley-errors-container="#contest_iderror" data-parsley-required-message="Please select contest"  required>
                                                        <option value="">Select Contest</option>
                                                        <?php foreach($contests as $key => $value){ ?> 
                                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <?php echo form_error('contest_id'); ?>
                                                    <label id="contest_iderror"></label>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6 col-lg-6 col-xl-6 m-t-30">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('prize_name')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="prize_name" id="prize_name" value="<?php echo set_value('prize_name'); ?>" data-parsley-trigger="change"data-parsley-required-message="Please enter prize name" data-parsley-errors-container="#error" required>
                                                        <label class="form-label">Prize Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('prize_name'); ?>
                                                </div>
                                            </div> -->
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Prize
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control select2" id="prize_name" name="prize_name" data-parsley-trigger="change" data-parsley-required-message="Please select price name" data-parsley-errors-container="#country_codeerror"  required >
                                                        <?php $prize_name = !empty(set_value('prize_name')) ? set_value('prize_name') : ''; ?>
                                                        <option value="1st Prize" <?php echo ($prize_name == '1st Prize')? 'selected="selected"' : ''; ?>>1st Prize</option>
                                                        <option value="2st Prize" <?php echo ($prize_name == '2st Prize') ? 'selected="selected"' : ''; ?>>2st Prize</option>
                                                        <option value="3st Prize" <?php echo ($prize_name == '3st Prize') ? 'selected="selected"' : ''; ?>>3st Prize</option>
                                                        <option value="4st Prize" <?php echo ($prize_name == '4st Prize')? 'selected="selected"' : ''; ?>>4st Prize</option>
                                                        <option value="5st Prize" <?php echo ($prize_name == '5st Prize') ? 'selected="selected"' : ''; ?>>5st Prize</option>
                                                        <option value="6st Prize" <?php echo ($prize_name == '6st Prize') ? 'selected="selected"' : ''; ?>>6st Prize</option>
                                                        <option value="7st Prize" <?php echo ($prize_name == '7st Prize')? 'selected="selected"' : ''; ?>>7st Prize</option>
                                                        <option value="8st Prize" <?php echo ($prize_name == '8st Prize') ? 'selected="selected"' : ''; ?>>2st Prize</option>
                                                        <option value="9st Prize" <?php echo ($prize_name == '9st Prize') ? 'selected="selected"' : ''; ?>>3st Prize</option>
                                                    </select>
                                                    <?php echo form_error('prize_name'); ?>
                                                    <label id="prize_name"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('credit_points')) ? 'focused' : '');?>">
                                                                                                                <label class="">
                                                            Credit Points <span class="text-danger">*</span>
                                                        </label>
                                                        <input type="text" class="form-control" name="credit_points" id="credit_points" value="<?php echo set_value('credit_points');  ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter credit points" data-parsley-errors-container="#creditpoints_error" required  onkeypress="return isNumberKey1(event);" onkeyup = "Count_price();">

                                                    </div>
                                                    <?php echo form_error('credit_points'); ?>
                                                    <label id="creditpoints_error"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="input-group">
                                                       
                                                        <div class="form-line <?php echo !empty(set_value('price')) ? 'focused' : ''; ?>">
                                                            <label class="">Price <span class="text-danger">*</span></label>   
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo set_value('price'); ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" disabled>
                                                           
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description') || @$result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" style="height: 100px" data-parsley-required-message="Please write description" required><?php echo set_value('description'); ?></textarea>
                                                        <label class="form-label">Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/prize" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                }); 
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.PRIZE_IMAGE.'default.png'; ?>')");
                 $("#imagePreview").css("background-size", "100% 100%");
            });
            

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#prize_image").change(function () {
                readURL(this);
            });

          

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
              function Count_price(){
               const val = document.getElementById('credit_points').value;
               var total_price = ((10 * val)/100);   
               document.getElementById("price").value = total_price;
            }
        </script>
    </body>
</html>