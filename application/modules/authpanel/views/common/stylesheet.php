<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="<?php echo PROJECT_NAME." Admin Portal"; ?>">
<title><?php echo PROJECT_NAME; echo ($title != '')?' | ' .$title:''; ?></title>
<link rel="icon" href="<?php echo base_url().LOGO_NAME;?>" type="image/x-icon">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/color_skins.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/parsley/parsley.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate_page.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/customization.css">
<link href="<?php echo base_url();?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/fontawesome.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .img-thumbnail{
        height: 120px;
    background-size: cover;
    width: 120px;
    }
</style>
<script type="text/javascript">
    const SITE_URL = '<?php echo base_url();?>';
</script>

    
