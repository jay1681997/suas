<footer class="footer">
  <div class="card" style="margin-bottom: 0px;">
      <div class="body">
          <p class="m-b-0">Copyright © <?php echo date('Y')." ".PROJECT_NAME; ?> | All Rights Reserved </p>
      </div>
  </div>
</footer>