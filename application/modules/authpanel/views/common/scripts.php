<script src="<?php echo base_url();?>assets/bundles/libscripts.bundle.js"></script>
<script src="<?php echo base_url();?>assets/bundles/vendorscripts.bundle.js"></script>
<script src="<?php echo base_url();?>assets/bundles/mainscripts.bundle.js"></script>
<script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> 
<script src="<?php echo base_url();?>assets/js/pages/notifications.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.bs-table.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatablescripts.bundle.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/fontawesome/js/fontawesome.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY;?>&region=GB&libraries=places"></script>
<script src="<?php echo base_url();?>assets/js/customized.js"></script>
<script type="text/javascript">$('form').parsley();</script>
<script type="text/javascript">
function check_logout() 
{
  swal({   
    title: "Logout ?",   
    text: "Are you sure you want to logout ?",   
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "<?php echo THEME_COLOR;?>",
    confirmButtonText: "Yes, Logout!",  
    cancelButtonText: "No, Wait",
    closeOnConfirm: false
  }, function () {
    var url = '<?php echo site_url();?>authpanel/login/logout';
    window.location.href = url;
  });
}
function isNumber(evt) 
{
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57 )) {
    return false;
  }
  return true;
}
</script>
<?php if($this->session->flashdata('success_msg')){ ?>
<script type="text/javascript">
  swal({   
    title: "Success",   
    text: "<?php echo trim($this->session->flashdata('success_msg')); ?>",   
    type: "success",
    html: true
  });
  // showNotification('alert-danger','<?php echo trim($this->session->flashdata('success_msg')); ?>','top','right','zoomIn animated','zoomOut animated');
</script>
<?php } if($this->session->flashdata('error_msg')){ ?>
<script type="text/javascript">
  swal({
    title: 'Error',
    text: '<?php echo trim($this->session->flashdata('error_msg')); ?>',
    type: "error",
    html: true
  });
  // showNotification('alert-success','<?php echo trim($this->session->flashdata('error_msg')); ?>','top','right','zoomIn animated','zoomOut animated');
</script>
<?php } ?>


<script type="text/javascript">
  /*Reset the count value on dashboard*/
  resetSidebarCounts('pageload');
  //For counter animation
  function CounterAnimation(counterid, x) {
    $('#'+counterid).addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass(x + ' animated');
    });
  };
  //Set sidebar counts
  function resetSidebarCounts(calltype)
  {   
    var url = '<?php echo site_url();?>authpanel/dashboard/getDashboardCounts';
    $.ajax({
      type: "GET",
      url: url,
      success:function(results)
      {   
        var animation = 'shake';
        var order_animation = 'zoomIn';
        var extra_animation = 'flip';
        counts = JSON.parse(results);
        if(calltype!='pageload'){
          if(counts['customers_counts'] > $('#sidebar__customers_counts').text()){
            showNotification('alert-success','You have new Customer Sign Up If you want check Please <a href="<?php echo base_url().'authpanel/customer'; ?>"> Click here </a>','top','center','zoomIn animated','zoomOut animated');
          }
        }
        $.each(counts, function( key, value ) {
          if(value > $("#sidebar__"+key).text()){
            if(calltype!='pageload'){
              CounterAnimation("sidebar__"+key,animation);
            }
          }
          $("#sidebar__"+key).html(value);
          if("<?php echo $this->router->fetch_class(); ?>"=='dashboard'){
            if(value > $("#"+key).text()){
              if(calltype!='pageload'){
                CounterAnimation(key,animation);  
              }
            }
            $('#'+key).html(value);
          }
        });
      }
    });
  }
  /*Reset the count value on sidebar*/
  setInterval(function () {
    //Reset sidebar counts
    resetSidebarCounts('ajaxcall');
  }, 15000);
</script>
<?php $this->load->view('authpanel/common/footer'); ?> 