<?php $admindata=$this->session->userdata(ADMIN_SESSION_NAME); 
$profile_image=!empty($admindata['profile_image']) ? $admindata['profile_image'] : 'default.png'; ?>
<!-- ========== Left Sidebar Start ========== -->
<style>
    .btn:not(.btn-link):not(.btn-circle) i {
        font-size: 20px;
        position: relative;
        top: -1px;
    }
    .custom_dp {
        padding: 8px 10px;
        margin-left: 20px;
        border-radius: 10px;
        margin-top: 10px;
    }
    a.dropdown-item.listing .material-icons {
    vertical-align: middle;
    margin-right: 10px;
}
</style>
<aside id="leftsidebar" class="sidebar"> 
    <!-- User Info -->
    <div class="user-info">
        <div class="image"> 
            <img src="<?php echo S3_BUCKET_ROOT.ADMIN_IMAGE.$profile_image; ?>" width="48" height="48"/> 
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <?php
                    $name_length=strlen(@$admindata['name']);
                    if($name_length>10){
                        echo substr(@$admindata['name'],0,10-$name_length)."...";
                    }else{
                        echo @$admindata['name'];    
                    } 
                ?>
            </div>
            <div class="email" style="font-size: 12px !important;">
                <?php
                    $email_length=strlen(@$admindata['email']);
                    if($email_length>15){
                        echo substr(@$admindata['email'],0,15-$email_length)."...";
                    }else{
                        echo @$admindata['email'];    
                    } 
                ?>
            </div>
        </div>
        <!-- <div class="clearfix"></div> -->
         <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle custom_dp" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"> keyboard_arrow_down </i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item listing" href="<?php echo site_url('authpanel/dashboard/profile');?>"><i class="material-icons">person</i>Edit Profile</a>
            <?php if($admindata['role'] != 'Sub') { ?>
            <a class="dropdown-item listing" href="<?php echo site_url('authpanel/subadmin');?>"> <i class="material-icons">people</i>Sub Admin</a>
             <?php } ?>
            <a class="dropdown-item listing" href="<?php echo site_url('authpanel/dashboard/setting');?>"> <i class="material-icons">settings</i>Setting</a>
            <a class="dropdown-item listing" href="<?php echo site_url('authpanel/login/lock_screen');?>">    <i class="material-icons">lock_outline</i>Lock Screen
            <a class="dropdown-item listing" href="javascript:void(0)" onClick="return check_logout();">  <i class="material-icons">power_settings_new</i> Logout</a>
        </div>
    </div>
       <!--  <div class="btn-group user-helper-dropdown"> 
            <i class="material-icons text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"> keyboard_arrow_down </i>
            <ul class="dropdown-menu pull-right">
                <li>
                    <a href="<?php echo site_url('authpanel/dashboard/profile');?>">
                        <i class="material-icons">person</i>Edit Profile
                    </a>
                </li>
                 <?php if($admindata['role'] != 'Sub') { ?>
                <li>
                    <a href="<?php echo site_url('authpanel/subadmin');?>">
                        <i class="material-icons">people</i>Sub Admin
                    </a>
                </li>
            <?php } ?>
                 <li>
                    <a href="<?php echo site_url('authpanel/dashboard/setting');?>">
                        <i class="material-icons">settings</i>Setting
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('authpanel/login/lock_screen');?>">
                        <i class="material-icons">lock_outline</i>Lock Screen
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" onClick="return check_logout();">
                        <i class="material-icons">power_settings_new</i> Logout
                    </a>
                </li>
            </ul>
        </div> -->
    </div>

    <!-- #User Info --> 
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">NAVIGATION</li>
            <li class="<?php echo $this->common_model->get_menu('dashboard'); ?> open">
                <a href="<?php echo base_url('authpanel/dashboard') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-home"></i>
                    <span>Dashboard</span> 
                </a> 
            </li>
            <li class="<?php echo $this->common_model->get_menu('home_videos'); ?> open">
                <a href="<?php echo base_url('authpanel/home_videos') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-collection-video"></i>
                    <span>Home Videos</span>                     
                </a> 
            </li>
            <?php if($admindata['role'] == 'Sub') { 
                $modules = $this->common_model->common_multipleSelect('tbl_subadmin_modules', array('is_deleted'=>'0','status'=>'Active'));

                $access_modules = $this->common_model->common_multipleSelect('tbl_subadmin_access_modules', array('subadmin_id'=> $admindata['id']));

                $modules_arr = [];
                foreach ($access_modules as $value) {
                    $modules_arr[] = $value['module_id'];
                }
                if(in_array('1', $modules_arr)) { 
            ?>

            
            <li class="<?php echo $this->common_model->get_menu('customer'); ?> open">
                <a href="<?php echo base_url('authpanel/customer') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-account"></i>
                    <span>Customers</span> 
                    <span id="sidebar__customer_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
            <?php  } if(in_array('2', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('subscription'); ?> open">
                <a href="<?php echo base_url('authpanel/subscription') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-balance-wallet"></i>
                    <span>Subscriptions</span> 
                    <span id="sidebar__subscription_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
            <?php  } if(in_array('3', $modules_arr)) { ?> 
            <li class="<?php echo $this->common_model->get_menu('contest'); ?> open">
                <a href="<?php echo base_url('authpanel/contest') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-spinner"></i>
                    <span>Contests</span> 
                    <span id="sidebar__contest_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <?php  } if(in_array('4', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('videolibrary'); ?> open">
                <a href="<?php echo base_url('authpanel/videolibrary') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-slideshow"></i>
                    <span>Video Libraries</span> 
                    <span id="sidebar__videolibrary_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <?php  } if(in_array('5', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('classes'); ?> open">
                <a href="<?php echo base_url('authpanel/classes') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-group-work"></i>
                    <span>Online Classes</span> 
                    <span id="sidebar__classes_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <?php  } if(in_array('6', $modules_arr)) { ?>  
            <li class="<?php echo $this->common_model->get_menu('promocode'); ?> open">
                <a href="<?php echo base_url('authpanel/promocode') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-label"></i>
                    <span>Promocodes</span> 
                    <span id="sidebar__promocode_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <?php  } if(in_array('7', $modules_arr)) { ?> 
            <li class="<?php echo $this->common_model->get_menu('masterprize'); ?> open">
                <a href="<?php echo base_url('authpanel/masterprize') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-star-circle"></i>
                    <span>Points Prize</span> 
                    <span id="sidebar__masterprize_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
            <?php  } if(in_array('8', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('speechevaluation'); ?> open">
                <a href="<?php echo base_url('authpanel/speechevaluation') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-dot-circle"></i>
                    <span>Speech Evaluations</span> 
                    <span id="sidebar__speechevaluation_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
            <?php  } if(in_array('9', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('suggestedcourse'); ?> open">
                <a href="<?php echo base_url('authpanel/suggestedcourse') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-square-o"></i>
                    <span>Suggested Courses</span> 
                    <span id="sidebar__suggestedcourse_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
            <?php  } if(in_array('10', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('leaderboard'); ?> open">
                <a href="<?php echo base_url('authpanel/leaderboard') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-card"></i>
                    <span>Leaderboard</span> 
                    <span id="sidebar__leaderboard_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <?php  } if(in_array('11', $modules_arr)) { ?> 
            <li class="<?php echo $this->common_model->get_menu('notification'); ?> open">
                <a href="<?php echo base_url('authpanel/notification') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-notifications-active"></i>
                    <span>Notifications</span>                     
                </a> 
            </li>
            <?php  } if(in_array('12', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('contacts'); ?> open">
                <a href="<?php echo base_url('authpanel/contacts') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-local-post-office zmdi-hc-fw"><div></div></i>
                    <span>Contact Us</span> 
                     <span id="sidebar__contacts_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
            <?php  } if(in_array('13', $modules_arr)) { ?>
            <li class="<?php echo $this->common_model->get_menu('faq'); ?> open">
                <a href="<?php echo base_url('authpanel/faq') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-help zmdi-hc-fw"></i>
                    <span>FAQ's</span> 
                    <span id="sidebar__faq_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
            <?php  } } else { ?>  
            <li class="<?php echo $this->common_model->get_menu('customer'); ?> open">
                <a href="<?php echo base_url('authpanel/customer') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-account"></i>
                    <span>Customers</span> 
                    <span id="sidebar__customer_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
           
            <li class="<?php echo $this->common_model->get_menu('subscription'); ?> open">
                <a href="<?php echo base_url('authpanel/subscription') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-balance-wallet"></i>
                    <span>Subscriptions</span> 
                    <span id="sidebar__subscription_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
          
            <li class="<?php echo $this->common_model->get_menu('contest'); ?> open">
                <a href="<?php echo base_url('authpanel/contest') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-spinner"></i>
                    <span>Contests</span> 
                    <span id="sidebar__contest_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
             <li class="<?php echo $this->common_model->get_menu('sponsor'); ?> open">
                <a href="<?php echo base_url('authpanel/sponsor') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-spinner"></i>
                    <span>Sponsor</span> 
                    <span id="sidebar__sponsor_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <li class="<?php echo $this->common_model->get_menu('videolibrary'); ?> open">
                <a href="<?php echo base_url('authpanel/videolibrary') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-slideshow"></i>
                    <span>Video Libraries</span> 
                    <span id="sidebar__videolibrary_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            
            <li class="<?php echo $this->common_model->get_menu('classes'); ?> open">
                <a href="<?php echo base_url('authpanel/classes') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-group-work"></i>
                    <span>Online Classes</span> 
                    <span id="sidebar__classes_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
           
            <li class="<?php echo $this->common_model->get_menu('promocode'); ?> open">
                <a href="<?php echo base_url('authpanel/promocode') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-label"></i>
                    <span>Promocodes</span> 
                    <span id="sidebar__promocode_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
          
            <li class="<?php echo $this->common_model->get_menu('masterprize'); ?> open">
                <a href="<?php echo base_url('authpanel/masterprize') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-star-circle"></i>
                    <span>Points Prize</span> 
                    <span id="sidebar__masterprize_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
           
            <li class="<?php echo $this->common_model->get_menu('speechevaluation'); ?> open">
                <a href="<?php echo base_url('authpanel/speechevaluation') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-dot-circle"></i>
                    <span>Speech Evaluations</span> 
                    <span id="sidebar__speechevaluation_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
           
            <li class="<?php echo $this->common_model->get_menu('suggestedcourse'); ?> open">
                <a href="<?php echo base_url('authpanel/suggestedcourse') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-square-o"></i>
                    <span>Suggested Courses</span> 
                    <span id="sidebar__suggestedcourse_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
            
            <li class="<?php echo $this->common_model->get_menu('leaderboard'); ?> open">
                <a href="<?php echo base_url('authpanel/leaderboard') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-card"></i>
                    <span>Leaderboards</span> 
                    <span id="sidebar__leaderboard_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <li class="<?php echo $this->common_model->get_menu('banner'); ?> open">
                <a href="<?php echo base_url('authpanel/banner') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-balance-wallet"></i>
                    <span>Banners</span> 
                    <span id="sidebar__banner_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <li class="<?php echo $this->common_model->get_menu('report'); ?> open">
                <a href="<?php echo base_url('authpanel/report') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-money-box"></i>
                    <span>Reports</span> 
                    <span id="sidebar__report_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>  
            <li class="<?php echo $this->common_model->get_menu('notification'); ?> open">
                <a href="<?php echo base_url('authpanel/notification') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-notifications-active"></i>
                    <span>Notifications</span>                     
                </a> 
            </li>
            <li class="<?php echo $this->common_model->get_menu('settings'); ?> open">
                <a href="<?php echo base_url('authpanel/settings') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-settings"></i>
                    <span>Settings</span>                     
                </a> 
            </li>
            
            <li class="<?php echo $this->common_model->get_menu('contacts'); ?> open">
                <a href="<?php echo base_url('authpanel/contacts') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-local-post-office zmdi-hc-fw"><div></div></i>
                    <span>Contact Us</span> 
                     <span id="sidebar__contacts_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
            
            <li class="<?php echo $this->common_model->get_menu('faq'); ?> open">
                <a href="<?php echo base_url('authpanel/faq') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-help zmdi-hc-fw"></i>
                    <span>FAQ's</span> 
                    <span id="sidebar__faq_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li> 
             <li class="<?php echo $this->common_model->get_menu('country'); ?> open">
                <a href="<?php echo base_url('authpanel/country') ?>" class="toggled waves-effect waves-block">
                    <i class="zmdi zmdi-globe zmdi-hc-fw"></i>
                    <span>Tax</span> 
                    <span id="sidebar__country_counts" class="label label-app pull-right">
                        0
                    </span>
                </a> 
            </li>
            <?php } ?>
            <li class="<?php echo $this->common_model->get_menu('app_content'); ?> open"> 
                <a href="javascript:void(0);" class="menu-toggle toggled waves-effect waves-block">
                    <i class="zmdi zmdi-format-list-bulleted zmdi-hc-fw"></i>
                    <span>CMS Content</span> 
                </a>
                <ul class="ml-menu" style="display: block;">
                    <!-- <li class="<?php echo $this->common_model->get_submenu_list(array('about_us','aboutus_update')); ?>">
                        <a href="<?php echo site_url('authpanel/app_content/about_us');?>" class="waves-effect waves-block">About Us</a> 
                    </li> -->
                    <li class="<?php echo $this->common_model->get_submenu_list(array('privacy_policy','privacypolicy_update')); ?>">
                        <a href="<?php echo site_url('authpanel/app_content/privacy_policy');?>" class="waves-effect waves-block">Privacy Policy</a>
                    </li>
                    <li class="<?php echo $this->common_model->get_submenu_list(array('terms_condition','termscondition_update')); ?>">
                        <a href="<?php echo site_url('authpanel/app_content/terms_condition');?>" class="waves-effect waves-block">Terms & Condition</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- #Menu --> 
</aside>