<!-- Page Loader -->
<!-- <div class="page-loader-wrapper">
    <div class="loader">        
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <p>Please wait...</p>
        <div class="m-t-30">
            <img src="<?php echo base_url().LOGO_NAME;?>" width="48" height="48" alt="<?php echo PROJECT_NAME; ?>">
        </div>
    </div>
</div> -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div><!-- Search  -->

<!-- Top Bar -->
<nav class="navbar">
    <div class="col-12">
        <div class="navbar-header" style="color: #fff !important;">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?php echo site_url();?>authpanel">
                <?php echo PROJECT_NAME; ?>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-left" style="color: #fff !important;">
            <li>
                <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true">
                    <i class="material-icons">swap_horiz</i>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right" style="color: #fff !important;">
          <!--   <li>
                <a href="javascript:void(0);" class="fullscreen hidden-sm-down" data-provide="fullscreen" data-close="true">
                    <i class="zmdi zmdi-fullscreen"></i>
                </a>
            </li> -->
            <li>
                <a href="<?php echo site_url('authpanel/login/lock_screen');?>" class="mega-menu" data-close="true">
                    <i class="zmdi zmdi-lock-outline zmdi-hc-fw"></i>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" onClick="return check_logout();" class="mega-menu" data-close="true">
                    <i class="zmdi zmdi-power"></i>
                </a>
            </li>
            <!-- <li class=""><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a></li> -->
        </ul>
    </div>
</nav>