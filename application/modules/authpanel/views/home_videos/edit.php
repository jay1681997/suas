<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Contest";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Contest Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/contest">
                                    Contest List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Contest Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/home_videos/edit/'.base64_encode($result['id']),array('id'=>'add_contest', 'name'=>'add_contest', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div id="video_data" class="form-group">
                                            <div class="text-center preview_holder" >
                                                <div class="row">
                                                    <div class="col-6">
                                                        <video width="400" controls class="img-responsive img-thumbnail" id="videoss">
                                                            <source src="<?php echo (!empty($result['video'])) ? S3_BUCKET_ROOT.HOME_VIDEO.$result['video'] : "" ; ?>" id="videoPreview">
                                                        </video>
                                                        <canvas class="img-responsive img-thumbnail d-none"></canvas>        
                                                    </div>
                                                    <div class="col-6">
                                                        <img class="img-responsive img-thumbnail" id="imagePreview"  />
                                                        <input type="hidden" name="thumb_image" id="thumb_image">
                                                        <label id="thumb_image_error"></label>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="text-center">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload  Video <input type="file" id="contest_image" name="contest_image" class="filestyle" data-parsley-trigger="change"  data-parsley-errors-container="#contest_error" accept="video/mp4, image/gif"/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="contest_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                       <!--  <div id="image_data" class="form-group">
                                        <div class="text-center preview_holder" >
                                                <img id="imagePreview_image" width="200" height="200" class="img-responsive img-thumbnail"/>
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file" >
                                                            Upload  Image <input type="file" id="contest_image_image" name="contest_image_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png,  image/jpg" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row clearfix">
                                            
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Title<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('title')) || !empty($result['title']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo !empty(set_value('title')) ? set_value('title') : $result['title']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter name"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 ">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Age<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('age')) || !empty($result['age']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="age" id="age" value="<?php echo !empty(set_value('age')) ? set_value('age') : $result['age']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter age"  required>
                                                        
                                                    </div>
                                                    <?php echo form_error('age'); ?>
                                                </div>
                                            </div>                       
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/contest"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/momentjs/moment.js"></script>
        <script type="text/javascript">

      
            $(document).ready(function(e) {
                $(".select2").select2({
                    "width":"100%"
                });   
                var url =    '<?php echo S3_BUCKET_ROOT.HOME_VIDEO.$result['thumb_image']; ?>';        
                $("#imagePreview").css("background-image", "url("+url+")");
                  var url_image =    '<?php echo S3_BUCKET_ROOT.HOME_VIDEO.$result['image']; ?>';        
                $("#imagePreview_image").css("background-image", "url("+url_image+")");;
                console.log(url);                
                $("#imagePreview").css("background-size", "100% 100%");  
                $("#imagePreview_image").css("background-size", "100% 100%");  
                var contest_startdate = $("#contest_startdate").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: "today",
                    onClose: function(selDate,dateStr){
                        contest_enddate.set('minDate',dateStr);
                    },
                });
                var contest_enddate = $("#contest_enddate").flatpickr({
                    enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y H:i",
                    minDate: moment().format("YYYY-MM-DD HH:MM"),
                    onClose: function(selDate,dateStr){
                        contest_startdate.set('maxDate',dateStr);
                    }
                });
                var winner_date = $("#winner_date").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y",
                    minDate: moment().format("DD-MM-YYYY"),
                    onClose: function(selDate,dateStr){
                        end_date.set('minDate',dateStr);
                    },
                });
            });
         
            function readURL(input) {
                var fileTypes = ['jpeg','jpg','png','gif','tiff'];  //acceptable file types
                if (input.files && input.files[0]) {
                    var video = $("video");
                    var thumbnail = $("canvas");
                    var img = $("#imagePreview");
                    
                    var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                        isImages = fileTypes.indexOf(extension) > -1; 
                  
                    if(isImages)
                    {
                        video.find("source").attr("src", '');
                        video.get(0).load();
                        img.append(img.attr("src", ''));
                    }
                     

                    var reader = new FileReader();
 
                    //videoPreview
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#contest_image").change(function () {
                readURL(this);
            });

            function readURL_image(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview_image").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#contest_image_image").change(function () {
                readURL_image(this);
            });

            function validate_dropdown(){
                let country_code = $("#country_code").val();
                if(country_code != '' && country_code.length > 0){
                    $("#countrycodeerror").text('');
                } else {
                    $("#country_code").attr('required','required');
                }   
            }

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
         <script type="text/javascript">
            $(function() {
                var video = $("video");
                var thumbnail = $("canvas");
                var input = $("#contest_image");
                var ctx = thumbnail.get(0).getContext("2d");
                var duration = 0;
                var img = $("#imagePreview");

                input.on("change", function(e) {
                    var file = e.target.files[0];

                    console.log(file)
                    console.log(file.length)

                    //alert("OUTER VIDEO ");
                    // Validate video file type
                    if (["video/mp4"].indexOf(file.type) === -1) {
                        //alert("Only 'MP4' video format allowed.");
                        return;
                    }
                    // Set video source

                    video.find("source").attr("src", URL.createObjectURL(file));

                    // Load the video
                    video.get(0).load();
                    // Load metadata of the video to get video duration and dimensions
                    video.on("loadedmetadata", function(e) {
                        duration = video.get(0).duration;
                        // Set canvas dimensions same as video dimensions
                        thumbnail[0].width = video[0].videoWidth;
                        thumbnail[0].height = video[0].videoHeight;
                        // Set video current time to get some random image
                        //video[0].currentTime = Math.ceil(duration / 2);
                        video[0].currentTime = 5;
                        // Draw the base-64 encoded image data when the time updates
                        video.one("timeupdate", function() {
                            ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                            img.append(img.attr("src", thumbnail[0].toDataURL()));
                            $('#thumb_image').val(thumbnail[0].toDataURL());
                        });
                    });
                });
            });
        </script>
    </body>
</html>