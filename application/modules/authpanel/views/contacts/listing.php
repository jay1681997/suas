<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Contact Us";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Contact Us List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Contact Us List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <div id="toolbar">
                        </div>
                        <table id="contacts_table" data-toggle="table"
                            data-toolcontacts="#toolbar"
                            data-url="<?php echo site_url('authpanel/contacts/ajax_list')?>"
                            data-pagination="true"
                            data-side-pagination="server"
                            data-search="true"
                            data-sort-name="id"
                            data-sort-order="desc"
                            data-page-list="[10, 50, 100]"
                            data-page-size="10"
                            class="table-bordered" >
                          <thead>
                            <tr>
                              <th data-field="action"> Action </th>
                              <th data-field="username" data-sortable="true"> Username </th>
                              <th data-field="email" data-sortable="true">Email </th> 
                              <th data-field="subject" data-sortable="true"> Subject </th>
                              <th data-field="description" data-sortable="true"> Description </th>
                              <th data-field="reply_message" data-sortable="true"> Reply Message </th>
                              <!-- <th data-field="status" data-align="center" >Status</th> -->
                              <th data-field="inserted_date" data-sortable="true"> Contacted On </th> 
                            </tr>
                          </thead>
                        </table>
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#contacts_table');
      $(document).ready(function() {
        table.bootstrapTable({ 
            "processing": true,
            "serverSide": true,
        });
      });

      $('#contacts_table').on('load-success.bs.table', function (data) {
        $('.image-popup').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          mainClass: 'mfp-fade',
          gallery: {
            enabled: false,
            navigateByImgClick: true,
            preload: [0,1]
          }
        });
      });

      function remove(contact_id) 
      {
        swal({   
          title: "Remove Contact Us?",   
          text: "Are you sure you want to delete this Contact Us?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/contacts/remove/"+contact_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#contacts_table').bootstrapTable('refresh');
              }
            });
          }
        });
      }

        $(document).on('change','input[name="changestatus"]',function(){
        if($(this).prop("checked") == true){
          var status="Active";
        } else {
          var status="Inactive";
        }
        $.ajax({
          url:  SITE_URL+"authpanel/contacts/changestatus/"+$(this).data('contactid')+"/"+status,
          type: "GET",
          error: function(jqXHR, textStatus, errorThrown){
            showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
          },
          success: function(message){
            showNotification('alert-success',message,'top','center','zoomIn animated','zoomOut animated');  
            $('#user_table').bootstrapTable('refresh');
          }
        }); 
      });
      
    </script>
  </body>
</html>