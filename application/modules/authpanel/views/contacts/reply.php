<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Reply Contact Us";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Reply Contact Us Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/contacts">
                                Contact Us List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Reply Contact Us Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/contacts/reply/'.base64_encode($result['id']),array('id'=>'add_contacts', 'name'=>'add_contacts', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="row clearfix">
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('email') || $result['email']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control no-resize auto-growth" name="email" id="email"  data-parsley-trigger="change" value="<?php echo $result['email'];?>" readonly>
                                                        <label class="form-label">Email<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>

                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('description') || $result['description']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="description" id="description"  data-parsley-trigger="change" required style="overflow: hidden; overflow-wrap: break-word; height: 50px;" readonly><?php echo $result['description']; ?></textarea>
                                                        <label class="form-label">User Description<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-lg-10 col-xl-10">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('reply_message') || $result['reply_message']) ? 'focused' : '');?>">
                                                        <textarea type="text" class="form-control no-resize auto-growth" name="reply_message" id="reply_message"  data-parsley-trigger="change" data-parsley-required-message="Please enter your reply" required ><?php echo set_value('reply_message'); ?></textarea>
                                                        <label class="form-label">Your Reply<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('reply_message'); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url();?>authpanel/contacts" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.min.js"></script>
        <script>
		    autosize(document.querySelectorAll('textarea'));
	    </script>
    </body>
</html>