<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Dashboard";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <style type="text/css">
            .body i{
                padding: 20px 30px;
                font-size: 60px;
                position: absolute;
                right: 0px;
                bottom: 0px;
                top: 0px;
                line-height: 60px;
            }
        </style>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <!-- Top Bar -->
        <?php $this->load->view('authpanel/common/header'); ?>
        <!-- Left Sidebar -->
        <?php $data['pagename'] = "dashboard"; $data['subpagename'] = ""; $this->load->view('authpanel/common/left-menu',$data); ?>
        <!-- Right Sidebar -->
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
           <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Dashboard
                            <small class="text-muted">
                                Welcome to <?php echo PROJECT_NAME; ?> Application
                            </small>
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i> <?php echo PROJECT_NAME; ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- l-blue,l-parpl,l-seagreen,l-amber -->
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/customer') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-account"></i>
                                    <h2 class="m-t-0" id="customer_counts">
                                        <?php echo empty(@$customer_counts) ? '0' : @$customer_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Customers
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/subscription') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-balance-wallet"></i>
                                    <h2 class="m-t-0" id="subscription_counts">
                                        <?php echo empty(@$subscription_counts) ? '0' : @$subscription_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Subscriptions
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/contest') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-spinner"></i>
                                    <h2 class="m-t-0" id="contest_counts">
                                        <?php echo empty(@$contest_counts) ? '0' : @$contest_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Contests
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/evaluation') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-file-text"></i>
                                    <h2 class="m-t-0" id="evaluation_counts">
                                        <?php echo empty(@$evaluation_counts) ? '0' : @$evaluation_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Evaluations
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/videolibrary') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-slideshow"></i>
                                    <h2 class="m-t-0" id="videolibrary_counts">
                                        <?php echo empty(@$videolibrary_counts) ? '0' : @$videolibrary_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Video Library
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/classes') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-group-work"></i>
                                    <h2 class="m-t-0" id="classes_counts">
                                        <?php echo empty(@$classes_counts) ? '0' : @$classes_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Online Classes
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/promocode') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-label"></i>
                                    <h2 class="m-t-0" id="promocode_counts">
                                        <?php echo empty(@$promocode_counts) ? '0' : @$promocode_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Promocodes
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/masterprize') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-star-circle"></i>
                                    <h2 class="m-t-0" id="masterprize_counts">
                                        <?php echo empty(@$masterprize_counts) ? '0' : @$masterprize_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Prize
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/speechevaluation') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-dot-circle"></i>
                                    <h2 class="m-t-0" id="speechevaluation_counts">
                                        <?php echo empty(@$speechevaluation_counts) ? '0' : @$speechevaluation_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Speech Evaluations
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/suggestedcourse') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-square-o"></i>
                                    <h2 class="m-t-0" id="suggestedcourse_counts">
                                        <?php echo empty(@$suggestedcourse_counts) ? '0' : @$suggestedcourse_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Suggested Courses
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/leaderboard') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-card"></i>
                                    <h2 class="m-t-0" id="leaderboard_counts">
                                        <?php echo empty(@$leaderboard_counts) ? '0' : @$leaderboard_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Leaderboards
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/banner') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-balance-wallet"></i>
                                    <h2 class="m-t-0" id="banner_counts">
                                        <?php echo empty(@$banner_counts) ? '0' : @$banner_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Banners
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/report') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-money-box"></i>
                                    <h2 class="m-t-0" id="report_counts">
                                        <?php echo empty(@$report_counts) ? '0' : @$report_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Reports
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/notification') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-local-post-office"><div></div></i>
                                    <h2 class="m-t-0" id="notification_counts">
                                        <?php echo empty(@$notification_counts) ? '0' : @$notification_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Notifications
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/contacts') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-local-post-office"><div></div></i>
                                    <h2 class="m-t-0" id="contacts_counts">
                                        <?php echo empty(@$contacts_counts) ? '0' : @$contacts_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    Contact Us
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <a href="<?php echo base_url('authpanel/faq') ?>">
                            <div class="card l-red bounceIn animated">
                                <div class="body text-white">
                                    <i class="zmdi zmdi-help"></i>
                                    <h2 class="m-t-0" id="faq_counts">
                                        <?php echo empty(@$faq_counts) ? '0' : @$faq_counts; ?>
                                    </h2>
                                    <p class="m-b-0">
                                    FAQ's
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
    </body>
</html>