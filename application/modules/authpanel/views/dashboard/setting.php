<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = " Setting";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Setting Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item active">
                                Setting Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/dashboard/setting/'.base64_encode($result['id']),array('id'=>'add_setting', 'name'=>'add_setting', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                      
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Tax<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('tax')) || !empty($result['tax']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="tax" id="tax"  data-parsley-trigger="change" data-parsley-required-message="Please enter tax" required value="<?php echo !empty(set_value('tax')) ? set_value('tax') : $result['tax']; ?>" >
                                                       
                                                    </div>
                                                    <?php echo form_error('tax'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Credit Point Discount<span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('credit_point_discount')) || !empty($result['credit_point_discount']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="credit_point_discount" id="credit_point_discount"  data-parsley-trigger="change" data-parsley-required-message="Please enter credit_point_discount" required value="<?php echo !empty(set_value('credit_point_discount')) ? set_value('credit_point_discount') : $result['credit_point_discount']; ?>" >
                                                        
                                                    </div>
                                                    <?php echo form_error('credit_point_discount'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Currency
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control select2" id="currency" name="currency" data-parsley-trigger="change" data-parsley-required-message="Please select currency" data-parsley-errors-container="#currencyerror"  required onchange="validate_dropdown()">
                                                        <option value="">Select Currency</option>
                                                        <?php $currency = !empty(set_value('currency')) ? set_value('currency') : $result['currency']; ?>
                                                        <?php if(!empty($currencies)){ foreach ($currencies as $key => $value) { ?>
                                                            <option value="<?php echo $value['id']; ?>" <?php echo ($currency==$value['currency']) ? 'selected="selected"' : ''; ?>><?php echo $value['currency'].' ('.$value['currency_symbol'].') '; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('currency'); ?>
                                                    <label id="currencyerror"></label>
                                                </div>
                                            </div>
                                          
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/setting"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $('.select2').select2({"width":"100%"});
            });
        </script>
    </body>
</html>