<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Admin profile";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <!-- Top Bar -->
        <?php $this->load->view('authpanel/common/header'); ?>
        <!-- Left Sidebar -->
        <?php $data['pagename'] = "dashboard"; $data['subpagename'] = ""; $this->load->view('authpanel/common/left-menu',$data); ?>
        <!-- Right Sidebar -->
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Edit Profile
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item"><a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
                             <li class="breadcrumb-item active">Edit Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if($this->session->flashdata('success_msg')){ ?>
                        <div class="alert alert-success" >
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success_msg')?>
                        </div>                
                        <?php } ?>
                        <?php if($this->session->flashdata('error_msg')){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('error_msg')?>
                        </div>
                        <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $error_msg; ?>
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/dashboard/profile/',array('id'=>'edit_customer', 'name'=>'edit_customer', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data', 'onsubmit' => 'return submit_form();')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Change <input type="file" id="profile_image" name="profile_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/gif, image/jpg" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name',$result['name']); ?>" parsley-trigger="change" data-parsley-required-message="Please enter name" required>
                                                        <label class="form-label">
                                                            Name <span class="text-danger">*</span>
                                                        </label>
                                                        <?php echo form_error('name'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email',$result['email']); ?>" parsley-trigger="change" data-parsley-required-message="Please enter email" required>
                                                        <label class="form-label">
                                                            Email <span class="text-danger">*</span>
                                                        </label>
                                                        <?php echo form_error('email'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="password" style="display: none;">
                                                        <input type="password" class="form-control" name="password" id="password"  data-parsley-required-message="Please enter password"  data-parsley-trigger="change" >
                                                        <label class="form-label">
                                                            Password
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line focused">
                                                        <input type="password" style="display: none;">
                                                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" data-parsley-trigger="change"  data-parsley-required-message="Please enter confirm password" data-parsley-equalto="#password" data-parsley-equalto-message="Confirm password should be same as password" >
                                                        <label class="form-label">
                                                            Confirm Password
                                                        </label>
                                                       
                                                    </div>
                                                     <?php echo form_error('password'); ?>
                                                </div>
                                            </div>
                                           <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country Code
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country Code" data-parsley-errors-container="#countrycodeerror"  required>
                                                        <option value="">Select Country Code</option>
                                                        <?php $country_code = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?> 
                                                        <?php //echo $country_code; die; ?>
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['dial_code']; ?>" <?php echo ($country_code==$value['dial_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'].'('.$value['dial_code'].')'; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country_code'); ?>
                                                    <label id="countrycodeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label text-left"></label>
                                                    <div class="form-line <?php echo (!empty(set_value('phone')) || !empty($result['phone']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>" parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter phone number" data-parsley-minlength="10" data-parsley-maxlength="12"data-parsley-errors-container="#phoneerr" required maxlength="12" onkeypress="return isNumberKey1(event);">
                                                        <label class="form-label" style="margin-top: -5px">
                                                            Phone Number <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('phone'); ?>
                                                    <label id="phoneerr"></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-4 col-lg-4 col-xl-4 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" name="edit_profile" value="update" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="javascript:;" onclick="history.go(-1); return false;" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                 $("#country_code").select2({
                    "width":"100%"
                });    
                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.ADMIN_IMAGE.$result['profile_image']; ?>')");
            });
         
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile_image").change(function () {
                readURL(this);
            });

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>