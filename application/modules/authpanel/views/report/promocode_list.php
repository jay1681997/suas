<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Promocodes";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Purchase Promocodes List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
                 <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/report/user_promocode">
                  <i class=""></i> User Promocode
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Total Purchase Promocodes List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <div id="toolbar">
                       
                        </div>
                        <table id="promo_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/report/ajax_promocode_list/'.base64_encode($user_id))?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="promo_image" data-align="center">Promocode Image</th>                       
                              <th data-field="promocode"data-sortable="true" data-align="center">Name</th>
                              <th data-field="discount_type" data-sortable="true" data-align="center">Promocode Type</th>
                              <th data-field="discount" data-sortable="true" data-align="center">Offer</th>
                              <th data-field="maxusage" data-sortable="true" data-align="center">Max Usability</th>
                              <th data-field="per_user_usage" data-sortable="true" data-align="center">Per User Limit</th>
                              <th data-field="start_date" data-sortable="true" data-align="center">Start Date</th>
                              <th data-field="end_date" data-sortable="true" data-align="center">End Date</th>
                              <th data-field="price" data-sortable="true" data-align="center">Price</th>
                              <th data-field="insert_datetime" data-sortable="true">Created On</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
     
    </script>
  </body>
</html>