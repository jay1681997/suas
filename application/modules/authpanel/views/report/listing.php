<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Report";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />

    <style type="text/css">
      .round-block{
        margin-right: 80px;
        height: 125px;
        width: 125px;
        border-radius: 75px;
        /*background-color: #0066FF;*/
        background-color: #FFE53B;
        background-image: -webkit-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: -moz-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: -o-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        color: #fff;
        display: inline-block;
        text-align: center;
      }
      .round-block > span:first-child{
        width: 120px;
        color: #fff;
        padding-top: 22px;
        line-height: 50px;
        font-size: 20px;
        text-align: center;
        display: block;
        font-weight: 600;
      }
      .round-block > span:last-child{
        width: 120px;
        color: #fff;
        padding-bottom: 19px;
        line-height: 24px;
        font-size: 16px;
        display: block;
        margin-top: -10px;
        font-weight: 600;
        text-align: center;
      }
    </style>
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Report List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Report List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <?php if($this->session->flashdata('success_msg')){ ?>
              <div class="alert alert-success" >
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success_msg')?>
              </div>                
              <?php } ?>
              <?php if($this->session->flashdata('error_msg')){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('error_msg')?>
              </div>
              <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $error_msg; ?>
              </div>
            <?php } ?> 
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                            <!-- <label class="form-label">Select Category Type</label> -->
                            <select class="form-control select2" id="category_type" name="category_type" parsley-trigger="change">
                              <?php $type = $this->session->userdata('category_type'); ?>
                                <option value="none">Select Category Type</option>
                                <option value="class" <?php echo ($type == 'class') ? 'selected' : ''; ?>>Class</option>
                                <option value="contest" <?php echo ($type == 'contest') ? 'selected' : ''; ?>>Contest</option>
                                <option value="promocode" <?php echo ($type == 'promocode') ? 'selected' : ''; ?>>Promocode</option>
                                <option value="video" <?php echo ($type == 'video') ? 'selected' : ''; ?>>Video Library</option>
                                <option value="speech" <?php echo ($type == 'speech') ? 'selected' : ''; ?>>Speech</option>
                                <option value="subscription" <?php echo ($type == 'subscription') ? 'selected' : ''; ?>>Subscription</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="" style="text-align: center;">
                          <div class="round-block"> 
                            <span><?php echo $totalamount['currency'].''.round($totalamount['total_amount'],2); ?></span>
                            <span >Totals</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <table id="class_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/report/ajax_list')?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="action" data-align="center">Action</th>
                              <th data-field="id" data-align="center">ID</th>
                              <th data-field="item_type"  data-align="center" data-sortable="true">Category Type </th>
                              <th data-field="profile_image"  data-align="center">Category Image </th>
                              <th data-field="name" data-align="center" data-sortable="true">Category Name</th>
                              <th data-field="username" data-align="center" data-sortable="true">User Name</th>
                              <th data-field="student_name" data-align="center" data-sortable="true">Student Name</th>
                              <th data-field="payment_mode" data-align="center" data-sortable="true">Payment Mode</th>
                              <th data-field="order_status" data-align="center" data-sortable="true">Payment Status</th>
                              <th data-field="subtotal" data-align="center" data-sortable="true">Subtotal</th>
                              <th data-field="tax" data-align="center" data-sortable="true">Tax</th>
                              <th data-field="discount" data-align="center" data-sortable="true">Discount</th>
                              <th data-field="totalamount" data-align="center" data-sortable="true">Total Amount</th>
                              <th data-field="status" data-align="center" data-sortable="true">Status</th>
                              <th data-field="insert_datetime" data-sortable="true">Added Date</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#class_table');
      $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
      });  

      $(document).ready(function() {

        $('.select2').select2({"width":"100%"});
        $('#category_type').on('change', function(){

           $.ajax({
            url:  SITE_URL+"authpanel/report/set_categorytype_session/"+$(this).val(),
            type: "GET",
            success: function(message){ 
              window.location.href=SITE_URL+"authpanel/report";
            }
          }); 
        });
      });

         function refund(order_id) 
      {

        swal({   
          title: "Refund Full Amount",   
          text: "Are you sure you want to refund full amount to user?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/report/refund/"+order_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#class_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
      }
    </script>
  </body>
 

</html>