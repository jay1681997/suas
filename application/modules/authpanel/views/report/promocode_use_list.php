<!doctype html>
<html class="no-js" lang="en">
  <head>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/magnific-popup/dist/magnific-popup.css" />
    <?php $data['title'] = "Report";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-table/dist/bootstrap-table.css" rel="stylesheet" />

    <style type="text/css">
      .round-block{
        margin-right: 80px;
        height: 125px;
        width: 125px;
        border-radius: 75px;
        /*background-color: #0066FF;*/
        background-color: #FFE53B;
        background-image: -webkit-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: -moz-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: -o-linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        background-image: linear-gradient(147deg, #2E622A 0%, #1FD010 90%);
        color: #fff;
        display: inline-block;
        text-align: center;
      }
      .round-block > span:first-child{
        width: 120px;
        color: #fff;
        padding-top: 22px;
        line-height: 50px;
        font-size: 20px;
        text-align: center;
        display: block;
        font-weight: 600;
      }
      .round-block > span:last-child{
        width: 120px;
        color: #fff;
        padding-bottom: 19px;
        line-height: 24px;
        font-size: 16px;
        display: block;
        margin-top: -10px;
        font-weight: 600;
        text-align: center;
      }
    </style>
  </head>
  <body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
      <div class="block-header">
        <div class="row">
          <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>
              Total Used Promocode List
            </h2>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
              <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/dashboard">
                  <i class="zmdi zmdi-home"></i> Dashboard
                </a>
              </li>
                 <li class="breadcrumb-item">
                <a href="<?php echo site_url();?>authpanel/report/user_promocode">
                  <i class=""></i> User Promocode
                </a>
              </li>
              <li class="breadcrumb-item active">
                <a href="javascript:void(0);">Total Used Promocode List</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row clearfix">
          <div class="col-sm-12">
            <?php if($this->session->flashdata('success_msg')){ ?>
              <div class="alert alert-success" >
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('success_msg')?>
              </div>                
              <?php } ?>
              <?php if($this->session->flashdata('error_msg')){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $this->session->flashdata('error_msg')?>
              </div>
              <?php } ?> <?php if(isset($error_msg) && $error_msg != ''){ ?>
              <div class="alert alert-danger alert-dismissable zoomIn animated">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo $error_msg; ?>
              </div>
            <?php } ?> 
            <div class="card"> 
              <div class="body">
                <div class="portlet">
                  <div id="portlet2" class="panel-collapse">
                    <div class="portlet-body p-t-0">
                      <div class="table-responsive">
                        <table id="class_table" data-toggle="table"
                          data-toolbar="#toolbar"
                          data-url="<?php echo site_url('authpanel/report/ajax_promocode_use_list/'.base64_encode($user_id))?>"
                          data-pagination="true"
                          data-side-pagination="server"
                          data-search="true"
                          data-show-toggle="false"
                          data-show-columns="false"
                          data-sort-name="id"
                          data-page-list="[10,50,100,500]"
                          data-page-size="10"
                          data-sort-order="desc"
                          data-show-refresh="false"
                          data-show-export="true"
                          data-export-types="['excel','csv','pdf']"
                          class="table-bordered"
                          data-row-style="rowStyle"
                        >
                          <thead>
                            <tr>
                              <th data-field="promo_image" data-align="center">Promocode Image</th>                       
                              <th data-field="promocode"data-sortable="true" data-align="center">Name</th>
                              <th data-field="discount_type" data-sortable="true" data-align="center">Promocode Type</th>
                              <th data-field="discount" data-sortable="true" data-align="center">Offer</th>
                              <th data-field="maxusage" data-sortable="true" data-align="center">Max Usability</th>
                              <th data-field="per_user_usage" data-sortable="true" data-align="center">Per User Limit</th>
                              <th data-field="start_date" data-sortable="true" data-align="center">Start Date</th>
                              <th data-field="end_date" data-sortable="true" data-align="center">End Date</th>
                              <th data-field="price" data-sortable="true" data-align="center">Price</th>
                              <th data-field="insert_datetime" data-sortable="true">Created On</th>
                            </tr>
                          </thead>
                        </table>
                        <!-- End Table -->
                      </div> 
                    </div>
                  </div>
                </div>
              </div> 
            </div>               
          </div>
        </div>
      </div>
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript">
      var table = $('#class_table');
      $(document).ready(function() {
        //datatables
        table.bootstrapTable({ 
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
        });
      });  

      $(document).ready(function() {

        $('.select2').select2({"width":"100%"});
        $('#category_type').on('change', function(){

           $.ajax({
            url:  SITE_URL+"authpanel/report/set_categorytype_session/"+$(this).val(),
            type: "GET",
            success: function(message){ 
              window.location.href=SITE_URL+"authpanel/report";
            }
          }); 
        });
      });

         function refund(order_id) 
      {

        swal({   
          title: "Refund Full Amount",   
          text: "Are you sure you want to refund full amount to user?",   
          type: "error",   
          showCancelButton: true, 
          confirmButtonColor: "#fdd329",   
          confirmButtonText: "Confirm",   
          closeOnConfirm: false 
        },function (isConfirm) {
          if(isConfirm){
            $.ajax({
              url:  SITE_URL+"authpanel/report/refund/"+order_id,
              type: "GET",
              error: function(jqXHR, textStatus, errorThrown){   
                swal("Error",errorThrown,"error");
              },
              success: function(message){
                swal("Success",message,"success");
                $('#class_table').bootstrapTable('refresh');
              }
            }); // END OF AJAX CALL
          }
        });
      }
    </script>
  </body>
 

</html>