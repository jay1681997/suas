<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Leaderboard";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Leaderboard Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/leaderboard">
                                    Leaderboard List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Leaderboard Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/leaderboard/add/',array('id'=>'add_leaderboard', 'name'=>'add_leaderboard', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail" />
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Upload Image <input type="file" id="image" name="image" class="filestyle" data-parsley-trigger="change"  
                                                                data-parsley-required-message = "Please upload image"
                                                                 data-parsley-errors-container="#class_error"  accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="class_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Name <span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo !empty(set_value('name')) ? 'focused' : ''; ?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" data-parsley-trigger="change"  data-parsley-errors-container="#nameerror" data-parsley-required-message="Please enter name" required>
                                                        
                                                        <?php echo form_error('name'); ?>
                                                    </div>
                                                    <label id="nameerror"></label>
                                                </div>   
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Value
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo !empty(set_value('value')) ? 'focused' : ''; ?>">
                                                        <input type="text" class="form-control" name="value" id="value" value="<?php echo set_value('value'); ?>" data-parsley-trigger="change"  data-parsley-errors-container="#valueerror" data-parsley-required-message="Please enter value" required onkeypress="return isNumberKey1(event);">
                                                    </div>
                                                    <?php echo form_error('value'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="country" name="country" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#countryerror"  required>
                                                        <option value="">Select Country</option>
                                                        <?php $country = !empty(set_value('country')) ? set_value('country') : $result['country']; ?>
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['name']; ?>" <?php echo ($country==$value['name']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country'); ?>
                                                    <label id="countryerror"></label>
                                                </div>
                                            </div>
                                            
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url();?>authpanel/leaderboard" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $("#country").select2({
                    "width":"100%",   
                }); 

                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.CLASS_IMAGE.'default.png'; ?>')");  
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function () {
                readURL(this);
            });

          

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
        
    </body>
</html>