<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Update Privacy Policy";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <style>
        .member-card .header {
            min-height: 80px;
        }
    </style>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
       <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Update Privacy Policy
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> <span style="margin-left: 20px">Dashboard</span> </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url();?>authpanel/app_content/privacy_policy">Privacy Policy</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a>Privacy Policy</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <!-- View -->
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12">
                                <div class="card member-card">
                                    <div class="header">
                                        <div class="col-12 text-left">
                                            <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>
                                        </div>
                                    </div>
                                    <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                    <div class="body">
                                        <h4 class=""><b>Update Privacy Policy</b></h4>
                                        <hr>
                                        <div class="card">
                                            <?php echo form_open('authpanel/app_content/privacypolicy_update/',array('id'=>'privacy_policy_update', 'name'=>'privacy_policy_update', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                                <div class="row text-left" >
                                                    <h5 style="margin-left: 20px;">Privacy Policy</h5>
                                                    <div class="col-lg-12 col-md-12">
                                                        <?php $privacy_policy = (!empty(set_value('privacy_policy'))) ? set_value('privacy_policy') : @$app_content['privacy_policy']; ?>
                                                        <textarea id="privacy_policy" name="privacy_policy" class="form-control" placeholder="Enter privacy_policy" ><?php echo $privacy_policy; ?></textarea>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12 offset-md-5">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                                Update
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- End View -->
                </div>
            </div>      
        </div>   
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            if($("#privacy_policy").length > 0){
                tinymce.init({
                    selector: "textarea#privacy_policy",
                    theme: "modern",
                    height:500,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                        "save table contextmenu directionality emoticons paste textcolor"
                    ],
                    toolbar: 'undo redo | styleselect | bold italic | fontselect | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview fullpage | forecolor backcolor emoticons',

                    font_formats: "Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Mallory=mallory; Open Sans=open sans; SF Pro Display=sf pro display; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",

                    fontsize_formats:
                    "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 24pt 30pt 36pt 48pt 60pt 72pt 96pt",

                    style_formats: [
                        {title: 'Header 1', format: 'h1'},
                        {title: 'Header 2', format: 'h2'},
                        {title: 'Header 3', format: 'h3'},
                        {title: 'Header 4', format: 'h4'},
                        {title: 'Header 5', format: 'h5'},
                        {title: 'Header 6', format: 'h6'}
                    ],
                    image_title: true, 
                    // enable automatic uploads of images represented by blob or data URIs
                    automatic_uploads: true,
                    // add custom filepicker only to Image dialog
                    file_picker_types: 'image',
                    file_picker_callback: function(cb, value, meta) {
                        var input = document.createElement('input');
                        input.setAttribute('type', 'file');
                        input.setAttribute('name', 'editor_image');
                        input.setAttribute('accept', 'image/*');

                        input.onchange = function() {
                            var file = this.files[0];
                            var reader = new FileReader();
                          
                            reader.onload = function () {
                                var id = 'blobid' + (new Date()).getTime();
                                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(',')[1];
                                var blobInfo = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);

                                // call the callback and populate the Title field with the file name
                                cb(blobInfo.blobUri(), { title: file.name });
                            };
                            reader.readAsDataURL(file);
                        };
                        input.click();
                    },
                });    
            }  
        });
    </script>
</body>
</html>