<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "About Us";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    <style>
        .member-card .header {
            min-height: 80px;
        }
    </style>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>About Us
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> <span style="margin-left: 20px">Dashboard</span> </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo site_url();?>authpanel/app_content/about_us">About Us</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a>About Us</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <!-- View -->
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12">
                                <div class="card member-card">
                                    <div class="header">
                                        <div class="col-12 text-left">
                                            <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>
                                            <a style="float:right;" href="<?php echo site_url('authpanel/app_content/aboutus_update/'); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                        </div>
                                    </div>
                                    <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                    <div class="body">
                                        <h4 class=""><b>About Us Information</b></h4>
                                        <hr>
                                        <div class="card">
                                            <div class="row text-left" >
                                                <div class="col-lg-12 col-md-12">
                                                    <?php if (isset($app_content['about_us'])){ echo $app_content['about_us']; }else{ echo '-'; } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- End View -->
                </div>
            </div>      
        </div>   
    </section>
    <!-- Jquery Core Js --> 
    <?php $this->load->view('authpanel/common/scripts'); ?>
</body>
</html>
