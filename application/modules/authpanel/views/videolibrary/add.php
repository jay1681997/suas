<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Add Video Library";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Add Video Library Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/videolibrary">
                                    Video Library List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Add Video Library Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                        <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                        </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/videolibrary/add/',array('id'=>'add_videolibrary', 'name'=>'add_videolibrary', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview1" style="height:150px;width:150px;" class="img-responsive img-thumbnail" />
                                            </div> 
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                             Cover Image Upload <input type="file" id="sponsor_logo" name="sponsor_logo" class="filestyle" data-parsley-trigger="change"  
                                                                data-parsley-required-message = "Please upload  Cover Image"
                                                                 data-parsley-errors-container="#sponsor_error"  accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="sponsor_error"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview2" style="height:150px;width:150px;" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Detail Image Upload<input type="file" id="thumb_image"data-parsley-required-message = "Please upload Details Image" name="thumb_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" required/>
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                    <label id="thumb_image"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo !empty(set_value('name')) ? 'focused' : ''; ?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name'); ?>" data-parsley-trigger="change"  data-parsley-errors-container="#nameerror" data-parsley-required-message="Please enter name" required>
                                                        <label class="form-label">Name <span class="text-danger">*</span></label>
                                                        <?php echo form_error('name'); ?>
                                                    </div>
                                                    <label id="nameerror"></label>
                                                </div>   
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo !empty(set_value('category')) ? 'focused' : ''; ?>">
                                                            <input type="text" class="form-control" name="category" id="category" value="<?php echo set_value('category'); ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter category" required>
                                                            <label class="form-label">Category <span class="text-danger">*</span></label>
                                                            <?php echo form_error('category'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo !empty(set_value('price')) ? 'focused' : ''; ?>">
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo set_value('price'); ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" required>
                                                            <label class="form-label">Price <span class="text-danger">*</span></label>
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('total_point')) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="total_point" id="total_point" value="<?php echo set_value('total_point');  ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total point" data-parsley-errors-container="#totalpointerr" required  onkeypress="return isNumberKey1(event);">
                                                        <label class="form-label">
                                                            Total Points <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('total_point'); ?>
                                                    <label id="totalpointerr"></label>
                                                </div>
                                            </div>
                                             <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Description
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo !empty(set_value('description')) ? 'focused' : ''; ?>">
                                                        <textarea  row ="50" class="form-control" name="description" id="description"  parsley-trigger="change" data-parsley-required-message="Please enter description" required><?php echo set_value('description');?></textarea>
                                                    </div>
                                                    <?php echo form_error('description'); ?>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">  
                                                <div class="text-center m-b-10">
                                                    <div class="form-group text-center">
                                                        <div class="">
                                                            <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                                Upload Video
                                                                <input type="file" id="media" name="media" class="filestyle" data-parsley-trigger="change" 
                                                                data-parsley-required-message = "Please upload video"
                                                                 data-parsley-errors-container="#media_error" accept="video/mp4/mov/mpeg" required />
                                                                <!-- <input type="hidden" id="imagePreview" name="imagePreview" value=""> -->
                                                            </span>
                                                            <?php echo form_error('media'); ?>
                                                            <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                        </div>
                                                         <label id="media_error"></label>
                                                    </div>
                                                </div>
                                                <div class="text-center preview_holder" >
                                                    <div class="clearfix"></div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <video width="400" controls class="img-responsive img-thumbnail" id="videoss">
                                                                <source src="" id="videoPreview">
                                                                Your browser does not support HTML5 video.
                                                            </video>
                                                            <canvas class="img-responsive img-thumbnail d-none"></canvas>        
                                                        </div>
                                                        <div class="col-6 m-t-20" hidden>
                                                            <img class="img-responsive img-thumbnail" id="imagePreview" />
                                                            <!-- <p>This image is coming from the video</p> -->
                                                            <input type="hidden" name="thumb_image" id="thumb_image">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Submit
                                                    </button>
                                                    <a href="<?php echo base_url();?>authpanel/videolibrary" class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script type="text/javascript">
            $(document).ready(function(e) {
                $('.select2').select2({"width":"100%"});
                $("#imagePreview").css("background-image", "url('')");
                $("#imagePreview").css("background-size", "100% 100%");   
            });


            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
           
            function readURL(input) {
                var fileTypes = ['jpeg','jpg','png','gif','tiff'];  //acceptable file types
                if (input.files && input.files[0]) {
                    var video = $("video");
                    var thumbnail = $("canvas");
                    var img = $("#imagePreview");
                    console.log(video.duration);
                    var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                        isImages = fileTypes.indexOf(extension) > -1; 
                  
                    if(isImages)
                    {
                        video.find("source").attr("src", '');
                        video.get(0).load();
                        img.append(img.attr("src", ''));
                    }
                     

                    var reader = new FileReader();
 
                    //videoPreview
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#media").change(function () {
                readURL(this);
            });
        </script>
        <script type="text/javascript">
            $(function() {
            var video = $("video");
            var thumbnail = $("canvas");
            var input = $("#media");
            var ctx = thumbnail.get(0).getContext("2d");
            var duration = 0;
            var img = $("#imagePreview");

            input.on("change", function(e) {
                var file = e.target.files[0];

                console.log(file)
                console.log(file.length);
                console.log(file.type);

                //alert("OUTER VIDEO ");
                // Validate video file type
                if (["video/mp4","video/mov","video/mpeg","video/quicktime"].indexOf(file.type) === -1) {
                    alert("Only 'MP4' video format allowed.");
                    return;
                }
                // Set video source

                video.find("source").attr("src", URL.createObjectURL(file));
                
                // Load the video
                video.get(0).load();
                // Load metadata of the video to get video duration and dimensions
                video.on("loadedmetadata", function(e) {
                    duration = video.get(0).duration;
                    console.log(duration);
                    if(duration > 1800){
                        alert("You have to Upload the video of total time 30 minutes");
                        video.find("source").attr("src", '');
                        video.get(0).load();
                        $("#imagePreview").css("background-image", "url('')");
                           return;
                    }
                    // Set canvas dimensions same as video dimensions
                    thumbnail[0].width = video[0].videoWidth;
                    thumbnail[0].height = video[0].videoHeight;
                    // Set video current time to get some random image
                    //video[0].currentTime = Math.ceil(duration / 2);
                    video[0].currentTime = 5;
                    // Draw the base-64 encoded image data when the time updates
                    video.one("timeupdate", function() {
                        ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                        img.append(img.attr("src", thumbnail[0].toDataURL()));
                        // $('#thumb_image').val(thumbnail[0].toDataURL());
                    });
                });
            });
        });
        </script>
        <script type="text/javascript">
            $(document).ready(function(e) {
              
                $("#imagePreview1").css("background-image", "url('<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.'default.png'; ?>')");
                $("#imagePreview1").css("background-size", "100% 100%"); 
            });
            

            function readURL1(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview1").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#sponsor_logo").change(function () {
                readURL1(this);
            });

          
          function readURL2(input1) {
                if (input1.files && input1.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e1) {
                        $("#imagePreview2").css("background-image", "url("+e1.target.result+")");
                    }
                    reader.readAsDataURL(input1.files[0]);
                }
            }
            $("#thumb_image").change(function () {
                readURL2(this);
            });

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>
