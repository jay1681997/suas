<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Video Library";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <!-- Bootstrap Material Datetime Picker Css -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"  />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <!-- Top Bar -->
        <?php $this->load->view('authpanel/common/header'); ?>
        <!-- Left Sidebar -->
        <?php $data['pagename'] = "Video Librarys List"; $data['subpagename'] = "Edit Video Library"; $this->load->view('authpanel/common/left-menu',$data); ?>
        <!-- Right Sidebar -->
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Video Library Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/videolibrary">
                                    Video Library</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Video Library
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid animated zoomIn">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="card">
                            <div class="header ">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-black text-left">
                                        <i class="zmdi zmdi-arrow-left"></i>
                                    </a>
                                    
                                </div>
                            </div>
                            <div class="bg-custom bg-theme button-color-css"></div>
                            <?php if(isset($error_msg) && $error_msg != ''){ ?>
                            <div class="alert alert-danger alert-dismissable zoomIn animated">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $error_msg; ?>.
                            </div>
                            <?php } ?> 
                            <div class="body">
                                <div class="col-md-6 col-lg-6 col-xl-6 offset-md-2">
                                    <?php echo form_open('authpanel/videolibrary/edit/'.base64_encode($result['id']),array('post_id'=>'add_post', 'name'=>'add_post', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                         <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview1" style="height:150px;width:150px;" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Cover Image Upload<input type="file" id="sponsor_logo" name="sponsor_logo" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview2" style="height:150px;width:150px;" class="img-responsive img-thumbnail" />
                                            </div>
                                             <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                            Detail Image Upload<input type="file" id="thumb_image" name="thumb_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png, image/jpg, video/mp4, image/gif" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Name <span class="text-danger">*</span></label>
                                                    <div class="form-line <?php echo (!empty(set_value('name')) || (!empty($result['name'])) ? 'focused' : ''); ?>">
                                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo !empty(set_value('name')) ? set_value('name') : $result['name'] ?>" data-parsley-trigger="change" data-parsley-errors-container="#nameerror" 
                                                        data-parsley-required-message="Please enter name" required>
                                                    </div>
                                                    <?php echo form_error('name'); ?>
                                                    <label id="nameerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Category <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo (!empty(set_value('category')) || (!empty($result['category'])) ? 'focused' : ''); ?>">
                                                            <input type="text" class="form-control" name="category" id="category" value="<?php echo !empty(set_value('category')) ? set_value('category') : $result['category'] ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter category" required>
                                                            <?php echo form_error('category'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">Price <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <div class="form-line <?php echo (!empty(set_value('price')) || (!empty($result['price'])) ? 'focused' : ''); ?>">
                                                            <input type="text" class="form-control" name="price" id="price" value="<?php echo !empty(set_value('price')) ? set_value('price') : $result['price'] ?>" data-parsley-trigger="change" data-parsley-type="number"data-parsley-required-message="Please enter price" min="1" required>
                                                            <?php echo form_error('price'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label">
                                                        Total Points <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line <?php echo (!empty(set_value('total_point')) || (!empty($result['total_point'])) ? 'focused' : ''); ?>">
                                                        <input type="text" class="form-control" name="total_point" id="total_point" value="<?php echo !empty(set_value('total_point')) ? set_value('total_point') : $result['total_point'] ?>" parsley-trigger="change" data-parsley-type="number" data-parsley-required-message="Please enter total point" data-parsley-errors-container="#totalpointerr" required  onkeypress="return isNumberKey1(event);">
                                                    </div>
                                                    <?php echo form_error('total_point'); ?>
                                                    <label id="totalpointerr"></label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Description<span class="text-danger">*</span>
                                                    </label>
                                                    <div class="form-line focused">
                                                        <textarea class="form-control" name="description" id="description"  parsley-trigger="change" data-parsley-required-message="Please enter  description" required><?php echo !empty(set_value('description')) ? set_value('description') : @$result['description']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>                                                                        
                                            <div class="form-group">
                                                
                                                <div class="text-center m-b-10">
                                                    <div class="form-group text-center">
                                                        <div class="">
                                                            <span class="btn g-bg-blue waves-effect m-b-15 btn-file">
                                                                Upload Video
                                                                <input type="file" id="media" name="media" class="filestyle" data-parsley-trigger="change" data-parsley-required-message="Please upload video" data-parsley-errors-container="#media_error" accept="video/mp4/mov/mpeg" />
                                                            </span>
                                                            <?php echo form_error('media'); ?>
                                                            <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                        </div>
                                                         <label id="media_error"></label>
                                                    </div>
                                                </div>
                                                <div class="text-center preview_holder" >
                                                    <div class="clearfix"></div>
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <video width="400px" controls class="img-responsive img-thumbnail" >
                                                                <source src="<?php echo (!empty($result['video'])) ? S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$result['video'] : "" ; ?>" id="videoPreview">
                                                                Your browser does not support HTML5 video.
                                                            </video>
                                                            <canvas class="img-responsive img-thumbnail d-none"></canvas>        
                                                        </div>
                                                        <div class="col-6" hidden>
                                                            <img class="img-responsive img-thumbnail" id="imagePreview" name="imagePreview" />
                                                            
                                                            <input type="hidden" name="thumb_image" id="thumb_image">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6 offset-md-5">
                                            <div class="form-group">
                                                <button type="submit" id="submitbutton" class="btn btn-raised bg-theme waves-effect">
                                                    Update
                                                </button>
                                                 <a href="<?php echo base_url();?>authpanel/videolibrary" class="btn btn-raised btn-default waves-effect">
                                                    Cancel
                                                </a>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
      
        <script type="text/javascript">
            
            $(document).ready(function(e) {
                $('.select2').select2({"width":"100%"});     
                var url =    '<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$result['thumb_image']; ?>';        
                $("#imagePreview").css("background-image", "url("+url+")");
                console.log(url);                
                $("#imagePreview").css("background-size", "100% 100%");                      
            });
            
            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            }

            function readURL(input) {
                var fileTypes = ['jpeg','jpg','png','gif','tiff'];  //acceptable file types
                if (input.files && input.files[0]) {
                    var video = $("video");
                    var thumbnail = $("canvas");
                    var img = $("#imagePreview");
                    
                    var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                        isImages = fileTypes.indexOf(extension) > -1; 
                  
                    if(isImages)
                    {
                        video.find("source").attr("src", '');
                        video.get(0).load();
                        img.append(img.attr("src", ''));
                    }
                     

                    var reader = new FileReader();
 
                    //videoPreview
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#media").change(function () {
                readURL(this);
            });
        </script>

        <script type="text/javascript">
            $(function() {
            var video = $("video");
            var thumbnail = $("canvas");
            var input = $("#media");
            var ctx = thumbnail.get(0).getContext("2d");
            var duration = 0;
            var img = $("#imagePreview");
            console.log(img);
            input.on("change", function(e) {
                var file = e.target.files[0];

                console.log(file)
                console.log(file.length)
                // Validate video file type
                if (["video/mp4","video/mov","video/mpeg","video/quicktime"].indexOf(file.type) === -1) {
                    // alert("Only 'MP4' video format allowed.");
                    return;
                }
                // Set video source

                video.find("source").attr("src", URL.createObjectURL(file));

                // Load the video
                console.log(video);
                video.get(0).load();
                // Load metadata of the video to get video duration and dimensions
                video.on("loadedmetadata", function(e) {
                    duration = video.get(0).duration;
                    // Set canvas dimensions same as video dimensions
                    thumbnail[0].width = video[0].videoWidth;
                    thumbnail[0].height = video[0].videoHeight;
                    // Set video current time to get some random image
                    //video[0].currentTime = Math.ceil(duration / 2);
                    video[0].currentTime = 5;
                    // Draw the base-64 encoded image data when the time updates
                    video.one("timeupdate", function() {
                        ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                        img.append(img.attr("src", thumbnail[0].toDataURL()));
                        // $('#thumb_image').val(thumbnail[0].toDataURL());
                    });
                });
            });
        });
        </script>
          <script type="text/javascript">
            $(document).ready(function(e) {
               
                $("#imagePreview1").css("background-image", "url('<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$result['image']; ?>')");
                   $("#imagePreview1").css("background-size", "100% 100%"); 
                    $("#imagePreview2").css("background-image", "url('<?php echo S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$result['thumb_image']; ?>')");
                   $("#imagePreview2").css("background-size", "100% 100%");
            });
            

            function readURL1(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview1").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#sponsor_logo").change(function () {
                readURL1(this);
            });

          function readURL2(input1) {
                if (input1.files && input1.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e1) {
                        $("#imagePreview2").css("background-image", "url("+e1.target.result+")");
                    }
                    reader.readAsDataURL(input1.files[0]);
                }
            }
            $("#thumb_image").change(function () {
                readURL2(this);
            });

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>
