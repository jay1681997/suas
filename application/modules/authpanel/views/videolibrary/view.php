<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Video Library Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Video Library Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/videolibrary">Video Library List</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a>Video Library Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- View -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card member-card">
                            <div class="header l-red">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>   
                                </div>
                            </div>
                                <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                <div class="member-img m-t-5">
                                    <?php if (!empty($result['video']) && $result['video'] != '') { ?> 
                                        <video width="300" controls style="margin-top: -7%;" > 
                                            <source src=" <?php echo $result['video']; ?> "> Your browser does not support HTML5 video. 
                                        </video>
                                    <?php } else if(!empty($result['image']) && $result['image'] != '') {?>
                                        <img src="<?php echo $result['image']; ?>" class="rounded-circle" alt="no-image" width="100" height="150px" style="margin-top: -7%">
                                    <?php } ?>                                        
                                </div>
                                <div class="body">
                                    <div class="col-12">
                                        <ul class="list-inline">
                                            <a href="<?php echo site_url('authpanel/videolibrary/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                        </ul>
                                    </div>
                                <hr>
                                <div class="card">
                                    <div class="row text-left">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <h4 class="" ><b>Video Library Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    <div class="about-info-p">
                                                        <strong>Name</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['name']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Category</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['category']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Price</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['currency'].''.@$result['price']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Total Points</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['total_point']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Description</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['description']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <h4 class="" ><b>Other Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    
                                                    <div class="about-info-p">
                                                        <strong>Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['status'] == 'Active')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted">'.@$result['status'].'</p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Created Date</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['inserted_date'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- End View -->
    </div>
</div>      
</div>   
</section>

<!-- Jquery Core Js --> 
<?php $this->load->view('authpanel/common/scripts'); ?>
<script type="text/javascript">
    /*
    ** Function for remove customer details
    */
    function remove(videolibrary_id) 
    {
        swal({   
            title: "Remove Video Library?",   
            text: "Are you sure you want to delete this Video Library?",   
            type: "error",   
            showCancelButton: true, 
            confirmButtonColor: "#fdd329",   
            confirmButtonText: "Confirm",   
            closeOnConfirm: false 
        },  function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url:  SITE_URL+"authpanel/videolibrary/removevideolibrary/"+videolibrary_id,
                    type: "GET",
                    success: function(data) {
                        var url = SITE_URL+'authpanel/videolibrary/';
                        window.location.href = url;
                    },
                    error: function(jqXHR, textStatus, errorThrown){},
                    complete: function(){}
                }); // END OF AJAX CALL
            }
        });
    }

</script>
</body>
</html>
