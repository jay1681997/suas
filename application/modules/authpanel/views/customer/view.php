<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php $data['title'] = "Customer Details";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
</head>
<body class="<?php echo THEME_COLOR; ?>">
    <?php $this->load->view('authpanel/common/header'); ?>
    <?php $this->load->view('authpanel/common/left-menu'); ?>
    <?php $this->load->view('authpanel/common/right-bar'); ?>
    <section class="content">
     <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Customer Detail
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/dashboard"><i class="zmdi zmdi-home"></i> Dashboard </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?php echo site_url();?>authpanel/customer">Customer List</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a>Customer Detail</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <!-- View -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="card member-card">
                            <div class="header l-red">
                                <div class="col-12 text-left">
                                    <a href="javascript:void(0)" onclick="history.go(-1)" class="btn btn-raised waves-effect waves-float waves-red text-left"><i class="zmdi zmdi-arrow-left"></i></a>  
                                </div>
                                </div>
                                <div class="bg-custom bg-profile  button-color-css">
                                </div>
                                <div class="member-img m-t-5">
                                        <img src="<?php echo S3_BUCKET_ROOT.USER_IMAGE.@$result['profile_image']; ?>" class="rounded-circle" alt="profile-image" width="100" height="150px" style="margin-top: -7%;">
                                </div>
                                <div class="body">
                                    <div class="col-12">
                                        <ul class="list-inline">
                                            <li><h4 class="m-t-5"><?php echo @$result['username'];?></h4></li>
                                            <a href="<?php echo site_url('authpanel/customer/edit/').base64_encode(@$result['id']); ?>" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-edit"></i></a>
                                            <a href="javascript:void(0)" onclick="remove('<?php echo @$result['id']; ?>')" class="btn btn-raised waves-effect waves-float waves-red"><i class="zmdi zmdi-delete"></i></a>
                                        </ul>
                                    </div>
                                    <hr>
                                    <div class="card">
                                    <div class="row text-left" >
                                        <div class="col-md-6">
                                            <div class="card">
                                                <h4 class=""><b>Customer Information</b></h4>
                                                <hr>
                                                <div class="p-20">
                                                    <div class="about-info-p">
                                                        <strong>Email</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['email']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Mobile</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['country_code']." ".@$result['phone']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Date of Birth</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['dob']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Age Category</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['age_category']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Promocode</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo @$result['promocode']; ?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>OTP Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['otp_status'] == 'Verify')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['otp_status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted"><span class="text-danger">'.@$result['otp_status'].'</span></p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Login</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['login_status'] == 'Online')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['login_status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted"><span class="text-danger">'.@$result['login_status'].'</span></p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['status'] == 'Active')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['status'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted">'.@$result['status'].'</p></b>';
                                                        ?>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Last Login</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['last_login'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Registration Date</strong>
                                                        <br>
                                                        <p class="text-muted"><?php echo $this->common_model->date_convert(@$result['insert_datetime'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                    </div>

                                                    <div class="about-info-p">
                                                        <strong>Uploaded Profile Video</strong>
                                                        <br>
                                                        <video width="300" controls> 
                                                            <source src="<?php echo S3_BUCKET_ROOT.USER_IMAGE.@$result['profile_video']; ?>"> Your browser does not support HTML5 video. 
                                                        </video>
                                                    </div>
                                                    <div class="about-info-p">
                                                        <strong>Video Verification Status</strong>
                                                        <br>
                                                        <?php 
                                                        if(@$result['is_video_verify'] == 'Verify')
                                                            echo '<b><p class="text-muted"><span class="text-success">'.@$result['is_video_verify'].'</span></p></b>';
                                                        else if(@$result['is_video_verify'] == 'Unverify')
                                                            echo '<b><p class="text-muted"><span class="text-danger">'.@$result['is_video_verify'].'</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted">'.@$result['is_video_verify'].'</p></b>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h4 class=""><b>Location Information</b></h4>
                                            <hr>
                                            <div class="p-20">
                                                <div class="about-info-p">
                                                    <strong>Latitude</strong>
                                                    <br>
                                                    <?php if(!empty($result['latitude'])){
                                                        echo '<p class="text-muted" style="word-break: normal !important;">'.$result['latitude'].'</p>';
                                                    }else{
                                                        echo '<p class="text-muted"><span class="text-danger">Not defined</span></p>';
                                                    } ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Longitude</strong>
                                                    <br>
                                                    <?php if(!empty($result['longitude'])){
                                                        echo '<p class="text-muted" style="word-break: normal !important;">'.$result['longitude'].'</p>';
                                                    }else{
                                                        echo '<p class="text-muted"><span class="text-danger">Not defined</span></p>';
                                                    } ?>
                                                </div>
                                            </div>
                                            <hr>
                                            <h4 class="m-t-0"><b>Device Information</b></h4>
                                            <hr>
                                            <div class="p-20">
                                                <div class="about-info-p">
                                                    <strong>Token</strong>
                                                    <br>
                                                    <?php if(!empty($deviceinfo['token'])){
                                                        echo '<b><p class="text-muted">'.$deviceinfo['token'].'</p></b>';
                                                    } else {
                                                        echo '<b><p class="text-danger">Not defined</p></b></b>';
                                                    } ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Device Token</strong>
                                                    <br>
                                                    <?php if(!empty($deviceinfo['device_token'])){
                                                        echo '<b><p class="text-muted" style="word-break: normal !important;">'.$deviceinfo['device_token'].'</p></b>';
                                                    } else {
                                                        echo '<b><span class="text-danger">Not defined</span></b>';
                                                    } ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Device Name</strong>
                                                    <br>
                                                    <?php
                                                    if(!empty($deviceinfo['model_name'])){
                                                        echo '<p class="text-muted">'.$deviceinfo['model_name'].'</p>';
                                                    }else{
                                                        echo '<b><span class="text-danger">Not defined</span></b>';
                                                    }
                                                    ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Device Type</strong>
                                                    <br>
                                                    <?php if(!empty($deviceinfo['device_type'])){
                                                        if($deviceinfo['device_type'] == 'A')
                                                            echo '<b><p class="text-muted"><span class="text-success">Andriod</span></p></b>';
                                                        else 
                                                            echo '<b><p class="text-muted"><span class="text-danger">I</span></p></b>';
                                                    }else{
                                                        echo '<b><p class="text-muted"><span class="text-danger">Not defined</span></p></b>';
                                                    } ?>
                                                </div>
                                                <div class="about-info-p">
                                                    <strong>Insert Time</strong>
                                                    <br>
                                                    <p class="text-muted"><?php echo$this->common_model->date_convert($deviceinfo['insert_datetime'],ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));?></p>
                                                </div>
                                            </div>

                                        </div> 
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <!-- End View -->
    </div>
</div>      
</div>   
</section>
<!-- Jquery Core Js --> 
<?php $this->load->view('authpanel/common/scripts'); ?>
<script type="text/javascript">
    /*
    ** Function for remove customer details
    */
    function remove(customer_id) 
    {
        swal({   
            title: "Remove user?",   
            text: "Are you sure you want to delete this user?",   
            type: "error",   
            showCancelButton: true, 
            confirmButtonColor: "#fdd329",   
            confirmButtonText: "Confirm",   
            closeOnConfirm: false 
        },  function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url:  SITE_URL+"authpanel/customer/removecustomer/"+customer_id,
                    type: "GET",
                    success: function(data) {
                        var url = SITE_URL+'authpanel/customer/';
                        window.location.href = url;
                    },
                    error: function(jqXHR, textStatus, errorThrown){},
                    complete: function(){}
                }); // END OF AJAX CALL
            }
        });
    }
</script>
</body>
</html>
