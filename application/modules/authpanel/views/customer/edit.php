<!doctype html>
<html class="no-js " lang="en">
    <head>
        <?php $data['title'] = "Edit Customer";  $this->load->view('authpanel/common/stylesheet',$data);  ?>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" />
        <link href="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="<?php echo THEME_COLOR; ?>">
        <?php $this->load->view('authpanel/common/header'); ?>
        <?php $this->load->view('authpanel/common/left-menu'); ?>
        <?php $this->load->view('authpanel/common/right-bar'); ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>
                            Edit Customer Details
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/dashboard">
                                    <i class="zmdi zmdi-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="<?php echo site_url();?>authpanel/customer">
                                    Customer List</a>
                                </li>
                            <li class="breadcrumb-item active">
                                Edit Customer Details
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <?php if(isset($error_msg) && $error_msg != ''){ ?>
                          <div class="alert alert-danger alert-dismissable zoomIn animated">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $error_msg; ?><a class="alert-link" href="#"></a>.
                          </div>
                        <?php } ?> 
                        <div class="card">
                            <div class="body">
                                <div class="col-md-8 col-lg-8 col-xl-8 offset-md-2">
                                    <?php echo form_open('authpanel/customer/edit/'.base64_encode($result['id']),array('id'=>'add_customer', 'name'=>'add_customer', 'class' => 'form-horizontal group-border-dashed', 'method' => 'post', 'enctype'=>'multipart/form-data')); ?>
                                       <div class="form-group">
                                            <div class="text-center preview_holder" >
                                                <img id="imagePreview" class="img-responsive img-thumbnail"/>
                                            </div>
                                            <div class="text-center m-b-10">
                                                <div class="form-group text-center">
                                                    <div class="">
                                                        <span class="btn g-bg-blue waves-effect m-b-15 btn-file" >
                                                            Profile Image <input type="file" id="profile_image" name="profile_image" class="filestyle" data-parsley-trigger="change" accept="image/jpeg, image/png,  image/jpg" />
                                                        </span>
                                                        <?php echo form_error('image'); ?>
                                                        <?php if(isset($img_error_msg) && $img_error_msg != ''){ ?><ul class="parsley-errors-list filled"><li class="parsley-required"><?php echo $img_error_msg; ?></li></ul><?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('firstname')) || !empty($result['firstname']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo !empty(set_value('firstname')) ? set_value('firstname') : $result['firstname']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter first name" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter valid first name"  required>
                                                        <label class="form-label">First Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('firstname'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('lastname')) || !empty($result['lastname']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo !empty(set_value('lastname')) ? set_value('lastname') : $result['lastname']; ?>" data-parsley-trigger="change" data-parsley-required-message="Please enter last name" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter valid last name"  required>
                                                        <label class="form-label">Last Name<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('lastname'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('email')) || !empty($result['email']) ? 'focused' : '');?>">
                                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo !empty(set_value('email')) ? set_value('email') : $result['email']; ?>" data-parsley-trigger="change" data-parsley-pattern = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'  data-parsley-pattern-message="Please enter valid email" data-parsley-required-message="Please enter email"  required>
                                                        <label class="form-label">Email<span class="text-danger">*</span></label>
                                                    </div>
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line <?php echo (!empty(set_value('dob')) || !empty($result['dob']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" id="dob" name="dob" data-parsley-trigger="change" data-parsley-required-message="Please select date of birth" required readonly value="<?php echo !empty(set_value('dob')) ? set_value('dob') : date('d-m-Y', strtotime($result['dob'])); ?>">
                                                        <label class="form-label">
                                                            Date of Birth<span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('dob'); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Country Code
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="country_code" name="country_code" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#country_codeerror"  required onchange="validate_dropdown()">
                                                        <option value="">Select Country Code</option>
                                                        <?php $country_code = !empty(set_value('country_code')) ? set_value('country_code') : $result['country_code']; ?>
                                                        <?php if(!empty($countries)){ foreach ($countries as $key => $value) { ?>
                                                            <option data-sort_code = "<?php echo $value['iso2'];?>" value="<?php echo $value['dial_code']; ?>" <?php echo ($country_code==$value['dial_code']) ? 'selected="selected"' : ''; ?>><?php echo $value['name'].'('.$value['dial_code'].')'; ?></option>
                                                        <?php } } ?>
                                                    </select>
                                                    <?php echo form_error('country_code'); ?>
                                                    <label id="country_codeerror"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group form-float">
                                                    <label class="form-label text-left"></label>
                                                    <div class="form-line <?php echo (!empty(set_value('phone')) || !empty($result['phone']) ? 'focused' : '');?>">
                                                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo !empty(set_value('phone')) ? set_value('phone') : $result['phone']; ?>" parsley-trigger="change" data-parsley-pattern="^[0-9- ]+$" data-parsley-pattern-message="Please enter valid phone number"  data-parsley-required-message="Please enter phone number" data-parsley-minlength="10" data-parsley-maxlength="12" data-parsley-errors-container="#phoneerr" required maxlength="12">
                                                        <label class="form-label">
                                                            Phone Number <span class="text-danger">*</span>
                                                        </label>
                                                    </div>
                                                    <?php echo form_error('phone'); ?>
                                                     <label id="phoneerr"></label>
                                                </div>
                                            </div>
                                             <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select profile verification 
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="is_video_verify" name="is_video_verify" data-parsley-trigger="change" data-parsley-required-message="Please select country code" data-parsley-errors-container="#country_codeerror"  required onchange="validate_dropdown()">
                                                        <?php $is_video_verify = !empty(set_value('is_video_verify')) ? set_value('is_video_verify') : $result['is_video_verify']; ?>
                                                        <option value="Pending" <?php echo ($is_video_verify == 'Pending')? 'selected="selected"' : ''; ?>>Pending</option>
                                                        <option value="Verify" <?php echo ($is_video_verify == 'Verify') ? 'selected="selected"' : ''; ?>>Verify</option>
                                                        <option value="Unverify" <?php echo ($is_video_verify == 'Unverify') ? 'selected="selected"' : ''; ?>>Unverify</option>
                                                    </select>
                                                    <?php echo form_error('is_video_verify'); ?>
                                                    <label id="is_video_verify"></label>
                                                </div>
                                            </div>
                                            <!-- <div class="clearfix"></div>
                                            <div class="col-md-6 col-lg-6 col-xl-6">
                                                <div class="form-group">
                                                    <label class="form-label text-left">
                                                        Select Date OF Birth Edit
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <select class="form-control" id="is_dob_edit" name="is_dob_edit" data-parsley-trigger="change" data-parsley-required-message="Please select " data-parsley-errors-container="#country_codeerror"  required >
                                                        <?php $is_dob_edit = !empty(set_value('is_dob_edit')) ? set_value('is_dob_edit') : $result['is_dob_edit']; ?>
                                                        <option value="1" <?php echo ($is_dob_edit == '1')? 'selected="selected"' : ''; ?>>Yes</option>
                                                        <option value="0" <?php echo ($is_dob_edit == '0') ? 'selected="selected"' : ''; ?>>No</option>
                                                                                                           </select>
                                                    <?php echo form_error('is_dob_edit'); ?>
                                                    <label id="is_dob_edit"></label>
                                                </div>
                                            </div> -->
                                            <div class="clearfix"></div>
                                            <div class="col-md-6 offset-md-5">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-raised bg-theme waves-effect m-t-20">
                                                        Update
                                                    </button>
                                                    <a href="<?php echo base_url(); ?>authpanel/customer"  class="btn btn-raised btn-default waves-effect m-t-20">
                                                        Cancel
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <?php $this->load->view('authpanel/common/scripts'); ?>
        <script src="<?php echo base_url();?>assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="<?php echo base_url(); ?>assets/plugins/flatpickr/flatpickr.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/momentjs/moment.js"></script>
        <script type="text/javascript">
             $(document).ready(function(e) {
                $("#country_code").select2({
                    "width":"100%",   
                }); 
                $("#is_video_verify").select2({
                    "width":"100%",   
                }); 
                  $("#is_dob_edit").select2({
                    "width":"100%",   
                }); 

                $("#imagePreview").css("background-image", "url('<?php echo S3_BUCKET_ROOT.USER_IMAGE.$result['profile_image']; ?>')");
                var dob = $("#dob").flatpickr({
                    // enableTime: !0,
                    // noCalendar: !0,
                    dateFormat: "d-m-Y",
                    maxDate: moment().format("DD-MM-YYYY"),
                    
                });
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imagePreview").css("background-image", "url("+e.target.result+")");
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#profile_image").change(function () {
                readURL(this);
            });


            function validate_dropdown(){
                let country_code = $("#country_code").val();
                if(country_code != '' && country_code.length > 0){
                    $("#country_codeerror").text('');
                } else {
                    $("#country_code").attr('required','required');
                }   
            }

            function isNumberKey1(evt)
            {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if(charCode != 46)
                {
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;    
                }   
                return true;
            } 
        </script>
    </body>
</html>