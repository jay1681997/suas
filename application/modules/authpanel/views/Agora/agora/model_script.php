<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.css">
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/lib/momentjs/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/ion.rangeSlider.js"></script>

<script type="text/javascript">
	$(document).on("click",".change_role",function() {

		/*if (navigator.geolocation) {        
	          navigator.geolocation.getCurrentPosition(showPosition);
	      }*/

	      navigator.geolocation.getCurrentPosition(function(position) { 
		    	$('#current_latitude').val(position.coords.latitude);
                $('#current_longitude').val(position.coords.longitude);
		    },
		    function(failure) {
		        $.getJSON('https://ipinfo.io/geo', function(response) { 
		        var loc = response.loc.split(',');
		        console.log(loc)
		        $('#current_latitude').val(loc[0]);
                $('#current_longitude').val(loc[1]);
		        
		    });
		});

		var role = $(this).attr("data-role");
		$.ajax({
			type:'POST',
			url:'<?php echo base_url('change_role');?>',
            data:{role:role},
			beforeSend: function()
            {
                // $('.loader').show();
            },
			success:function(data){
				
				var data = JSON.parse(data);
				if(data.status == 'true'){
					$('.web-role').text(role);
					$('#web_role').val(role);
					$('#switch-acc').modal('hide');
					//$('#modal-signin').modal({backdrop: 'static', keyboard: false});
					$('#modal-signin').modal('show');
					$('#student-id').hide();
					$('#sign-in-as').show();
				}

				// $('.loader').hide();
			}
		});
	});


  /* Script For Redirect student signup seperately  */

  $(document).on("click",".signup_student",function() {
    var role = 'Student';
		$.ajax({
			type:'POST',
			url:'<?php echo base_url('change_role');?>',
            data:{role:role},
			beforeSend: function()
            {
                // $('.loader').show();
            },
			success:function(data){
				var data = JSON.parse(data);
        //alert(data.status); return;
				if(data.status == 'true'){

          //window.location.href = "<?php echo site_url('signup'); ?>";
          window.location.href = "https://www.bayisetutor.com/signup";
					// $('.web-role').text(role);
					// $('#web_role').val(role);
					// $('#switch-acc').modal('hide');
					// $('#modal-signin').modal('show');
					// $('#student-id').hide();
					// $('#sign-in-as').show();
				}

				// $('.loader').hide();
			}
		});

  });




	
	$('#session_date').change(function(){
		var date = $(this).val();
		var cur_datetime = moment().format('YYYY-MM-DD HH:mm a');          
        var car_date = moment().format('YYYY-MM-DD');      

        if(car_date == date){
            var greater_time = moment().add('2','hours').format('HH:00:00');
            $('.slot').each(function(i,e){
              var id = $(this).data('id');
              console.log('greater time :-'+greater_time+'id:-'+id);
              if(greater_time > id){
                $(this).css('display','none');
                $(this).addClass('time');
              }
            }); 
        }else{
            $('.slot').css('display','inline-block');
            $('.slot').removeClass('time');
        }
	});

$('.fa').click(function(){
        var count = $(this).data('id');
        
            if(count == 1){
                        
                for(var i = 1; i <= count;i++){
                    $('.star1').addClass('checked');
                }
            }
            else{
                $('.star2').removeClass('checked');
                $('.star3').removeClass('checked');
                $('.star4').removeClass('checked');
                $('.star5').removeClass('checked');
            }
            
            if(count == 2){
                for(var i = 2; i <= count;i++){
                    if(i == 1){
                        $('.star1').addClass('checked');       
                    }
                    if(i == 2){
                        $('.star2').addClass('checked');   
                    }
                }
            }
            else{
                $('.star3').removeClass('checked');
                $('.star4').removeClass('checked');
                $('.star5').removeClass('checked');   
            }
            
            if(count == 3){
                for(var i = 1; i <= count;i++){
                    if(i == 1){
                        $('.star1').addClass('checked');       
                    }
                    if(i == 2){
                        $('.star2').addClass('checked');   
                    }
                    if(i == 3){
                        $('.star3').addClass('checked');   
                    }
                }
            }
            else{
                $('.star4').removeClass('checked');
                $('.star5').removeClass('checked');
            }
            
            if(count == 4){
                for(var i = 1; i <= count;i++){
                     if(i == 1){
                        $('.star1').addClass('checked');       
                    }
                    if(i == 2){
                        $('.star2').addClass('checked');   
                    }
                    if(i == 3){
                        $('.star3').addClass('checked');   
                    }
                    if(i == 4){
                        $('.star4').addClass('checked');       
                    }
                }
            }else{
                $('.star5').removeClass('checked');
            }
            
            if(count == 5){
                for(var i = 1; i <= count;i++){
                   if(i == 1){
                        $('.star1').addClass('checked');       
                    }
                    if(i == 2){
                        $('.star2').addClass('checked');   
                    }
                    if(i == 3){
                        $('.star3').addClass('checked');   
                    }
                    if(i == 4){
                        $('.star4').addClass('checked');       
                    }
                    if(i == 5) {
                        $('.star5').addClass('checked');   
                    }
                }
            }
        
        //$(this).addClass('checked');
        //$('#rating').val(id);
        $('#rating').val(count);
    });

	/* forgor password otp screen target next on focus  */
	$(document).ready(function(){
    	$('.otp_item').keyup(function(){
        	if(this.value.length==$(this).attr("maxlength")){
            	$(this).next().focus();
        	}
    	});

    	/*$('.login-btn').click(function(){
			$('#switch-acc').modal('show');
		});*/

   	});


   	//$("#modal-signin").click(function(){
        /*$("#modal-signin").modal({
            backdrop: 'static',
            keyboard: false
        });*/
    //});

	/* student home page filder modal popup hide past date in Select Date & time slot field */
	//$('#session_date').
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $("#session_date").datepicker({
        format: 'yyyy-mm-dd',
        minDate: today
    });

	/*$('#forgot-otp').submit(function(){

		var otp_1 = $('.otp_1').val();
		var otp_2 = $('.otp_2').val();
		var otp_3 = $('.otp_3').val();
		var otp_4 = $('.otp_4').val();

		if(otp_1 == ''){
			$('.otp_1').css('border','1px solid red');
			return false;
		}else if(otp_2 == ''){
			$('.otp_2').css('border','1px solid red');
			return false;

		}else if(otp_3 == ''){
			$('.otp_3').css('border','1px solid red');
			return false;
		}else{
			$('.otp_4').css('border','1px solid red');
			return false;
		}
	
	});	*/

	/*function showPosition(position) {
		alert(position); return false;
		$('#current_latitude').val(position.coords.latitude)
		$('#current_longitude').val(position.coords.longitude)
  }*/
</script>

<?php if($this->session->tempdata('complete_profile')){ ?>
  <script>
    $('#modal-success').modal('show');
  </script>
<?php } $this->session->unset_tempdata('complete_profile'); ?>


<?php if($this->session->tempdata('forgot_email')){ ?>
  <script>
    $('#email-forgot').modal('show');
  </script>
<?php } $this->session->unset_tempdata('forgot_email'); ?>


<?php if($this->session->tempdata('otp_verification')){ ?>
  <script>
    $('#modal-otp').modal('show');
  </script>
<?php } $this->session->unset_tempdata('otp_verification'); ?>


<?php if($this->session->tempdata('otp_response')){ ?>
  <script>
    $('#reset-password').modal('show');
  </script>
<?php } $this->session->unset_tempdata('otp_response'); ?>

<?php if($this->session->tempdata('tutor_logout')){ ?>
  <script>
    $('#modal-signin').modal('show');
  </script>
<?php } $this->session->unset_tempdata('tutor_logout'); ?>
<?php if($this->session->tempdata('student_logout')){ ?>
  <script>
    //$('#switch-acc-role').modal('show');
    $('#modal-signin').modal('show');
  </script>
<?php } $this->session->unset_tempdata('student_logout'); 
if($this->session->tempdata('writer_logout')){ ?>
  <script>
    //$('#switch-acc-role').modal('show');
    $('#modal-signin').modal('show');
  </script>
<?php } $this->session->unset_tempdata('writer_logout'); 
if($this->session->tempdata('content_writing')){ ?>
  <script>
    //$('#switch-acc-role').modal('show');
    $('#switch-home-role').modal('show');
  </script>
<?php } $this->session->unset_tempdata('content_writing'); 
if($this->session->tempdata('student_courses_data')){ ?>
  <script>
    //$('#switch-acc-role').modal('show');
    $('#student-signin').modal('show');
  </script>
<?php } $this->session->unset_tempdata('student_courses_data'); 
if($this->session->tempdata('tutor_courses_data')){ ?>
  <script>
    $('#tutor-signin').modal('show');
  </script>
<?php } $this->session->unset_tempdata('tutor_courses_data'); ?>