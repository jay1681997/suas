<!-- popup for video accept or reject -->
<div class="modal fade in" id="receiveagoravideocall" role="dialog">
  <div class="modal-dialog">       
    <div class="modal-content">
      <div class="modal-body agora-theme mai-bg">
        <div class="row center mb-50" id="agoravideocallreceiveoption" style="text-align: center;">
          <img src="<?php echo base_url();?>web_assets/img/tutor.png" style="width: 150px;height: 150px;"/>
          <h3 id="agoravideocallusername" style="margin-bottom: 10px;font-size: 30px;">Tutor</h3>
          <button type="button" class="btn btn-raised btn-theme btn-success waves-effect waves-light mr-10" name="agoraacceptvideocallbtn" id="agoraacceptvideocallbtn">Accept Call</button>
          <button type="button" class="btn btn-raised btn-theme btn-danger waves-effect waves-light" name="agorarejctvideocallbtn" id="agorarejctvideocallbtn">Reject Call</button>
        </div>
        <div class="row" style="display: none;" id="agoravideoacceptedtojoin">
          <div class="col-lg-12 col-12 text-center pt-5">
               <div class="mine-pic m-auto d-flex main-audion">
                     <img src="<?php echo base_url();?>web_assets/img/image-1.png"/>
                     <span class="text-white">Name Here</span>
                </div>
              <button type="button" class="btn-theme-blue  agoravideocallscreen_share mr-10">SCREEN SHARE</button>
              <button type="button" class="btn-theme-green waves-light agoravideocallleave">Cut Call</button>          
          </div>
          <div class="col-lg-12 col-12">
            <div class="row">
               <div class="col-md-12">
                   <div class="video-grid" id="agoravideocalllvideomaindiv">
                    <div class="video-view">
                      <div id="local_stream" class="video-placeholder"></div>
                      <div id="local_video_info" class="video-profile hide"></div>
                      <div id="video_autoplay_local" class="autoplay-fallback hide"></div>
                    </div>
                  </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- popup for group class accept or reject -->
<div class="modal fade in" id="receiveagoragroupclasscall" role="dialog">
  <div class="modal-dialog">       
    <div class="modal-content">
      <div class="modal-body agora-theme">
        <div class="row center" style="text-align: center;">
          <img src="<?php echo base_url();?>web_assets/img/tutor.png" style="width: 150px;height: 150px;"/>
          <h3 id="agoragroupclasscallusername" style="margin-bottom: 10px;font-size: 30px;">Tutor</h3>
          <a href="javascript:void(0);" class="btn btn-raised btn-success waves-effect waves-light" id="agoraacceptgroupclasscallbtn">Join</a>
          <a href="javascript:void(0);" class="btn btn-raised btn-danger waves-effect waves-light" id="agorarejctgroupclasscallbtn">Cancel</a>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url();?>web_assets/lib/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url();?>web_assets/lib/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>web_assets/js/gijgo.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/parsley/parsley.min.js"></script><!-- Parsley Js -->
<!-- <script src="<?php //echo site_url();?>web_assets/lib/agora/js/AgoraRTC_N-4.2.1.js"></script> -->
<!-- <script src="<?php echo site_url();?>web_assets/lib/agora/js/AgoraRTCSDK-3.2.3.js"></script> --> 
<script src="https://cdn.agora.io/sdk/release/AgoraRTCSDK-3.4.0.js"></script>
<script src="<?php echo base_url();?>web_assets/lib/agora/js/agora-rtm-sdk-1.4.1.js"></script> 
<script src="<?php echo base_url();?>web_assets/lib/agora/js/agoravideocallfile.js"></script>
<script type="text/javascript">
  $("video").prop('controls', true);
$(document).on("scroll", function(){
  if($(document).scrollTop() > 90){
    $(".header__mobile,.header-top").addClass("navbar-fixed-top");
  } else {
    $(".header__mobile,.header-top").removeClass("navbar-fixed-top");
  }
  /* add active class from menu link when page url is changed */
});
$(function(){
  //$(document).on("click",".test", (function(){
    //$(this).addClass('active');
    //alert($thid)
  //}));
  var current_page_URL = location.href;
  $( "a" ).each(function() {
    //if($(this).attr("href") !== "#"){
      var target_URL = $(this).prop("href");
      if (target_URL == current_page_URL) {
        $('.nav a').parents('li, ul').removeClass('active');
        $(this).addClass('active');
        //$('.nav').parents('li, ul').removeClass('active');
      }
    //}
  });
  
});    
$(document).ready(function() {
  $('form').parsley();
  var $toggleButton = $('.toggle-button'),
  $menuWrap = $('.menu-wrap'),
  $sidebarArrow = $('.sidebar-menu-arrow');
  // Hamburger button
  $toggleButton.on('click', function() {
    $(this).toggleClass('button-open');
    $menuWrap.toggleClass('menu-show');
  });
  // Sidebar navigation arrows
  $sidebarArrow.click(function() {
    $(this).next().slideToggle(300);
  });
  //Get browser timezone & set it
  var timezone = jstz.determine();
  var url = '<?php echo site_url();?>home/set_timezone';
  $.ajax({
    type: "GET",
    url: url,
    data: {
      "timezone":timezone.name(),
    },
    success:function(data){   
    }
  });
});
setTimeout(function () {
  $("#droba-loader").addClass("loaded")
}, 1500);
if('<?php echo $this->session->userdata('userdata')->id; ?>' != ''){
  var agoraUserId = "<?php echo strtoupper(substr($this->session->userdata('userdata')->role,0,1)).'_'.$this->session->userdata('userdata')->id.'_'.$this->session->userdata('userdata')->first_name.' '.$this->session->userdata('userdata')->last_name; ?>";
  var agoraloginId = "<?php echo str_ireplace(" ",".",strtoupper(substr($this->session->userdata('userdata')->role,0,1)).'_'.$this->session->userdata('userdata')->id.'_'.$this->session->userdata('userdata')->first_name.'.'.$this->session->userdata('userdata')->last_name); ?>";
  agoralogin(agoraUserId,{'id':'<?php echo $this->session->userdata('userdata')->id; ?>','name':'<?php echo $this->session->userdata('userdata')->first_name.' '.$this->session->userdata('userdata')->last_name; ?>','image':'<?php echo $this->session->userdata('userdata')->image; ?>'});
}
</script>
<script src="<?php echo base_url();?>web_assets/lib/slick/slick.min.js"></script>
<script src="<?php echo base_url();?>web_assets/js/wow.js"></script>
<script src="<?php echo base_url();?>web_assets/js/jquery-matchHeight.js"></script>
<script src="<?php echo base_url();?>web_assets/js/slicks.js"></script>
<script src="<?php echo base_url();?>web_assets/js/home.js"></script>
<script src="<?php echo base_url();?>web_assets/js/script-course-detail.js"></script>
<script src="<?php echo base_url();?>web_assets/js/script-pricing-table.js"></script>
<script src="<?php echo base_url();?>web_assets/js/script.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>web_assets/js/jstz.min.js"></script> <!-- for chatting  -->
<script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 
<script type="text/javascript">
function check_logout() 
{
  swal({   
    title: "Are you sure",   
    text: "you want to logout ?",   
    type: "warning",   
    showCancelButton: true,   
    confirmButtonColor: "#2E4799",
    confirmButtonText: "YES",  
    cancelButtonText: "NO",
    closeOnConfirm: false
  },function(){
    agoralogout();
    var url = '<?php echo site_url();?>login/logout';
    window.location.href = url;
  });
}
function errorNotify(message) {
  var random = Math.floor((Math.random() * 100) + 1);
  var msg = `<div class="cust-notify errNotify `+random+`">
    <button type="button" class="close m-l-10" data-dismiss="alert">×</button>
    <span class="text">`+message+`</span>
  </div>`;
  $('.notifySpan').prepend(msg);
  $('.'+random).hide().fadeIn(2000).delay(4000).fadeOut(1500,'linear');
}
function succNotify(message) {
  var random = Math.floor((Math.random() * 100) + 1);
  var msg = `<div class="cust-notify succNotify `+random+`">
    <button type="button" class="close m-l-10" data-dismiss="alert">×</button>
    <span class="text">`+message+`</span>
  </div>`;
  $('.notifySpan').prepend(msg);
  $('.'+random).hide().fadeIn(2000).delay(3000).fadeOut(1000,'linear');
}
/* Student Notification Count */
var $role = '<?php echo $this->session->userdata('userdata')->role; ?>';
if($role != '' && $role == 'Student'){
  $.ajax({
    url: SITE_URL+'student-notification-count',
    type: 'post',
    success: function(data){
      // Perform operation on the return value
      var data = JSON.parse(data);
      if(data.count != 0){
        $('.student-notification-count').html(data.count);
      }else{
        $('.student-notification-count').hide();
      }
    }
  });
}
if($role != '' && $role == 'Tutor'){
  /* Tutor Notification Count */
  $.ajax({
    url: SITE_URL+'tutor-notification-count',
    type: 'post',
    success: function(data){
      // Perform operation on the return value
      var data = JSON.parse(data);
      if(data.count != 0){
        $('.tutor-notification-count').html(data.count);
      }else{
        $('.tutor-notification-count').hide();
      }
    }
  });
}
/* Writer Notification Count */
if($role != '' && $role == 'Writer'){
  $.ajax({
    url: SITE_URL+'writer-notification-count',
    type: 'post',
    success: function(data){
      // Perform operation on the return value
      var data = JSON.parse(data);
      if(data.count != 0) {
        $('.writer-notification-count').html(data.count);
      }else{
        $('.writer-notification-count').hide();
      }
    }
  });
}
</script>
<?php if($this->session->tempdata('err_msg1')){ ?>
<script>
errorNotify("<?php echo trim($this->session->tempdata('err_msg1')); ?>");
</script>
<?php } $this->session->unset_tempdata('err_msg1'); ?>
<?php if($this->session->tempdata('succ_msg1')){ ?>
<script>
succNotify("<?php echo trim($this->session->tempdata('succ_msg1')); ?>");
</script>
<?php } $this->session->unset_tempdata('succ_msg1'); ?>