const rtmclient = AgoraRTM.createInstance("28bea0826acb47feb37e2f13dd036491");
var token = undefined;
var invitation = null;
var remoteInvitation1 = null;
var params = {'appID':'28bea0826acb47feb37e2f13dd036491','mode':'live','codec':'h264'};
var fields = ["appID", "channel"];
var agoracall_start_datetime = new Date();
var chatchannel = null;
var my_audioElement = document.createElement('audio');
my_audioElement.setAttribute('src',SITE_URL+'web_assets/lib/agora/js/ringtone.wav');
var peer_audioElement = document.createElement('audio');
peer_audioElement.setAttribute('src',SITE_URL+'web_assets/lib/agora/js/ringtone.wav');
function noBack()
{
  window.history.pushState(null, "", window.location.href);
}





function agoralogin(agoraUserId,userattr)
{
  rtmclient.login({uid:agoraUserId,token}).then(() => {
    rtmclient.setLocalUserAttributes(userattr).then(() => {
      params['uid'] = agoraUserId;
      params['agoraUserId'] = agoraUserId;
      console.log("agora rtm login successfully");
    }).catch((err) => {
      console.log(err);
    });
  }).catch((err) => {
    console.log(err);
  });
}
function agoralogout()
{
  rtmclient.logout().then(() => {
    params['uid'] = '';
    params['agoraUserId'] = '';
  }).catch((err) => {
    console.log(err);
  });
}
let timerId = null;
function checkpeerlogin(calleeId)
{
  // repeat with the interval of 2 seconds
  timerId = setInterval(() => { 
    rtmclient.queryPeersOnlineStatus([calleeId]).then((res) => {
      params['peerstatus'] = res[calleeId];
      if(params['peerstatus']){
        clearInterval(timerId);
        return ;
      }
    }).catch((err) => {
      params['peerstatus'] = false;
      console.error(err)
    });
  },1000); 
}
function agoragroupinvitation(joinstudents,channel)
{  
  /*window.location.hash=""; 
  window.onhashchange=function(){window.location.hash="";} */
  var calltype = channel.split("_");
  var usertype = params['agoraUserId'].split("_");
  if(joinstudents.length > 0){
    params['channel'] = channel;
    for (var i = 0; i < joinstudents.length; i++){
      var calleeId = 'S_'+joinstudents[i].student_id+'_'+joinstudents[i].first_name+' '+joinstudents[i].last_name;
      invitation = rtmclient.createLocalInvitation(calleeId);
      //Local monitoring and inviting peers
      invitation.on('LocalInvitationReceivedByPeer',()=>{
      });
      //Cancel call invitation
      invitation.on('LocalInvitationCanceled',()=>{
      });
      //Called accepted call invitation
      invitation.on('LocalInvitationAccepted',()=>{
        rtmclient.getUserAttributes(calleeId).then((userattr)=>{
          succNotify(userattr.name+" has join a group class");
        }).catch(error => {
          console.error("get user attributes failed",error);
        });
      });
      //Called down
      invitation.on('LocalInvitationRefused',()=>{
      });
      //Local call failed
      invitation.on('LocalInvitationFailure',()=>{
      });
      invitation.content = params['channel'];
      invitation.send();
    }
    $.ajax({
      url: SITE_URL+"start-group-class-call", 
      type:'POST',
      data:{'agora_channel_tutor_uid':usertype[1]+',111'+calltype[1]+'111','agora_uid':calltype[1],'channel_name':channel,'group_class_id':calltype[1]},
      dataType:'json',
      error:function(jqXHR,textStatus,errorThrown){
        errorNotify(errorThrown);
      },
      success: function(result){
        if(result.code == '1'){
          $('#agoravideocalloption').hide();
          $('#agoravideocalloption').slideUp();
          $('#agoravideoacceptedtojoin').show();
          $('#agoravideoacceptedtojoin').slideDown();
          $(".agoracallstatus").text("");
          agoracall_start_datetime = new Date();
          join(rtc,params);
          console.log(result);
          succNotify("Group class invitation sent successfully");
        } else {
          errorNotify(result.message);
        }
      }
    });
  } else {
    errorNotify("Please at add least one student");
  }
}
function agoralocalinvitation(calleeId,channel)
{  
  /*window.location.hash=""; 
  window.onhashchange=function(){window.location.hash="";}*/
  var calltype = channel.split("_"); 
  params['channel'] = channel;
  $(".agoracallstatus").text("CONNECTING");
  checkpeerlogin(calleeId);
  setTimeout(() => { 
    clearInterval(timerId);
    if(params['peerstatus']){
      my_audioElement.play();
      $(".agoracallstatus").text("RINGING");
      invitation = rtmclient.createLocalInvitation(calleeId);
      //Local monitoring and inviting peers
      invitation.on('LocalInvitationReceivedByPeer',()=>{
      });
      //Cancel call invitation
      invitation.on('LocalInvitationCanceled',()=>{
        my_audioElement.pause();
        $(".agoracallstatus").text("");
        if(calltype[0] != 'groupClass'){
          leave(rtc);
        }
      });
      //Called accepted call invitation
      invitation.on('LocalInvitationAccepted',()=>{
        my_audioElement.pause();
        agoracall_start_datetime = new Date();
        console.log(params);
        var usertype = params['agoraUserId'].split("_");
        if(usertype[0] == 'S'){
          var ajaxurl = SITE_URL+"student-start-session-call";
        } else {
          var ajaxurl = SITE_URL+"tutor-start-session-call";
        }
        $.ajax({
          url:ajaxurl, 
          type:'POST',
          data:{'agora_uid':calltype[calltype.length - 2],'channel_name':channel,'session_id':calltype[calltype.length - 2],'session_way':calltype[calltype.length - 1],'subscribe_video_uid':'111'+calltype[calltype.length - 2]+'111'},
          dataType:'json',
          error:function(jqXHR,textStatus,errorThrown){
            errorNotify(errorThrown);
          },
          success: function(result){
            if(result.code == '1'){
              $('#agoravideocalloption').hide();
              $('#agoravideocalloption').slideUp();
              $('#agoravideoacceptedtojoin').show();
              $('#agoravideoacceptedtojoin').slideDown();
              $(".agoracallstatus").text("");
              join(rtc,params);
              console.log(result);
            } else {
              errorNotify(result.message);
            }
          }
        });
      });
      //Called down
      invitation.on('LocalInvitationRefused',()=>{
        my_audioElement.pause();
        $(".agoracallstatus").text("");
        if(calltype[0] != 'groupClass'){
          errorNotify("Peer has rejected your call invitation");
          leave(rtc);
        }
      });
      //Local call failed
      invitation.on('LocalInvitationFailure',()=>{
        my_audioElement.pause();
        $(".agoracallstatus").text("");
        if(calltype[0] != 'groupClass'){
          leave(rtc);
        }
      });
      invitation.content = params['channel'];
      invitation.send();
      succNotify("Peer Video call invitation sent successfully");
    } else {
      $(".agoracallstatus").text("");
      errorNotify("You can't call request")
    }
  }, 10000);
}
rtmclient.on('RemoteInvitationReceived',(remoteInvitation)=>{
  remoteInvitation1 = remoteInvitation;
  console.log('Receive call invitation',remoteInvitation.callerId);
  params['callerId'] = remoteInvitation.callerId;
  var username = remoteInvitation.callerId.split("_");
  var calltype = remoteInvitation1.content.split('_');
  if(calltype[0] == 'groupClass'){
    $('#agoragroupclasscallusername').text(username[2].replace('.',' ').toUpperCase());
    $('#agoraacceptgroupclasscallbtn').attr('href',SITE_URL+'join-group-class-call/'+window.btoa(calltype[1]));
    $('#receiveagoragroupclasscall').modal('show');
  } else {
    peer_audioElement.play();
    $('#agoravideocallusername').text(username[2].replace('.',' ').toUpperCase());
    $('#receiveagoravideocall').modal('show');
  }
  peerEvents();
});
function peerEvents() 
{
  var calltype = remoteInvitation1.content.split('_');
  //The caller has cancelled the call invitation
  remoteInvitation1.on('RemoteInvitationCanceled',()=>{
    if(calltype[0] != 'groupClass'){
      leave(rtc);
    }
  });
  //Accepted call invitation successfully
  remoteInvitation1.on('RemoteInvitationAccepted',()=>{
  });
  //Call invitation rejected successfully
  remoteInvitation1.on('RemoteInvitationRefused',()=>{
  });
  //Call invitation process failed
  remoteInvitation1.on('RemoteInvitationFailure',()=>{
    if(calltype[0] != 'groupClass'){
      leave(rtc);
    }
  });
}
$("#agoraacceptvideocallbtn").click(function(){ 
 /* $('body').attr("onLoad","noBack();");
  $('body').attr("onpageshow","if (event.persisted) noBack();");
  $('body').attr("onUnload","");
  window.location.hash=""; 
  window.onhashchange=function(){window.location.hash="";}*/
  remoteInvitation1.accept();
  peer_audioElement.pause();
  params['channel'] = remoteInvitation1.content;
  $('#agoravideocallreceiveoption').slideUp ();
  $('#agoravideoacceptedtojoin').show();
  $('#agoravideoacceptedtojoin').slideDown();
  $('#receiveagoravideocall').find('.modal-dialog').addClass('modal-lg');
  var calltype = remoteInvitation1.content.split('_');
  if(calltype[calltype.length - 1] == 'Audio'){
    $(".agoravideocallscreen_share").hide();
  } else {
    $(".agoravideocallscreen_share").show()
  }
  peerEvents();
  join(rtc,params);
});
$("#agorarejctvideocallbtn").click(function(){
  remoteInvitation1.refuse();
  peer_audioElement.pause();
  $('#receiveagoravideocall').modal('hide');
  peerEvents();
});
$('#agoraacceptgroupclasscallbtn').click(function(e){
  $('body').attr("onLoad","noBack();");
  $('body').attr("onpageshow","if (event.persisted) noBack();");
  $('body').attr("onUnload","");  
  window.location.hash=""; 
  window.onhashchange=function(){window.location.hash="";}
  e.preventDefault();
  remoteInvitation1.accept();
  peerEvents();
  window.location.href = $(this).attr('href');
});
$('#agorarejctgroupclasscallbtn').click(function(){
  $('#receiveagoragroupclasscall').modal('hide');
  remoteInvitation1.refuse();
  peerEvents();
});
var resolutions = [
  {
    name: "default",
    value: "default",
  },
  {
    name: "480p",
    value: "480p",
  },
  {
    name: "720p",
    value: "720p",
  },
  {
    name: "1080p",
    value: "1080p"
  }
];
function validator(formData, fields) {
  var keys = Object.keys(formData)
  for (let key of keys) {
    if (fields.indexOf(key) != -1) {
      if (!formData[key]) {
        console.log("Please Enter " + key)
        return false
      }
    }
  }
  return true
}
function serializeformData() {
  var formData = $("#agoraform").serializeArray();
  var obj = {};
  for (var item of formData) {
    var key = item.name;
    var val = item.value;
    obj[key] = val;
  }
  return obj;
}
function addView (id, show) {
  if (!$("#" + id)[0]) {
    $("<div/>", {
      id: "remote_video_panel_" + id,
      class: "video-view",
    }).appendTo("#agoravideocalllvideomaindiv");
    $("<div/>", {
      id: "remote_video_" + id,
      class: "video-placeholder",
    }).appendTo("#remote_video_panel_" + id);
    $("<div/>", {
      id: "remote_video_info_" + id,
      class: "video-profile " + (show ? "" :  "hide"),
    }).appendTo("#remote_video_panel_" + id);
    $("<div/>", {
      id: "video_autoplay_"+ id,
      class: "autoplay-fallback hide",
    }).appendTo("#remote_video_panel_" + id);
    var calltype = params['channel'].split("_");
    if(calltype[0] == 'groupClass'){
      var screenshareId = parseInt('111'+calltype[1]+'111');
    } else {
      var screenshareId = parseInt('111'+calltype[calltype.length - 2]+'111');
    }
    rtmclient.getUserAttributes(params['callerId']).then((userattr)=>{
      if(id == screenshareId){
        $("#remote_video_panel_" + id).prepend("<h3>"+userattr.name+" SCREEN SHARE</h3>");
      } else {
        $("#remote_video_panel_" + id).prepend("<h3>"+userattr.name+" Video</h3>");
      }
    }).catch(error => {
      if(id == screenshareId){
        $("#remote_video_panel_" + id).prepend("<h3>Peer SCREEN SHARE</h3>");
      } else {
        $("#remote_video_panel_" + id).prepend("<h3>Peer Video</h3>");
      }
      console.error("get user attributes failed",error);
    });
  }
}
function removeView (id) {
  if ($("#remote_video_panel_" + id)[0]) {
    $("#remote_video_panel_"+id).remove();
  }
}
function getDevices (next) {
  AgoraRTC.getDevices(function (items) {
    items.filter(function (item) {
      return ["audioinput", "videoinput"].indexOf(item.kind) !== -1
    }).map(function (item) {
      return {
        name: item.label,
        value: item.deviceId,
        kind: item.kind,
      }
    });
    var videos = [];
    var audios = [];
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if ("videoinput" == item.kind) {
        var name = item.label;
        var value = item.deviceId;
        if (!name) {
          name = "camera-" + videos.length;
        }
        videos.push({
          name: name,
          value: value,
          kind: item.kind
        });
      }
      if ("audioinput" == item.kind) {
        var name = item.label;
        var value = item.deviceId;
        if (!name) {
          name = "microphone-" + audios.length;
        }
        audios.push({
          name: name,
          value: value,
          kind: item.kind
        });
      }
    }
    next({videos: videos, audios: audios});
  });
}
var rtc = {
  client: null,
  screen: null,
  joined: false,
  published: false,
  localStream: null,
  remoteStreams: [],
  screenStream: null,
  remoteScreens: {
    camera: {
        id: "",
        stream: {}
    },
    screen: {
        id: "",
        stream: {}
    }
  },
  screenShareActive: false,
  params: {}
}
function handleEvents (rtc) {
  // Occurs when an error message is reported and requires error handling.
  rtc.client.on("error", (err) => {
    console.log(err);
  });
  // Occurs when the peer user leaves the channel; for example, the peer user calls Client.leave.
  rtc.client.on("peer-leave", function (evt) {
    var id = evt.uid;
    console.log("id", evt);
    let streams = rtc.remoteStreams.filter(e => id !== e.getId());
    let peerStream = rtc.remoteStreams.find(e => id === e.getId());
    if(peerStream && peerStream.isPlaying()) {
      peerStream.stop();
    }
    rtc.remoteStreams = streams;
    if (id !== rtc.params.uid) {
      removeView(id);
    }
    var calltype = params['channel'].split("_");
    if(calltype[0] != 'groupClass'){
      leave(rtc);
    }
    console.log("peer-leave", id);
  });
  // Occurs when the local stream is published.
  rtc.client.on("stream-published", function (evt) {
    local_stream_video_add_control();
    console.log("stream-published");
  });
  // Occurs when the remote stream is added.
  rtc.client.on("stream-added", function (evt) {  
    var remoteStream = evt.stream;
    var id = remoteStream.getId();
    if (id !== rtc.params.uid) {
      rtc.client.subscribe(remoteStream, function (err) {
        console.log("stream subscribe failed", err);
      });
    }
    local_stream_video_add_control();
    console.log("stream-added remote-uid: ", id);
  });
  // Occurs when a user subscribes to a remote stream.
  rtc.client.on("stream-subscribed", function (evt) {
    var remoteStream = evt.stream;
    var id = remoteStream.getId();
    rtc.remoteStreams.push(remoteStream);
    addView(id);
    remoteStream.play("remote_video_" + id);
    //Call function to add fullscreen in videos.
    local_stream_video_add_control();
    console.log("stream-subscribed remote-uid: ", id);
  });
  // Occurs when the remote stream is removed; for example, a peer user calls Client.unpublish.
  rtc.client.on("stream-removed", function (evt) {
    var remoteStream = evt.stream
    var id = remoteStream.getId()
    if(remoteStream.isPlaying()) {
      remoteStream.stop()
    }
    rtc.remoteStreams = rtc.remoteStreams.filter(function (stream) {
      return stream.getId() !== id
    });
    removeView(id)
    console.log("stream-removed remote-uid: ", id)
  });
  rtc.client.on("onTokenPrivilegeWillExpire", function(){
    console.log("onTokenPrivilegeWillExpire")
  });
  rtc.client.on("onTokenPrivilegeDidExpire", function(){
    console.log("onTokenPrivilegeDidExpire")
  });
}
function join (rtc, option) {
  if (rtc.joined) {
    console.log("Your already joined")
    return;
  }

 

  console.log(option);
  chatchannel = rtmclient.createChannel(option.channel);
  rtc.client = AgoraRTC.createClient({mode: option.mode, codec: option.codec});
  rtc.params = option;
  handleEvents(rtc);
  rtc.client.init(option.appID, function () {
    rtc.client.join(option.token ? option.token : null, option.channel,option.uid ? +option.uid : null,function(uid) {
      console.log("join channel: " + option.channel + " success, uid: " + uid)
      rtc.joined = true;
      rtc.params.uid = uid;
      var calltype = option.channel.split('_');
      if(calltype[0] == 'groupClass'){
        var usertype = params['agoraUserId'].split("_");
        if(usertype[0] == 'S'){
          // create local stream
          rtc.localStream = AgoraRTC.createStream({
            streamID: rtc.params.uid,
            audio: true,
            video: false,
            screen: false,
            microphoneId: option.microphoneId,
            cameraId: option.cameraId
          });
        } else {
          // create local stream
          rtc.localStream = AgoraRTC.createStream({
            streamID: rtc.params.uid,
            audio: true,
            video: true,
            screen: false,
            microphoneId: option.microphoneId,
            cameraId: option.cameraId
          });
        }
      } else if(calltype[calltype.length - 1] == 'Audio'){
        // create local stream
        rtc.localStream = AgoraRTC.createStream({
          streamID: rtc.params.uid,
          audio: true,
          video: false,
          screen: false,
          microphoneId: option.microphoneId,
          cameraId: option.cameraId
        });
      } else {
        // create local stream
        rtc.localStream = AgoraRTC.createStream({
          streamID: rtc.params.uid,
          audio: true,
          video: true,
          screen: false,
          microphoneId: option.microphoneId,
          cameraId: option.cameraId
        });
      }
      // initialize local stream. Callback function executed after intitialization is done
      rtc.localStream.init(function () {
        console.log("init local stream success")
        // play stream with html element id "local_stream"
        rtc.localStream.play("local_stream");
        //rtc.localStream.setAudioVolume(100);
        //rtc.localStream.adjustRecordingSignalVolume(100);
        //rtc.localStream.adjustPlaybackSignalVolume(100);
        // publish local stream
        publish(rtc);
        chatchannel.join(params['agoraUserId']).then(() => {
          rtmclient.getUserAttributes(params['agoraUserId']).then((userattr)=>{
            $('#local_stream').parent('.video-view').prepend("<h3>"+userattr.name+" Video</h3>");
            succNotify(userattr.name+" has join a group class");
             window.onbeforeunload = (event) => {
                const e = event || window.event;
                // Cancel the event
                e.preventDefault();
                if (e) {
                  // e.returnValue = 'Do you want to leave the page?'; // Legacy method for cross browser support
                  return "string";
                }
                // return 'Do you want to leave the page?'; // Legacy method for cross browser support
                return "string";
              };

              
          }).catch(error => {
            console.error("get user attributes failed",error);
          });
        }).catch(error => {
          console.error("client chat chanel join failed",error);
        });
        chatchannelevent();
      }, function (err)  {
        console.error("init local stream failed",err);
      });
    }, function(err) {
      console.error("client join failed", err);
    });
  }, (err) => {
    console.error(err);
  });
}
function chatchannelevent()
{
  chatchannel.on('ChannelMessage',(messages,senderId)=>{
    rtmclient.getUserAttributes(senderId).then((userattr)=>{
      $('.conversation-list').append('<div class="chat-receiver"><div class="chat-profile" style="margin-right:16px;width: 50px;height: 51px;float: left;"><img class="profile__image" src="'+userattr.image+'"></div><p>'+messages.text+'</p><span>'+userattr.name+'</span></div>');
      $('.conversation-list').scrollTo('100%', '100%', {
        easing: 'swing'
      }); 
    });
  });
}
function publish (rtc) {
  if (!rtc.client) {
    console.log("Please Join Room First");
    return;
  }
  if (rtc.published) {
    console.log("Your already published");
    return;
  }
  var oldState = rtc.published;
  // publish localStream
  rtc.client.publish(rtc.localStream, function (err) {
    rtc.published = oldState;
    console.log("publish failed");
    console.error(err);
  });
  rtc.published = true;
}
function unpublish (rtc) {
  if (!rtc.client) {
    console.log("Please Join Room First");
    return;
  }
  if (!rtc.published) {
    console.log("Your didn't publish");
    return;
  }
  var oldState = rtc.published;
  rtc.client.unpublish(rtc.localStream, function (err) {
    rtc.published = oldState;
    console.log("unpublish failed");
    console.error(err);
  });
  rtc.published = false
}
// SCREEN SHARING
function initScreenShare(rtc, option) {

  console.log("============SCREEN SHARE=======");
  rtc.screen = AgoraRTC.createClient({mode: 'rtc', codec: 'vp8'});
  rtc.screen.init(option.appID, function () {
    console.log("AgoraRTC screenClient initialized");
    option['uid'] = '';
    joinChannelAsScreenShare(rtc, option);
    screenShareActive = true;
  }, function (err) {
    console.log("[ERROR] : AgoraRTC screenClient init failed", err);
  });  
}
function joinChannelAsScreenShare(rtc, option) {
  console.log(option)
  var calltype = option.channel.split("_");

  var rndnumber = Math.floor((Math.random() * 100) + 1);

  if(calltype[0] == 'groupClass'){
    var screenshareId = parseInt('111'+calltype[1]+rndnumber+'111');
  } else {
    var screenshareId = parseInt('111'+calltype[calltype.length - 2]+rndnumber+'111');
  }

  console.log('screenshareId screenshareId == screenshareId == screenshareId=== screenshareId== screenshareId');
  console.log(screenshareId);
  console.log('END TAB END TAB screenshareId screenshareId == screenshareIdEND TAB END TAB END TAB');

  rtc.params.uid = screenshareId;
  rtc.screen.join(option.token ? option.token : null, option.channel, screenshareId ? screenshareId : null, function(uid) {
    rtc.params.uid = screenshareId;
    rtc.remoteScreens.screen.id = screenshareId;  // keep track of the uid of the screen stream.
    // Create the stream for screen sharing.
    const streamSpec = {
      streamID: screenshareId,
      audio: false,
      video: false,
      screen: true
    }
    // Create the stream for screen sharing.
    rtc.screenStream = AgoraRTC.createStream(streamSpec);
    // screenStream.setScreenProfile(screenVideoProfile); // set the profile of the screen
    rtc.screenStream.init(function(){
      console.log("getScreen successful");
      rtc.screenStream.play('local_stream');
      rtc.remoteScreens.screen.stream = rtc.screenStream; // keep track of the screen stream
      // $("#screen-share-btn").prop("disabled",false); // enable button
      rtc.screen.publish(rtc.screenStream, function (err) {
        console.log("[ERROR] : publish screen stream error: " + err);
      });
    }, function (err) {
      console.log("[ERROR] : getScreen failed", err);
      rtc.remoteScreens.screen.id = ""; // reset screen stream id
      rtc.remoteScreens.screen.stream = {}; // reset the screen stream
      rtc.screenShareActive = false; // resest screenShare
      // toggleScreenShareBtn(); // toggle the button icon back (will appear disabled)
    });
  }, function(err) {
    console.log("[ERROR] : join channel as screen-share failed", err);
  });
  rtc.screen.on('stream-published', function (evt) {
    console.log("Publish screen stream successfully");
    rtc.remoteScreens.camera.stream.disableVideo(); // disable the local video stream (will send a mute signal)
    rtc.remoteScreens.camera.stream.stop(); // stop playing the local stream
    // TODO: add logic to swap main video feed back from container
    rtc.remoteScreens[mainStreamId].stop(); // stop the main video stream playback
    addRemoteStreamMiniView(rtc.remoteScreens[mainStreamId]); // send the main video stream to a container
    // localStreams.screen.stream.play('full-screen-video'); // play the screen share as full-screen-video (vortext effect?)
    // $("#video-btn").prop("disabled",true); // disable the video button (as cameara video stream is disabled)
  });
  rtc.screen.on('stopScreenSharing', function (evt) {
    console.log("screen sharing stopped", err);
  });
}
function toggleScreenShareBtn() {
  $('#screen-share-btn').toggleClass('btn-danger');
  $('#screen-share-icon').toggleClass('fa-share-square').toggleClass('fa-times-circle');
}
function stopScreenShare() {
  rtc.remoteScreens.screen.stream.disableVideo(); // disable the local video stream (will send a mute signal)
  rtc.remoteScreens.screen.stream.stop(); // stop playing the local stream
  rtc.remoteScreens.camera.stream.enableVideo(); // enable the camera feed
  rtc.remoteScreens.camera.stream.play('local-video'); // play the camera within the full-screen-video div
  $("#video-btn").prop("disabled",false);
  rtc.screen.leave(function() {
    rtc.screenShareActive = false; 
    console.log("screen client leaves channel");
    $("#screen-share-btn").prop("disabled",false); // enable button
    rtc.screen.unpublish(rtc.remoteScreens.screen.stream); // unpublish the screen client
    rtc.remoteScreens.screen.stream.close(); // close the screen client stream
    //rtc.remoteScreens.screen.id = ""; // reset the screen id
    //rtc.remoteScreens.screen.stream = {}; // reset the stream obj
  }, function(err) {
    console.log("client leave failed ", err); //error handling
  }); 
}
// REMOTE STREAMS UI
function addRemoteStreamMiniView(remoteStream){
  var streamId = remoteStream.getId();
  // append the remote stream template to #remote-streams
  $('#remote-streams').append(
    $('<div/>', {'id': streamId + '_container',  'class': 'remote-stream-container col'}).append(
      $('<div/>', {'id': streamId + '_mute', 'class': 'mute-overlay'}).append(
        $('<i/>', {'class': 'fas fa-microphone-slash'})
      ),
      $('<div/>', {'id': streamId + '_no-video', 'class': 'no-video-overlay text-center'}).append(
        $('<i/>', {'class': 'fas fa-user'})
      ),
      $('<div/>', {'id': 'agora_remote_' + streamId, 'class': 'remote-video'})
    )
  );
  remoteStream.play('agora_remote_' + streamId); 
  var containerId = '#' + streamId + '_container';
  $(containerId).dblclick(function() {
    // play selected container as full screen - swap out current full screen stream
    remoteStreams[mainStreamId].stop(); // stop the main video stream playback
    addRemoteStreamMiniView(remoteStreams[mainStreamId]); // send the main video stream to a container
    $(containerId).empty().remove(); // remove the stream's miniView container
    remoteStreams[streamId].stop() // stop the container's video stream playback
    remoteStreams[streamId].play('full-screen-video'); // play the remote stream as the full screen video
    mainStreamId = streamId; // set the container stream id as the new main stream id
  });
}
function leave (rtc) {
  if (!rtc.client) {
    console.log("Please Join First!");
    return;
  }
  if (!rtc.joined) {
    console.log("You are not in channel");
    return;
  }
  rtc.client.leave(function () {
    // stop stream
    if(rtc.localStream.isPlaying()) {
      rtc.localStream.stop();
    }
    // close stream
    rtc.localStream.close();
    for (let i = 0; i < rtc.remoteStreams.length; i++) {
      var stream = rtc.remoteStreams.shift();
      var id = stream.getId();
      if(stream.isPlaying()) {
        stream.stop();
      }
      removeView(id);
    }
    rtc.localStream = null;
    rtc.remoteStreams = [];
    rtc.client = null;
    console.log("client leaves channel success");
    rtc.published = false;
    rtc.joined = false;
    $('#agoravideocalloption').show();
    $('#agoravideoacceptedtojoin').hide();
    $('#receiveagoravideocall').modal('hide');
    window.location.href = SITE_URL;
  }, function (err) {
    console.log("channel leave failed");
    console.error(err);
  });
}
// This function automatically executes when a document is ready.
$(function () {
  // This will fetch all the devices and will populate the UI for every device. (Audio and Video)
  getDevices(function (devices) {
    devices.audios.forEach(function (audio) {
      $("<option/>", {
        value: audio.value,
        text: audio.name,
      }).appendTo("#microphoneId");
    });
    devices.videos.forEach(function (video) {
      $("<option/>", {
        value: video.value,
        text: video.name,
      }).appendTo("#cameraId");
    });
    // To populate UI with different camera resolutions
    resolutions.forEach(function (resolution) {
      $("<option/>", {
        value: resolution.value,
        text: resolution.name
      }).appendTo("#cameraResolution");
    });
    //M.AutoInit();
  });
});
function diff_minutes(dt2, dt1) 
{
  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));  
}
$(".agoravideocallleave").on("click", function (e) {
  e.preventDefault();
  console.log(params);
  var calltype = params['channel'].split("_");
  var usertype = params['agoraUserId'].split("_");
  var agoracall_end_datetime = new Date();
  var duration = diff_minutes(agoracall_end_datetime,agoracall_start_datetime);
  if(calltype[0] == 'groupClass'){
    var ajaxurl = SITE_URL+"complete-group-class-call";
    var ajaxparam = {'group_class_id':calltype[1],'duration':duration};
  } else {
    if(usertype[0] == 'S'){
      var ajaxurl = SITE_URL+"student-complete-session-call";
    } else {
      var ajaxurl = SITE_URL+"tutor-complete-session-call";
    }
    var ajaxparam = {'session_id':calltype[calltype.length - 2],'duration':duration};
  }
  $.ajax({
    url:ajaxurl, 
    type:'POST',
    data:ajaxparam,
    dataType:'json',
    error:function(jqXHR,textStatus,errorThrown){
      errorNotify(errorThrown);
    },
    success:function(result){
      if(result.code == '1'){
      } else {
        errorNotify(result.message);
      }
    }
  });
  leave(rtc);
});
$(".agoravideocallscreen_share").on("click", function (e) {
	e.preventDefault();
	initScreenShare(rtc,params);
});

function sendagorachannelmessage(){
  chatchannel.sendMessage({text:$('.chat-input').val()}).then(() => {
    rtmclient.getUserAttributes(params['agoraUserId']).then((userattr)=>{
      $('.conversation-list').append('<div class="chat-sender"><div class="chat-profile" style="margin-left: 16px;width: 50px;height: 51px;float: right;"><img class="profile__image" src="'+userattr.image+'"></div><p>'+$('.chat-input').val()+'</p><span>'+userattr.name+'</span></div>');
      $('.chat-input').val('');
      $('.conversation-list').scrollTo('100%', '100%', {
        easing: 'swing'
      }); 
    });
  }).catch(error => {
    errorNotify('Channel message has not send'+error);
  });
}
function sendchannelmessage(event)
{
  if(event.keyCode == 13){
    sendagorachannelmessage();
  }
}
$('.agoramuteaudio').click(function(){
  rtc.localStream.muteAudio();
  rtc.localStream.setAudioVolume(0);
  $(this).removeClass('agoramuteaudio');
  $(this).addClass('agoraunmuteaudio');
  $(this).text('Unmute');
  succNotify("Audio Has Muted successfully");
});
$('.agoraunmuteaudio').click(function(){
  rtc.localStream.unmuteAudio();
  rtc.localStream.setAudioVolume(100);
  $(this).removeClass('agoraunmuteaudio');
  $(this).addClass('agoramuteaudio');
  $(this).text('Mute');
  succNotify("Audio Has Unmuted successfully");
});

//Screen share add full screen option
function local_stream_video_add_control() {
  $("video").prop('controls', true);
}



