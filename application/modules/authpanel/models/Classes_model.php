<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Classes_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.program_title','tc.description','tc.start_datetime','tc.end_datetime', 'tc.insert_datetime','tc.price','tc.total_point','tc.age_category','tc.subadmin_id');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*,TIMEDIFF(tc.start_datetime,NOW()) AS dtime , tc.id, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image,IF(tc.start_datetime >= NOW(),1,0) as date_flag ,IF(tc.end_datetime <= NOW(),1,0) as date_flag_end,IF(NOW() BETWEEN tc.start_datetime AND tc.end_datetime , 1 ,0) AS between_flag ,ta.name as subadmin_name");
        $this->db->from('tbl_master_class tc');
        $this->db->join('tbl_admin ta','tc.subadmin_id = ta.id','left');
        $this->db->where('tc.is_deleted','0');

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

       private function _get_datatables_booking($contest_id)
    {
        // $booking_id = $this->session->userdata('booking_id');
        $this->column_search = array('u.username');
        $this->order = array('o.id' => 'DESC');
        $this->db->select("u.*");
        $this->db->from('tbl_order o');
        $this->db->join('tbl_order_detail od','o.id = od.order_id','left');
        $this->db->join('tbl_user u','o.user_id = u.id','left');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('od.item_id',$contest_id);
        $this->db->where('od.item_type','class');
        $this->db->where('u.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('u.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    
    /**
    ** Datatable query
    */
    private function _get_datatables_query_enrolled()
    {
        $this->column_search = array('tc.program_title','tu.username', 'tc.age_category');
        $this->order = array('tor.id' => 'DESC');
        $this->db->select("tor.*, tor.id, tc.program_title, tu.username, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image, tc.age_category");
        $this->db->from('tbl_order tor');
        $this->db->join('tbl_master_class tc', 'tor.item_id = tc.id', 'left');
        $this->db->join('tbl_user tu', 'tor.user_id = tu.id', 'left');
        $this->db->join('tbl_order_detail od', 'tor.id = od.order_id', 'left');
        $this->db->where('tor.is_deleted','0');
        $this->db->where('od.item_type','class');
        
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $prefix = 'tor.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'username') {
                $prefix = 'tu.';
                $key = 'username';
            }  else if ($_REQUEST['sort'] == 'program_title') {
                $prefix = 'tc.';
                $key = 'program_title';
            } else if ($_REQUEST['sort'] == 'age_category') {
                $prefix = 'tc.';
                $key = 'age_category';
            }
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
            // $this->db->order_by('tor.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables_enrolled(){
        $this->_get_datatables_query_enrolled();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered_enrolled(){
        $this->_get_datatables_query_enrolled();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query_gift()
    {
        $this->column_search = array('tc.program_title','tu.email','tu.username', 'tc.age_category');
        $this->order = array('tor.id' => 'DESC');
        $this->db->select("tor.*, tor.id, tc.program_title, tu.username , tu.email, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tu.profile_image) as profile_image, tc.age_category");
        $this->db->from('tbl_gift_enrolledclass tor');
        $this->db->join('tbl_master_class tc', 'tor.class_id = tc.id', 'left');
        $this->db->join('tbl_user tu', 'tor.receiver_id = tu.id', 'left');
        $this->db->where('tor.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
             $prefix = 'tor.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'username') {
                $prefix = 'tu.';
                $key = 'username';
            }  else if ($_REQUEST['sort'] == 'program_title') {
                $prefix = 'tc.';
                $key = 'program_title';
            } else if ($_REQUEST['sort'] == 'age_category') {
                $prefix = 'tc.';
                $key = 'age_category';
            }
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
            // $this->db->order_by('tor.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables_gift(){
        $this->_get_datatables_query_gift();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
  function get_datatables_gift_export(){
        $this->_get_datatables_query_gift();
        // if($_REQUEST['limit'] != -1)
        // $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return false;
        }
    }

    /*
    ** Function to get count records
    */
    function count_filtered_gift(){
        $this->_get_datatables_query_gift();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
    ** Function to execute datatable query
    */
    function get_datatables_booking($contest_id){
        $this->_get_datatables_booking($contest_id);
        // print_r($contest_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        // print_r($query->result());
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
     function count_filtered_d($contest_id){
        $this->_get_datatables_booking($contest_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_user_list($user_id = 0,$contest_id,$is_block_contest = 0){
        $this->db->select("u.*,od.order_id,od.id AS order_detail_id");
        $this->db->from('tbl_order o');
        $this->db->join('tbl_order_detail od','o.id = od.order_id','left');
        $this->db->join('tbl_user u','o.user_id = u.id','left');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('od.item_id',$contest_id);
        $this->db->where('od.item_type','class');
        $this->db->where('u.is_deleted','0');
        if($user_id != 0){
            $this->db->where("o.user_id",$user_id);
        }
        if($is_block_contest != 0){
            $this->db->where("od.is_block_contest",$is_block_contest);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return false;
        }
    }
    
}
