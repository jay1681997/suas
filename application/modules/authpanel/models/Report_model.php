<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Report_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.program_title','tv.name','tmc.name','tmc.name', 'tms.name','tp.promocode','tus.title','tu.username', 'tor.discount','tor.tax','tor.subtotal','tor.payment_mode','tor.order_status','tor.totalamount');
        $this->order = array('tor.id' => 'DESC');
        $this->db->select("tor.*,od.item_type,tor.id, tc.program_title as class_name, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image, tv.name as video_name, CONCAT('".S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE."','',tv.video) as video_image, tmc.name as contest_name, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tmc.contest_image) as contest_image, tmc.name as contest_name,tmc.contest_type, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tmc.contest_image) as contest_image, tms.name as subscription_name, CONCAT('".S3_BUCKET_ROOT.SUBSCRIPTION_IMAGE."','',If(tms.image != '',tms.image,'default.png')) as subscription_image, tp.promocode as promocode_name, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as promocode_image, tus.title as speech_name, CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.video) as speech_image , CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',tus.thumb_image) as speech_thumb_image, tu.username, ts.username as student_name, (SELECT currency_symbol from tbl_setting where is_deleted = '0' AND status='Active') as currency");
        $this->db->from('tbl_order tor');
        $this->db->join('tbl_order_detail od', 'tor.id = od.order_id', 'left');
        $this->db->join('tbl_master_class tc','od.item_id = tc.id','left');
        $this->db->join('tbl_master_videolibrary tv','od.item_id = tv.id','left');
        $this->db->join('tbl_master_contest tmc','od.item_id = tmc.id','left');
        $this->db->join('tbl_master_subscription tms','od.item_id = tms.id','left');
        $this->db->join('tbl_promocode tp','od.item_id = tp.id','left');
        $this->db->join('tbl_user_speech tus','od.item_id = tus.id','left');
        $this->db->join('tbl_user tu','tor.user_id = tu.id','left');
        $this->db->join('tbl_student ts','tor.student_id = ts.id','left');
        $this->db->where('tor.is_deleted','0');
        $this->db->where('tu.is_deleted','0');
        $this->db->group_by('tor.id'); 
        $category_type = $this->session->userdata('category_type');
        if(!empty($category_type)){
            $this->db->where('od.item_type',$category_type);
        }

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){

            $prefix = 'tor.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'username') {
                $prefix = 'tu.';
                $key = 'username';
            }else if ($_REQUEST['sort'] == 'username') {
                $prefix = 'ts.';
                $key = 'username';
            }else if ($_REQUEST['sort'] == 'student_name') {
                $prefix = 'ts.';
                $key = 'username';
            }else if ($_REQUEST['sort'] == 'program_title') {
                $prefix = 'tc.';
                $key = 'program_title';
            }else if ($_REQUEST['sort'] == 'program_title') {
                $prefix = 'tc.';
                $key = 'class_name';
            }else if ($_REQUEST['sort'] == 'video_name') {
                $prefix = 'tv.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'name') {
                $prefix = 'tv.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'contest_name') {
                $prefix = 'tmc.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'name') {
                $prefix = 'tmc.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'name') {
                $prefix = 'tms.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'subscription_name') {
                $prefix = 'tms.';
                $key = 'name';
            }else if ($_REQUEST['sort'] == 'promocode') {
                $prefix = 'tp.';
                $key = 'promocode';
            }else if ($_REQUEST['sort'] == 'promocode_name') {
                $prefix = 'tp.';
                $key = 'promocode';
            }else if ($_REQUEST['sort'] == 'speech_name') {
                $prefix = 'tus.';
                $key = 'title';
            }else if ($_REQUEST['sort'] == 'title') {
                $prefix = 'tus.';
                $key = 'title';
            }
      
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
           

        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_totalSumOfOrders(){
        $this->db->select("tor.*, IFNULL(SUM(tor.totalamount),0) as total_amount, (SELECT currency_symbol from tbl_setting where is_deleted = '0' AND status='Active') as currency");
        $this->db->from("tbl_order tor");
        $this->db->join('tbl_order_detail od', 'tor.id = od.order_id', 'left');
           $this->db->join('tbl_user tu','tor.user_id = tu.id','left');
        $this->db->where('tor.status','Confirm');
        $this->db->where('tor.order_status','Paid');
        $this->db->where('tor.is_refund','0');
        $this->db->where('tu.is_deleted','0');
           $this->db->where('tor.is_deleted','0');
        // $this->db->group_by('od.order_id'); 

        $category_type = $this->session->userdata('category_type');
        if(!empty($category_type)){
            $this->db->where('od.item_type',$category_type);
        }
        return $this->db->get()->row_array();

    }

     private function get_user_datatables_query()
    {
        $this->column_search = array('tc.username','tc.email','tc.country_code','tc.phone','tc.otp_code', 'tc.otp_status', 'tc.insert_datetime','tc.login_status','tc.age','tc.dob');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*, tc.id, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tc.profile_image) as profile_image,(SELECT count(id) AS total_video from tbl_user_speech AS s where tc.id = s.user_id AND s.is_deleted = '0') AS total_video,
            (SELECT count(o.id) AS total_contest FROM suasdb.tbl_order AS o left join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = tc.id AND o.is_deleted = '0' AND od.is_deleted = '0' AND od.item_type = 'contest') AS total_contest,(SELECT count(o.id) AS total_classes FROM suasdb.tbl_order AS o left join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = tc.id AND o.is_deleted = '0' AND od.is_deleted = '0' AND od.item_type = 'class') AS total_classes,(SELECT count(o.id) AS total_promocode FROM suasdb.tbl_order AS o left join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = tc.id AND o.is_deleted = '0' AND od.is_deleted = '0' AND od.item_type = 'promocode') AS total_promocode,(SELECT COUNT(*) AS total_promocode_use
FROM (
    SELECT COUNT(promocode) AS promocode_count
    FROM suasdb.tbl_order
    WHERE promocode != '' AND promocode != '0' AND user_id = tc.id AND is_deleted = '0'
    GROUP BY promocode
) AS subquery) AS total_promocode_use");
        $this->db->from('tbl_user tc');
        $this->db->where('tc.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $prefix = 'tc.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'total_video') {
                $prefix = '';
                $key = 'total_video';
            }elseif ($_REQUEST['sort'] == 'total_contest') {
                 $prefix = '';
                $key = 'total_contest';
            }elseif ($_REQUEST['sort'] == 'total_classes') {
                 $prefix = '';
                $key = 'total_classes';
            }elseif ($_REQUEST['sort'] == 'total_promocode') {
                 $prefix = '';
                $key = 'total_promocode';
            }elseif ($_REQUEST['sort'] == 'total_promocode_use') {
                 $prefix = '';
                $key = 'total_promocode_use';
            }
            // $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_user_data(){
        $this->get_user_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
   function count_user_filtered(){
        $this->get_user_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

  function get_video_datatables_query($user_id)
  {
        $this->column_search = array("ts.title,ts.description,ts.uploading_type,ts.is_evaluated,ts.insert_datetime");
        $this->order = array('ts.id' => 'DESC');
        $this->db->select("ts.*,CONCAT('".S3_BUCKET_ROOT.SPEECH_IMAGE."','',ts.video) as video_url");
        $this->db->from('tbl_user_speech ts');
        $this->db->where('ts.is_deleted','0');
        $this->db->where("ts.user_id",$user_id);
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('ts.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }  
  }
    function get_video_data($user_id){
        $this->get_video_datatables_query($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
    function count_video_filtered($user_id='')
    {
        $this->get_video_datatables_query($user_id);
        $query = $this->db->get();
        return $query->num_rows(); 
    }
     

      function get_contest_datatables_query($user_id)
  {
        $this->column_search = array('tc.name','tc.description','tc.contest_type','tc.total_participant','tc.contest_startdate','tc.contest_enddate', 'tc.insert_datetime','tc.price','tc.total_point','tc.age_category','tc.winner_date','tc.contest_by');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*,o.insert_datetime,tc.id AS contest_id, tc.id, TIMEDIFF(tc.contest_startdate,NOW()) AS dtime ,CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, ta.name as subadmin_name , IF(NOW() BETWEEN tc.contest_startdate AND tc.contest_enddate , 1 ,0) AS between_flag ,IF(tc.contest_startdate >= NOW(),1,0) as date_flag");
        $this->db->from("tbl_order o");
        $this->db->join("tbl_order_detail od",'o.id = od.order_id','left');
        $this->db->join('tbl_master_contest tc','tc.id = od.item_id','left');
        $this->db->join('tbl_admin ta','tc.subadmin_id = ta.id','left');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('o.user_id',$user_id);
        $this->db->where('od.item_type','contest');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }  
  }
    function get_contest_data($user_id){
        $this->get_contest_datatables_query($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
    function count_contest_filtered($user_id='')
    {
        $this->get_contest_datatables_query($user_id);
        $query = $this->db->get();
        return $query->num_rows(); 
    }

     function get_class_datatables_query($user_id)
  {
        $this->column_search = array('tc.program_title','tc.description','tc.start_datetime','tc.end_datetime', 'tc.insert_datetime','tc.price','tc.total_point','tc.age_category','tc.subadmin_id');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*,o.insert_datetime,TIMEDIFF(tc.start_datetime,NOW()) AS dtime , tc.id, CONCAT('".S3_BUCKET_ROOT.CLASS_IMAGE."','',tc.class_image) as class_image,IF(tc.start_datetime >= NOW(),1,0) as date_flag ,IF(tc.end_datetime <= NOW(),1,0) as date_flag_end,IF(NOW() BETWEEN tc.start_datetime AND tc.end_datetime , 1 ,0) AS between_flag ,ta.name as subadmin_name");
        $this->db->from("tbl_order o");
        $this->db->join("tbl_order_detail od",'o.id = od.order_id','left');
        $this->db->join('tbl_master_class tc','tc.id = od.item_id','left');
        $this->db->join('tbl_admin ta','tc.subadmin_id = ta.id','left');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('o.user_id',$user_id);
        $this->db->where('od.item_type','class');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }  
  }
    function get_class_data($user_id){
        $this->get_class_datatables_query($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
    function count_class_filtered($user_id='')
    {
        $this->get_class_datatables_query($user_id);
        $query = $this->db->get();
        return $query->num_rows(); 
    }


    function get_promocode_datatables_query($user_id)
  {
        $this->column_search = array('tp.promocode', 'tp.discount', 'tp.discount_type', 'tp.per_user_usage', 'tp.maxusage', 'tp.insert_datetime','tp.start_date','tp.end_date');
        $this->order = array('tp.id' => 'DESC');
        $this->db->select("tp.*,o.insert_datetime,tp.id, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as promo_image");
        $this->db->from("tbl_order o");
        $this->db->join("tbl_order_detail od",'o.id = od.order_id','left');
        $this->db->join('tbl_promocode tp','tp.id = od.item_id','left');
        // $this->db->join('tbl_admin ta','tc.subadmin_id = ta.id','left');
        $this->db->where('tp.is_deleted','0');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('o.user_id',$user_id);
        $this->db->where('od.item_type','promocode');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tp.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }  
  }
    function get_promocode_data($user_id){
        $this->get_promocode_datatables_query($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
    function count_promocode_filtered($user_id='')
    {
        $this->get_promocode_datatables_query($user_id);
        $query = $this->db->get();
        return $query->num_rows(); 
    }

     function get_promocode_use_datatables_query($user_id)
  {
        $this->column_search = array('tp.promocode', 'tp.discount', 'tp.discount_type', 'tp.per_user_usage', 'tp.maxusage', 'tp.insert_datetime','tp.start_date','tp.end_date');
        $this->order = array('tp.id' => 'DESC');
        $this->db->select("tp.*,o.insert_datetime, tp.id, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as promo_image");
        $this->db->from("tbl_order o");
        $this->db->join('tbl_promocode tp','tp.promocode = o.promocode','left');
        $this->db->where('tp.is_deleted','0');
        $this->db->where('o.is_deleted','0');
        // $this->db->where('od.is_deleted','0');
        $this->db->where('o.user_id',$user_id);
        // $this->db->where('od.item_type','promocode');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tp.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }  
  }
    function get_promocode_use_data($user_id){
        $this->get_promocode_use_datatables_query($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
         // echo "<pre>"; print_r($this->db->last_query()); die;
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
    function count_promocode_use_filtered($user_id='')
    {
        $this->get_promocode_use_datatables_query($user_id);
        $query = $this->db->get();
        return $query->num_rows(); 
    }
}
?>