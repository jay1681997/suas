<?php
/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Contacts_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * get a contacts record count for filtered
    */
    function count_filtered() {

        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
    * get a contacts record search
    */
    private function _get_datatables_query() {
        
        $this->db->select('m.*, CONCAT(tu.firstname," ",tu.lastname) as username');
        $this->db->from('tbl_contact_us m');
        $this->db->join('tbl_user tu','m.user_id=tu.id','LEFT');
        $this->db->Where('m.is_deleted','0');
        $this->db->group_by('m.id');
        $i = 0;
        $order_by = array('id' => 'desc');
        $search_fields = array('m.email','m.subject','m.description','m.is_reply', 'tu.firstname','tu.lastname', 'm.reply_message');

        foreach ($search_fields as $item) {
            if(isset($_REQUEST['search'])) {
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                } else {
                    $this->db->or_like($item, $_REQUEST['search']);
                }
 
                if(count($search_fields) - 1 == $i){
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($_REQUEST['order'])) {
            $this->db->order_by($_REQUEST['sort'], $_REQUEST['order']);
        } else if(isset($order_by)) {
            $order = $order_by;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /*
     * get a contacts record by limit
    */
    function get_contactsdata_list() {

        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
       // echo  $this->db->last_query(); die;
        if($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * Function to get contact us details
     */
    function contactsdata($contact_id){
        // $this->db->select('m.*, CONCAT(tu.firstname," ",tu.lastname) as username');
        $this->db->select('m.*, tu.id AS user_ids,  tu.firstname, tu.lastname, CONCAT(tu.firstname," ",tu.lastname) as usernames , m.name AS username');
        $this->db->from('tbl_contact_us m');
        $this->db->from('tbl_user tu','m.user_id=tu.id','LEFT');
        $this->db->Where('m.is_deleted','0');
        $this->db->where('m.id',$contact_id);
        $this->db->group_by('m.id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }
}