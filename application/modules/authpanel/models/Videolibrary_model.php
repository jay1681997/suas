<?php

/*
** Project      : SUAS
** Date         : 18-feb-2022
** Modified On  : -  
*/
class Videolibrary_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tp.name','tp.category','tp.description','tp.insert_datetime','tp.price','tp.total_point');
        $this->order = array('tp.id' => 'DESC');
        $this->db->select("tp.*, tp.id, IF(tp.image = '', '', CONCAT('" .S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE. "','', tp.image)) as image, IF(tp.video = '', '', CONCAT('" .S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE. "','', tp.video)) as video");
        $this->db->from('tbl_master_videolibrary tp');
        $this->db->where('tp.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tp.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
    ** Function to get post
    */
    function view_videolibrary($post_id)
    {        
        $this->db->select("*, IF(image = '', '', CONCAT('" .S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE. "','', image)) as image, IF(video = '', '', CONCAT('" .S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE. "','', video)) as video");
        $this->db->from('tbl_master_videolibrary');        
        $this->db->where('id', $post_id);        
        $result = $this->db->get()->row_array();        
        if(isset($result) && !empty($result)){           
            return $result;
        } else {
            return false;
        }
    }
}
?>