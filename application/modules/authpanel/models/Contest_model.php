<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Contest_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.name','tc.description','tc.contest_type','tc.total_participant','tc.contest_startdate','tc.contest_enddate', 'tc.insert_datetime','tc.price','tc.total_point','tc.age_category','tc.winner_date','tc.contest_by');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*,tc.id AS contest_id, tc.id, TIMEDIFF(tc.contest_startdate,NOW()) AS dtime ,CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.contest_image) as contest_image, ta.name as subadmin_name , IF(NOW() BETWEEN tc.contest_startdate AND tc.contest_enddate , 1 ,0) AS between_flag ,IF(tc.contest_startdate >= NOW(),1,0) as date_flag");
        $this->db->from('tbl_master_contest tc');
        $this->db->join('tbl_admin ta','tc.subadmin_id = ta.id','left');
        $this->db->where('tc.is_deleted','0');

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

     private function _get_datatables_booking($contest_id)
    {
        // $booking_id = $this->session->userdata('booking_id');
        $this->column_search = array('u.username');
        $this->order = array('o.id' => 'DESC');
        $this->db->select("u.*");
        $this->db->from('tbl_order o');
        $this->db->join('tbl_order_detail od','o.id = od.order_id','left');
        $this->db->join('tbl_user u','o.user_id = u.id','left');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('od.item_id',$contest_id);
        $this->db->where('od.item_type','contest');
        $this->db->where('u.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('u.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }

    /*
    ** Function to execute datatable query
    */
    function get_datatables_booking($contest_id){
        $this->_get_datatables_booking($contest_id);
        // print_r($contest_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        // print_r($query->result());
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
     function count_filtered_d($contest_id){
        $this->_get_datatables_booking($contest_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query_contest($contest_id)
    {
        $this->column_search = array('tu.username');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*, tc.id, tu.username, CONCAT('".S3_BUCKET_ROOT.CONTEST_IMAGE."','',tc.video) as video");
        $this->db->from('tbl_user_contest tc');
        $this->db->join('tbl_user tu','tc.user_id=tu.id','id');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tc.contest_id',$contest_id);
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables_contest($contest_id){
        $this->_get_datatables_query_contest($contest_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered_contest($contest_id){
        $this->_get_datatables_query_contest($contest_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_user_list($user_id = 0,$contest_id,$is_block_contest = 0){
        $this->db->select("u.*,od.order_id,od.id AS order_detail_id");
        $this->db->from('tbl_order o');
        $this->db->join('tbl_order_detail od','o.id = od.order_id','left');
        $this->db->join('tbl_user u','o.user_id = u.id','left');
        $this->db->where('o.is_deleted','0');
        $this->db->where('od.is_deleted','0');
        $this->db->where('od.item_id',$contest_id);
        $this->db->where('od.item_type','contest');
        $this->db->where('u.is_deleted','0');
        if($user_id != 0){
            $this->db->where("o.user_id",$user_id);
        }
        if($is_block_contest != 0){
            $this->db->where("od.is_block_contest",$is_block_contest);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return false;
        }
    }
}
?>