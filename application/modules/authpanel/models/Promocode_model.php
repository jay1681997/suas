<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Promocode_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /*
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tp.promocode', 'tp.discount', 'tp.discount_type', 'tp.per_user_usage', 'tp.maxusage', 'tp.insert_datetime','tp.start_date','tp.end_date');
        $this->order = array('tp.id' => 'DESC');
        $this->db->select("tp.*, tp.id, CONCAT('".S3_BUCKET_ROOT.PROMOCODE_IMAGE."','',tp.promocode_image) as promo_image");
        $this->db->from('tbl_promocode tp');
        $this->db->where('tp.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tp.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
    ** Function to update promocode
    */
    function update_promocode($id,$params)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_promocode',$params);
    }

    /*
    ** Function for the get promocode details
    */
    function GetPromocodeDetails($promo_id)
    {
        $this->db->select("*")->from('tbl_promocode');
        $this->db->where('is_deleted','0');
        $this->db->where('id', $promo_id);    
        $this->db->group_by('id');
        $promodetails=$this->db->get()->row_array();

        
        return $promodetails;
    }

}
?>