<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Content_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function get_app_content()
    {
    	return $this->db->get('tbl_app_content')->row_array();
    }

    function save_app_content($content)
    {
    	$data=$this->get_app_content();
    	if(!empty($data))
    	{
            $content['update_datetime']=date('Y-m-d H:i:s');
    		$this->db->where('id',$data['id'])->update('tbl_app_content',$content);
    	}
    	else
    	{
            $content['insert_datetime']=date('Y-m-d H:i:s');
    		$this->db->insert('tbl_app_content',$content);
    	}
    }
}