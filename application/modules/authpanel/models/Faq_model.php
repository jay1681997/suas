<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Faq_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /*
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.question','tc.answer','tc.is_deleted','tc.inserted_date');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*");
        $this->db->from('tbl_faq tc');
        $this->db->where('tc.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /**
     * Function to execute datatable query
     */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /**
     * Function to get count records
     */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*
    ** Function for export the faq data
    */
    function export()
    {
        $this->column_search = array('tc.question','tc.answer','tc.status','tc.is_deleted','tc.inserted_date');
        $this->order = array('l.id' => 'DESC'); // default order 
        $this->db->select("u.*");
        $this->db->from('tbl_faq u');
        $i = 0;
        if(isset($this->session->userdata['session_aray']['search'])) // if datatable send POST for search
        {
            foreach ($this->column_search as $item) // loop column 
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $this->session->userdata['session_aray']['search']);
                }
                else
                {
                    $this->db->or_like($item, $this->session->userdata['session_aray']['search']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
                
                $i++;
            }
        }
        $query = $this->db->get();      

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
}
?>