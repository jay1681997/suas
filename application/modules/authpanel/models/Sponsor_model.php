<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Sponsor_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('ts.sponsor_name','ts.sponsor_link','ts.bio','ts.insert_datetime');
        $this->order = array('ts.id' => 'DESC');
        // $this->db->select("ts.*, ts.id, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo, tc.name as contest_name");
         $this->db->select("ts.*, ts.id, CONCAT('".S3_BUCKET_ROOT.SPONSOR_LOGO."','',ts.sponsor_logo) as sponsor_logo");

        $this->db->from('tbl_sponsor ts');
        // $this->db->join('tbl_master_contest tc', 'ts.contest_id=tc.id','left');
        $this->db->where('ts.is_deleted','0');

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $prefix = 'ts.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'contest_name') {
                $prefix = 'tc.';
                $key = 'name';
            }
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    


}
?>