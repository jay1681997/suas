<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Customer_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.username','tc.email','tc.country_code','tc.phone','tc.otp_code', 'tc.otp_status', 'tc.insert_datetime','tc.login_status','tc.age','tc.dob');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*, tc.id, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tc.profile_image) as profile_image");
        $this->db->from('tbl_user tc');
        $this->db->where('tc.is_deleted','0');

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    private function _get_datatables_query_deleted()
    {
        $this->column_search = array('tc.username','tc.email','tc.country_code','tc.phone','tc.otp_code', 'tc.otp_status', 'tc.insert_datetime','tc.login_status','tc.age','tc.dob');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*, tc.id, CONCAT('".S3_BUCKET_ROOT.USER_IMAGE."','',tc.profile_image) as profile_image");
        $this->db->from('tbl_user tc');
        $this->db->where('tc.is_deleted','1');

        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
     function get_datatables_deleted(){
        $this->_get_datatables_query_deleted();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_filtered_deleted(){
        $this->_get_datatables_query_deleted();
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*
    ** Function for export the customer data
    */
    function export()
    {
        $this->column_search = array('name','email','country_code','phone', 'inserted_date');
        $this->order = array('l.id' => 'DESC');
        $this->db->select("u.*");
        $this->db->from('tbl_user u');
        // $this->db->where('u.is_deleted','0');
        $i = 0;
        if(isset($this->session->userdata['session_aray']['search']))
        {
            foreach ($this->column_search as $item)
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->session->userdata['session_aray']['search']);
                }
                else
                {
                    $this->db->or_like($item, $this->session->userdata['session_aray']['search']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
                
                $i++;
            }
        }
        $query = $this->db->get();      

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function get_studentcount($user_id){
        $this->db->select("COUNT(ts.id) as subprofile_count");
        $this->db->from('tbl_student ts');
        $this->db->where('ts.is_deleted','0');
        $this->db->where('ts.user_id',$user_id);
        $qry =  $this->db->get();

        if($qry->num_rows() > 0){
            return $qry->row();
        }
        else{
            return false;
        }

    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query_student($user_id)
    {
        $this->column_search = array('tc.username','tc.insert_datetime','tc.login_status','tc.age','tc.dob');
        $this->order = array('tc.id' => 'DESC');
        $this->db->select("tc.*, tc.id, CONCAT('".S3_BUCKET_ROOT.STUDENT_IMAGE."','',tc.profile_image) as profile_image, CONCAT('".S3_BUCKET_ROOT.STUDENT_IMAGE."','',tc.profile_video) as profile_video");
        $this->db->from('tbl_student tc');
        $this->db->where('tc.is_deleted','0');
        $this->db->where('tc.user_id',$user_id);
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables_student($user_id){
        $this->_get_datatables_query_student($user_id);
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered_student($user_id){
        $this->_get_datatables_query_student($user_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

}
?>