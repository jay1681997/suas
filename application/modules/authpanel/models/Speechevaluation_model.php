<?php

/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Speechevaluation_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    ** Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('ts.title','ts.description','ts.insert_datetime','tu.username','tc.username','su.uploading_type','ms.name');
        $this->order = array('ts.id' => 'DESC');
         $this->db->select("ts.*,ms.name AS subscription_name,su.uploading_type,(SELECT is_evaluated FROM tbl_user_speech where id = ts.speech_id) AS is_evaluated, ts.id, tu.username, tc.username as student_name, IF(ts.video = '', '', CONCAT('" .S3_BUCKET_ROOT.SPEECH_IMAGE. "','', ts.video)) as video");
        $this->db->from('tbl_user_evaluation ts');
        $this->db->join('tbl_user_speech su','ts.speech_id = su.id','left');
        $this->db->join('tbl_master_subscription ms','su.subscription_id = ms.id','left');
        $this->db->join('tbl_user tu', 'ts.user_id=tu.id','left');
        $this->db->join('tbl_student tc', 'ts.student_id=tc.id','left');
        $this->db->where('ts.is_deleted','0');
        $this->db->group_by('ts.id');
        $this->db->order_by('ts.id','desc');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $prefix = 'ts.';
            $key = $_REQUEST['sort'];
            if ($_REQUEST['sort'] == 'contest_name') {
                $prefix = 'tc.';
                $key = 'name';
            }elseif ($_REQUEST['sort'] == 'username') {
                 $prefix = 'tu.';
                $key = 'username';
            }elseif ($_REQUEST['sort'] == 'student_name') {
                 $prefix = 'tc.';
                $key = 'username';
            }elseif ($_REQUEST['sort'] == 'uploading_type') {
                 $prefix = 'su.';
                $key = 'uploading_type';
            }
            $this->db->order_by($prefix.$key, $_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /*
    ** Function to execute datatable query
    */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
                 // echo "<pre>"; print_r($this->db->last_query()); die;
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /*
    ** Function to get count records
    */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    


}
?>