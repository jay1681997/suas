<?php

/*
** Project      : EzPark
** Date         : 12-12-2019
** Modified On  : -  
*/
class Country_model extends CI_Model {

    function __construct(){
        parent::__construct();        
    }

    /**
    * Datatable query
    */
    private function _get_datatables_query()
    {
        $this->column_search = array('tc.region','tc.tax','c.name');
        $this->order = array('tc.id' => 'ASC');

        $this->db->select("tc.*,c.name");
        $this->db->from('tbl_tax_details tc');
        $this->db->join("tbl_country c","c.id = tc.country_id");
        $this->db->where('tc.is_deleted','0');
        $i = 0;
        foreach ($this->column_search as $item){
            if(isset($_REQUEST['search'])){
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_REQUEST['search']);
                }
                else{
                    $this->db->or_like($item, $_REQUEST['search']);
                }
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        if(isset($_REQUEST['order'])){
            $this->db->order_by('tc.'.$_REQUEST['sort'],$_REQUEST['order']);
        } else {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    /**
     * Function to execute datatable query
     */
    function get_datatables(){
        $this->_get_datatables_query();
        if($_REQUEST['limit'] != -1)
        // if(!empty($this->session->userdata('offsetl'))){
        //     $_REQUEST['offset'] = $this->session->userdata('offsetl');
        //     $this->session->unset_userdata('offsetl');
        // }
        // if(!empty($this->session->userdata('limitl'))){
        //     $_REQUEST['limit'] = $this->session->userdata('limitl');
        //     $this->session->unset_userdata('limitl');
        // }
        $this->db->limit($_REQUEST['limit'], $_REQUEST['offset']);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return false;
        }
    }
 
    /**
     * Function to get count records
     */
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

   
}
?>