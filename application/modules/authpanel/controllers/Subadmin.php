<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAs
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Subadmin extends MY_Controller {

    private $view_folder = 'authpanel/subadmin/';
    function __construct(){
        parent::__construct();
        //load subadmin model
        $this->load->model('authpanel/subadmin_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load subadmin data
    */
    public function ajax_list(){
        $list = $this->subadmin_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = (!empty($row_data->profile_image)) ? '<a href="'.$row_data->profile_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->profile_image.' alt="No image"></a>' : '-';
                $row['profile_image']        = $image;
                $row['name']                 = $row_data->name;
                $row['email']                = $row_data->email;
                $row['phone']                = $row_data->country_code.' '.$row_data->phone;
              
                
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-subadminid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

               
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/subadmin/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->subadmin_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the subadmin details
    */
    public function add()
    {  
        $data['countries']      = $this->common_model->get_country_code();
        $data['module'] = $this->common_model->common_multipleSelect('tbl_subadmin_modules', array('is_deleted'=>'0','status'=>'Active'));
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('email','Email','required|trim');
            $this->form_validation->set_rules('country_code','Country Code','required|trim');
            $this->form_validation->set_rules('phone','Phone','required|trim');
            
            $this->form_validation->set_error_delimiters('<div subadmin="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

                $profile_image = "default.png";
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],ADMIN_IMAGE);
                    if(!$profile_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $subadmindata = array(
                    'name'          => $this->input->post('name'),
                    'profile_image' => $profile_image,
                    'email'         => $this->input->post('email'), 
                    'country_code'  => $this->input->post('country_code'),
                    'phone'         => $this->input->post('phone'),
                    'role'          => 'Sub',
                    'password'      => $this->common_model->encrypt_password($this->input->post('password')),
                );

                $subadmin_id=$this->common_model->common_insert('tbl_admin', $subadmindata);

                $module_ids = $this->input->post('module');
              
                foreach ($module_ids as $mod) {
                    $this->common_model->common_insert('tbl_subadmin_access_modules', array('subadmin_id'=>$subadmin_id, 'module_id'=>$mod));
                }
                $subadmindata['password'] = $this->input->post('password');
                $message = $this->load->view('template/welcome_page_sub_admin',$subadmindata,TRUE);
                $send_email = $this->common_model->send_mail($this->input->post('email'), ADMIN_EMAIL,  $message,'Account has been Created Successfully');
                // echo "SSS";
                // print_r($send_mail);
                // print_r($message);
                // die;
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_subadmin_add_success'));
                redirect('authpanel/subadmin');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the subadmin details
    */
    public function edit($subadmin_id)
    {
        $subadmin_id     = base64_decode($subadmin_id);
        $data['countries']  = $this->common_model->get_country_code();
        $data['module']     = $this->common_model->common_multipleSelect('tbl_subadmin_modules', array('is_deleted'=>'0','status'=>'Active'));
        $data['assign_module']  = $this->common_model->get_selected_module($subadmin_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_admin',array('id'=>$subadmin_id));
        if($this->input->post() && !empty($data['result']))
        {
            // print_r($this->input->post()); die;
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('email','Email','required|trim');
            $this->form_validation->set_rules('country_code','Country Code','required|trim');
            $this->form_validation->set_rules('phone','Phone','required|trim');
            $this->form_validation->set_error_delimiters('<div subadmin="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                
                $profile_image = @$data['result']['profile_image'];
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],ADMIN_IMAGE);
                    if(!$profile_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $subadmindata = array(
                    'name'          => $this->input->post('name'),
                    'profile_image' => $profile_image,
                    'email'         => $this->input->post('email'), 
                    'country_code'  => $this->input->post('country_code'),
                    'phone'         => $this->input->post('phone'),
                    'password'       => !empty($this->input->post('password')) ? $this->common_model->encrypt_password($this->input->post('password')) : $data['result']['password'],
                );
                
                $this->common_model->common_singleUpdate('tbl_admin',$subadmindata,array('id'=>$subadmin_id));

                $AllPostData = $this->input->post('module');
                if(!empty($AllPostData)) {
                    $this->common_model->common_delete("tbl_subadmin_access_modules",array("subadmin_id"=>$subadmin_id));
                    $permission = array();
                    foreach ($AllPostData as $key => $value) {
                        $param_section = array("subadmin_id"=>$subadmin_id,"module_id"=>$value); 
                        $permission[] = $param_section;
                    }
                    $this->db->insert_batch("tbl_subadmin_access_modules",$permission);
                }else{
                    $this->common_model->common_delete("tbl_subadmin_access_modules",array("subadmin_id"=>$subadmin_id));
                }
                
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_subadmin_update_success'));
                redirect('authpanel/subadmin');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the subadmin view page
    */
    public function view($subadmin_id){
        $subadmin_id          = base64_decode($subadmin_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_admin',array('id'=>$subadmin_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of subadmin
    */
    public function changestatus($subadmin_id, $status){

        $this->common_model->common_singleUpdate('tbl_admin',array('status'=>$status),array('id'=>$subadmin_id));
        echo $this->lang->line('adminpanel_message_subadmin_'.$status.'_success');die;
    }

    /*
    ** Function for remove subadmin
    */
    public function removesubadmin($subadmin_id)
    {
        $this->common_model->common_singleUpdate('tbl_admin',array('is_deleted'=>'1'),array('id'=>$subadmin_id));
        echo $this->lang->line('adminpanel_message_subadmin_delete_success');die;
    }


}
