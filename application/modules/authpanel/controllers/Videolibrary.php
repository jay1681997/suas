<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Jan-2022
** Modified On  : -  
*/
require 'vendor/autoload.php';
class Videolibrary extends MY_Controller
{

    private $view_folder = 'authpanel/videolibrary/';
    function __construct()
    {
        parent::__construct();
        //load videolibrary model
        $this->load->model('authpanel/videolibrary_model');
    }

    /*
    **Load index page
    */
    public function index()
    {
        $this->load->view($this->view_folder . 'listing');
    }

    /*
    ** Function for load videolibrary data
    */
    public function ajax_list()
    {

        $list = $this->videolibrary_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $row['id']  = $row_data->id;
                $image = (!empty($row_data->image)) ? '<a href="' . $row_data->image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->image . ' alt="No image"></a>' : '-';

                $row['image'] = $image;

                $video = (!empty($row_data->video)) ? '<video width="150px" controls > <source src="' . $row_data->video . '" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']               = $video;
                $row['name']                = $row_data->name;
                $row['description']         = $row_data->description;
                $row['price']               = $row_data->currency . '' . $row_data->price;
                $row['total_point']         = $row_data->total_point;
                $row['category']            = $row_data->category;

                //Check status
                if ($row_data->status == 'Active') {
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status = '<div class="switch"><label><input type="checkbox" name="changestatus" data-videolibraryid="' . $row_data->id . '" data-status="' . $row_data->status . '" ' . $checkstete . '><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                $action = '<a href="' . site_url() . 'authpanel/videolibrary/view/' . base64_encode($row_data->id) . '"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action .= ' <a href="' . site_url() . 'authpanel/videolibrary/edit/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove(' . $row_data->id . ');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;

                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->videolibrary_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the post details
    */
    public function add()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('category', 'Category', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');

            if ($this->form_validation->run()) {
                $videoname = "";
                $imgname   = "";
                $sponsor_logo = "";
                if (!empty($_FILES['sponsor_logo']) && $_FILES['sponsor_logo']['size'] > 0) {
                    $sponsor_logo = $this->common_model->uploadImageS3($_FILES['sponsor_logo'], VIDEOLIBRARY_IMAGE);
                    if (!$sponsor_logo) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'edit', $data);
                        return false;
                    }
                }
                if (!empty($_FILES['thumb_image']) && $_FILES['thumb_image']['size'] > 0) {
                    $thumb_image = $this->common_model->uploadImageS3($_FILES['thumb_image'], VIDEOLIBRARY_IMAGE);
                    // console.log($thumb_image);
                    // die;
                    if (!$thumb_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'edit', $data);
                        return false;
                    }
                }
                if (!empty($_FILES['media']) && $_FILES['media']['size'] > 0) {
                    $uploaded_filename = $this->common_model->uploadImageS3($_FILES['media'], VIDEOLIBRARY_IMAGE);
                    if (!$uploaded_filename) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'add', $data);
                        return false;
                    }

                    if (!empty($uploaded_filename) && $uploaded_filename != "") {
                        //FOR video and its IMAGE
                        // if (!empty($this->input->post('thumb_image') )) {
                        //     $data1 = $_POST['thumb_image'];
                        //     //list($type, $data) = explode(';', $data);
                        //     //list(, $data)      = explode(',', $data);
                        //     $data1 = str_replace('data:image/png;base64,', '', $data1);
                        //     $data1 = str_replace(' ', '+', $data1);
                        //     $data1 = base64_decode($data1);
                        //     $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                        //     file_put_contents(THUMB_DIR . $thumb_media_name, $data1);
                        //     $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, VIDEOLIBRARY_IMAGE);
                        //     $imgname = $uploaded_imagename;
                        // }
                        $videoname = $uploaded_filename;
                    } else {
                        $this->load->view($this->view_folder . 'add', $data);
                    }
                }

                $video_duration = '';


                $remotefilename = S3_BUCKET_ROOT . VIDEOLIBRARY_IMAGE . $videoname;
                if ($fp_remote = fopen($remotefilename, 'rb')) {
                    $localtempfilename = tempnam('/tmp', 'getID3');
                    if ($fp_local = fopen($localtempfilename, 'wb')) {
                        while ($buffer = fread($fp_remote, 8192)) {
                            fwrite($fp_local, $buffer);
                        }
                        fclose($fp_local);
                        // Initialize getID3 engine
                        $getID3 = new getID3;
                        $ThisFileInfo = $getID3->analyze($localtempfilename);
                        $video_duration = $ThisFileInfo['playtime_string'];
                        // Delete temporary file
                        unlink($localtempfilename);
                    }
                    fclose($fp_remote);
                }
                //  if(!empty($sponsor_logo)){
                //         	$imgname = $sponsor_logo;
                //         }
                if (!empty($sponsor_logo)) {
      
                    $sponsor_logo = $sponsor_logo;
                } else {
                
                    $sponsor_logo = $imgname;
                }
                // if (!empty($imgname)) {
                //     $thumb_image = $imgname;
                // } else {
                //     $thumb_image = $imgname;
                // }
                $postdata = array(
                    'name'          => $this->input->post('name'),
                    'category'      => $this->input->post('category'),
                    'price'         => $this->input->post('price'),
                    'total_point'   => $this->input->post('total_point'),
                    'description'   => $this->input->post('description'),
                    'image'         => $sponsor_logo,
                    'thumb_image' => $thumb_image,
                    'video'         => $videoname,
                    'video_duration' => $video_duration,
                );
                // print_r($postdata);die;
                $videolibrary_id = $this->common_model->common_insert('tbl_master_videolibrary', $postdata);

                $users = $this->common_model->getUserList();

                foreach ($users as $key => $value) {
                    $notification_data = array(
                        'sender_id'        => '1',
                        'sender_type'      => 'admin',
                        'receiver_id'      => $value['id'],
                        'receiver_type'    => 'user',
                        'primary_id'       => $videolibrary_id,
                        'notification_tag' => "video_added",
                        'message'          => 'A new video has been added on "'. $this->input->post('name').'"', //"New video added by admin",
                        'title'            => 'New Video Added'
                    );
                    // print_r($notification_data);
                    $this->common_model->send_notification($notification_data);
                }


                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_videolibrary_add_success'));
                redirect('authpanel/videolibrary');
            } else {
                $this->load->view($this->view_folder . 'add', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'add', $data);
        }
    }

    /*
    ** Function for edit the post details
    */
    public function edit($videolibrary_id)
    {
        $videolibrary_id        = base64_decode($videolibrary_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_master_videolibrary', array('id' => $videolibrary_id));
        if ($this->input->post() && !empty($data['result'])) {
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('category', 'Category', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');
            if ($this->form_validation->run()) {

                $videoname = $data['result']['video'];
                $imgname = $data['result']['image'];
                $media_type = $data['result']['media_type'];
                $sponsor_logo = "";
                $sponsor_logo_a = $_FILES['sponsor_logo']['size'];
                if (!empty($_FILES['sponsor_logo']) && $_FILES['sponsor_logo']['size'] > 0) {
                    $sponsor_logo = $this->common_model->uploadImageS3($_FILES['sponsor_logo'], VIDEOLIBRARY_IMAGE);

                    if (!$sponsor_logo) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'edit', $data);
                        return false;
                    }
                }
                if (!empty($_FILES['thumb_image']) && $_FILES['thumb_image']['size'] > 0) {
                    $thumb_image = $this->common_model->uploadImageS3($_FILES['thumb_image'], VIDEOLIBRARY_IMAGE);
                    // console.log($thumb_image);
                    // die;
                    if (!$thumb_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'edit', $data);
                        return false;
                    }
                }

                if (!empty($_FILES['media']) && $_FILES['media']['size'] > 0) {
                    $uploaded_filename = $this->common_model->uploadImageS3($_FILES['media'], VIDEOLIBRARY_IMAGE);

                    if (!empty($uploaded_filename) && $uploaded_filename != "") {
                        $ext = explode(".", $uploaded_filename);
                        $img_ext = array("jpg", "jpeg", "png", "JPG", "JPEG", "PNG", "gif", "GIF");

                        if (in_array($ext[1], $img_ext)) {
                            //FOR IMAGE 
                            $media_type = 'image';
                            $imgname = $uploaded_filename;
                        } else {

                            //FOR video and its IMAGE
                            // if (!empty($this->input->post('thumb_image'))) {
                            //     $data1 = $_POST['thumb_image'];
                            //     //list($type, $data) = explode(';', $data);
                            //     //list(, $data)      = explode(',', $data);
                            //     $data1 = str_replace('data:image/png;base64,', '', $data1);
                            //     $data1 = str_replace(' ', '+', $data1);
                            //     $data1 = base64_decode($data1);
                            //     $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                            //     file_put_contents(THUMB_DIR . $thumb_media_name, $data1);
                            //     $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, VIDEOLIBRARY_IMAGE);
                            //     $imgname = $uploaded_imagename;
                            // }
                            $videoname = $uploaded_filename;
                            $media_type = 'video';
                        }
                    } else {
                        $this->load->view($this->view_folder . 'edit', $data1);
                    }
                }

                $video_duration = '';


                $remotefilename = S3_BUCKET_ROOT . VIDEOLIBRARY_IMAGE . $videoname;
                if ($fp_remote = fopen($remotefilename, 'rb')) {
                    $localtempfilename = tempnam('/tmp', 'getID3');
                    if ($fp_local = fopen($localtempfilename, 'wb')) {
                        while ($buffer = fread($fp_remote, 8192)) {
                            fwrite($fp_local, $buffer);
                        }
                        fclose($fp_local);
                        // Initialize getID3 engine
                        $getID3 = new getID3;
                        $ThisFileInfo = $getID3->analyze($localtempfilename);
                        $video_duration = $ThisFileInfo['playtime_string'];
                        // Delete temporary file
                        unlink($localtempfilename);
                    }
                    fclose($fp_remote);
                }

                // if(!empty($sponsor_logo)){
                // 	$imgname = $sponsor_logo;
                // }else{
                // 	$imgname = $data['result']['image'];
                // }
                if (!empty($sponsor_logo)) {
                    echo "if";
                    $sponsor_logo = $sponsor_logo;
                } else {
                    echo "else";
                    print_r($data['result']['image']);
                    $sponsor_logo = $data['result']['image'];
                }
                // if (!empty($imgname)) {
                //     $thumb_image = $imgname;
                // } else {
                //     $thumb_image = $data['result']['thumb_image'];
                // }

                $postdata = array(
                    'name'          => $this->input->post('name'),
                    'category'      => $this->input->post('category'),
                    'price'         => $this->input->post('price'),
                    'total_point'   => $this->input->post('total_point'),
                    'description'   => $this->input->post('description'),
                    'image'         => $sponsor_logo,
                    'thumb_image' => $thumb_image,
                    'video'         => $videoname,
                    'video_duration' => $video_duration,
                );
                // echo "<pre>";
                // print_r($data);
                // print_r($sponsor_logo_a);
                // // // print_r($_FILES['sponsor_logo']);
                // // print_r($this->input->post());
                // // print_r($sponsor_logo);
                // // print_r($imgname);
                // print_r($postdata);
                // die;
                $this->common_model->common_singleUpdate('tbl_master_videolibrary', $postdata, array('id' => $videolibrary_id));
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_videolibrary_update_success'));
                redirect('authpanel/videolibrary');
            } else {
                $this->load->view($this->view_folder . 'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'edit', $data);
        }
    }

    /*
    ** Function for load the video library view page
    */
    public function view($videolibrary_id)
    {
        $videolibrary_id    = base64_decode($videolibrary_id);
        $data['result']     = $this->videolibrary_model->view_videolibrary($videolibrary_id);
        $this->load->view($this->view_folder . 'view', $data);
    }

    /*
    ** Function for update the status of video library
    */
    public function changestatus($videolibrary_id, $status)
    {
        $this->common_model->common_singleUpdate('tbl_master_videolibrary', array('status' => $status), array('id' => $videolibrary_id));
        echo $this->lang->line('adminpanel_message_videolibrary_' . $status . '_success');
        die;
    }

    /*
    ** Function for remove video library
    */
    public function removevideolibrary($videolibrary_id)
    {
        $this->common_model->common_singleUpdate('tbl_master_videolibrary', array('is_deleted' => '1'), array('id' => $videolibrary_id));
        echo $this->lang->line('adminpanel_message_videolibrary_delete_success');
        die;
    }
}
