<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAs
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Report extends MY_Controller {

    private $view_folder = 'authpanel/report/';
    function __construct(){
        parent::__construct();
        //load report model
        $this->load->model('authpanel/report_model');
    }

    /*
    *Load index page
    */
	public function index(){
        $data['totalamount'] = $this->report_model->get_totalSumOfOrders();
         // echo "<pre>"; print_r($this->db->last_query()); die;
		$this->load->view($this->view_folder.'listing', $data);
	}

      public function user_list()
    {
        $this->load->view($this->view_folder.'user_list');
    }
    public function user_video_uploaded()
    {
        $this->load->view($this->view_folder.'user_video_uploaded');
    }
      public function user_contest_list()
    {
        $this->load->view($this->view_folder.'user_contest_list');
    }
      public function user_online_classes_list()
    {
        $this->load->view($this->view_folder.'user_online_classes_list');
    }
     public function user_promocode()
    {
        $this->load->view($this->view_folder.'user_promocode');
    }
    public function video_list($user_id)
    {
        $data['user_id'] = base64_decode($user_id);
        $this->load->view($this->view_folder . 'video_list', $data);
    }
    public function contest_list($user_id='')
    {
        $data['user_id'] = base64_decode($user_id);
        $this->load->view($this->view_folder . 'contest_list', $data);
    }
    public function classes_list($user_id='')
    {
        $data['user_id'] = base64_decode($user_id);
        $this->load->view($this->view_folder . 'class_list', $data);
    }
     public function promocode_list($user_id='')
    {
        $data['user_id'] = base64_decode($user_id);
        $this->load->view($this->view_folder . 'promocode_list', $data);
    }
    public function promocode_use_list($user_id='')
    {
        $data['user_id'] = base64_decode($user_id);
        $this->load->view($this->view_folder . 'promocode_use_list', $data);    
    }
    public function ajax_user_list($type = 'user'){
        // echo $type;die;
        $list = $this->report_model->get_user_data();
         $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $image      = '<a href="'.$row_data->profile_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->profile_image.'  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $video = (!empty($row_data->profile_video)) ? '<video width="150px" controls > <source src="'.S3_BUCKET_ROOT.USER_IMAGE. $row_data->profile_video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']         =  $video;
                $row['username']      =  $row_data->firstname.' '.$row_data->lastname; 
                $row['email']         =  $row_data->email;
                $row['country_code']  =  $row_data->country_code;
                $row['phone']         =  $row_data->country_code.' '.$row_data->phone;
                $row['country']       =  $row_data->country;
                if($type == 'upload_video'){
                    $row['total_video'] = '<a href="' . site_url() . 'authpanel/report/video_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">'.$row_data->total_video.'</a>';
                }else if($type == 'contest'){
                    $row['total_contest'] = '<a href="' . site_url() . 'authpanel/report/contest_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">'.$row_data->total_contest.'</a>';
                }elseif ($type == 'class') {
                     $row['total_classes'] = '<a href="' . site_url() . 'authpanel/report/classes_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">'.$row_data->total_classes.'</a>';
                }elseif($type == 'promocode'){
                     $row['total_promocode'] = '<a href="' . site_url() . 'authpanel/report/promocode_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">'.$row_data->total_promocode.'</a>';
                     $row['total_promocode_use'] = '<a href="' . site_url() . 'authpanel/report/promocode_use_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">'.$row_data->total_promocode_use.'</a>';
                }
                $date1=date_create($row_data->insert_datetime);
                $date2=date_create($row_data->last_login);
                $diff=date_diff($date1,$date2);
                $row['total_dates'] = $diff->format("Year: %y, Month: %m, days: %d.");
                if($row_data->login_status == 'Online')
                    $row['login_status'] = '<span class="text-success">'.$row_data->login_status.'</span>';
                else 
                    $row['login_status'] = '<span class="text-danger">'.$row_data->login_status.'</span>';

                $row['last_login'] = $this->common_model->date_convert($row_data->last_login,ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                 $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->report_model->count_user_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_video_list($user_id)
    {   
        $user_id = base64_decode($user_id);
        $list = $this->report_model->get_video_data($user_id);
         $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                  $video = (!empty($row_data->video)) ? '<video width="150px" controls > <source src="'. $row_data->video_url.'" > '.$row_data->video_url.' </video>' : '-';
                $row['video']               = $video;
                $row['title'] = $row_data->title;
                $row['description'] = $row_data->description;
                $row['uploading_type'] = $row_data->uploading_type;
                $row['is_evaluated'] = ucfirst($row_data->is_evaluated);
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                 $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->report_model->count_video_filtered($user_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);   
    }

    public function ajax_contest_list($user_id='')
    {
         $user_id = base64_decode($user_id);
         $list = $this->report_model->get_contest_data($user_id);
         $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $row['id']                   = $row_data->id;

                $file_ext = explode('.', $row_data->contest_image);
                // print_r($file_ext); die;                                   
                if ($row_data->contest_type != 'live') {
                    $image                       = (!empty($row_data->contest_image)) ? '<video width="150px" controls > <source src="' . $row_data->contest_image . '" > Your browser does not support HTML5 video. </video>' : '-';
                } else {
                    $image                       = '<a href="' . $row_data->contest_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->contest_image . '  alt="No image"></a>';
                }
                $row['contest_image']        = $image;
                $row['name']                 = $row_data->name;
                $row['description']          = $row_data->description;
                $contest_startdate = $this->common_model->date_convert($row_data->contest_startdate, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $contest_enddate = $this->common_model->date_convert($row_data->contest_enddate, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $row['contest_startdate']    = $contest_startdate;
                $row['contest_enddate']      = $contest_enddate;
                $row['total_participant']    = $row_data->total_participant;
                $row['total_point']          = $row_data->total_point;
                $row['price']                = $row_data->currency . '' . $row_data->price;
                $row['winner_date']          = date('d M Y', strtotime($row_data->winner_date));
                $row['age_category']         = $row_data->age_category;
                $row['contest_by']           = $row_data->contest_by;
                if ($row_data->contest_type == 'live')
                    $row['contest_type'] = '<span class="text-success">Live</span>';
                else
                    $row['contest_type'] = '<span class="text-primary">Video</span>';

                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                 $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->report_model->count_contest_filtered($user_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output); 
    }

    public function ajax_class_list($user_id = ''){
        $user_id = base64_decode($user_id);
        $list = $this->report_model->get_class_data($user_id);
          $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                 $row['id']                   = $row_data->id;
                $image                       = '<a href="' . $row_data->class_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->class_image . '  alt="No image"></a>';
                $row['class_image']          = $image;
                $row['program_title']        = $row_data->program_title;
                $row['description']          = $row_data->description;
                $start_datetime = $this->common_model->date_convert($row_data->start_datetime, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $end_datetime = $this->common_model->date_convert($row_data->end_datetime, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $row['start_datetime']       = $start_datetime;
                $row['end_datetime']         = $end_datetime;

                $row['grade']                = $row_data->grade;
                $row['total_point']          = $row_data->total_point;
                $row['price']                = $row_data->currency . '' . $row_data->price;
                $row['age_category']         = $row_data->age_category;
                $row['total_spot']           = $row_data->total_spot;
                $row['subadmin_name']        = $row_data->subadmin_name;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->report_model->count_class_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_promocode_list($user_id)
    {   
        $user_id = base64_decode($user_id);
        $list = $this->report_model->get_promocode_data($user_id);
         $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $promo_image             = '<a href="'.$row_data->promo_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->promo_image.'  alt="No image"></a>';
                $row['promo_image']      = $promo_image;
                $row['promocode']        = $row_data->promocode;
                $row['discount_type']    = $row_data->discount_type;
                $row['start_date']       = date('d M Y', strtotime($row_data->start_date));
                $row['end_date']         = date('d M Y', strtotime($row_data->end_date));
                $row['maxusage']         = $row_data->maxusage;
                $row['per_user_usage']   = $row_data->per_user_usage;
                $row['price']            = $row_data->price;
                if ($row_data->discount_type == 'flat') {
                    $row['discount'] = '$'.$row_data->discount;
                } else {
                    $row['discount'] = $row_data->discount."%";
                }
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                 $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->report_model->count_promocode_filtered($user_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);   
    }

    public function ajax_promocode_use_list($user_id)
    {   
        $user_id = base64_decode($user_id);
        $list = $this->report_model->get_promocode_use_data($user_id);
         $data = array();
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $promo_image             = '<a href="'.$row_data->promo_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->promo_image.'  alt="No image"></a>';
                $row['promo_image']      = $promo_image;
                $row['promocode']        = $row_data->promocode;
                $row['discount_type']    = $row_data->discount_type;
                $row['start_date']       = date('d M Y', strtotime($row_data->start_date));
                $row['end_date']         = date('d M Y', strtotime($row_data->end_date));
                $row['maxusage']         = $row_data->maxusage;
                $row['per_user_usage']   = $row_data->per_user_usage;
                $row['price']            = $row_data->price;
                if ($row_data->discount_type == 'flat') {
                    $row['discount'] = '$'.$row_data->discount;
                } else {
                    $row['discount'] = $row_data->discount."%";
                }
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                 $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->report_model->count_promocode_use_filtered($user_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);   
    }

    /*
    ** Function for load report data
    */
    public function ajax_list(){
        $list = $this->report_model->get_datatables();
        $data = array();
        // echo "<pre>";print_r($list);die;
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                 =  $i++;
                $row['id']                  =  '#'.$row_data->id;
                $row['username']            =  $row_data->username;
                $row['student_name']        =  $row_data->student_name;
                $row['subtotal']            =  $row_data->currency.''.$row_data->subtotal;
                $row['payment_mode']        =  $row_data->payment_mode;
                $row['order_status']        =  $row_data->order_status;
                $row['tax']                 =  $row_data->currency.''.$row_data->tax;
                $row['discount']            =  $row_data->currency.''.$row_data->discount;
                $row['totalamount']         =  $row_data->currency.''.$row_data->totalamount;
             


                if($row_data->item_type == 'class'){
                    $item_type = '<span style="color:blue">Class</span>';
                    $name   = $row_data->class_name;
                    $image  = '<a href="'.$row_data->class_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->class_image.'  alt="No image"></a>';

                }else if($row_data->item_type == 'video'){
                    $item_type = '<span style="color:yellow">Video Library</span>';
                    $name   = $row_data->video_name;
                    $image = (!empty($row_data->video_image)) ? '<video width="150px" controls > <source src="'. $row_data->video_image.'" > Your browser does not support HTML5 video. </video>' : '-';

                }else if($row_data->item_type == 'contest'){
                     $file_ext = explode('.', $row_data->contest_image);
                    $item_type = '<span style="color:red">Contest</span>';
                    $name   = $row_data->contest_name;
                      if($row_data->contest_type == 'video') { 
                      $image = (!empty($row_data->contest_image)) ? '<video width="150px" controls > <source src="'. $row_data->contest_image.'" > Your browser does not support HTML5 video. </video>' : '-';
                    }else{
                        $image  = '<a href="'.$row_data->contest_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->contest_image.'  alt="No image"></a>';
                    }

                }else if($row_data->item_type == 'subscription'){
                    $item_type = '<span style="color:green">Subscription</span>';
                    $name   = $row_data->subscription_name;
                    $image  = '<a href="'.$row_data->subscription_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->subscription_image.'  alt="No image"></a>';

                }else if($row_data->item_type == 'promocode'){
                    $item_type = '<span style="color:purple">Promocode</span>';
                    $name   = $row_data->promocode_name;
                    $image  = '<a href="'.$row_data->promocode_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->promocode_image.'  alt="No image"></a>';

                }else if($row_data->item_type == 'speech'){
                    $item_type = '<span style="color:brown">Speech</span>';
                    $name   = $row_data->speech_name;
                    $image  = '<a href="'.$row_data->speech_thumb_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->speech_thumb_image.'  alt="No image"></a>';

                }
                
                $row['profile_image']        = $image;
                $row['item_type']            = $item_type;
                $row['name']                 = $name;

                //Check login status
                if($row_data->status == 'Pending'){

                    $row['status'] = '<span class="text-primary"><b>'.$row_data->status.'</b></span>';

                }else if($row_data->status == 'Confirm'){

                    $row['status'] = '<span class="text-success"><b>'.$row_data->status.'<b></span>';

                }else{

                    $row['status'] = '<span class="text-danger"><b>'.$row_data->status.'<b></span>';
                }
                 $action = '';
                 if($row_data->is_refund == 0){
                     $action .= '<a href="javascript:void(0);" onclick="refund('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-card-travel"></i></a>';
                 }else{
                    $action .= '<span class="text-success"><b>Refund done<b></span>';
                 }
                
                 $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->report_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function set_categorytype_session($type){
        if(!empty($type) && $type == 'none'){
            $this->session->unset_userdata('category_type');
        }else {
            $this->session->set_userdata('category_type', $type);
        }
        
        echo "success"; die;
    }

    function refund($order_id){
        $order_data = $this->common_model->common_singleSelect("tbl_order",array("id"=>$order_id));
        
        $this->load->library('customstripe');
        $stripe = new ci_customstripe();

        $response = $stripe->directChargeRefund($order_data['transaction_id']);
        // print_r($response);
        if($response['status'] == '1'){
             $this->common_model->common_singleUpdate("tbl_order",array("refund_id"=>$response['refund_id'],"is_refund"=>"1"),array("id"=>$order_id));
                 
                 $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_refund_success'));
                 echo $this->lang->line('adminpanel_message_refund_success');die;
            // redirect('authpanel/report');
        }else{
               
            $this->session->set_flashdata('error_msg', $this->lang->line('adminpanel_message_refund_error'));
                 echo $this->lang->line('adminpanel_message_refund_error');die;
            // redirect('authpanel/report');
        }
       
        die;
    }
}
