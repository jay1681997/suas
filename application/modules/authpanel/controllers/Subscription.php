<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Subscription extends MY_Controller {

    private $view_folder = 'authpanel/subscription/';
    function __construct(){
        parent::__construct();
        //load subscription model
        $this->load->model('authpanel/subscription_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load subscription data
    */
    public function ajax_list(){
        $list = $this->subscription_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']              =  $i++;
                $row['id']               = $row_data->id;
                $row['name']             = $row_data->name;
                $row['description']      = $row_data->description;
                $row['evaluation_type']  = $row_data->evaluation_type;
                $row['price']            = $row_data->currency.''.$row_data->price;
                $row['total_video']      = $row_data->total_video;
                $row['validity']         = $row_data->validity;
                $row['duration']         = $row_data->duration;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-subscriptionid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/subscription/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/subscription/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->subscription_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the subscription details
    */
    public function add()
    {  
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description','Description','required|trim');
            $this->form_validation->set_rules('evaluation_type','Evaluation Type','required|trim');
            $this->form_validation->set_rules('price','Price','required|trim');
            $this->form_validation->set_rules('total_video','Total Video','required|trim');
            $this->form_validation->set_rules('validity','Validity','required|trim');
            $this->form_validation->set_rules('duration','Duration','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {
                  $subscription_image = 'default.png';
                if (!empty($_FILES['subscription_image']) && $_FILES['subscription_image']['size'] > 0) {
                    $subscription_image = $this->common_model->uploadImageS3($_FILES['subscription_image'],SUBSCRIPTION_IMAGE);
                    if(!$subscription_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add');
                        return false;
                    }
                }
                $subscriptiondata = array(
                    'name'              => $this->input->post('name'),
                    'description'       => $this->input->post('description'),                    
                    'evaluation_type'   => $this->input->post('evaluation_type'),
                    'price'             => $this->input->post('price'),
                    'total_video'       => $this->input->post('total_video'),
                    'validity'          => $this->input->post('validity'),
                    'duration'          => $this->input->post('duration'),
                     'image'             => $subscription_image,
                );

                $subscription_id=$this->common_model->common_insert('tbl_master_subscription', $subscriptiondata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_subscription_add_success'));
                redirect('authpanel/subscription');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the subscription details
    */
    public function edit($subscription_id)
    {
        $subscription_id = base64_decode($subscription_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_master_subscription',array('id'=>$subscription_id));

        if($this->input->post() && !empty($data['result']))
        {
           $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description','Description','required|trim');
            $this->form_validation->set_rules('evaluation_type','Evaluation Type','required|trim');
            $this->form_validation->set_rules('price','Price','required|trim');
            $this->form_validation->set_rules('total_video','Total Video','required|trim');
            $this->form_validation->set_rules('validity','Validity','required|trim');
            $this->form_validation->set_rules('duration','Duration','required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                $subscription_image = @$data['result']['image'];
                if (!empty($_FILES['subscription_image']) && $_FILES['subscription_image']['size'] > 0) {
                    $subscription_image = $this->common_model->uploadImageS3($_FILES['subscription_image'],SUBSCRIPTION_IMAGE);
                    if(!$subscription_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }
                $subscriptiondata = array(
                    'name'              => $this->input->post('name'),
                    'description'       => $this->input->post('description'),                    
                    'evaluation_type'   => $this->input->post('evaluation_type'),
                    'price'             => $this->input->post('price'),
                    'total_video'       => $this->input->post('total_video'),
                    'validity'          => $this->input->post('validity'),
                    'duration'          => $this->input->post('duration'),
                    'image'             => $subscription_image,
                );
                
                $this->common_model->common_singleUpdate('tbl_master_subscription',$subscriptiondata,array('id'=>$subscription_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_subscription_update_success'));
                redirect('authpanel/subscription');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the subscription view page
    */
    public function view($subscription_id){
        $subscription_id    = base64_decode($subscription_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_subscription',array('id'=>$subscription_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of subscription
    */
    public function changestatus($subscription_id, $status){

        $this->common_model->common_singleUpdate('tbl_master_subscription',array('status'=>$status),array('id'=>$subscription_id));
        echo $this->lang->line('adminpanel_message_subscription_'.$status.'_success');die;
    }

    /*
    ** Function for remove subscription
    */
    public function removesubscription($subscription_id)
    {
        $this->common_model->common_singleUpdate('tbl_master_subscription',array('is_deleted'=>'1'),array('id'=>$subscription_id));
        echo $this->lang->line('adminpanel_message_subscription_delete_success');die;
    }
    
}
