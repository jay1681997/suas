<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Customer extends MY_Controller {

    private $view_folder = 'authpanel/customer/';
    function __construct(){
        parent::__construct();
        //load customer model
        $this->load->model('authpanel/customer_model');
    }

    /* 
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}
    public function reset_user_list()
    {
        $this->load->view($this->view_folder.'listing_deleted');
    }
    /*
    ** Function for load customer data
    */
    public function ajax_list()
    {   
        $list = $this->customer_model->get_datatables();
        // echo "<pre>"; print_r($this->db->last_query()); die();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $image      = '<a href="'.$row_data->profile_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->profile_image.'  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $video = (!empty($row_data->profile_video)) ? '<video width="150px" controls > <source src="'.S3_BUCKET_ROOT.USER_IMAGE. $row_data->profile_video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']         =  $video;
                $row['username']      =  $row_data->firstname.' '.$row_data->lastname; 
                $row['email']         =  $row_data->email;
                $row['country_code']  =  $row_data->country_code;
                $row['phone']         =  $row_data->country_code.' '.$row_data->phone;
                $row['country']       =  $row_data->country;
                $row['otp_code']      =  $row_data->otp_code;
                $row['otp_status']    =  $row_data->otp_status;
                $row['dob']           =  date('M d Y', strtotime($row_data->dob));
                $row['age']           =  $row_data->age;
                $row['promocode']     =  $row_data->promocode;
                $subprofile_count     =  $this->customer_model->get_studentcount($row_data->id);
             
                $row['subprofile_count'] = $subprofile_count->subprofile_count;

                if($row_data->age_category == 'Adults'){
                    $row['age_category'] = '<span class="text-success">'.$row_data->age_category.'</span>';
                }else if($row_data->age_category == 'Primary'){
                    $row['age_category'] = '<span class="text-primary">'.$row_data->age_category.'</span>';
                }else if($row_data->age_category == 'Juniors'){
                    $row['age_category'] = '<span class="text-warning">'.$row_data->age_category.'</span>';
                }else{
                    $row['age_category'] = '<span class="text-danger">'.$row_data->age_category.'</span>';
                }
                    
                if($row_data->is_video_verify == 'Verify'){
                    $is_verify = '<span class="text-success" style="font-weight:bold">Verify</span>';
                }else if($row_data->is_video_verify == 'Unverify'){
                    $is_verify = '<span class="text-danger" style="font-weight:bold">Unverify</span>';
                }else{
                    $is_verify = '<a href="'.site_url().'authpanel/customer/accepted/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-check"></i></a>';
       
                    $is_verify .= ' <a href="'.site_url().'authpanel/customer/rejected/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-close"></i></a>';
                }

                $row['is_verify'] =$is_verify;

                //Check otp verify or not
                if($row_data->otp_status == 'Verify')
                    $row['otp_status'] = '<span class="text-success" style="font-weight:bold">'.$row_data->otp_status.'</span>';
                else 
                    $row['otp_status'] = '<span class="text-danger" style="font-weight:bold">'.$row_data->otp_status.'</span>';

               
                //Check login status
                if($row_data->login_status == 'Online')
                    $row['login_status'] = '<span class="text-success">'.$row_data->login_status.'</span>';
                else 
                    $row['login_status'] = '<span class="text-danger">'.$row_data->login_status.'</span>';

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-customerid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                $action = '<a href="'.site_url().'authpanel/customer/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action .= ' <a href="'.site_url().'authpanel/customer/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                if($row_data->is_deleted == 0){
                     $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';
                }else{
                     $action .= ' <a href="javascript:void(0);" onclick="addremoveuser('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-time-restore"></i></a>'; 
                }
                               
                $action .= ' <a href="'.site_url().'authpanel/customer/student/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-accounts-add"></i></a>';

                $row['action'] = $action;

                $row['last_login'] = $this->common_model->date_convert($row_data->last_login,ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->customer_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

  public function reset_user_ajax_list()
    {   
        $list = $this->customer_model->get_datatables_deleted();
        // echo "<pre>"; print_r($this->db->last_query()); die();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $image      = '<a href="'.$row_data->profile_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->profile_image.'  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $video = (!empty($row_data->profile_video)) ? '<video width="150px" controls > <source src="'.S3_BUCKET_ROOT.USER_IMAGE. $row_data->profile_video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']         =  $video;
                $row['username']      =  $row_data->firstname.' '.$row_data->lastname; 
                $row['email']         =  $row_data->email;
                $row['country_code']  =  $row_data->country_code;
                $row['phone']         =  $row_data->country_code.' '.$row_data->phone;
                $row['country']       =  $row_data->country;
                $row['otp_code']      =  $row_data->otp_code;
                $row['otp_status']    =  $row_data->otp_status;
                $row['dob']           =  date('M d Y', strtotime($row_data->dob));
                $row['age']           =  $row_data->age;
                $row['promocode']     =  $row_data->promocode;
                $subprofile_count     =  $this->customer_model->get_studentcount($row_data->id);
             
                $row['subprofile_count'] = $subprofile_count->subprofile_count;

                if($row_data->age_category == 'Adults'){
                    $row['age_category'] = '<span class="text-success">'.$row_data->age_category.'</span>';
                }else if($row_data->age_category == 'Primary'){
                    $row['age_category'] = '<span class="text-primary">'.$row_data->age_category.'</span>';
                }else if($row_data->age_category == 'Juniors'){
                    $row['age_category'] = '<span class="text-warning">'.$row_data->age_category.'</span>';
                }else{
                    $row['age_category'] = '<span class="text-danger">'.$row_data->age_category.'</span>';
                }
                    
                if($row_data->is_video_verify == 'Verify'){
                    $is_verify = '<span class="text-success" style="font-weight:bold">Verify</span>';
                }else if($row_data->is_video_verify == 'Unverify'){
                    $is_verify = '<span class="text-danger" style="font-weight:bold">Unverify</span>';
                }else{
                    $is_verify = '<a href="'.site_url().'authpanel/customer/accepted/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-check"></i></a>';
       
                    $is_verify .= ' <a href="'.site_url().'authpanel/customer/rejected/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-close"></i></a>';
                }

                $row['is_verify'] =$is_verify;

                //Check otp verify or not
                if($row_data->otp_status == 'Verify')
                    $row['otp_status'] = '<span class="text-success" style="font-weight:bold">'.$row_data->otp_status.'</span>';
                else 
                    $row['otp_status'] = '<span class="text-danger" style="font-weight:bold">'.$row_data->otp_status.'</span>';

               
                //Check login status
                if($row_data->login_status == 'Online')
                    $row['login_status'] = '<span class="text-success">'.$row_data->login_status.'</span>';
                else 
                    $row['login_status'] = '<span class="text-danger">'.$row_data->login_status.'</span>';

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-customerid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // Edit page Link prepared
                // Remove Function Link prepared
              
                     $action = ' <a href="javascript:void(0);" onclick="addremoveuser('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-time-restore"></i></a>'; 
              
                      
                $row['action'] = $action;

                $row['last_login'] = $this->common_model->date_convert($row_data->last_login,ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->customer_model->count_filtered_deleted(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    /*
    ** Function for edit the customer details
    */
    public function edit($user_id)
    {
        $user_id                = base64_decode($user_id);
        $data['countries']      = $this->common_model->get_country_code();
        $data['result'] = $this->common_model->common_singleSelect('tbl_user',array('id'=>$user_id));
         // echo "<pre>"; print_r($data); die;
        if($this->input->post() && !empty($data['result'])){

            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.id != '.$user_id.' AND is_deleted="0" AND email=]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'required|trim');     
            $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[8]|max_length[12]|is_unique[tbl_user.id != '.$user_id.' AND is_deleted="0" AND phone=]|max_length[14]');
            $this->form_validation->set_rules('is_video_verify','Is Video Verify','required');
            $this->form_validation->set_rules('dob', 'Date of birth', 'required|trim'); 
            $this->form_validation->set_error_delimiters('<div class = "error" style="color:#F44336;">','</div>');

            if ($this->form_validation->run()) {
                
                $profile_image = @$data['result']['profile_image']; 
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],USER_IMAGE);
                     //echo $profile_image; die;
                    if(!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $dob = $this->input->post('dob');
                $diff = (date('Y') - date('Y',strtotime($dob)));
                echo $diff;

                $age_category = '';
                if($diff >= '18'){
                    $age_category = 'Adults';
                }else if($diff >= '5' && $diff <= '8'){
                    $age_category = 'Primary';
                }else if($diff >= '9' && $diff <= '12'){
                    $age_category = 'Juniors';
                }else{
                    $age_category = 'Teens';
                }
                

                $customerdata = array(
                    'firstname'          => $this->input->post('firstname'),
                    'lastname'           => $this->input->post('lastname'),
                    'username'           => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'email'              => $this->input->post('email'),
                    'country_code'       => $this->input->post('country_code'),
                    'phone'              => $this->input->post('phone'),
                    'dob'                => date('Y-m-d', strtotime($this->input->post('dob'))),
                    'profile_image'      => $profile_image,
                    'age'                => $diff,
                    'age_category'       => $age_category,
                    'is_video_verify'    => $this->input->post('is_video_verify'),
                    // 'is_dob_edit'        => $this->input->post('is_dob_edit')
                );
                // echo "<pre>"; print_r($customerdata); die;
                $this->common_model->common_singleUpdate('tbl_user', $customerdata,  array('id'=>$user_id));

                if ($this->input->post('status') == 'Inactive') {
                    $userdeviceinfo = array('token'=>"",'device_token'=>"");
                    $this->common_model->save_user_deviceinfo($user_id,"U",$userdeviceinfo);
                }
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_customer_update_success'));
                redirect('authpanel/customer');   
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }



    /*
    ** Function for add the customer details
    */
    public function add()
    {
        // $user_id                = base64_decode($user_id);
        $data['countries']      = $this->common_model->get_country_code();
        // $data['result'] = $this->common_model->common_singleSelect('tbl_user',array('id'=>$user_id));
         // echo "<pre>"; print_r($data); die;
        if($this->input->post()){

            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tbl_user.is_deleted="0" AND email=]');
            $this->form_validation->set_rules('country_code', 'Country Code', 'required|trim');     
            $this->form_validation->set_rules('phone', 'Phone', 'required|trim|min_length[8]|max_length[12]|is_unique[tbl_user.is_deleted="0" AND phone=]|max_length[14]');
            $this->form_validation->set_rules('dob', 'Date of birth', 'required|trim'); 
            $this->form_validation->set_error_delimiters('<div class = "error" style="color:#F44336;">','</div>');

            if ($this->form_validation->run()) {
                
                $profile_image = 'default.png'; 
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'],USER_IMAGE);
                     //echo $profile_image; die;
                    if(!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $dob = $this->input->post('dob');
                $diff = (date('Y') - date('Y',strtotime($dob)));
                echo $diff;

                $age_category = '';
                if($diff >= '18'){
                    $age_category = 'Adults';
                }else if($diff >= '5' && $diff <= '8'){
                    $age_category = 'Primary';
                }else if($diff >= '9' && $diff <= '12'){
                    $age_category = 'Juniors';
                }else{
                    $age_category = 'Teens';
                }
                

                $customerdata = array(
                    'firstname'          => $this->input->post('firstname'),
                    'lastname'           => $this->input->post('lastname'),
                    'username'           => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'email'              => $this->input->post('email'),
                    'country_code'       => $this->input->post('country_code'),
                    'phone'              => $this->input->post('phone'),
                    'dob'                => date('Y-m-d', strtotime($this->input->post('dob'))),
                    'profile_image'      => $profile_image,
                    'age'                => $diff,
                    'age_category'       => $age_category,
                    'is_dob_edit'        => $this->input->post('is_dob_edit')
                );
                // echo "<pre>"; print_r($customerdata); die;
                $this->common_model->common_insert('tbl_user', $customerdata);

                if ($this->input->post('status') == 'Inactive') {
                    $userdeviceinfo = array('token'=>"",'device_token'=>"");
                    $this->common_model->save_user_deviceinfo($user_id,"U",$userdeviceinfo);
                }
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_customer_add_success'));
                redirect('authpanel/customer');   
            } else {
                $this->load->view($this->view_folder.'add', $data);
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }



    /*
    ** Function for load the cusomer view page
    */
    public function view($user_id)
    {
        $user_id              = base64_decode($user_id);
        $data['result']       = $this->common_model->common_singleSelect('tbl_user',array('id'=>$user_id));
        $data['deviceinfo']   = $this->common_model->common_singleSelect('tbl_user_deviceinfo',array('user_id'=>$user_id,'user_type'=>'U'));
        // echo "<pre>"; print_r($data); die();
        $this->load->view($this->view_folder.'view', $data);
    }

    public function accepted($user_id){
        $user_id = base64_decode($user_id);
        $this->common_model->common_singleUpdate('tbl_user',array('is_video_verify'=>'Verify'),array('id'=>$user_id));
        $this->session->set_flashdata('success_msg',"User profile video is verified successfully.");
        redirect('authpanel/customer'); 
    }

    public function rejected($user_id){
        $user_id = base64_decode($user_id);
        $this->common_model->common_singleUpdate('tbl_user',array('is_video_verify'=>'Unverify'),array('id'=>$user_id));
        $this->session->set_flashdata('error_msg',"User profile video is not verify.");
        redirect('authpanel/customer'); 
    }

    /*
    ** Function for update the status of customer
    */
    public function changestatus($user_id,$status)
    {
        $this->common_model->common_singleUpdate('tbl_user',array('status'=>$status),array('id'=>$user_id));
        if($status  == 'Inactive') {
            $userdeviceinfo['token'] = "";
            $userdeviceinfo['device_token'] = "";
            $this->common_model->save_user_deviceinfo($user_id,"U",$userdeviceinfo);
        }
        echo $this->lang->line('adminpanel_message_customer_'.$status.'_success');die;
    }

    /*
    ** Function for remove customer
    */
    public function removecustomer($user_id)
    {
        $this->common_model->common_singleUpdate('tbl_user',array('is_deleted'=>'1'),array('id'=>$user_id));
        $userdeviceinfo['token'] = "";
        $userdeviceinfo['device_token'] = "";
        $this->common_model->save_user_deviceinfo($user_id,"U",$userdeviceinfo);   
        echo $this->lang->line('adminpanel_message_customer_delete_success');die;
    }

  /*
    ** Function for remove customer
    */
    public function restorecustomer($user_id)
    {
        $data['user_data'] = $this->common_model->common_singleSelect("tbl_user",array("id"=>$user_id));
        
        $data['user_data_email'] = $this->common_model->common_singleSelect("tbl_user",array('email'=>$data['user_data']['email'],'is_deleted'=>'0'));
        $data['user_data_phone'] = $this->common_model->common_singleSelect("tbl_user",array('country_code'=>$data['user_data']['country_code'],'phone'=>$data['user_data']['phone'],'is_deleted'=>'0'));
        if(empty($data['user_data_email']) && empty($data['user_data_phone'])){
            $this->common_model->common_singleUpdate('tbl_user',array('is_deleted'=>'0'),array('id'=>$user_id));
        echo $this->lang->line('adminpanel_message_customer_restore_success');die;
        }else{
            // $this->common_model->common_singleUpdate('tbl_user',array('is_deleted'=>'0'),array('id'=>$user_id));
            if(empty($data['user_data_email'])){
                    echo $this->lang->line('rest_keywords_eamil_already_exists');die;
            }else{
                    echo $this->lang->line('rest_keywords_phone_already_exists');die;
            }
        
        }
        die;
        $this->common_model->common_singleUpdate('tbl_user',array('is_deleted'=>'0'),array('id'=>$user_id));
        echo $this->lang->line('adminpanel_message_customer_restore_success');die;
    }

    /*
    ** Function for export the data
    */
    public function export(){
        $data['data_list'] = $this->customer_model->export();
        if(!empty($data['data_list'])){
            $this->load->view($this->view_folder.'report', $data);
        }else{
            $this->session->set_flashdata('error_msg', 'There is no data for export.');
            redirect('authpanel/customer');
        }
    }

    public function student($user_id){
        $data['user_id'] = $user_id;
        $this->load->view($this->view_folder.'student_listing', $data);
    }

    public function student_ajax_list($user_id){
        $user_id = base64_decode($user_id);
        $list = $this->customer_model->get_datatables_student($user_id);
         // echo "<pre>"; print_r($this->db->last_query()); die();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $image      = '<a href="'.$row_data->profile_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->profile_image.'  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $video = (!empty($row_data->profile_video)) ? '<video width="150px" controls > <source src="'. $row_data->profile_video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']         =  $video;
                $row['username']      =  $row_data->username; 
                $row['dob']           =  date('d M Y', strtotime($row_data->dob));
                $row['age']           =  $row_data->age;
                $row['age_category']     =  $row_data->age_category;
       
                $subprofile_count     =  $this->customer_model->get_studentcount($row_data->id);
             
                $row['subprofile_count'] = $subprofile_count->subprofile_count;

                    
                if($row_data->is_video_verify == 'Verify'){
                    $is_verify = '<span class="text-success" style="font-weight:bold">Verify</span>';
                }else if($row_data->is_video_verify == 'Unverify'){
                    $is_verify = '<span class="text-danger" style="font-weight:bold">Unverify</span>';
                }else{
                    $is_verify = '<a href="'.site_url().'authpanel/customer/student_accepted/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-check"></i></a>';
       
                    $is_verify .= ' <a href="'.site_url().'authpanel/customer/student_rejected/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-close"></i></a>';
                }

                $row['is_verify'] =$is_verify;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-studentid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                $action = '<a href="'.site_url().'authpanel/customer/student_view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action .= ' <a href="'.site_url().'authpanel/customer/student_edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;

                $row['last_login'] = $this->common_model->date_convert($row_data->last_login,ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->customer_model->count_filtered_student($user_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function student_edit($student_id){
        $student_id                = base64_decode($student_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_student',array('id'=>$student_id));
         // echo "<pre>"; print_r($data); die;
        if($this->input->post() && !empty($data['result'])){

            $this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
            $this->form_validation->set_rules('dob', 'Date of birth', 'required|trim'); 
            $this->form_validation->set_error_delimiters('<div class = "error" style="color:#F44336;">','</div>');

            if ($this->form_validation->run()) {
                
                $profile_image = @$data['result']['profile_image']; 
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'], STUDENT_IMAGE);
                     //echo $profile_image; die;
                    if(!$profile_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $dob = $this->input->post('dob');
                $diff = (date('Y') - date('Y',strtotime($dob)));
                echo $diff;

                $age_category = '';
                if($diff >= '18'){
                    $age_category = 'Adults';
                }else if($diff >= '5' && $diff <= '8'){
                    $age_category = 'Primary';
                }else if($diff >= '9' && $diff <= '12'){
                    $age_category = 'Juniors';
                }else{
                    $age_category = 'Teens';
                }
                

                $customerdata = array(
                    'firstname'          => $this->input->post('firstname'),
                    'lastname'           => $this->input->post('lastname'),
                    'dob'                => date('Y-m-d', strtotime($this->input->post('dob'))),
                    'profile_image'      => $profile_image,
                    'age'                => $diff,
                    'age_category'       => $age_category,
                    'username'           => $this->input->post('firstname').' '.$this->input->post('lastname'),
                    'is_dob_edit'        => $this->input->post('is_dob_edit')
                );
                $this->common_model->common_singleUpdate('tbl_student', $customerdata,  array('id'=>$student_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_student_update_success'));
                redirect('authpanel/customer/student/'.base64_encode($data['result']['user_id']));   
            } else {
                $this->load->view($this->view_folder.'student_edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'student_edit', $data);
        }
    }

    /*
    ** Function for load the cusomer view page
    */
    public function student_view($student_id)
    {
        $student_id           = base64_decode($student_id);
        $data['result']       = $this->common_model->common_singleSelect('tbl_student',array('id'=>$student_id));
        $this->load->view($this->view_folder.'student_view', $data);
    }

    public function student_accepted($student_id){
        $student_id = base64_decode($student_id);
        $this->common_model->common_singleUpdate('tbl_student',array('is_video_verify'=>'Verify'),array('id'=>$student_id));
        $stud = $this->common_model->common_singleSelect('tbl_student',array('id'=>$student_id));
        $this->session->set_flashdata('success_msg',"Student profile video is verified successfully.");
        redirect('authpanel/customer/student/'.base64_encode($stud['user_id'])); 
    }

    public function student_rejected($student_id){
        $student_id = base64_decode($student_id);
        $this->common_model->common_singleUpdate('tbl_student',array('is_video_verify'=>'Unverify'),array('id'=>$student_id));
        $stud = $this->common_model->common_singleSelect('tbl_student',array('id'=>$student_id));
        $this->session->set_flashdata('error_msg',"Student profile video is not verify.");
        redirect('authpanel/customer/student/'.base64_encode($stud['user_id'])); 
    }

    /*
    ** Function for update the status of customer
    */
    public function student_changestatus($student,$status)
    {
        $this->common_model->common_singleUpdate('tbl_student',array('status'=>$status),array('id'=>$student));
        echo $this->lang->line('adminpanel_message_student_'.$status.'_success');die;
    }

    /*
    ** Function for remove student
    */
    public function removestudent($student)
    {
        $this->common_model->common_singleUpdate('tbl_student',array('is_deleted'=>'1'),array('id'=>$student)); 
        echo $this->lang->line('adminpanel_message_student_delete_success');die;
    }

    

}
