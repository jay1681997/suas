<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Masterprize extends MY_Controller {

    private $view_folder = 'authpanel/masterprize/';
    function __construct(){
        parent::__construct();
        //load prize model
        $this->load->model('authpanel/masterprize_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load prize data
    */
    public function ajax_list(){
        $list = $this->masterprize_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = '<a href="'.$row_data->prize_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->prize_image.'  alt="No image"></a>';
                $row['prize_image']          = $image;
                $row['name']                = $row_data->name;
                $row['description']          = $row_data->description;
                $row['reward_point']        = $row_data->reward_point;
                $row['price']                = $row_data->currency.''.$row_data->price;
                $row['contest_name']         = $row_data->contest_name;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-prizeid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/prize/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/masterprize/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->masterprize_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the prize details
    */
    public function add()
    {  
        $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description','Description','required|trim');
            $this->form_validation->set_rules('reward_point','Reward Point','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

                $prize_image = "default.png";
                if (!empty($_FILES['prize_image']) && $_FILES['prize_image']['size'] > 0) {
                    $prize_image = $this->common_model->uploadImageS3($_FILES['prize_image'],PRIZE_IMAGE);
                    if(!$prize_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $prizedata = array(
                    'name'              => $this->input->post('name'),
                    'prize_image'       => $prize_image,
                    'description'       => $this->input->post('description'),                    
                    'reward_point'      => $this->input->post('reward_point'),
                );

                $prize_id=$this->common_model->common_insert('tbl_master_prize', $prizedata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_prize_add_success'));
                redirect('authpanel/masterprize');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the prize details
    */
    public function edit($prize_id)
    {
        $prize_id     = base64_decode($prize_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_master_prize',array('id'=>$prize_id));
        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description','Description','required|trim');
            $this->form_validation->set_rules('reward_point','Reward Point','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                
                $prize_image = @$data['result']['prize_image'];
                if (!empty($_FILES['prize_image']) && $_FILES['prize_image']['size'] > 0) {
                    $prize_image = $this->common_model->uploadImageS3($_FILES['prize_image'],PRIZE_IMAGE);
                    if(!$prize_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $prizedata = array(
                    'name'              => $this->input->post('name'),
                    'prize_image'       => $prize_image,
                    'description'       => $this->input->post('description'),                    
                    'reward_point'      => $this->input->post('reward_point'),
                );
                
                $this->common_model->common_singleUpdate('tbl_master_prize',$prizedata,array('id'=>$prize_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_prize_update_success'));
                redirect('authpanel/masterprize');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the prize view page
    */
    public function view($prize_id){
        $prize_id          = base64_decode($prize_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_prize',array('id'=>$prize_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of prize
    */
    public function changestatus($prize_id, $status){

        $this->common_model->common_singleUpdate('tbl_master_prize',array('status'=>$status),array('id'=>$prize_id));
        echo $this->lang->line('adminpanel_message_prize_'.$status.'_success');die;
    }

    /*
    ** Function for remove prize
    */
    public function removeprize($prize_id)
    {
        $this->common_model->common_singleUpdate('tbl_master_prize',array('is_deleted'=>'1'),array('id'=>$prize_id));
        echo $this->lang->line('adminpanel_message_prize_delete_success');die;
    }
    
}
