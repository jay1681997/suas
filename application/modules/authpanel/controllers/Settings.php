<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 04-Feb-2022
** Modified On  : -  
*/
class Settings extends MY_Controller
{

    /*
    ** Default constructor
    */
    private $view_folder = 'authpanel/settings/';
    function __construct()
    {
        parent::__construct();
        $this->load->model('authpanel/common_model');
    }
    /*
    * index method 
    */
    public function index()
    {
        $this->session->unset_userdata('users');
        $data["result"]   = $this->common_model->common_singleSelect('tbl_setting', array("is_deleted" => "0"));
        $this->load->view($this->view_folder . 'settings', $data);
    }

    public function edit($setting_id)
    {
        $data["result"]   = $this->common_model->common_singleSelect('tbl_setting', array("is_deleted" => "0"));
        $setting_id        = base64_decode($setting_id);
        if ($this->input->post()) {
            // echo "<pre>";
            // print_r($this->input->post());
            // die;
            $setting_data = array('currency' => $this->input->post('currency'), 'currency_symbol' => $this->input->post('currency_symbol'), 'tax' => $this->input->post('tax'), 'credit_point_discount' => $this->input->post('credit_point_discount'));
            $this->common_model->common_singleUpdate('tbl_setting', $setting_data, array("id" => $setting_id));
            $this->session->set_flashdata('success_msg', $this->lang->line('rest_keywords_edit_setting_details'));
            redirect('authpanel/settings');
        } else {
            $this->load->view($this->view_folder . 'settings', $data);
        }
    }
}
