<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAs
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Banner extends MY_Controller {

    private $view_folder = 'authpanel/banner/';
    function __construct(){
        parent::__construct();
        //load banner model
        $this->load->model('authpanel/banner_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load banner data
    */
    public function ajax_list(){
        $list = $this->banner_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = '<a href="'.$row_data->banner_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->banner_image.'  alt="No image"></a>';
                $row['banner_image']        = $image;
                $row['title']                 = $row_data->title;
                $row['email']                = $row_data->email;
                $row['phone']                = $row_data->country_code.' '.$row_data->phone;
              
                
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-bannerid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

               
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/banner/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                // $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->banner_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the banner details
    */
    public function add()
    {  
        if($this->input->post())
        {
            $this->form_validation->set_rules('title','Title','required|trim');
            $this->form_validation->set_error_delimiters('<div banner="error" style="color:#F44336;">','</div>');
            if($this->form_validation->run()) {

                $banner_image = "default.png";
                if (!empty($_FILES['banner_image']) && $_FILES['banner_image']['size'] > 0) {
                    $banner_image = $this->common_model->uploadImageS3($_FILES['banner_image'],BANNER_IMAGE);
                    if(!$banner_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $bannerdata = array(
                    'title'          => $this->input->post('title'),
                    'banner_image'   => $banner_image,
                );

                $banner_id=$this->common_model->common_insert('tbl_banner', $bannerdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_banner_add_success'));
                redirect('authpanel/banner');
            } else {
                $this->load->view($this->view_folder.'add');    
            }
        } else {
            $this->load->view($this->view_folder.'add');
        }
    }

    /*
    ** Function for edit the banner details
    */
    public function edit($banner_id)
    {
        $banner_id     = base64_decode($banner_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_banner',array('id'=>$banner_id));
        if($this->input->post() && !empty($data['result']))
        {
            // print_r($this->input->post()); die;
            $this->form_validation->set_rules('title','Title','required|trim');
            $this->form_validation->set_error_delimiters('<div banner="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                
                $banner_image = @$data['result']['banner_image'];
                if (!empty($_FILES['banner_image']) && $_FILES['banner_image']['size'] > 0) {
                    $banner_image = $this->common_model->uploadImageS3($_FILES['banner_image'],BANNER_IMAGE);
                    if(!$banner_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $bannerdata = array(
                    'title'          => $this->input->post('title'),
                    'banner_image'   => $banner_image,
                );
                
                $this->common_model->common_singleUpdate('tbl_banner',$bannerdata,array('id'=>$banner_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_banner_update_success'));
                redirect('authpanel/banner');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the banner view page
    */
    public function view($banner_id){
        $banner_id          = base64_decode($banner_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_banner',array('id'=>$banner_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of banner
    */
    public function changestatus($banner_id, $status){

        $this->common_model->common_singleUpdate('tbl_banner',array('status'=>$status),array('id'=>$banner_id));
        echo $this->lang->line('adminpanel_message_banner_'.$status.'_success');die;
    }

    /*
    ** Function for remove banner
    */
    public function removebanner($banner_id)
    {
        $this->common_model->common_singleUpdate('tbl_banner',array('is_deleted'=>'1'),array('id'=>$banner_id));
        echo $this->lang->line('adminpanel_message_banner_delete_success');die;
    }


}
