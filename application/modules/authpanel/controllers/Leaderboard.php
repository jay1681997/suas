<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Jan-2022
** Modified On  : -  
*/
class Leaderboard extends MY_Controller {

    private $view_folder = 'authpanel/leaderboard/';
    function __construct(){
        parent::__construct();
        //load leaderboard model
        $this->load->model('authpanel/leaderboard_model');
    }

    /*
    **Load index page
    */
    public function index(){
        $this->load->view($this->view_folder.'listing');
    }

    /*
    ** Function for load leaderboard data
    */
    public function ajax_list(){
        
        $list = $this->leaderboard_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $row['id']  = $row_data->id;

                $image = '<a href="'.$row_data->image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->image.'  alt="No image"></a>';

                $row['image']         = $image;
                $row['name']          = $row_data->name;
                $row['value']         = $row_data->value;
                $row['country']         = $row_data->country;
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-leaderboardid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/leaderboard/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/leaderboard/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
              
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->leaderboard_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the post details
    */
    public function add()
    {  
        $data['countries']      = $this->common_model->get_country_code();
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('country','Country','required|trim');
            $this->form_validation->set_rules('value', 'Value','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {
                $image = "default.png";
                if (!empty($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $image = $this->common_model->uploadImageS3($_FILES['image'],LEADERBOARD_IMAGE);
                    if(!$image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $postdata = array(                  
                    'name'          => $this->input->post('name'),
                    'country'       => $this->input->post('country'),
                    'value'         => $this->input->post('value'),
                    'image'         => $image,
                );

                $leaderboard_id=$this->common_model->common_insert('tbl_leaderboard', $postdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_leaderboard_add_success'));
                redirect('authpanel/leaderboard');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the post details
    */
    public function edit($leaderboard_id)
    {
        $leaderboard_id     = base64_decode($leaderboard_id);
        $data['countries']  = $this->common_model->get_country_code();
        $data['result']     = $this->common_model->common_singleSelect('tbl_leaderboard',array('id' => $leaderboard_id));
        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('country','Country','required|trim');
            $this->form_validation->set_rules('value', 'Value','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                $image = @$data['result']['image']; 
                if (!empty($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $image = $this->common_model->uploadImageS3($_FILES['image'],LEADERBOARD_IMAGE);
                    if(!$image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $postdata = array(                  
                    'name'          => $this->input->post('name'),
                    'country'       => $this->input->post('country'),
                    'value'         => $this->input->post('value'),
                    'image'         => $image,
                );
        
                $this->common_model->common_singleUpdate('tbl_leaderboard',$postdata,array('id'=>$leaderboard_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_leaderboard_update_success'));
                redirect('authpanel/leaderboard');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the video library view page
    */
    public function view($leaderboard_id){
        $leaderboard_id    = base64_decode($leaderboard_id);
        $data['result']     = $this->leaderboard_model->view_leaderboard($leaderboard_id);
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of video library
    */
    public function changestatus($leaderboard_id, $status){
        $this->common_model->common_singleUpdate('tbl_leaderboard',array('status'=>$status),array('id'=>$leaderboard_id));
        echo $this->lang->line('adminpanel_message_leaderboard_'.$status.'_success');die;
    }

    /*
    ** Function for remove video library
    */
    public function removeleaderboard($leaderboard_id)
    {
        $this->common_model->common_singleUpdate('tbl_leaderboard',array('is_deleted'=>'1'),array('id'=>$leaderboard_id));
        echo $this->lang->line('adminpanel_message_leaderboard_delete_success');die;
    }

}
