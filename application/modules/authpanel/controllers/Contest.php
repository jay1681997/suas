<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Contest extends MY_Controller
{

    private $view_folder = 'authpanel/contest/';
    function __construct()
    {
        parent::__construct();
        // ini_set('upload_max_filesize', '20M');
        // ini_set('post_max_size', '20M');
        // ini_set('max_input_time', 300);
        // ini_set('max_execution_time', 300);

        // echo ini_get("upload_max_filesize")."\n<br>";
        // ini_set("upload_max_filesize","10M");
        // echo ini_get("upload_max_filesize")."\n<br>";

        // echo ini_get("post_max_size")."\n<br>";
        // ini_set("post_max_size","10M");
        // echo ini_get("post_max_size")."\n<br>";

        // echo ini_get("max_input_time")."\n<br>";
        // ini_set("max_input_time",300);
        // echo ini_get("max_input_time")."\n<br>";

        // echo ini_get("max_execution_time")."\n<br>";
        // ini_set("max_execution_time",300);
        // echo ini_get("max_execution_time")."\n<br>";


        // echo "<pre>"; print_r(ini_get('display_errors'));die;
        //load contest model
        $this->load->model('authpanel/contest_model');
    }

    /*
    *Load index page
    */
    public function index()
    {
        $this->load->view($this->view_folder . 'listing');
    }

    public function booking_list($contest_id)
    {
        // $contest_id     = base64_decode($contest_id);
        $data['contest_id'] = base64_decode($contest_id);
        //  print_r($data);
        // die;
        $this->load->view($this->view_folder . 'booking_listing', $data);
    }

    public function booking_ajax_list($category_id)
    {

        $category_id = base64_decode($category_id);

        $list = $this->contest_model->get_datatables_booking($category_id);

        $data = array();
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['username']                   = $row_data->username;
                $image      = '<a href="' . S3_BUCKET_ROOT . USER_IMAGE . $row_data->profile_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . S3_BUCKET_ROOT . USER_IMAGE . $row_data->profile_image . '  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->contest_model->count_filtered_d($contest_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for load contest data
    */
    public function ajax_list()
    {
        $list = $this->contest_model->get_datatables();
        $data = array();
        // echo "<pre>";
        // print_r($list);
        // die;
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;

                $file_ext = explode('.', $row_data->contest_image);
                // print_r($file_ext); die;                                   
                if ($row_data->contest_type != 'live') {
                    $image                       = (!empty($row_data->contest_image)) ? '<video width="150px" controls > <source src="' . $row_data->contest_image . '" > Your browser does not support HTML5 video. </video>' : '-';
                } else {
                    $image                       = '<a href="' . $row_data->contest_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->contest_image . '  alt="No image"></a>';
                }


                $row['contest_image']        = $image;
                $row['name']                 = $row_data->name;
                $row['description']          = $row_data->description;
                $contest_startdate = $this->common_model->date_convert($row_data->contest_startdate, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $contest_enddate = $this->common_model->date_convert($row_data->contest_enddate, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $row['contest_startdate']    = $contest_startdate;
                $row['contest_enddate']      = $contest_enddate;
                $row['total_participant']    = $row_data->total_participant;
                $row['total_point']          = $row_data->total_point;
                $row['price']                = $row_data->currency . '' . $row_data->price;
                $row['winner_date']          = date('d M Y', strtotime($row_data->winner_date));
                $row['age_category']         = $row_data->age_category;
                $row['contest_by']           = $row_data->contest_by;
                if ($row_data->contest_type == 'live')
                    $row['contest_type'] = '<span class="text-success">Live</span>';
                else
                    $row['contest_type'] = '<span class="text-primary">Video</span>';

                //Check status
                if ($row_data->status == 'Active') {
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status = '<div class="switch"><label><input type="checkbox" name="changestatus" data-contestid="' . $row_data->id . '" data-status="' . $row_data->status . '" ' . $checkstete . '><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                $action = '<a href="' . site_url() . 'authpanel/contest/view/' . base64_encode($row_data->id) . '"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action .= ' <a href="' . site_url() . 'authpanel/contest/edit/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove(' . $row_data->id . ');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';
                $action .= ' <a href="' . site_url() . 'authpanel/contest/booking_list/' . base64_encode($row_data->contest_id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-menu"></i></a>';
                $row['action'] = $action;

                $row['subadmin_name']        = $row_data->subadmin_name;
                $days_list = explode(":", $row_data->dtime);
                // if($days_list[0] > 0){
                //     $row['hosting']              =  '<a href="#"class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>'; 
                // }else if($days_list[1] <= 30 && $row_data->contest_type == 'live'){
                //      $row['hosting']              =  '<a href="'.site_url().'authpanel/contest/agora_hosting/'.base64_encode($row_data->id).'"class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>';
                // }else{
                //      $row['hosting']              =  "-";
                // }
                if ($row_data->between_flag == 1 && $row_data->contest_type == 'live') {
                    $row['hosting']              = '<a href="' . site_url() . 'authpanel/contest/agora_hosting/' . base64_encode($row_data->id) . '"class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>';
                } else if ($row_data->date_flag == 1 && $row_data->contest_type == 'live') {
                    $row['hosting']              = '<a href="" class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Upcoming Contest</a>';
                } else {
                    $row['hosting']              =  "-";
                }
                // if(){
                //      $row['hosting']              =  '<a href="'.site_url().'authpanel/contest/agora_hosting/'.base64_encode($row_data->id).'"class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>';
                //  }else{
                //       $row['hosting']              =  "-";
                //  }


                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->contest_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the contest details
    */
    public function add()
    {
        $data['subadmins']  = $this->common_model->common_multipleSelect('tbl_admin', array('is_deleted' => '0', 'status' => 'Active', 'role' => 'Sub'));
        $data['sponsor'] = $this->common_model->common_multipleSelect('tbl_sponsor', array('is_deleted' => '0', 'status' => 'Active'));
        $data['scorecard_list'] = $this->common_model->common_multipleSelect("tbl_contest_scorecard", array("is_deleted" => "0", "status" => "Active"));
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            $this->form_validation->set_rules('contest_type', 'Contest Type', 'required|trim');
            $this->form_validation->set_rules('total_participant', 'Total Participant', 'required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('contest_startdate', 'Contest Start Datetime', 'required|trim');
            $this->form_validation->set_rules('contest_enddate', 'Contest End Datetime', 'required|trim');
            $this->form_validation->set_rules('age_category', 'Age category', 'required|trim');
            $this->form_validation->set_rules('winner_date', 'Winner Date', 'required|trim');
            $this->form_validation->set_rules('contest_by', 'Contest By', 'required|trim');
            // $this->form_validation->set_rules('subadmin_id', 'Sub Admin', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');
            // if ($this->input->post('contest_type') == 'video') {
            //     $this->form_validation->set_rules('thumb_image', 'Thumb Image', 'required|trim');
            // }
            if ($this->form_validation->run()) {

                $imgname   = "default.png";
                $thumb_image = '';

                if ($this->input->post('contest_type') == 'video') {
                    if (!empty($_FILES['contest_image']) && $_FILES['contest_image']['size'] > 0) {
                        $uploaded_filename = $this->common_model->uploadImageS3($_FILES['contest_image'], CONTEST_IMAGE);
                        // echo "<pre>"; print_r($uploaded_filename); die;
                        if (!$uploaded_filename) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder . 'add', $data);
                            return false;
                        }

                        if (!empty($uploaded_filename) && $uploaded_filename != "") {
                            //FOR video and its IMAGE
                            if (!empty($this->input->post('thumb_image'))) {
                                $data = $_POST['thumb_image'];
                                //list($type, $data) = explode(';', $data);
                                //list(, $data)      = explode(',', $data);
                                $data = str_replace('data:image/png;base64,', '', $data);
                                $data = str_replace(' ', '+', $data);
                                $data = base64_decode($data);
                                $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                                file_put_contents(THUMB_DIR . $thumb_media_name, $data);
                                $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, CONTEST_IMAGE);
                                // $imgname = $uploaded_imagename;  
                                $thumb_image = $uploaded_imagename;
                            }
                            $imgname = $uploaded_filename;
                        } else {
                            $this->load->view($this->view_folder . 'add', $data);
                        }
                    }

                    $video_duration = '';
                    if ($this->input->post('contest_type') == 'video') {
                        $remotefilename = S3_BUCKET_ROOT . CONTEST_IMAGE . $imgname;
                        if ($fp_remote = fopen($remotefilename, 'rb')) {
                            $localtempfilename = tempnam('/tmp', 'getID3');
                            if ($fp_local = fopen($localtempfilename, 'wb')) {
                                while ($buffer = fread($fp_remote, 8192)) {
                                    fwrite($fp_local, $buffer);
                                }
                                fclose($fp_local);
                                // Initialize getID3 engine
                                $getID3 = new getID3;
                                $ThisFileInfo = $getID3->analyze($localtempfilename);
                                $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                                // Delete temporary file
                                unlink($localtempfilename);
                            }
                            fclose($fp_remote);
                        }
                    }
                }
                if ($this->input->post('contest_type') == 'live') {
                    $thumb_image = '';
                    $video_duration = '';
                    // echo "P1";
                    if (!empty($_FILES['contest_image_image']) && $_FILES['contest_image_image']['size'] > 0) {
                        // echo "P2";
                        $imgname = $this->common_model->uploadImageS3($_FILES['contest_image_image'], CONTEST_IMAGE);
                        //echo $profile_image; die;
                        echo $imgname;
                        if (!$imgname) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder . 'add');
                            return false;
                        }
                    }
                }
                $contest_startdate = $this->common_model->convertTimetoUTC($this->input->post('contest_startdate'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $contest_enddate = $this->common_model->convertTimetoUTC($this->input->post('contest_enddate'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));

                // print_r($video_duration);
                $contestdata = array(
                    'name'               => $this->input->post('name'),
                    'contest_image'      => $imgname,
                    'thumb_image'        => $thumb_image,
                    'video_duration'     => $video_duration,
                    'description'        => $this->input->post('description'),
                    'contest_type'       => $this->input->post('contest_type'),
                    'price'              => $this->input->post('price'),
                    // 'contest_startdate'  => date('Y-m-d H:i', strtotime($this->input->post('contest_startdate'))),
                    // 'contest_enddate'    => date('Y-m-d H:i', strtotime($this->input->post('contest_enddate'))),
                    'contest_startdate'  => $contest_startdate,
                    'contest_enddate'    => $contest_enddate,
                    //  'sponsor_id'         => $this->input->post('sponsor_id'),
                    'total_participant'  => $this->input->post('total_participant'),
                    'total_point'        => $this->input->post('total_point'),
                    'age_category'       => $this->input->post('age_category'),
                    'contest_by'         => $this->input->post('contest_by'),
                    // 'subadmin_id'        => $this->input->post('subadmin_id'),
                    'winner_date'        => date('Y-m-d', strtotime($this->input->post('winner_date'))),
                    'channel_name'       => 'CONTEST' . random_string('numeric', 8),
                    'scorecard_id' => (!empty($this->input->post('scorecard_id'))) ? implode(",", $this->input->post('scorecard_id')) : '',
                );
                // echo "<pre>";
                // print_r($contestdata);
                // die;
                if (!empty($this->input->post('sponsor_id'))) {
                    $contestdata['sponsor_id'] = $this->input->post('sponsor_id');
                }
                if (!empty($this->input->post('subadmin_id'))) {
                    $contestdata['subadmin_id'] = $this->input->post('subadmin_id');
                }
                $contest_id = $this->common_model->common_insert('tbl_master_contest', $contestdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_contest_add_success'));
                redirect('authpanel/contest');
            } else {
                $this->load->view($this->view_folder . 'add', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'add', $data);
        }
    }
    public function add_old()
    {
        $data['subadmins']  = $this->common_model->common_multipleSelect('tbl_admin', array('is_deleted' => '0', 'status' => 'Active', 'role' => 'Sub'));
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            $this->form_validation->set_rules('contest_type', 'Contest Type', 'required|trim');
            $this->form_validation->set_rules('total_participant', 'Total Participant', 'required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('contest_startdate', 'Contest Start Datetime', 'required|trim');
            $this->form_validation->set_rules('contest_enddate', 'Contest End Datetime', 'required|trim');
            $this->form_validation->set_rules('age_category', 'Age category', 'required|trim');
            $this->form_validation->set_rules('winner_date', 'Winner Date', 'required|trim');
            $this->form_validation->set_rules('contest_by', 'Contest By', 'required|trim');
            $this->form_validation->set_rules('subadmin_id', 'Sub Admin', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');

            if ($this->form_validation->run()) {

                $imgname   = "default.png";
                $thumb_image = '';
                if (!empty($_FILES['contest_image']) && $_FILES['contest_image']['size'] > 0) {
                    $uploaded_filename = $this->common_model->uploadImageS3($_FILES['contest_image'], CONTEST_IMAGE);
                    // echo "<pre>"; print_r($uploaded_filename); die;
                    if (!$uploaded_filename) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'add', $data);
                        return false;
                    }

                    if (!empty($uploaded_filename) && $uploaded_filename != "") {
                        //FOR video and its IMAGE
                        if (!empty($this->input->post('thumb_image'))) {
                            $data = $_POST['thumb_image'];
                            //list($type, $data) = explode(';', $data);
                            //list(, $data)      = explode(',', $data);
                            $data = str_replace('data:image/png;base64,', '', $data);
                            $data = str_replace(' ', '+', $data);
                            $data = base64_decode($data);
                            $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                            file_put_contents(THUMB_DIR . $thumb_media_name, $data);
                            $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, CONTEST_IMAGE);
                            // $imgname = $uploaded_imagename;  
                            $thumb_image = $uploaded_imagename;
                        }
                        $imgname = $uploaded_filename;
                    } else {
                        $this->load->view($this->view_folder . 'add', $data);
                    }
                }

                $video_duration = '';
                if ($this->input->post('contest_type') == 'video') {
                    $remotefilename = S3_BUCKET_ROOT . CONTEST_IMAGE . $imgname;
                    if ($fp_remote = fopen($remotefilename, 'rb')) {
                        $localtempfilename = tempnam('/tmp', 'getID3');
                        if ($fp_local = fopen($localtempfilename, 'wb')) {
                            while ($buffer = fread($fp_remote, 8192)) {
                                fwrite($fp_local, $buffer);
                            }
                            fclose($fp_local);
                            // Initialize getID3 engine
                            $getID3 = new getID3;
                            $ThisFileInfo = $getID3->analyze($localtempfilename);
                            $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                            // Delete temporary file
                            unlink($localtempfilename);
                        }
                        fclose($fp_remote);
                    }
                }

                $contestdata = array(
                    'name'               => $this->input->post('name'),
                    'contest_image'      => $imgname,
                    'thumb_image'        => $thumb_image,
                    'video_duration'     => $video_duration,
                    'description'        => $this->input->post('description'),
                    'contest_type'       => $this->input->post('contest_type'),
                    'price'              => $this->input->post('price'),
                    'contest_startdate'  => date('Y-m-d H:i', strtotime($this->input->post('contest_startdate'))),
                    'contest_enddate'    => date('Y-m-d H:i', strtotime($this->input->post('contest_enddate'))),
                    'total_participant'  => $this->input->post('total_participant'),
                    'total_point'        => $this->input->post('total_point'),
                    'age_category'       => $this->input->post('age_category'),
                    'contest_by'         => $this->input->post('contest_by'),
                    'subadmin_id'        => $this->input->post('subadmin_id'),
                    'winner_date'        => date('Y-m-d', strtotime($this->input->post('winner_date'))),
                    'channel_name'       => 'CONTEST' . random_string('numeric', 8),
                );

                $contest_id = $this->common_model->common_insert('tbl_master_contest', $contestdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_contest_add_success'));
                redirect('authpanel/contest');
            } else {
                $this->load->view($this->view_folder . 'add', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'add', $data);
        }
    }

    /*
    ** Function for edit the contest details
    */
    public function edit($contest_id)
    {
        $contest_id     = base64_decode($contest_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $data['subadmins']  = $this->common_model->common_multipleSelect('tbl_admin', array('is_deleted' => '0', 'status' => 'Active', 'role' => 'Sub'));
        $data['sponsor'] = $this->common_model->common_multipleSelect('tbl_sponsor', array('is_deleted' => '0', 'status' => 'Active'));
        $data['scorecard_list'] = $this->common_model->common_multipleSelect("tbl_contest_scorecard", array("is_deleted" => "0", "status" => "Active"));
        $contest_startdate = $this->common_model->date_convert($data['result']['contest_startdate'], 'd M Y H:i', $this->session->userdata(ADMIN_TIMEZONE));
        $contest_enddate = $this->common_model->date_convert($data['result']['contest_enddate'], 'd M Y H:i', $this->session->userdata(ADMIN_TIMEZONE));
        $data['result']['contest_startdate'] = $contest_startdate;
        $data['result']['contest_enddate'] = $contest_enddate;

        if ($this->input->post() && !empty($data['result'])) {
            $this->form_validation->set_rules('name', 'Name', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            $this->form_validation->set_rules('contest_type', 'Contest Type', 'required|trim');
            $this->form_validation->set_rules('total_participant', 'Total Participant', 'required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('contest_startdate', 'Contest Start Datetime', 'required|trim');
            $this->form_validation->set_rules('contest_enddate', 'Contest End Datetime', 'required|trim');
            $this->form_validation->set_rules('age_category', 'Age category', 'required|trim');
            $this->form_validation->set_rules('winner_date', 'Winner Date', 'required|trim');
            $this->form_validation->set_rules('contest_by', 'Contest By', 'required|trim');
            // $this->form_validation->set_rules('subadmin_id','Sub Admin','required|trim');
            // if(!empty($_FILES['contest_image']['type']) && $_FILES['contest_image']['type'] == 'video/mp4'){
            //     $this->form_validation->set_rules('thumb_image', 'Thumb Image', 'required|trim');
            // }
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">', '</div>');
            if ($this->form_validation->run()) {
                $imgname = @$data['result']['contest_image'];
                $thumb_image = @$data['result']['thumb_image'];
                if ($this->input->post('contest_type') == 'video') {
                    if (!empty($_FILES['contest_image']) && $_FILES['contest_image']['size'] > 0) {

                        $uploaded_filename = $this->common_model->uploadImageS3($_FILES['contest_image'], CONTEST_IMAGE);
                        // echo "<pre>"; print_r($uploaded_filename); die;
                        if (!$uploaded_filename) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder . 'add', $data);
                            return false;
                        }

                        if (!empty($uploaded_filename) && $uploaded_filename != "") {
                            //FOR video and its IMAGE
                            if (!empty($this->input->post('thumb_image'))) {
                                $data = $_POST['thumb_image'];
                                //list($type, $data) = explode(';', $data);
                                //list(, $data)      = explode(',', $data);
                                $data = str_replace('data:image/png;base64,', '', $data);
                                $data = str_replace(' ', '+', $data);
                                $data = base64_decode($data);
                                $thumb_media_name = uniqid() . strtotime(date("Ymd his")) . ".png";
                                // echo "<pre>"; print_r($thumb_media_name);
                                file_put_contents(THUMB_DIR . $thumb_media_name, $data);
                                $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, CONTEST_IMAGE);
                                // echo "<pre>"; print_r($uploaded_imagename);
                                // $imgname = $uploaded_imagename;   
                                $thumb_image = $uploaded_imagename;
                            }
                            $imgname = $uploaded_filename;
                            // die;
                        } else {
                            $this->load->view($this->view_folder . 'add', $data);
                        }
                    }
                }
                if ($this->input->post('contest_type') == 'live') {
                    
                    if (!empty($_FILES['contest_image_image']) && $_FILES['contest_image_image']['size'] > 0) {
                        // echo "P2";
                        $imgname = $this->common_model->uploadImageS3($_FILES['contest_image_image'], CONTEST_IMAGE);
                        //echo $profile_image; die;
                        echo $imgname;
                        if (!$imgname) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder . 'add');
                            return false;
                        }
                    }
                }
                $video_duration = '';
                if ($this->input->post('contest_type') == 'video') {
                    $remotefilename = S3_BUCKET_ROOT . CONTEST_IMAGE . $imgname;
                    if ($fp_remote = fopen($remotefilename, 'rb')) {
                        $localtempfilename = tempnam('/tmp', 'getID3');
                        if ($fp_local = fopen($localtempfilename, 'wb')) {
                            while ($buffer = fread($fp_remote, 8192)) {
                                fwrite($fp_local, $buffer);
                            }
                            fclose($fp_local);
                            // Initialize getID3 engine
                            $getID3 = new getID3;
                            $ThisFileInfo = $getID3->analyze($localtempfilename);
                            $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                            // Delete temporary file
                            unlink($localtempfilename);
                        }
                        fclose($fp_remote);
                    }
                }
                $contest_startdate = $this->common_model->convertTimetoUTC($this->input->post('contest_startdate'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $contest_enddate = $this->common_model->convertTimetoUTC($this->input->post('contest_enddate'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $contestdata = array(
                    'name'               => $this->input->post('name'),
                    'contest_image'      => $imgname,
                    'thumb_image'        => $thumb_image,
                    'video_duration'     => $video_duration,
                    'description'        => $this->input->post('description'),
                    'contest_type'       => $this->input->post('contest_type'),
                    'price'              => $this->input->post('price'),
                    'contest_startdate'  => $contest_startdate,
                    'contest_enddate'    => $contest_enddate,
                    'total_participant'  => $this->input->post('total_participant'),
                    'total_point'        => $this->input->post('total_point'),
                    'age_category'       => $this->input->post('age_category'),
                    'contest_by'         => $this->input->post('contest_by'),
                    'winner_date'        => date('Y-m-d', strtotime($this->input->post('winner_date'))),
                    'scorecard_id' => (!empty($this->input->post('scorecard_id'))) ? implode(",", $this->input->post('scorecard_id')) : '',
                );
                if (!empty($this->input->post('sponsor_id'))) {
                    $contestdata['sponsor_id'] = $this->input->post('sponsor_id');
                }
                if (!empty($this->input->post('subadmin_id'))) {
                    $contestdata['subadmin_id'] = $this->input->post('subadmin_id');
                }
                $this->common_model->common_singleUpdate('tbl_master_contest', $contestdata, array('id' => $contest_id));
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_contest_update_success'));
                redirect('authpanel/contest');
            } else {
                $this->load->view($this->view_folder . 'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'edit', $data);
        }
    }

    public function agora_hosting($contest_id)
    {
        $contest_id         = base64_decode($contest_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $agora_token        = $this->common_model->get_agora_token($contest_id, $data['result']['subadmin_id'], 'contest');
        $data['token']      = $agora_token['data']['token'];
        $data['channel']    = $agora_token['data']['channelname'];
        $data['userid']     = $data['result']['subadmin_id'];
        $data['contest_id'] = $contest_id;
        $data['id']         = rand();
        $users = $this->common_model->get_contest_list('contest', $contest_id);
        foreach ($users as $key => $value) {
            $notification_data = array(
                'sender_id'        => '1',
                'sender_type'      => 'admin',
                'receiver_id'      => $value['id'],
                'receiver_type'    => 'user',
                'primary_id'       => $contest_id,
                'notification_tag' => "contest_started",
                'message'          => 'The "' . $data['result']['name'] . '" contest starts now. Please join the contest.',
                'title'            => 'Contest Started'
            );
            $this->common_model->send_notification($notification_data);
        }


        $this->load->view($this->view_folder . 'agora', $data);
    }

    /*
    ** Function for load the contest view page
    */
    public function view($contest_id)
    {
        $contest_id          = base64_decode($contest_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $data['scorecard']          = $this->common_model->common_multipleSelect('tbl_contest_scorecard', array("contest_id" => $contest_id, "is_deleted" => "0", "status" => "Active"));
        $data['winner_list'] = $this->common_model->common_multipleSelect("tbl_prize", array("contest_id" => $contest_id, "is_deleted" => "0"));
        // echo "<pre>";
        // print_r($data['result']);
        // die;
        $this->load->view($this->view_folder . 'view', $data);
    }

    /*
    ** Function for update the status of contest
    */
    public function changestatus($contest_id, $status)
    {

        $this->common_model->common_singleUpdate('tbl_master_contest', array('status' => $status), array('id' => $contest_id));
        echo $this->lang->line('adminpanel_message_contest_' . $status . '_success');
        die;
    }

    /*
    ** Function for remove contest
    */
    public function removecontest($contest_id)
    {
        $this->common_model->common_singleUpdate('tbl_master_contest', array('is_deleted' => '1'), array('id' => $contest_id));
        echo $this->lang->line('adminpanel_message_contest_delete_success');
        die;
    }


    public function participant_ajax_list($contest_id)
    {
        $list = $this->contest_model->get_datatables_contest($contest_id);
        $data = array();
        $winner_list = $this->common_model->common_multipleSelect_order("tbl_prize", array("contest_id" => $contest_id, "is_deleted" => "0"), "prize_name", "asc");
        $winner_number = $this->common_model->common_multipleSelect_order("tbl_user_contest", array("contest_id" => $contest_id, "is_deleted" => "0"), "winner", "asc");
        $winner_number_list = array("0");
        if (!empty($winner_number)) {
            foreach ($winner_number as $key => $value) {
                // $winner_number_list.push($value['winner']);
                array_push($winner_number_list, $value['winner']);
            }
        }
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $row['username']             = $row_data->username;
                $video = (!empty($row_data->video)) ? '<video width="150px" controls > <source src="' . $row_data->video . '" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']                =  $video;
                $row['insert_datetime']      = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $action = "";
                foreach ($winner_list as $key => $value) {
                    $winner_value = explode('st', $value['prize_name']);
                    if ($row_data->winner == $winner_value[0]) {
                        $action = '<span class="text-success">' . $winner_value[0] . 'st Winner</span>';
                        break;
                    } else {
                        if (!in_array($winner_value[0], $winner_number_list)) {
                            $action .= '<a href="javascript:void(0);" onclick="add_winner(' . $row_data->id . ',' . $winner_value[0] . ');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">' . $winner_value[0] . '</a>';
                        }
                    }
                }
                // if($row_data->winner == '1'){
                //     $action = '<span class="text-success">1st Winner</span>';
                // }else if($row_data->winner == '2'){
                //     $action = '<span class="text-primary">2nd Winner</span>';
                // }else if($row_data->winner == '3'){
                //     $action = '<span class="text-warning">3rd Winner</span>';
                // }else{
                //     $action = '<a href="javascript:void(0);" onclick="add_winner('.$row_data->id.','."1".');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;" data-contestid="1">1</a>';
                //     $action .= ' <a href="javascript:void(0);" onclick="add_winner('.$row_data->id.','."2".');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">2</a>';
                //     $action .= ' <a href="javascript:void(0);" onclick="add_winner('.$row_data->id.','."3".');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">3</a>';
                // }


                $row['action'] = ($action == '') ? "-" : $action;

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->contest_model->count_filtered_contest($contest_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function add_winner($winner_id, $number)
    {
        $this->common_model->common_singleUpdate('tbl_user_contest', array('winner' => $number), array('id' => $winner_id));
        $data = $this->common_model->common_singleSelect('tbl_user_contest', array("id" => $winner_id));
        $prize = $this->common_model->common_singleSelect('tbl_prize', array("prize_name" => $number . 'st Prize'));
        $contest = $this->common_model->common_singleSelect('tbl_master_contest', array("id" => $data['contest_id']));
        $notification_data = array(
            'sender_id'        => '1',
            'sender_type'      => 'admin',
            'receiver_id'      => $data['user_id'],
            'receiver_type'    => 'user',
            'primary_id'       => $data['contest_id'],
            'notification_tag' => "contest_winner",
            'message'          => "You have winner of the " . $contest['name'] . " contest",
            'title'            => '' . $contest['name'] . ' winner'
        );
        $this->common_model->send_notification($notification_data);
        $wallet_data = array(
            "user_id" => $data['user_id'],
            "order_id" => "0",
            "item_id" => $data['contest_id'],
            "total_point" => $prize['credit_points'],
            "redeem_points" => $prize['credit_points'],
            "reward_points" => $prize['credit_points'],
            "item_type" => "contest",
            "status" => "credited"
        );
        $this->common_model->common_insert('tbl_wallet', $wallet_data);
        echo "This user is the winner of the contest";
        die;
    }

    public function create_token($user_name)
    {
        // print_r($user_name);
        $token = $this->common_model->create_token(array("user_name" => $user_name));

        print_r($token);
        die;
    }
    public function user_data($user_id)
    {
        $data = $this->common_model->common_singleSelect("tbl_user", array("id" => $user_id));
        echo json_encode($data);
        die;
    }
    public function check_status($user_id,$contest_id){
         $order_data= $this->common_model->get_order_details('contest', $contest_id, $user_id);
        echo json_encode($order_data);
        die;
    }
    public function send_notification($user_id, $contest_id)
    {
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $notification_data = array(
            'sender_id'        => '1',
            'sender_type'      => 'admin',
            'receiver_id'      => $user_id,
            'receiver_type'    => 'user',
            'primary_id'       => $contest_id,
            'notification_tag' => "speech_evaluations",
            'message'          => 'Admin has removed you from "' . $data['result']['name'] . '"',
            'title'            => 'Remove User'
        );
        echo "<pre>";
        print_r($notification_data);
        $this->common_model->send_notification($notification_data);
        echo "send_notification";
        die;
    }

    public function block_unblock_user($user_id, $contest_id)
    {
        $data['get_user'] = $this->contest_model->get_user_list($user_id, $contest_id, 0);
        $update_data = $this->common_model->common_singleUpdate('tbl_order_detail', array("is_block_contest" => "1"), array("id" => $data['get_user'][0]['order_detail_id']));
        die;
    }
    public function unblock_user($user_id, $contest_id)
    {
        $data['get_user'] = $this->contest_model->get_user_list($user_id, $contest_id, 0);
        $update_data = $this->common_model->common_singleUpdate('tbl_order_detail', array("is_block_contest" => "0"), array("id" => $data['get_user'][0]['order_detail_id']));
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_contest', array('id' => $contest_id));
        $notification_data = array(
            'sender_id'        => '1',
            'sender_type'      => 'admin',
            'receiver_id'      => $user_id,
            'receiver_type'    => 'user',
            'primary_id'       => $contest_id,
            'notification_tag' => "speech_evaluations",
            'message'          => 'Admin has unblock you from "' . $data['result']['name'] . '"',
            'title'            => 'unblock User'
        );
        echo "<pre>";
        print_r($notification_data);
        $this->common_model->send_notification($notification_data);
        echo "send_notification";
        die;
    }
    public function get_block_user_list($contest_id)
    {
        $data['get_user'] = $this->contest_model->get_user_list(0, $contest_id, 1);
        echo json_encode($data['get_user']);
        die;
    }
    public function host_start($contest_id, $flag, $uid)
    {
        if ($flag == 0 || $flag == '0') {
            $this->common_model->common_singleUpdate("tbl_order_detail", array("is_mute_contest" => 0, "is_video_contest" => 0), array("item_id" => $contest_id, "item_type" => "contest"));
            // $this->common_model->common_singleUpdate('tbl_master_contest', array("speaker_id" => 0), array("id" => $contest_id));
        }
        $this->common_model->common_singleUpdate('tbl_master_contest', array("is_host" => $flag, "host_id" => $uid ,"speaker_id" => $uid), array("id" => $contest_id));
        die;
    }
    public function change_flag($type, $flag, $user_id, $contest_id)
    {
        if ($type == 'video') {
            $data = array("is_video_contest" => $flag);
        } else {
            $data = array("is_mute_contest" => $flag);
        }
        $get_user = $this->contest_model->get_user_list($user_id, $contest_id, 0);
        // echo "<pre>";print_r($get_user);
        // print_r($data);
        $this->common_model->common_singleUpdate('tbl_order_detail', $data, array("id" => $get_user[0]['order_detail_id']));
        die;
    }
    public function set_speaker($user_id, $contest_id){
         $this->common_model->common_singleUpdate('tbl_master_contest', array("speaker_id" => $user_id), array("id" => $contest_id));die;
    }
    public function check_user_place($contest_id){
        $contest_data = $this->common_model->common_singleSelect("tbl_master_contest",array("id"=>$contest_id));
        echo json_encode($contest_data);
        die;
    }
}
