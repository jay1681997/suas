<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Evaluation extends MY_Controller {

    private $view_folder = 'authpanel/evaluation/';
    function __construct(){
        parent::__construct();
        //load evaluation model
        $this->load->model('authpanel/evaluation_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load evaluation data
    */
    public function ajax_list(){
        $list = $this->evaluation_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']     =  $i++;
                $row['id']      = $row_data->id;
                $row['name']    = $row_data->name;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-evaluationid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                $action = ' <a href="'.site_url().'authpanel/evaluation/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->evaluation_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the evaluation details
    */
    public function add()
    {  
        $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','name','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

                $evaluationdata = array(
                    'name' => $this->input->post('name'),
                );

                $evaluation_id=$this->common_model->common_insert('tbl_evaluation_factor', $evaluationdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_evaluation_add_success'));
                redirect('authpanel/evaluation');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the evaluation details
    */
    public function edit($evaluation_id)
    {
        $evaluation_id  = base64_decode($evaluation_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_evaluation_factor',array('id'=>$evaluation_id));

        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('name','name','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
               
                $evaluationdata = array(
                    'name' => $this->input->post('name'),
                );
                
                $this->common_model->common_singleUpdate('tbl_evaluation_factor',$evaluationdata,array('id'=>$evaluation_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_evaluation_update_success'));
                redirect('authpanel/evaluation');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for update the status of evaluation
    */
    public function changestatus($evaluation_id, $status){

        $this->common_model->common_singleUpdate('tbl_evaluation_factor',array('status'=>$status),array('id'=>$evaluation_id));
        echo $this->lang->line('adminpanel_message_evaluation_'.$status.'_success');die;
    }

    /*
    ** Function for remove evaluation
    */
    public function removeevaluation($evaluation_id)
    {
        $this->common_model->common_singleUpdate('tbl_evaluation_factor',array('is_deleted'=>'1'),array('id'=>$evaluation_id));
        echo $this->lang->line('adminpanel_message_evaluation_delete_success');die;
    }
    
}
