<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 11-Feb-2022
** Modified On  : -  
*/
class Promocode extends MY_Controller {

    private $view_folder = 'authpanel/promocode/';
    function __construct(){
        parent::__construct();
        //load promocode model
        $this->load->model('authpanel/promocode_model');
    }

    /*
    **Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load promocode data
    */
    public function ajax_list(){
        
        $list = $this->promocode_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['id']               = $row_data->id;
                $promo_image             = '<a href="'.$row_data->promo_image.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->promo_image.'  alt="No image"></a>';
                $row['promo_image']      = $promo_image;
                $row['promocode']        = $row_data->promocode;
                $row['discount_type']    = $row_data->discount_type;
                $row['start_date']       = date('d M Y', strtotime($row_data->start_date));
                $row['end_date']         = date('d M Y', strtotime($row_data->end_date));
                $row['maxusage']         = $row_data->maxusage;
                $row['per_user_usage']   = $row_data->per_user_usage;
                $row['price']            = $row_data->price;
                if ($row_data->discount_type == 'flat') {
                    $row['discount'] = '$'.$row_data->discount;
                } else {
                    $row['discount'] = $row_data->discount."%";
                }
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-promocodeid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                //View page Link prepared
                $action = '<a href="'.site_url().'authpanel/promocode/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                //Edit page Link prepared
                $action .= ' <a href="'.site_url().'authpanel/promocode/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';
                $row['action'] = $action;

                $row['inserted_date'] = $this->common_model->date_convert($row_data->inserted_date, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->promocode_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the promocode details
    */
    public function add()
    {
       $data['user_list'] = $this->common_model->common_multipleSelect("tbl_user",array("is_deleted"=>"0","status"=>"active"));
        if($this->input->post()){
            $this->form_validation->set_rules('promocode','Promocode','required|is_unique[tbl_promocode.is_deleted="0" AND promocode=]|trim');
            $this->form_validation->set_rules('discount_type','Discount Type','required|trim');
            $this->form_validation->set_rules('discount','Discount','required|trim');
            $this->form_validation->set_rules('start_date','Start Date','required|trim');
            $this->form_validation->set_rules('end_date','End Date','required|trim');
            $this->form_validation->set_rules('maxusage','Max Usage','required|trim');
            $this->form_validation->set_rules('per_user_usage','Per User Use','required|trim');
            $this->form_validation->set_rules('description','Description','trim');
            $this->form_validation->set_rules('price','Price','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if($this->form_validation->run()) {

                $promo_image = 'default.png';
                if (!empty($_FILES['promo_image']) && $_FILES['promo_image']['size'] > 0) {
                    $promo_image = $this->common_model->uploadImageS3($_FILES['promo_image'],PROMOCODE_IMAGE);
                    if(!$promo_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $promocodedata  = array(
                    'promocode'      => strtoupper($this->input->post('promocode')),
                    'discount_type'  => $this->input->post('discount_type'),
                    'discount'       => $this->input->post('discount'),
                    'start_date'     => $this->input->post('start_date'),
                    'end_date'       => $this->input->post('end_date'),
                    'maxusage'       => $this->input->post('maxusage'),
                    'per_user_usage' => $this->input->post('per_user_usage'),
                    'price'          => $this->input->post('price'),
                    'promocode_image'=> $promo_image,
                    'description'    => !empty($this->input->post('description')) ? $this->input->post('description') : "",
                    "type" => $this->input->post('type'),
                );
                $promocode_id = $this->common_model->common_insert('tbl_promocode',$promocodedata);

                if(!empty($this->input->post('benefits')) && array_filter($this->input->post('benefits'))){
                    foreach ($this->input->post('benefits') as $key => $value) {
                        $this->common_model->common_insert('tbl_promocode_benefit',array('promocode_id'=>$promocode_id, 'benefits'=>$value));
                    }
                }
                if(!empty($this->input->post('user_ids'))  && $this->input->post('type') == '1'){
                    foreach ($this->input->post('user_ids') as $key1 => $value1) {
                        $this->common_model->common_insert("tbl_user_promocode",array("user_id"=>$value1,"promocode_id"=>$promocode_id));
                    }
                }
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_promocode_add_success'));
                redirect('authpanel/promocode');
            } else {
                $this->load->view($this->view_folder.'add',$data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the promocode details
    */
    public function edit($promocode_id)
    {
        $promocode_id          = base64_decode($promocode_id);
        $data['result']        = $this->common_model->common_singleSelect('tbl_promocode',array('id'=>$promocode_id));
        $data['benfits']       = $this->common_model->common_multipleSelect('tbl_promocode_benefit',array('promocode_id'=>$promocode_id,"is_deleted"=>"0"));
         $data['user_list'] = $this->common_model->common_multipleSelect("tbl_user",array("is_deleted"=>"0","status"=>"active"));
         $user_promocode = $this->common_model->common_multipleSelect('tbl_user_promocode',array("is_deleted"=>"0","status"=>"active","promocode_id"=>$promocode_id));
         $p_user = array();
         if(!empty($user_promocode)){
            foreach ($user_promocode as $key => $value) {
                array_push($p_user,$value['user_id']);
            }
            $data['promocode_user_list'] = $p_user;
         }else{
            $data['promocode_user_list'] = array();
         }
         // echo "<pre>";print_r($data['result']);print_r(explode(',', $data['result']['user_ids']));die;
        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('promocode','Promocode','required|trim|is_unique[tbl_promocode.id != '.$promocode_id.' AND is_deleted="0" AND promocode=]');
            $this->form_validation->set_rules('discount_type','Discount Type','required|trim');
            $this->form_validation->set_rules('discount','Discount','required|trim');
            $this->form_validation->set_rules('start_date','Start Date','required|trim');
            $this->form_validation->set_rules('end_date','End Date','required|trim');
            $this->form_validation->set_rules('maxusage','Max Usage','required|trim');
            $this->form_validation->set_rules('per_user_usage','Per User Use','required|trim');
            $this->form_validation->set_rules('description','Description','trim');
            $this->form_validation->set_rules('price','Price','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) 
            {
                $promo_image = @$data['result']['promocode_image'];
                if (!empty($_FILES['promo_image']) && $_FILES['promo_image']['size'] > 0) {
                    $promo_image = $this->common_model->uploadImageS3($_FILES['promo_image'],PROMOCODE_IMAGE);
                    if(!$promo_image) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }
      
                $promocodedata = array(
                    'promocode'      => strtoupper($this->input->post('promocode')),
                    'discount_type'  => $this->input->post('discount_type'),
                    'discount'       => $this->input->post('discount'),
                    'start_date'     => $this->input->post('start_date'),
                    'end_date'       => $this->input->post('end_date'),
                    'maxusage'       => $this->input->post('maxusage'),
                    'per_user_usage' => $this->input->post('per_user_usage'),
                    'price'          => $this->input->post('price'),
                    'promocode_image'=> $promo_image,
                    'description'    => !empty($this->input->post('description')) ? $this->input->post('description') : "",
                        "type" => $this->input->post('type'),
                );
                $this->promocode_model->update_promocode($promocode_id, $promocodedata);
                // echo "<pre>";print_r($this->input->post());die;
                if(!empty($this->input->post('benefits')) && array_filter($this->input->post('benefits'))){
                    $this->common_model->common_singleUpdate("tbl_promocode_benefit",array("is_deleted"=>"1"),array("promocode_id"=>$promocode_id));
                    foreach ($this->input->post('benefits') as $key => $value) {
                        if($value != ''){
                              $this->common_model->common_insert('tbl_promocode_benefit',array('promocode_id'=>$promocode_id, 'benefits'=>$value));
                        }
                      
                    }
                }
                $this->common_model->common_delete("tbl_user_promocode",array("promocode_id"=>$promocode_id));
                if(!empty($this->input->post('user_ids')) && $this->input->post('type') == '1'){
                    foreach ($this->input->post('user_ids') as $key1 => $value1) {
                        $this->common_model->common_insert("tbl_user_promocode",array("user_id"=>$value1,"promocode_id"=>$promocode_id));
                    }
                }
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_promocode_update_success'));
                redirect('authpanel/promocode');   
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the Promocode view page
    */
    public function view($promocode_id){
        $promocode_id     = base64_decode($promocode_id);
        $data['result']   = $this->promocode_model->GetPromocodeDetails($promocode_id);
        $data['benefits'] = $this->common_model->common_multipleSelect('tbl_promocode_benefit', array('promocode_id'=>$promocode_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of promocode
    */
    public function changestatus($promocode_id, $status){

        $this->common_model->common_singleUpdate('tbl_promocode',array('status'=>$status),array('id'=>$promocode_id));
        echo $this->lang->line('adminpanel_message_promocode_'.$status.'_success');die;
    }

    /*
    ** Function for remove promocode
    */
    public function removepromocode($promocode_id)
    {
        $this->common_model->common_delete('tbl_promocode_benefit',array('promocode_id'=>$promocode_id));
        $this->common_model->common_singleUpdate('tbl_promocode',array('is_deleted'=>'1'),array('id'=>$promocode_id));
        echo $this->lang->line('adminpanel_message_promocode_delete_success');die;
    }

}
