<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Scorecard extends MY_Controller {

    private $view_folder = 'authpanel/scorecard/';
    function __construct(){
        parent::__construct();
        //load scorecard model
        $this->load->model('authpanel/scorecard_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load scorecard data
    */
    public function ajax_list(){
        $list = $this->scorecard_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $row['title']                = $row_data->title;
                // $row['description']          = $row_data->description;
                // $row['contest_name']         = $row_data->contest_name;
                $row['points']              = $row_data->points;
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-scorecardid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/scorecard/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/scorecard/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->scorecard_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the scorecard details
    */
    public function add()
    {  
        // $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));
        if($this->input->post())
        {
            $this->form_validation->set_rules('title','title','required|trim');
            $this->form_validation->set_rules('points','Points','required|trim');
            // $this->form_validation->set_rules('contest_id','Contest ID','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

                $scorecarddata = array(
                    'title'             => $this->input->post('title'),
                    'points'       => $this->input->post('points'),    
                    // 'contest_id'        => $this->input->post('contest_id'),
                );

                $scorecard_id=$this->common_model->common_insert('tbl_contest_scorecard', $scorecarddata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_scorecard_add_success'));
                redirect('authpanel/scorecard');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the scorecard details
    */
    public function edit($scorecard_id)
    {
        $scorecard_id     = base64_decode($scorecard_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_contest_scorecard',array('id'=>$scorecard_id));
        // $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));

        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('title','title','required|trim');
            $this->form_validation->set_rules('points','points','required|trim');
            // $this->form_validation->set_rules('contest_id','Contest ID','required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
               
                $scorecarddata = array(
                    'title'             => $this->input->post('title'),
                    'points'       => $this->input->post('points'),    
                    // 'contest_id'        => $this->input->post('contest_id'),
                );
                
                $this->common_model->common_singleUpdate('tbl_contest_scorecard',$scorecarddata,array('id'=>$scorecard_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_scorecard_update_success'));
                redirect('authpanel/scorecard');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for update the status of scorecard
    */
    public function changestatus($scorecard_id, $status){

        $this->common_model->common_singleUpdate('tbl_contest_scorecard',array('status'=>$status),array('id'=>$scorecard_id));
        echo $this->lang->line('adminpanel_message_scorecard_'.$status.'_success');die;
    }

    /*
    ** Function for remove scorecard
    */
    public function removescorecard($scorecard_id)
    {
        $this->common_model->common_singleUpdate('tbl_contest_scorecard',array('is_deleted'=>'1'),array('id'=>$scorecard_id));
        echo $this->lang->line('adminpanel_message_scorecard_delete_success');die;
    }
    
}
