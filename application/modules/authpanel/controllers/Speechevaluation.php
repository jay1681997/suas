<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Speechevaluation extends MY_Controller {

    private $view_folder = 'authpanel/speechevaluation/';
    function __construct(){
        parent::__construct();
        //load speechevaluation model
        $this->load->model('authpanel/speechevaluation_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load speechevaluation data
    */
    public function ajax_list(){
        $list = $this->speechevaluation_model->get_datatables();
        $data = array();
        
        // echo"<pre>";print_r($list);
        // die;
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $video = (!empty($row_data->video)) ? '<video width="150px" controls > <source src="'. $row_data->video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']               = $video;
                $row['username']            = $row_data->username;
                $row['student_name']        = $row_data->student_name;
                $row['title']               = $row_data->title;
                $row['description']         = $row_data->description;
                $row['uploading_type']      = $row_data->uploading_type;
                if($row_data->is_request == '1'){
                    $row['is_request'] = '<a href="'.site_url().'authpanel/speechevaluation/edit/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;">Request</a>';
                }
                $row['subscription_name'] = ($row_data->subscription_name == '')?'-':$row_data->subscription_name;
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-speechevaluationid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                //Check status
                if($row_data->is_evaluated == 'yes'){
                    $row['is_evaluated'] = '<span class="text-success">Yes</span>';
                } else {
                    $row['is_evaluated'] = '<span class="text-danger">No</span>';
                }
                



                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/speechevaluation/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/speechevaluation/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->speechevaluation_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function removespeechevaluation($speechevaluation_id){
     //   echo $speechevaluation_id;
    $this->common_model->common_singleUpdate('tbl_user_evaluation',array("is_deleted"=>"1"), array('id'=>$speechevaluation_id));
      echo $this->lang->line('rest_keywords_speechevalution_remove_success');die;
    }
    /*
    ** Function for edit the speechevaluation details
    */
    public function edit($speechevaluation_id)
    {

        $speechevaluation_id = base64_decode($speechevaluation_id);

        $data['result']      = $this->common_model->common_singleSelect('tbl_user_evaluation',array('id'=>$speechevaluation_id));
        $data['result_factor']      = $this->common_model->common_singleSelect('tbl_user_evaluation',array('id'=>$speechevaluation_id));
                       $notification_data = array(
                '_video'            => S3_BUCKET_ROOT.SPEECH_IMAGE.$data['result']['video']
            );
        // if($data['result']['uploading_type'] == 'AI'){

        // }
        //   $data['api_result']   = $this->common_model->Python_api($notification_data);

        // echo "<pre>";
        //  print_r($speechevaluation_id);
        //  print_r($data);
        // die;
        if($this->input->post() && !empty($data['result']))
        {
            $evaluation = array(
                'pace_result'               => $this->input->post('pace_result'),
                'pace_description'          => $this->input->post('pace_description'),
                'energy_result'             => $this->input->post('energy_result'),
                'energy_description'        => $this->input->post('energy_description'),
                'fillerword_result'         => $this->input->post('fillerword_result'),
                'fillerword_description'    => $this->input->post('fillerword_description'),
                'conciseness_result'        => $this->input->post('conciseness_result'),
                'conciseness_description'   => $this->input->post('conciseness_description'),
                'eyecontact_result'         => $this->input->post('eyecontact_result'),
                'eyecontact_description'    => $this->input->post('eyecontact_description'),
                //'speech_id'                 => $speechevaluation_id,
                'is_request'                => '0'
            );
               $notification_data = array(
                'sender_id'        => '1',
                'sender_type'      => 'admin',
                'receiver_id'      => $data['result']['user_id'],
                'receiver_type'    => 'user',
                'primary_id'       => $speechevaluation_id,
                'notification_tag' => "speech_evaluations",
                'message'          => 'Your "'.$data['result']['title'].'" speech evaluation is completed',
                'title'            => 'Speech Evaluations'
            );
               // echo "<pre>";
         // print_r($speechevaluation_id);
       
            $this->common_model->send_notification($notification_data);
            // $this->common_model->common_insert('tbl_user_evaluation',$evaluation);
            $this->common_model->common_singleUpdate('tbl_user_evaluation',$evaluation, array('id'=>$data['result_factor']['id']));
            $this->common_model->common_singleUpdate('tbl_user_speech',array('is_evaluated'=>'yes'), array('id'=>$data['result']['speech_id']));

  // print_r($data);
  //        print_r($evaluation);
  //       die;
            $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_speechevaluation_update_success'));
            redirect('authpanel/speechevaluation');  
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }
    
}
