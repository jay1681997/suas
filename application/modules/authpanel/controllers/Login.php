<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 10-Feb-2022
** Modified On  : -  
*/
class Login extends MX_Controller {

    private $view_folder = 'authpanel/login/';
    /*
    *Default constructor
    */
	function __construct()
    { 
    	parent::__construct();
    }

    //load a login page
    function index()
	{   
        if($this->session->userdata(ADMIN_SESSION_NAME)){
            redirect('authpanel/dashboard','refresh');
        }
        $this->load->view($this->view_folder.'login');
	}

    //check login details
	function checklogin()
	{  
		if($this->session->userdata(ADMIN_SESSION_NAME)){
            redirect('authpanel/dashboard');
        }
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {
            $result = $this->common_model->common_singleSelect('tbl_admin', array('email'=>$this->input->post('email'),'status'=>'Active'));
            if(!empty($result)) {
                
                if ($this->common_model->decrypt_password($result['password'])==$this->input->post('password')) {
                    $this->session->set_userdata(ADMIN_SESSION_NAME, $result);
                    
                    $this->session->set_userdata(ADMIN_LOCK_NAME,'1');
                    $this->session->set_userdata(ADMIN_TIMEZONE,$this->input->post('timezone'));  
                   
                    $this->common_model->common_singleUpdate('tbl_admin', array("login_status" => 'Online', 'last_login' => date('Y-m-d H:i:s')), array('id'=>$result['id']));
                    redirect("authpanel/dashboard"); 
                } else {
                    $this->session->set_flashdata('error_msg',$this->lang->line('adminpanel_message_invalid_password'));
                    redirect(base_url().'authpanel/login');    
                }  
            } else {
                $this->session->set_flashdata('error_msg',$this->lang->line('adminpanel_message_invalid_email'));
                redirect(base_url().'authpanel/login');
            }
        } else {
            $this->load->view('authpanel/login');
        }
    }
    
    //locak screen load page
    function lock_screen()
    {
        $this->session->set_userdata(ADMIN_LOCK_NAME,'0');
        $this->load->view($this->view_folder.'lock_screen');
    } 

    //unlock admin panel
    function unlock()
    {
        $admin_session = $this->session->userdata(ADMIN_SESSION_NAME);

        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run()) {   
            $result = $this->common_model->common_singleSelect('tbl_admin', array('email'=>$admin_session['email'], 'status'=>'Active'));
            
            if(!empty($result) && $this->common_model->decrypt_password($result['password'])==$this->input->post('password')) {
                $this->session->set_userdata(ADMIN_LOCK_NAME,'1');
                redirect("authpanel/dashboard");
            } else {
                $this->session->set_flashdata('error_msg',$this->lang->line('adminpanel_message_invalid_password'));
                redirect(base_url().'authpanel/login/lock_screen');
            }
        } else {
            $this->load->view($this->view_folder.'lock_screen');
        }
    } 
    
    //logout super admin admin 
    function logout()
    {
        $this->session->unset_userdata(ADMIN_SESSION_NAME);
        $this->session->unset_userdata(ADMIN_LOCK_NAME);
        $this->session->unset_userdata(ADMIN_TIMEZONE);
        redirect('authpanel/login');
    }
}