<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 14-March-2022
** Modified On  : -  
*/
class App_content extends MY_Controller {

    /*
    * Default constructor
    */
    private $viewfolder = 'authpanel/app_content/';   
	function __construct()
    { 
    	parent::__construct();
        $this->load->model('authpanel/content_model');
    }

    /*
    * load a about us page
    */
    public function index() {
        redirect('authpanel/app_content/about_us');
    }

    /*
    * load a about us page
    */
    public function about_us() {

        $data['app_content']=$this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'about_us',$data);
    }

    /*
    * update a about us content
    */
    public function aboutus_update() {

        $data['app_content'] = $this->content_model->get_app_content();
        $Allpostdata         = $this->input->post();
        if($Allpostdata) {

            $this->form_validation->set_rules('about_us','About Us Content', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run()) {

                $param = array(
                    'about_us' => $this->input->post('about_us')
                );
                $this->session->set_flashdata("success_msg",$this->lang->line('authpanel_aboutus_update_success'));
                $this->content_model->save_app_content($param);

            } else {
                $this->load->view($this->viewfolder.'aboutus_update',$data);
            }
            redirect('authpanel/app_content/about_us');

        } else {
            $this->load->view($this->viewfolder.'aboutus_update',$data);
        }
    }

    /*
    * load a privacy policy us page
    */
    public function privacy_policy() {

        $data['app_content'] = $this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'privacy_policy',$data);
    }

    /*
    * update privacy policy content
    */
    public function privacypolicy_update() {

        $data['app_content'] = $this->content_model->get_app_content();
        $Allpostdata         = $this->input->post();
        if($Allpostdata) {

            $this->form_validation->set_rules('privacy_policy','Privacy Policy Content', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run()) {

                $param = array(
                    'privacy_policy' => $this->input->post('privacy_policy')
                );
                $this->session->set_flashdata("success_msg",$this->lang->line('authpanel_privacy_update_success'));
                $this->content_model->save_app_content($param);
            } else {
                $this->load->view($this->viewfolder.'privacypolicy_update',$data);
            }
            redirect('authpanel/app_content/privacy_policy');
        } else {

            $this->load->view($this->viewfolder.'privacypolicy_update',$data);
        }
    }

    /*
    * load a terms condition us page
    */
    public function terms_condition() {
        
        $data['app_content'] = $this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'terms_condition',$data);
    }

    /*
    * update terms condition content
    */
    public function termscondition_update() {

        $data['app_content'] = $this->content_model->get_app_content();
        $Allpostdata         = $this->input->post();
        if($Allpostdata) {

            $this->form_validation->set_rules('terms_and_condition','Terms & Condition', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run()) {

                $param = array(
                    'terms_and_condition' => $this->input->post('terms_and_condition')
                );
                $this->session->set_flashdata("success_msg",$this->lang->line('authpanel_terms_update_success'));
                $this->content_model->save_app_content($param);
            } else {
                $this->load->view($this->viewfolder.'termscondition_update',$data);
            }
            redirect('authpanel/app_content/terms_condition');

        } else {
            $this->load->view($this->viewfolder.'termscondition_update',$data);
        }
    }

    /*
    * load a terms condition us page
    */
    public function date_of_birth() {
        
        $data['app_content'] = $this->content_model->get_app_content();
        $this->load->view($this->viewfolder.'date_of_birth',$data);
    }

    /*
    * update terms condition content
    */
    public function dateofbirth_update() {

        $data['app_content'] = $this->content_model->get_app_content();
        $Allpostdata         = $this->input->post();
        if($Allpostdata) {

            $this->form_validation->set_rules('date_of_birth','Date of Birth', 'trim|required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run()) {

                $param = array(
                    'date_of_birth' => $this->input->post('date_of_birth')
                );
                $this->session->set_flashdata("success_msg",$this->lang->line('authpanel_terms_update_success'));
                $this->content_model->save_app_content($param);
            } else {
                $this->load->view($this->viewfolder.'dateofbirth_update',$data);
            }
            redirect('authpanel/app_content/date_of_birth');

        } else {
            $this->load->view($this->viewfolder.'dateofbirth_update',$data);
        }
    }
}