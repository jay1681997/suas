<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 14-March-2022
** Modified On  : -  
*/
class Faq extends MY_Controller {

    private $view_folder = 'authpanel/faq/';
    function __construct(){
        parent::__construct();
        //load faq model
        $this->load->model('authpanel/faq_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load faq data
    */
    public function ajax_list()
    {
        $list = $this->faq_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['id']       = $row_data->id;
                $row['question'] = $row_data->question;
                $row['answer']   = $row_data->answer;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status  = '<div class="switch"><label><input type="checkbox" name="changestatus" data-faqid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/faq/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';
                $row['action'] = $action;
                $row['inserted_date'] = $this->common_model->date_convert($row_data->inserted_date, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->faq_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the faq details
    */
    public function add()
    {    
        if($this->input->post())
        { 
            $this->form_validation->set_rules('question','Question','required|trim');
            $this->form_validation->set_rules('answer',"Answer",'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if($this->form_validation->run()) {
                
                $faqdata = array(
                    'question'        => $this->input->post('question'),
                    'answer'          => $this->input->post('answer')
                );
              
                $faq_id=$this->common_model->common_insert('tbl_faq',$faqdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_faqadd_success'));
                redirect('authpanel/faq');
            } else {
              
                $this->load->view($this->view_folder.'add');    
            }
        } else {
         
            $this->load->view($this->view_folder.'add');
        }
    }

    /*
    ** Function for edit the faq details
    */
    public function edit($faq_id)
    {
        $faq_id = base64_decode($faq_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_faq',array('id'=>$faq_id));
        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('question','Question','required|trim');
            $this->form_validation->set_rules('answer',"Answer",'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {   
                $faqdata = array(
                    'question'        => $this->input->post('question'),
                    'answer'          => $this->input->post('answer')
                );
                $this->common_model->common_singleUpdate('tbl_faq',$faqdata,array('id'=>$faq_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_faq_update_success'));
                redirect('authpanel/faq');   
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for update the status of faq
    */
    public function changestatus($faq_id,$status){
        $this->common_model->common_singleUpdate('tbl_faq',array('status'=>$status),array('id'=>$faq_id));
        echo $this->lang->line('adminpanel_message_faq_'.$status.'_success');die;
    }

    /*
    ** Function for remove faq
    */
    public function remove($faq_id) {
        $this->common_model->common_singleUpdate('tbl_faq',array('is_deleted'=>'1'),array('id'=>$faq_id));
        echo $this->lang->line('adminpanel_message_faqdelete_success');die;
    }

    /*
    ** Function for export the data
    */
    public function export(){
        $data['data_list'] = $this->faq_model->export();
        if(!empty($data['data_list'])){
            $this->load->view($this->view_folder.'report', $data);
        }else{
            $this->session->set_flashdata('error_msg', 'There is no data for export.');
            redirect('authpanel/faq/listing'.base64_encode($this->input->post('id')));
        }
    }
}
