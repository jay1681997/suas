<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 04-Feb-2022
** Modified On  : -  
*/
class Notification extends MY_Controller {

    /*
    ** Default constructor
    */
    private $view_folder = 'authpanel/notification/';   
    function __construct()
    { 
        parent::__construct();
        $this->load->model('authpanel/common_model');
    }
    /*
    * index method 
    */
    public function index(){
        $this->session->unset_userdata('users');                              
        $data["users"]   = $this->common_model->getUserList();   
        $this->load->view($this->view_folder.'notification', $data);  
    }
    
    /*
    ** Function to send notification
    */
    public function send_push()
    {
        $data["users"] = $this->common_model->getUserList();   
        if($this->input->post())
        {
            $this->form_validation->set_rules('message',' Message','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if($this->form_validation->run()) {
              
                $users_id = $this->input->post('users');
                foreach ($users_id as $key => $value) {
                    $notification_data = array(
                        'sender_id'        => '1',
                        'sender_type'      => 'admin',
                        'receiver_id'      => $value,
                        'receiver_type'    => 'user',
                        'primary_id'       => '0',
                        'notification_tag' => "admin_push",
                        'message'          => $this->input->post('message'),
                        'title'            => 'Admin Notification'
                    );
                    $this->common_model->send_notification($notification_data);
                }

                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_send_push_success'));
                redirect('authpanel/notification');
            
            } else {
                $this->load->view($this->view_folder.'notification', $data);
            }
        } else {
            $this->load->view($this->view_folder.'notification', $data);
        }
    }

   

}