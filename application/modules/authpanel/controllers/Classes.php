<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAs
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Classes extends MY_Controller
{

    private $view_folder = 'authpanel/classes/';
    function __construct()
    {
        parent::__construct();
        //load classes model
        $this->load->model('authpanel/classes_model');
    }

    /*
    *Load index page
    */
    public function index()
    {
        $this->load->view($this->view_folder . 'listing');
    }

    /*
    ** Function for load classes data
    */
    public function ajax_list()
    {
        $list = $this->classes_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = '<a href="' . $row_data->class_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->class_image . '  alt="No image"></a>';
                $row['class_image']          = $image;
                $row['program_title']        = $row_data->program_title;
                $row['description']          = $row_data->description;
                $start_datetime = $this->common_model->date_convert($row_data->start_datetime, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $end_datetime = $this->common_model->date_convert($row_data->end_datetime, 'd M Y H:i ', $this->session->userdata(ADMIN_TIMEZONE));
                $row['start_datetime']       = $start_datetime;
                $row['end_datetime']         = $end_datetime;

                $row['grade']                = $row_data->grade;
                $row['total_point']          = $row_data->total_point;
                $row['price']                = $row_data->currency . '' . $row_data->price;
                $row['age_category']         = $row_data->age_category;
                $row['total_spot']           = $row_data->total_spot;
                $row['subadmin_name']        = $row_data->subadmin_name;
                $myDate = date("d-m-y h:i");
                $days_list = explode(":", $row_data->dtime);
                if ($row_data->between_flag == 1) {
                    $row['hosting']              =  '<a href="' . site_url() . 'authpanel/classes/agora_hosting/' . base64_encode($row_data->id) . '"class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>';
                } else if ($row_data->date_flag == 1) {
                    $row['hosting']              = '<a href="" class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Upcoming Contest</a>';
                } else {
                    // $row['hosting']              =  '<a class="btn btn-primary waves-effect waves-float waves-green" style="margin-bottom:5px;">Join as host</a>'; 
                    $row['hosting']              =  "-";
                }
                if ($row_data->status == 'Active') {
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status = '<div class="switch"><label><input type="checkbox" name="changestatus" data-classid="' . $row_data->id . '" data-status="' . $row_data->status . '" ' . $checkstete . '><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                $action = '<a href="' . site_url() . 'authpanel/classes/view/' . base64_encode($row_data->id) . '"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action .= ' <a href="' . site_url() . 'authpanel/classes/edit/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove(' . $row_data->id . ');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';
                $action .= ' <a href="' . site_url() . 'authpanel/classes/booking_list/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-menu"></i></a>';
                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->classes_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the classes details
    */
    public function add()
    {
        $data['subadmins']  = $this->common_model->common_multipleSelect('tbl_admin', array('is_deleted' => '0', 'status' => 'Active', 'role' => 'Sub'));
        if ($this->input->post()) {
            $this->form_validation->set_rules('program_title', 'Program Title', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            // $this->form_validation->set_rules('grade','Grade','required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('start_datetime', 'Classes Start Datetime', 'required|trim');
            $this->form_validation->set_rules('end_datetime', 'Classes End Datetime', 'required|trim');
            $this->form_validation->set_rules('age_category', 'Age category', 'required|trim');
            $this->form_validation->set_rules('total_spot', 'Total Spot', 'required|trim');
            // $this->form_validation->set_rules('subadmin_id','Sub Admin','required|trim');
            // $this->form_validation->set_error_delimiters('<div classes="error" style="color:#F44336;">','</div>');
            // if($this->input->post('age_category') != 'Adults'){
            //     $this->form_validation->set_rules('grade','Grade','required|trim');
            // }
            if ($this->form_validation->run()) {

                $class_image = "default.png";
                if (!empty($_FILES['class_image']) && $_FILES['class_image']['size'] > 0) {
                    $class_image = $this->common_model->uploadImageS3($_FILES['class_image'], CLASS_IMAGE);
                    if (!$class_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'add', $data);
                        return false;
                    }
                }
                $start_datetime = $this->common_model->convertTimetoUTC($this->input->post('start_datetime'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $end_datetime = $this->common_model->convertTimetoUTC($this->input->post('end_datetime'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $classesdata = array(
                    'program_title'      => $this->input->post('program_title'),
                    'class_image'        => $class_image,
                    'description'        => $this->input->post('description'),
                    'price'              => $this->input->post('price'),
                    // 'start_datetime'     => date('Y-m-d H:i', strtotime($this->input->post('start_datetime'))),
                    // 'end_datetime'       => date('Y-m-d H:i', strtotime($this->input->post('end_datetime'))),
                    'start_datetime'        => $start_datetime,
                    'end_datetime'          => $end_datetime,
                    // 'grade'              => $this->input->post('grade'),
                    'total_point'        => $this->input->post('total_point'),
                    'age_category'       => $this->input->post('age_category'),
                    'total_spot'         => $this->input->post('total_spot'),
                    'channel_name'       => 'CLASS' . random_string('numeric', 8),
                    'subadmin_id'        => $this->input->post('subadmin_id'),
                );

                $classes_id = $this->common_model->common_insert('tbl_master_class', $classesdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_class_add_success'));
                redirect('authpanel/classes');
            } else {
                $this->load->view($this->view_folder . 'add', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'add', $data);
        }
    }

    public function agora_hosting($class_id)
    {
        $classes_id         = base64_decode($class_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $agora_token        = $this->common_model->get_agora_token($classes_id, $data['result']['subadmin_id'], 'class');
        $data['token']      = $agora_token['data']['token'];
        $data['channel']    = $agora_token['data']['channelname'];
        $data['userid']     = $data['result']['subadmin_id'];
        $data['classes_id'] = $classes_id;
        $data['id']         = rand();

        // $users = $this->common_model->getUserList(); 
        $users = $this->common_model->get_contest_list('class', $classes_id);

        foreach ($users as $key => $value) {
            $notification_data = array(
                'sender_id'        => '1',
                'sender_type'      => 'admin',
                'receiver_id'      => $value['id'],
                'receiver_type'    => 'user',
                'primary_id'       => $classes_id,
                'notification_tag' => "class_started",
                'message'          => 'The "' . $data['result']['program_title'] . '" class starts now. Please join the class.',
                'title'            => 'Class Started'
            );
            $this->common_model->send_notification($notification_data);
        }
        $this->load->view($this->view_folder . 'agora', $data);
    }

    /*
    ** Function for edit the classes details
    */
    public function edit($classes_id)
    {
        $classes_id     = base64_decode($classes_id);
        $data['subadmins']  = $this->common_model->common_multipleSelect('tbl_admin', array('is_deleted' => '0', 'status' => 'Active', 'role' => 'Sub'));
        $data['result'] = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $start_datetime = $this->common_model->date_convert($data['result']['start_datetime'], 'd M Y H:i', $this->session->userdata(ADMIN_TIMEZONE));
        $end_datetime = $this->common_model->date_convert($data['result']['end_datetime'], 'd M Y H:i', $this->session->userdata(ADMIN_TIMEZONE));
        $data['result']['start_datetime'] = $start_datetime;
        $data['result']['end_datetime'] = $end_datetime;
        if ($this->input->post() && !empty($data['result'])) {
            $this->form_validation->set_rules('program_title', 'Program Title', 'required|trim');
            $this->form_validation->set_rules('description', 'Description', 'required|trim');
            // $this->form_validation->set_rules('grade','Grade','required|trim');
            $this->form_validation->set_rules('total_point', 'Total Point', 'required|trim');
            $this->form_validation->set_rules('price', 'Price', 'required|trim');
            $this->form_validation->set_rules('start_datetime', 'Classes Start Datetime', 'required|trim');
            $this->form_validation->set_rules('end_datetime', 'Classes End Datetime', 'required|trim');
            $this->form_validation->set_rules('age_category', 'Age category', 'required|trim');
            $this->form_validation->set_rules('total_spot', 'Total Spot', 'required|trim');
            $this->form_validation->set_error_delimiters('<div classes="error" style="color:#F44336;">', '</div>');
         
            if ($this->form_validation->run()) {

                $class_image = @$data['result']['class_image'];
                if (!empty($_FILES['class_image']) && $_FILES['class_image']['size'] > 0) {
                    $class_image = $this->common_model->uploadImageS3($_FILES['class_image'], CLASS_IMAGE);
                    if (!$class_image) {
                        $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder . 'edit', $data);
                        return false;
                    }
                }

                $start_datetime = $this->common_model->convertTimetoUTC($this->input->post('start_datetime'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $end_datetime = $this->common_model->convertTimetoUTC($this->input->post('end_datetime'), 'Y-m-d H:i', $this->session->userdata(ADMIN_TIMEZONE));
                $classesdata = array(
                    'program_title'         => $this->input->post('program_title'),
                    'class_image'           => $class_image,
                    'description'           => $this->input->post('description'),
                    'price'                 => $this->input->post('price'),
                    'start_datetime'        => $start_datetime,
                    'end_datetime'          => $end_datetime,
                    'total_point'           => $this->input->post('total_point'),
                    'age_category'          => $this->input->post('age_category'),
                    'total_spot'            => $this->input->post('total_spot'),
                    'subadmin_id'           => $this->input->post('subadmin_id'),
                );
                if (@$data['result']['start_datetime'] == date('Y-m-d H:i', strtotime($this->input->post('start_datetime')))) {
                }
                $this->common_model->common_singleUpdate('tbl_master_class', $classesdata, array('id' => $classes_id));
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_class_update_success'));
                redirect('authpanel/classes');
            } else {
                $this->load->view($this->view_folder . 'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder . 'edit', $data);
        }
    }

    /*
    ** Function for load the classes view page
    */
    public function view($classes_id)
    {
        $classes_id          = base64_decode($classes_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $start_datetime = $this->common_model->date_convert($data['result']['start_datetime'], 'd M Y H:i A', $this->session->userdata(ADMIN_TIMEZONE));
        $end_datetime = $this->common_model->date_convert($data['result']['end_datetime'], 'd M Y H:i A', $this->session->userdata(ADMIN_TIMEZONE));
        $data['result']['start_datetime'] = $start_datetime;
        $data['result']['end_datetime'] = $end_datetime;
        // $data['']
        // echo "<pre>";
        // print_r($data);
        // die;
        $this->load->view($this->view_folder . 'view', $data);
    }

    /*
    ** Function for update the status of classes
    */
    public function changestatus($classes_id, $status)
    {

        $this->common_model->common_singleUpdate('tbl_master_class', array('status' => $status), array('id' => $classes_id));
        echo $this->lang->line('adminpanel_message_class_' . $status . '_success');
        die;
    }

    /*
    ** Function for remove classes
    */
    public function removeclasses($classes_id)
    {
        $this->common_model->common_singleUpdate('tbl_master_class', array('is_deleted' => '1'), array('id' => $classes_id));
        echo $this->lang->line('adminpanel_message_class_delete_success');
        die;
    }


    public function enrolled_classlist()
    {
        $this->load->view($this->view_folder . 'enrolled_listing');
    }


    public function ajax_list_enrolled()
    {
        $list = $this->classes_model->get_datatables_enrolled();
        $data = array();
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = '<a href="' . $row_data->profile_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->profile_image . '  alt="No image"></a>';
                $row['profile_image']        = $image;
                $row['program_title']        = $row_data->program_title;
                $row['username']             = $row_data->username;
                $row['age_category']         = $row_data->age_category;


                if ($row_data->status == 'Confirm') {
                    $row['status'] = '<span class="text-success">' . $row_data->status . '</span>';
                } else if ($row_data->status == 'Rejected') {
                    $row['status'] = '<span class="text-danger">' . $row_data->status . '</span>';
                } else {
                    $row['status'] = '<span class="text-primary">' . $row_data->status . '</span>';
                }

                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->classes_model->count_filtered_enrolled(),
            "rows"  => $data,
        );
        //output to json format
        echo json_encode($output);
    }


    public function gift_classlist()
    {
        $this->load->view($this->view_folder . 'gift_listing');
    }


    public function ajax_list_gift()
    {
        $list = $this->classes_model->get_datatables_gift();
        $data = array();
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $row['gift_to']              = $row_data->username;
                $row['gift_from']            = $row_data->program_title;
                $row['program_title']        = $row_data->program_title;
                $row['username']             = $row_data->username;
                $row['email']                = $row_data->email;
                $row['age_category']         = $row_data->age_category;


                if ($row_data->status == 'Confirm') {
                    $row['status'] = '<span class="text-success">' . $row_data->status . '</span>';
                } else if ($row_data->status == 'Rejected') {
                    $row['status'] = '<span class="text-danger">' . $row_data->status . '</span>';
                } else {
                    $row['status'] = '<span class="text-primary">' . $row_data->status . '</span>';
                }

                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->classes_model->count_filtered_gift(),
            "rows"  => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function export()
    {
        $data['data_list'] = $this->classes_model->get_datatables_gift_export();

        if (!empty($data['data_list'])) {
         
            $this->load->view($this->view_folder . 'report', $data);

        } else {
            $this->session->set_flashdata('error_msg', 'There is no data for export.');
            redirect('authpanel/classes');
        }
    }
    public function create_token($user_name)
    {
        // print_r($user_name);
        $token = $this->common_model->create_token(array("user_name" => $user_name));

        print_r($token);
        die;
    }
    public function user_data($user_id)
    {
        $data = $this->common_model->common_singleSelect("tbl_user", array("id" => $user_id));
        echo json_encode($data);
        die;
    }
    public function check_status($user_id,$contest_id){
         $order_data= $this->common_model->get_order_details('class', $contest_id, $user_id);
        echo json_encode($order_data);
        die;
    }
    public function send_notification($user_id, $classes_id)
    {
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $notification_data = array(
            'sender_id'        => '1',
            'sender_type'      => 'admin',
            'receiver_id'      => $user_id,
            'receiver_type'    => 'user',
            'primary_id'       => $classes_id,
            'notification_tag' => "class_started",
            'message'          => 'Admin has removed you from "' . $data['result']['program_title'] . '"',
            'title'            => 'Remove User'
        );
        echo "<pre>";
        print_r($notification_data);
        $this->common_model->send_notification($notification_data);
    }

    public function booking_list($classes_id)
    {
        // $classes_id     = base64_decode($classes_id);
        $data['classes_id'] = base64_decode($classes_id);
        //  print_r($data);
        // die;
        $this->load->view($this->view_folder . 'booking_listing', $data);
    }

    public function booking_ajax_list($category_id)
    {

        $category_id = base64_decode($category_id);

        $list = $this->classes_model->get_datatables_booking($category_id);

        $data = array();
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['username']                   = $row_data->username;
                $image      = '<a href="' . S3_BUCKET_ROOT . USER_IMAGE . $row_data->profile_image . '" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src=' . S3_BUCKET_ROOT . USER_IMAGE . $row_data->profile_image . '  alt="No image"></a>';
                $row['profile_image'] =  $image;

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->classes_model->count_filtered_d($classes_id),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    public function block_unblock_user($user_id, $classes_id)
    {
        $data['get_user'] = $this->classes_model->get_user_list($user_id, $classes_id, 0);
        $update_data = $this->common_model->common_singleUpdate('tbl_order_detail', array("is_block_classess" => "1"), array("id" => $data['get_user'][0]['order_detail_id']));
        die;
    }
    public function host_start($classes_id, $flag, $uid)
    {
        if ($flag == 0 || $flag == '0') {
            $this->common_model->common_singleUpdate("tbl_order_detail", array("is_mute_classess" => "0", "is_video_classess" => "0"), array("item_id" => $classes_id, "item_type" => "class"));
             // $this->common_model->common_singleUpdate('tbl_master_class', array("speaker_id" => 0), array("id" => $classes_id));
        }
        $this->common_model->common_singleUpdate('tbl_master_class', array("is_host" => $flag, "host_id" => $uid ,"speaker_id" => $uid), array("id" => $classes_id));
        die;
    }
    public function unblock_user($user_id, $classes_id)
    {
        $data['get_user'] = $this->classes_model->get_user_list($user_id, $classes_id, 0);
        $update_data = $this->common_model->common_singleUpdate('tbl_order_detail', array("is_block_contest" => "0"), array("id" => $data['get_user'][0]['order_detail_id']));
        $data['result']     = $this->common_model->common_singleSelect('tbl_master_class', array('id' => $classes_id));
        $notification_data = array(
            'sender_id'        => '1',
            'sender_type'      => 'admin',
            'receiver_id'      => $user_id,
            'receiver_type'    => 'user',
            'primary_id'       => $classes_id,
            'notification_tag' => "class_started",
            'message'          => 'Admin has unblock you from "' . $data['result']['program_title'] . '"',
            'title'            => 'Unblock User'
        );
        echo "<pre>";
        print_r($notification_data);
        $this->common_model->send_notification($notification_data);
        echo "send_notification";
        die;
    }
    public function get_block_user_list($classes_id)
    {
        $data['get_user'] = $this->classes_model->get_user_list(0, $classes_id, 1);
        echo json_encode($data['get_user']);
        die;
    }

    public function change_flag($type, $flag, $user_id, $classes_id)
    {
        if ($type == 'video') {
            $data = array("is_video_classess" => $flag);
        } else {
            $data = array("is_mute_classess" => $flag);
        }
        $get_user = $this->classes_model->get_user_list($user_id, $classes_id, 0);
        $this->common_model->common_singleUpdate('tbl_order_detail', $data, array("id" => $get_user[0]['order_detail_id']));
        die;
    }
    public function set_speaker($user_id, $classes_id){
         $this->common_model->common_singleUpdate('tbl_master_class', array("speaker_id" => $user_id), array("id" => $classes_id));die;
    }
    public function check_user_place($classes_id){
        $class_data = $this->common_model->common_singleSelect("tbl_master_class",array("id"=>$classes_id));
        echo json_encode($class_data);
        die;
    }
}
