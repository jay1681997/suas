<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Jan-2022
** Modified On  : -  
*/
require 'vendor/autoload.php';
class Suggestedcourse extends MY_Controller {

    private $view_folder = 'authpanel/suggestedcourse/';
    function __construct(){
        parent::__construct();
        //load suggestedcourse model
        $this->load->model('authpanel/suggestedcourse_model');
    }

    /*
    **Load index page
    */
    public function index(){
        $this->load->view($this->view_folder.'listing');
    }

    /*
    ** Function for load suggestedcourse data
    */
    public function ajax_list(){
        
        $list = $this->suggestedcourse_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $row['id']  = $row_data->id;

                $video = (!empty($row_data->video)) ? '<video width="150px" controls > <source src="'. $row_data->video.'" > Your browser does not support HTML5 video. </video>' : '-';

                $row['video']               = $video;
                $row['name']                = $row_data->name;
                $row['description']         = $row_data->description;

                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-suggestedcourseid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/suggestedcourse/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/suggestedcourse/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
              
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->suggestedcourse_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the post details
    */
    public function add()
    {  
        if($this->input->post())
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description', 'Description','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {
                $videoname = "";
                $thumb_image = "";
                //echo "<pre>"; print_r($this->input->post('thumb_image')); die;
                if (!empty($_FILES['media']) && $_FILES['media']['size'] > 0) 
                {   
                    $uploaded_filename = $this->common_model->uploadImageS3($_FILES['media'], VIDEOLIBRARY_IMAGE);
                     // echo "<pre>"; print_r($uploaded_filename); die;
                    if(!$uploaded_filename) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add', $data);
                        return false;
                    } 

                    if(!empty($uploaded_filename) && $uploaded_filename != "")
                    {
                        if (!empty($this->input->post('thumb_image'))) 
                        {
                            $data = $_POST['thumb_image'];
                            $data = str_replace('data:image/png;base64,', '', $data);
                            $data = str_replace(' ', '+', $data);
                            $data = base64_decode($data);
                            $thumb_media_name = uniqid().strtotime(date("Ymd his")).".png";
                            //echo "<pre>"; print_r($thumb_media_name);
                            file_put_contents(THUMB_DIR.$thumb_media_name, $data);
                            //echo "<pre>"; print_r(THUMB_DIR);
                            $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, VIDEOLIBRARY_IMAGE);
                           // echo "<pre>"; print_r($uploaded_imagename); die;
                            $thumb_image = $uploaded_imagename;                      
                        }
                        
                        $videoname = $uploaded_filename;
                    } 
                    else 
                    {
                        $this->load->view($this->view_folder.'add', $data);
                    }                    
                }

                $video_duration = '';


                $remotefilename = S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$videoname;
                if ($fp_remote = fopen($remotefilename, 'rb')) {
                    $localtempfilename = tempnam('/tmp', 'getID3');
                    if ($fp_local = fopen($localtempfilename, 'wb')) {
                        while ($buffer = fread($fp_remote, 8192)) {
                            fwrite($fp_local, $buffer);
                        }
                        fclose($fp_local);
                        // Initialize getID3 engine
                        $getID3 = new getID3;
                        $ThisFileInfo = $getID3->analyze($localtempfilename);
                        $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                        // Delete temporary file
                        unlink($localtempfilename);
                    }
                    fclose($fp_remote);
                } 

                $postdata = array(                  
                    'name'          => $this->input->post('name'),
                    'video'         => $videoname,
                    'video_duration'=> $video_duration,
                    'thumb_image'   => $thumb_image,
                    'description'   => $this->input->post('description'),
                );

                $suggestedcourse_id=$this->common_model->common_insert('tbl_suggested_courses', $postdata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_suggestedcourse_add_success'));
                redirect('authpanel/suggestedcourse');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the post details
    */
    public function edit($suggestedcourse_id)
    {
        $suggestedcourse_id        = base64_decode($suggestedcourse_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_suggested_courses',array('id' => $suggestedcourse_id));
        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('name','Name','required|trim');
            $this->form_validation->set_rules('description', 'Description','required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                
                $videoname = $data['result']['video'];
                $thumb_image = $data['result']['thumb_image'];
                if (!empty($_FILES['media']) && $_FILES['media']['size'] > 0) 
                {   
                    $uploaded_filename = $this->common_model->uploadImageS3($_FILES['media'],VIDEOLIBRARY_IMAGE);

                    if(!empty($uploaded_filename) && $uploaded_filename!="")
                    {
                         if (!empty($this->input->post('thumb_image'))) 
                        {
                            $data = $_POST['thumb_image'];
                            $data = str_replace('data:image/png;base64,', '', $data);
                            $data = str_replace(' ', '+', $data);
                            $data = base64_decode($data);
                            $thumb_media_name = uniqid().strtotime(date("Ymd his")).".png";
                            file_put_contents(THUMB_DIR.$thumb_media_name, $data);
                            
                            $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, VIDEOLIBRARY_IMAGE);
                            $thumb_image = $uploaded_imagename;                      
                        }
                        $videoname = $uploaded_filename;
                    } else {
                        $this->load->view($this->view_folder.'edit', $data);
                    }
                }

                $remotefilename = S3_BUCKET_ROOT.VIDEOLIBRARY_IMAGE.$videoname;
                if ($fp_remote = fopen($remotefilename, 'rb')) {
                    $localtempfilename = tempnam('/tmp', 'getID3');
                    if ($fp_local = fopen($localtempfilename, 'wb')) {
                        while ($buffer = fread($fp_remote, 8192)) {
                            fwrite($fp_local, $buffer);
                        }
                        fclose($fp_local);
                        // Initialize getID3 engine
                        $getID3 = new getID3;
                        $ThisFileInfo = $getID3->analyze($localtempfilename);
                        //echo "<pre>"; print_r($ThisFileInfo); 
                        $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                        //echo "<pre>"; print_r($video_duration); die;
                        // Delete temporary file
                        unlink($localtempfilename);
                    }
                    fclose($fp_remote);
                } 
                // echo "<pre>"; print_r($video_duration); die;
                $postdata = array(                  
                    'name'    => $this->input->post('name'),
                    'video'   => $videoname,
                    'thumb_image'   => $thumb_image,
                    'video_duration'=> $video_duration,
                    'description'   => $this->input->post('description'),
                );
        
                $this->common_model->common_singleUpdate('tbl_suggested_courses',$postdata,array('id'=>$suggestedcourse_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_suggestedcourse_update_success'));
                redirect('authpanel/suggestedcourse');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the video library view page
    */
    public function view($suggestedcourse_id){
        $suggestedcourse_id    = base64_decode($suggestedcourse_id);
        $data['result']     = $this->suggestedcourse_model->view_suggestedcourse($suggestedcourse_id);
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of video library
    */
    public function changestatus($suggestedcourse_id, $status){
        $this->common_model->common_singleUpdate('tbl_suggested_courses',array('status'=>$status),array('id'=>$suggestedcourse_id));
        echo $this->lang->line('adminpanel_message_suggestedcourse_'.$status.'_success');die;
    }

    /*
    ** Function for remove video library
    */
    public function removesuggestedcourse($suggestedcourse_id)
    {
        $this->common_model->common_singleUpdate('tbl_suggested_courses',array('is_deleted'=>'1'),array('id'=>$suggestedcourse_id));
        echo $this->lang->line('adminpanel_message_suggestedcourse_delete_success');die;
    }

}
