<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 18-Feb-2022
** Modified On  : -  
*/
class Sponsor extends MY_Controller {

    private $view_folder = 'authpanel/sponsor/';
    function __construct(){
        parent::__construct();
        //load sponsor model
        $this->load->model('authpanel/sponsor_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load sponsor data
    */
    public function ajax_list(){
        $list = $this->sponsor_model->get_datatables();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i= 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num']                  =  $i++;
                $row['id']                   = $row_data->id;
                $image                       = '<a href="'.$row_data->sponsor_logo.'" class="image-popup"><img class="img-responsive img-circle img-thumbnail thumb-md" src='.$row_data->sponsor_logo.'  alt="No image"></a>';
                $row['sponsor_logo']          = $image;
                $row['sponsor_name']           = $row_data->sponsor_name;
                $row['sponsor_link']          = $row_data->sponsor_link;
                $row['bio']                     = $row_data->bio;
                // $row['contest_name']         = $row_data->contest_name;
                //Check status
                if($row_data->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-sponsorid="'.$row_data->id.'" data-status="'.$row_data->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                // $action = '<a href="'.site_url().'authpanel/sponsor/view/'.base64_encode($row_data->id).'"class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/sponsor/edit/'.base64_encode($row_data->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                $row['action'] = $action;
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                
                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->sponsor_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for add the sponsor details
    */
    public function add()
    {  
        $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));
        if($this->input->post())
        {
            $this->form_validation->set_rules('sponsor_name','Name','required|trim');
            $this->form_validation->set_rules('sponsor_link','Link','required|trim');
            $this->form_validation->set_rules('bio','Bio','required|trim');
            // $this->form_validation->set_rules('contest_id','Contest ID','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');

            if($this->form_validation->run()) {

                $sponsor_logo = "default.png";
                if (!empty($_FILES['sponsor_logo']) && $_FILES['sponsor_logo']['size'] > 0) {
                    $sponsor_logo = $this->common_model->uploadImageS3($_FILES['sponsor_logo'],SPONSOR_LOGO);
                    if(!$sponsor_logo) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'add',$data);
                        return false;
                    }
                }

                $sponsordata = array(
                    'sponsor_name'      => $this->input->post('sponsor_name'),
                    'sponsor_logo'      => $sponsor_logo,
                    'sponsor_link'      => $this->input->post('sponsor_link'),                    
                    'bio'               => $this->input->post('bio'),
                    'contest_id'        => '0'
                    // 'contest_id'        => $this->input->post('contest_id'),
                );

                $sponsor_id=$this->common_model->common_insert('tbl_sponsor', $sponsordata);
                $this->session->set_flashdata('success_msg', $this->lang->line('adminpanel_message_sponsor_add_success'));
                redirect('authpanel/sponsor');
            } else {
                $this->load->view($this->view_folder.'add', $data);    
            }
        } else {
            $this->load->view($this->view_folder.'add', $data);
        }
    }

    /*
    ** Function for edit the sponsor details
    */
    public function edit($sponsor_id)
    {
        $sponsor_id     = base64_decode($sponsor_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_sponsor',array('id'=>$sponsor_id));
        $data['contests'] = $this->common_model->common_multipleSelect('tbl_master_contest', array('status'=>'Active','is_deleted'=>'0'));

        if($this->input->post() && !empty($data['result']))
        {
            $this->form_validation->set_rules('sponsor_name','Name','required|trim');
            $this->form_validation->set_rules('sponsor_link','Link','required|trim');
            $this->form_validation->set_rules('bio','Bio','required|trim');
            // $this->form_validation->set_rules('contest_id','Contest ID','required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                
                $sponsor_logo = @$data['result']['sponsor_logo'];
                if (!empty($_FILES['sponsor_logo']) && $_FILES['sponsor_logo']['size'] > 0) {
                    $sponsor_logo = $this->common_model->uploadImageS3($_FILES['sponsor_logo'],SPONSOR_LOGO);
                    if(!$sponsor_logo) {
                        $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                        $this->load->view($this->view_folder.'edit',$data);
                        return false;
                    }
                }

                $sponsordata = array(
                    'sponsor_name'      => $this->input->post('sponsor_name'),
                    'sponsor_logo'      => $sponsor_logo,
                    'sponsor_link'      => $this->input->post('sponsor_link'),                    
                    'bio'               => $this->input->post('bio'),
                    'contest_id'        => '0'
                    // 'contest_id'        => $this->input->post('contest_id'),
                );
                
                $this->common_model->common_singleUpdate('tbl_sponsor',$sponsordata,array('id'=>$sponsor_id));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_sponsor_update_success'));
                redirect('authpanel/sponsor');  
            } else {
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    /*
    ** Function for load the sponsor view page
    */
    public function view($sponsor_id){
        $sponsor_id          = base64_decode($sponsor_id);
        $data['result']     = $this->common_model->common_singleSelect('tbl_sponsor',array('id'=>$sponsor_id));
        $this->load->view($this->view_folder.'view', $data);
    }

    /*
    ** Function for update the status of sponsor
    */
    public function changestatus($sponsor_id, $status){

        $this->common_model->common_singleUpdate('tbl_sponsor',array('status'=>$status),array('id'=>$sponsor_id));
        echo $this->lang->line('adminpanel_message_sponsor_'.$status.'_success');die;
    }

    /*
    ** Function for remove sponsor
    */
    public function removesponsor($sponsor_id)
    {
        $this->common_model->common_singleUpdate('tbl_sponsor',array('is_deleted'=>'1'),array('id'=>$sponsor_id));
        echo $this->lang->line('adminpanel_message_sponsor_delete_success');die;
    }
    
}
