<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 14-March-2022
** Modified On  : -  
*/
class Contacts extends My_Controller {

    /*
     * Default constructor
    */
    private $viewfolder = 'authpanel/contacts/';
	function __construct()
    { 
    	parent::__construct();
        $this->load->model('authpanel/contacts_model');
    }

    /*
    *default contacts list page load
    */
    public function index()
	{   
        $this->load->view($this->viewfolder.'listing');
	}

    /*
     * Listing of contacts by ajax
    */
    public function ajax_list()
    {
        /*get contacts data list*/
        $contacts_list = $this->contacts_model->get_contactsdata_list();

        $data = array();
        if (!empty($contacts_list)) {

            $i=1;
            foreach ($contacts_list as $contacts) {
                $row = array();
                $row['id']          = $i;
                $row['username']    = $contacts->name;
                $row['email']       = $contacts->email;
                $row['subject']     = $contacts->subject;
                $row['description'] = $contacts->description;

               if($contacts->status == 'Active'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-contactid="'.$contacts->id.'" data-status="'.$contacts->status.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                $row['inserted_date']=$this->common_model->date_convert(@$contacts->inserted_date,ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));

                // Remove Function Link prepared
                $action = ' <a href="javascript:void(0);" onclick="remove('.$contacts->id.');" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-delete"></i></a>';

                if($contacts->is_reply=='0') {
                    $row['reply_message'] = '<a href="'.site_url('authpanel/contacts/reply/').base64_encode($contacts->id).'" class="btn btn-xs waves-effect waves-float waves-green">Not Replied</a>';
                    // Reply Function Link prepared
                    $action .= ' <a href="'.site_url('authpanel/contacts/reply/').base64_encode($contacts->id).'" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-mail-reply zmdi-hc-fw"></i></a>';
                } else {
                    $row['reply_message'] = $contacts->reply_message;
                }

                $row['action'] = $action;
                $data[] = $row;
                $i++;
            }
        }
 
        $output = array(
            "total" => $this->contacts_model->count_filtered(),
            "rows"  => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    *reply to contact user
    */
    public function reply($contacts_id) 
    {

        $contacts_id      = base64_decode($contacts_id);
        $contacts_details = $this->contacts_model->contactsdata($contacts_id);
        $data['result']   = $contacts_details;
        $AllPostData      = $this->input->post();
        if($AllPostData && !empty($data['result'])) {

            $this->form_validation->set_rules('reply_message',"Reply Message",'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" >', '</div>');
            if($this->form_validation->run()) {

                $contacteddata['username']    = $contacts_details['username'];
                $contacteddata['reply_message'] = $AllPostData['reply_message'];
                $message = $this->load->view('template/contactus_reply',$contacteddata,TRUE);
                $subject = 'Reply Contact Us Request';
                // echo "<pre>";
                // print_r($message);
                // print_r($contacteddata);
                // print_r($contacts_details);
                // die;
                if($this->common_model->send_mail($contacts_details['email'],ADMIN_EMAIL,$message,$subject)) {

                    $contacts_array=array(
                        'reply_message' => $AllPostData['reply_message'],
                        'is_reply'      => '1',
                    );
                    $this->common_model->common_singleUpdate('tbl_contact_us',$contacts_array,array('id'=>$contacts_id));
                    $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_contactus_repliedsuccess'));

                } else {
                    $this->session->set_flashdata('error_msg',$this->lang->line('adminpanel_message_replycontacts_error'));
                }
                redirect('authpanel/contacts/');
            } else {
                $this->load->view($this->viewfolder.'reply',$data);
            }
        } else {
            $this->load->view($this->viewfolder.'reply',$data);
        }
    }

    /*
    * remove contacts and releted all data
    */
    public function remove($contacts_id) 
    {
        $param["is_deleted"] = '1';
        $this->common_model->common_singleUpdate('tbl_contact_us',$param,array('id'=>$contacts_id));
        echo $this->lang->line('adminpanel_message_deletecontacts_succcess');die;
    }

    /*
    * change status of contacts
    */
    public function changestatus($contacts_id,$status)
    {
        $this->common_model->common_singleUpdate('tbl_contact_us',array('status'=>$status),array('id'=>$contacts_id));  
        echo $this->lang->line('adminpanel_message_contacts_'.$status.'_success');die;
    }
}