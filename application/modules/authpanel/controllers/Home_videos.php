<?php defined('BASEPATH') or exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 14-Feb-2022
** Modified On  : -  
*/
class Home_videos extends MY_Controller
{

    private $view_folder = 'authpanel/home_videos/';
    function __construct()
    {
        parent::__construct();
        //load customer model
        $this->load->model('authpanel/home_videos_model');
    }

    /* 
    *Load index page
    */
    public function index()
    {
        $this->load->view($this->view_folder . 'listing');
    }

    /*
    ** Function for load customer data
    */
    public function ajax_list()
    {
        $list = $this->home_videos_model->get_datatables();
        // echo "<pre>"; print_r($this->db->last_query());print_r($list); die();
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            $i = 1;
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['num'] =  $i++;
                $row['title']         = $row_data->title;
                $row['age']           =  $row_data->age;
                $row['image']         = (!empty($row_data->thumb_image)) ?'<a href="' . $row_data->thumb_image_url . '" class="image-popup"><img width="700px" height="900px"class="img-responsive img-circle img-thumbnail thumb-md" src=' . $row_data->thumb_image_url . '  alt="No image"></a>':'-';
                $row['video']         =  (!empty($row_data->video)) ? '<video width="150px" controls > <source src="'.S3_BUCKET_ROOT.HOME_VIDEO. $row_data->video.'" > Your browser does not support HTML5 video. </video>' : '-';;
                // View page Link prepared
                // Edit page Link prepared
                $action = ' <a href="' . site_url() . 'authpanel/home_videos/edit/' . base64_encode($row_data->id) . '" class="btn btn-xs waves-effect waves-float waves-green" style="margin-bottom:5px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared


                $row['action'] = $action;

                $row['last_login'] = $this->common_model->date_convert($row_data->last_login, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));
                $row['insert_datetime'] = $this->common_model->date_convert($row_data->insert_datetime, ADMIN_LONGDATE, $this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }
        $output = array(
            "total" => $this->home_videos_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    ** Function for edit the customer details
    */
    public function edit($home_video_id)
    {
        $home_video_id                = base64_decode($home_video_id);
        $data['result'] = $this->common_model->common_singleSelect('tbl_home_video', array('id' => $home_video_id));
        // echo "<pre>"; print_r($data); die;
      
        if ($this->input->post() && !empty($data['result'])) {
            $imgname1 = '';
            $imgname = '';
            $thumb_image = '';
            $video_duration = '';
        if (!empty($_FILES['contest_image']) && $_FILES['contest_image']['size'] > 0) 
                    {   

                        $uploaded_filename = $this->common_model->uploadImageS3($_FILES['contest_image'], HOME_VIDEO);
                        // echo "<pre>"; print_r($uploaded_filename); die;
                        if(!$uploaded_filename) {
                            $data['error_msg']=$this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder.'edit', $data);
                            return false;
                        } 
                       
                        if(!empty($uploaded_filename) && $uploaded_filename != "")
                        {
                            //FOR video and its IMAGE
                            if (!empty($this->input->post('thumb_image'))) 
                            {
                                $data = $_POST['thumb_image'];
                                //list($type, $data) = explode(';', $data);
                                //list(, $data)      = explode(',', $data);
                                $data = str_replace('data:image/png;base64,', '', $data);
                                $data = str_replace(' ', '+', $data);
                                $data = base64_decode($data);
                                $thumb_media_name = uniqid().strtotime(date("Ymd his")).".png";
                                // echo "<pre>"; print_r($thumb_media_name);
                                file_put_contents(THUMB_DIR.$thumb_media_name, $data);
                                $uploaded_imagename = $this->common_model->video_thumb_upload_S3($thumb_media_name, HOME_VIDEO);
                                // echo "<pre>"; print_r($uploaded_imagename);
                                // $imgname = $uploaded_imagename;   
                                $thumb_image = $uploaded_imagename;                                      
                            }
                            $imgname = $uploaded_filename;
                             // die;
                        }
                        else 
                        {
                            $this->load->view($this->view_folder.'add', $data);
                        }                    
                    }
                    if (!empty($_FILES['contest_image_image']) && $_FILES['contest_image_image']['size'] > 0) {
                        // echo "P2";
                        $imgname1 = $this->common_model->uploadImageS3($_FILES['contest_image_image'], HOME_VIDEO);
                        //echo $profile_image; die;
                        echo $imgname1;
                        if (!$imgname1) {
                            $data['error_msg'] = $this->lang->line('adminpanel_message_imageupload_failed');
                            $this->load->view($this->view_folder . 'edit',$data);
                            return false;
                        }
                    }
                    $video_duration = '';
                    $remotefilename = S3_BUCKET_ROOT.HOME_VIDEO.$imgname;
                    if ($fp_remote = fopen($remotefilename, 'rb')) {
                        $localtempfilename = tempnam('/tmp', 'getID3');
                        if ($fp_local = fopen($localtempfilename, 'wb')) {
                            while ($buffer = fread($fp_remote, 8192)) {
                                fwrite($fp_local, $buffer);
                            }
                            fclose($fp_local);
                            // Initialize getID3 engine
                            $getID3 = new getID3;
                            $ThisFileInfo = $getID3->analyze($localtempfilename);
                            $video_duration = gmdate("H:i:s", $ThisFileInfo['playtime_seconds']);
                            // Delete temporary file
                            unlink($localtempfilename);
                        }
                        fclose($fp_remote);
                    } 
                
                // echo "<pre>";
                // print_r($imgname1);
                // print_r($imgname);
                // print_r($thumb_image);
                // print_r($video_duration);
                $home_video_data = array(
                    "title" => $this->input->post('title'),
                    "age" => $this->input->post('age')
                );
                if($imgname1 != ''){
                    $home_video_data['image'] = $imgname1;
                }
                if($uploaded_filename != ''){
                    $home_video_data['video'] = $uploaded_filename;
                }
                if($thumb_image != ''){
                    $home_video_data['thumb_image'] = $thumb_image;
                }
                if($video_duration != ''){
                    $home_video_data['video_duration'] = $video_duration;
                }
                // print_r($home_video_data);
                // die;
                $this->common_model->common_singleUpdate('tbl_home_video',$home_video_data,array("id"=>$home_video_id));
                  $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_contest_update_success'));
                redirect('authpanel/home_videos');  
        //         print_r($home_video_data);
        // die;      
        } else {
            $this->load->view($this->view_folder . 'edit', $data);
        }
    }
}
