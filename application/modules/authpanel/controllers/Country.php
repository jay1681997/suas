<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : EzPark
** Date         : 12-12-2019
** Modified On  : -  
*/
class Country extends MY_Controller {

    private $view_folder = 'authpanel/country/';
    function __construct(){
        parent::__construct();
        //load customer model
        $this->load->model('authpanel/country_model');
    }

    /*
    *Load index page
    */
	public function index(){
		$this->load->view($this->view_folder.'listing');
	}

    /*
    ** Function for load country data
    */
    public function ajax_list(){
        
        $list = $this->country_model->get_datatables();
        $this->session->set_userdata('lastlimit', $_REQUEST['limit']);
        $this->session->set_userdata('lastoffset',$_REQUEST['offset']);
        $data = array();
        // check if list is not empty
        if (!empty($list)) {
            // Loop through each records
            foreach ($list as $row_data) {
                $row = array();
                $row['id'] = $row_data->id;
                $row['name'] = $row_data->name;
                $row['region']   = $row_data->region;
                $row['tax'] = $row_data->tax;

                //Check status
                if($row_data->is_active == '1'){
                    $checkstete = 'checked="checked"';
                } else {
                    $checkstete = '';
                }
                $status='<div class="switch"><label><input type="checkbox" name="changestatus" data-customerid="'.$row_data->id.'" data-status="'.$row_data->is_active.'" '.$checkstete.'><span class="lever switch-col-green"></span></label></div>';
                $row['status'] = $status;

                // View page Link prepared
                //$action = '<a href="'.site_url().'authpanel/customer/view/'.base64_encode($row_data->id).'"class="btn waves-effect waves-float waves-green" style="margin-bottom:5px;padding: 3px 8px;"><i class="zmdi zmdi-eye"></i></a>';
                // Edit page Link prepared
                $action = ' <a href="'.site_url().'authpanel/country/edit/'.base64_encode($row_data->id).'" class="btn waves-effect waves-float waves-green" style="margin-bottom:5px;padding: 3px 8px;"><i class="zmdi zmdi-edit"></i></a>';
                // Remove Function Link prepared
                $action .= ' <a href="javascript:void(0);" onclick="remove('.$row_data->id.');" class="btn waves-effect waves-float waves-green" style="margin-bottom:5px;padding: 3px 8px;"><i class="zmdi zmdi-delete"></i></a>';
                $row['action'] = $action;

                // $row['last_login'] = $this->common_model->date_convert($row_data->last_login,'Y-m-d h:i a', $this->session->userdata(ADMIN_TIMEZONE));
                  $row['insertdate'] = $this->common_model->date_convert($row_data->insertdate, ADMIN_LONGDATE,$this->session->userdata(ADMIN_TIMEZONE));
                // $row['insertdate'] = $this->common_model->date_convert($row_data->insertdate, 'Y-m-d h:i a',$this->session->userdata(ADMIN_TIMEZONE));

                $data[] = $row;
            }
        }

        $output = array(
            "total" => $this->country_model->count_filtered(),
            "rows" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*
    **Function for add the country details
    */
    function add()
    {
        $data['countries'] = $this->common_model->get_country_code_new();
        if($this->input->post()){
            
           $this->form_validation->set_rules('region','Size name','required|trim');
            $this->form_validation->set_rules('tax','Tax','required|trim');
            $this->form_validation->set_rules("country_id","Country Name","required|trim");
            // $this->form_validation->set_rules('status','Status','required|trim');

            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if($this->form_validation->run()) {

                // $profile_image = "default-user.png"; 
               
                
                $typedata = array(
                    'region'           => $this->input->post('region'),
                    'tax'       => $this->input->post('tax'),
                    "country_id" => $this->input->post('country_id')
                 );
                $category_id=$this->common_model->common_insert('tbl_tax_details',$typedata);

                $this->session->set_flashdata('success_msg', $this->lang->line('admin_keywords_add_country'));

                redirect('authpanel/country');
            } else {
                $this->load->view($this->view_folder.'add',$data);   
            }
        } else {
            $this->load->view($this->view_folder.'add',$data);
        }
    }

    /*
    ** Function for edit the country details
    */
    function edit($country_id){
        $country_id = base64_decode($country_id);
        $data['countries'] = $this->common_model->get_country_code_new();
        $data['result'] = $this->common_model->common_singleSelect('tbl_tax_details',array('id'=>$country_id));
        if($this->input->post() && !empty($data['result'])){
            
           $this->form_validation->set_rules('region','Size name','required|trim');
            $this->form_validation->set_rules('tax','Tax','required|trim');
            $this->form_validation->set_rules("country_id","Country Name","required|trim");
            // $this->form_validation->set_rules('status','Status','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                 

                $typedata = array(
                    'region'           => $this->input->post('region'),
                    'tax'       => $this->input->post('tax'),
                    "country_id" => $this->input->post('country_id')
                 );
                $this->common_model->common_singleUpdate('tbl_tax_details',$typedata,array('id'=>$country_id));

                $this->session->set_flashdata('success_msg',$this->lang->line('admin_keywords_countryupdate_success'));
               
            redirect('authpanel/country'); 

            } else {
                // echo "Elseeeeeeee"; die;
                $this->load->view($this->view_folder.'edit', $data);
            }
        } else {
            // echo "Elseeeeeeee1111111"; die;
            $this->load->view($this->view_folder.'edit', $data);
        }
    }

    

    /*
    ** Function for update the status of country
    */
    function changestatus($country_id,$status){
        // print_r($country_id);
        $sts = (!empty($status) && $status=="Active")?'1':'0';
        $this->common_model->common_singleUpdate('tbl_tax_details',array('is_active'=>$sts),array('id'=>$country_id));
        echo $this->lang->line('text_rest_admincountry_'.$status.'_success');
    }

    /*
    ** Function for remove country
    */
    function removecountry($country_id)
    {
        $this->common_model->common_singleUpdate('tbl_tax_details',array('is_deleted'=>'1'),array('id'=>$country_id));
        echo $this->lang->line('admin_keywords_country_delete_success');
    }

    
}
