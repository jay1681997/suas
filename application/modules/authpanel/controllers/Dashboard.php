<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
** Project      : SUAS
** Date         : 10-Feb-2022
** Modified On  : -  
*/
class Dashboard extends MY_Controller {

    private $view_folder = 'authpanel/dashboard/';
    //Default constructor
	function __construct()
    { 
        parent::__construct();
        $this->load->model('authpanel/customer_model');
        $this->load->model('authpanel/faq_model');
        $this->load->model('authpanel/contacts_model');
        $this->load->model('authpanel/contest_model');
        $this->load->model('authpanel/promocode_model');
        $this->load->model('authpanel/videolibrary_model');
        $this->load->model('authpanel/evaluation_model');
        $this->load->model('authpanel/subscription_model');
        $this->load->model('authpanel/speechevaluation_model');
        $this->load->model('authpanel/masterprize_model');
        $this->load->model('authpanel/classes_model');
        $this->load->model('authpanel/suggestedcourse_model');
        $this->load->model('authpanel/leaderboard_model');
        $this->load->model('authpanel/banner_model');
        $this->load->model('authpanel/report_model');
        $this->load->model('authpanel/sponsor_model');
        $this->load->model('authpanel/country_model');
    }

    //load admin panel dashboard with all count.
    function index()
    { 
        $this->load->view($this->view_folder.'dashboard');
    }

    //get ajax side bar count
    function getDashboardCounts()
    {
        $data['customer_counts']         = $this->customer_model->count_filtered();
        $data['contest_counts']          = $this->contest_model->count_filtered();
        $data['faq_counts']              = $this->faq_model->count_filtered();
        $data['promocode_counts']        = $this->promocode_model->count_filtered();
        $data['contacts_counts']         = $this->contacts_model->count_filtered();
        $data['videolibrary_counts']     = $this->videolibrary_model->count_filtered();
        $data['evaluation_counts']       = $this->evaluation_model->count_filtered();
        $data['subscription_counts']     = $this->subscription_model->count_filtered();
        $data['speechevaluation_counts'] = $this->speechevaluation_model->count_filtered();
        $data['masterprize_counts']      = $this->masterprize_model->count_filtered();
        $data['classes_counts']          = $this->classes_model->count_filtered();
        $data['suggestedcourse_counts']  = $this->suggestedcourse_model->count_filtered();
        $data['leaderboard_counts']      = $this->leaderboard_model->count_filtered();
        $data['banner_counts']           = $this->banner_model->count_filtered();
        $data['report_counts']           = $this->report_model->count_filtered();
        $data['sponsor_counts']          = $this->sponsor_model->count_filtered();
        $data['country_counts']          = $this->country_model->count_filtered();
        echo json_encode($data);
    }

    //edit profile super admin
    function profile()
    {
        if(!$this->session->userdata(ADMIN_SESSION_NAME)){
            redirect('authpanel/login');
        }
        $data['countries']  = $this->common_model->get_country_code();
        $admin_session = $this->session->userdata(ADMIN_SESSION_NAME);
        $data['result'] = $admin_session;
        if($this->input->post()) {
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('country_code','Country Code','required|trim');
            $this->form_validation->set_rules('phone','Phone','required|trim|min_length[8]|max_length[12]');

            if(!empty($this->input->post('password')) || !empty($this->input->post('confirmpassword'))){

                $this->form_validation->set_rules('password', 'Password', 'required');
                $this->form_validation->set_rules('confirmpassword', 'Password Confirmation', 'required|matches[password]');
            }
            $this->form_validation->set_error_delimiters('<div class="error" >', '</div>');
            
            if($this->form_validation->run()) {  

                $profile_image = $admin_session['profile_image'];                
                if (!empty($_FILES['profile_image']) && $_FILES['profile_image']['size'] > 0) {   
                    //Upload Profile Image
                    $profile_image = $this->common_model->uploadImageS3($_FILES['profile_image'], ADMIN_IMAGE);

                }
                if(isset($profile_image['status']) && $profile_image['status'] == false) {  
                    $data['error_msg'] = $profile_image['error'];
                    $this->load->view($this->view_folder.'profile',$data);
                } else {                                    
                    $params = array(
                        'name'          => $this->input->post('name'),
                        'country_code'  => $this->input->post('country_code'),
                        'phone'         => $this->input->post('phone'),
                        'email'         => $this->input->post('email'),
                        'profile_image' => $profile_image,
                    );

                    $admin_session['name']          = $this->input->post('name');
                    $admin_session['country_code']  = $this->input->post('country_code');
                    $admin_session['phone']         = $this->input->post('phone');
                    $admin_session['profile_image'] = $profile_image;
                    $admin_session['email']         = $this->input->post('email');
                    //update admin profiles
                    $this->session->set_userdata(ADMIN_SESSION_NAME, $admin_session);
                    //If password change
                    if(!empty($this->input->post('password'))) {
                        $params['password'] = $this->common_model->encrypt_password($this->input->post('password'));
                    }
                    $this->common_model->common_singleUpdate('tbl_admin', $params, array('id'=>$admin_session['id']));
                    $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_profile_update_success'));
                    redirect('authpanel/dashboard');
                }
            } else {
                $data['result']['profile_image'] = $admin_session['profile_image'];
                $this->load->view($this->view_folder.'profile',$data);
            }
        } else {
            $this->load->view($this->view_folder.'profile',$data);
        }
    }

    //edit setting
    public function setting()
    {
        $data['result']     = $this->common_model->common_singleSelect('tbl_setting',array('is_deleted'=>'0'));
        $data['currencies'] = $this->common_model->get_country_code();
        if($this->input->post() && !empty($data['result']))
        {
            // print_r($this->input->post() ); die;
            $this->form_validation->set_rules('tax','Tax','required|trim');
            $this->form_validation->set_rules('currency','currency','required|trim');
            $this->form_validation->set_rules('credit_point_discount','Credit Point','required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:#F44336;">','</div>');
            if ($this->form_validation->run()) {
                $currency = $this->common_model->common_singleSelect('tbl_master_countries', array('id'=>$this->input->post('currency')));
                $settingdata = array(
                    'tax'                   => $this->input->post('tax'),
                    'currency'              => $currency['currency'],
                    'currency_symbol'       => $currency['currency_symbol'],
                    'credit_point_discount' => $this->input->post('credit_point_discount'),
                );
                
                $this->common_model->common_singleUpdate('tbl_setting',$settingdata,array('id'=>$data['result']['id']));
                $this->session->set_flashdata('success_msg',$this->lang->line('adminpanel_message_setting_update_success'));
                redirect('authpanel/dashboard');  
            } else {
                $this->load->view($this->view_folder.'setting', $data);
            }
        } else {
            $this->load->view($this->view_folder.'setting', $data);
        }
    }

}