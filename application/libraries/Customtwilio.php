<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Twilio class
 * Help to twilio activity like create channel,user,member and chat grant token
 *
 * @author Hyperlink Infosystem
 */
require_once APPPATH."third_party/twilio-php/autoload.php";

use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Rest\Client;

class Customtwilio extends CI_Model {

	var $twilio;
    
	public function __construct()
    {
    	$this->twilio = new Client(TWILLIO_ACCOUNT_SID,TWILLIO_ACCOUNT_AUTH);
    }

    /*
    * send SMS
    */
    public function sendSMS($phone,$message)
    {
        if(!empty($phone) && !empty($message)){
            try{
                $message = $this->twilio->messages->create($phone,["body" => $message,"from" => TWILLIO_ACCOUNT_PHONE]);
                if(!empty($message->sid)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } catch(Exception $e) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /*
    *Function for create user
    */
    function createUser($user_identity)
    {
        try{
            $userdata = $this->common_model->common_singleSelect("tbl_user","id = '".$user_identity."'");
            if(!empty($userdata)){
                $user_attributes = array(
                    'id'=>$userdata['id'],
                    'first_name'=>$userdata['first_name'],
                    'last_name'=>$userdata['last_name'],
                    'country_id'=>$userdata['country_id'],
                    'country_code'=>$userdata['country_code'],
                    'phone'=>$userdata['phone'],
                    'email'=>$userdata['email'],
                    'profile_image'=>USER_IMAGE.$userdata['image'],
                    'cover_image'=>USER_IMAGE.$userdata['cover_image'],
                );
                $user = $this->twilio->conversations->v1->users($user_identity)->fetch();
                if(isset($user->sid) && !empty($user->sid)){
                    $upduser = $this->twilio->conversations->v1->users($user->sid)->update(array("friendlyName" => $userdata['first_name']." ".$userdata['last_name'],"attributes" => json_encode($user_attributes)));
                    $this->common_model->common_singleUpdate("tbl_user",array("twilio_user_sid"=>$user->sid),array("id"=>$user_identity));
                    return $user->sid;
                } else {
                    $newuser = $this->twilio->conversations->v1->users->create(array("identity" => $user_identity,"friendlyName" => $userdata['first_name']." ".$userdata['last_name'],"attributes" => json_encode($user_attributes)));
                    if(isset($newuser->sid) && !empty($newuser->sid)){
                        $this->common_model->common_singleUpdate("tbl_user",array("twilio_user_sid"=>$newuser->sid),array("id"=>$user_identity));
                        return $newuser->sid;
                    } else {
                        return "";
                    }
                }
            } else {
                return "";
            }
        } catch(Exception $e) {
            return "";
        }
    }

    /*
    * Function to create users of chat
    */
    public function removeUser($user_identity)
    {
        try{
            $this->twilio->conversations->v1->users($user_identity)->delete();
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }

    /*
    * Function to create Participants of conversations
    */
    public function createParticipants($user_identity,$channel_sid)
    {
        try{
            $userdata = $this->common_model->common_singleSelect("tbl_user","id = '".$user_identity."'");
            if(!empty($userdata)){
                $user_attributes = array(
                    'id'=>$userdata['id'],
                    'first_name'=>$userdata['first_name'],
                    'last_name'=>$userdata['last_name'],
                    'country_id'=>$userdata['country_id'],
                    'country_code'=>$userdata['country_code'],
                    'phone'=>$userdata['phone'],
                    'email'=>$userdata['email'],
                    'profile_image'=>USER_IMAGE.$userdata['image'],
                    'cover_image'=>USER_IMAGE.$userdata['cover_image'],
                );
                $participant = $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->conversations($channel_sid)->participants($user_identity)->fetch();
                if(isset($participant->sid) && !empty($participant->sid)){
                    $updparticipant = $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->conversations($channel_sid)->participants($participant->sid)->update(array("friendlyName" => $userdata['first_name']." ".$userdata['last_name'],"attributes" => json_encode($user_attributes)));
                    return $participant->sid;
                } else {
                    $newparticipant = $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->conversations($channel_sid)->participants->create(array("friendlyName" => $userdata['first_name']." ".$userdata['last_name'],"attributes" => json_encode($user_attributes)));
                    if(isset($newparticipant->sid) && !empty($newparticipant->sid)){
                        return $newparticipant->sid;
                    } else {
                        return "";
                    }
                }
            } else {
                return "";
            }
        } catch(Exception $e) {
            return "";
        }
    }

    /*
    ** Function to remove Participants of conversations
    */
    public function removeParticipants($channel_sid,$participant_identity)
    {
        try{
            $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->conversations($channel_sid)->participants($participant_identity)->delete();
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }

    /*
    ** Get conversations all Participants list
    */
    public function getChannelAllParticipants($channel_sid)
    {
        try{
            return $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->conversations($channel_sid)->participants->read(20);
        } catch(Exception $e) {
            return array();
        } 
    }

    /*
    * Function to Create channel for chat between two users
    */
    public function createChatChannel($unique_name,$friendly_name)
    {
        try{
            $conversation = $this->twilio->conversations->v1->conversations($unique_name)->fetch();
            if(isset($conversation->sid) && !empty($conversation->sid)){
                $updconversation = $this->twilio->conversations->v1->conversations($conversation->sid)->update(array("friendlyName" => $friendly_name));
                return $conversation->sid;
            } else {
                $newconversation = $this->twilio->conversations->v1->conversations->create(array("uniqueName" => $unique_name,"friendlyName" => $friendly_name));
                if(isset($newconversation->sid) && !empty($newconversation->sid)){
                    return $newconversation->sid;
                } else {
                    return "";
                }
            }
        } catch(Exception $e) {
            return "";
        }
    }

    /*
    ** Function to remove channel from twillio
    */
    public function removeChannel($channel_sid)
    {
        try{
            $this->twilio->conversations->v1->conversations($channel_sid)->delete();
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }

    /*
    ** Generate chat access token 
    */
    public function chatAccessToken($identity,$device_type,$device_token)
    {
        try{
            $configuration = $this->twilio->conversations->v1->configuration()->update(
                array(
                    "NewMessage.enabled" => true,
                    "NewMessage.sound" => "default",
                    "NewMessage.template" => 'You have a new message from ${PARTICIPANT}: ${MESSAGE}',
                    "addedToConversation.enabled" => true,
                    "addedToConversation.sound" => "default",
                    "addedToConversation.template" => '${MESSAGE}'
                )
            );

            // Create access token, which we will serialize and send to the client
            $token = new AccessToken(TWILLIO_ACCOUNT_SID,TWILLIO_API_KEY,TWILLIO_API_SECRET,3600,$identity);

            // Create Chat grant
            $chatGrant = new ChatGrant();
            $chatGrant->setServiceSid(TWILLIO_SERVICE_SID);
            $chatGrant->setEndpointId("BagZag".$identity.$device_token);

            if($device_type == 'I') {
                $chatGrant->setpushCredentialSid(PUSH_SID_APN);
            } else {
                $chatGrant->setpushCredentialSid(PUSH_SID_FCM);
            }

            // Add grant to token
            $token->addGrant($chatGrant);

            $strtoken=$token->toJWT();
        } catch(Exception $e){
            $strtoken="";
        }
        return $strtoken;
    }

    /*
    * Get all channels 
    */
    public function getAllChannel()
    {
        try{
            return $this->twilio->conversations->v1->conversations->read(20);
        } catch(Exception $e) {
            return array();
        }
    }

    /*
    ** Remove all channels 
    */
    public function removeAllChannel()
    {
        try{
            $conversations = $this->twilio->conversations->v1->conversations->read(20);    
            if(!empty($conversations)){
                foreach ($conversations as $record) {
                    $this->twilio->conversations->v1->conversations($record->sid)->delete();
                }
            }
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        } 
    }

    /*
    *get user conversations list
    */
    public function getAllUserChannel($user_identity)
    {
        try{
            return $this->twilio->conversations->v1->users($user_identity)->userConversations->read(20);
        } catch(Exception $e) {
            return array();
        }
    }

    /*
    *get user conversations data
    */
    public function getuserconversationsdata($channel_sid,$user_sid)
    {
        try{
            return $this->twilio->conversations->v1->users($user_sid)->userConversations($channel_sid)->fetch();
        } catch(Exception $e) {
            return array();
        }
    }

    /*
    *get conversations last message data
    */
    public function getuserconversationslastmsg($channel_sid)
    {
        try{
            $messages = $this->twilio->conversations->v1->conversations($channel_sid)->messages->read(["order" => "desc"], 1);
            foreach ($messages as $record) {
                return $record->body;
                break;
            }
        } catch(Exception $e) {
            return "";
        }
    }

    /*
    * Remove user bindings
    */
    public function removeUserBindings($user_identity)
    {
        try{
            $bindings = $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->bindings->read();
            if(!empty($bindings)){
                foreach ($bindings as $record) {
                    if($record->identity == $user_identity){
                        $this->twilio->conversations->v1->services(TWILLIO_SERVICE_SID)->bindings($record->sid)->delete();
                    }
                }
            }
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }
}