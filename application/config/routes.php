<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller']   					= 'website';
$route['login']   								= 'website/login';
$route['sign-up']   							= 'website/signup';
$route['otp-verification']   					= 'website/otp_verification';
$route['create-new-password']   				= 'website/create_password';
$route['forget-password']      					= 'website/forget_password';
$route['upload-video']      					= 'website/upload_video';
$route['add-student']      	    				= 'website/add_student';
$route['edit-student/(:any)']   				= 'website/edit_student/$1';

$route['dashboard']   							= 'customer/dashboard';
$route['notification']   						= 'customer/dashboard/notification';
$route['my-profile']   							= 'customer/dashboard/my_profile';
$route['my-create-new-password']   				= 'customer/dashboard/my_create_new_password';

$route['upload-speeches']   					= 'customer/service/upload_speeches';
$route['online-class']   						= 'customer/service/online_class';
$route['online-class-detail/(:any)']			= 'customer/service/online_class_detail/$1';
$route['contest-list']   						= 'customer/service/contest_list';
$route['contests-live-details/(:any)']			= 'customer/service/contests_live_details/$1';
$route['my-leaderboard']   						= 'customer/service/my_leaderboard';
$route['sponsors']   							= 'customer/service/sponsors';
$route['score-card-for-contest/(:any)']   		= 'customer/service/score_card_for_contest/$1';
$route['prize-list']   						    = 'customer/service/prize_list';
$route['prize-detail/(:any)']   				= 'customer/service/prize_detail/$1';


$route['subscription']   						= 'customer/user/subscription';
$route['sub-profile-list']   					= 'customer/user/sub_profile_list';
$route['my-payments']   						= 'customer/user/my_payments';
$route['my-speeches']   						= 'customer/user/my_speeches';
$route['my-evaluations']   						= 'customer/user/my_evaluations';
$route['enrolled-classes']   					= 'customer/user/enrolled_classes';
$route['my-contest-online']   					= 'customer/user/my_contest_online';
$route['my-contest-detail/(:any)']  			= 'customer/user/my_contest_detail/$1';
$route['won-contest-details/(:any)']			= 'customer/user/won_contest_detail/$1';
$route['my-winnings']   						= 'customer/user/my_winnings';
$route['video-library']   						= 'customer/user/video_library';
$route['my-owned-video']   						= 'customer/user/my_owned_video';
$route['my-reward-points']   					= 'customer/user/my_reward_points';
$route['gifted-enrolled-class']   				= 'customer/user/gifted_enrolled_class';
$route['wish-list']   							= 'customer/user/wish_list';
$route['purchase-promo-codes']   				= 'customer/user/purchase_promo_codes';
$route['my-speeches-inner/(:any)']  			= 'customer/user/my_speeches_inner/$1';
$route['add-sub-profile']   	    			= 'customer/user/add_sub_profile';
$route['edit-sub-profile/(:any)']   			= 'customer/user/edit_sub_profile/$1';
$route['my-add-debit-card']   	    			= 'customer/user/my_add_debit_card';
$route['my-evaluations-details/(:any)']    		= 'customer/user/my_evaluations_details/$1';
$route['subscription-active']    				= 'customer/user/subscription_active';
$route['cart/(:any)']    						= 'customer/user/cart/$1';
$route['cart']    						        = 'customer/user/cart';
$route['add-debit-card/(:any)']    				= 'customer/user/add_debit_card/$1';
$route['video-library-details/(:any)']    		= 'customer/user/video_library_details/$1';
$route['purchased-video-library-details/(:any)']= 'customer/user/purchased_video_library_details/$1';
$route['purchase-promo-details/(:any)']    		= 'customer/user/purchase_promo_details/$1';
$route['submission-video-for-contest/(:any)']   = 'customer/user/submission_video_for_contest/$1';
$route['points-history']    					= 'customer/user/points_history';
$route['gift-class/(:any)']    	      			= 'customer/user/gift_class/$1';
$route['gifted-enrolled-class-details/(:any)']  = 'customer/user/gifted_enrolled_class_details/$1';
$route['received-from-class-details/(:any)']    = 'customer/user/received_from_class_details/$1';
$route['enrolled-class-details/(:any)']  		= 'customer/user/enrolled_class_details/$1';
$route['joined-class/(:any)']  					= 'customer/user/joined_class/$1';
$route['joined-contest/(:any)']  					= 'customer/user/joined_contest/$1';
$route['upgrade-profile/(:any)']  				= 'customer/user/upgrade_profile/$1';
$route['notification-setting']                  = 'customer/user/notification_setting';
$route['my-suggested-details/(:any)']           = 'customer/user/my_suggested_details/$1';
$route['notification-settings']                 = 'website/notification_setting';
$route['student_data_id/(:any)']                = 'customer/user/student_data_id/$1';



$route['privacy-policy']   						= 'website/privacy_policy';
$route['terms-conditions']   					= 'website/terms_conditions';
$route['about-us']   							= 'website/about_us';
$route['help-and-faq']   						= 'website/help_and_faq';
$route['subscription']   						= 'website/subscription';
$route['contact']   							= 'website/contact';


$route['failure']               				= "home/failure";
$route['success']               				= "home/success";

//HMVC structure 
$route['authpanel']            				= 'authpanel/login';
$route['404_override']         				= '';
$route['translate_uri_dashes'] = FALSE;

$route['purchase-promo-codes-list']   			= 'customer/user/purchase_promo_codes_list';
$route['purchase-promo-details-list/(:any)']           = 'customer/user/purchase_promo_details_list/$1';