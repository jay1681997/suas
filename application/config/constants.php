<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')                 OR define('EXIT_SUCCESS', 0);
defined('EXIT_ERROR')                   OR define('EXIT_ERROR', 1);
defined('EXIT_CONFIG')                  OR define('EXIT_CONFIG', 3);
defined('EXIT_UNKNOWN_FILE')            OR define('EXIT_UNKNOWN_FILE', 4);
defined('EXIT_UNKNOWN_CLASS')           OR define('EXIT_UNKNOWN_CLASS', 5);
defined('EXIT_UNKNOWN_METHOD')          OR define('EXIT_UNKNOWN_METHOD', 6);
defined('EXIT_USER_INPUT')              OR define('EXIT_USER_INPUT', 7);
defined('EXIT_DATABASE')                OR define('EXIT_DATABASE', 8);
defined('EXIT__AUTO_MIN')               OR define('EXIT__AUTO_MIN', 9);
defined('EXIT__AUTO_MAX')               OR define('EXIT__AUTO_MAX', 125);
/*
|
|Set The project constant value
|
*/
// Admin panel
defined('PROJECT_NAME') 				OR define('PROJECT_NAME', 'Stand Up & Speak');
// defined('ADMIN_EMAIL')  				OR define('ADMIN_EMAIL', 'hlis.infosystem@gmail.com');
defined('ADMIN_EMAIL')  				OR define('ADMIN_EMAIL', 'app@standupandspeak.com');
// defined('ADMIN_EMAIL')  				OR define('ADMIN_EMAIL', 'yojosac783@dmonies.com');
defined('ADMIN_SESSION_NAME')           OR define('ADMIN_SESSION_NAME', 'suassuperadmindata');
defined('ADMIN_LOCK_NAME')				OR define('ADMIN_LOCK_NAME', 'suassuperadminlock');
defined('ADMIN_TIMEZONE')				OR define('ADMIN_TIMEZONE', 'suassuperadmintimezone');
defined('ADMIN_DATETIMEFORMAT')			OR define('ADMIN_DATETIMEFORMAT', 'Y-m-d h:i a');
defined('ADMIN_SHORTDATE')				OR define('ADMIN_SHORTDATE', 'Y-m-d');
defined('ADMIN_SHORTTIME')				OR define('ADMIN_SHORTTIME', 'h:i a');
defined('ADMIN_LONGDATE')				OR define('ADMIN_LONGDATE', 'Y-m-d h:i a');
defined('CURRENCY')						OR define('CURRENCY', '$');

//Mail Details
// defined('SMTP_USER')				    OR define('SMTP_USER', 'hlis.infosystem@gmail.com');
// defined('SMTP_PASS')				    OR define('SMTP_PASS', 'jalaekjbkvswesci');
defined('SMTP_USER')				    OR define('SMTP_USER', 'sonali.hyperlinkinfosystem@gmail.com');
defined('SMTP_PASS')				    OR define('SMTP_PASS', 'bwjlwureailswgwz');

// Password Encryption Keys
defined('KEY_256')      	   	        OR define('KEY_256','msTuWsuwKZOxarPlK8IykWWKHX4S243O');	
defined('IV')      			   	        OR define('IV','msTuWsuwKZOxarPl');
defined('GOOGLE_API_KEY')      	        OR define('GOOGLE_API_KEY',''); 
defined('AGORA_APP_KEY')      	        OR define('AGORA_APP_KEY','9a66bf67924441818932f9fccc95a960'); 
// AIzaSyC8iM9FUEtieiufRp_pvJvRvC8lddXGdJc          

defined('THEME_COLOR') 					OR define('THEME_COLOR','rgb(21, 21, 18)');

defined('LOGO_NAME')      		        OR define('LOGO_NAME','assets/images/app_icon.png');
defined('S3_BUCKET_ROOT')  				OR define('S3_BUCKET_ROOT', 'https://susalive.s3.amazonaws.com/');
defined('S3_BUCKET_NAME')  				OR define('S3_BUCKET_NAME', 'susalive');
defined('S3_BUCKET_KEY')  				OR define('S3_BUCKET_KEY', 'AKIAS42QDYETINJ4ZH5P');
defined('S3_BUCKET_SECRET')  			OR define('S3_BUCKET_SECRET', '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');
defined('THUMB_DIR')  		            OR define('THUMB_DIR', 'assets/media/');
defined('ADMIN_IMAGE')           	    OR define('ADMIN_IMAGE','admin/');
defined('USER_IMAGE')           	    OR define('USER_IMAGE','user/');
defined('STUDENT_IMAGE')           	    OR define('STUDENT_IMAGE','student/');
defined('PROGRAM_IMAGE')           	    OR define('PROGRAM_IMAGE','program/');
defined('CONTEST_IMAGE')           	    OR define('CONTEST_IMAGE','contest/');
defined('SPEECH_IMAGE')           	    OR define('SPEECH_IMAGE','speech/');
defined('WINNER_IMAGE')           	    OR define('WINNER_IMAGE','winner/');
defined('PRODUCT_IMAGE')           	    OR define('PRODUCT_IMAGE','product/');
defined('PRIZE_IMAGE')           	    OR define('PRIZE_IMAGE','prize/');
defined('SPONSOR_LOGO')           	    OR define('SPONSOR_LOGO','sponsor/');
defined('PROMOCODE_IMAGE')           	OR define('PROMOCODE_IMAGE','promocode/');
defined('CLASS_IMAGE')           	    OR define('CLASS_IMAGE','class/');  
defined('VIDEOLIBRARY_IMAGE')           OR define('VIDEOLIBRARY_IMAGE','videolibrary/'); 
defined('LEADERBOARD_IMAGE')            OR define('LEADERBOARD_IMAGE','leaderboard/'); 
defined('BANNER_IMAGE')                 OR define('BANNER_IMAGE','banner/'); 
defined('SUBSCRIPTION_IMAGE')           OR define('SUBSCRIPTION_IMAGE','subscription/');
defined('HOME_VIDEO')					OR define("HOME_VIDEO",'home_video/');
// defined("COUNTRY_FLAG")                 OR defined("COUNTRY_FLAG",'country_flag/');


//Stripe Payment
defined('PAYMENT_SECRET_KEY')           OR define('PAYMENT_SECRET_KEY','sk_test_51JcX0GKnTVBhpWS0vvO37UnG1PQNvTNWZ4gfr6AL4kNlD4a0xyOwTVQQgO0Za3u5VolsjO1EQQR4CErw4Q11AjYD00nj5Zdiof'); 
defined('PAYMENT_PUBLISHABLE_KEY')      OR define('PAYMENT_PUBLISHABLE_KEY','pk_test_51BTUDGJAJfZb9HEBwDg86TN1KNprHjkfipXmEDMb0gSCassK5T3ZfxsAbcgKVmAIXF7oZ6ItlZZbXO6idTHE67IM007EwQ4uN3'); 

//Live Stripe Payment key
// defined('PAYMENT_SECRET_KEY')           OR define('PAYMENT_SECRET_KEY','pk_live_51JcX0GKnTVBhpWS0mnasKjVD2OKhCVbky8JGgziZeTMIwpWigKHEISiJimZhSGlQb5VI6SJFfaxtts41GyVgpeao00r4cA3Edt'); 
// defined('PAYMENT_PUBLISHABLE_KEY')      OR define('PAYMENT_PUBLISHABLE_KEY','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'); 

defined('TWILLIO_ACCOUNT_SID')          OR define('TWILLIO_ACCOUNT_SID','ACf8057d875c1bbcdc0ec0c3d6162e914c');
defined('TWILLIO_ACCOUNT_AUTH')         OR define('TWILLIO_ACCOUNT_AUTH','5e74c578b64d714940b06b4028b64229');
defined('TWILLIO_ACCOUNT_PHONE')        OR define('TWILLIO_ACCOUNT_PHONE','+13466448906');
// defined("BASE_IMAGE_URL")                  OR define('BASE_IMAGE_URL', 'https://susalive.s3.us-east-1.amazonaws.com/app_icon.png');
defined("BASE_IMAGE_URL")                  OR define('BASE_IMAGE_URL', 'http://52.7.79.220/assets/images/app_icon.png');