<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(APPPATH . "views/website/inc/style.php"); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/agora.css">

</head>

<body>
    <div class="wrapper">
        <?php include(APPPATH . "views/website/inc/header.php"); ?>
        <!--start-body-->
        <section class=" ">
            <div class="container">
                <!--start-nav-->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Join Contest</li>
                    </ol>
                </nav>
                <!--end-nav-->

                <!--start-row-->
                <div class=" ">
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- <p id="local-player-name" class="player-name"></p> -->
                            <div id="local-player" class="player" style="background-color: #a3a6a3;"></div>
                        </div>
                        <div class="col-lg-6">
                            <form id="join-form" name="join-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input id="appid" type="hidden" placeholder="enter appid" value="<?php echo AGORA_APP_KEY; ?>">
                                        <input id="token" type="hidden" placeholder="enter token" value="<?php echo $token; ?>">
                                        <input id="channel" type="hidden" placeholder="enter channel name" value="<?php echo $channel; ?>">
                                        <input id="accountName" type="hidden" placeholder="enter channel name" value="<?php echo $accountName; ?>">
                                        <input id="uid" type="hidden" placeholder="Enter the user ID" value="<?php echo $id; ?>">
                                        <input id="admin_id" name="admin_id" type="hidden" placeholder="Enter the user ID" value="<?php echo $host_id; ?>">

                                        <button id="mute-audio" type="button" class="btn btn-primary btn-sm remoteMicrophone" style="min-width: 50px !important;margin-left: 100px;border-radius: 50%;background-color: #a3a6a3;"><i class='fas fa-microphone'></i></button>
                                        <button id="mute-video" type="button" class="btn btn-primary btn-sm remoteCamera" style="min-width: 50px !important;margin-left: 18px;border-radius: 50%;background-color: #a3a6a3;"><i class='fas fa-video'></i></button>
                                    </div>

                                    <div class="col-md-3">
                                        <button id="host-join" name="host-join" type="submit" class="btn btn-primary btn__rounded " style="margin-left:0%">Join Contest</button>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn__primary btn__rounded bg-danger" data-toggle="modal" data-target="#Leave"><span>Leave</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><br><br>
                    <h6>People Joined Here</h6><br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="other-user-pic" id="remote-playerlist"></div>
                        </div>
                    </div>
                </div>
                <!--end-row-->
            </div>
        </section>
        <!--end-body-->


        <!--start-modal-->
        <div class="modal fade" id="Leave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-modal">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94" class="mb-25">
                        <div class="content">
                            <h4>Contest In Progress</h4>
                            <p class="fz-16">There are no make-up contest or
                                refunds if you leave the
                                contest in progress.</p>
                            <!-- <p class="fz-16">Are you sure you want to leave 
                            the class in progress?</p> -->
                        </div>
                    </div>
                    <input id="accountName" type="hidden" placeholder="enter channel name" value="<?php echo $accountName; ?>">
                    <div class="modal-footer border-0">
                        <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn__primary btn__rounded bg-danger" id="if-leave">Leave Now</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end-modal-->
        <div class="col">
            <!-- All Users List with Mute Controls -->
            <!-- <div class="all-users-text my-3">
                <h5 class="text-decoration-underline">All Users:</h5>
                <div class="all-users" id="all-users">
                    <ul id="insert-all-users">
                    </ul>
                </div>
            </div> -->
            <!--start-sucessful-Modal-->
            <div class="modal fade" id="sucessful-leave" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content rounded-modal">
                        <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="<?php echo base_url(); ?>website_assets/images/Group -33.png" alt="Group -94" class="mb-25">
                            <div class="content">
                                <h4>Contest In Progress</h4>
                                <!-- <p class="fz-16">There are no make-up classes or
                                refunds if you leave the 
                                class in progress.</p> -->
                                <p class="fz-16">Are you sure you want to leave
                                    the class in progress?</p>
                            </div>
                        </div>
                        <div class="modal-footer border-0 justify-content-center">
                            <div class="modal-footer border-0">
                                <button type="button" class="btn btn__primary btn__rounded bg-transparent text-gray" data-dismiss="modal"> No</button>
                                <a href="<?php echo base_url(); ?>my-contest-detail/<?php echo base64_encode($contest_id); ?>" class="btn btn__primary btn__rounded bg-danger" id="leave">Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-modal-->

            <?php include(APPPATH . "views/website/inc/footer.php"); ?>
        </div><!-- /.wrapper -->
        <?php include(APPPATH . "views/website/inc/script.php"); ?>

        <script src="https://download.agora.io/sdk/release/AgoraRTC_N.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/agora-rtm-sdk@1.3.1/index.js"></script>
        <script src="https://cdn.agora.io/rtmsdk/release/AgoraRTMTokenBuilder-1.4.0.js"></script>
        <script>
            //set button id on click to hide first modal
            $("#if-leave").on("click", function() {
                $('#Leave').modal('hide');
            });
            //trigger next modal
            $("#if-leave").on("click", function() {
                $('#sucessful-leave').modal('show');
            });
        </script>
        <script src="https://download.agora.io/sdk/release/AgoraRTC_N.js"></script>
        <script type="text/javascript">
            window.onload = function() {
                var is_block_contest = "<?php echo $order_data['is_block_contest']; ?>";
                if (is_block_contest == 1) {
                    document.querySelector('#host-join').disabled = true;
                }
            };
            var user_ids = [];
            var admin_id = "<?php echo $host_id; ?>";
            var is_mute_contest = "<?php echo $order_data['is_mute_contest']; ?>";
            var is_video_contest = "<?php echo $order_data['is_video_contest']; ?>";
            // create Agora client
            var client = AgoraRTC.createClient({
                mode: "live",
                codec: "vp8"
            });
            var localTracks = {
                videoTrack: null,
                audioTrack: null
            };

            var localTrackState = {
                videoTrackMuted: false,
                audioTrackMuted: false
            }

            var remoteUsers = {};
            // Agora client options
            var options = {

                appid: '<?php echo AGORA_APP_KEY; ?>',
                channel: '<?php echo $channel; ?>',
                uid: '<?php echo $id; ?>',
                token: '<?php echo $token; ?>',
                role: "audience", // host or audience
                audienceLatency: 2
            };

            let initRtm = async (name) => {
                rtmClient = AgoraRTM.createInstance('<?php echo AGORA_APP_KEY; ?>');
                await rtmClient.login({
                    'uid': rtmUid,
                    'token': token
                })
                Channel = rtmClient.createChannel('<?php echo $channel; ?>');
                await channel.join();
            }
            // the demo can auto join channel with params in url
            $(() => {
                var urlParams = new URL(location.href).searchParams;
                options.appid = urlParams.get("appid");
                options.channel = urlParams.get("channel");
                options.token = urlParams.get("token");
                options.uid = urlParams.get("uid");
                if (options.appid && options.channel) {
                    $("#uid").val(options.uid);
                    $("#appid").val(options.appid);
                    $("#token").val(options.token);
                    $("#channel").val(options.channel);
                    $("#join-form").submit();
                }
            })

            $("#host-join").click(function(e) {
                options.role = "host"
            })

            $("#lowLatency").click(function(e) {
                options.role = "audience"
                options.audienceLatency = 1
                $("#join-form").submit()
            })

            $("#ultraLowLatency").click(function(e) {
                options.role = "audience"
                options.audienceLatency = 2
                $("#join-form").submit()
            })

            $("#join-form").submit(async function(e) {
                e.preventDefault();
                $("#host-join").attr("disabled", true);
                $("#audience-join").attr("disabled", true);
                // RTMJoin();
                try {
                    options.appid = $("#appid").val();
                    options.token = $("#token").val();
                    options.channel = $("#channel").val();
                    options.uid = Number($("#uid").val());
                    // RTMJoin();
                    await join();
                    if (options.role === "host") {
                        $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                        if (options.token) {
                            $("#success-alert-with-token").css("display", "block");
                        } else {
                            $("#success-alert a").attr("href", `index.html?appid=${options.appid}&channel=${options.channel}&token=${options.token}`);
                            $("#success-alert").css("display", "block");
                        }
                    }
                } catch (error) {
                    console.error(error);
                } finally {
                    $("#leave").attr("disabled", false);
                }
            })

            $("#leave").click(function(e) {
                leave();
            })

            $("#mute-audio").click(function(e) {
                if (!localTrackState.audioTrackMuted) {
                    muteAudio();
                } else {
                    unmuteAudio();
                }
            });

            $("#mute-video").click(function(e) {
                if (!localTrackState.videoTrackMuted) {
                    muteVideo();
                } else {
                    unmuteVideo();
                }
            })

            async function join() {
                // create Agora client
                if (options.role === "audience") {
                    client.setClientRole(options.role, {
                        level: options.audienceLatency
                    });
                    // add event listener to play remote tracks when remote user publishs.
                    client.on("user-published", handleUserPublished);
                    client.on("user-unpublished", handleUserUnpublished);
                } else {
                    client.setClientRole(options.role);
                    client.on("user-published", handleUserPublished);
                    client.on("user-unpublished", handleUserUnpublished);
                }

                // join the channel
                options.uid = await client.join(options.appid, options.channel, options.token || null, options.uid || null);

                if (options.role === "host") {
                    // create local audio and video tracks
                    localTracks.audioTrack = await AgoraRTC.createMicrophoneAudioTrack();
                    localTracks.videoTrack = await AgoraRTC.createCameraVideoTrack();
                    // play local video track
                    localTracks.videoTrack.play("local-player");

                    $("#local-player-name").text(`localTrack(${options.uid})`);
                    // publish local tracks to channel
                    await client.publish(Object.values(localTracks));
                    document.getElementById("uid").value = options.uid;
                    RTMJoin();
                    console.log("publish success");
                    if (is_mute_contest == 1) {
                        muteAudio();
                        document.getElementById("mute-audio").disabled = true;
                    }
                    if (is_video_contest == 1) {
                        muteVideo();
                        document.getElementById("mute-video").disabled = true;
                    }
                }
            }



            async function leave() {
                for (trackName in localTracks) {
                    var track = localTracks[trackName];
                    if (track) {
                        track.stop();
                        track.close();
                        localTracks[trackName] = undefined;
                    }
                }

                // remove remote users and player views
                remoteUsers = {};
                $("#remote-playerlist").html("");

                // leave the channel
                await client.leave();

                $("#local-player-name").text("");
                $("#host-join").attr("disabled", false);
                $("#audience-join").attr("disabled", false);
                $("#leave").attr("disabled", true);
                console.log("client leaves channel success");
            }

            async function subscribe(user, mediaType) {
                // RTMJoin();
                console.log("HOSTING================================");
                const uid = user.uid;
                // subscribe to a remote user
                console.log(user);
                console.log("HOSTING========+++++++++++++++===");
                console.log(mediaType);
                console.log("HOSTING================================");
                await client.subscribe(user, mediaType);

                if (admin_id == uid) {
                    var user_type = "admin";
                    var path = "<?php echo S3_BUCKET_ROOT . ADMIN_IMAGE; ?>";
                } else {
                    var user_type = "user";
                    var path = "<?php echo S3_BUCKET_ROOT . USER_IMAGE; ?>";
                }
                $.ajax({
                    // url: SITE_URL + "customer/user/user_data/" + uid,
                    url: "<?php echo base_url(); ?>customer/user/user_data/" + uid + "/" + user_type,
                    type: "POST",
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('errr');
                    },
                    success: function(message1) {
                        var dataa = JSON.parse(message1);
                        if (mediaType === 'video') {

                            const player = $(`
                  <div id="player-wrapper-${uid}">
                    <div id="player-${uid}" class="player" style="background-image: url('${path}${dataa.profile_image}');"></div>
                  </div>
                `);
                            console.log("player**********************************");
                            console.log(player)
                            if (user_ids.includes(user.uid)) {
                                $(`#player-wrapper-${uid}`).remove();
                            }
                            $("#remote-playerlist").append(player);
                            user_ids.push(uid);
                            user.videoTrack.play(`player-${uid}`, {
                                fit: "contain"
                            });
                            // $("#remote-playerlist").append(player);
                            user.videoTrack.play(`player-${uid}`, {
                                fit: "contain"
                            });
                        }
                        if (mediaType === 'audio') {
                            user.audioTrack.play();
                        }
                    },
                });
                console.log("subscribe success");

            }

            function handleUserPublished(user, mediaType) {

                //print in the console log for debugging 
                console.log('"user-published" event for remote users is triggered.');

                const id = user.uid;
                remoteUsers[id] = user;
                subscribe(user, mediaType);
            }

            client.on('user-left', function(evt) {
                var uid = evt.uid;
                console.log('User left the channel: ' + uid);
                if (admin_id == uid) {
                    leave();
                    window.location = "<?php echo base_url(); ?>my-contest-detail/<?php echo base64_encode($contest_id); ?>";
                }
                $(`#player-wrapper-${uid}`).remove();
            });

            function handleUserUnpublished(user, mediaType) {

                //print in the console log for debugging 
                console.log('"user-unpublished" event for remote users is triggered.');
            }

            function hideMuteButton() {
                $("#mute-video").css("display", "none");
                $("#mute-audio").css("display", "none");
            }

            function showMuteButton() {
                $("#mute-video").css("display", "inline-block");
                $("#mute-audio").css("display", "inline-block");
            }

            async function muteAudio() {
                if (!localTracks.audioTrack) return;
                /**
                 * After calling setMuted to mute an audio or video track, the SDK stops sending the audio or video stream. Users whose tracks are muted are not counted as users sending streams.
                 * Calling setEnabled to disable a track, the SDK stops audio or video capture
                 */
                await localTracks.audioTrack.setMuted(true);
                localTrackState.audioTrackMuted = true;
                $("#mute-audio").html("<i class='fas fa-microphone-slash'></i>");
            }

            async function muteVideo() {
                if (!localTracks.videoTrack) return;
                await localTracks.videoTrack.setMuted(true);
                localTrackState.videoTrackMuted = true;
                $("#mute-video").html("<i class='fas fa-video-slash'></i>");
            }

            async function unmuteAudio() {
                if (!localTracks.audioTrack) return;
                await localTracks.audioTrack.setMuted(false);
                localTrackState.audioTrackMuted = false;
                $("#mute-audio").html("<i class='fas fa-microphone'></i>");
            }

            async function unmuteVideo() {
                if (!localTracks.videoTrack) return;
                await localTracks.videoTrack.setMuted(false);
                localTrackState.videoTrackMuted = false;
                $("#mute-video").html("<i class='fas fa-video'></i>");
            }

            async function RTMJoin() {
                // Create Agora RTM client
                const clientRTM = AgoraRTM.createInstance($("#appid").val(), {
                    enableLogUpload: false
                });
                const userId = document.getElementById("uid").value;

                $.ajax({
                    url: "<?php echo base_url(); ?>customer/user/create_token/" + userId,
                    type: "POST",
                    error: function(jqXHR, textStatus, errorThrown) {
                        // showNotification('alert-danger',errorThrown,'top','center','zoomIn animated','zoomOut animated');
                        console.log('errr');
                    },
                    success: function(message) {
                        clientRTM.login({
                            uid: userId,
                            "token": JSON.parse(message).token
                        }).then(() => {
                            isLoggedIn = true;
                            // RTM Channel Join
                            var channelName = $('#channel').val();
                            channel = clientRTM.createChannel(channelName);
                            channel.join().then(() => {
                                // Get all members in RTM Channel
                                channel.getMembers().then((memberNames) => {
                                    console.log("All members in the channel are as follows: ");
                                    console.log(memberNames);

                                });
                                // Send peer-to-peer message for audio muting and unmuting
                                $(document).on('click', '.remoteMicrophone', function() {
                                    fullDivId = $(this).attr('id');
                                    peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                    console.log("Remote microphone button pressed.");
                                    let peerMessage = "audio";
                                    clientRTM.sendMessageToPeer({
                                            text: peerMessage
                                        },
                                        peerId,
                                    ).then(sendResult => {
                                        if (sendResult.hasPeerReceived) {
                                            console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                        } else {
                                            console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                        }
                                    })
                                });
                                // Send peer-to-peer message for video muting and unmuting
                                $(document).on('click', '.remoteCamera', function() {
                                    fullDivId = $(this).attr('id');
                                    peerId = fullDivId.substring(fullDivId.indexOf("-") + 1);
                                    console.log("Remote video button pressed.");
                                    let peerMessage = "video";
                                    clientRTM.sendMessageToPeer({
                                            text: peerMessage
                                        },
                                        peerId,
                                    ).then(sendResult => {
                                        if (sendResult.hasPeerReceived) {
                                            console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                        } else {
                                            console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                        }
                                    })
                                });
                                $(document).on('click', '.leave_user', function() {
                                    fullDivId = $(this).attr('id');
                                    peerId = '<?php echo $host_id; ?>';
                                    console.log("Remote video button pressed.");
                                    console.log(peerId);
                                    let peerMessage = "leave";
                                    clientRTM.sendMessageToPeer({
                                            text: peerMessage
                                        },
                                        peerId,
                                    ).then(sendResult => {
                                        if (sendResult.hasPeerReceived) {
                                            console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                        } else {
                                            console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                        }
                                    })
                                });

                                function sendMessage(peerMessage, peerId) {
                                    clientRTM.sendMessageToPeer({
                                            text: peerMessage
                                        },
                                        peerId,
                                    ).then(sendResult => {
                                        if (sendResult.hasPeerReceived) {
                                            console.log("Message has been received by: " + peerId + " Message: " + peerMessage);
                                        } else {
                                            console.log("Message sent to: " + peerId + " Message: " + peerMessage);
                                        }
                                    })
                                }
                                channel.on('MemberJoined', function() {
                                    // Get all members in RTM Channel
                                    channel.getMembers().then((memberNames) => {
                                        console.log("New member joined so updated list is: ");
                                        console.log(memberNames);
                                        var newHTML = $.map(memberNames, function(singleMember) {
                                            console.log(singleMember);
                                            console.log("JOIN USER");
                                        });
                                    });
                                })
                                // Display messages from peer
                                clientRTM.on('MessageFromPeer', function({
                                    text
                                }, peerId) {
                                    if (text == 'Mute') {
                                        muteAudio();
                                        sendMessage(text, peerId);
                                        document.getElementById("mute-audio").disabled = true;
                                    } else if (text == "Unmute") {
                                        unmuteAudio();
                                        sendMessage(text, peerId);
                                        document.getElementById("mute-audio").disabled = false;
                                    } else if (text == "VideoOn") {
                                        unmuteVideo();
                                        sendMessage(text, peerId);
                                        document.getElementById("mute-video").disabled = false;
                                    } else if (text == "VideoOff") {
                                        muteVideo();
                                        sendMessage(text, peerId);
                                        document.getElementById("mute-video").disabled = true;
                                    } else if (text == "Remove") {
                                        leave();
                                        sendMessage(text, peerId);
                                        // location.reload();

                                        setTimeout(function() {
                                            // window.location.reload(1);
                                            window.location = "<?php echo base_url(); ?>my-contest-detail/<?php echo base64_encode($contest_id); ?>";
                                        }, 1000);
                                    }

                                })
                                // Display channel member joined updated users
                                channel.on('MemberJoined', function() {
                                    // Get all members in RTM Channel
                                    channel.getMembers().then((memberNames) => {
                                        console.log("New member joined so updated list is: ");
                                        console.log(memberNames);
                                    });
                                })
                                // Display channel member left updated users
                                channel.on('MemberLeft', function() {
                                    // Get all members in RTM Channel
                                    channel.getMembers().then((memberNames) => {
                                        console.log("A member left so updated list is: ");
                                        console.log(memberNames);
                                    });
                                });
                            }).catch(error => {
                                console.log('AgoraRTM client channel join failed: ', error);
                            }).catch(err => {
                                console.log('AgoraRTM client login failure: ', err);
                            });
                        });
                    }
                })
                // Logout
                document.getElementById("leave").onclick = async function() {

                    console.log("Client logged out of RTM.");
                    await clientRTM.logout();
                }
            }
        </script>
</body>

</html>