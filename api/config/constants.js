var environment = require("../utilities/environment");
let Globals = {

    //App Details
    'DOMAIN'               : 'www.suas.com',
    'APP_NAME'             : 'SUAS App ',
    'LOGO'                 : 'assets/images/app_icon.png',
    'ARROW_IMAGE'          : 'assets/images/arrow-right.gif',
    'BASE_URL'             : environment.protocol+'://'+environment.ipaddress+'/api/',
    'BASE_URL_WITHOUT_API' : environment.protocol+'://'+environment.ipaddress+'/',
    'PORT_BASE_URL'        : environment.protocol+'://'+environment.ipaddress+':'+environment.port+'/',
    
    //Encryption Detail
    'API_KEY'              : 'SUASAPP08022022',
    'KEY'                  : 'msTuWsuwKZOxarPlK8IykWWKHX4S243O',
    'IV'                   : 'msTuWsuwKZOxarPl',
   
    //S3 Bucket Detail
    // 'S3_BUCKET_ROOT'       : 'https://susalive.s3.amazonaws.com/',
    'S3_BUCKET_ROOT'       : 'https://susalive.s3.us-east-1.amazonaws.com/',
    'USER_IMAGE'           : 'user/',
    'ADMIN_IMAGE'          : 'admin/',
    'STUDENT_IMAGE'        : 'student/',
    'PROGRAM_IMAGE'        : 'program/',
    'CONTEST_IMAGE'        : 'contest/',
    'SPEECH_IMAGE'         : 'speech/',
    'WINNER_IMAGE'         : 'winner/',
    'PRODUCT_IMAGE'        : 'product/',
    'PRIZE_IMAGE'          : 'prize/',
    'SPONSOR_LOGO'         : 'sponsor/',
    'PROMOCODE_IMAGE'      : 'promocode/',
    'VIDEOLIBRARY_IMAGE'   : 'videolibrary/',
    'CARD_IMAGE'           : 'card/',
    'CLASS_IMAGE'          : 'class/',
    'LEADERBOARD_IMAGE'    : 'leaderboard/',
    'BANNER_IMAGE'         : 'banner/',
    'SUBSCRIPTION_IMAGE'   : 'subscription/',
    'HOME_VIDEO'           : 'home_video/',
    "COUNTRY_FLAG_IMAGE"   : 'country_flag/',
    //Email Details
    'EMAIL_ID'             : 'sonali.hyperlinkinfosystem@gmail.com',
    'EMAIL_PASSWORD'       : 'bwjlwureailswgwz',
    'TO_EMAIL'             : 'app@standupandspeak.com',
    // 'TO_EMAIL'             : 'yojosac783@dmonies.com',

    //Limit
    'PER_PAGE'             :  10,

    //p8 credentials
    'BUNDLE_ID'            : 'com.suas.app',
    'KEY_ID'               : 'W2XDMW9R6D',
    'TEAM_ID'              : 'U3QF765359',
    'PUSH_KEY'             : '',    
    'P8_CERTIFICATE_NAME'  : './pem/AuthKey_W2XDMW9R6D.p8',

       //Stripe Payment
    'PAYMENT_SECRET_KEY'      : 'sk_test_51JcX0GKnTVBhpWS0vvO37UnG1PQNvTNWZ4gfr6AL4kNlD4a0xyOwTVQQgO0Za3u5VolsjO1EQQR4CErw4Q11AjYD00nj5Zdiof',
    'PAYMENT_PUBLISHABLE_KEY' : 'pk_test_51JcX0GKnTVBhpWS03ldK4hlF6TVr2KmSjmzfYy4yRVhDbmSbHMWc6vuKPV6uRY1yej6XF4f767MEGs9LDEuHFdTo00CT1mXxfI',
    
    //Live Stripe Payment Key
    //'PAYMENT_SECRET_KEY'      : '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',
    //'PAYMENT_PUBLISHABLE_KEY' : 'pk_live_51JcX0GKnTVBhpWS0mnasKjVD2OKhCVbky8JGgziZeTMIwpWigKHEISiJimZhSGlQb5VI6SJFfaxtts41GyVgpeao00r4cA3Edt',


    //Agora API
    'APP_ID'                  : '9a66bf67924441818932f9fccc95a960',
    'APP_CERTIFICATE'         : '6f1d7020c7024163b88fe4443ce11513',

     //twilio Server details
    "TWILLIO_ACCOUNT_SID"     :"ACf8057d875c1bbcdc0ec0c3d6162e914c",
    "TWILLIO_ACCOUNT_AUTH"    :"5e74c578b64d714940b06b4028b64229",
    "TWILLIO_ACCOUNT_PHONE"   :"+13466448906",
    "API_PASSWORD"           :"Abc@1234",
    "APP_CURRENCY"           :"USD",
    // "EXCHANGERATES_APIKEY"    :"BHNHpTneYCIWcS2cAP0pTtqI0s8F29q7",
    "EXCHANGERATES_APIKEY"    :"93e5583032e1f56b50abe06c45da63a6",
}
if (environment.env==="Production") {
    
    //App Details
    Globals.BASE_URL             = environment.protocol+'://'+environment.ipaddress+'/api/';
    Globals.BASE_URL_WITHOUT_API = environment.protocol+'://'+environment.ipaddress+'/';
    Globals.PORT_BASE_URL        = environment.protocol+'://'+environment.ipaddress+':'+environment.port+'/';

    //S3 Bucket Detail
    // Globals.S3_BUCKET_ROOT       = 'https://susalive.s3.amazonaws.com/';
       Globals.S3_BUCKET_ROOT       = 'https://susalive.s3.us-east-1.amazonaws.com/';
    Globals.USER_IMAGE           = 'user/';

    //Email Details
    Globals.EMAIL_ID             = 'firstnathshah@gmail.com';
    Globals.EMAIL_PASSWORD       = '123firstnathshah';
    Globals.TO_EMAIL             = 'abc@gmail.com';
    Globals.API_PASSWORD         = "Abc@1234";
    Globals.APP_CURRENCY         = "USD";
    // Globals.EXCHANGERATES_APIKEY = "BHNHpTneYCIWcS2cAP0pTtqI0s8F29q7";
    Globals.EXCHANGERATES_APIKEY = "93e5583032e1f56b50abe06c45da63a6";
}

module.exports = Globals;