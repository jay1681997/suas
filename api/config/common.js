var con           = require('./database');
var GLOBALS       = require('./constants');
var cryptoLib     = require('cryptlib');
var shaKey        = cryptoLib.getHashSha256(GLOBALS.KEY, 32);
const localizify  = require('localizify');
const en          = require('../languages/en.js');
var asyncLoop     = require('node-async-loop');
const { t }       = require('localizify');

var Validate      = {

    /*
    ** Function to validate the header for any user application
    ** 08-Feb-2022
    ** @param {Request Object} req 
    ** @param {Response Object} res 
    ** @param {Function} callback 
    */
    validate_token: function (req, res, callback) {
        console.log('--------------------- API ----------------------');
        console.log(req.path);
        console.log(req.headers['api-key']);
        console.log(req.headers['token']);
        var path_data = req.path.split("/");
        var bypassMethod = new Array('get_ratting','token',"region_list","sendPushNotification","generate_token_agora","apiuserList","get_countrycode_list","home_video_list","signup","verify_otp","resend_otp","create_password", "upload_video","student_profile","login","forgotpassword","get_prize_list","get_prize_details","get_leaderboard_list");
        var bypassAdminMethod = [""];

        try {
            var api_key = (req.headers['api-key'] != undefined && req.headers['api-key'] != '') ? cryptoLib.decrypt(req.headers['api-key'], shaKey, GLOBALS.IV) : "";
          
            localizify.add('en', en).setLocale('en');
            console.log("P1");
            if (api_key == GLOBALS.API_KEY) {
                console.log("P2");
                if (bypassMethod.indexOf(path_data[4]) === -1 && bypassAdminMethod.indexOf(path_data[1]) === -1) { 
                     console.log("P3");
                    if (req.headers['token'] && req.headers['token'] != '') {
                         console.log("P4");
                        var headtoken = cryptoLib.decrypt(req.headers['token'], shaKey, GLOBALS.IV).replace(/\s/g, '');
                        console.log(headtoken);
                        if (headtoken !== '') {
                            // con.getConnection(function(connectionError,connection){})
                            var query = con.query("SELECT * FROM tbl_user_deviceinfo WHERE token = '" + headtoken + "' ", function (err, result) {
                                console.log(result)
                                console.log(this.sql)
                                if (result[0] != undefined) {
                                    req.user_id = result[0].user_id;
                                    req.user_type = result[0].user_type;
                                    callback();
                                } else {
                                    response_data = {
                                        code: '-1',
                                        message: t('rest_keywords_tokeninvalid')
                                    };
                                    Validate.encryption(response_data, function (response) {
                                        res.status(401)
                                        res.json(response)
                                    });
                                }
                            });
                        } else {
                            response_data = {
                                code: '-1',
                                message: t('rest_keywords_tokeninvalid')
                            };
                            Validate.encryption(response_data, function (response) {
                                res.status(401)
                                res.json(response)
                            })
                        }
                    } else {
                        response_data = {
                            code: '-1',
                            message: t('rest_keywords_tokeninvalid')
                        };
                        Validate.encryption(response_data, function (response) {
                            res.status(401)
                            res.json(response)
                        })
                    }
                } else {
                    callback();
                }
            } else {
                response_data = {
                    code: '-1',
                    message: t('rest_keywords_invalid_api_key')
                };
                Validate.encryption(response_data, function (response) {
                    res.status(401);
                    res.json(response)
                })
            }
        }catch (error) {
            let param = {
                api_path: req.path,
                api_key_enc: req.headers['api-key'],
                head_token_enc: (req.headers['token'] != undefined) ? req.headers['token'] : '',
                api_key_enc: req.headers['api-key'],
                error: error,
            }
            // Validate.storeLogs(param);
            Validate.sendresponse(res,'-1', t('rest_keywords_tokeninvalid'),null);
        }
    },


    
    /*
    ** Function to check validation rules for all api's 
    ** 08-Feb-2022
    ** @param {Request Parameters} request 
    ** @param {Response Object} response 
    ** @param {Validattion Rules} rules 
    ** @param {Messages} messages 
    ** @param {Keywords} keywords 
    */
    checkValidationRules: function (request, response, rules, messages, keywords) {

        var v = require('Validator').make(request, rules, messages, keywords);
        if (v.fails()) {
            var Validator_errors = v.getErrors();
            for (var key in Validator_errors) {
                error = Validator_errors[key][0];
                break;
            }
            response_data = {
                code: '0',
                message: error
            };
            Validate.encryption(response_data, function (responseData) {
                response.status(200);
                response.json(responseData);
            })
            return false;
        } else {
            return true;
        }
    },

    /*
    ** Function to return response for any api
    ** 08-Feb-2022
    ** @param {Response Object} res
    ** @param {Response Code} responsecode 
    ** @param {Response Message} responsemessage 
    ** @param {Response Data} responsedata 
    */
    sendresponse: function(res, responsecode, responsemessage, responsedata) {            
        status_code = (responsecode == '-1') ? 401 : 200 ;
        // console.log(responsedata);
        response_data = {
            code: responsecode,
            message: responsemessage
        };
        if (responsedata != null) {
            response_data.data = responsedata
        }
        console.log(response_data);
        Validate.encryption(response_data, function (response) {
            res.status(status_code);
            res.json(response);
        })
    },

    /*
    ** Function to decrypt the data of request body
    ** 08-Feb-2022
    ** @param {Request Parameters} req
    */
    decryption: function(req, callback) {
        if (req != undefined && Object.keys(req).length !== 0) {
            
            var request = JSON.parse(cryptoLib.decrypt(req, shaKey, GLOBALS.IV));
            console.log('------------------- Request decrypt--------------------');
            console.log(request);
            callback(request);
        } else  {
            callback({});
        }
    },

    /*
    ** Function to encrypt the response body before sending response
    ** 08-Feb-2022
    ** @param {Response Body} req 
    ** @param {Function} callback 
    */
    encryption: function (req, callback) {
        var cryptoLib = require('cryptlib');
        var shaKey = cryptoLib.getHashSha256(GLOBALS.KEY, 32);
        var response = cryptoLib.encrypt(JSON.stringify(response_data), shaKey, GLOBALS.IV);
        callback(response);
    },

    /*
    ** Function to generate the random hash for token
    ** 08-Feb-2022
    ** @param {Login User ID} user_id 
    ** @param {Role Of Users} role 
    ** @param {Function} callback 
    */
    generateSessionCode: function (user_id, role, callback) {

        var _randomhash = require('crypto-toolkit').RandomHash('base64-urlsafe');
        var usersession = _randomhash.sha256();
        Validate.checkDeviceInfo(user_id, role, function (DeviceInfo, Error) {
            if (DeviceInfo != null) {
                var params = {
                    token: usersession
                };
                Validate.updateDeviceInfo(user_id, role, params, function () {
                    callback(usersession);
                });
            } else {
                var params = {
                    token: usersession,
                    user_id: user_id,
                    user_type: role
                };
                Validate.addDeviceInformation(params, function () {
                    callback(usersession);
                });
            }
        });
    },

    /*
    ** Function to check device information of any users
    ** 08-Feb-2022
    ** @param {Login User ID} user_id 
    ** @param {Role} role 
    ** @param {Function} callback 
    */
    checkDeviceInfo: function (user_id, role, callback) {

        var query = con.query("SELECT * FROM tbl_user_deviceinfo WHERE user_id = '" + user_id + "' AND user_type='" + role + "' ", function (err, result) {
            if (!err && result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null, err);
            }
        });
    },

    /*
    ** Function to update device information of any users
    ** 08-Feb-2022
    ** @param {Login User ID} user_id 
    ** @param {Role} role 
    ** @param {Parameters} params 
    ** @param {Function} callback 
    */
    updateDeviceInfo: function (user_id, role, params, callback) {
        var query = con.query("UPDATE tbl_user_deviceinfo SET ? WHERE user_id = '" + user_id + "' AND user_type='" + role + "' ", params, function (err, result, fields) {
            callback(result);
        });
    },

    /*
    ** Add Device Information for users
    ** 08-Feb-2022
    ** @param {Parameters} params 
    ** @param {Function} callback 
    */
    addDeviceInformation: function (params, callback) {

        var query = con.query('INSERT INTO tbl_user_deviceinfo SET ?', params, function (err, result, fields) {
            callback(result.insertId);
        });
    },

    /*
    ** Function to check and update device information based on roles
    ** 08-Feb-2022
    ** @param {Login User ID} user_id 
    ** @param {Role} role 
    ** @param {Parameters} params 
    ** @param {Function} callback 
    */
    checkUpdateDeviceInfo: function (user_id, role, params, callback) {

        var upd_device = {
            uuid         : (params.uuid != undefined) ? params.uuid : "",
            ip           : (params.ip != undefined) ? params.ip : "",
            os_version   : (params.os_version != undefined) ? params.os_version : "",
            model_name   : (params.model_name != undefined) ? params.model_name : "",
            device_type  : params.device_type,
            device_token : params.device_token,
        };

        Validate.checkDeviceInfo(user_id, role, function (DeviceInfo, Error) {
            if (DeviceInfo != null) {
                Validate.updateDeviceInfo(user_id, role, upd_device, function (result, error) {
                    callback(result);
                })
            } else {
                upd_device.user_id = user_id;
                upd_device.user_type = role;
                Validate.addDeviceInformation(upd_device, function (result, error) {
                    callback(result);
                });
            }
        });
    },

    
    /*
    ** Function to Send email
    ** 08-Feb-2022
    ** @param {Subject} subject 
    ** @param {To email} to_email 
    ** @param {Message} message 
    ** @param {Function} callback 
    */
    send_email : function(subject, from_email, to_email, message, callback)
    {
        console.log("SEND");
        console.log(from_email);
        console.log(to_email);
        console.log(message);
        var transporter= require('nodemailer').createTransport({
            host : 'smtp.gmail.com',
            secure : false,
            port : 587,
            auth: {
              user: GLOBALS.EMAIL_ID,
              pass: GLOBALS.EMAIL_PASSWORD
            },
            tls: {
                rejectUnauthorized: false
            }
        });
        var mailOptions = 
        {
            from    : from_email,
            to      : to_email,
            subject : subject,
            html    : message
        };
        transporter.sendMail(mailOptions, function(error, info)
        {
            console.log(error);
            if(error)
            {
                callback(false);
            }
            else
            {
                callback(true);
            }
      });
    },

    /*
    ** Common function to send sms
    ** 08-Feb-2022
    ** @param {Phone} phone 
    ** @param {Message} message 
    ** @param {Function} callback 
    */
 sendSMS: function(phone, message, callback) {
        // callback(true); 
        // return ;
        if (phone != '' && phone != undefined) {
            // callback(true)
            const client = require('twilio')(GLOBALS.TWILLIO_ACCOUNT_SID, GLOBALS.TWILLIO_ACCOUNT_AUTH);
            client.messages
                .create({
                    body: message,
                    from: GLOBALS.TWILLIO_ACCOUNT_PHONE,
                    to: phone
                })
                .then((message) => {
                    console.log(message);
                    callback(true);
                })
                .catch((e) => {
                    console.log(e);
                    callback(false);
            });
        } else {
            callback(false);
        }
    },

    /*
    ** get user notification listing
    ** 08-Feb-2022
    */
    notification_list: function (request, callback) {
        var query = con.query("SELECT tn.* FROM tbl_notification as tn LEFT JOIN tbl_user tu ON tn.sender_id=tu.id WHERE tn.receiver_id = '" + request.user_id + "' AND tn.receiver_type='user' ORDER BY id DESC ", function (err, result, fields) {
            if (!err && result.length != '0') {
                Validate.readNotifications(request, function (response) {
                       callback(result);
                });
             
            } else {
                callback(null, err);
            }
        });
    },

    /*
    ** update user notification status as read
    ** 08-Feb-2022
    */
    readNotifications: function (request, callback) {
        var query = con.query("UPDATE tbl_notification SET is_read = 'read' WHERE receiver_id = '" + request.user_id + "' AND receiver_type='user' ", function (err, result, fields) {
            if (!err) {
                callback(result);
            } else {
                callback(err);
            }
        });
    },

    /*
    ** Get common setting data
    ** 08-Feb-2022
    */
    getSettings: function(callback){
        var query = con.query("SELECT * FROM tbl_setting WHERE is_deleted = '0'", function (err, result, fields) {
            if (!err && result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Get common data of master tables likes subscription, class, contest, video, promocode
    ** 08-Feb-2022
    */
    get_commonMasterData: function(type, common_id, callback){
        var qry = '';
        if(type == 'subscription'){
            qry = "SELECT * FROM tbl_master_subscription WHERE id = '"+common_id+"' AND is_deleted='0'";
        }else if(type == 'class'){
            qry = "SELECT * FROM tbl_master_class WHERE id = '"+common_id+"' AND is_deleted='0'";
        }else if(type == 'contest'){
            qry = "SELECT * FROM tbl_master_contest WHERE id = '"+common_id+"' AND is_deleted='0'";
        }else if(type == 'video'){
            qry = "SELECT * FROM tbl_master_videolibrary WHERE id = '"+common_id+"' AND is_deleted='0'";
        }else if(type == 'promocode'){
            qry = "SELECT * FROM tbl_promocode WHERE id = '"+common_id+"' AND is_deleted='0'";
        }else{
            qry = "SELECT * FROM tbl_user_speech WHERE id = '"+common_id+"' AND is_deleted='0'";
        }
        console.log(qry)
        con.query(qry, function (err, result, fields) {
            if (!err && result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Get student details
    ** 08-Feb-2022
    */
    getStudentDetails: function(student_id, user_id, callback){
        con.query("SELECT * FROM tbl_student WHERE id ='"+student_id+"' AND user_id = '"+user_id+"' AND is_deleted='0'", function(error, resuls, fields){
            console.log(this.sql);
            if(!error && resuls[0] != undefined){
                callback(resuls[0]);
            } else {
                callback(null);
            }
        });
    },
    /*
    ** Get Country details
    ** 08-Feb-2022
    */
    getCountryList: function(callback){
        con.query("SELECT * FROM suasdb.tbl_country where is_deleted = '0'",function(error,resuls,fields){
            console.log(this.sql);
            if(!error && resuls[0] != undefined){
                callback(resuls);
            } else {
                callback(null);
            }
        });
    },

isEmptyObject: function(obj){
        return !Object.keys(obj).length;
    },
    /**
     * Function for common insertion function
     * 03-12-2019
     * @param {Table name} tablename 
     * @param {Parameters} parameters 
     * @param {Function} callback 
     */
    commonSingleInsert: function (tablename, parameters, callback) {

        var query = con.query('INSERT INTO ' + tablename + ' SET ?', parameters, function (err, result, fields) {
            if (!err) {
                callback(result.insertId, err);
            } else {
                callback(null, err);
            }
        });
    },
       getUserCartData_student: function(item_type, user_id,student_id, callback){
        var qry = '';
        const { getVideoDurationInSeconds } = require('get-video-duration');
        console.log(getVideoDurationInSeconds("https://hlis-bucket.s3.us-east-2.amazonaws.com/suas/speech/kpWrDyubnP1653375785.mp4"))
        con.query("SELECT * FROM suasdb.tbl_user where id = '"+user_id+"'",function(usererror,resultss){
        var where = '';
        if(student_id != 0){
             where = "AND tuc.student_id = '" + student_id + "' ";
        }
        if(item_type == 'subscription'){
            qry = "SELECT tuc.*, tc.name as title, tc.description, (SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SUBSCRIPTION_IMAGE+"','',IF(tc.image != '',tc.image,'default.png')) as image  FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_subscription as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"' \
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'class'){
            qry = "SELECT tuc.*, tc.program_title as title, tc.total_point,(SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol,tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CLASS_IMAGE+"','',tc.class_image) as image FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_class as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"' \
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'contest'){
            qry = "SELECT tuc.*, tc.name as title, tc.contest_type, tc.description,tc.total_point, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','',tc.contest_image) as image, (SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_contest as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'video'){
             qry = "SELECT tuc.*, tc.total_point , tc.name as title, tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.VIDEOLIBRARY_IMAGE+"','',IF(tc.video = '','918341659077811.mp4',tc.video)) as image ,tc.video, (SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_videolibrary as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'promocode'){
            qry = "SELECT tuc.*, tc.*, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',tc.promocode_image) as image, (SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"') AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) as currency_symbol FROM tbl_user_cart as tuc \
                JOIN tbl_promocode as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else{
            qry = "SELECT tuc.*, tc.title, tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SPEECH_IMAGE+"','',video) as image, (SELECT (cr.rate*tc.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_user_speech as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' "+where+" AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }
        con.query(qry, function (err, result, fields) {  
            console.log(err);
            console.log(result);
            console.log(this.sql);
            console.log("OOOOOOOOOOOOOOOOOOOOO");
            if (!err && result[0] != undefined) {
                // callback(result[0]);
                asyncLoop(result, function(item, next){
                    if(item_type == 'speech'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            item.total_point = 0;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'contest' && item.contest_type == 'video'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            item.total_point = item.total_point;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'contest' && item.contest_type == 'live'){
                        item.total_point = item.total_point;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })
                        
                    }else if(item_type == 'video'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            // item.total_point = 0;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'class'){
                        item.total_point = item.total_point;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                            item.wishlist_status = wishlist_status;
                            next()
                        })
                    }else{
                        item.total_point = 0;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                            item.wishlist_status = wishlist_status;
                            next()
                        })
                    }
                }, function(){
                    callback(result)
                })

            } else {
                callback(null);
            }
        });
    });
    },

    /*
    ** Get common master data from user cart
    ** 08-Feb-2022
    */
    getUserCartData: function(item_type, user_id, callback){
        var qry = '';
        const { getVideoDurationInSeconds } = require('get-video-duration');
        console.log(getVideoDurationInSeconds("https://hlis-bucket.s3.us-east-2.amazonaws.com/suas/speech/kpWrDyubnP1653375785.mp4"))
        if(item_type == 'subscription'){
            qry = "SELECT tuc.*, tc.name as title, tc.description, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SUBSCRIPTION_IMAGE+"','',IF(tc.image != '',tc.image,'default.png')) as image  FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_subscription as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"' \
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'class'){
            qry = "SELECT tuc.*, tc.program_title as title, tc.total_point, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol,tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CLASS_IMAGE+"','',tc.class_image) as image FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_class as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"' \
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'contest'){
            qry = "SELECT tuc.*, tc.name as title, tc.contest_type, tc.description,tc.total_point, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','',tc.contest_image) as image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_contest as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'video'){
             qry = "SELECT tuc.*, tc.total_point , tc.name as title, tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.VIDEOLIBRARY_IMAGE+"','',IF(tc.video = '','918341659077811.mp4',tc.video)) as image ,tc.video, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_master_videolibrary as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else if(item_type == 'promocode'){
            qry = "SELECT tuc.*, tc.*, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',tc.promocode_image) as image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM tbl_user_cart as tuc \
                JOIN tbl_promocode as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"' AND tc.is_deleted='0' AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }else{
            qry = "SELECT tuc.*, tc.title, tc.description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SPEECH_IMAGE+"','',video) as image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM tbl_user_cart as tuc \
                LEFT JOIN tbl_user_speech as tc ON tuc.item_id = tc.id \
                WHERE tuc.user_id='"+user_id+"'  AND tuc.is_deleted='0' AND item_type = '"+item_type+"'\
                GROUP BY tuc.id ORDER BY tuc.id ASC";
        }
        con.query(qry, function (err, result, fields) {  
            console.log(err);
            console.log(result);
            console.log(this.sql);
            console.log("OOOOOOOOOOOOOOOOOOOOO");
            if (!err && result[0] != undefined) {
                // callback(result[0]);
                asyncLoop(result, function(item, next){
                    if(item_type == 'speech'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            item.total_point = 0;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'contest' && item.contest_type == 'video'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            item.total_point = item.total_point;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'contest' && item.contest_type == 'live'){
                        item.total_point = item.total_point;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })
                        
                    }else if(item_type == 'video'){
                        getVideoDurationInSeconds(item.image).then((duration) => {
                            // item.total_point = 0;
                            item.duration = duration;
                            Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                                item.wishlist_status = wishlist_status;
                                next()
                            })

                        }) 
                    }else if(item_type == 'class'){
                        item.total_point = item.total_point;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                            item.wishlist_status = wishlist_status;
                            next()
                        })
                    }else{
                        item.total_point = 0;
                        item.duration = 0;
                        Validate.check_wishlist_items(item_type, item.item_id, function(wishlist_status){
                            item.wishlist_status = wishlist_status;
                            next()
                        })
                    }
                }, function(){
                    callback(result)
                })

            } else {
                callback(null);
            }
        });
    },

    check_wishlist_items: function(item_type, item_id, callback){
        con.query("SELECT * FROM tbl_user_wishlist WHERE item_type = '"+ item_type +"' AND item_id = '"+ item_id +"' AND is_deleted = '0'", function(err, res, fields){
            console.log(this.sql)
            if(!err && res[0] != undefined){
                callback("1");
            }else{
                callback("0")
            }
        });
    },


    /*
    ** Send push notification to user or admin
    ** 07-June-2022
    */
        sendPushNotification: function (requestData, callback) {
        console.log(requestData);
        var topic = '';
        if (requestData.sender_type == "admin") {
            topic = GLOBALS.BUNDLE_ID;
            var r_id = requestData.receiver_id;
            var receiver_type = "user";
            var opponent_qry = "SELECT d.id,d.username as fullname,d.email, IFNULL(ut.device_type,'') as device_type, IFNULL(ut.device_token,'') as device_token FROM tbl_user as d  LEFT JOIN tbl_user_deviceinfo ut ON d.id = ut.user_id AND ut.user_type='U'  WHERE d.id = '" + r_id + "' GROUP by d.id  order by ut.user_id ASC limit 1";

            var sender_id = requestData.sender_id;
            var sender_type = "admin";

            var qry_excu = "SELECT t.id,t.name as fullname,t.email,CONCAT('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.ADMIN_IMAGE + "', t.profile_image) as profile_image FROM tbl_admin as t  WHERE t.id ='" + sender_id + "'";
        }

        var primary_id = requestData.primary_id;
        var title = requestData.title;
        var sql = "SELECT * FROM `tbl_user_notification_setting` WHERE user_id = '" + requestData.receiver_id + "' AND is_deleted = '0'";
        con.query(sql, function (notification_setting_error, notification_setting_result) {
            // console.log(notification_setting_error);
            // console.log(notification_setting_result);
            // console.log(sql);
            if (!notification_setting_error && notification_setting_result.length > 0) {
                console.log(notification_setting_result);
                if (notification_setting_result[0]['push_notification'] == 'yes') {
                    var query = con.query(qry_excu, function (ersr, result_data, fields) {
                        console.log(this.sql)
                        console.log("P1");
                        if (!ersr && result_data[0] != undefined) {
                            var username = result_data[0].fullname;
                            var profile_image = result_data[0].profile_image;
                              console.log("P2");
                            var qry = con.query(opponent_qry, function (err, result, fields) {
                                console.log(this.sql)
                                console.log(result);
                                console.log('=====================');
                                if (!err && result[0] != undefined) {
                                    var receiver_id = result[0].id;
                                    var tags = requestData.notification_tag;
                                    var msg = requestData.message;
                                    console.log("P3");
                                    const registrationIds = [];

                                    if (result[0].device_type != "" && result[0].device_token != 0) {
                                        console.log("P4");
                                        registrationIds.push(result[0].device_token);
                                        var push_data = {
                                            topic: topic,
                                            alert: {
                                                title: GLOBALS.APP_NAME,
                                                body: msg,
                                                icon: GLOBALS.LOGO
                                            },
                                            custom: {
                                                title: GLOBALS.APP_NAME,
                                                body: msg,
                                                primary_id: primary_id,
                                                sender_name: username,
                                                sender_profile: profile_image,
                                                sender_id: sender_id,
                                                sender_type: sender_type,
                                                receiver_id: receiver_id,
                                                receiver_type: receiver_type,
                                                tag: tags,
                                            }
                                        };

                                        if (result[0].device_type == "I") {
                                            push_data['sound'] = "default"
                                        }
                                        console.log("P5");
                                        Validate.send_push(registrationIds, push_data, function (respss, err3) { });
                                    }

                                    //insert into notification table
                                    var notify_data = {
                                        primary_id: primary_id,
                                        sender_id: sender_id,
                                        sender_type: sender_type,
                                        receiver_id: receiver_id,
                                        receiver_type: receiver_type,
                                        notification_tag: tags,
                                        message: msg,
                                        title: title,
                                        tag:tags
                                    };
                                    console.log("P6");
                                    Validate.insert_notification(notify_data, function (resp, err1) {
                                        callback(true);
                                    });
                                }
                                else {
                                    callback(true);
                                }
                            });
                        }
                        else {
                            callback(false);
                        }

                    });
                } else {
                    callback(true);
                }
            } else {
                var query = con.query(qry_excu, function (ersr, result_data, fields) {
                    console.log(this.sql);
                    console.log("P7");
                    if (!ersr && result_data[0] != undefined) {
                        var username = result_data[0].fullname;
                        var profile_image = result_data[0].profile_image;
                        console.log("P8");
                        var qry = con.query(opponent_qry, function (err, result, fields) {
                            console.log(this.sql)
                            if (!err && result[0] != undefined) {
                                var receiver_id = result[0].id;
                                var tags = requestData.notification_tag;
                                var msg = requestData.message;

                                const registrationIds = [];
                                console.log("P9");
                                console.log(result[0]);
                                if (result[0].device_type != "" && result[0].device_token != 0) {
                                    registrationIds.push(result[0].device_token);
                                    var push_data = {
                                        topic: topic,
                                        alert: {
                                            title: GLOBALS.APP_NAME,
                                            body: msg,
                                            icon: GLOBALS.LOGO
                                        },
                                        custom: {
                                            title: GLOBALS.APP_NAME,
                                            body: msg,
                                            primary_id: primary_id,
                                            sender_name: username,
                                            sender_profile: profile_image,
                                            sender_id: sender_id,
                                            sender_type: sender_type,
                                            receiver_id: receiver_id,
                                            receiver_type: receiver_type,
                                            tag: tags,
                                        }
                                    };
                                    console.log("P10");
                                    if (result[0].device_type == "I") {
                                        push_data['sound'] = "default"
                                    }

                                    Validate.send_push(registrationIds, push_data, function (respss, err3) { });
                                }

                                //insert into notification table
                                var notify_data = {
                                    primary_id: primary_id,
                                    sender_id: sender_id,
                                    sender_type: sender_type,
                                    receiver_id: receiver_id,
                                    receiver_type: receiver_type,
                                    tag: tags,
                                    message: msg,
                                    title: title,
                                };

                                Validate.insert_notification(notify_data, function (resp, err1) {
                                    console.log(resp);
                                    console.log(err1);
                                    callback(true);
                                });
                            }
                            else {
                                callback(true);
                            }
                        });
                    }
                    else {
                        callback(false);
                    }

                });
            }
        })

    },
    sendPushNotification1: function (requestData,callback) {
        console.log(requestData);   
        var topic = '';
        if(requestData.sender_type == "admin")
        {
            topic               = GLOBALS.BUNDLE_ID;
            var r_id            = requestData.receiver_id;
            var receiver_type   = "user";
            var opponent_qry    = "SELECT d.id,d.username as fullname,d.email, IFNULL(ut.device_type,'') as device_type, IFNULL(ut.device_token,'') as device_token FROM tbl_user as d  LEFT JOIN tbl_user_deviceinfo ut ON d.id = ut.user_id AND ut.user_type='U'  WHERE d.id = '"+r_id+"' GROUP by d.id  order by ut.user_id ASC limit 1";

            var sender_id       = requestData.sender_id;
            var sender_type     = "admin";

            var qry_excu = "SELECT t.id,t.name as fullname,t.email,CONCAT('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.ADMIN_IMAGE+"', t.profile_image) as profile_image FROM tbl_admin as t  WHERE t.id ='"+sender_id+"'";
        }

        var primary_id   = requestData.primary_id;
        var title        = requestData.title;

        var query = con.query(qry_excu, function (ersr, result_data, fields) {
            console.log(this.sql)
            if (!ersr && result_data[0] != undefined) 
            {
                var username = result_data[0].fullname;
                var profile_image = result_data[0].profile_image;

                var qry = con.query(opponent_qry, function (err, result, fields) {
                     console.log(this.sql)
                    if (!err && result[0] != undefined) 
                    {
                        var receiver_id = result[0].id;
                        var tags        = requestData.notification_tag;
                        var msg         = requestData.message;
                        
                        const registrationIds = [];
                        console.log(result[0]);
                        if(result[0].device_type!="" && result[0].device_token!=0)
                        {
                            registrationIds.push(result[0].device_token);
                            var push_data = {
                                topic: topic,
                                alert: {
                                    title       : GLOBALS.APP_NAME,
                                    body        : msg,
                                     icon: GLOBALS.LOGO
                                },
                                custom: {
                                    title         : GLOBALS.APP_NAME,
                                    body          : msg,
                                    primary_id    : primary_id,
                                    sender_name   : username,
                                    sender_profile: profile_image,
                                    sender_id     : sender_id,
                                    sender_type   : sender_type,
                                    receiver_id   : receiver_id,
                                    receiver_type : receiver_type,
                                    tag           : tags,
                                }
                            };

                            if(result[0].device_type == "I")
                            {
                                push_data['sound'] = "default"
                            }
                            
                            Validate.send_push(registrationIds,push_data,function(respss,err3){});
                        } 

                        //insert into notification table
                        var notify_data  = {
                            primary_id          : primary_id,
                            sender_id           : sender_id,
                            sender_type         : sender_type,
                            receiver_id         : receiver_id,
                            receiver_type       : receiver_type,
                            notification_tag    : tags,
                            message             : msg,
                            title               : title,
                        };

                        Validate.insert_notification(notify_data, function(resp,err1){
                            callback(true);
                        });
                    }
                    else 
                    {
                        callback(true);
                    }
                });
            }
            else 
            {
                callback(false);
            }

        });
    },
     

    /*
    ** Insert Notification
    ** 07-June-2022
    */
    insert_notification: function(req,callback){
        var query = con.query("INSERT INTO tbl_notification SET ?", req, function (err, result, fields) {
            console.log(this.sql)
            if (!err) {
                callback(result.insertId);
            }
            else {
                callback(null, "Error when insert notification");
            }
        });
    },


    /*
    ** Insert Notification
    ** 07-June-2022
    */
    send_push: function (registrationIds, data, callback) {
        console.log(registrationIds)
        console.log(data)
        console.log("SSSSSSSSS");
        const settings = {
            gcm: {
                id: GLOBALS.PUSH_KEY,
            },
            apn: {
                token: {
                    key    : GLOBALS.P8_CERTIFICATE_NAME,
                    keyId  : GLOBALS.KEY_ID,
                    teamId : GLOBALS.TEAM_ID,
                },
            }
        };
        const PushNotifications = require('node-pushnotifications');
        const push = new PushNotifications(settings);

        push.send(registrationIds, data, (err, result) => {

            if (err) {
                console.log('error');
                console.log(err);
            } else {
                console.log('succ');
                console.log(result);
                console.log(result[0].message);
            }
            callback()
        });
    }








}

module.exports = Validate;
