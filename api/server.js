var express     = require('express');
var common 	    = require('./config/common');
var environment = require("./utilities/environment");
var cron  = require('./modules/crons/cron_route');
/*
** Load all routes file in particular variables
** 08-Feb-2022
*/

var api_doc 	= require('./modules/v1/Api_document/route');
var apidoc = require('./modules/v1/api_document/index');
var v1_routes   = require('./modules/v1/v1_route');  // get an instance of the express Router



app = express();
app.use(express.text());
app.use(express.urlencoded({ extended: false }));
app.use('/crons/',cron);
app.use('/v1/Api_document/', api_doc);
app.use('/v1/api_document/', apidoc);
app.get('/token', function (req, res) {
	console.log(":Aasss");
})

app.post('/token', function (req, res) {
	console.log("-1-1-in");
	  common.decryption(req.body,function(request){
     const RtmTokenBuilder = require('../api/src/RtmTokenBuilder').RtmTokenBuilder;
    const RtmRole = require('../api/src/RtmTokenBuilder').Role;
    const Priviledges = require('../api/src/AccessToken').priviledges;
    const appID = "9a66bf67924441818932f9fccc95a960";
    const appCertificate = "6f1d7020c7024163b88fe4443ce11513";
    const userId = request.user_name;
    const account = userId;
    console.log(request);
    const expirationTimeInSeconds = 3600
    const currentTimestamp = Math.floor(Date.now() / 1000)

    const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds

    const token = RtmTokenBuilder.buildToken(appID, appCertificate, account, RtmRole, privilegeExpiredTs);
    console.log("Rtm Token: " + token);
    // common.sendresponse(res,'1','token success',{"token":token});
    var response =  {"token":token};

     res.status(200);
            res.json(response);
        })
    // res.json({ token });
})
app.use(common.validate_token);

app.use('/api/v1', v1_routes);

app.post('/send_notification', function (req, res) {
	console.log("Sending");
	console.log(req.body);
    common.send_admin_push(req.body, function(code){
     console.log(code);
    });

    res.status(200);
    res.json({ code: "1", message: "success" });  
    // console.log(res);
});


// Listen to port for app
try {
	server = app.listen(environment.port);
	console.log("Connected to SUAS App On PORT : "+environment.port);
} catch (err) {
	console.log("Failed to connect");
}

