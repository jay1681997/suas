var con = require('../../../config/database');
var GLOBALS = require('../../../config/constants');
var common = require('../../../config/common');
var cryptoLib = require('cryptlib');
var auth_model = require('../Auth/auth_model');
var service_model = require('../Service/service_model');
var payment_model = require('../Payment/payment_model');
var asyncLoop = require('node-async-loop');
var shaKey = cryptoLib.getHashSha256(GLOBALS.KEY, 32);
var datetime = require('node-datetime');
var stripe = require('../../../utilities/customstripe');
var template = require('../../../config/template');
let now = new Date();
// Language file load
const { t } = require('localizify');
const { request } = require('express');
const { getVideoDurationInSeconds } = require('get-video-duration');

var User = {

    /*
    ** api list
    ** 15-feb-2022
    */
    apiuserList: function (callback) {
        var query = con.query("SELECT u.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',u.profile_image) as profile_image,IFNULL(ut.device_token,'') as device_token,IFNULL(ut.device_type,'') as device_type,IFNULL(ut.token,'') as token FROM tbl_user u LEFT JOIN tbl_user_deviceinfo as ut ON u.id = ut.user_id AND user_type='U' WHERE u.status = 'Active' AND u.is_deleted='0' GROUP BY u.id", function (err, result, fields) {
            if (!err) {
                callback(result);
            } else {
                callback(null, err);
            }
        });
    },

    /* 
    ** Function to get sub-profile or student list
    */
    get_subprofile_list: function (user_id, callback) {
        con.query("SELECT *, id as student_id, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.STUDENT_IMAGE + "','',profile_image) as profile_image, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.STUDENT_IMAGE + "','',profile_video) as profile_video FROM tbl_student WHERE is_deleted = '0' AND user_id = '" + user_id + "' GROUP BY id", function (error, results, fields) {
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_subprofile_list_found'), results);
            } else {
                callback('0', t('rest_keywords_subprofile_list_failed'));
            }
        });
    },

    /* 
    ** Function to add or upload user speech
    */
    add_speech: function (user_id, request, callback) {
        User.get_total_user_video(user_id, request, function (code, message, data) {
            if (code == 1) {
                if (data['is_display_speech_pop'] == 3) {
                    var is_display_speech_pop = 1;
                } else {
                    var is_display_speech_pop = (Number(data['is_display_speech_pop']) + 1);
                }
                var param = {
                    user_id: user_id,
                    title: request.title,
                    description: request.description,
                    video: request.video,
                    thumb_image: (request.thumb_image != undefined && request.thumb_image != '') ? request.thumb_image : '',
                    uploading_type: request.uploading_type,
                    is_display_speech_pop: is_display_speech_pop,
                }
                if (request.student_id != undefined && request.student_id != '') {
                    param.student_id = request.student_id;
                }
                if (request.subscription_id != undefined && request.subscription_id != '') {
                    param.subscription_id = request.subscription_id;
                }
                getVideoDurationInSeconds(GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + request.video).then((duration) => {
                    const duration_format = new Date(duration * 1000).toISOString().slice(11, 19);
                    param.video_duration = duration_format;
                    con.query("INSERT INTO tbl_user_speech SET ?", param, function (error, results, fields) {
                        if (!error) {
                            User.get_speech_details(user_id, results.insertId, function (code, msg, data) {
                                if (data != null) {
                                    con.query("SELECT COUNT(id) as video_count FROM tbl_user_speech WHERE user_id='" + user_id + "'", function (errr, resss, fieldsss) {
                                        data.video_count = resss[0].video_count;
                                        con.query("UPDATE tbl_user_speech SET is_display_speech_pop = '" + is_display_speech_pop + "' WHERE user_id = ?", user_id, function (erroru, resultsu, fieldsu) {
                                            con.query("SELECT tor.id as order_id,  ts.evaluation_type FROM tbl_order tor LEFT JOIN tbl_master_subscription ts ON tor.item_id = ts.id WHERE tor.item_type='subscription' AND tor.user_id='" + user_id + "' AND tor.status='Confirm'", function (err, res, field) {
                                                if (!err && res[0] != undefined) {
                                                    data.evaluation = res[0];
                                                    if (duration != undefined && duration != '' && duration >= '120' && (request.subscription_id == '' || request.subscription_id == undefined || request.subscription_id == 0) && request.uploading_type == 'Manual') {
                                                        callback('2', t('rest_keywords_video_length_long'), data);
                                                    } else {
                                                        callback('1', t('rest_keywords_speech_added_success'), data);
                                                    }
                                                } else {
                                                    if (duration != undefined && duration != '' && duration >= '120' && (request.subscription_id == '' || request.subscription_id == undefined || request.subscription_id == 0) && request.uploading_type == 'Manual') {
                                                        callback('2', t('rest_keywords_video_length_long'), data);
                                                    } else {
                                                        callback('1', t('rest_keywords_speech_added_success'), data);
                                                    }
                                                }
                                            })
                                        })

                                    })
                                } else {
                                    callback('0', t('rest_keywords_speech_added_failed'));
                                }
                            })

                        } else {
                            callback('0', t('rest_keywords_speech_added_failed'));
                        }
                    });
                    // }
                })
            } else {
                var param = {
                    user_id: user_id,
                    title: request.title,
                    description: request.description,
                    video: request.video,
                    thumb_image: (request.thumb_image != undefined && request.thumb_image != '') ? request.thumb_image : '',
                    uploading_type: request.uploading_type,
                    is_display_speech_pop: 1,
                }
                if (request.student_id != undefined && request.student_id != '') {
                    param.student_id = request.student_id;
                }
                if (request.subscription_id != undefined && request.subscription_id != '') {
                    param.subscription_id = request.subscription_id;
                }
                getVideoDurationInSeconds(GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + request.video).then((duration) => {
                    const duration_format = new Date(duration * 1000).toISOString().slice(11, 19);
                    param.video_duration = duration_format;
                    con.query("INSERT INTO tbl_user_speech SET ?", param, function (error, results, fields) {
                        if (!error) {
                            User.get_speech_details(user_id, results.insertId, function (code, msg, data) {
                                if (data != null) {
                                    con.query("SELECT COUNT(id) as video_count FROM tbl_user_speech WHERE user_id='" + user_id + "'", function (errr, resss, fieldsss) {
                                        data.video_count = resss[0].video_count;
                                        con.query("SELECT tor.id as order_id,  ts.evaluation_type FROM tbl_order tor LEFT JOIN tbl_master_subscription ts ON tor.item_id = ts.id WHERE tor.item_type='subscription' AND tor.user_id='" + user_id + "' AND tor.status='Confirm'", function (err, res, field) {
                                            if (!err && res[0] != undefined) {
                                                data.evaluation = res[0];
                                                if (duration != undefined && duration != '' && duration >= '120' && (request.subscription_id == '' || request.subscription_id == undefined || request.subscription_id == 0) && request.uploading_type == 'Manual') {
                                                    callback('2', t('rest_keywords_video_length_long'), data);
                                                } else {
                                                    callback('1', t('rest_keywords_speech_added_success'), data);
                                                }
                                            } else {
                                                if (duration != undefined && duration != '' && duration >= '120' && (request.subscription_id == '' || request.subscription_id == undefined || request.subscription_id == 0) && request.uploading_type == 'Manual') {
                                                    callback('2', t('rest_keywords_video_length_long'), data);
                                                } else {
                                                    callback('1', t('rest_keywords_speech_added_success'), data);
                                                }
                                            }
                                        })
                                    })
                                } else {
                                    callback('0', t('rest_keywords_speech_added_failed'));
                                }
                            })

                        } else {
                            callback('0', t('rest_keywords_speech_added_failed'));
                        }
                    });
                    // }
                })
            }
        })


    },

    submit_for_evaluation: function (user_id, request, callback) {


        var sql = "SELECT * FROM suasdb.tbl_user_evaluation where speech_id = " + request.speech_id + " AND is_deleted = '0'";
        con.query(sql, function (speech_error, speech_result) {
            if (!speech_error && speech_result.length > 0) {
                callback('0', t('rest_keywords_speech_submit'));
            } else {
                con.query("SELECT COUNT(id) as total_speech FROM tbl_user_speech WHERE user_id = '" + user_id + "' AND is_deleted='0'", function (errr, ress, fields) {
                    if (!errr && ress[0] != undefined) {
                        con.query("SELECT u.username , u.email , s.*, COUNT(s.id) as total_speech FROM tbl_user_speech AS s LEFT JOIN suasdb.tbl_user AS u ON s.user_id = u.id  WHERE s.id = '" + request.speech_id + "' AND s.is_deleted='0'", function (err, res, field) {
                            if (!err && res[0] != undefined) {
                                var param = {
                                    user_id: user_id,
                                    student_id: res[0].student_id,
                                    title: res[0].title,
                                    description: res[0].description,
                                    video: res[0].video,
                                    thumb_image: res[0].thumb_image,
                                    speech_id: request.speech_id,
                                }
                                if (res[0]['uploading_type'] == 'AI') {
                                    var max = 4;
                                    var min = 1;
                                    if (request.pace_result != '' && request.pace_result != undefined) {
                                        var v1 = request.pace_result;
                                    } else {
                                        var v1 = (Math.random() * (3 - 1) + 1).toFixed(1);
                                    }
                                    if (request.energy_result != '' && request.energy_result != undefined) {
                                        var v2 = request.energy_result;
                                    } else {
                                        var v2 = (Math.random() * (3 - 1) + 1).toFixed(1);
                                    }
                                    if (request.fillerword_result != '' && request.fillerword_result != undefined) {
                                        var v3 = request.fillerword_result;
                                    } else {
                                        var v3 = (Math.random() * (3 - 1) + 1).toFixed(1);
                                    }
                                    if (request.conciseness_result != '' && request.conciseness_result != undefined) {
                                        var v4 = request.conciseness_result;
                                    } else {
                                        var v4 = (Math.random() * (3 - 1) + 1).toFixed(1);
                                    }
                                    if (request.eyecontact_result != '' && request.eyecontact_result != undefined) {
                                        var v5 = request.eyecontact_result;
                                    } else {
                                        var v5 = (Math.random() * (3 - 1) + 1).toFixed(1);
                                    }
                                    s_v1 = v1.split('.');
                                    s_v2 = v2.split('.');
                                    s_v3 = v3.split('.');
                                    s_v4 = v4.split('.');
                                    s_v5 = v5.split('.');
                                    if (s_v1[1] > 5) {
                                        v1_l = (parseFloat(s_v1[0]) + 0.5);
                                    }
                                    if (s_v1[1] < 5) {
                                        v1_l = (parseFloat(s_v1[0]) + 0);
                                    }
                                    if (s_v1[1] == 5) {
                                        v1_l = v1;
                                    }

                                    if (s_v2[1] > 5) {
                                        v2_l = (parseFloat(s_v2[0]) + 0.5);
                                    }
                                    if (s_v2[1] < 5) {
                                        v2_l = (parseFloat(s_v2[0]) + 0);
                                    }
                                    if (s_v2[1] == 5) {
                                        v2_l = v2;
                                    }
                                    if (s_v3[1] > 5) {
                                        v3_l = (parseFloat(s_v3[0]) + 0.5);
                                    }
                                    if (s_v3[1] < 5) {
                                        v3_l = (parseFloat(s_v3[0]) + 0);
                                    }
                                    if (s_v3[1] == 5) {
                                        v3_l = v3;
                                    }
                                    if (s_v4[1] > 5) {
                                        v4_l = (parseFloat(s_v4[0]) + 0.5);
                                    }
                                    if (s_v4[1] < 5) {
                                        v4_l = (parseFloat(s_v4[0]) + 0);
                                    }
                                    if (s_v4[1] == 5) {
                                        v4_l = v4;
                                    }
                                    if (s_v5[1] > 5) {
                                        v5_l = (parseFloat(s_v5[0]) + 0.5);
                                    }
                                    if (s_v5[1] < 5) {
                                        v5_l = (parseFloat(s_v5[0]) + 0);
                                    }
                                    if (s_v5[1] == 5) {
                                        v5_l = v5;
                                    }
                                    if (request.fillerword_result != '' && request.fillerword_result != undefined) {
                                        var v3_l = parseFloat(request.fillerword_result);
                                    }
                                    if (request.pace_result != '' && request.pace_result != undefined) {
                                        var v1_l = parseFloat(request.pace_result);
                                    }
                                    if (request.energy_result != '' && request.energy_result != undefined) {
                                        var v2_l = parseFloat(request.energy_result);
                                    }
                                    if (request.conciseness_result != '' && request.conciseness_result != undefined) {
                                        var v4_l = parseFloat(request.conciseness_result);
                                    }
                                    if (request.eyecontact_result != '' && request.eyecontact_result != undefined) {
                                        var v5_l = parseFloat(request.eyecontact_result);
                                    }
                                    param['pace_result'] = (v1_l > 5)?5:v1_l;
                                    param['energy_result'] = (v2_l > 5)?5:v2_l;
                                    param['fillerword_result'] = (v3_l > 5)?5:v3_l;
                                    param['conciseness_result'] = (v4_l > 5)?5:v4_l;
                                    param['eyecontact_result'] = (v5_l > 5)?5:v5_l;
                                    if (s_v1[0] > 0 && s_v1[0] < 1) {
                                        param['pace_description'] = "Too fast for AI to understand. Slow down and add in pauses.";
                                    } else if (s_v1[0] > 0 && s_v1[0] < 2 && s_v1[0] != '0') {
                                        param['pace_description'] = "Fast delivery, AI could understand some of the content. Add in more pauses.";
                                    } else if (s_v1[0] > 0 && s_v1[0] < 3 && s_v1[0] != '0') {
                                        param['pace_description'] = " Moderate pace, try adding emphasis to key words.";
                                    } else if (s_v1[0] > 0 && s_v1[0] < 4 && s_v1[0] != '0') {
                                        param['pace_description'] = "Good pace, well done!";
                                    } else if (s_v1[0] > 0 && s_v1[0] <= 5 && s_v1[0] != '0') {
                                        param['pace_description'] = "Excellent pace, bravo! ";
                                    }

                                    if (s_v2[0] > 0 && s_v2[0] < 1) {
                                        param['energy_description'] = "No energy in speech, add passion to your delivery";
                                    } else if (s_v2[0] > 0 && s_v2[0] < 2 && s_v2[0] != '0') {
                                        param['energy_description'] = "Little energy, add rise and fall to your voice";
                                    } else if (s_v2[0] > 0 && s_v2[0] < 3 && s_v2[0] != '0') {
                                        param['energy_description'] = "Moderate energy, be excited about your presentation";
                                    } else if (s_v2[0] > 0 && s_v2[0] < 4 && s_v2[0] != '0') {
                                        param['energy_description'] = "Good energy and enthusiasm, great work!";
                                    } else if (s_v2[0] > 0 && s_v2[0] <= 5 && s_v2[0] != '0') {
                                        param['energy_description'] = "Great energy and enthusiasm, wow!";
                                    }


                                    if (s_v3[0] > 0 && s_v3[0] < 1 ) {
                                        param['fillerword_description'] = "Had a lot of filler words, take more pauses";
                                    } else if (s_v3[0] > 0 && s_v3[0] < 2 && s_v3[0] != '0') {
                                        param['fillerword_description'] = "Had too many filler words, slow down your pace";
                                    } else if (s_v3[0] > 0 && s_v3[0] < 3 && s_v3[0] != '0') {
                                        param['fillerword_description'] = "Had moderate filler words, add emphasis to key words";
                                    } else if (s_v3[0] > 0 && s_v3[0] < 4 && s_v3[0] != '0') {
                                        param['fillerword_description'] = "Not bad at all, keep speaking!";
                                    } else if (s_v3[0] > 0 && s_v3[0] <= 5 && s_v3[0] != '0') {
                                        param['fillerword_description'] = "Fantastic delivery!";
                                    }
                                    if (s_v4[0] > 0 && s_v4[0] < 1) {
                                        param['conciseness_description'] = "Had a lot of filler phrases, focus on vocabulary in speech";
                                    } else if (s_v4[0] > 0 && s_v4[0] < 2 && s_v4[0] != '0') {
                                        param['conciseness_description'] = "Had too many filler phrases or repetitive wording";
                                    } else if (s_v4[0] > 0 && s_v4[0] < 3 && s_v4[0] != '0') {
                                        param['conciseness_description'] = "Had moderate or repetitive wording";
                                    } else if (s_v4[0] > 0 && s_v4[0] < 4 && s_v4[0] != '0') {
                                        param['conciseness_description'] = "Concise and easy to follow, great job!";
                                    } else if (s_v4[0] > 0 && s_v4[0] <= 5 && s_v4[0] != '0') {
                                        param['conciseness_description'] = "Conciseness award goes to you …Superb!";
                                    }

                                    if (s_v5[0] > 0 && s_v5[0] < 1) {
                                        param['eyecontact_description'] = "Minimal eye contact; keep eyes on the camera area when speaking";
                                    } else if (s_v5[0] > 0 && s_v5[0] < 2 && s_v5[0] != '0') {
                                        param['eyecontact_description'] = "Little eye contact detected; focus on the camera when speaking";
                                    } else if (s_v5[0] > 0 && s_v5[0] < 3 && s_v5[0] != '0') {
                                        param['eyecontact_description'] = "Moderate eye contact, always keep your gaze straight ahead";
                                    } else if (s_v5[0] > 0 && s_v5[0] < 4 && s_v5[0] != '0') {
                                        param['eyecontact_description'] = "Good eye contact, strong connection when speaking";
                                    } else if (s_v5[0] > 0 && s_v5[0] <= 5 && s_v5[0] != '0') {
                                        param['eyecontact_description'] = "Great eye contact; you are very engaging!";
                                    }
                                }
                                template.speechevaluation(res[0], function (message) {
                                    con.query("INSERT INTO tbl_user_evaluation  SET ?", param, function (error, results, fields) {
                                        if (!error) {
                                            common.send_email('Speech Evaluation', GLOBALS.EMAIL_ID, GLOBALS.TO_EMAIL, message, function (send_code) {
                                                if (res[0]['uploading_type'] == 'AI') {
                                                    con.query("UPDATE tbl_user_speech SET is_evaluated = 'yes' WHERE id = '" + request.speech_id + "'", function (evaluation_error, evaluation_result) {
                                                        if (!evaluation_error) {
                                                            var notifydata = {
                                                                sender_id: '1',
                                                                sender_type: 'admin',
                                                                receiver_id: user_id,
                                                                receiver_type: 'user',
                                                                primary_id: results.insertId,
                                                                notification_tag: "speech_evaluations",
                                                                message: 'Your "' + res[0]['title'] + '" speech evaluation is completed',
                                                                title: "Speech Evaluations"
                                                            }
                                                            setTimeout(function () {
                                                                common.sendPushNotification(notifydata, function (notification_code) {

                                                                })
                                                            }, 40000)
                                                            setTimeout(function () {
                                                                User.get_user_evaluation(user_id, { "evaluation_id": results.insertId }, function (evalution_code, evalution_message, evalution_data) {
                                                                    callback('1', t('rest_keywords_speech_added_success'), evalution_data);
                                                                })
                                                            }, 20000)

                                                        } else {
                                                            callback('0', t('rest_keywords_speech_added_failed'));
                                                        }
                                                    })
                                                } else {
                                                    callback('1', t('rest_keywords_speech_added_success'));
                                                }
                                            })
                                        } else {
                                            callback('0', t('rest_keywords_speech_added_failed'));
                                        }
                                    });
                                })
                            } else {
                                callback('0', t('rest_keywords_speech_details_notfound'));
                            }
                        })
                        // }
                    } else {
                        callback('0', t('rest_keywords_speech_details_notfound'));
                    }
                })
            }
        });
    },

    submit_for_evaluation_old: function (user_id, request, callback) {
        con.query("SELECT COUNT(id) as total_speech FROM tbl_user_speech WHERE user_id = '" + user_id + "' AND is_deleted='0'", function (errr, ress, fields) {

            if (!errr && ress[0] != undefined) {
                if (ress[0].total_speech >= 10) {
                    callback('0', t('rest_keywords_speech_for_paid_evaluation'));
                } else {
                    con.query("SELECT *, COUNT(id) as total_speech FROM tbl_user_speech WHERE id = '" + request.speech_id + "' AND is_deleted='0'", function (err, res, field) {

                        if (!err && res[0] != undefined) {
                            var param = {
                                user_id: user_id,
                                student_id: res[0].student_id,
                                title: res[0].title,
                                description: res[0].description,
                                video: res[0].video,
                                thumb_image: res[0].thumb_image,
                                speech_id: request.speech_id,
                                // video_duration  : res[0].video_duration
                            }

                            con.query("INSERT INTO tbl_user_evaluation  SET ?", param, function (error, results, fields) {
                                if (!error) {
                                    callback('1', t('rest_keywords_speech_added_success'));
                                } else {
                                    callback('0', t('rest_keywords_speech_added_failed'));
                                }
                            });
                        } else {
                            callback('0', t('rest_keywords_speech_details_notfound'));
                        }
                    })
                }
            } else {
                callback('0', t('rest_keywords_speech_details_notfound'));
            }
        })

    },

    /* 
    ** Function to get user speech list
    */
    get_speech_list: function (user_id, request, callback) {
        if (request.student_id != undefined && request.student_id != '') {
            var sql = "SELECT *, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',video) as video, (SELECT COUNT(id) FROM tbl_user_evaluation WHERE user_id = '" + user_id + "' AND student_id = '" + request.student_id + "' AND is_deleted='0') as total_user_video FROM tbl_user_speech WHERE user_id = '" + user_id + "' AND student_id = '" + request.student_id + "' AND is_deleted='0' ORDER BY id DESC";
        } else {
            var sql = "SELECT *, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',video) as video, (SELECT COUNT(id) FROM tbl_user_evaluation WHERE user_id = '" + user_id + "' AND is_deleted='0') as total_user_video FROM tbl_user_speech WHERE user_id = '" + user_id + "' AND is_deleted='0' ORDER BY id DESC";
        }
        con.query(sql, function (error, results, fields) {
            if (!error && results[0] != undefined) {
                asyncLoop(results, function (item, next) {
                    con.query("SELECT * FROM suasdb.tbl_user_evaluation where speech_id = '" + item.id + "' AND is_deleted = '0'", function (speech_error, speech_result) {
                        if (!speech_error && speech_result.length > 0) {
                            item.total_video = 10;
                            item.remaining_video = 10 - item.total_user_video;
                            item.is_speech_evaluation = 'yes';
                            next()
                        } else {
                            item.total_video = 10;
                            item.remaining_video = 10 - item.total_user_video;
                            item.is_speech_evaluation = 'no';
                            next()
                        }
                    })

                }, function () {
                    callback('1', t('rest_keywords_speech_list_found'), results);
                })

            } else {
                callback('0', t('rest_keywords_speech_list_notfound'));
            }
        });
    },

    /* 
    ** Function to get user speech details
    */
    get_speech_details: function (user_id, speech_id, callback) {
        // var sql = ""SELECT id, user_id, student_id, title, description, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SPEECH_IMAGE+"','',thumb_image) as video_image,  concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SPEECH_IMAGE+"','',video) as video FROM tbl_user_speech WHERE user_id = '"+user_id+"' AND id='"+speech_id+"'"";
        var sql = "SELECT id, user_id,is_display_speech_pop,student_id, title, description, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',video) as video FROM tbl_user_speech WHERE user_id = '" + user_id + "' AND id='" + speech_id + "'";
        con.query(sql, function (error, results, fields) {
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_speech_details_found'), results[0]);
            } else {
                callback('0', t('rest_keywords_speech_details_notfound'), null);
            }
        });
    },

    /* 
    ** Function to delete user speech details
    */
    delete_speech: function (user_id, request, callback) {
        var up_param = {
            is_deleted: '1'
        }
        con.query("UPDATE tbl_user_speech SET ? WHERE user_id = '" + user_id + "' AND id='" + request.speech_id + "'", up_param, function (error, results, fields) {
            if (!error) {
                callback('1', t('rest_keywords_speech_deleted'));
            } else {
                callback('0', t('rest_keywords_speech_not_deleted'));
            }
        });
    },

    /* 
    ** Function to get user card data
    */
    get_user_carddata: function (req, callback) {
        var query = con.query("SELECT * FROM tbl_user_card WHERE id = '" + req + "' AND is_deleted = '0' ", function (err, result, fields) {
            if (result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },


    /*
    ** remove / delete  user card details
    */
    remove_card: function (user_id, req, callback) {
        con.query("UPDATE tbl_user_card SET is_deleted = '1' WHERE id = '" + req.card_id + "' AND user_id ='" + user_id + "' AND is_deleted = '0' ", function (error, result, fields) {
            if (!error) {
                callback('1', t('rest_keywords_user_carddata_removed_success'));
            } else {
                callback('0', t('rest_keywords_something_went_wrong'));
            }
        })
    },

    /* 
    ** Function to add gift enrolled class
    */
    add_gift_enrolled_class: function (user_id, request, callback) {

        var param = {
            user_id: user_id,
            name: request.name,
            email: request.email,
            country_code: request.country_code,
            phone: request.phone,
            class_id: request.class_id,
        }
        con.query("SELECT * FROM tbl_user WHERE id = '" + user_id + "' AND is_deleted='0'", function (errs, ress, fields) {
            if (!errs && ress[0] != undefined) {
                con.query("SELECT * FROM tbl_user WHERE email = '" + request.email + "' AND is_deleted='0' AND age_category = '" + ress[0].age_category + "'", function (err, res, field) {
                    if (!err && res[0] != undefined) {
                        param.receiver_id = res[0].id;
                        con.query("INSERT INTO tbl_gift_enrolledclass SET ?", param, function (error, results, fields) {
                            if (!error) {
                                callback('1', t('rest_keywords_gift_enrolled_class_added_success'));
                            } else {
                                callback('0', t('rest_keywords_gift_enrolled_class_added_failed'));
                            }
                        });
                    } else {
                        callback('0', t('rest_keywords_email_not_registered'));
                    }
                })
            } else {
                callback('2', t('rest_keywords_no_data_found'));
            }
        });

    },


    /* 
    ** Function to add items into cart
    */
    addtocart_old: function (user_id, request, callback) {
        con.query("SELECT * FROM tbl_order WHERE item_id='" + request.item_id + "' AND item_type = '" + request.type + "' AND user_id='" + user_id + "' AND status = 'Confirm' AND is_deleted='0'", function (ers, rsss, fields) {
            if (!ers) {
                if (rsss[0] != undefined) {
                    callback('0', t('rest_keywords_already_item_added', { type: request.type }));
                } else {
                    common.get_commonMasterData(request.type, request.item_id, function (commonData) {
                        if (commonData != null) {
                            if (request.student_id != undefined && request.student_id != '') {
                                common.getStudentDetails(request.student_id, user_id, function (studentData) {
                                    if (studentData != null) {
                                        User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                            if (allcartdata != null) {
                                                User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                    callback(rescode, resmsg, resdata);
                                                });
                                            } else {
                                                con.query("DELETE FROM tbl_user_cart WHERE item_type = '" + user_id + "'", function (er, res, field) { });
                                                User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                    callback(rescode, resmsg, resdata);
                                                });
                                            }
                                        });
                                    } else {
                                        callback('2', t('rest_keywords_student_details_not_found'));
                                    }
                                })
                            } else {
                                User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                    if (allcartdata != null) {
                                        User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                            callback(rescode, resmsg, resdata);
                                        });
                                    } else {
                                        con.query("DELETE FROM tbl_user_cart WHERE user_id = '" + user_id + "'", function (er, res, field) { });
                                        User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                            callback(rescode, resmsg, resdata);
                                        });
                                    }
                                });
                            }
                        } else {
                            callback('0', t('rest_keywords_itemdetails_notavailable'), null);
                        }
                    })
                }
            } else {
                callback('0', t('rest_keywords_something_went_wrong'));
            }
        })

    },

    addtocart: function (user_id, request, callback) {
        con.query("SELECT too.* FROM tbl_order AS too LEFT JOIN tbl_order_detail AS tod ON tod.order_id = too.id WHERE tod.item_id='" + request.item_id + "' AND tod.item_type = '" + request.type + "' AND too.user_id='" + user_id + "' AND too.status = 'Confirm' AND tod.is_deleted='0'", function (ers, rsss, fields) {
            if (!ers) {
                if (rsss[0] != undefined) {
                    callback('0', t('rest_keywords_already_item_added', { type: request.type }));
                } else {
                    common.get_commonMasterData(request.type, request.item_id, function (commonData) {
                        if (commonData != null) {
                            if (request.student_id != undefined && request.student_id != '') {
                                common.getStudentDetails(request.student_id, user_id, function (studentData) {
                                    if (studentData != null) {
                                        if (request.type == "subscription") {
                                            con.query("SELECT tod.*,DATEDIFF(now(),tod.insert_datetime) AS datediffer,tms.validity ,tms.evaluation_type,tms.duration,tms.total_video FROM `tbl_order_detail` AS tod LEFT JOIN tbl_order AS o ON tod.order_id = o.id LEFT JOIN tbl_master_subscription AS tms ON tod.item_id = tms.id WHERE o.user_id ='" + user_id + "' AND tod.item_type = 'subscription' AND tod.is_deleted = '0' ORDER BY tod.id", function (sub_error, sub_result) {
                                                if (!sub_error && sub_result.length > 0) {
                                                    var sub_total = 0;
                                                    if (sub_result[0]['duration'] == 'year') {
                                                        sub_total = ((365 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                                    } else if (sub_result[0]['duration'] == 'month') {
                                                        sub_total = ((30 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                                    } else if (sub_result[0]['duration'] == 'week') {
                                                        sub_total = ((7 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                                    } else {
                                                        sub_total = ((1 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                                    }
                                                    if (sub_total > 1) {
                                                        con.query("SELECT COUNT(id) AS total_video FROM `tbl_user_speech` WHERE user_id = '" + user_id + "' AND subscription_id = '" + sub_result[0]['item_id'] + "' AND is_deleted = '0'", function (total_video_error, total_video_result) {
                                                            if (!total_video_error && total_video_result.length > 0) {
                                                                var total_video_r = (total_video_result[0]['total_video'] == null) ? 0 : parseInt(total_video_result[0]['total_video']);
                                                                if (total_video_r <= sub_result[0]['total_video']) {
                                                                    callback("0", t("rest_keywords_subscription_exits"), sub_result);
                                                                } else {
                                                                    User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                                        callback(rescode, resmsg, resdata);
                                                                    });
                                                                }
                                                            } else {
                                                                callback("0", t("rest_keywords_subscription_exits"), sub_result);
                                                            }
                                                        })

                                                    } else {
                                                        User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                            callback(rescode, resmsg, resdata);
                                                        });
                                                    }
                                                } else {
                                                    User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                                        if (allcartdata == null) {
                                                            User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                                callback(rescode, resmsg, resdata);
                                                            });
                                                        } else {
                                                            callback("0", t("rest_keywords_subscription_exits"), sub_result);
                                                        }
                                                    });
                                                }
                                            })
                                        } else {
                                            User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                                if (allcartdata == null) {
                                                    User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                        callback(rescode, resmsg, resdata);
                                                    });
                                                } else {
                                                    callback("0", t("rest_keywords_subscription_exits"), null);
                                                }
                                            });
                                        }
                                    } else {
                                        callback('2', t('rest_keywords_student_details_not_found'), null);
                                    }
                                })
                            } else {
                                if (request.type == "subscription") {
                                    con.query("SELECT tod.*,DATEDIFF(now(),tod.insert_datetime) AS datediffer,tms.validity ,tms.evaluation_type,tms.duration,tms.total_video FROM `tbl_order_detail` AS tod LEFT JOIN tbl_order AS o ON tod.order_id = o.id LEFT JOIN tbl_master_subscription AS tms ON tod.item_id = tms.id WHERE o.user_id ='" + user_id + "' AND tod.item_type = 'subscription' AND tod.is_deleted = '0' ORDER BY tod.id DESC", function (sub_error, sub_result) {
                                        if (!sub_error && sub_result.length > 0) {
                                            var sub_total = 0;
                                            if (sub_result[0]['duration'] == 'year') {
                                                sub_total = ((365 * sub_result[0]['validity']) - sub_result[0]['datediffer']);

                                            } else if (sub_result[0]['duration'] == 'month') {
                                                sub_total = ((30 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                            } else if (sub_result[0]['duration'] == 'week') {
                                                sub_total = ((7 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                            } else {
                                                sub_total = ((1 * sub_result[0]['validity']) - sub_result[0]['datediffer']);
                                            }
                                            if (sub_total > 1) {
                                                con.query("SELECT COUNT(id) AS total_video FROM `tbl_user_speech` WHERE user_id = '" + user_id + "' AND subscription_id = '" + sub_result[0]['item_id'] + "' AND is_deleted = '0'", function (total_video_error, total_video_result) {
                                                    if (!total_video_error && total_video_result.length > 0) {
                                                        var total_video_r = (total_video_result[0]['total_video'] == null) ? 0 : parseInt(total_video_result[0]['total_video']);
                                                        if (total_video_r <= sub_result[0]['total_video']) {
                                                            callback("0", t("rest_keywords_subscription_exits"), sub_result);
                                                        } else {
                                                            User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                                callback(rescode, resmsg, resdata);
                                                            });
                                                        }
                                                    } else {
                                                        callback("0", t("rest_keywords_subscription_exits"), sub_result);

                                                    }
                                                })
                                            } else {
                                                User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                    callback(rescode, resmsg, resdata);
                                                });
                                            }

                                        } else {
                                            User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                                if (allcartdata == null) {
                                                    User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                        callback(rescode, resmsg, resdata);
                                                    });
                                                } else {
                                                    callback("0", t("rest_keywords_subscription_exits"), sub_result);
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    User.fetchAllCartdata(user_id, request, function (allcartdata) {
                                        if (allcartdata == null) {
                                            User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                callback(rescode, resmsg, resdata);
                                            });
                                        } else {
                                            User.addUpdateCart(user_id, request, commonData, function (rescode, resmsg, resdata) {
                                                callback(rescode, resmsg, resdata);
                                            });
                                        }
                                    });
                                }
                            }
                        } else {
                            callback('0', t('rest_keywords_itemdetails_notavailable'), null);
                        }
                    })
                }
            } else {
                callback('0', t('rest_keywords_something_went_wrong'));
            }
        })

    },
    /* 
    ** Function to fetch all cart data
    */
    fetchAllCartdata: function (user_id, request, callback) {
        var cartQuery = con.query("SELECT tuc.* FROM tbl_user_cart as tuc where tuc.user_id='" + user_id + "' AND item_type='" + request.type + "' AND item_id='" + request.item_id + "' GROUP BY tuc.id", function (error, result, fields) {
            if (!error & result[0] != undefined) {
                callback(result);
            } else {
                callback(null);
            }
        });
    },

    /* 
    ** Function to add or update cart
    */
    addUpdateCart: function (user_id, request, commonData, callback) {
        User.fetchCartRecord(user_id, request, function (cartdata) {
            if (request.type == 'speech' && commonData.price == 0) {
                commonData.price = 10;
            }
            var cartObject = {
                price: parseFloat(commonData.price),
                student_id: (request.student_id != undefined && request.student_id != '') ? request.student_id : 0,
            }
            if (cartdata != null) {
                var whereCondition = "id='" + cartdata.id + "'";
                con.query("UPDATE tbl_user_cart SET ? WHERE id = '" + cartdata.id + "'", cartObject, function (err, res, fields) {
                    if (!err) {
                        if (request.type == 'speech') {
                            con.query("UPDATE tbl_user_speech SET is_deleted = '1' where id = '" + request.item_id + "'", function (speech_error, speech_result) {
                                callback('1', t('rest_keywords_itemadded_tocart_success'), null);
                            })
                        } else {
                            callback('1', t('rest_keywords_itemadded_tocart_success'), null);
                        }

                    } else {
                        callback('0', t('rest_keywords_somethingwrong_addcart'), null);
                    }
                })

            } else {
                cartObject.user_id = user_id;
                cartObject.item_id = request.item_id;
                cartObject.item_type = request.type;
                con.query("INSERT INTO tbl_user_cart SET ?", cartObject, function (err, res, fields) {
                    if (!err) {
                        if (request.type == 'speech') {
                            con.query("UPDATE tbl_user_speech SET is_deleted = '1' where id = '" + request.item_id + "'", function (speech_error, speech_result) {
                                callback('1', t('rest_keywords_itemadded_tocart_success'), null);
                            })
                        } else {
                            callback('1', t('rest_keywords_itemadded_tocart_success'), null);
                        }

                    } else {
                        callback('0', t('rest_keywords_somethingwrong_addcart'), null);
                    }
                })
            }
        });
    },

    /* 
    ** Function to fetch cart record
    */
    fetchCartRecord: function (user_id, request, callback) {
        var cartQuery = con.query("SELECT * FROM tbl_user_cart where user_id='" + user_id + "' AND item_id='" + request.item_id + "'  AND item_type='" + request.type + "' GROUP BY id", function (error, result, fields) {
            if (!error & result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /* 
    ** Function to remove item from cart
    */
    remove_fromcart: function (user_id, request, callback) {
        var joindata = " ";
        if (request.item_id != '' && request.item_id != undefined) {
            joindata += " AND item_id='" + request.item_id + "'";
        }
        var cartQuery = con.query("DELETE FROM tbl_user_cart where user_id='" + user_id + "'" + joindata, function (error, result, fields) {
            if (!error) {
                callback('1', t('rest_keywords_itemremoved_fromcart_success'), null);
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    /* 
    ** Function to get cart data
    */
    fetchUsercartList_old: function (user_id, request, callback) {
        var cartObject = {}
        var where = '';

        if (request.student_id != undefined && request.student_id != '') {
            where = "AND student_id = '" + request.student_id + "' ";
        }
        con.query("SELECT * FROM tbl_user_cart WHERE user_id = '" + user_id + "' " + where + "", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                asyncLoop(res, function (item, next) {
                    common.getUserCartData(item.item_type, user_id, function (cartData) {
                        if (cartData != null) {
                            cartObject.item_list = cartData;
                            cartObject.totalitem = cartData.length;
                            User.calculateSubtotal(user_id, cartData, function (subtotal) {
                                cartObject.subtotal = subtotal;
                                common.getSettings(function (setting) {
                                    cartObject.currency = setting.currency;
                                    cartObject.currency_symbol = setting.currency_symbol;
                                    cartObject.tax = setting.tax;
                                    cartObject.credit_point_discount = setting.credit_point_discount;
                                    cartObject.total_cost = ((parseFloat(subtotal) + parseFloat(setting.tax)) - parseFloat(setting.credit_point_discount));
                                    next();
                                });
                            });
                        } else {
                            next();
                        }
                    })
                }, function () {
                    callback('1', t('rest_keywords_cartdatafound_success'), cartObject)
                });
            } else {
                callback('2', t('rest_keywords_noitemsavailable_incart'), null);
            }
        })
    },


    fetchUsercartList: function (user_id, request, callback) {
        var cartObject = {}
        var where = '';
        var item_list_data = [];
        var item_type = [];
        var student_id = 0;
        if (request.student_id != undefined && request.student_id != '') {
            where = "AND student_id = '" + request.student_id + "' ";
            student_id = request.student_id;
        }
        con.query("SELECT td.* FROM suasdb.tbl_user AS u join tbl_tax_details AS td ON u.region_id = td.id WHERE u.id = '" + user_id + "'", function (taxerror, taxresult) {
            if (!taxerror && taxresult[0] != undefined) {
                var tax_value = taxresult[0]['tax'];
            } else {
                var tax_value = 1;
            }
            con.query("SELECT * FROM tbl_user_cart WHERE user_id = '" + user_id + "' AND is_deleted = '0' " + where + "", function (err, res, fields) {
                if (!err && res[0] != undefined) {
                    asyncLoop(res, function (item, next) {
                        common.getUserCartData_student(item.item_type, user_id, student_id, function (cartData) {
                            if (cartData != null) {
                                if (item_type.includes(item.item_type)) {
                                } else {
                                    item_type.push(item.item_type);
                                    item_list_data = item_list_data.concat(cartData);

                                }
                                cartObject.totalitem = cartData.length;
                                User.calculateSubtotal(user_id, cartData, function (subtotal) {
                                    cartObject.subtotal = subtotal;
                                    common.getSettings(function (setting) {
                                        con.query("SELECT c.currency_symbol,c.currency_code,cr.rate,c.tax FROM suasdb.tbl_user AS u join tbl_country AS c ON u.country = c.name join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code where u.id = '" + user_id + "' ORDER BY cr.id DESC LIMIT 0,1 ", function (usererror, userresult) {

                                            User.get_reward_points(user_id, function (code, message, total_point) {
                                                cartObject.total_point = parseFloat(total_point.total_point);
                                                cartObject.currency = userresult[0]['currency_code'];
                                                cartObject.currency_symbol = userresult[0]['currency_symbol'];
                                                cartObject.tax = parseFloat(tax_value);
                                                cartObject.total_tax = ((parseFloat(tax_value) * subtotal) / 100);
                                                cartObject.credit_point_discount = parseFloat(setting.credit_point_discount);
                                                cartObject.total_cost = ((parseFloat(subtotal) + parseFloat(cartObject.total_tax)) - parseFloat(setting.credit_point_discount));

                                                next();
                                            })


                                        })


                                    });
                                });
                                // }
                            } else {
                                next();
                            }
                        })
                    }, function () {
                        if (item_list_data[0] != undefined) {
                            cartObject.item_list = item_list_data;
                            cartObject.totalitem = item_list_data.length;
                            var subtotal = 0;
                            asyncLoop(item_list_data, function (element, next) {
                                subtotal += parseFloat(element.price);
                                common.getSettings(function (setting) {
                                    con.query("SELECT c.currency_symbol,c.currency_code,cr.rate,c.tax FROM suasdb.tbl_user AS u join tbl_country AS c ON u.country = c.name join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code where u.id = '" + user_id + "' ORDER BY cr.id DESC LIMIT 0,1 ", function (usererror, userresult) {
                                        User.get_reward_points(user_id, function (code, message, total_point) {
                                            cartObject.total_point = parseFloat(total_point.total_point);
                                            cartObject.currency = userresult[0]['currency_code'];
                                            cartObject.currency_symbol = userresult[0]['currency_symbol'];
                                            cartObject.tax = parseFloat(tax_value);
                                            cartObject.total_tax = ((parseFloat(tax_value) * subtotal) / 100);
                                            cartObject.credit_point_discount = parseFloat(setting.credit_point_discount);
                                            cartObject.total_cost = ((parseFloat(subtotal) + parseFloat(cartObject.total_tax)) - parseFloat(setting.credit_point_discount));
                                            next();
                                        })
                                    })

                                });
                            }, function () {
                                cartObject.subtotal = subtotal;
                                callback('1', t('rest_keywords_cartdatafound_success'), cartObject);
                            });

                        } else {
                            cartObject.item_list = item_list_data;
                            callback('1', t('rest_keywords_cartdatafound_success'), cartObject);
                        }

                    });
                } else {
                    callback('2', t('rest_keywords_noitemsavailable_incart'), null);
                }
            })
        })
    },

    calculateSubtotal: function (user_id, cartlist, callback) {
        var subtotal = 0;
        asyncLoop(cartlist, function (item, next) {
            subtotal = parseFloat(subtotal) + parseFloat(item.price);
            next();
        }, function () {
            callback(subtotal);
        });
    },

    calculateTax: function (callback) {
        con.query("SELECT * FROM tbl_setting WHERE status='Active' AND is_deleted='0'", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                callback(res[0].tax, res[0].credit_point_discount);
            } else {
                callback(null);
            }
        });
    },

    /* 
    ** Function to get user contest list
    */
    get_user_contest_list: function (user_id, request, callback) {

        var where = '';
        if (request.student_id != undefined && request.student_id != '') {
            where += "AND student_id = '" + request.student_id + "'";
        }
        con.query("SELECT * FROM suasdb.tbl_user where id = '" + user_id + "'", function (usererror, resultss) {
            var sql = "SELECT tor.*, tc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.contest_image) as contest_image, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.thumb_image) as contest_thumb_image, (SELECT currency_symbol FROM suasdb.tbl_country where name = '" + resultss[0]['country'] + "') as currency_symbol FROM tbl_order tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id LEFT JOIN tbl_master_contest tc ON od.item_id = tc.id WHERE tor.user_id = '" + user_id + "' AND tor.status='Confirm' AND od.item_type='contest' AND od.is_deleted = '0' AND tor.is_deleted = '0' AND tc.is_deleted = '0' " + where + " ORDER BY tor.id DESC ";
            con.query(sql, function (error, results, fields) {
                if (!error && results[0] != undefined) {
                    asyncLoop(results, function (item, next) {
                        User.get_contest_video(user_id, item.id, function (code, contest_video) {
                            item.video = (code == 1) ? contest_video[0]['video'] : '';
                            item.thumb_image = (code == 1) ? contest_video[0]['thumb_image'] : '';
                            item.user_video_duration = (code == 1) ? contest_video[0]['user_video_duration'] : '';
                            service_model.get_prize_list(user_id, item.id, function (prizeerr, prizermsg, prizedata) {
                                item.prizelist = prizedata;
                                service_model.get_sponsor_list(item.id, function (sponsorerr, sponsorrmsg, sponsordata) {
                                    item.sponsorlist = sponsordata;
                                    service_model.get_scorecard_list(item.id, function (scorecarderr, scorecardrmsg, scorecarddata) {
                                        item.scorecardlist = scorecarddata;
                                        next();
                                    })
                                })
                            });
                        })

                    }, function () {
                        callback('1', t('rest_keywords_user_contest_list_found'), results);
                    });
                } else {
                    callback('0', t('rest_keywords_user_contest_list_notfound'));
                }
            });
        });
    },

    get_contest_video: function (user_id, contest_id, callback) {
        var sql = "SELECT *,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',video) as video, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',thumb_image) as thumb_image,video_duration as user_video_duration FROM suasdb.tbl_user_contest where user_id = '" + user_id + "' AND contest_id = '" + contest_id + "' AND is_deleted = '0'";
        con.query(sql, function (error, result) {
            if (!error && result[0] != undefined) {
                callback("1", result);
            } else {
                callback("0", null);
            }
        })
    },
    get_user_class_list: function (user_id, request, callback) {
        var where = '';
        if (request.student_id != undefined && request.student_id != '') {
            where += "AND student_id = '" + request.student_id + "'";
        }
        if (request.class_id != undefined && request.class_id != '') {
            where += "AND tc.id = '" + request.class_id + "'";

        }
        con.query("SELECT tor.*,od.item_id,od.item_type , od.price , od.totalamount, tc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CLASS_IMAGE + "','',tc.class_image) as class_image FROM tbl_order tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id JOIN tbl_master_class tc ON od.item_id = tc.id WHERE tor.user_id = '" + user_id + "' AND tor.status='Confirm' AND od.is_deleted = '0' AND tc.is_deleted = '0' AND od.item_type='class' " + where + " ORDER BY tor.id DESC", function (error, results, fields) {
            if (!error && results[0] != undefined) {

                callback('1', t('rest_keywords_enrolled_class_list_found'), results);

            } else {
                callback('0', t('rest_keywords_enrolled_class_list_failed'));
            }
        });
    },


    get_user_contest_details: function (user_id, request, callback) {

        con.query("SELECT tor.*, tc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.contest_image) as contest_image FROM tbl_order tor JOIN tbl_master_contest tc ON tor.item_id = tc.id WHERE tor.user_id = '" + user_id + "' AND tor.item_id='" + request.contest_id + "' AND item_type='contest'", function (error, results, fields) {
            if (!error && results[0] != undefined) {
                service_model.get_prize_list(user_id, request.contest_id, function (prizeerr, prizermsg, prizedata) {
                    results[0].prizelist = prizedata;
                    service_model.get_sponsor_list(request.contest_id, function (sponsorerr, sponsorrmsg, sponsordata) {
                        results[0].sponsorlist = sponsordata;
                        service_model.get_scorecard_list(request.contest_id, function (scorecarderr, scorecardrmsg, scorecarddata) {
                            results[0].scorecardlist = scorecarddata;
                            callback('1', t('rest_keywords_user_contest_details_found'), results[0]);
                        })
                    })
                });
            } else {
                callback('0', t('rest_keywords_user_contest_details_notfound'));
            }
        });
    },

    /* 
    ** Function to get user subscription list
    */
    get_activesubscriptions_list: function (user_id, callback) {
        var cartObject = [];
        con.query("SELECT * FROM suasdb.tbl_user where id = '" + user_id + "'", function (usererror, resultss) {
            con.query("SELECT tor.*,DATEDIFF(now(),od.insert_datetime) AS datediffer, tor.validity as expired_date, ts.*, ts.validity as subscription_validity,(SELECT (cr.rate*ts.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '" + resultss[0]['country'] + "' LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '" + resultss[0]['country'] + "') as currency_symbol, ( SELECT COUNT(id) FROM tbl_user_evaluation WHERE user_id = '" + user_id + "' AND is_deleted='0') as total_user_video FROM tbl_order AS tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id  LEFT JOIN tbl_master_subscription ts ON od.item_id=ts.id WHERE tor.user_id = '" + user_id + "' AND tor.status='Confirm' AND tor.is_deleted = '0' AND od.is_deleted = '0' AND od.item_type='subscription' ORDER BY tor.id DESC", function (error, results, fields) {
                if (!error && results[0] != undefined) {
                    asyncLoop(results, function (item, next) {
                        var sub_total = 0;
                        if (item.duration == 'year') {
                            sub_total = ((365 * item.validity) - item.datediffer);
                        } else if (item.duration == 'month') {
                            sub_total = ((30 * item.validity) - item.datediffer);
                        } else if (item.duration == 'week') {
                            sub_total = ((7 * item.validity) - item.datediffer);
                        } else {
                            sub_total = ((1 * item.validity) - item.datediffer);
                        }
                        if (sub_total > 1) {
                            con.query("SELECT COUNT(id) AS total_video FROM `tbl_user_speech` WHERE user_id = '" + user_id + "' AND subscription_id = '" + item.id + "' AND is_deleted = '0'", function (total_video_error, total_video_result) {
                                var total_video_r = (total_video_result[0]['total_video'] == null) ? 0 : parseInt(total_video_result[0]['total_video']);
                                if (total_video_r <= parseInt(item.total_video)) {
                                    item.remaining_video = (parseInt(item.total_video) - parseInt(total_video_r));
                                    cartObject = cartObject.concat(item);
                                    next();
                                } else {
                                    next();
                                }
                            })
                        } else {
                            next();
                        }
                    }, function () {
                        callback('1', t('rest_keywords_user_subscription_list_found'), cartObject);
                    });

                } else {
                    callback('0', t('rest_keywords_user_subscription_list_notfound'));
                }
            });
        });
    },

    /* 
    ** Function to add item into wishlist
    */
    addtowishlist: function (user_id, request, callback) {
        var param = {
            'user_id': user_id,
            'item_id': request.item_id,
            'item_type': request.type
        };
        var cartQuery = con.query("INSERT INTO tbl_user_wishlist SET ? ", param, function (error, result, fields) {
            if (!error) {
                callback('1', t('rest_keywords_item_added_into_wishlist'), null);
            } else {
                callback('0', t('rest_keywords_item_notadded_into_wishlist'), null);
            }
        });
    },

    get_wishlist: function (user_id, callback) {



        con.query("SELECT tw.*, IFNULL(concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.contest_image), '') as contest_image,IFNULL(concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.thumb_image), '') as contest_thumb_image,  IFNULL(tc.video_duration,'') as contest_video_duration, IFNULL(tc.name,'') as contest_name, IFNULL(tc.contest_type,'') as contest_type, IFNULL(concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CLASS_IMAGE + "','',tmc.class_image), '') as class_image, IFNULL(tmc.program_title,'') as class_name, IFNULL(concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',tv.video), '') as video_image, IFNULL(concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',tv.image), '') as video_thumb_image, IFNULL(tv.name,'') as video_name,IFNULL(tv.video_duration,'') as video_library_duration FROM tbl_user_wishlist tw\
            LEFT JOIN tbl_master_contest tc ON tw.item_id=tc.id AND tw.item_type='contest'\
            LEFT JOIN tbl_master_class tmc ON tw.item_id=tmc.id AND tw.item_type='class'\
            LEFT JOIN tbl_master_videolibrary tv ON tw.item_id=tv.id AND tw.item_type='video'\
            WHERE tw.user_id = '"+ user_id + "' AND tw.item_type IN ('class', 'contest','video') ORDER BY tw.id DESC ", function (error, result, fields) {
            if (!error && result[0] != undefined) {
                callback('1', t('rest_keywords_wishlist_found_success'), result);
            } else {
                callback('0', t('rest_keywords_wishlist_notfound'), null);
            }
        });
    },

    remove_fromwishlist: function (user_id, request, callback) {

        con.query("DELETE FROM tbl_user_wishlist WHERE id='" + request.wish_itemid + "'", function (error, result, fields) {
            if (!error) {
                callback('1', t('rest_keywords_wishlist_delete_success'));
            } else {
                callback('0', t('rest_keywords_wishlist_notdelete'), null);
            }
        });
    },

    /*
    ** Function to apply promocodes for users
    */
    apply_promocode: function (user_id, request, callback) {
        User.fetchUsercartList(user_id, '', function (cartcode, cartmessage, cartdata) {
            if (cartdata != null) {
                con.query("select * from tbl_promocode where status='Active' AND is_deleted='0' AND promocode=? GROUP BY id ", [request.promocode], function (error, result, fields) {
                    if (!error && result[0] != undefined) {
                        if (require('node-datetime').create().format('Y-m-d') < result[0].start_date) {
                            callback('0', t('rest_keywords_promocode_isnotusable_yet'), null);
                        }
                        else if (require('node-datetime').create().format('Y-m-d') > result[0].end_date) {
                            callback('0', t('rest_keywords_promocode_isexpired_now'), null);
                        }
                        else {
                            con.query("SELECT * FROM suasdb.tbl_order AS o join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = ? AND od.item_id = ? AND od.item_type = 'promocode' AND o.is_deleted = '0' AND od.is_deleted = '0'", [user_id, result[0].id], function (buy_error, buy_result) {
                                if (!buy_error && buy_result.length > 0) {
                                    con.query("select * from tbl_usedpromocode where promocode_id=? GROUP BY id ", [result[0].id], function (error, usage, fields) {
                                        if (error || usage.length >= parseInt(result[0].maxusage)) {
                                            callback('0', t('rest_keywords_promocode_maxusagelimit_over'), null);
                                        }
                                        else {
                                            con.query("SELECT * FROM suasdb.tbl_order AS o join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = ? AND promocode = ? AND o.is_deleted = '0' AND od.is_deleted = '0'", [user_id, request.promocode], function (error, peruserusage, fields) {
                                                if (error || peruserusage.length >= parseInt(result[0].per_user_usage)) {
                                                    callback('0', t('rest_keywords_promocode_already_used'), null);
                                                }
                                                else {
                                                    con.query("SELECT c.currency_symbol,c.currency_code,cr.rate FROM suasdb.tbl_user AS u join tbl_country AS c ON u.country = c.name join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code where u.id ='" + user_id + "' ORDER BY cr.id DESC LIMIT 0,1 ", function (usererror, resultss) {
                                                        var mainresponse = {}
                                                        if (result[0].discount_type === 'percentage') {
                                                            mainresponse.promocode_id = result[0].id;
                                                            mainresponse.promocode_name = result[0].promocode;
                                                            mainresponse.discount_type = result[0].discount_type;
                                                            mainresponse.description = result[0].description;
                                                            var perc = '%';
                                                            mainresponse.discount = parseFloat(result[0].discount);
                                                            callback('1', t('rest_keywords_promocode_applied_success'), mainresponse);

                                                        } else {
                                                            mainresponse.promocode_id = result[0].id;
                                                            mainresponse.promocode_name = result[0].promocode;
                                                            mainresponse.discount_type = result[0].discount_type;
                                                            mainresponse.description = result[0].description;
                                                            var dollar = resultss[0]['currency_symbol'];
                                                            mainresponse.discount = (resultss[0]['rate'] * parseFloat(result[0].discount));
                                                            callback('1', t('rest_keywords_promocode_applied_success'), mainresponse);

                                                        }
                                                    })
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    con.query("SELECT * FROM suasdb.tbl_user_promocode WHERE user_id = ? AND promocode_id = ? AND is_deleted = '0'", [user_id, result[0].id], function (u_promocode_error, u_promocode_result) {
                                        if (!u_promocode_error && u_promocode_result[0] != undefined) {
                                            con.query("SELECT * FROM suasdb.tbl_order AS o join tbl_order_detail AS od ON o.id = od.order_id WHERE o.user_id = ? AND promocode = ? AND o.is_deleted = '0' AND od.is_deleted = '0'", [user_id, request.promocode], function (error, peruserusage, fields) {
                                                if (error || peruserusage.length >= parseInt(result[0].per_user_usage)) {
                                                    callback('0', t('rest_keywords_promocode_already_used'), null);
                                                }
                                                else {
                                                    con.query("SELECT c.currency_symbol,c.currency_code,cr.rate FROM suasdb.tbl_user AS u join tbl_country AS c ON u.country = c.name join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code where u.id ='" + user_id + "' ORDER BY cr.id DESC LIMIT 0,1 ", function (usererror, resultss) {
                                                        var mainresponse = {}
                                                        if (result[0].discount_type === 'percentage') {
                                                            mainresponse.promocode_id = result[0].id;
                                                            mainresponse.promocode_name = result[0].promocode;
                                                            mainresponse.discount_type = result[0].discount_type;
                                                            mainresponse.description = result[0].description;
                                                            var perc = '%';
                                                            mainresponse.discount = parseFloat(result[0].discount);
                                                            callback('1', t('rest_keywords_promocode_applied_success'), mainresponse);

                                                        } else {
                                                            mainresponse.promocode_id = result[0].id;
                                                            mainresponse.promocode_name = result[0].promocode;
                                                            mainresponse.discount_type = result[0].discount_type;
                                                            mainresponse.description = result[0].description;
                                                            var dollar = resultss[0]['currency_symbol'];
                                                            mainresponse.discount = (resultss[0]['rate'] * parseFloat(result[0].discount));
                                                            callback('1', t('rest_keywords_promocode_applied_success'), mainresponse);
                                                        }
                                                    })
                                                }
                                            });
                                        } else {
                                            callback('0', t('rest_keywords_promocode_purchase'), null);
                                        }
                                    })

                                }
                            })

                        }
                    } else {
                        callback('0', t('rest_keywords_promocode_invalid'), null);
                    }
                });
            } else {
                callback(cartcode, cartmessage, cartdata);
            }
        });
    },

    validatePaymentMethod: function (request, callback) {
        if (request.payment_mode === 'Card') {
            User.get_user_carddata(request.card_id, function (carddetails) {
                if (carddetails != null) {
                    callback('1', "Proceed card payment", carddetails);
                } else {
                    callback('0', t('rest_keywords_carddetails_notfound'), null);
                }
            });
        } else {
            callback('1', "Proceed other payments", request);
        }
    },
    validatePaymentMethod_old: function (request, callback) {
        if (request.payment_mode === 'Card') {
            User.get_user_carddata(request.card_id, function (carddetails) {
                if (carddetails != null) {
                    callback('1', "Proceed card payment", request);
                } else {
                    callback('0', t('rest_keywords_carddetails_notfound'), null);
                }
            });
        } else {
            request.card_id = '0';
            callback('1', "Proceed other payments", request);
        }
    },

    /*
    ** Function to place order for users
    */
    placeorder: function (user_id, request, callback) {
        con.query("SELECT c.currency_symbol,c.currency_code,cr.rate FROM suasdb.tbl_user AS u join tbl_country AS c ON u.country = c.name join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code where u.id ='" + user_id + "' ORDER BY cr.id DESC LIMIT 0,1 ", function (usererror, resultss) {
            if (request.student_id != undefined && request.student_id != '') {
                common.getStudentDetails(request.student_id, user_id, function (stud_profile) {
                    if (stud_profile != null) {
                        auth_model.get_userdetails(user_id, function (userprofile) {
                            if (userprofile != null) {
                                User.validatePaymentMethod(request, function (requestcode, requestmessage, request_card) {
                                    if (request_card != null) {
                                        if (request.redeem_points != '' && request.redeem_points != undefined) {
                                            var point = ((request.redeem_points) / 10);
                                            var point1 = (resultss[0]['rate'] * point);
                                            var totalamount = (request.totalamount - point1);
                                            request.totalamount = totalamount;
                                            request.discount = point1;
                                        } else {
                                            var totalamount = request.totalamount;
                                        }
                                        if (totalamount <= 0) {
                                            request.totalamount = 0;
                                        }
                                        if (request.payment_mode == 'Card' && totalamount > 0) {
                                            let currency = resultss[0]['currency_code'].toLowerCase();
                                            let payment_obj = {
                                                amount: Math.round(totalamount * 100),
                                                currency: currency,
                                                customer: request_card['customer_id'],
                                                description: 'Place Order',
                                            }
                                            stripe.tranferStripePlatform(payment_obj, function (code, msg, stripe_data) {
                                                if (code == 1) {
                                                    User.validatePlaceOrder(userprofile, request, stripe_data.transaction_id, function (_ordercode, _ordermsg, _orderdetails) {
                                                        callback(_ordercode, _ordermsg, _orderdetails);
                                                    });
                                                } else {
                                                    callback(code, msg, null);
                                                }
                                            })
                                        } else {
                                            User.validatePlaceOrder(userprofile, request, 'abcd123456', function (_ordercode, _ordermsg, _orderdetails) {
                                                callback(_ordercode, _ordermsg, _orderdetails);
                                            });
                                        }


                                    } else {
                                        callback(requestcode, requestmessage, request);
                                    }
                                })
                            } else {
                                callback('2', t('rest_keywords_userdetailsnot_found'), null);
                            }
                        });
                    } else {
                        callback('2', t('rest_keywords_student_details_not_found'), null);
                    }
                })
            } else {
                auth_model.get_userdetails(user_id, function (userprofile) {
                    if (userprofile != null) {
                        User.validatePaymentMethod(request, function (requestcode, requestmessage, request_card) {
                            if (request_card != null) {
                                if (request.redeem_points != '' && request.redeem_points != undefined) {
                                    var point = ((request.redeem_points) / 10);
                                    var point1 = (resultss[0]['rate'] * point);
                                    var totalamount = (request.totalamount - point1);
                                    request.totalamount = totalamount;
                                    request.discount = point1;
                                } else {
                                    var totalamount = request.totalamount;
                                }
                                if (totalamount <= 0) {
                                    request.totalamount = 0;
                                }
                                if (request.payment_mode == 'Card' && totalamount > 0) {
                                    let currency = resultss[0]['currency_code'].toLowerCase();
                                    let payment_obj = {
                                        amount: Math.round(totalamount * 100),
                                        currency: currency,
                                        customer: request_card['customer_id'],
                                        description: 'Place Order',
                                    }
                                    stripe.tranferStripePlatform(payment_obj, function (code, msg, stripe_data) {
                                        if (code == 1) {
                                            User.validatePlaceOrder(userprofile, request, stripe_data.transaction_id, function (_ordercode, _ordermsg, _orderdetails) {
                                                callback(_ordercode, _ordermsg, _orderdetails);
                                            });
                                        } else {
                                            callback(code, msg, null);
                                        }
                                    })
                                } else {
                                    User.validatePlaceOrder(userprofile, request, 'abcd123456', function (_ordercode, _ordermsg, _orderdetails) {
                                        callback(_ordercode, _ordermsg, _orderdetails);
                                    });
                                }


                            } else {
                                callback(requestcode, requestmessage, request);
                            }
                        })
                    } else {
                        callback('2', t('rest_keywords_userdetailsnot_found'), null);
                    }
                });
            }
        })
    },

    placeorder_old: function (user_id, request, callback) {
        if (request.student_id != undefined && request.student_id != '') {
            common.getStudentDetails(request.student_id, user_id, function (stud_profile) {
                if (stud_profile != null) {
                    auth_model.get_userdetails(user_id, function (userprofile) {
                        if (userprofile != null) {
                            User.validatePaymentMethod(request, function (requestcode, requestmessage, request) {
                                if (request != null) {
                                    User.validatePlaceOrder(userprofile, request, function (_ordercode, _ordermsg, _orderdetails) {
                                        callback(_ordercode, _ordermsg, _orderdetails);
                                    });
                                } else {
                                    callback(requestcode, requestmessage, request);
                                }
                            })
                        } else {
                            callback('2', t('rest_keywords_userdetailsnot_found'), null);
                        }
                    });
                } else {
                    callback('2', t('rest_keywords_student_details_not_found'), null);
                }
            })
        } else {
            auth_model.get_userdetails(user_id, function (userprofile) {
                if (userprofile != null) {
                    User.validatePaymentMethod(request, function (requestcode, requestmessage, request) {
                        if (request != null) {
                            User.validatePlaceOrder(userprofile, request, function (_ordercode, _ordermsg, _orderdetails) {
                                callback(_ordercode, _ordermsg, _orderdetails);
                            });
                        } else {
                            callback(requestcode, requestmessage, request);
                        }
                    })
                } else {
                    callback('2', t('rest_keywords_userdetailsnot_found'), null);
                }
            });
        }
    },

    /**
    ** Function to place order for users 
    */
    validatePlaceOrder: function (userprofile, request, transaction_id, callback) {
        var orderobject = {
            uniqueid: Math.floor(100000 + Math.random() * 900000),
            item_type: 'video',
            item_id: '0',
            transaction_id: transaction_id,
            user_id: userprofile.id,
            card_id: request.card_id,
            payment_mode: request.payment_mode,
            payment_status: 'pending',
            status: 'Pending',
            subtotal: request.subtotal,
            tax: request.tax,
            discount: (request.discount != undefined && request.discount != "") ? request.discount : 0,
            totalamount: Number(request.totalamount),
            status: 'Confirm',
            validity: require('node-datetime').create().format('Y-m-d H:M:S'),
            promocode: (request.promocode != undefined && request.promocode != "") ? request.promocode : "",
            student_id: (request.student_id != undefined && request.student_id != "") ? request.student_id : 0
        };

        var orderQuery = con.query('INSERT INTO tbl_order SET ?', orderobject, function (err, result, fields) {
            if (!err) {

                asyncLoop(request.suborder, function (item, next) {
                    var order_item = {
                        item_type: item.item_type,
                        item_id: item.item_id,
                        order_id: result.insertId,
                        price: item.price,
                        totalamount: Number(request.totalamount),
                    }
                    User.check_itemDetailsByID(item.item_id, item.item_type, userprofile.id, function (_itemdetails) {
                        if (_itemdetails != null) {
                            con.query('INSERT INTO tbl_order_detail SET ?', order_item, function (err, res, fields) {
                                if ((item.total_points != undefined && item.total_points != '') && (request.redeem_points != undefined && request.redeem_points != '')) {
                                    var wallet = {
                                        item_type: item.item_type,
                                        item_id: item.item_id,
                                        user_id: userprofile.id,
                                        order_id: result.insertId,
                                        total_point: item.total_points,
                                        redeem_points: request.redeem_points,
                                        reward_points: parseInt(0) - parseInt(request.redeem_points),
                                        status: 'debited',
                                    }
                                    con.query('INSERT INTO tbl_wallet SET ?', wallet, function (err, res, fields) {
                                        var wallet_c = {
                                            item_type: item.item_type,
                                            item_id: item.item_id,
                                            user_id: userprofile.id,
                                            order_id: result.insertId,
                                            total_point: item.total_points,
                                            redeem_points: 0,
                                            reward_points: item.total_points,
                                            status: 'credited',
                                        }
                                        con.query('INSERT INTO tbl_wallet SET ?', wallet_c, function (err, res, fields) {
                                            if (item.item_type == 'speech') {
                                                con.query("UPDATE tbl_user_speech SET is_deleted = '0' WHERE id = '" + item.item_id + "'", function (speech_error, speech_result) {
                                                    next();
                                                })
                                            } else if (item.item_type == 'contest') {
                                                if (_itemdetails.contest_type == 'live') {
                                                    var param = {
                                                        user_id: userprofile.id,
                                                        contest_id: item.item_id,
                                                        video: 'default.png',
                                                        thumb_image: 'default.png',
                                                        video_duration: '0'
                                                    }
                                                    con.query("INSERT INTO tbl_user_contest SET ?", param, function (err, res, fields) {
                                                        next();
                                                    })
                                                } else {
                                                    next();
                                                }
                                            } else {
                                                next();
                                            }
                                        })

                                    })
                                } else {
                                    var wallet = {
                                        item_type: item.item_type,
                                        item_id: item.item_id,
                                        user_id: userprofile.id,
                                        order_id: result.insertId,
                                        total_point: item.total_points,
                                        redeem_points: 0,
                                        reward_points: item.total_points,
                                        status: 'credited',
                                    }
                                    con.query('INSERT INTO tbl_wallet SET ?', wallet, function (err, res, fields) {
                                        if (item.item_type == 'speech') {
                                            con.query("UPDATE tbl_user_speech SET is_deleted = '0' WHERE id = '" + item.item_id + "'", function (speech_error, speech_result) {
                                                next();
                                            })
                                        } else if (item.item_type == 'contest') {
                                            if (_itemdetails.contest_type == 'live') {
                                                var param = {
                                                    user_id: userprofile.id,
                                                    contest_id: item.item_id,
                                                    video: 'default.png',
                                                    thumb_image: 'default.png',
                                                    video_duration: '0'
                                                }
                                                con.query("INSERT INTO tbl_user_contest SET ?", param, function (err, res, fields) {

                                                    next();
                                                })
                                            } else {
                                                next();
                                            }
                                        } else {
                                            next();
                                        }
                                    })
                                }

                                // }
                            });
                        } else {
                            callback('0', t('rest_keywords_someproductsare_missing'), null);
                            return;
                        }
                    });

                }, function () {


                    User.addpromocodeUsage(userprofile.id, request, function (usage_id) {
                        var where = '';
                        if (request.student_id != undefined && request.student_id != '' && request.student_id != 0) { where = " AND student_id = '" + request.student_id + "' "; }
                        con.query("DELETE FROM tbl_user_cart WHERE user_id='" + userprofile.id + "' " + where + "", function () { });
                        con.query("SELECT tor.*, IFNULL(tmc.name,'') as contest_name, IFNULL(tms.name,'') as subcription_name, IFNULL(tc.program_title,'') as program_title, IFNULL(tue.title,'') as speech_title, IFNULL(tmv.name,'') as video, IFNULL(tp.promocode,'') as promocodes FROM tbl_order tor \
                            LEFT JOIN tbl_user tu ON tor.user_id = tu.id\
                            LEFT JOIN tbl_master_contest tmc ON tor.item_id = tmc.id AND item_type='contest'\
                            LEFT JOIN tbl_master_subscription tms ON tor.item_id = tms.id AND item_type='subscription'\
                            LEFT JOIN tbl_master_class tc ON tor.item_id = tc.id AND item_type='class'\
                            LEFT JOIN tbl_user_speech tue ON tor.item_id = tue.id AND item_type='speech'\
                            LEFT JOIN tbl_master_videolibrary tmv ON tor.item_id = tmv.id AND item_type='video'\
                            LEFT JOIN tbl_promocode tp ON tor.item_id = tp.id AND item_type='promocode'\
                            WHERE tor.id = '"+ result.insertId + "' AND tor.is_deleted = '0' ", function (errorss, resultss, fieldss) {

                            if (!errorss && resultss[0] != undefined) {
                                request.order_id = result.insertId;
                                if (request.item_type == 'class') {
                                    callback('1', t('rest_keywords_enrolledclass_success'), resultss[0]);
                                } else if (request.item_type == 'subscription') {
                                    callback('1', t('rest_keywords_subscription_success'), resultss[0]);
                                } else if (request.item_type == 'contest') {
                                    callback('1', t('rest_keywords_contest_success'), resultss[0]);
                                } else if (request.item_type == 'video') {
                                    callback('1', t('rest_keywords_videosubmit_success'), resultss[0]);
                                } else if (request.item_type == 'promocode') {
                                    callback('1', t('rest_keywords_promocodesubmit_success'), resultss[0]);
                                } else {
                                    callback('1', t('rest_keywords_speechsubmit_success'), resultss);
                                }
                            } else {
                                callback('2', t('rest_keywords_no_data_found'));
                            }
                        });
                    });
                })

            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    validatePlaceOrder_old: function (userprofile, request, transaction_id, callback) {
        var orderobject = {
            uniqueid: Math.floor(100000 + Math.random() * 900000),
            item_type: request.item_type,
            item_id: request.item_id,
            transaction_id: transaction_id,
            user_id: userprofile.id,
            card_id: request.card_id,
            payment_mode: request.payment_mode,
            payment_status: 'pending',
            status: 'Pending',
            subtotal: request.subtotal,
            tax: request.tax,
            discount: request.discount,
            totalamount: Number(request.totalamount),
            status: 'Confirm',
            validity: require('node-datetime').create().format('Y-m-d H:M:S'),
            promocode: (request.promocode != undefined && request.promocode != "") ? request.promocode : "",
            student_id: (request.student_id != undefined && request.student_id != "") ? request.student_id : 0
        };

        var orderQuery = con.query('INSERT INTO tbl_order SET ?', orderobject, function (err, result, fields) {
            if (!err) {

                asyncLoop(request.suborder, function (item, next) {
                    var order_item = {
                        item_id: item.item_id,
                        order_id: result.insertId,
                        price: item.price,
                        totalamount: request.totalamount,
                        item_type: request.item_type,
                    }
                    User.check_itemDetailsByID(item.item_id, request.item_type, userprofile.id, function (_itemdetails) {
                        if (_itemdetails != null) {
                            con.query('INSERT INTO tbl_order_detail SET ?', order_item, function (err, res, fields) {
                                next();
                            });
                        } else {
                            callback('0', t('rest_keywords_someproductsare_missing'), null);
                            return;
                        }
                    });

                }, function () {
                    if (request.item_type == 'class' || request.item_type == 'contest' || request.item_type == 'video') {

                        if ((request.total_points != undefined && request.total_points != '') && (request.redeem_points != undefined && request.redeem_points != '')) {
                            var wallet = {
                                // item_type           : request.item_type,
                                item_id: request.item_id,
                                user_id: userprofile.id,
                                order_id: result.insertId,
                                total_point: request.total_points,
                                redeem_points: request.redeem_points,
                                reward_points: parseInt(request.total_points) - parseInt(request.redeem_points),
                                status: 'debited',
                            }
                            con.query('INSERT INTO tbl_wallet SET ?', wallet, function (err, res, fields) {
                            })
                        } else {
                            var wallet = {
                                item_type: request.item_type,
                                item_id: request.item_id,
                                user_id: userprofile.id,
                                order_id: result.insertId,
                                total_point: request.total_points,
                                redeem_points: 0,
                                reward_points: result.total_points,
                                status: 'credited',
                            }
                            con.query('INSERT INTO tbl_wallet SET ?', wallet, function (err, res, fields) {
                            })
                        }

                    }

                    User.addpromocodeUsage(userprofile.id, request, function (usage_id) {
                        con.query("DELETE FROM tbl_user_cart WHERE user_id='" + userprofile.id + "'", function () { });
                        con.query("SELECT tor.*, IFNULL(tmc.name,'') as contest_name, IFNULL(tms.name,'') as subcription_name, IFNULL(tc.program_title,'') as program_title, IFNULL(tue.title,'') as speech_title, IFNULL(tmv.name,'') as video, IFNULL(tp.promocode,'') as promocodes FROM tbl_order tor \
                            LEFT JOIN tbl_user tu ON tor.user_id = tu.id\
                            LEFT JOIN tbl_master_contest tmc ON tor.item_id = tmc.id AND item_type='contest'\
                            LEFT JOIN tbl_master_subscription tms ON tor.item_id = tms.id AND item_type='subscription'\
                            LEFT JOIN tbl_master_class tc ON tor.item_id = tc.id AND item_type='class'\
                            LEFT JOIN tbl_user_speech tue ON tor.item_id = tue.id AND item_type='speech'\
                            LEFT JOIN tbl_master_videolibrary tmv ON tor.item_id = tmv.id AND item_type='video'\
                            LEFT JOIN tbl_promocode tp ON tor.item_id = tp.id AND item_type='promocode'\
                            WHERE tor.id = '"+ result.insertId + "' AND tor.is_deleted = '0' ", function (errorss, resultss, fieldss) {

                            if (!errorss && resultss[0] != undefined) {
                                request.order_id = result.insertId;

                                payment_model.make_payment(userprofile.id, request, function (code, msg, data) {
                                    if (code == '1') {
                                        if (request.item_type == 'class') {
                                            callback('1', t('rest_keywords_enrolledclass_success'), resultss[0]);
                                        } else if (request.item_type == 'subscription') {
                                            callback('1', t('rest_keywords_subscription_success'), resultss[0]);
                                        } else if (request.item_type == 'contest') {
                                            callback('1', t('rest_keywords_contest_success'), resultss[0]);
                                        } else if (request.item_type == 'video') {
                                            callback('1', t('rest_keywords_videosubmit_success'), resultss[0]);
                                        } else if (request.item_type == 'promocode') {
                                            callback('1', t('rest_keywords_promocodesubmit_success'), resultss[0]);
                                        } else {
                                            callback('1', t('rest_keywords_speechsubmit_success'), resultss[0]);
                                        }
                                    } else {
                                        callback(code, msg, data);
                                    }
                                });
                            } else {
                                callback('2', t('rest_keywords_no_data_found'));
                            }
                        });
                    });
                })

            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    check_itemDetailsByID: function (item_id, item_type, user_id, callback) {
        if (item_type == 'class') {
            con.query("SELECT tmc.* FROM tbl_master_class tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'class'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "' AND tmc.is_deleted='0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                } else {
                    callback(null);
                }
            })
        } else if (item_type == 'subscription') {
            con.query("SELECT tmc.* FROM tbl_master_subscription tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'subcription'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "' AND tmc.is_deleted='0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } else if (item_type == 'contest') {
            con.query("SELECT tmc.* FROM tbl_master_contest tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'contest'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "' AND tmc.is_deleted='0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } else if (item_type == 'video') {
            con.query("SELECT tmc.* FROM tbl_master_videolibrary tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'video'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "' AND tmc.is_deleted='0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } else if (item_type == 'promocode') {
            con.query("SELECT tmc.* FROM tbl_promocode tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'promocode'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "' AND tmc.is_deleted='0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        } else {
            con.query("SELECT tmc.* FROM tbl_user_speech tmc LEFT JOIN tbl_user_cart tuc ON tmc.id = tuc.item_id AND item_type = 'class'  AND tuc.user_id='" + user_id + "' WHERE tmc.id = '" + item_id + "'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }
            })
        }
    },

    /*
    ** Function to add promocode usage
    */
    addpromocodeUsage: function (user_id, request, callback) {
        if (request.promocode != undefined && request.promocode != "") {
            con.query("select * from tbl_promocode where promocode = ? GROUP BY id ", [request.promocode], function (error, result, fields) {
                if (!error && result[0] != undefined) {
                    var promouse = {
                        user_id: user_id,
                        promocode_id: result[0].id
                    };
                    con.query("INSERT INTO tbl_usedpromocode SET ? ", promouse, function (err, res, fields) {
                        callback(true);
                    })
                } else {
                    callback(true);
                }
            });
        } else {
            callback(true);
        }
    },

    /*
    ** Function to get user evaluation details
    */
    get_user_evaluation: function (user_id, request, callback) {
        var where = '';
        if (request.student_id != undefined && request.student_id != '') {
            where += "AND te.student_id = '" + request.student_id + "'";
        }
        if (request.evaluation_id != undefined && request.evaluation_id != '') {
            where += " AND te.id = '" + request.evaluation_id + "'";
        }
        con.query("SELECT te.*, ts.is_evaluated , concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.video) as video, ts.video_duration FROM tbl_user_evaluation te LEFT JOIN tbl_user_speech ts ON te.speech_id = ts.id WHERE te.user_id = '" + user_id + "' AND te.is_deleted ='0' " + where + " ORDER BY id DESC", function (err, res, fields) {
            if (!err && res[0] != undefined) {

                con.query("SELECT *, name as title, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',video) as video, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',thumb_image) as video_image, (video_duration) as video_duration from tbl_suggested_courses WHERE is_deleted='0' AND status='Active' GROUP BY id ORDER BY id DESC", function (error, result, field) {
                    if (!error && result[0] != undefined) {
                        var temp = {
                            evaluation: res,
                            suggested_course: result
                        }
                        callback('1', t('rest_keywords_userevaluation_found'), temp);
                    } else {
                        res.suggested_course = [];
                        callback('1', t('rest_keywords_userevaluation_found'), res);
                    }
                })

            } else {
                callback('2', t('rest_keywords_userevaluationnot_found'), null);
            }
        })
    },

    /*
    ** Function to clear notification single and multiple
    */
    clear_notification: function (user_id, request, callback) {
        if (request.notification_id != undefined && request.notification_id != '') {
            var removeCondition = "id='" + request.notification_id + "'";
        } else {
            var removeCondition = "receiver_id='" + user_id + "' AND receiver_type='user' ";
        }
        con.query("DELETE FROM tbl_notification WHERE " + removeCondition + "", function (err, res, fields) {
            if (request.notification_id != undefined && request.notification_id != '') {
                callback('1', t('rest_keywords_notificationcleared_success'), null);
            } else {
                callback('1', t('rest_keywords_notificationlist_deleted_success'), null);
            }
        });
    },

    upload_contestvideo: function (user_id, request, callback) {
        var param = {
            user_id: user_id,
            contest_id: request.contest_id,
            video: request.video,
            thumb_image: request.thumb_image,
        }
        getVideoDurationInSeconds(GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + request.video).then((duration) => {

            const duration_format = new Date(duration * 1000).toISOString().slice(11, 19);
            param.video_duration = duration_format;
            con.query("INSERT INTO tbl_user_contest SET ?", param, function (err, res, fields) {
                if (!err) {
                    callback('1', t('rest_keywords_videoupload_contest_success'), param);
                } else {
                    callback('2', t('rest_keywords_something_went_wrong'), null);
                }
            })
        });
    },

    get_winnercontest_list: function (user_id, callback) {
        con.query("SELECT tuc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.contest_image) as contest_image, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tc.thumb_image) as contest_thumb_image, tc.name, tc.contest_type, tc.video_duration as contest_video_duration,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tuc.video) as video, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CONTEST_IMAGE + "','',tuc.thumb_image) as thumb_image  FROM tbl_user_contest tuc LEFT JOIN tbl_master_contest tc ON tuc.contest_id = tc.id WHERE tc.status='Active' AND tc.is_deleted='0' AND tuc.user_id = '" + user_id + "' AND tuc.winner != '0' ORDER BY tuc.id DESC ", function (error, results, fields) {
            if (!error && results[0] != undefined) {
                asyncLoop(results, function (item, next) {
                    service_model.get_prize_list(user_id, item.contest_id, function (prizeerr, prizermsg, prizedata) {
                        item.prizelist = prizedata;
                        service_model.get_sponsor_list(item.contest_id, function (sponsorerr, sponsorrmsg, sponsordata) {
                            item.sponsorlist = sponsordata;
                            service_model.get_scorecard_list(item.contest_id, function (scorecarderr, scorecardrmsg, scorecarddata) {
                                item.scorecardlist = scorecarddata;
                                con.query("SELECT tuc.*, tc.username, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',tc.profile_image) as user_image, tc.country FROM tbl_user_contest tuc LEFT JOIN tbl_user tc ON  tc.id = tuc.user_id WHERE tuc.is_deleted='0' AND tc.is_deleted = '0' AND tuc.status = 'Active' AND tuc.contest_id = '" + item.contest_id + "' ORDER BY tuc.id DESC ", function (winner_err, winner_res, winner_fields) {
                                    item.winnerlist = winner_res;
                                    next();
                                })
                            })
                        })
                    });
                }, function () {
                    callback('1', t('rest_keywords_winnerlist_found_success'), results);
                });
            } else {
                callback('0', t('rest_keywords_winnerlist_notfound'));
            }
        });
    },


    set_manual_evaluation: function (user_id, request, callback) {
        var param = {
            is_request: '1',
            factor_name: request.name
        }
        con.query("UPDATE tbl_user_evaluation SET ? WHERE id = '" + request.evaluation_id + "'", param, function (error, result, fields) {
            if (!error) {
                callback('1', t('rest_keywords_submited_manual_evaluation_success'));
            } else {
                callback('0', t('rest_keywords_submited_manual_evaluation_failed'), null);
            }
        });
    },

    check_userChannel: function (request, callback) {
        var query = '';
        if (request.type == 'class') {
            query = "SELECT * FROM tbl_master_class WHERE id = '" + request.class_id + "' ";
        } else {
            query = "SELECT * FROM tbl_master_contest WHERE id = '" + request.class_id + "' ";
        }
        con.query(query, function (err, res, fields) {
            if (!err && res[0] != undefined) {
                User.generateAgoraViCall(request.user_id, res[0].channel_name, function (response) {
                    response.host_id = res[0]['host_id'];
                     response.speaker_id = res[0]['speaker_id'];
                     if(response.host_id == response.speaker_id){
                        response.is_mute = 0;
                        response.is_video = 0;
                        callback('1', t('rest_keywords_token_generate_success'), response);
                     }else{
                        console.log(request);
                        var sql = "SELECT o.user_id, od.* FROM suasdb.tbl_order_detail AS od JOIN tbl_order AS o ON o.id = od.order_id WHERE od.item_type = '"+request.type+"' AND od.item_id = '"+request.class_id+"' AND o.user_id = '"+request.user_id+"' AND od.is_deleted = '0'";
                        con.query(sql,function(order_error,order_result){
                            console.log(order_error);
                            console.log(order_result);
                            console.log(sql);
                            if(!order_error && order_result[0] != undefined){
                                if(request.type == 'class'){
                                  response.is_mute = order_result[0]['is_mute_classess'];
                                  response.is_video = order_result[0]['is_video_classess'];
                                }else{
                                  response.is_mute = order_result[0]['is_mute_contest'];
                                  response.is_video = order_result[0]['is_video_contest'];
                                }
                                callback('1', t('rest_keywords_token_generate_success'), response); 
                            }else{
                                 response.is_mute = 0;
                                response.is_video = 0;
                                callback('1', t('rest_keywords_token_generate_success'), response); 
                            }
                        })
                     }
                    
                });
            } else {
                callback('0', t('rest_keywords_no_data_found'));
            }
        })
    },


    generateAgoraViCall: function (loginusers_id, channelName, callback) {
        var mainresponse = {}
        var { RtcTokenBuilder, RtcRole } = require('agora-access-token');
        var appID = GLOBALS.APP_ID;
        var appCertificate = GLOBALS.APP_CERTIFICATE;
        var expirationTimeInSeconds = 14400;
        var uid = 0;
        var role = RtcRole.PUBLISHER;
        var currentTimestamp = Math.floor(Date.now() / 1000);
        var privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds;
        var key = RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, channelName, uid, role, privilegeExpiredTs);
        mainresponse.channelname = channelName;
        mainresponse.token = key;
        callback(mainresponse);
    },

    generateAgoraChannel: function (loginusers_id, callback) {
        var channelName = "AAA" + loginusers_id + "AAA" + Math.floor(Date.now() / 1000);
        callback(channelName);
    },
    /**
    * This function is used to create the rtm token
    * @param {Request} request 
    * @param {Function} callback 
    */
    generate_token_agora_RTM: function (request, callback) {
        const RtmTokenBuilder = require('../../../src/RtmTokenBuilder').RtmTokenBuilder;
        const RtmRole = require('../../../src/RtmTokenBuilder').Role;
        const Priviledges = require('../../../src/AccessToken').priviledges;
        const appID = "9a66bf67924441818932f9fccc95a960";
        const appCertificate = "6f1d7020c7024163b88fe4443ce11513";
        const userId = String(request.user_id);
        const account = userId;
        const expirationTimeInSeconds = 3600
        const currentTimestamp = Math.floor(Date.now() / 1000)

        const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds

        const token = RtmTokenBuilder.buildToken(appID, appCertificate, account, RtmRole, privilegeExpiredTs);
        var response = { "token": token };
        callback("1", t("rest_keywords_something_went_wrong"), response);

    },

    get_user_giftClassDetails: function (user_id, request, callback) {
        if (request.gift_type == 'giftto') {
            con.query("SELECT tg.*, tc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CLASS_IMAGE + "','',tc.class_image) as class_image FROM tbl_gift_enrolledclass tg LEFT JOIN tbl_master_class tc ON tg.class_id = tc.id WHERE tg.user_id = '" + user_id + "' AND tg.is_deleted = '0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    asyncLoop(result, function (item, next) {
                        con.query("SELECT *, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',profile_image) as profile_image  FROM tbl_user WHERE id = '" + item.user_id + "' AND is_deleted = '0'", function (error, res, fields) {
                            if (!error && res[0] != undefined) {
                                item.gifted_to = res[0].username;
                                item.userdetails = res[0];
                                next();
                            } else {
                                next();
                            }
                        })
                    }, function () {
                        callback('1', t('rest_keywords_gift_found'), result);
                    })


                } else {
                    callback('0', t('rest_keywords_not_gift_found'));
                }
            })
        } else {
            con.query("SELECT tg.*, tc.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.CLASS_IMAGE + "','',tc.class_image) as class_image FROM tbl_gift_enrolledclass tg LEFT JOIN tbl_master_class tc ON tg.class_id = tc.id WHERE tg.user_id = '" + user_id + "' AND tg.is_deleted = '0'", function (err, result, fields) {
                if (!err && result[0] != undefined) {
                    asyncLoop(result, function (item, next) {
                        con.query("SELECT *, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',profile_image) as profile_image  FROM tbl_user WHERE email = '" + item.email + "' AND is_deleted = '0'", function (error, res, fields) {
                            if (!error && res[0] != undefined) {
                                item.gifted_from = res[0].username;
                                item.userdetails = res[0];
                                next();
                            } else {
                                next();
                            }
                        })
                    }, function () {
                        callback('1', t('rest_keywords_gift_found'), result);
                    })


                } else {
                    callback('0', t('rest_keywords_not_gift_found'));
                }
            })
        }
    },


    get_purchase_video: function (user_id, callback) {
        con.query("SELECT * FROM suasdb.tbl_user where id = '" + user_id + "'", function (usererror, resultss) {
            con.query("SELECT tor.*, tv.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',tv.video) as video,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',tv.image) as image,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.VIDEOLIBRARY_IMAGE + "','',tv.thumb_image) as thumb_image, (SELECT (cr.rate*tv.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '" + resultss[0]['country'] + "' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '" + resultss[0]['country'] + "') as currency_symbol FROM tbl_order tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id  LEFT JOIN tbl_master_videolibrary tv ON od.item_id = tv.id WHERE tor.user_id = '" + user_id + "' AND od.item_type = 'video' AND tor.status = 'Confirm' AND tor.is_deleted='0'", function (err, res, fields) {

                if (!err && res[0] != undefined) {
                    callback('1', t('rest_keywords_purchased_video_found'), res);
                } else {
                    callback('0', t('rest_keywords_purchased_video_not_found'));
                }
            })
        })
    },


    notification_serviceupdate: function (user_id, request, callback) {
        var param = {
            user_id: user_id,
            push_notification: request.push_notification,
            email: request.email,
            sms: request.sms
        }
        con.query("SELECT * FROM tbl_user_notification_setting WHERE user_id = '" + user_id + "' AND is_deleted = '0'", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                con.query("UPDATE tbl_user_notification_setting SET ? WHERE user_id = '" + user_id + "'", param, function (error, result, fields) {
                    if (!error) {
                        callback('1', t('rest_keywords_notification_setting_updated'));
                    } else {
                        callback('0', t('rest_keywords_notification_setting_not_updated'));
                    }
                })
            } else {
                con.query("INSERT INTO tbl_user_notification_setting SET ?", param, function (error, result, fields) {
                    if (!error) {
                        callback('1', t('rest_keywords_notification_setting_added'));
                    } else {
                        callback('0', t('rest_keywords_notification_setting_not_added'));
                    }
                })
            }
        })
    },

    notification_setting: function (user_id, callback) {
        var sql = "SELECT * FROM suasdb.tbl_user_notification_setting where user_id =  '" + user_id + "' AND is_deleted = '0' ";
        con.query(sql, function (error, result) {
            if (!error && result.length > 0) {
                callback("1", t('rest_keywords_notification_setting_added'), result[0]);
            } else if (!error) {
                callback("2", t('rest_keywords_notification_setting_not_added'), result[0]);
            }
            else {
                callback("0", t('rest_keywords_notification_setting_not_added'), null);
            }
        })
    },
    get_reward_points: function (user_id, callback) {
        con.query("SELECT SUM(reward_points) as total_point FROM tbl_wallet WHERE user_id = '" + user_id + "' AND is_deleted = '0'", function (err, res, fields) {

            if (!err && res[0] != undefined) {
                con.query("SELECT tb.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.BANNER_IMAGE + "','',tb.banner_image) as banner_image FROM tbl_banner tb WHERE tb.status= 'Active' AND tb.is_deleted = '0'", function (error, result, fields) {

                    if (!error && result[0] != undefined) {
                        res[0].banner_image = result[0].banner_image;
                        res[0].title = result[0].title;
                        callback('1', t('rest_keywords_reward_point_found'), res[0]);
                    } else {
                        callback('1', t('rest_keywords_reward_point_found'), res[0]);
                    }
                })

            } else {
                callback('0', t('rest_keywords_reward_point_not_found'));
            }
        })
    },

    get_reward_points_history: function (user_id, callback) {
        con.query("SELECT tw.* FROM tbl_wallet tw WHERE tw.user_id = '" + user_id + "' AND tw.is_deleted='0' ORDER BY tw.id DESC", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                callback('1', t('rest_keywords_reward_point_found'), res);
            } else {
                callback('0', t('rest_keywords_reward_point_not_found'));
            }
        })
    },

    get_homepage_details: function (user_id, callback) {

        var current_datetime = require('node-datetime').create().format('Y-m-d H:M:S');

        var where = " AND contest_startdate >= '" + current_datetime + "'";
        con.query("SELECT * FROM tbl_master_contest WHERE is_deleted = '0' AND status = 'Active' " + where + " ORDER BY contest_startdate ASC", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                con.query("SELECT COUNT(id) as total_participants FROM tbl_order WHERE status = 'Confirm' AND is_deleted = '0' AND item_type = 'contest' AND item_id = '" + res[0].id + "'", function (error, result, fields) {
                    res[0].total_participants = result[0].total_participants;
                    con.query("SELECT te.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.video) as video, ts.video_duration FROM tbl_user_evaluation te LEFT JOIN tbl_user_speech ts ON te.speech_id = ts.id WHERE te.user_id = '" + user_id + "' AND te.is_deleted ='0' ORDER BY te.id DESC", function (errors, resultss, fields) {
                        var params = {
                            contest_data: res[0],
                            speech_data: resultss[0],
                        }
                        if (!errors && resultss.length > 0) {
                            var params = {
                                contest_data: res[0],
                                speech_data: resultss[0]
                            }

                        } else {
                            var params = {
                                contest_data: null,
                                speech_data: null,
                            }
                        }
                        callback('1', t('rest_keywords_homepage_data_found'), params);
                    })
                    // })
                });
            } else {
                con.query("SELECT te.*, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.thumb_image) as video_image,  concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.SPEECH_IMAGE + "','',te.video) as video, ts.video_duration FROM tbl_user_evaluation te LEFT JOIN tbl_user_speech ts ON te.speech_id = ts.id WHERE te.user_id = '" + user_id + "' AND te.is_deleted ='0' ORDER BY te.id DESC", function (errors, resultss, fields) {
                    if (!errors && resultss.length > 0) {
                        var params = {
                            contest_data: null,
                            speech_data: resultss[0],
                        }

                    } else {
                        var params = {
                            contest_data: null,
                            speech_data: null,
                        }
                    }
                    callback('1', t('rest_keywords_homepage_data_found'), params);
                })
            }
        })
    },

    get_notification_count: function (user_id, callback) {
        var query = con.query("SELECT COUNT(tn.id) as notification_count FROM tbl_notification as tn LEFT JOIN tbl_user tu ON tn.sender_id=tu.id WHERE tn.receiver_id = '" + user_id + "' AND tn.receiver_type='user' AND tn.is_read = 'unread'", function (err, result, fields) {
            if (!err && result[0] != undefined) {
                callback('1', t('rest_keywords_notification_count'), result[0]);
            } else {
                callback('0', t('rest_keywords_no_notify_count'));
            }
        });
    },


    createPaymentIntent: function (user_id, request, callback) {
        con.query("SELECT * FROM tbl_user WHERE id = '" + user_id + "' AND is_deleted='0'", function (errs, ress, fields) {
            if (!errs && ress[0] != undefined) {
                User.get_user_carddata(request.card_id, function (carddetails) {
                    if (request.redeem_points != '' && request.redeem_points != undefined) {
                        var point = ((request.redeem_points) / 10);
                        var totalamount = (request.totalamount - point);
                    } else {
                        var totalamount = request.totalamount;
                    }
                    if (carddetails != null) {
                        var paymentObject = {
                            amount: Math.round(totalamount * 100),
                            currency: 'usd',
                            payment_method_types: ['card'],
                            customer: carddetails.customer_id,
                            description: "Purchase package, charge of user: #" + user_id,
                            receipt_email: ress[0].email,
                            confirm: true,
                            statement_descriptor: GLOBALS.APP_NAME,
                            statement_descriptor_suffix: GLOBALS.APP_NAME,
                            setup_future_usage: 'off_session',
                            confirmation_method: 'automatic',
                            capture_method: 'automatic',
                            use_stripe_sdk: 'true'
                        }
                        stripe.createPaymentIntent(paymentObject, function (code, message, data) {
                            callback(code, message, data);
                        })
                    } else {
                        callback('0', t('rest_keywords_carddetails_notfound'), null);
                    }
                })
            } else {
                callback('2', t('rest_keywords_no_data_found'));
            }

        })

    },
    get_total_user_video: function (user_id, request, callback) {
        var concat_query = "";
        if (request.student_id != undefined && request.student_id != '') {
            concat_query += " AND student_id = '" + request.student_id + "'";
        }
        var sql = "SELECT COUNT(id) AS total_user_video FROM tbl_user_evaluation WHERE user_id = '" + user_id + "' AND is_deleted='0'" + concat_query;
        con.query(sql, function (error, result) {
            if (!error && result.length) {
                var sql_speech = "SELECT is_display_speech_pop FROM `tbl_user_speech` WHERE user_id = '" + user_id + "'" + concat_query;
                con.query(sql_speech, function (speech_error, speech_result) {
                    if (!speech_error && speech_result.length > 0) {
                        result[0]['is_display_speech_pop'] = speech_result[0]['is_display_speech_pop'];
                        callback("1", t("rest_keywords_total_user_video"), result[0]);
                    } else {
                        result[0]['is_display_speech_pop'] = 0;
                        callback("1", t("rest_keywords_total_user_video"), result[0]);
                    }
                })
            } else {
                result[0]['total_user_video'] = 0;
                callback("0", t("rest_keywords_total_user_video_failed"), result[0]);
            }
        })
    },

    check_user_block_unblock_status: function (request, callback) {
        if (request.type == 'contest') {
            var sql_query = "SELECT * FROM suasdb.tbl_master_contest where id = '" + request.id + "'";
        } else {
            var sql_query = "SELECT * FROM suasdb.tbl_master_class where id = '" + request.id + "'";
        }
        con.query(sql_query, function (errors, resultss) {
            if (!errors && resultss[0] != undefined) {
                if (resultss[0]['is_host'] == '0') {
                    var data = {
                        "is_host": 0,
                        "is_block": 0,
                    }
                    var message = (request.type == 'contest') ? "rest_keywords_host_not_join_contest" : "rest_keywords_host_not_join_class";
                    callback("1", t(message), data);
                } else {
                    var sql = "SELECT od.*,o.user_id FROM suasdb.tbl_order_detail AS od join tbl_order AS o on o.id = od.order_id where od.item_type = '" + request.type + "' AND od.item_id = '" + request.id + "' AND o.user_id = '" + request.user_id + "'";
                    con.query(sql, function (error, result) {
                        if (!error && result[0] != undefined) {
                            if (request.type == 'contest') {
                                var data = {
                                    "is_host": 1,
                                    "is_block": result[0]['is_block_contest'],
                                    "is_mute": result[0]['is_mute_contest'],
                                    "is_video": result[0]['is_video_contest'],
                                }
                                var message = (result[0]['is_block_contest'] == '1') ? "rest_keywords_user_block" : "rest_keywords_user_unblock";
                            } else {
                                var data = {
                                    "is_host": 1,
                                    "is_block": result[0]['is_block_classess'],
                                    "is_mute": result[0]['is_mute_classess'],
                                    "is_video": result[0]['is_video_classess'],
                                }
                                var message = (result[0]['is_block_contest'] == '1') ? "rest_keywords_user_block" : "rest_keywords_user_unblock";
                            }
                            callback("1", t(message), data);
                        } else {
                            callback("0", t("rest_keywords_something_went_wrong"), null);
                        }
                    })
                }

            } else {
                callback("0", t("rest_keywords_something_went_wrong"), null);
            }
        })

    },
    /**
     * This function is used to get the booking user list 
     * @param {Request} request 
     * @param {Function} callback 
     */
    get_booking_user: function (request, callback) {
        var sql = "SELECT o.user_id, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',u.profile_image) as profile_image,u.username FROM suasdb.tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id LEFT JOIN tbl_user AS u ON o.user_id = u.id WHERE od.item_id = '" + request.id + "' AND od.item_type = '" + request.type + "' AND od.is_deleted = '0' AND o.is_deleted = '0'";
        con.query(sql, function (error, result) {
            if (!error && result.length > 0) {
                console.log(result);
                var sql_admin = "SELECT concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.ADMIN_IMAGE + "',profile_image) as profile_image FROM suasdb.tbl_admin WHERE id = '1'";
                con.query(sql_admin, function (admin_error, admin_result) {
                    if (!admin_error && admin_result.length > 0) {
                        result.splice(0, 0, { "user_id": "0", "profile_image": admin_result[0]['profile_image'],"username":"Admin" });
                        // result[result.length - 1] = { "user_id": "0", "profile_image": admin_result[0]['profile_image'] };
                        callback("1", "Booking User List Find Successfully", result);
                    } else {
                        result.splice(0, 0, { "user_id": "0", "profile_image": GLOBALS.S3_BUCKET_ROOT + GLOBALS.ADMIN_IMAGE + "default.png","username":admin_result[0]['name'] });
                        // result[result.length - 1] = { "user_id": "0", "profile_image": GLOBALS.S3_BUCKET_ROOT + GLOBALS.ADMIN_IMAGE + "default.png" };
                        callback("1", "Booking User List Find Successfully", result);
                    }
                })
            } else {
                callback("0", t("rest_keywords_something_went_wrong"), null);
            }
        })
    }
}

module.exports = User;