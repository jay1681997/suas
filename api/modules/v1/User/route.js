var express = require('express');
var user_model = require('./user_model');
var common = require('../../../config/common');
var auth_model = require('../Auth/auth_model');
var GLOBALS = require('../../../config/constants');
const {
    t
} = require('localizify');
var router = express.Router();

/* 
** 15-Feb-2022
** API to get user subscription list
*/
router.post("/get_activesubscriptions_list", function (req, res) {

    user_model.get_activesubscriptions_list(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });

});

/* 
** 10-Feb-2022
** API to get sub-profile or student list
*/
router.post("/get_subprofile_list", function (req, res) {
    user_model.get_subprofile_list(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

/* 
** 10-Feb-2022
** API to add or upload user speech
*/
router.post("/add_speech", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            student_id: '',
            title: 'required',
            description: 'required',
            video: 'required',
            duration: '',
            thumb_image: '',
            uploading_type: 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'title': t('rest_keywords_title'),
            'description': t('rest_keywords_description'),
            'video': t('rest_keywords_video'),
            'uploading_type': t('rest_keywords_uploading_type'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.add_speech(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 10-Feb-2022
** API to get user speech list
*/
router.post("/get_speech_list", function (req, res) {
    common.decryption(req.body, function (request) {
        user_model.get_speech_list(req.user_id, request, function (code, msg, respData) {
            common.sendresponse(res, code, msg, respData);
        });
    })
});

/* 
** 10-Feb-2022
** API to get user speech details
*/
router.post("/get_speech_details", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            speech_id: 'required',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'speech_id': t('rest_keywords_speech_id'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            var speech_id = request.speech_id;
            user_model.get_speech_details(req.user_id, speech_id, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 10-Feb-2022
** API to get user speech details
*/
router.post("/delete_speech", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            speech_id: 'required',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'speech_id': t('rest_keywords_speech_id'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.delete_speech(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 10-Feb-2022
** API to add or upload user speech
*/
router.post("/submit_for_evaluation", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            speech_id: 'required',

        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'speech_id': t('rest_keywords_speech_id'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.submit_for_evaluation(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 10-Feb-2022
** API to add gift enrolled class
*/
router.post("/add_gift_enrolled_class", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            class_id: 'required',
            name: 'required',
            email: 'required',
            country_code: 'required',
            phone: 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'name': t('rest_keywords_name'),
            'email': t('rest_keywords_email'),
            'country_code': t('rest_keywords_country_code'),
            'phone': t('rest_keywords_phone'),
            'class_id': t('rest_keywords_class_id')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.add_gift_enrolled_class(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to add items into cart
*/
router.post("/addtocart", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            type: 'required|in:subscription,speech,contest,class,video,promocode',
            item_id: 'required|integer',
            student_id: '',
        }
        const messages = {
            'required': t('required'),
            'integer': t('integer'),
            'in': t('in'),
        }
        var keywords = {
            'item_id': t('rest_keywords_item_id'),
            'type': t('rest_keywords_type')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.addtocart(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to remove item from cart
*/
router.post("/remove_fromcart", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            // item_id    : 'required|integer',
            item_id: '',
        }
        const messages = {
            'required': t('required'),
            'integer': t('integer')
        }
        var keywords = {
            'item_id': t('rest_keywords_item_id')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.remove_fromcart(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to add item into wishlist
*/
router.post("/addtowishlist", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            item_id: 'required|integer',
            type: 'required|in:subscription,speech,contest,class,video,promocode',
        }
        const messages = {
            'required': t('required'),
            'integer': t('integer')
        }
        var keywords = {
            'item_id': t('rest_keywords_item_id'),
            'type': t('rest_keywords_type')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.addtowishlist(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to add item into wishlist
*/
router.post("/get_wishlist", function (req, res) {
    user_model.get_wishlist(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

/*
** 10-Feb-2022
** API to add item into wishlist
*/
router.post("/remove_fromwishlist", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            wish_itemid: 'required|integer',
        }
        const messages = {
            'required': t('required'),
            'integer': t('integer')
        }
        var keywords = {
            'wish_itemid': t('rest_keywords_wish_itemid')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.remove_fromwishlist(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to get cart data
*/
router.post("/fetchcartdata", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            student_id: '',
        }
        const messages = {
            'required': t('required'),
        }
        var keywords = {
            'student_id': t('rest_keywords_student_id'),
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.fetchUsercartList(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to apply promocode for user side
*/
router.post("/apply_promocode", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            promocode: 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'promocode': t('rest_keywords_promocode')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.apply_promocode(req.user_id, request, function (code, message, data) {
                common.sendresponse(res, code, message, data);
            });
        }
    });
});

/*
** 10-Feb-2022
** API to place order for customer
*/
router.post("/placeorder", function (req, res) {

    common.decryption(req.body, function (request) {

        var request = request
        var rules = {
            // item_type           : 'required|in:subscription,speech,contest,class,video,promocode',
            // item_id             : 'required',
            subtotal: 'required',
            discount: '',
            tax: 'required',
            totalamount: 'required',
            promocode: '',
            payment_mode: 'required|in:UPI,APPLE,Card',
            card_id: 'required_if:payment_mode,Card',
            student_id: '',
            suborder: 'required',
            total_points: '',
            redeem_points: '',
        }
        const messages = {
            'required_if': t('required_if'),
            'required': t('required'),
            'integer': t('integer'),
            // 'in'                : t('in')
        }
        var keywords = {
            'card_id': t('rest_keywords_card_id'),
            'subtotal': t('rest_keywords_subtotal'),
            'discount': t('rest_keywords_discount'),
            'tax': t('rest_keywords_tax'),
            // 'item_id'           : t('rest_keywords_item_id'),
            'totalamount': t('rest_keywords_totalamount'),
            'promocode': t('rest_keywords_promocode'),
            // 'item_type'         : t('rest_keywords_item_type'),
            'payment_mode': t('rest_keywords_payment_mode'),
            'suborder': t('rest_keywords_suborder'),
        };

        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.placeorder(req.user_id, request, function (code, message, data) {
                common.sendresponse(res, code, message, data);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to get user contest list
*/
router.post("/get_user_contest_list", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {

            student_id: ''
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {

        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_contest_list(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to get user contest list
*/
router.post("/get_user_class_list", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {

            student_id: ''
        }
        const messages = {

        }
        var keywords = {

        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_class_list(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

router.post("/get_user_class_list_details", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {

            class_id: 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'class_id': t('rest_keywords_class_id')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_class_list(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to get user contest details
*/
router.post("/get_user_contest_details", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            contest_id: 'required',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'contest_id': t('rest_keywords_contest_id'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_contest_details(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to upload video for contest
*/
router.post("/upload_contestvideo", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            video: 'required',
            contest_id: 'required',
            thumb_image: 'required',

        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'video': t('rest_keywords_video'),
            'contest_id': t('rest_keywords_contest_id'),
            'thumb_image': t('rest_keywords_thumb_image'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.upload_contestvideo(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to get user evaluation details
*/
router.post("/get_user_evaluation", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            student_id: '',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_evaluation(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

router.post("/get_user_evaluation_details", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            evaluation_id: 'required',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            evaluation_id: t('rest_keywords_evaluation_id')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_evaluation(req.user_id, request, function (code, msg, respData) {

                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/*
** 15-Feb-2022
** API to get notification list
*/
router.post("/notification_list", function (req, res) {

    auth_model.get_userdetails(req.user_id, function (response) {
        if (response == null) {
            common.sendresponse(res, '0', t('rest_keywords_userdetailsnot_found'), null);
        } else {
            var notify = {
                user_id: req.user_id,
                user_type: 'U',
            };
            common.notification_list(notify, function (notification, error) {
                if (response == null) {
                    common.sendresponse(res, '2', t('rest_keywords_youhavenonewnotify'), null);
                } else {
                    common.sendresponse(res, '1', t('rest_keywords_notificationlistfound_success'), notification);
                }
            });
        }
    });

});

/*
** 15-Feb-2022
** API to clear notification single and multiple
*/
router.post("/clear_notification", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            notification_id: '',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'notification_id': t('rest_keywords_notification_id')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            auth_model.get_userdetails(req.user_id, function (response) {
                if (response == null) {
                    common.sendresponse(res, '0', t('rest_keywords_userdetailsnot_found'), null);
                } else {
                    user_model.clear_notification(req.user_id, request, function (code, msg, respData) {
                        common.sendresponse(res, code, msg, respData);
                    });
                }
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to get user contest list
*/
router.post("/get_winnercontest_list", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {

        }
        const messages = {

        }
        var keywords = {

        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_winnercontest_list(req.user_id, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});


/* 
** 01-June-2022
** API to set manual evaluation details
*/
router.post("/set_manual_evaluation", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            evaluation_id: 'required',
            name: 'required',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'evaluation_id': t('rest_keywords_evaluation_id'),
            'name': t('rest_keywords_name'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.set_manual_evaluation(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 15-Feb-2022
** API to generate token through agora api for online classes details
*/
router.post("/generate_token_agora", function (req, res) {

    common.decryption(req.body, function (request) {
        var request = request
        req.user_id = request.user_id;

        var rules = {
            class_id: 'required',
            type: 'required|in:class,contest'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'class_id': t('rest_keywords_class_id'),
            'type': t('rest_keywords_type'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            // console.log(req.user_id);
            // request.user_id = req.user_id;
            user_model.check_userChannel(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });

});

router.post("/generate_token_agora_RTM", function (req, res) {
    common.decryption(req.body, function (request) {
        request.user_id = req.user_id;
        user_model.generate_token_agora_RTM(request, function (code, msg, respData) {
            common.sendresponse(res, code, msg, respData);
        });
    });

});


/* 
** 01-June-2022
** API to set manual evaluation details
*/
router.post("/sendPushNotification", function (req, res) {

    common.decryption(req.body, function (request) {

        var request = request
        common.sendPushNotification(request, function (code, msg, respData) {
            common.sendresponse(res, code, msg, respData);
        });
    });
});

/* 
** 17-July-2022
** API to get user gifted class details
*/
router.post("/get_user_giftClassDetails", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            gift_type: 'required|in:giftto,received',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'gift_type': t('rest_keywords_gift_type'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.get_user_giftClassDetails(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 17-July-2022
** API to get user purchase video
*/
router.post("/get_purchase_video", function (req, res) {
    user_model.get_purchase_video(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});


/* 
** 17-July-2022
** API to update notification setting
*/
router.post("/notification_serviceupdate", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            push_notification: 'required|in:yes,no',
            email: 'required|in:yes,no',
            sms: 'required|in:yes,no',
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            'push_notification': t('rest_keywords_push_notification'),
            'email': t('rest_keywords_email'),
            'sms': t('rest_keywords_sms'),
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.notification_serviceupdate(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/**
 * 
 * */
router.get("/notification_setting", function (req, res) {
    user_model.notification_setting(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
})

/* 
** 29-July-2022
** API to get reward points
*/
router.post("/get_reward_points", function (req, res) {
    user_model.get_reward_points(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});


/* 
** 29-July-2022
** API to get reward points history
*/
router.post("/get_reward_points_history", function (req, res) {
    user_model.get_reward_points_history(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

/* 
** 01-August-2022
** API to get home page data
*/
router.post("/get_homepage_details", function (req, res) {
    user_model.get_homepage_details(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

/* 
** 01-August-2022
** API to get notification count
*/
router.post("/get_notification_count", function (req, res) {
    user_model.get_notification_count(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});


router.post("/createPaymentIntent", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            totalamount: 'required',
            payment_mode: 'required|in:UPI,APPLE,Card',
            card_id: 'required_if:payment_mode,Card',
            redeem_points: '',
        }
        const messages = {
            'required_if': t('required_if'),
            'required': t('required'),
            'integer': t('integer'),
            'in': t('in')
        }
        var keywords = {
            'card_id': t('rest_keywords_card_id'),
            'totalamount': t('rest_keywords_totalamount'),
            'promocode': t('rest_keywords_promocode'),
            'item_type': t('rest_keywords_item_type'),
            'payment_mode': t('rest_keywords_payment_mode'),

        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            user_model.createPaymentIntent(req.user_id, request, function (code, message, data) {
                common.sendresponse(res, code, message, data);
            });
        }
    });
});

router.post("/get_total_user_video", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request

        user_model.get_total_user_video(req.user_id, request, function (code, msg, respData) {
            common.sendresponse(res, code, msg, respData);
        });
    })
});

router.post("/check_user_block_unblock_status", function (req, res) {
    common.decryption(req.body, function (request) {
        var rules = {
            "id": 'required',
            "type": 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            "id": t("rest_keyword_id"),
            "type": t("rest_keywords_type")
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            request.user_id = req.user_id;
            user_model.check_user_block_unblock_status(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            })
        }
    })
})

router.post("/get_booking_user", function (req, res) {
    common.decryption(req.body, function (request) {
        var rules = {
            "id": 'required',
            "type": 'required'
        }
        const messages = {
            'required': t('required')
        }
        var keywords = {
            "id": t("rest_keyword_id"),
            "type": t("rest_keywords_type")
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            request.user_id = req.user_id;
            user_model.get_booking_user(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            })
        }
    })
})
module.exports = router;