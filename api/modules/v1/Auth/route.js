var express       = require('express');
var auth_model    = require('./auth_model');
var user_model = require('../User/user_model');
var common        = require('../../../config/common');
var GLOBALS       = require('../../../config/constants');
var template_data = require('../../../config/template');
var cryptoLib     = require('cryptlib');
var shaKey        = cryptoLib.getHashSha256(GLOBALS.KEY, 32);
// Language file load
var datetime         = require('node-datetime');
let now              = new Date();
const { t } = require('localizify');
var router  = express.Router();

/* 
** 08-Feb-2022
** Authentication API's
** Signup API
*/
router.post("/signup", function (req, res) {
    //request method encryption
    common.decryption(req.body, function (request) {

        var request = request
        var rules = {
            firstname           : 'required',
            lastname            : 'required',
            email               : 'required|email',
            country_code        : 'required',
            phone               : 'required',
            dob                 : 'required',
            password            : 'required',
            confirm_password    : 'required',
            country             : 'required',
            profile_image       : '',
            promocode           : '',
            latitude            : '',
            longitude           : '',
            device_type         : 'required|in:A,I',
            device_token        : 'required',
            uuid                : '',
            os_version          : '',
            ip                  : ''
        }
        const messages = {
            'required'          : t('required'),
            'in'                : t('in'),
            'email'             : t('email')
        }
        var keywords = {
            'firstname'         : t('rest_keywords_firstname'),
            'lastname'          : t('rest_keywords_lastname'),
            'email'             : t('rest_keywords_email'),
            'country_code'      : t('rest_keywords_country_code'),
            'phone'             : t('rest_keywords_phone'),
            'dob'               : t('rest_keywords_dob'),
            'confirm_password'  : t('rest_keywords_confirm_password'),
            'country'           : t('rest_keywords_country'),
            'password'          : t('rest_keywords_password'),
            'device_type'       : t('rest_keywords_device_type'),
            'device_token'      : t('rest_keywords_device_token'),
            }
    
        if (common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            auth_model.check_unique(' ', request, function (response, error) {
                if (response == false) {
                    common.sendresponse(res, '0', error, null);
                } else {
                    auth_model.insert_user(request, function (code, msg, respData) {
                        common.sendresponse(res, code, msg, respData);
                    });
                }
            });
        }
    });
});

/* 
** 08-Feb-2022
** Verify OTP API
*/
router.post("/verify_otp",function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            user_id     : 'required',
            otp_code    : 'required',
            device_type         : 'required|in:A,I',
            device_token        : 'required',
            uuid                : '',
            os_version          : '',
            ip                  : ''
        }
        const messages = {
            'required'  : t('required')
        }
        var keywords = {
            'user_id'   : t('rest_keywords_user_id'),
            'otp_code'  : t('rest_keywords_otp_code'),
            'device_type'       : t('rest_keywords_device_type'),
            'device_token'      : t('rest_keywords_device_token'),
         
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            auth_model.verify_otp(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 08-Feb-2022
** Resend OTP API
*/
router.post("/resend_otp",function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            user_id      : 'required',
            country_code : 'required',
            phone        : 'required'
        }
        const messages = {
            'required'   : t('required')
        }
        var keywords = {
            'user_id'     : t('rest_keywords_user_id'),
            'country_code': t('rest_keywords_country_code'),
            'phone'       : t('rest_keywords_phone')
        }
        if (common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            auth_model.resend_otp(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 08-Feb-2022
** Upload Video API
*/
router.post("/upload_video",function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            user_id     : 'required',
            video       : 'required',
        }
        const messages = {
            'required'  : t('required')
        }
        var keywords = {
            'user_id'   : t('rest_keywords_user_id'),
            'video'     : t('rest_keywords_video'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            auth_model.upload_video(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 08-Feb-2022
** Add student Profile API
*/
router.post("/student_profile",function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            action       : 'required|in:1,2,3',
            user_id      : 'required_if:action,1',
            student_id   : 'required_if:action,2,3',
            firstname    : 'required_if:action,1,2',
            lastname     : 'required_if:action,1,2',
            dob          : 'required_if:action,1,2',
            video        : '',
            image        : '',
        }
        const messages = {
            'required'   : t('required'),
            'required_if': t('required_if'),
            'in'         : t('in')
        }
        var keywords = {
            'user_id'    : t('rest_keywords_user_id'),
            'firstname'  : t('rest_keywords_firstname'),
            'lastname'   : t('rest_keywords_lastname'),
            'dob'        : t('rest_keywords_dob'),
            'video'      : t('rest_keywords_video'),
            'action'     : t('rest_keywords_action'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            auth_model.student_profile(request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
** 08-Feb-2022
** login API
*/
router.post("/login", function(req, res){ 
    common.decryption(req.body, function(request){
        var request = request
        console.log(request)
        var rules = {
            country_code    : 'required',
            phone           : 'required',
            password        : 'required',
            latitude        : 'required',
            longitude       : 'required',
            device_type     : 'required|in:A,I',
            device_token    : 'required',
            uuid            : '',
            os_version      : '',
            ip              : ''
        }
        const messages = {
            'required'      : t('required'),
            'in'            : t('in')
        }
        var keywords = {
            'email'         : t('rest_keywords_email'),
            'latitude'      : t('rest_keywords_latitude'),
            'longitude'     : t('rest_keywords_longitude'),
            'device_type'   : t('rest_keywords_device_type'),
            'device_token'  : t('rest_keywords_device_token'),
            'password'      : t('rest_keywords_password'),
        }
        if (common.checkValidationRules(request,res,rules,messages,keywords))
        {
            auth_model.login_details(request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            });    
        }
    });
});

/*
** 08-Feb-2022
** Get user details API
*/
router.post("/get_userdetails", function(req, res){
    auth_model.get_userdetails(req.user_id, function(userProfile, Error){
        if (userProfile != null) {
            common.sendresponse(res, '1', t('rest_keywords_user_data_successfound'), userProfile);
        } else {
            common.sendresponse(res, '0', t('rest_keywords_userdetailsnot_found'), userProfile);
        }
    })       
});

/*
** 08-Feb-2022
** Forgotpassword API
*/
router.post("/forgotpassword", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            type            : 'required|in:email,phone',
            email           : 'required_if:type,email',
            country_code    : 'required_if:type,phone',
            phone           : 'required_if:type,phone',
        }
        const messages = {
            'required'      : t('required'),
            'required_if'   : t('required_if'),
        }
        var keywords = {
            'type'          : t('rest_keywords_type')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            if(request.type == 'email'){
                auth_model.checkEmailExist(request.email, function (userdata, response, callback) {
                    if (userdata == null) {
                        common.sendresponse(res, '0', t('rest_keywords_user_emaildoesnot_exist'), null);
                    } else {
                        var upduser = {
                            forgotpassword_datetime  : require('node-datetime').create().format('Y-m-d H:M:S'),
                            forgotpassword_token     : userdata.id + require('node-datetime').create().format('YmdHMS'),
                        };
                        auth_model.update_user(userdata.id, upduser, function (response, error) {
                            console.log(response);
                            if (error) {
                                common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                            } else {
                                userdata.username = userdata.username;
                                userdata.url = GLOBALS.BASE_URL_WITHOUT_API + "home/forgotpassword/user/" + Buffer.from(userdata.id.toString()).toString('base64');
                                template_data.forgot_password(userdata, function (result) {
                                    var subject = GLOBALS.APP_NAME + ' - Forgot Password';
                                    common.send_email(subject, GLOBALS.EMAIL_ID, userdata.email, result, function (emailSent) {
                                        if (emailSent) {
                                              auth_model.get_userdetails(user_id, function (response, err) {
                                                 common.sendresponse(res, '1', t('rest_keywords_user_forgot_password_success'), response);     
                                              })
                                           
                                        } else {
                                            common.sendresponse(res, '0', t('rest_keywords_user_forgot_password_failed'), null);
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }else{
                auth_model.checkPhoneExist(request, function (userdata) {
                    if (userdata == null) {
                        common.sendresponse(res, '0', t('rest_keywords_user_phone_doesnot_exist'), null);
                    } else {
                        var otp_code = '1234';
                        var phone    = userdata.country_code + userdata.phone;
                        var message  = "Your OTP Code is " + otp_code;
                        // sendSMS
                        common.sendSMS(phone, message, function (sms_send) {
                            if (sms_send) {
                                var upd_params = {
                                    otp_code   : otp_code,
                                    otp_status : 'Pending'
                                }
                                auth_model.update_user(userdata.id, upd_params, function (response, error) {
                                    if (error) {
                                        common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                                    } else {
                                        common.sendresponse(res, '1', t('rest_keywords_otp_send_success'), response);
                                    }
                                });
                            } else {
                                common.sendresponse(res, '0', t('rest_keywords_otp_send_failed'), null);
                            }

                        });

                    }
                });
            }
        }
    });
});

/*
** 08-Feb-2022
** Change password
*/
router.post("/change_password", function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            old_password   : 'required',
            new_password   : 'required'
        }
        const messages = {
            'required'     : t('required')
        }
        var keywords = {
            'old_password' : t('rest_keywords_old_pass'),
            'new_password' : t('rest_keywords_new_pass')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) 
        {   
            auth_model.get_userdetails(req.user_id, function (userdata, error) {
                if (error) {
                    common.sendresponse(res, '0', t('rest_keywords_userdetailsnot_found'), null);
                } else {
                    var current_password = cryptoLib.decrypt(userdata.password, shaKey, GLOBALS.IV);
                    auth_model.change_password(req.user_id, request, current_password, function (code, msg, data) {
                        common.sendresponse(res, code, msg, data);
                    });
                }
            });
        }
    });
});


/*
** 08-Feb-2022
** Create password
*/
router.post("/create_password", function(req,res){
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            country_code        : 'required',
            phone               : 'required',
            password            : 'required',
            confirm_password    : 'required'
        }
        const messages = {
            'required'          : t('required')
        }
        var keywords = {
            'country_code'      : t('rest_keywords_country_code'),
            'phone'             : t('rest_keywords_phone'),
            'password'          : t('rest_keywords_password'),
            'confirm_password'  : t('rest_keywords_confirm_password')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) 
        {   
            auth_model.get_userdatafromphone(request, function (userdata) {
                if (userdata != null) {
                    var current_password = cryptoLib.decrypt(userdata.password, shaKey, GLOBALS.IV);
                    auth_model.create_password(userdata.id, request, current_password, function (code, msg, data) {
                        common.sendresponse(res, code, msg, data);
                    });
                    
                } else {
                    common.sendresponse(res, '0', t('rest_keywords_userdetailsnot_found'), null);
                }
            });
        }
    });
});

/*
** 08-Feb-2022
** Edit/Update Device information
*/
router.post("/updatedevice_info", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            device_type    : 'required|in:A,I',
            device_token   : 'required'
        }
        const messages = {
            'required'     : t('required'),
            'in'           : t('in')
        }
        var keywords = {
            'device_type'  : t('rest_keywords_device_type'),
            'device_token' : t('rest_keywords_device_token')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            common.checkUpdateDeviceInfo(req.user_id, 'U', request, function () {
                common.sendresponse(res, '1', t('rest_keywords_userdevice_infosuccess'), null);
            });
        }
    });
});

/*
** 08-Feb-2022
** Edit/Update latlng
*/
router.post("/updatelatlng", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            latitude    : 'required',
            longitude   : 'required'
        }
        const messages = {
            'required'  : t('required')
        }
        var keywords = {
            'latitude'  : t('rest_keywords_latitude'),
            'longitude' : t('rest_keywords_longitude')
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            var upd_customer = {
                latitude: request.latitude,
                longitude: request.longitude
            };
            auth_model.update_user(req.user_id, upd_customer, function (response, error) {
                if (error) {
                    common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                } else {
                    common.sendresponse(res, '1', t('rest_keywords_locationupdate_success'), response);
                }
            });
        }
    });
});

/*
** 08-Feb-2022
** Logout
*/
router.post("/logout", function (req, res) {
    auth_model.logout(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

/*
** Edit/Update user profile
*/
router.post("/edit_profile", function(req, res){
    common.decryption(req.body, function(request){
        var request = request
        var rules = {  
        }
        const messages = {
        }
        var keywords = {
        }
        console.log(request);
        if (common.checkValidationRules(request, res, rules, messages, keywords))
        {
            auth_model.check_unique(req.user_id, request, function (response, error) {
                if (response == false) {
                    common.sendresponse(res, '0', error, null);
                } else {
                    if(request.profile_image != undefined && request.profile_image != "") {
                        request.profile_image = request.profile_image;
                    }

                    if(request.promocode != undefined && request.promocode != ''){
                        request.promocode = request.promocode;
                    }
                    if(request.firstname != undefined && request.firstname != ''){
                        request.firstname = request.firstname;
                    }
                    if(request.lastname != undefined && request.lastname != ''){
                        request.lastname = request.lastname;
                    }
                    if(request.email != undefined && request.email != ''){
                        request.email = request.email;
                    }
                    if(request.country_code != undefined && request.country_code != ''){
                        request.country_code = request.country_code;
                    }
                    if(request.phone != undefined && request.phone != ''){
                        request.phone = request.phone;
                        // request.otp_code = '1234';
                        // request.otp_status = 'Pending';
                    }
                    if(request.dob != undefined && request.dob != ''){
                        request.dob = request.dob;
                        request.is_dob_edit = '0';
                    }
                    if(request.country != undefined && request.country != ''){
                        request.country = request.country;
                    }
                    if(request.region_id != undefined && request.region_id != ''){
                        request.region_id = request.region_id;
                    }
                    if(request.firstname != undefined && request.firstname != '' || request.lastname != undefined && request.lastname != ''){
                        request.username = request.firstname+" "+request.lastname;
                    }
                    var today = new Date();
                    var birthDate = new Date(request.dob);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    
                    if(age >= '18'){
                       request.age_category = 'Adults';
                    }else if(age >= 5 && age <= 8){
                        request.age_category = 'Primary';
                    }else if(age >= 9 && age <= 12){
                        request.age_category = 'Juniors';
                    }else{
                        request.age_category = 'Teens';
                    }
                    
                    // request.otp_code = '1234';
                    // request.otp_status = 'Pending';
                    auth_model.check_userstatus(req.user_id, function(is_active){
                        if(is_active == '0'){
                            common.sendresponse(res, '0', t('rest_keywords_status_inactive'), null); 
                        }else{
                            console.log(request);
                            auth_model.update_user(req.user_id, request, function(respData){
                                if(respData != null){
                                    common.sendresponse(res, '1', t('rest_keywords_user_edit_profile_success'), respData);
                                }else{
                                    common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), respData);   
                                }
                            }); 
                        }
                    })
                }
            })
        }
    });
});

/*
** contact us API
*/
router.post("/contactus", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            name: 'required',
            email: 'required|email',
            description: 'required'
        }
        const messages = {
            'required': t('required'),
            'email': t('email')
        }
        var keywords = {
            'name': t('rest_keywords_name'),
            'email': t('rest_keywords_email'),
            'description': t('rest_keywords_description'),
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            auth_model.add_contactus(req.user_id, request, function (response) {
                if (response != null) {
                    var subject = GLOBALS.APP_NAME + '  - Contactus inquery';
                    // request.subject = subject;
                    template_data.contactus(request, function (result) {
                        console.log(request.email);
                        common.send_email(subject, request.email, GLOBALS.TO_EMAIL, result, function (emailSent) {
                            console.log(emailSent);
                            user_model.notification_setting(req.user_id, function (code,mess,res_data) {
                                if (code == 1) {
                                    if (res_data['sms'] == 'yes') {
                                        template_data.contactus_user_view(request, function (messages) {
                                            common.send_email(subject, GLOBALS.TO_EMAIL, request.email, messages, function (emailSent1) {
                                                if (emailSent1) {
                                                    common.sendresponse(res, '1', t('rest_keywords_contactus_success'), null);
                                                }
                                                else {
                                                    common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                                                }
                                            });
                                        });
                                    } else {
                                        common.sendresponse(res, '1', t('rest_keywords_contactus_success'), null);
                                    }

                                } else {
                                    common.sendresponse(res, '1', t('rest_keywords_contactus_success'), null);
                                }
                            })


                        });
                    });
                }
                else {
                    common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                }
            });
        }
    });
});
router.post("/contactus_old", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            name           : 'required',
            email          : 'required|email',
            description    : 'required'
        }
        const messages = {
            'required'     : t('required'),
            'email'        : t('email')
        }
        var keywords = {
            'name'         : t('rest_keywords_name'),
            'email'        : t('rest_keywords_email'),
            'description'  : t('rest_keywords_description'),
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            auth_model.add_contactus(req.user_id, request, function (response) {
                if (response != null) {
                    var subject = GLOBALS.APP_NAME + '  - Contactus inquery';
                    // request.subject = subject;
                    template_data.contactus(request, function (result) {
                       console.log(request.email);
                        common.send_email(subject, request.email,GLOBALS.TO_EMAIL, result, function (emailSent) {
                            console.log(emailSent);
                            if (emailSent) {
                                common.sendresponse(res, '1', t('rest_keywords_contactus_success'), null);
                            }
                            else {
                                common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                            }
                        });
                    });
                }
                else {
                    common.sendresponse(res, '0', t('rest_keywords_something_went_wrong'), null);
                }
            });
        }
    });
});

/*
** Updrade student profile into main profile
*/
router.post("/upgrade_tomainprofile", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            student_id      : 'required',
            email           : 'required',
            country_code    : 'required',
            phone           : 'required',
            country         : 'required',
            password        : 'required',
        }
        const messages = {
            'required'      : t('required')
        }
        var keywords = {
            'email'         : t('rest_keywords_email'),
            'country_code'  : t('rest_keywords_country_code'),
            'phone'         : t('rest_keywords_phone'),
            'country'       : t('rest_keywords_country'),
            'password'      : t('rest_keywords_password'),
            'student_id'    : t('rest_keywords_student_id'),
        };
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            auth_model.check_unique(req.user_id, request, function (response, error) {
                if (response == false) {
                    common.sendresponse(res, '0', error, null);
                } else {
                    auth_model.upgrade_tomainprofile(req.user_id, request, function (code, msg, respData) {
                        common.sendresponse(res, code, msg, respData);
                    });
                }
            });
        }
    });
});


router.post("/delete_user", function (req, res) {
    auth_model.delete_user(req.user_id, function (code, msg, respData) {
        common.sendresponse(res, code, msg, respData);
    });
});

router.post("/home_video_list",function(req,res){
    auth_model.home_video_list(function(code,msg,respData){
       common.sendresponse(res, code, msg, respData); 
    })
})

router.post("/region_list",function(req,res){
    common.decryption(req.body, function (request) {
      auth_model.region_list(request, function (code, msg, respData) {
                        common.sendresponse(res, code, msg, respData);
                    });  
    })
})
module.exports = router;