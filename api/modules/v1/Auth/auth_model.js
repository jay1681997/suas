var con = require('../../../config/database');
var GLOBALS = require('../../../config/constants');
var common = require('../../../config/common');
var cryptoLib = require('cryptlib');
var asyncLoop = require('node-async-loop');
var shaKey = cryptoLib.getHashSha256(GLOBALS.KEY, 32);
var _randomhash = require('crypto-toolkit').RandomHash('base64-urlsafe');
var datetime = require('node-datetime');
let now = new Date();
// Language file load
const { t } = require('localizify');
const { request } = require('express');
var template_data = require('../../../config/template');

var Authentication = {

    /* 
    ** to check unique email or phone
    */
    check_unique: function (user_id, request, callback) {
        Authentication.checkUniqueEmail(user_id, request, function (response, error) {
            if (response) {
                Authentication.checkUniquePhone(user_id, request, function (isUnique, errors) {
                    if (isUnique) {
                        callback(true);
                    } else {
                        callback(isUnique, errors);
                    }
                });
            } else {
                callback(response, error);
            }
        });
    },

    /*
    ** Function to check email uniqueness
    */
    checkUniqueEmail: function (user_id, request, callback) {

        if (user_id != undefined && user_id != '') {
            var uniqueEmail = "SELECT * FROM tbl_user WHERE email = '" + request.email + "' AND is_deleted='0' AND id != '" + user_id + "' ";
        } else {
            var uniqueEmail = "SELECT * FROM tbl_user WHERE email = '" + request.email + "' AND is_deleted='0' ";
        }
        var Emailquery = con.query(uniqueEmail, function (error, result, fields) {
            if (!error && result[0] != undefined) {
                callback(false, t('rest_keywords_user_exist_email'));
            } else {
                callback(true);
            }
        });
    },

    /*
    ** Function to check phone uniqueness
    */
    checkUniquePhone: function (user_id, request, callback) {
        if (user_id != undefined && user_id != '') {
            var uniquePhone = "SELECT * FROM tbl_user WHERE phone = '" + request.phone + "' AND is_deleted='0' AND id != '" + user_id + "' ";
        } else {
            var uniquePhone = "SELECT * FROM tbl_user WHERE phone = '" + request.phone + "' AND is_deleted='0' ";
        }
        // Check database for this phone number registered
        var Phonequery = con.query(uniquePhone, function (error, result, fields) {
            if (!error && result[0] != undefined) {
                callback(false, t('rest_keywords_user_exist_phone'));
            } else {
                callback(true);
            }
        });
    },

    get_userdatafromphone: function (request, callback) {

        // Check database for this phone number registered
        var Phonequery = con.query("SELECT * FROM tbl_user WHERE (phone = '" + request.phone + "' AND country_code = '" + request.country_code + "') AND is_deleted='0'", function (error, result, fields) {
            console.log(this.sql)
            if (!error && result[0] != undefined) {
                Authentication.get_userdetails(result[0].id, function (userdata, err) {
                    if (userdata != null) {
                        callback(userdata)
                    } else {
                        callback(null)
                    }
                })
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Function to check user status
    */
    check_userstatus: function (user_id, callback) {
        var Phonequery = con.query("SELECT * FROM tbl_user WHERE id = '" + user_id + "' AND is_deleted='0' AND status ='Active' ", function (error, result, fields) {
            console.log(this.sql);
            if (!error && result[0] != undefined) {
                callback("1");
            } else {
                callback("0");
            }
        });
    },

    /*
    ** Function to signup user
    */
    insert_user: function (request, callback) {

        if (request.password != request.confirm_password) {
            callback('0', t('rest_keywords_password_mismatch'));
        } else {
            var otp = Math.floor(1000 + Math.random() * 9000);
            //var otp = '1234';
            var customer = {
                firstname: request.firstname,
                lastname: request.lastname,
                username: request.firstname + " " + request.lastname,
                country_code: request.country_code,
                phone: request.phone,
                email: request.email,
                country: request.country,
                dob: request.dob,
                promocode: (request.promocode != undefined && request.promocode != '') ? request.promocode : '',
                profile_image: (request.profile_image != undefined && request.profile_image != '') ? request.profile_image : 'default.png',
                latitude: (request.latitude != undefined && request.latitude != '') ? request.latitude : '0.0',
                longitude: (request.longitude != undefined && request.longitude != '') ? request.longitude : '0.0',
                otp_code: otp,
                password: cryptoLib.encrypt(request.password, shaKey, GLOBALS.IV),
                region_id: (request.region_id != undefined && request.region_id != '') ? request.region_id : '',
            };

            var today = new Date();
            var birthDate = new Date(request.dob);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            if (age >= '18') {
                customer.age_category = 'Adults';
            } else if (age >= 5 && age <= 8) {
                customer.age_category = 'Primary';
            } else if (age >= 9 && age <= 12) {
                customer.age_category = 'Juniors';
            } else {
                customer.age_category = 'Teens';
            }
            customer.age = age;

            var template = "Thanks for registering at Stand Up and Speak.  Your OTP code is " + otp;
            var phone = request.country_code + request.phone;

            common.sendSMS(phone, template, function (code) {
                // console.log(respon);
                if (code == '1') {
                    var addCustomer = con.query('INSERT INTO tbl_user SET ?', customer, function (err, result, fields) {
                        if (!err) {
                            common.checkUpdateDeviceInfo(result.insertId, 'U', request, function () {
                                template_data.registeruser(customer, function (template_mesage) {
                                    console.log(template_mesage);
                                    common.send_email('Register New User', GLOBALS.EMAIL_ID, GLOBALS.EMAIL_ID, template_mesage, function (send_code) {
                                        Authentication.get_userdetails(result.insertId, function (userdata, err) {
                                            common.generateSessionCode(userdata.id, 'U', function (Token) {
                                                userdata.token = Token;
                                                callback('1', t('rest_keywords_user_signup_success'), userdata);
                                            });
                                        });
                                    })
                                })
                            });
                        } else {
                            callback('0', t('rest_keywords_user_signup_failed'), null);
                        }
                    });
                }
                else {
                    callback('0', t('rest_keywords_sms_send_failed'));
                }
            });//sendSMS function end
        }
    },

    /*
    ** Function to get user details
    */
    get_userdetails: function (user_id, callback) {
        var userQuery = con.query("SELECT u.*,(SELECT c.id FROM suasdb.tbl_country AS c join tbl_user AS tu ON c.name = tu.country where tu.id = u.id limit 0,1) AS country_id,(SELECT ta.region FROM suasdb.tbl_tax_details AS ta JOIN tbl_user AS tu ON ta.id = tu.region_id WHERE tu.id = u.id limit 0,1) AS region,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',u.profile_image) as profile_image,IFNULL(ut.device_token,'') as device_token,IFNULL(ut.device_type,'') as device_type,IFNULL(ut.token,'') as token FROM tbl_user u LEFT JOIN tbl_user_deviceinfo as ut ON u.id = ut.user_id AND user_type='U' WHERE u.id = '" + user_id + "' AND ut.user_type = 'U'  AND u.is_deleted = '0'", function (err, result, fields) {
            console.log(this.sql);
            console.log("GGGGGGGGGGG");
            console.log(err);
            console.log(result);
            console.log(user_id);
            if (!err && result.length > 0) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Function to check email exist or not
    */
    checkEmailExist: function (email, callback) {

        var query = con.query("SELECT * FROM tbl_user WHERE email = '" + email + "' AND is_deleted='0' ", function (err, result, fields) {

            if (!err && result[0] != undefined) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Function to check phone exist or not
    */
    checkPhoneExist: function (request, callback) {

        var query = con.query("SELECT * FROM tbl_user WHERE (country_code = '" + request.country_code + "' AND phone='" + request.phone + "') AND is_deleted='0' ", function (err, result, fields) {
            console.log(this.sql)
            if (!err) {
                if (result[0] != undefined) {
                    callback(result[0]);
                } else {
                    callback(null);
                }

            } else {
                callback(null);
            }
        });
    },

    /*
    ** Function to verify OTP
    */
    verify_otp: function (request, callback) {
        var where = "id = '" + request.user_id + "' AND otp_code = '" + request.otp_code + "' ";
        var query = con.query("SELECT * FROM tbl_user u where " + where + " ", function (err, result, fields) {
            console.log(this.sql);
            if (!err && result[0] != undefined) {
                var verify_user = {
                    otp_status: 'Verify',
                    login_status: "Online",
                    last_login: require('node-datetime').create().format('Y-m-d H:M:S'),
                    latitude: request.latitude,
                    longitude: request.longitude
                }
                var query = con.query("UPDATE tbl_user SET ? WHERE id = '" + result[0].id + "' ", verify_user, function (errs, reslt, fields) {
                    if (result[0].profile_video == '') {
                        common.checkUpdateDeviceInfo(result[0].id, 'U', request, function () {
                              common.generateSessionCode(result[0].id, 'U', function (Token) {
                                Authentication.get_userdetails(result[0].id, function (response, err) {
                                     callback('12', t("rest_keywrods_upload_video"), response);
                                });
                        // callback("12", t("rest_keywrods_upload_video"), result[0])
                    })
                    })
                    } else {
                        common.checkUpdateDeviceInfo(result[0].id, 'U', request, function () {
                        // Authentication.update_user(result[0].id, upd_request, function (userdata, error) {
                            common.generateSessionCode(result[0].id, 'U', function (Token) {
                                // userdata.token = Token;
                                Authentication.get_userdetails(result[0].id, function (response, err) {
                                     callback('1', t("rest_keywords_otp_success"), response);
                                });
                              
                            });
                        
                    })
                    }

                });
            } else {
                callback('0', t('rest_keywords_otp_invalid'), null);
            }
        });
    },

    /*
    ** Function to Resend OTP
    */
    resend_otp_new: function (request, callback) {

        var query = con.query("SELECT * FROM tbl_user where id = '" + request.user_id + "' ", function (err, result, fields) {
            console.log(this.sql);
            if (!err && result[0] != undefined) {
                var otp_update = {
                    otp_status: 'Pending',
                    country_code: request.country_code,
                    phone: request.phone,
                    otp_code: '1212',
                }
                var query = con.query("UPDATE tbl_user SET ? WHERE id = '" + result[0].id + "' ", otp_update, function (errs, reslt, fields) {
                    console.log(this.sql);
                    if (!errs) {
                        Authentication.get_userdetails(result[0].id, function (response, err) {
                            callback('1', t('rest_keywords_resendotp_success'), response);
                        });
                    }
                    else {
                        callback('0', t('rest_keywords_something_went_wrong'), null);
                    }

                });
            } else {
                callback('2', t('rest_keywords_no_data_found'), null);
            }
        });
    },
    resend_otp: function (request, callback) {
        var query = con.query("SELECT * FROM tbl_user where id = '" + request.user_id + "' ", function (err, result, fields) {
            console.log(this.sql);
            if (!err && result[0] != undefined) {
                var otp = Math.floor(1000 + Math.random() * 9000);
                var otp_update = {
                    otp_status: 'Pending',
                    country_code: request.country_code,
                    phone: request.phone,
                    otp_code: otp,
                }
                // var otp = 123;
                var template = "Thanks for registering at Stand Up and Speak.  Your OTP code is " + otp;
                var phone = request.country_code + request.phone;
                // var phone = "+14169703336";
                console.log(phone);
                // return false;
                common.sendSMS(phone, template, function (code) {
                    console.log(code);
                    if (code == '1') {
                        var query = con.query("UPDATE tbl_user SET ? WHERE id = '" + result[0].id + "' ", otp_update, function (errs, reslt, fields) {
                            console.log(this.sql);
                            if (!errs) {
                                Authentication.get_userdetails(result[0].id, function (response, err) {
                                    callback('1', t('rest_keywords_resendotp_success'), response);
                                });
                            }
                            else {
                                callback('0', t('rest_keywords_something_went_wrong'), null);
                            }

                        });
                    } else {
                        callback('0', t('rest_keywords_sms_send_failed'));
                    }
                });


            } else {
                callback('2', t('rest_keywords_no_data_found'), null);
            }
        });
    },

    /*
    ** Function to Upload Video
    */
    upload_video: function (request, callback) {
        var param = {
            profile_video: request.video
        };

        var query = con.query("UPDATE tbl_user SET ? WHERE id = '" + request.user_id + "' ", param, function (err, result, fields) {
            console.log(this.sql);
            if (!err) {
                  Authentication.get_userdetails(request.user_id, function (userProfile, error) {
                callback('1', t('rest_keywords_video_upload_success'), userProfile);
            })
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    /*
    ** Function to get student details
    */
    get_studentdetails: function (student_id, callback) {
        var userQuery = con.query("SELECT u.*,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.STUDENT_IMAGE + "','',u.profile_image) as profile_image FROM tbl_student u WHERE u.id = '" + student_id + "' AND u.is_deleted = '0'", function (err, result, fields) {
            console.log(this.sql);
            if (!err && result.length > 0) {
                callback(result[0]);
            } else {
                callback(null);
            }
        });
    },

    /*
    ** Function to Upload Video
    */
    student_profile: function (request, callback) {
        if (request.action == '1') {
            con.query("SELECT * FROM tbl_student WHERE user_id = '" + request.user_id + "' AND is_deleted='0'", function (error, results, field) {
                if (!error && results.length < 5) {
                    var param = {
                        user_id: request.user_id,
                        firstname: request.firstname,
                        lastname: request.lastname,
                        username: request.firstname + " " + request.lastname,
                        dob: request.dob,
                        profile_image: (request.image != undefined && request.image != '') ? request.image : 'default.png',
                        profile_video: (request.video != undefined && request.video != '') ? request.video : '',
                        is_dob_edit: '0'
                    };

                    var today = new Date();
                    var birthDate = new Date(request.dob);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }

                    if (age >= '18') {
                        param.age_category = 'Adults';
                    } else if (age >= 5 && age <= 8) {
                        param.age_category = 'Primary';
                    } else if (age >= 9 && age <= 12) {
                        param.age_category = 'Juniors';
                    } else {
                        param.age_category = 'Teens';
                    }
                    param.age = age;

                    var query = con.query("INSERT INTO tbl_student SET ?", param, function (err, result, fields) {
                        console.log(result);
                        if (!err) {
                            Authentication.get_studentdetails(result.insertId, function (response, err) {
                                callback('1', t('rest_keywords_student_added_success'), response);
                            });
                        } else {
                            callback('0', t('rest_keywords_student_added_failed'), null);
                        }
                    });
                } else {
                    callback('0', t('rest_keywords_maximum_student_limit_reached'), null);
                }
            });
        } else if (request.action == '2') {
            con.query("SELECT * FROM tbl_student WHERE id = '" + request.student_id + "' AND is_deleted='0'", function (error, results, field) {
                if (!error && results[0] != undefined) {
                    var param = {
                        firstname: request.firstname,
                        lastname: request.lastname,
                        username: request.firstname + " " + request.lastname,
                        dob: request.dob,
                        profile_image: (request.image != undefined && request.image != '') ? request.image : results[0].profile_image,
                        profile_video: (request.video != undefined && request.video != '') ? request.video : results[0].profile_video,
                        is_dob_edit: '0'
                    };

                    var today = new Date();
                    var birthDate = new Date(request.dob);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }

                    if (age >= '18') {
                        param.age_category = 'Adults';
                    } else if (age >= 5 && age <= 8) {
                        param.age_category = 'Primary';
                    } else if (age >= 9 && age <= 12) {
                        param.age_category = 'Juniors';
                    } else {
                        param.age_category = 'Teens';
                    }
                    param.age = age;

                    var query = con.query("UPDATE tbl_student SET ? WHERE id = '" + request.student_id + "'", param, function (err, result, fields) {
                        if (!err) {
                            Authentication.get_studentdetails(request.student_id, function (response, err) {
                                callback('1', t('rest_keywords_student_update_success'), response);
                            });
                        } else {
                            callback('0', t('rest_keywords_student_update_failed'), null);
                        }
                    });
                } else {
                    callback('2', t('rest_keywords_student_details_not_found'), null);
                }
            });
        } else {
            var query = con.query("DELETE FROM tbl_student WHERE id = '" + request.student_id + "'", function (err, result, fields) {
                if (!err) {
                    callback('1', t('rest_keywords_student_delete_success'), null);
                } else {
                    callback('0', t('rest_keywords_student_delete_failed'), null);
                }
            });
        }
    },

    /*
    ** Function to Login
    */
    login_details: function (request, callback) {
        var query = con.query("SELECT * FROM tbl_user where (country_code='" + request.country_code + "' and phone='" + request.phone + "') AND is_deleted='0' ", function (err, result, fields) {
            if (!err && result[0] != undefined) {
                Authentication.get_userdetails(result[0].id, function (userProfile, error) {
                    console.log("------=======+++++");
                    console.log(userProfile);
                    console.log("------=======+++++");
                    console.log(result);
                    console.log("------=======+++++");
                    if (result[0].status == 'Inactive') {
                        callback('0', t('rest_keywords_inactive_accountby_admin'), null);
                    } else if (result[0].otp_status == 'Pending') {
                        var otp = Math.floor(1000 + Math.random() * 9000);
                        var template = "Thanks for registering at Stand Up and Speak.  Your OTP code is " + otp;
                        var phone = request.country_code + request.phone;
                        common.sendSMS(phone, template, function (code) {
                            if (code == '1') {
                                var otp_update = {
                                    otp_status: 'Pending',
                                    country_code: request.country_code,
                                    phone: request.phone,
                                    otp_code: otp,
                                }
                                var query = con.query("UPDATE tbl_user SET ? WHERE id = '" + result[0].id + "' ", otp_update, function (errs, reslt, fields) {
                                     common.checkUpdateDeviceInfo(result[0].id, 'U', request, function () {
                                        var upd_request = {
                                login_status: "Online",
                                last_login: require('node-datetime').create().format('Y-m-d H:M:S'),
                                latitude: request.latitude,
                                longitude: request.longitude
                            }
                                Authentication.update_user(result[0].id, upd_request, function (userdata, error) {
                                    if (!errs) {
                                         Authentication.get_userdetails(result[0]['id'], function (response, err) {
                                            console.log(result[0]['id']);
                                            console.log(response);
                                        callback('4', t("rest_keywords_login_verify"), response);
                                      })
                                    } else {
                                        callback('0', t('rest_keywords_sms_send_failed'),null);
                                    }
                                })
                            })
                                })
                                //callback('4', t("rest_keywords_login_verify"),result[0]);
                            } else {
                                callback('0', t('rest_keywords_sms_send_failed'),null);
                            }
                        })

                    } else {
                        var password = cryptoLib.decrypt(result[0].password, shaKey, GLOBALS.IV);
                        console.log(password);
                        console.log(request.password);
                        if (password !== request.password) {
                            callback('0', t('rest_keywords_invalid_password'), null);
                        } else {
                            var upd_request = {
                                login_status: "Online",
                                last_login: require('node-datetime').create().format('Y-m-d H:M:S'),
                                latitude: request.latitude,
                                longitude: request.longitude
                            }
                            // update device information of user
                            common.checkUpdateDeviceInfo(result[0].id, 'U', request, function () {
                                Authentication.update_user(result[0].id, upd_request, function (userdata, error) {
                                    common.generateSessionCode(result[0].id, 'U', function (Token) {
                                        userdata.token = Token;
                                        if (result[0].profile_video == '') {
                                            callback('12', t('rest_keywrods_upload_video'), userdata);
                                        } else {
                                            callback('1', t("rest_keywords_user_login_success"), userdata);
                                        }

                                    });
                                });
                            });
                        }
                    }
                });
            } else {
                callback('0', t('rest_keywords_invalid_phone'), null);
            }
        });
    },

    /*
    ** Edit / Update user details
    */
    update_user: function (user_id, upd_params, callback) {
        console.log(upd_params)
        var query = con.query("UPDATE tbl_user SET ? WHERE id = ? ", [upd_params, user_id], function (err, result, fields) {
            console.log(this.sql)
            if (!err) {
                Authentication.get_userdetails(user_id, function (response, err) {
                    console.log("UPDATE");
                    console.log(response);
                    callback(response);
                });
            } else {
                callback(null, err);
            }
        });
    },

    /*
    ** Function to changed password
    */
    change_password: function (user_id, request, currentpass, callback) {

        if (currentpass != request.old_password) {
            callback('0', t('rest_keywords_user_old_password_incorrect'), null);
        } else if (currentpass == request.new_password) {
            callback('0', t('rest_keywords_user_newold_password_similar'), null);
        } else {
            var password = cryptoLib.encrypt(request.new_password, shaKey, GLOBALS.IV);
            var upd_customer = {
                password: password
            };
            Authentication.update_user(user_id, upd_customer, function (userdata, error) {
                if (error) {
                    callback('0', t('rest_keywords_something_went_wrong'), null);
                } else {
                    callback('1', t('rest_keywords_user_change_password_success'), userdata);
                }
            });
        }
    },

    /*
    ** Function to create password
    */
    create_password: function (user_id, request, currentpass, callback) {

        if (request.password != request.confirm_password) {
            callback('0', t('rest_keywords_password_mismatch'), null);
        } else if (currentpass == request.password) {
            callback('0', t('rest_keywords_user_newold_password_similar'), null);
        } else {
            var password = cryptoLib.encrypt(request.password, shaKey, GLOBALS.IV);
            var upd_customer = {
                password: password
            };
            Authentication.update_user(user_id, upd_customer, function (userdata, error) {
                if (error) {
                    callback('0', t('rest_keywords_something_went_wrong'), null);
                } else {
                    callback('1', t('rest_keywords_user_create_password_success'), userdata);
                }
            });
        }
    },

    /*
    ** Function to logout
    */
    logout: function (user_id, callback) {
        var qry = con.query("UPDATE tbl_user SET login_status = 'Offline' WHERE id = '" + user_id + "'", function (err, result, fields) {
            if (!err) {
                var update_logoutparam = {
                    token: "",
                    device_type: "",
                    device_token: '0'
                };
                var queys = con.query("UPDATE tbl_user_deviceinfo SET ? WHERE user_id = '" + user_id + "' AND user_type= 'U'", update_logoutparam, function (err, result, fields) {
                    callback('1', t('rest_keywords_userlogout_success'), null);
                })
            }
            else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    delete_user: function (user_id, callback) {
        var qry = con.query("UPDATE tbl_user SET login_status = 'Offline' , is_deleted = '1' WHERE id = '" + user_id + "'", function (err, result, fields) {
            console.log(user_id);
            console.log(err);
            console.log(result);
            if (!err) {
                var update_logoutparam = {
                    token: "",
                    device_type: "",
                    device_token: '0'
                };
                var queys = con.query("UPDATE tbl_user_deviceinfo SET ? WHERE user_id = '" + user_id + "' AND user_type= 'U'", update_logoutparam, function (err, result, fields) {
                    callback('1', t('rest_keywords_user_deleted_success'), null);
                })
            }
            else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },
    /*
    ** Function to add contact details
    */
    add_contactus: function (user_id, request, callback) {

        var param =
        {
            user_id: user_id,
            email: request.email,
            name: request.name,
            description: request.description
        }
        con.query("SELECT * FROM tbl_user WHERE id= '" + user_id + "' AND is_deleted = '0' ", function (err, res, fields) {
            if (!err && res[0] != undefined) {
                var query = con.query("INSERT INTO tbl_contact_us SET ?", [param], function (err, results) {
                    if (!err && results != undefined) {
                        callback(results);
                    }
                    else {
                        callback(null);
                    }
                });
            } else {
                callback(null);
            }
        })
    },

    home_video_list: function (callback) {
        var sql = "SELECT *,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.HOME_VIDEO + "','',image) AS image_url ,concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.HOME_VIDEO + "','',thumb_image) AS thumb_image_url , concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.HOME_VIDEO + "','',video) AS video_url FROM `tbl_home_video` where is_deleted = '0'";
        con.query(sql, function (error, result, fields) {
            if (!error && result.length > 0) {
                callback("1", t("rest_keywords_home_video_success"), result);
            } else if (!error) {
                callback("2", t("rest_keywords_home_video_failed"), result);
            } else {
                callback("0", t("rest_keywords_home_video_failed"), null);
            }
        })
    },

    /*
    ** Function to upgrade student profile into main profile
    */
    upgrade_tomainprofile: function (user_id, request, callback) {
        con.query("SELECT * FROM tbl_student WHERE user_id = '" + user_id + "' AND id='" + request.student_id + "'", function (error1, result1, fields1) {
            console.log(this.sql)
            if (!error1 && result1 != undefined) {

                var customer = {
                    firstname: result1[0].firstname,
                    lastname: result1[0].lastname,
                    username: result1[0].firstname + " " + result1[0].lastname,
                    country_code: request.country_code,
                    phone: request.phone,
                    email: request.email,
                    country: request.country,
                    dob: result1[0].dob,
                    profile_image: result1[0].profile_image,
                    profile_video: result1[0].profile_video,
                    otp_code: '1234',
                    password: cryptoLib.encrypt(request.password, shaKey, GLOBALS.IV),
                    region_id: request.region_id,
                    age:result1[0].age,
                    age_category: result1[0].age_category
                };
                var queys = con.query("INSERT INTO tbl_user SET ?", customer, function (err, result, fields) {
                    console.log(this.sql)
                    console.log(result.insertId)
                    if (!err) {
                        con.query("SELECT *, concat('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.USER_IMAGE + "','',profile_image) as profile_image FROM tbl_user WHERE id='" + result.insertId + "'", function (uerr, ures, ufileds) {
                            if (!uerr && ures != undefined) {
                                var qry = con.query("DELETE FROM tbl_student WHERE id = '" + result1[0].id + "'", function (errd, resultd, fieldsd) { });
                                callback('1', t('rest_keywords_upgrade_tomainprofile'), ures[0]);
                            } else {
                                callback('2', t('rest_keywords_something_went_wrong'), null);
                            }
                        })
                    } else {
                        callback('2', t('rest_keywords_something_went_wrong'), null);
                    }
                })
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },

    region_list:function(request,callback){
        var where =" ";
        if(request.country_id != '' && request.country_id != undefined){
            where += " AND country_id = '"+request.country_id+"'";
        }
        var sql = "SELECT * FROM suasdb.tbl_tax_details WHERE is_active = '1' AND is_deleted = '0'"+where;
        con.query(sql,function(error,result){
            if(!error && result.length > 0){
                callback("1",t("rest_keywords_region_list_success"),result);
            }else{
                callback("0",t("rest_keywords_region_list_failed"),null);
            }
        })
    }
}

module.exports = Authentication;