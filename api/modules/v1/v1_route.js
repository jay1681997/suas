const express = require('express');
const app     = express();

var auth      = require('./Auth/route');
var user      = require('./User/route');
var service   = require('./Service/route');
var payment   = require('./Payment/route');

app.use('/auth', auth);
app.use('/user', user);
app.use('/service', service);
app.use('/payment', payment);
module.exports = app;