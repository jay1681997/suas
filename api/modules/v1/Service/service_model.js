var con              = require('../../../config/database');
var GLOBALS          = require('../../../config/constants');
var common           = require('../../../config/common');
var asyncLoop        = require('node-async-loop');
// Language file load
const { t }          = require('localizify');
const { request }    = require('express');


var Service   = {

	/* 
    ** Function to get country code list
    */
	get_countrycode_list: function(request, callback) {
        const limit =  ((request.page * GLOBALS.PER_PAGE) - GLOBALS.PER_PAGE);
        const per_page =  GLOBALS.PER_PAGE;
        con.query("SELECT *, dial_code as country_code FROM tbl_master_countries WHERE is_active = '1' AND is_deleted = '0' GROUP BY dial_code ORDER BY dial_code ", function(error, results, fields) {   
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_countrycode_list_found'), results);
            } else {
                callback('0', t('rest_keywords_countrycode_list_failed'));
            }
        });
    },

     /**
     * This Function is used to get the country list
     * @param {Request} request 
     * @param {Function} callback 
     */
    country_list: function (request, callback) {
        // var page = Globals.PER_PAGE;
        // var start = (request.page - 1) * page;
        var sql = "SELECT *,CONCAT('" + GLOBALS.S3_BUCKET_ROOT + GLOBALS.COUNTRY_FLAG_IMAGE + "',flag) AS flag_image FROM `tbl_country` WHERE is_active = '1' AND is_deleted = '0' ORDER BY  name ASC";
        con.query(sql, function (error, result, field) {
            if (!error && result.length > 0) {
                callback("1", t('rest_keywords_countrycode_list_found'), result);
            } else if (!error) {
                callback("2", t('rest_keywords_countrycode_list_found'), result);
            } else {
                callback("0", t('rest_keywords_countrycode_list_failed'), null);
            }
        })
    },
   

    /* 
    ** Function to get subscription list
    */
    get_subscription_list: function(user_id , callback) {
       var promocode_list_data = [];
       con.query("SELECT * FROM suasdb.tbl_user where id = '"+user_id+"'",function(usererror,resultss){
        con.query("SELECT *, (SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_master_subscription WHERE status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
            console.log(error);
            console.log(results);
            if (!error && results[0] != undefined) {
             
                asyncLoop(results, function(item, next){
                    con.query("SELECT tor.* FROM tbl_order AS tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id WHERE od.item_type='subscription' AND od.item_id ='"+item.id+"' AND tor.user_id = '"+user_id+"' AND tor.status='Confirm' AND tor.is_deleted = '0' AND od.is_deleted  = '0'", function(err, res, fild){
                        if(!err && res[0] != undefined){
                            // item.is_purchased = '1';
                            console.log("IF");
                            next();
                        }else{
                              console.log("ELSE");
                            promocode_list_data = promocode_list_data.concat(item);
                            // item.is_purchased = '0';
                            // promocode_list_data = promocode_list_data.concat(item);
                            next();
                        }
                    });
                }, function(){
                      callback('1', t('rest_keywords_subscription_list_found'), promocode_list_data);
                })

                
            } else {
                callback('0', t('rest_keywords_subscription_list_failed'));
            }
        });
    });
    },

    /* 
    ** Function to get contests list
    */
    get_contests_list: function(user_id, request, callback) {
        var where= "";
        if ((request.contest_type != undefined && request.contest_type != ''))  {
            where += " contest_type = '"+request.contest_type+"' AND ";
        }
        if((request.contest_id != undefined && request.contest_id != '')){
            where += " id = '"+request.contest_id+"' AND ";
        }
        if(request.filter_byage != undefined && request.filter_byage != ''){
            where += " age_category = '"+request.filter_byage+"' AND ";
        }
        con.query("SELECT * FROM tbl_user WHERE id = '"+user_id+"' AND is_deleted='0'", function(errorss, resultss, fieldss){
            if(!errorss && resultss[0] != undefined){
                if(request.student_id != undefined && request.student_id != ''){
                    con.query("SELECT * FROM tbl_student WHERE id ='"+request.student_id+"' AND user_id ='"+user_id+"' AND is_deleted ='0'", function(err, res, fields){
                        console.log(this.sql);
                        console.log(res);
                        console.log("!!!!!!!!!");
                        if(!err && res[0] != undefined){
                            // age = '"+res[0].age+"' AND 
                            if(request.filter_byage != undefined && request.filter_byage != ''){
                               // where += " age_category = '"+request.filter_byage+"' AND ";
                            }else{
                                where += " age_category = '"+res[0].age_category+"' AND"; 
                            }
                            con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', contest_image) as contest_image, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', thumb_image) as thumb_image,(SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_master_contest WHERE "+where+" status = 'Active' AND is_deleted = '0' AND contest_type = '"+request.contest_type+"' GROUP BY id ORDER BY id DESC ", function(error, results, fields) {   
                                console.log(this.sql);
                                console.log("!!!!!!!!!!!!");
                                if (!error && results[0] != undefined) {
                                    asyncLoop(results, function(item, next){
                                        con.query("SELECT o.* FROM tbl_order AS o  LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_id='"+item.id+"' AND od.item_type = 'contest' AND o.user_id='"+user_id+"' AND o.status = 'Confirm' AND o.is_deleted='0'", function(ers, rsss, fields){
                                        console.log(this.sql)
                                           if(!ers){
                                                if(rsss[0] != undefined){
                                                    item.already_purchased ="yes";
                                                }else{
                                                  item.already_purchased ="no";
                                                }
                                            }else{
                                                item.already_purchased ="no";
                                            }

                                            Service.get_prize_list(user_id,item.id, function(prizeerr, prizermsg, prizedata){
                                                item.prizelist = prizedata; 
                                                Service.get_sponsor_list(item.id, function(sponsorerr, sponsorrmsg, sponsordata){
                                                    item.sponsorlist =  sponsordata;
                                                     // var score = results[0].scorecard_id;
                       
                                                    Service.get_scorecard_list_new(item.scorecard_id, function(scorecarderr, scorecardrmsg, scorecarddata){
                                                        item.scorecardlist =  scorecarddata;
                                                         console.log("P1");
                                                        next();
                                                    })
                                                })
                                            });
                                        })
                                        
                                       
                                    }, function(){

                                        console.log("AAAASDDSFDVFVfv");
                                        asyncLoop(results, function(item, next){
                                            con.query("SELECT * FROM suasdb.tbl_user_contest Where user_id = '"+user_id+"' AND contest_id = '"+item.id+"'",function(upload_contest_error,upload_contest_result){
                                                if(!upload_contest_error && upload_contest_result[0] != undefined){
                                                    console.log(upload_contest_result);
                                                      item.is_already_upload_video ="yes";
                                                      next();
                                                }else{
                                                       item.is_already_upload_video ="no";
                                                        next();
                                                }
                                            })
                                           }, function(){
                                            console.log(results);
                                            console.log("AGDGGCDVSGGBDCSBVF BF B");
                                                callback('1', t('rest_keywords_contests_list_found'), results);
                                        });
                                    });
                                } else {

                                    callback('0', t('rest_keywords_contests_list_failed',{contest_type:request.contest_type}));
                                }
                            });
                        }else{
                            callback('2', t('rest_keywords_student_details_not_found'));
                        }
                    }); 
                }else{
                    con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', contest_image) as contest_image, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', thumb_image) as thumb_image,(SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_master_contest WHERE "+where+" status = 'Active' AND is_deleted = '0'  AND contest_type = '"+request.contest_type+"' GROUP BY id ORDER BY id DESC ", function(error, results, fields) {   
                        console.log(this.sql);
                        console.log(results);
                        console.log(error);
                        if (!error && results[0] != undefined) {
                            asyncLoop(results, function(item, next){
                                con.query("SELECT * FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_id='"+item.id+"' AND od.item_type = 'contest' AND o.user_id='"+user_id+"' AND o.status = 'Confirm' AND o.is_deleted='0'", function(ers, rsss, fields){
                                        console.log(this.sql)
                                            if(!ers){
                                                if(rsss[0] != undefined){
                                                    item.already_purchased ="yes";
                                                }else{
                                                  
                                                  item.already_purchased ="no";
                                                }
                                            }else{
                                                item.already_purchased ="no";
                                            }

                                            Service.get_prize_list(user_id,item.id, function(prizeerr, prizermsg, prizedata){
                                                item.prizelist = prizedata; 
                                                Service.get_sponsor_list(item.id, function(sponsorerr, sponsorrmsg, sponsordata){
                                                    item.sponsorlist =  sponsordata;
                                                    Service.get_scorecard_list_new(item.scorecard_id, function(scorecarderr, scorecardrmsg, scorecarddata){
                                                        item.scorecardlist =  scorecarddata;
                                                         
                                                        next();
                                                    })
                                                })
                                            });
                                        })
                            }, function(){
                                console.log("AAAASDDSFDVFVfv");
                                        asyncLoop(results, function(item, next){
                                            con.query("SELECT * FROM suasdb.tbl_user_contest Where user_id = '"+user_id+"' AND contest_id = '"+item.id+"'",function(upload_contest_error,upload_contest_result){
                                                if(!upload_contest_error && upload_contest_result[0] != undefined){
                                                    console.log(upload_contest_result);
                                                      item.is_already_upload_video ="yes";
                                                      next();
                                                }else{
                                                       item.is_already_upload_video ="no";
                                                        next();
                                                }
                                            })
                                           }, function(){
                                            console.log(results);
                                            console.log("AGDGGCDVSGGBDCSBVF BF B");
                                                callback('1', t('rest_keywords_contests_list_found'), results);
                                        });
                                // callback('1', t('rest_keywords_contests_list_found'), results);
                            });
                            
                        } else {
                            callback('0', t('rest_keywords_contests_list_failed',{contest_type:request.contest_type}));
                        }
                    });
                }
            }else{
                callback('2', t('rest_keywords_userdetailsnot_found'));
            }
        })
    },

    /* 
    ** Function to get prize list
    */
    get_prize_list: function(user_id,contest_id, callback) {
    con.query("SELECT * FROM suasdb.tbl_user where id = '"+user_id+"'",function(usererror,resultss){
        con.query("SELECT *,(SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' LIMIT 0,1) AS price, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PRIZE_IMAGE+"','',prize_image) as prize_image, (SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"' LIMIT 0,1) as currency_symbol FROM tbl_prize WHERE contest_id='"+contest_id+"' AND status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
            console.log(this.sql);
            console.log(results[0]);
            console.log(error);
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_prize_list_found'), results);
            } else {
                callback('0', t('rest_keywords_prize_list_failed'),null);
            }
        });
    });
    },
    /* 
    ** Function to get prize list
    */
    get_masterprize_list: function(request, callback) {
     
        var where = "";
      
        if ((request.filter_by_points_min != undefined && request.filter_by_points_min != '') && (request.filter_by_points_max != undefined && request.filter_by_points_max != ''))  {
            where += " ((reward_point >= '"+request.filter_by_points_min+"' AND reward_point <= '"+request.filter_by_points_max+"')) AND ";
        }


        if ((request.filter_by_points_by500 != undefined && request.filter_by_points_by500 != ''))  {
            where += " reward_point >= '"+request.filter_by_points_by500+"' AND ";
        }
        console.log("==========================")
        console.log(where)
        con.query("SELECT *,  concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PRIZE_IMAGE+"','',prize_image) as prize_image FROM tbl_master_prize WHERE  "+where+" status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
            console.log(this.sql);
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_prize_list_found'), results);
            } else {
                callback('0', t('rest_keywords_prize_list_failed'),null);
            }
        });
    },

    get_sponsor_list: function(contest_id, callback) {
        con.query("SELECT ts.*,  concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.SPONSOR_LOGO+"','',ts.sponsor_logo) as sponsor_logo FROM tbl_sponsor AS ts LEFT JOIN tbl_master_contest AS tc ON ts.id = tc.sponsor_id WHERE tc.id='"+contest_id+"' AND ts.status = 'Active' AND ts.is_deleted = '0' GROUP BY ts.id ORDER BY ts.id DESC", function(error, results, fields) {   
            console.log(this.sql);
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_sponsor_list_found'), results);
            } else {
                callback('0', t('rest_keywords_sponsor_list_failed'),null);
            }
        });
    },

    get_scorecard_list: function(contest_id, callback) {
        con.query("SELECT * FROM tbl_contest_scorecard WHERE contest_id='"+contest_id+"' AND status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
            console.log(this.sql);
            if (!error && results[0] != undefined) {
                console.log(results);
                console.log("----------------");
               // con.query("SELECT SUM(Points) AS total_point FROM tbl_contest_scorecard WHERE contest_id='"+contest_id+"' AND status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error_sum, results_sum, fields) {   
                    // if(!error_sum){
                            // results[0].total_point = results_sum[0].total_point;
                            // results = results.concat(results_sum[0]);
                            // var data = array("results":request,"total_point":results_sum[0]['total_point']);
                           callback('1', t('rest_keywords_scorecard_list_found'), results);

                    // }else{
                    //        callback('1', t('rest_keywords_scorecard_list_found'), results);
                    // }
               // })
            } else {
                callback('0', t('rest_keywords_scorecard_list_failed'),null);
            }
        });
    },

       get_scorecard_list_new: function(scorecard_id, callback) {
        if(scorecard_id != '' && scorecard_id != undefined){
            const arr1 = scorecard_id.split(',');
            if(arr1[0] != undefined){
                scorecard = [];
                asyncLoop(arr1, function (item, next) {
                    con.query("SELECT * FROM tbl_contest_scorecard WHERE id='"+item+"' AND status = 'Active' AND is_deleted = '0'", function(error, results, fields) {  
                        if(!error && results[0] != undefined){
                            scorecard.push(results[0]);
                            next();
                        }
                    })
                }, function () {
                    callback('1', t('rest_keywords_scorecard_list_found'), scorecard);
              })
            }else{
                 callback('0', t('rest_keywords_scorecard_list_failed'),null);
            }
        }else{
            callback('0', t('rest_keywords_scorecard_list_failed'),null);
        }
        // con.query("SELECT * FROM tbl_contest_scorecard WHERE id='"+id+"' AND status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
        //     console.log(this.sql);  
        //     if (!error && results[0] != undefined) {
        //         console.log(results);
        //         console.log("----------------");
        //                    callback('1', t('rest_keywords_scorecard_list_found'), results[0]);

        //     } else {
        //         callback('0', t('rest_keywords_scorecard_list_failed'),null);
        //     }
        // });
    },

    /* 
    ** Function to get prize details
    */
    get_prize_details: function(request, callback){
        con.query("SELECT *,  concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PRIZE_IMAGE+"','',prize_image) as prize_image FROM tbl_master_prize WHERE id = '"+request.prize_id+"' AND status = 'Active' AND is_deleted = '0'", function(error, results, fields) {   
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_prize_details_found'), results[0]);
            } else {
                callback('0', t('rest_keywords_prize_details_failed'));
            }
        });
    },

    /* 
    ** Function to get promocode list
    */
    get_promocode_list: function(request,callback) {
        console.log(request);
        // if(request.already_purchased_promocode == "1"){
        //     sql = "SELECT p.*, TIMESTAMPDIFF(MONTH,p.start_date,p.end_date) as validity, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',p.promocode_image) as promocode_image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM `tbl_order` AS o LEFT JOIN tbl_promocode AS p ON o.item_id = p.id WHERE o.item_type = 'promocode' AND o.user_id = '"+user_id+"' AND p.status = 'Active' AND p.is_deleted = '0'AND o.status = 'Active' AND o.is_deleted = '0'";
        // }else{
            con.query("SELECT * FROM suasdb.tbl_user where id = '"+request.user_id+"'",function(usererror,resultss){
            sql = "SELECT *, TIMESTAMPDIFF(MONTH,start_date,end_date) as validity, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',promocode_image) as promocode_image, (SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_promocode WHERE end_date >= now() AND status = 'Active' AND is_deleted = '0' AND type = '0' GROUP BY id ORDER BY id DESC";
        // }
        con.query(sql, function(error, results, fields) {
        console.log(results);
        console.log(sql);   
        console.log("PROMOCODE LIST");
             var promocode_list_data = [];
            if (!error && results[0] != undefined) {
                asyncLoop(results, function(item, next){
                    con.query("SELECT o.* FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_type='promocode' AND od.item_id ='"+item.id+"' AND user_id = '"+request.user_id+"' AND o.status='Confirm' AND o.is_deleted = '0'", function(err, res, fild){
                        console.log(item.id);
                        console.log(request.user_id);
                        console.log(err);
                        console.log(res);
                        console.log("---------------AAAAAAAA");
                        if(!err && res[0] != undefined){
                            item.is_purchased = '1';
                            promocode_list_data = promocode_list_data.concat(item);
                            next();
                        }else{
                            item.is_purchased = '0';
                            // promocode_list_data = promocode_list_data.concat(item);
                            next();
                        }
                    });
                }, function(){
                    con.query("SELECT p.* ,@is_purchased := 1 AS is_purchased ,(SELECT (cr.rate*p.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM suasdb.tbl_promocode AS p JOIN tbl_user_promocode AS up ON p.id = up.promocode_id WHERE up.user_id = ? AND up.is_deleted = '0' AND p.is_deleted = '0' AND p.status = 'active'",request.user_id,function(user_error,user_result){
                        promocode_list_data = promocode_list_data.concat(user_result);
                        console.log(promocode_list_data);
                        results = results.concat(user_result);
                        if(request.already_purchased_promocode == '1'){
                               callback('1', t('rest_keywords_promocode_list_found'), promocode_list_data);
                        }else{
                                callback('1', t('rest_keywords_promocode_list_found'), results);
                        }
                    })
                })
                
                
            } else {
                con.query("SELECT p.*, @is_purchased := 1 AS is_purchased ,(SELECT (cr.rate*p.price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1 ) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol  FROM suasdb.tbl_promocode AS p JOIN tbl_user_promocode AS up ON p.id = up.promocode_id WHERE up.user_id = ? AND up.is_deleted = '0' AND p.is_deleted = '0' AND p.status = 'active'",request.user_id,function(user_error,user_result){
                        // promocode_list_data = promocode_list_data.concat(user_result);
                        // console.log(promocode_list_data);
                        // console.log();
                        if(!user_error && user_result[0] != undefined){
                               callback('1', t('rest_keywords_promocode_list_found'), user_result);
                        }else{
                            callback('0', t('rest_keywords_promocode_list_failed'));
                        }
                    })
                
            }
        });
    });
    },

    // /* 
    // ** Function to get user promocode list
    // */
    // get_user_promocode_list: function(user_id,callback) {
    //     var promocode_list_data = [];
    //     var sql = "SELECT o.id AS order_id , o.uniqueid AS uniqueid, p.*, TIMESTAMPDIFF(MONTH,p.start_date,p.end_date) as validity, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',p.promocode_image) as promocode_image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM `tbl_order` AS o LEFT JOIN tbl_promocode AS p ON o.item_id = p.id WHERE o.item_type = 'promocode' AND o.user_id = '"+user_id+"' AND p.status = 'Active' AND p.is_deleted = '0'AND o.status = 'Active' AND o.is_deleted = '0'";
    //     // con.query("SELECT *, TIMESTAMPDIFF(MONTH,start_date,end_date) as validity, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',promocode_image) as promocode_image, (SELECT currency_symbol FROM tbl_setting WHERE is_deleted = '0') as currency_symbol FROM tbl_promocode WHERE status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
    //         con.query(sql, function(error, results, fields) {
    //         console.log(sql);
    //         console.log(user_id);   
    //             console.log(error);
    //             console.log(results);
    //             console.log("---------------------------");
    //         // if (!error && results[0] != undefined) {
    //         //     asyncLoop(results, function(item, next){
    //         //         con.query("SELECT * FROM tbl_order WHERE item_type='promocode' AND user_id ='"+item.id+"' AND status='Confirm'", function(err, res, fild){
    //         //             if(!err && res[0] != undefined){
    //         //                 item.is_purchased = '1';
    //         //                  item_list_data = item_list_data.concat(item);
    //         //                 next();
    //         //             }else{
    //         //                 item.is_purchased = '0';
    //         //                 next();
    //         //             }
    //         //         });
    //         //     }, function(){
    //         //         callback('1', t('rest_keywords_promocode_list_found'), item_list_data);
    //         //     })
                
                
    //         // } else {
    //         //     callback('0', t('rest_keywords_promocode_list_failed'));
    //         // }
    //     });
    // },

    /* 
    ** Function to get promocode details
    */
    get_promocode_details: function(user_id, request, callback) {
 con.query("SELECT * FROM suasdb.tbl_user where id = '"+user_id+"'",function(usererror,resultss){
        con.query("SELECT *, TIMESTAMPDIFF(MONTH,start_date,end_date) as validity, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.PROMOCODE_IMAGE+"','',promocode_image) as promocode_image, (SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_promocode WHERE id='"+request.promocode_id+"' AND status = 'Active' AND is_deleted = '0'", function(error, results, fields) {   
            console.log("P1");
            if (!error && results[0] != undefined) {
                con.query("SELECT * FROM tbl_promocode_benefit WHERE promocode_id='"+request.promocode_id+"'", function(err, res, fild){
                    results[0].benefits = res;
                       console.log("P2");
                    con.query("SELECT o.* FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_id='"+request.promocode_id+"' AND od.item_type = 'promocode' AND o.user_id='"+user_id+"' AND o.status = 'Confirm' AND o.is_deleted='0'", function(ers, rsss, fields){
                        console.log(this.sql);
                       if(!ers && rsss[0] != undefined){
                        console.log(rsss[0]);
                        console.log('-----------------------');
                                results[0].already_purchased ="yes";
                                callback('1', t('rest_keywords_promocode_detail_found'), results[0]);
                            // }else{
                            //       results[0].already_purchased ="no";
                            //     // console.log('RRRRRRRRRRRRRRRRRRR');
                            //     // console.log(results[0]);
                            //     callback('1', t('rest_keywords_promocode_detail_found'), results[0]);
                            // }
                        }else{
                            con.query("SELECT * FROM suasdb.tbl_user_promocode WHERE user_id = ? AND promocode_id = ? AND is_deleted = '0'",[user_id,request.promocode_id],function(user_error,user_result){
                                if(!user_error && user_result[0] != undefined){
                                     results[0].already_purchased ="yes";
                            callback('1', t('rest_keywords_promocode_detail_found'), results[0]);
                                }else{
                                 results[0].already_purchased ="no";
                            callback('1', t('rest_keywords_promocode_detail_found'), results[0]);      
                                }
                            })
                              
                        }
                    })
                })
             
            } else {
                callback('0', t('rest_keywords_promocode_detail_failed'));
            }
        });
    });
    },

    /* 
    ** Function to get class list
    */
    get_class_list: function(user_id, request, callback){
        console.log(user_id)
   
        var table = '';
        var where = '';
        var condition = '';
        if(request.student_id != undefined && request.student_id != ''){
            table = 'tbl_student';
            where = "id = '"+request.student_id+"'";
        }else{
            table = 'tbl_user';
            where = "id = '"+user_id+"'";
        }  
        con.query("SELECT * FROM tbl_user WHERE id = '"+user_id+"'", function(usererr, resultss){
        con.query("SELECT * FROM "+table+" WHERE "+where+"", function(err, res, fields){
            console.log(res[0])
            if(!err && res[0] != undefined){

                if(request.filter_byage != undefined && request.filter_byage != ''){
                    condition = "AND age_category = '"+request.filter_byage+"'";
                }else{
                    condition = "AND age_category = '"+res[0].age_category+"'";
                }

                con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CLASS_IMAGE+"','',class_image) as class_image, (SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol  FROM tbl_master_class WHERE status = 'Active' AND is_deleted = '0' "+condition+" GROUP BY id ORDER BY id DESC ", function(error, results, fields) {   
                   
                    if (!error && results[0] != undefined) {

                        asyncLoop(results, function(item, next){
                            con.query("SELECT tor.* FROM tbl_order AS tor LEFT JOIN tbl_order_detail AS od ON tor.id = od.order_id  WHERE tor.user_id = '"+user_id+"' AND od.item_id = '"+item.id+"' AND tor.status='Confirm' AND od.item_type='class' AND od.is_deleted = '0'", function(errors, resultss, fields) {
                                console.log(this.sql);   
                    
                                if (!errors && resultss[0] != undefined) {
                                      con.query("SELECT COUNT(o.id) as remaining_spot FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_type='class' AND od.item_id = '"+item.id+"' AND o.status = 'Confirm'", function(er, ress, fields){
                                    item.is_purchased = 'yes';
                                      item.remaining_spot = (item.total_spot) - (ress[0].remaining_spot);
                                    next();
                                })
                           
                                } else {

                                    con.query("SELECT COUNT(o.id) as remaining_spot FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_type='class' AND od.item_id = '"+item.id+"' AND o.status = 'Confirm'", function(er, ress, fields){
                                       // console.log(ress[0]);
                                       // console.log(item.total_spot);
                                       // console.log("!MCMZMZM>!K!!<!>!");
                                        item.is_purchased = 'no';
                                        item.remaining_spot = (item.total_spot) - (ress[0].remaining_spot);
                                        next()
                                    })
                                    
                                }
                            });
                        }, function() {
                            callback('1', t('rest_keywords_class_list_found'), results);
                        })
                        
                    } else {
                        callback('0', t('rest_keywords_class_list_failed'));
                    }
                });
            }else{
                callback('2', t('rest_keywords_no_data_found'));
            }
        })  
        })
    },

    /* 
    ** Function to get class details
    */
    // get_class_details: function(user_id, request, callback){
       
    //     con.query("SELECT *,concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CLASS_IMAGE+"','',class_image) as class_image FROM tbl_master_class WHERE id = '"+request.class_id+"' AND is_deleted = '0'", function(error, results, fields) {   
    //         if (!error && results[0] != undefined) {

    //             con.query("SELECT tor.* FROM tbl_order tor JOIN tbl_master_class tc ON tor.item_id = tc.id WHERE tor.user_id = '"+user_id+"' AND tor.item_id = '"+request.class_id+"' AND tor.status='Confirm' AND tor.item_type='class'", function(errors, resultss, fields) {
    //                 console.log(this.sql);   
    //                 if (!errors && resultss[0] != undefined) {
    //                     results[0].is_purchased = 'yes';
    //                     results[0].is_purchased = resultss.remaining_spot;
    //                     callback('1', t('rest_keywords_class_details_found'), results[0]);
               
    //                 } else {

    //                     con.query("SELECT COUNT(id) as remaining_spot FROM tbl_order WHERE item_type='class' AND status = 'Confirm'", function(er, ress, fields){
    //                         results[0].is_purchased = 'no';
    //                         results[0].remaining_spot = (results[0].total_spot) - (ress[0].remaining_spot);
    //                         callback('1', t('rest_keywords_class_details_found'), results[0]);
    //                     })
                        
    //                 }
    //             });

                
    //         } else {
    //             callback('0', t('rest_keywords_class_details_failed'));
    //         }
    //     });
    // },

    
    /* 
    ** Function to get class details
    */
    get_contests_details: function(user_id, request, callback){
       
        con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', contest_image) as contest_image,  concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.CONTEST_IMAGE+"','', thumb_image) as thumb_image, FROM tbl_master_contest WHERE id = '"+request.contest_id+"' AND is_deleted = '0'", function(error, results, fields) {   
            if (!error && results[0] != undefined) {
                Service.get_prize_list(user_id,results[0].id, function(prizeerr, prizermsg, prizedata){
                    results[0].prizelist = prizedata; 
                    Service.get_sponsor_list(results[0].id, function(sponsorerr, sponsorrmsg, sponsordata){
                        results[0].sponsorlist =  sponsordata;
                        var score = results[0].scorecard_id;
                        const arr1 = score.split(',');
                        Service.get_scorecard_list(results[0].id, function(scorecarderr, scorecardrmsg, scorecarddata){
                            results[0].scorecardlist =  scorecarddata;
                            callback('1', t('rest_keywords_contests_details_found'), results[0]);
                        });
                    });
                });
            } else {
                callback('0', t('rest_keywords_contests_details_failed'));
            }
        });
    },

    /* 
    ** Function to get video list
    */
    get_video_list: function(user_id, callback) {
        con.query("SELECT * FROM suasdb.tbl_user where id = '"+user_id+"'",function(usererror,resultss){
        con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.VIDEOLIBRARY_IMAGE+"','',image) as image, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.VIDEOLIBRARY_IMAGE+"','',thumb_image) as thumb_image, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.VIDEOLIBRARY_IMAGE+"','',video) as video,(SELECT (cr.rate*price) AS price FROM suasdb.tbl_country AS c join tbl_currency_rate AS cr ON c.currency_code = cr.to_currency_code WHERE c.name = '"+resultss[0]['country']+"' ORDER BY cr.id DESC LIMIT 0,1) AS price,(SELECT currency_symbol FROM suasdb.tbl_country where name = '"+resultss[0]['country']+"') as currency_symbol FROM tbl_master_videolibrary WHERE status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY id DESC", function(error, results, fields) {   
            if (!error && results[0] != undefined) {

                asyncLoop(results, function(item, next){
                    con.query("SELECT o.* FROM tbl_order AS o LEFT JOIN tbl_order_detail AS od ON o.id = od.order_id  WHERE od.item_id='"+item.id+"' AND od.item_type = 'video' AND o.user_id='"+user_id+"' AND o.status = 'Confirm' AND o.is_deleted='0'", function(ers, rsss, fields){
                    console.log(this.sql)
                       if(!ers){
                            if(rsss[0] != undefined){
                                item.already_purchased ="yes";
                                next();
                            }else{
                              
                              item.already_purchased ="no";
                              next();
                            }
                        }else{
                            item.already_purchased ="no";
                            next();
                        }
                    })
                }, function(){
                    callback('1', t('rest_keywords_video_list_found'), results);
                });
                
            } else {
                callback('0', t('rest_keywords_video_list_failed'));
            }
        });
    });
    },

    /* 
    ** Function to get leaderboard list
    */
    get_leaderboard_list: function(callback) {

        // con.query("SELECT *, concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.LEADERBOARD_IMAGE+"','',image) as image FROM tbl_leaderboard WHERE status = 'Active' AND is_deleted = '0' GROUP BY id ORDER BY value DESC", function(error, results, fields) {   
            var sql = "SELECT sum(w.reward_points) AS value,u.firstname AS name,concat('"+GLOBALS.S3_BUCKET_ROOT+GLOBALS.USER_IMAGE+"','',u.profile_image) as image,u.country FROM suasdb.tbl_wallet AS w JOIN suasdb.tbl_user AS u ON w.user_id = u.id WHERE w.is_deleted = '0'GROUP BY w.user_id ORDER BY value DESC LIMIT 0, 50;"
            con.query(sql,function(error,results){
            console.log(error);
            console.log(results);
            if (!error && results[0] != undefined) {
                callback('1', t('rest_keywords_leaderboard_list_found'), results);
            } else {
                callback('0', t('rest_keywords_leaderboard_list_failed'));
            }
        });
    },
}


module.exports = Service;