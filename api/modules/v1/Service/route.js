var express       = require('express');
var service_model = require('./service_model');
var common        = require('../../../config/common');

// Language file load
const { t }       = require('localizify');
var router        = express.Router();

// /* 
// ** 08-Feb-2022
// ** Service API's
// ** Get country code list API
// */
// router.post("/get_countrycode_list", function(req, res){ 
//     common.decryption(req.body,function(request){
//         var request = request
//         var rules = {
//             page         : '',
//         }
//         const messages = {
//             'required'   : t('required')
//         }
//         var keywords = {
//             'page'       : t('rest_keywords_page'),
//         }
//         if(common.checkValidationRules(request,res,rules,messages,keywords)) 
//         {
//             service_model.get_countrycode_list(request, function(code, msg, userData){
//                 common.sendresponse(res, code, msg, userData);
//             });
//         }
//     });      
// });

/**
 * Country Listing api 
 */
router.post("/get_countrycode_list", function (req, res) {
    common.decryption(req.body, function (request) {
        service_model.country_list(request, function (responseCode, responseMsg, responseData) {
            common.sendresponse(res, responseCode, responseMsg, responseData);
        });
    })
})
/* 
** 10-Feb-2022
** Get subscription list API
*/
router.post("/get_subscription_list", function(req, res){ 

    service_model.get_subscription_list(req.user_id,function(code, msg, userData){
        common.sendresponse(res, code, msg, userData);
    });   
         
});

/* 
** 10-Feb-2022
** Get contests list API
*/
router.post("/get_contests_list", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            contest_type : 'required|in:live,video',
            student_id   : '',
            contest_type : ''
        }
        const messages = {
            'required'   : t('required'),
            'in'         : t('in'),
        }
        var keywords = {
            'page'        : t('rest_keywords_page'),
            'contest_type': t('rest_keywords_contest_type'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            service_model.get_contests_list(req.user_id, request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            });   
        }
    });   
});

router.post("/get_contests_list_details", function(req, res){ 
    console.log("GET CONTEST LIST DETAILS");
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            contest_id : 'required'
        }
        const messages = {
            'required'   : t('required'),
            'in'         : t('in'),
        }
        var keywords = {
            'contest_id'   : t('rest_keywords_contest_id')
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            console.log(request);
            console.log(")))))0000000");
            service_model.get_contests_list(req.user_id, request, function(code, msg, userData){
                
                common.sendresponse(res, code, msg, userData);
            });   
        }
    });   
});

/* 
** 10-Feb-2022
** Get contests details API
*/
router.post("/get_contests_details", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            contest_id   : 'required',
        }
        const messages = {
            'required'   : t('required')
        }
        var keywords = {
            'contest_id' : t('rest_keywords_contest_id'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            service_model.get_contests_details(req.user_id, request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            });   
        }
    });   
});

/* 
** 10-Feb-2022
** Get prize list API
*/
router.post("/get_prize_list", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            filter_by_points_min : '',
            filter_by_points_max : '',
            filter_by_points_by500 : ''
        }
        const messages = {
            'required'           : t('required')
        }
        var keywords = {
            'page'               : t('rest_keywords_page'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
             request.user_id = req.user_id;
            service_model.get_masterprize_list(request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            }); 
        }
    });         
});

/* 
** 15-Feb-2022
** Get prize details API
*/
router.post("/get_prize_details", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            prize_id   : 'required',
        }
        const messages = {
            'required' : t('required')
        }
        var keywords = {
            'prize_id' : t('rest_keywords_prize_id'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            service_model.get_prize_details(request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            });   
        }
    });   
});

/* 
** 10-Feb-2022
** Get promocode list API
*/
router.post("/get_promocode_list", function(req, res){ 
     common.decryption(req.body,function(request){
        var request = request
        var rules = {
            already_purchased_promocode    : 'required',
        }
        const messages = {
            'required'      : t('required')
        }
        var keywords = {
            'already_purchased_promocode'  : t('already_purchased_promocode'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
        // console.log(req);
        console.log(req.user_id);
        request.user_id = req.user_id;
        // request = req.user_id;
        service_model.get_promocode_list(request,function(code, msg, userData){
            common.sendresponse(res, code, msg, userData);
        });
    } 
})
});

// router.post("/get_promocode_list", function(req, res){ 
//     service_model.get_promocode_list(function(code, msg, userData){
//         common.sendresponse(res, code, msg, userData);
//     }); 
// });

/* 
** 15-Feb-2022
** Get promocode details API
*/
router.post("/get_promocode_details", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            promocode_id    : 'required',
        }
        const messages = {
            'required'      : t('required')
        }
        var keywords = {
            'promocode_id'  : t('rest_keywords_promocode_id'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            service_model.get_promocode_details(req.user_id, request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            });   
        }
    });   
});

/* 
** 10-Feb-2022
** Get class list API
*/
router.post("/get_class_list", function(req, res){ 
    common.decryption(req.body,function(request){
        var request = request
        var rules = {
            student_id   : '',
            filter_byage : ''
        }
        const messages = {
            'required'   : t('required')
        }
        var keywords = {
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) 
        {
            service_model.get_class_list(req.user_id, request, function(code, msg, userData){
                common.sendresponse(res, code, msg, userData);
            }); 
        }
    });      
});


/* 
** 10-Feb-2022
** Get video list API
*/
router.post("/get_video_list", function(req, res){ 
   
    service_model.get_video_list(req.user_id, function(code, msg, userData){
        common.sendresponse(res, code, msg, userData);
    }); 
            
});

/* 
** 10-Feb-2022
** Get Leaderboard list API
*/
router.post("/get_leaderboard_list", function(req, res){ 
   
    service_model.get_leaderboard_list(function(code, msg, userData){
        common.sendresponse(res, code, msg, userData);
    }); 
            
});

/**
 * 09-12-2022
 * Get the Promocode List which user have Buy
 * */
router.post("/get_user_promocode_list", function(req, res){ 
    service_model.get_user_promocode_list(req.user_id,function(code, msg, userData){
        common.sendresponse(res, code, msg, userData);
    }); 
});

module.exports = router;