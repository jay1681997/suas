var express         = require('express');
var router          = express.Router();
var payment_model   = require('./payment_model');
var common          = require('../../../config/common');

const {
    t
} = require('localizify');
var router = express.Router();

/* 
** 10-Feb-2022
** API to add card
*/
router.post("/add_card", function (req, res) {
    common.decryption(req.body, function (request) {
        var request = request
        var rules = {
            card_holder_name    : 'required',
            card_number         : 'required',
            expiry_month        : 'required',
            expiry_year         : 'required',
            security_code       : 'required',
        }
        const messages = {
            'required'          : t('required')
        }
        var keywords = {
            'card_holder_name'  : t('rest_keywords_card_holder_name'),
            'card_number'       : t('rest_keywords_card_number'),
            'expiry_month'      : t('rest_keywords_expiry_month'),
            'expiry_year'       : t('rest_keywords_expiry_year'),
            'security_code'     : t('rest_keywords_security_code'),
        }
        if(common.checkValidationRules(request,res,rules,messages,keywords)) {
            payment_model.add_card(req.user_id, request, function (code, msg, respData) {
                common.sendresponse(res, code, msg, respData);
            });
        }
    });
});

/* 
|| API to set primary card
|| 02-06-2021
*/
// router.post("/set_primary_card", function (req, res) {    
//     common.decryption(req.body, function(request) {
//         var request = request
//         var rules = {
//             "card_id"               : 'required'            
//         }
//         const messages = {
//             'required'              : lang[req.lang]['required'],
//             'in'                    : lang[req.lang]['in'],
//         }
//         var keywords = {
//             'card_id'               : lang[req.lang]['rest_keywords_card_id'],
//         }
//         if (common.checkValidationRules(request, res, rules, messages, keywords)) {
//             request.lang = req.lang;
//             request.user_id = req.user_id;
//             payment_model.set_primary_card(request, function(code, message, data) {
//                 common.sendresponse(res, code, message, data);
//             });    
//         }        
//     });
// });


/*
** 10-Feb-2022
** API to remove / delete user card details
*/
router.post("/get_user_cardlist", function(req, res) {
    payment_model.get_user_cardlist(req.user_id, function(code, message, data) {
        common.sendresponse(res, code, message, data);
    });    
});


/*
** 10-Feb-2022
** API to remove / delete user card details
*/
router.post("/remove_card", function(req, res) {
    common.decryption(req.body, function(request) {
        var request = request
        var rules = {
            'card_id'             : 'required|integer'
        }
        const messages = {
            'required'            : t('required'),
            'integer'             : t('integer')
        }
        var keywords = {
            'card_id'             : t('rest_keywords_card_id')
        }
        if (common.checkValidationRules(request, res, rules, messages, keywords)) {
            payment_model.remove_card(req.user_id, request, function(code, message, data) {
                common.sendresponse(res, code, message, data);
            });    
        }        
    });
});

/*
** 10-Feb-2022
** API to remove / delete user card details
*/
router.post("/check_usageofapi", function(req, res) {
    payment_model.check_usageofapi(req.user_id, function(code, message, data) {
        common.sendresponse(res, code, message, data);
    });    
});


module.exports = router;