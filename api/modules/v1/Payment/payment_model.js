var con             = require('../../../config/database');
var GLOBALS         = require('../../../config/constants');
var common          = require('../../../config/common');
var stripe          = require('../../../utilities/customstripe');
const asyncLoop     = require('node-async-loop');
const { t }        = require('localizify');
const { request }    = require('express');
var Payment = {

    /* 
    ** Function to add card
    */
    add_card: function(user_id, req, callback) {
        var cardobject = {
            "name"              : req.card_holder_name,
            "number"            : req.card_number,
            "exp_month"         : req.expiry_month,
            "exp_year"          : req.expiry_year,
            "cvc"               : req.security_code,
        }
        stripe.createCardToken(cardobject, function(code, message, token) {
            console.log(token)
            if (token != null) {
                con.query("SELECT * FROM tbl_user_card WHERE status = 'Active' AND user_id = '"+user_id+"' AND fingerprint = '"+ token.card.fingerprint +"'", function(err, result) {
                    if (!err && result.length > 0) {
                        callback('0', t('rest_keywords_card_already_exist'), null);
                    } else {
                        var customerObject = {
                            "source"    : token.id,
                            "email"     : req.invoice_email,
                            "description" : GLOBALS.APP_NAME + " User #" + req.user_id,
                            "address"   : {
                                "city"      : "Nagpur",
                                "country"   : "India",
                                "line1"     : "Mendhepathar",
                                "line2"     : "Katol",
                                "postal_code" : "441302",
                                "state"     : "Maharashtra",
                            }
                        }
                        stripe.createCustomer(customerObject, function(customercode, customermsg, customer) {
                            // console.log(customer)
                            if (customer != null) {
                                var param = {
                                    "user_id"           : user_id,
                                    "card_holder_name"  : req.card_holder_name,
                                    "card_number_last4" : token.card.last4,
                                    "fingerprint"       : token.card.fingerprint,
                                    "stripe_card_id"    : token.card.id,
                                    "card_token"        : token.id,
                                    "card_type"         : token.card.brand,
                                    "expiry_month"      : req.expiry_month,
                                    "expiry_year"       : req.expiry_year,
                                    "customer_id"       : customer.id,
                                }   
                                con.query(` INSERT INTO tbl_user_card SET ? `, param, function(error, results) {
                                    console.log(this.sql)
                                    if (!error && results.insertId > 0) {
                                        callback('1', t('rest_keywords_card_add_success'), null);
                                    } else {
                                        callback('0', t('rest_keywords_card_insert_issue'), null);
                                    }
                                }); 
                            } else {
                                callback(customercode, customermsg, null);
                            }
                        })
                    }
                })
            } else {
                callback(code, message, null);
            }
        });
    },

    /*
    ** set primary card
    */
    set_primary_card: function(req, callback) {
        con.query(` UPDATE tbl_user_card SET is_primary = '1' WHERE id = '${req.card_id}' AND user_id = ${req.user_id} `, function(error, results, fields) {     

            if (!error)  {
                con.query(` UPDATE tbl_user_card SET is_primary = '0' WHERE id != '${req.card_id}' AND user_id = ${req.user_id} `, function(upderror, updresults, fields) {     

                    if (!upderror) {
                        callback('1', t('rest_keywords_set_primary_card_success'), null);
                    } else {
                        callback('0', t('rest_keywords_something_went_wrong'), null);        
                    }
                })
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },   

    /*
    ** get  user card list
    */
    get_user_cardlist: function(user_id, callback) {
        con.query("SELECT c.*, CONCAT('*****', c.card_number_last4) as card_number_last4, c.card_number_last4 as card_last4 FROM tbl_user_card c  WHERE c.user_id = '"+user_id+"' AND c.status = 'Active' AND c.is_deleted = '0'", function(error, results, fields) {  
        console.log(this.sql)   
            if (!error && results.length > 0)  {
                results.forEach((item, key) => {
                    item.card_type = item.card_type.toString().replace(" ", "_");
                    item.card_image = GLOBALS.S3_BUCKET_ROOT + GLOBALS.CARD_IMAGE + item.card_type + '.png';
                    if (key == (results.length - 1)) {
                        callback('1', t('rest_keywords_user_cardlist_found_success'), results);
                    }
                });
            } else if (!error && results.length == 0)  {
                callback('2', t('rest_keywords_card_list_empty'), results);
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    },   
    
    /*
    ** remove / delete  user card details
    */
    remove_card: function(user_id, req, callback) {
        con.query("SELECT * FROM tbl_user_card WHERE id = '"+req.card_id+"' ", function(error, cards, fields) {

            if (!error && cards.length > 0) {
                stripe.deleteCustomer(cards[0].customer_id, function(cuscode, cusmessage, customer) {
                    con.query("DELETE FROM tbl_user_card WHERE id = '"+req.card_id+"'  ", function(delError, delResults, fields) {
                        if (!delError ) {
                            callback('1', t('rest_keywords_user_carddata_removed_success'), null);
                        } else {
                             callback('0', t('rest_keywords_something_went_wrong'), null);
                        }
                    })
                })
            } else {
                callback('0', t('rest_keywords_something_went_wrong'), null);
            }
        });
    }, 

    /*
    ** make payment
    */
    make_payment: function(user_id, req, callback) {
        console.log(req)
        common.getSettings(function(setting) {
            if(setting != null) {
                con.query("SELECT * FROM tbl_user_card WHERE id = '"+req.card_id+"' ", function(error, results) {
              
                    if (!error && results.length > 0) {
                        if(req.totalamount > 0){
                            var paymentObject = {
                            amount : Math.round(req.totalamount * 100),
                            // currency : toLowerCase(setting.currency),
                             currency : setting.currency,
                            capture : true,
                            customer : results[0].customer_id,
                            description : "Payment charge of user: #"+user_id,
                            // receipt_email : user.email
                        }

                        stripe.createStripeCharge(paymentObject,function(chargecode, chargemsg, charge) {
                            console.log("--------------------------------------------")
                            // console.log(charge);
                            console.log("--------------------------------------------")
                            if (charge != null) {
                                let temp = {
                                    'status'            : 'Confirm',
                                    'transaction_id'    : charge.id,
                                    'order_status'      : 'Paid',
                                    'payment_datetime'  : require('node-datetime').create().format('Y-m-d H:M:S'),
                                    'payment_status'    : charge.status,
                                    'order_token'       : charge.balance_transaction,
                                    'receipt_link'      : charge.receipt_url

                                }
                                con.query("UPDATE tbl_order SET ? WHERE id = '"+req.order_id+"'", temp, function(insError, insResults, fields) {
                                    console.log(this.sql);
                                    console.log(error);
                                    if (!insError ) {
                                        if(charge.status == 'succeeded') {
                                            callback('1', t('rest_keywords_payment_success'), charge.id);
                                        } else {
                                            callback('0', t('rest_keywords_payment_failed'), charge.id);    
                                        }
                                    } else {
                                        callback('0', t('rest_keywords_payment_failed'));
                                    }
                                });
                            } else { 
                                callback('0', t(chargemsg));
                            }
                        })
                        }else{
                            let temp = {
                                    'status'            : 'Confirm',
                                    'transaction_id'    : '0',
                                    'order_status'      : 'Paid',
                                    'payment_datetime'  : require('node-datetime').create().format('Y-m-d H:M:S'),
                                    'payment_status'    : 'Paid',
                                    'order_token'       : '',
                                    'receipt_link'      : ''

                                }
                                con.query("UPDATE tbl_order SET ? WHERE id = '"+req.order_id+"'", temp, function(insError, insResults, fields) {
                                    console.log(this.sql);
                                    console.log(error);
                                    if (!insError ) {
                                           callback('1', t('rest_keywords_payment_failed'), 0);    
                                        // if(charge.status == 'succeeded') {
                                        //     callback('1', t('rest_keywords_payment_success'), charge.id);
                                        // } else {
                                        //     callback('0', t('rest_keywords_payment_failed'), charge.id);    
                                        // }
                                    } else {
                                        callback('0', t('rest_keywords_payment_failed'));
                                    }
                                });
                        }
                        
                    } else {
                        callback('0', t('rest_keywords_card_data_not_found'));
                    }
                });
            } else {
                callback('0', t('rest_keywords_something_went_wrong'));
            }
        });
    },
}

module.exports = Payment;