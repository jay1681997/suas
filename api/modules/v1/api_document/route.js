var express 	= require('express');
var path 		= require('path');
var GLOBALS 	= require('../../../config/constants');
var user_model 	= require('../User/user_model');
app 			= express();
var router 		= express.Router();

//set the template engine ejs
app.set('view engine', 'ejs');

/*
** Function to load view of api document with all data
** 08-Feb-2022
*/
router.get('/api_doc', (req, res) => {
  res.render(path.join(__dirname + '/view/api_doc.ejs'), { GLOBALS: GLOBALS });
});

/*
** Function to load reference code page
** 08-Feb-2022
*/
router.get('/code', (req, res) => {
  res.render(path.join(__dirname + '/view/reference_code.ejs'), { GLOBALS: GLOBALS });
});

/*
** Function to load list of users 
** 08-Feb-2022
*/
router.get('/user_list', (req, res) => {
  	user_model.apiuserList(function (response) {
  		console.log(response)
    	res.render(path.join(__dirname + '/view/user_list.ejs'), { data: response,GLOBALS: GLOBALS })
  	});
});



module.exports = router;