var express    = require('express');
var path       = require('path');
var common     = require('../../config/common');
var globals    = require('../../config/constants');
var cron_model = require('./cron_model');
var cron       = require('cron');
var asyncLoop  = require('node-async-loop');
const moment   = require('moment');
var router     = express.Router();

/* 
** The function to check order shipment status every day 12:05
*/
var checkordershipment = new cron.CronJob({
    cronTime: '* * * * *',  //cron fire on every day 12:05
    onTick: function() {
        console.log("CRON");
        // cron_model.
        cron_model.checkordershipmentstatus().then((message)=>{
            console.log(message);
        });
          
    },
    start: true,
    timeZone: 'UTC'
});


/* 
** The function to check order shipment status every 30 min
*/
var checkshipment = new cron.CronJob({
    cronTime: '0 0 0/24 * * *', //run cron every 30 min
    onTick: function() {
        cron_model.changeage(function (code) {
            console.log(code);
        });
    },
    start: true,
    timeZone: 'UTC'
});

var online_class_notifications = new cron.CronJob({
    cronTime: '* * * * *',  //cron fire on every day 12:05
    onTick: function() {
        console.log("online_class_notifications");
        // cron_model.
        cron_model.online_class_notifications(function (code) {
            console.log(code);
        });        
    },
    start: true,
    timeZone: 'UTC'
});


/*
** Cron for send subscription reminder to user/business
** 08-05-2023
*/
var currency_rates = new cron.CronJob({
    // cronTime: '*/1 * * * *',
    cronTime: '1 0 * * *',
    // cronTime: '* * * * *',
    onTick: function() {

        console.log("SAVE CURRENCY RATES API CALLED...");
        // Save currency rates for all available countries
        cron_model.saveCurrencyRates(function(ratesSaved) {
            if (ratesSaved != null) {
                console.log(ratesSaved);
            } else {
                console.log(ratesSaved);
            }
        });
    },
    start: true,
    timeZone: 'UTC'
});
module.exports = router;