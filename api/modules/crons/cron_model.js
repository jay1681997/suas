var con = require('../../config/database');
var globals = require('../../config/constants');
var common = require('../../config/common');
var template = require('../../config/template');
var asyncLoop = require('node-async-loop');
var moment = require("moment");
var GLOBALS = require("../../config/constants");
// var languages = require('../../config/languages');

class Cron {

    /*
    ** Function for check order shipment status
    */
    checkordershipmentstatus() {
        return new Promise((resolve, reject) => {
            var sql = "SELECT * , TIMEDIFF(start_datetime,NOW()) AS dtime , convert_tz(start_datetime,'-5:30','UTC'), NOW() , convert_tz(current_timestamp(),'US/Eastern','UTC') FROM `tbl_master_class` WHERE is_send = 0 AND start_datetime >= now()";
            con.query(sql, function (error, results, fields) {
                if (!error && results.length > 0) {
                    results.forEach(element => {
                        var string = element['dtime'].split(":");
                        var h = parseInt(string[0]);
                        var m = parseInt(string[1]);
                        var s = parseInt(string[2]);
                        if (h == 0 && m < 30) {
                            template.reminderemail({ "program_title": element['program_title'], "dtime": string[1].concat(':', string[2]),'start_datetime':element['start_datetime'],'end_datetime':element['end_datetime']}, function (message) {
                                common.send_email('Reminder Mail', globals.EMAIL_ID, globals.TO_EMAIL, message, function (send_code) {
                                    if (send_code) {
                                        con.query("UPDATE tbl_master_class SET is_send = 1 WHERE id = '"+element['id']+"'",function(updaterror,updateresult){
                                            if(!updaterror){
                                                resolve("Reminer Mail send successfully");
                                            }else{
                                                resolve("Failed to send the reminer mail");
                                            }
                                        })
                                    } else {
                                        console.log("NOt Send");
                                    }
                                })
                            });
                        }
                    });
                } else {
                    resolve("No classes list get");
                }
            });
        });
    }


    changeage(callback) {
        con.query("SELECT * FROM `tbl_user` WHERE is_deleted = '0'", function (error, resuls) {
            if (!error && resuls.length > 0) {
                // resuls.forEach(element => {
                asyncLoop(resuls, function (element, next) {
                    var today = new Date();
                    var birthDate = new Date(element['dob']);
                    var age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    var customer = {};
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }
                    if (age >= '18') {
                        customer.age_category = 'Adults';
                    } else if (age >= 5 && age <= 8) {
                        customer.age_category = 'Primary';
                    } else if (age >= 9 && age <= 12) {
                        customer.age_category = 'Juniors';
                    } else {
                        customer.age_category = 'Teens';
                    }
                    customer.age = age;
                    if (customer.age_category != element['age_category']) {
                        con.query("UPDATE tbl_user SET ? WHERE id = '" + element['id'] + "'", customer, function (updateerror, updateresult) {
                            next();
                        })
                    } else {
                        next();
                    }
                }, function () {
                    callback('1');
                });

            } else {
                callback('1');
            }
        })
    }

    online_class_notifications(callback){
        var sql = "SELECT * , TIMEDIFF(start_datetime,NOW()) AS dtime , convert_tz(start_datetime,'-5:30','UTC'), NOW() , convert_tz(current_timestamp(),'US/Eastern','UTC') FROM `tbl_master_class` WHERE is_send = 0 AND start_datetime >= now()";
        con.query(sql, function (error, results, fields) {
            if (!error && results.length > 0) {
                    results.forEach(element => {
                        var string = element['dtime'].split(":");
                        var h = parseInt(string[0]);
                        var m = parseInt(string[1]);
                        var s = parseInt(string[2]);
                        if (h == 0 && m < 30) {
                            var sql_order = "SELECT ot.*,tod.item_id , tod.item_type FROM `tbl_order` AS ot JOIN tbl_order_detail AS tod ON tod.order_id = ot.id WHERE tod.item_id = '"+element.id+"' AND tod.item_type = 'class'";
                            con.query(sql_order,function(order_error,order_result){
                                  order_result.forEach(order_element =>{
                                        var send_notification = {
                                            sender_id : 1,
                                            sender_type : 'admin',
                                            receiver_id : order_element.user_id,
                                            receiver_type : 'user',
                                            primary_id : order_element.id,
                                            notification_tag : 'reminer_classes_start',
                                            message : "Your "+element.program_title+" class is start in "+m+":"+s,
                                            title : "Reminer classes Start",
                                        }
                                        common.sendPushNotification(send_notification, function(code){
                                        console.log(code);
                                       
                                        });
                                  })
                            })
                            
                        }
                    })
            }else{
                 callback('1');
            }
        });
    }

    saveCurrencyRates(callback) {
        common.getCountryList(function(countrylisting) {
            if (countrylisting != null) {
                
                var symbols = '';
                countrylisting.forEach((element, key) => {
                    if (key === (countrylisting.length - 1)) {
                        symbols += element.currency_code;
                    } else {
                        symbols += element.currency_code + ',';
                    }
                });
                var geturls = `http://api.exchangeratesapi.io/v1/latest?access_key=${GLOBALS.EXCHANGERATES_APIKEY}&base=USD&symbols=${encodeURIComponent(symbols)}`;
                var request = require('request');
                var options = {
                    'method': 'GET',
                    'url': geturls,
                    'headers': {
                        'apikey': GLOBALS.EXCHANGERATES_APIKEY
                    }
                };
                request(options, function (error, response) {
                    if (error) {
                        callback(error);
                    } else {
                        var result = JSON.parse(response.body);
                        if (result.success && !common.isEmptyObject(result.rates)) {

                            var output = Object.entries(result.rates).map(([key, value]) => ({key,value}));
                            
                            asyncLoop(output, function(item, next) {
                                con.query("SELECT * FROM tbl_currency_rate WHERE from_currency_code = '"+result.base+"' AND to_currency_code = '"+item.key+"' ", function(error, response) {
                                    if (!error && response.length > 0) {
                                        
                                        var ratesObject = {
                                            rate: item.value
                                        };
                                        con.query("UPDATE tbl_currency_rate SET ? WHERE id = ? ", [ratesObject, response[0].id], function (err, response1, fields) {
                                            next();
                                        });
                                    } else {
                                        var ratesObject = {
                                            from_currency_code: result.base,
                                            to_currency_code: item.key,
                                            rate: item.value
                                        };
                                        common.commonSingleInsert("tbl_currency_rate", ratesObject, function(insertId) {
                                            next();
                                        });
                                    }
                                });
                            }, function() {
                                callback("Success! Currency Updated!");
                            });

                        } else {
                            callback("Failure in Exchange Rates API");
                        }
                    }
                });

            } else {
                callback("Currency Rates: Country listing not available");
            }
        });
    }
};
module.exports = new Cron();