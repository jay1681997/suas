var GLOBALS     = require('../config/constants');
let stripe      = require('stripe')(GLOBALS.PAYMENT_SECRET_KEY);       
var fs          = require('fs');

class Customstripe {
    
    /*
    ** Function to generate token of card using stripe library
    ** 15-11-2019
    ** @param {Object Of Card Parameters} cardobject 
    ** @param {Function} callback 
    */
    createCardToken(cardobject, callback) {

        stripe.tokens.create({
            card: cardobject
        }, function (err, token) {
            if (err) {
                callback('0',err.message,null);
            } else {
                callback('1',"Card Token Generation Success",token);
            }
        });
    }

    /*
    ** Function to update card using stripe library
    ** 12-01-2021
    ** @param {Object Of Card Parameters} cardobject 
    ** @param {Function} callback 
    */
    updateCardDetails(cardDetails, cardobject, callback) {
        stripe.customers.updateSource(cardDetails.customer_id, cardDetails.stripe_card_id, cardobject, function (err, card) {
            if (err) {
                callback('0',err.message,null);
            } else {
                callback('1',"Card Update Success",card);
            }
        });
    }

    /*
    ** Function to delete card using stripe library
    ** 12-01-2021
    ** @param {Object Of Card Parameters} cardobject 
    ** @param {Function} callback 
    */
    deleteCardDetails(cardobject, callback) {
        stripe.customers.deleteSource(cardobject.customer_id, cardobject.card_id, function (err, card) {
            if (err) {
                callback('0',err.message,null);
            } else {
                callback('1',"Card Delete Success",card);
            }
        });
    }

    /*
    ** Function to create customer using the token of stripe
    ** 15-11-2019
    ** @param {Customer Obejct} customerobject 
    ** @param {Function} callback 
    */
    createCustomer(customerobject, callback) {
        stripe.customers.create(
            customerobject,
        function (err,customer) {
            if (err) {
                callback('0',err.message,null);
            } else {  
                callback('1', "Customer create Success",customer);
            }
        });
    }

    /*
    ** Function to create customer using the token of stripe
    ** 15-11-2019
    ** @param {Customer Obejct} customerobject 
    ** @param {Function} callback 
    */
    deleteCustomer(customer_id, callback) {
        stripe.customers.del(
            customer_id,
        function (err,customer) {
            if (err) {
                callback('0',err.message,null);
            } else {  
                callback('1', "Customer delete Success",customer);
            }
        });
    }
    
    /*
    ** Note : Without hold functionality. No need to capture it.
    ** Function to transfer money from card to stripe platform account without destination
    ** 15-11-2019
    ** @param {Payment Object Of Stripe} paymentobject 
    ** @param {Function} callback 
    */
    tranferStripePlatform(paymentobject, callback) {
        stripe.charges.create(paymentobject, function(errors, charge) {
            if (!errors && charge != undefined) {
                var charge = {
                    transaction_id : charge.id,
                    balance_transaction : charge.balance_transaction
                }
                callback('1',"Transfer success",charge);
            } else {
               callback('0',errors.message,null);
            }
        });
    }

    /*
    ** Function to upload users identity to stripe
    ** 23-01-2020
    ** @param {Request Data} request 
    ** @param {Function} callback 
    */
    uploadIdentityStripe(request, callback) {
        
        var fp = fs.readFileSync("./" + GLOBALS.BANK_IMAGE + request.bank_document);
        var file = {};
        stripe.files.create({
            purpose: 'identity_document',
            file: {
                data: fp,
                name: request.bank_document,
                type: 'application/octet-stream'
            }
        }, function (errors, fileUpload) {
            if (errors) {
                callback('0',errors.message,null);
            } else {
                file = fileUpload;
                callback('1',"File upload success",file);
            }
        });
    }
    // additional_verification
    uploadCompanyDocument(request, callback) {
        var fp = fs.readFileSync("./" + GLOBALS.BANK_IMAGE + request.document);
        var file = {};
        stripe.files.create({
            purpose: 'tax_document_user_upload',
            file: {
                data: fp,
                name: request.document,
                type: 'application/octet-stream'
            }
        }, function (errors, fileUpload) {
            if (errors) {
                callback('0',errors.message,null);
            } else {
                file = fileUpload;
                callback('1',"File upload success",file);
            }
        });
    }

    uploadAdditionalDoc(request, callback) {
        var fp = fs.readFileSync("./" + GLOBALS.BANK_IMAGE + request.additional_document);
        var file = {};
        stripe.files.create({
            purpose: 'additional_verification',
            file: {
                data: fp,
                name: request.additional_document,
                type: 'application/octet-stream'
            }
        }, function (errors, fileUpload) {
            if (errors) {
                callback('0',errors.message,null);
            } else {
                file = fileUpload;
                callback('1',"File upload success",file);
            }
        });
    }

    async registerPerson(account,personObj) {
        const person = await stripe.accounts.createPerson(account.id,personObj);
        return person;
    }

    /*
    ** Function to create account on stripe
    ** 23-01-2020
    ** @param {Account Object} accountObject 
    ** @param {Function} callback 
    */
    createAccount(accountObject, callback) {
        stripe.accounts.create(accountObject, function(errors, account) {
            if (!errors && account != undefined) {
                callback('1',"Account Created",account);
            } else {
                callback('0',errors.message,null);
            }
        });
    }

    /*
    ** Function to create stripe charge [Note : if payment object contains capture parameter set to false than need to capture this charge]
    ** @description This is not direct charge so transaction fees will be deducted from platform account [client account]
    ** @param {Payment Object} paymentobject 
    ** @param {Function} callback 
    */
    createStripeCharge(paymentobject, callback) {
        stripe.charges.create(paymentobject,function(err, charge) {
            if (!err && charge != undefined) {
                callback('1',"Charge Created",charge);
            } else {
                callback('0',err.message,null);
            }      
        });
    }

    /*
    ** Function to capture the charge which is created with capture false otherwise no need for capture
    ** @param {Charge ID} charge_id 
    ** @param {Function} callback 
    */
    captureStripeCharge(charge_id, callback) {
        stripe.charges.capture(charge_id,function(err, charge) {
            if (!err && charge != undefined) {
                callback('1',"Charge Captured",charge);
            } else {
                callback('0',err.message,null);
            }
        });
    }

    /*
    ** Function to refund the charge which is put on hold by stripe.charge method
    ** @param {Charge ID} charge_id 
    ** @param {Function} callback 
    */
    createChargeRefund(charge_id, callback) {
        stripe.refunds.create({charge: charge_id},function(error, refund) {
            if (!error && refund != undefined) {
                callback('1',"Charge Refunded",refund);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    /*
    ** Function to create subscription 
    ** @param {ITEMS} items 
    ** @param {Function} callback 
    */
    createSubscription(price, customer_id, callback) {
        stripe.subscriptions.create({customer: customer_id, items: [{price: price}]},function(error, subscription) {
            if (!error && subscription != undefined) {
                callback('1',"Create subscription success",subscription);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    /*
    ** Function to create product 
    ** @param {PRODUCT NAME} name 
    ** @param {Function} callback 
    */
    createProduct(product_name, callback) {
        stripe.products.create({name: product_name},function(error, product) {
            if (!error && product != undefined) {
                callback('1',"Product create Success",product);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    /*
    ** Function to create price 
    ** @param {PRODUCT NAME} name 
    ** @param {Function} callback 
    */
    createPrice(product_id, callback) {
        
        stripe.prices.create({product: product_id,  unit_amount: 100,currency: 'inr',recurring: {interval: 'month', interval_count:1}},function(error, prices) {
            if (!error && prices != undefined) {
                callback('1',"prices create Success",prices);
            } else {
                callback('0',error.message,null);
            }
        });
    }


    createPaymentLink(price, callback) {
  
        stripe.paymentLinks.create({line_items: [{price: price,quantity: 1,},],},function(error, prices) {
            if (!error && prices != undefined) {
                callback('1',"Payment link create Success",prices);
            } else {
                callback('0',error.message,null);
            }
        });
    } 

    // Retrived balance => Balance
    getBalance(callback) {
        stripe.balance.retrieve(function(error, balance) {
            console.log(balance)
            if (!error && balance != undefined) {
                callback('1',"Retrieved balance",balance);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    // Create Charge => Charges
    createCharge(callback) {
        stripe.charges.create({amount: 2000,currency: 'inr',source: 'tok_amex',description: 'My First Test Charge demo'}, function(error, charges) {
            console.log(charges)
             console.log(error)
            if (!error && charges != undefined) {
                callback('1',"charges",charges);
            } else {
                callback('0',error.message,null);
            }
        });
    }

     // Create customers => Customers
    createCustomers(callback) {
        stripe.customers.create({name: 'snehal h',description: 'create first user',email: 'snehalh@gmail.com'}, function(error, customers) {
            console.log(customers)
             console.log(error)
            if (!error && customers != undefined) {
                callback('1',"customers",customers);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    // Create Payment Intent => Payment Intents
    createPaymentIntent(object,callback) {
        stripe.paymentIntents.create(object, function(error, paymentIntents) {
            console.log(paymentIntents)
            console.log(error)
            if (!error && paymentIntents != undefined) {
                callback('1',"paymentIntents",paymentIntents);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    // Create Payment Intent => Payment Intents
    createPaymentIntent_old(callback) {
        stripe.paymentIntents.create({amount: 2000,currency: 'inr',payment_method_types: ['card']}, function(error, paymentIntents) {
            console.log(paymentIntents)
            console.log(error)
            if (!error && paymentIntents != undefined) {
                callback('1',"paymentIntents",paymentIntents);
            } else {
                callback('0',error.message,null);
            }
        });
    }

    //Create session 
    createSessions(price, callback) {
        stripe.checkout.sessions.create({success_url: 'https://example.com/success',cancel_url: 'https://example.com/cancel',line_items: [{price: price, quantity: 2}],mode: 'subscription'}, function(error, sessions) {
            console.log(sessions)
            console.log(error)
            if (!error && sessions != undefined) {
                callback('1',"sessions",sessions);
            } else {
                callback('0',error.message,null);
            }
        });
    }
   
  
}

// var stripeObj = new Customstripe();
module.exports = new Customstripe();